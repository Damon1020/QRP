﻿namespace QRPUserControl
{
    partial class RichTextEditor
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.FontComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.FontSizeComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.BoldButton = new System.Windows.Forms.ToolStripButton();
            this.ItalicButton = new System.Windows.Forms.ToolStripButton();
            this.UnderlineButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.FontColorButton = new System.Windows.Forms.ToolStripButton();
            this.BackGroundColorbutton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.justifyLeftButton = new System.Windows.Forms.ToolStripButton();
            this.justifyCenterButton = new System.Windows.Forms.ToolStripButton();
            this.justifyRightButton = new System.Windows.Forms.ToolStripButton();
            this.justifyFullButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.OrderedListButton = new System.Windows.Forms.ToolStripButton();
            this.UnOrderedListButton = new System.Windows.Forms.ToolStripButton();
            this.Outdentbutton = new System.Windows.Forms.ToolStripButton();
            this.IndentButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(2, 26);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(724, 215);
            this.webBrowser1.TabIndex = 1;
            // 
            // FontComboBox
            // 
            this.FontComboBox.DropDownWidth = 140;
            this.FontComboBox.Name = "FontComboBox";
            this.FontComboBox.Size = new System.Drawing.Size(140, 25);
            this.FontComboBox.SelectedIndexChanged += new System.EventHandler(this.FontComboBox_SelectedIndexChanged);
            // 
            // FontSizeComboBox
            // 
            this.FontSizeComboBox.DropDownWidth = 87;
            this.FontSizeComboBox.Name = "FontSizeComboBox";
            this.FontSizeComboBox.Size = new System.Drawing.Size(87, 25);
            this.FontSizeComboBox.SelectedIndexChanged += new System.EventHandler(this.FontSizeComboBox_SelectedIndexChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // BoldButton
            // 
            this.BoldButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BoldButton.Image = global::QRPUserControl.Properties.Resources.bold;
            this.BoldButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BoldButton.Name = "BoldButton";
            this.BoldButton.Size = new System.Drawing.Size(23, 22);
            this.BoldButton.Text = "toolStripButton1";
            this.BoldButton.Click += new System.EventHandler(this.boldButton_Click);
            // 
            // ItalicButton
            // 
            this.ItalicButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ItalicButton.Image = global::QRPUserControl.Properties.Resources.italic;
            this.ItalicButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ItalicButton.Name = "ItalicButton";
            this.ItalicButton.Size = new System.Drawing.Size(23, 22);
            this.ItalicButton.Text = "toolStripButton2";
            this.ItalicButton.Click += new System.EventHandler(this.italicButton_Click);
            // 
            // UnderlineButton
            // 
            this.UnderlineButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UnderlineButton.Image = global::QRPUserControl.Properties.Resources.underscore;
            this.UnderlineButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UnderlineButton.Name = "UnderlineButton";
            this.UnderlineButton.Size = new System.Drawing.Size(23, 22);
            this.UnderlineButton.Text = "toolStripButton3";
            this.UnderlineButton.Click += new System.EventHandler(this.underlineButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // FontColorButton
            // 
            this.FontColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.FontColorButton.Image = global::QRPUserControl.Properties.Resources.color;
            this.FontColorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FontColorButton.Name = "FontColorButton";
            this.FontColorButton.Size = new System.Drawing.Size(23, 22);
            this.FontColorButton.Text = "toolStripButton1";
            this.FontColorButton.Click += new System.EventHandler(this.colorButton_Click);
            // 
            // BackGroundColorbutton
            // 
            this.BackGroundColorbutton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BackGroundColorbutton.Image = global::QRPUserControl.Properties.Resources.backcolor;
            this.BackGroundColorbutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.BackGroundColorbutton.Name = "BackGroundColorbutton";
            this.BackGroundColorbutton.Size = new System.Drawing.Size(23, 22);
            this.BackGroundColorbutton.Text = "toolStripButton2";
            this.BackGroundColorbutton.Click += new System.EventHandler(this.backColorButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // justifyLeftButton
            // 
            this.justifyLeftButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.justifyLeftButton.Image = global::QRPUserControl.Properties.Resources.lj;
            this.justifyLeftButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.justifyLeftButton.Name = "justifyLeftButton";
            this.justifyLeftButton.Size = new System.Drawing.Size(23, 22);
            this.justifyLeftButton.Text = "toolStripButton3";
            this.justifyLeftButton.Click += new System.EventHandler(this.justifyLeftButton_Click);
            // 
            // justifyCenterButton
            // 
            this.justifyCenterButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.justifyCenterButton.Image = global::QRPUserControl.Properties.Resources.cj;
            this.justifyCenterButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.justifyCenterButton.Name = "justifyCenterButton";
            this.justifyCenterButton.Size = new System.Drawing.Size(23, 22);
            this.justifyCenterButton.Text = "toolStripButton4";
            this.justifyCenterButton.Click += new System.EventHandler(this.justifyCenterButton_Click);
            // 
            // justifyRightButton
            // 
            this.justifyRightButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.justifyRightButton.Image = global::QRPUserControl.Properties.Resources.rj;
            this.justifyRightButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.justifyRightButton.Name = "justifyRightButton";
            this.justifyRightButton.Size = new System.Drawing.Size(23, 22);
            this.justifyRightButton.Text = "toolStripButton5";
            this.justifyRightButton.Click += new System.EventHandler(this.justifyRightButton_Click);
            // 
            // justifyFullButton
            // 
            this.justifyFullButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.justifyFullButton.Image = global::QRPUserControl.Properties.Resources.fj;
            this.justifyFullButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.justifyFullButton.Name = "justifyFullButton";
            this.justifyFullButton.Size = new System.Drawing.Size(23, 22);
            this.justifyFullButton.Text = "toolStripButton6";
            this.justifyFullButton.Click += new System.EventHandler(this.justifyFullButton_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // OrderedListButton
            // 
            this.OrderedListButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OrderedListButton.Image = global::QRPUserControl.Properties.Resources.ol;
            this.OrderedListButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OrderedListButton.Name = "OrderedListButton";
            this.OrderedListButton.Size = new System.Drawing.Size(23, 22);
            this.OrderedListButton.Text = "toolStripButton7";
            this.OrderedListButton.Click += new System.EventHandler(this.orderedListButton_Click);
            // 
            // UnOrderedListButton
            // 
            this.UnOrderedListButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.UnOrderedListButton.Image = global::QRPUserControl.Properties.Resources.uol;
            this.UnOrderedListButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UnOrderedListButton.Name = "UnOrderedListButton";
            this.UnOrderedListButton.Size = new System.Drawing.Size(23, 22);
            this.UnOrderedListButton.Text = "toolStripButton8";
            this.UnOrderedListButton.Click += new System.EventHandler(this.unorderedListButton_Click);
            // 
            // Outdentbutton
            // 
            this.Outdentbutton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Outdentbutton.Image = global::QRPUserControl.Properties.Resources.outdent;
            this.Outdentbutton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Outdentbutton.Name = "Outdentbutton";
            this.Outdentbutton.Size = new System.Drawing.Size(23, 22);
            this.Outdentbutton.Text = "toolStripButton1";
            this.Outdentbutton.Click += new System.EventHandler(this.outdentButton_Click);
            // 
            // IndentButton
            // 
            this.IndentButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.IndentButton.Image = global::QRPUserControl.Properties.Resources.indent;
            this.IndentButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.IndentButton.Name = "IndentButton";
            this.IndentButton.Size = new System.Drawing.Size(23, 22);
            this.IndentButton.Text = "toolStripButton2";
            this.IndentButton.Click += new System.EventHandler(this.indentButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FontComboBox,
            this.FontSizeComboBox,
            this.toolStripSeparator1,
            this.BoldButton,
            this.ItalicButton,
            this.UnderlineButton,
            this.toolStripSeparator2,
            this.FontColorButton,
            this.BackGroundColorbutton,
            this.toolStripSeparator3,
            this.justifyLeftButton,
            this.justifyCenterButton,
            this.justifyRightButton,
            this.justifyFullButton,
            this.toolStripSeparator4,
            this.OrderedListButton,
            this.UnOrderedListButton,
            this.Outdentbutton,
            this.IndentButton,
            this.toolStripSeparator5});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(727, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // RichTextEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "RichTextEditor";
            this.Size = new System.Drawing.Size(727, 243);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.ToolStripComboBox FontComboBox;
        private System.Windows.Forms.ToolStripComboBox FontSizeComboBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton BoldButton;
        private System.Windows.Forms.ToolStripButton ItalicButton;
        private System.Windows.Forms.ToolStripButton UnderlineButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton FontColorButton;
        private System.Windows.Forms.ToolStripButton BackGroundColorbutton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton justifyLeftButton;
        private System.Windows.Forms.ToolStripButton justifyCenterButton;
        private System.Windows.Forms.ToolStripButton justifyRightButton;
        private System.Windows.Forms.ToolStripButton justifyFullButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton OrderedListButton;
        private System.Windows.Forms.ToolStripButton UnOrderedListButton;
        private System.Windows.Forms.ToolStripButton Outdentbutton;
        private System.Windows.Forms.ToolStripButton IndentButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStrip toolStrip1;
    }
}
