﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가참조
using mshtml;
using System.Diagnostics;

namespace QRPUserControl
{
    public partial class RichTextEditor : UserControl
    {
        #region 전역변수
        private IHTMLDocument2 doc;
        #endregion

        #region 생성자
        public RichTextEditor()
        {
            InitializeComponent();
            InitializeRichTextBox();
            SetupFontComboBox();
            SetupFontSizeComboBox();
        }
        #endregion

        #region Initialize
        private void InitializeRichTextBox()
        {
            webBrowser1.DocumentText = "<HTML><BODY></BODY></HTML>";

            // 웹브라우져 편집가능하게 설정
            doc =
                webBrowser1.Document.DomDocument as IHTMLDocument2;
            doc.designMode = "ON";


            // 기본형식 지정
            webBrowser1.DocumentText = "<html><HEAD><style type="+"text/css"+">"+
                                        "<!--"+
                                        "p { MARGIN: 0cm 0cm 0pt class=MsoNormal }"+
                                        "-->"+
                                        "</style>"+
                                        "</HEAD><body>" +
                                        "</body></html>";

            // 부모창의 크기에 맞게 Webbrowser 창크기 자동조절
            this.webBrowser1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
        }
        #endregion

        #region FontComboBox
        // 폰트콤보박스 설정
        private void SetupFontComboBox()
        {
            // 자동완성기능
            AutoCompleteStringCollection ac = new AutoCompleteStringCollection();
            FontComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            FontComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            FontComboBox.AutoCompleteCustomSource = ac;

            // 지원되는 폰트를 콤보박스에 Add
            foreach (FontFamily fam in FontFamily.Families)
            {
                FontComboBox.Items.Add(fam.Name);
                ac.Add(fam.Name);
            }
            FontComboBox.SelectedIndex = FontComboBox.Items.IndexOf("SimSun");
        }

        // 폰트콤보박스의 값이 변경되었을때 이벤트
        private void FontComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FontFamily ff;
            try
            {
                ff = new FontFamily(FontComboBox.Text);
            }
            catch (Exception)
            {
                FontComboBox.Text = FontName.GetName(0);
                return;
            }
            FontName = ff;
        }
        #endregion

        #region FontSizeComboBox
        // 폰트사이즈콤보박스 설정
        private void SetupFontSizeComboBox()
        {
            for (int i = 1; i <= 7; i++)
            {
                this.FontSizeComboBox.Items.Add(i);
            }

            FontSizeComboBox.SelectedIndex = this.FontSizeComboBox.Items.IndexOf(3);
        }

        // 폰트사이즈콤보박스의 값이 변경되었을때 이벤트
        private void FontSizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (FontSizeComboBox.Text.Trim())
            {
                case "1":
                    FontSize = FontSize.One;
                    break;
                case "2":
                    FontSize = FontSize.Two;
                    break;
                case "3":
                    FontSize = FontSize.Three;
                    break;
                case "4":
                    FontSize = FontSize.Four;
                    break;
                case "5":
                    FontSize = FontSize.Five;
                    break;
                case "6":
                    FontSize = FontSize.Six;
                    break;
                case "7":
                    FontSize = FontSize.Seven;
                    break;
                default:
                    FontSize = FontSize.Seven;
                    break;
            }
        }
        #endregion

        #region SetColor
        #region SelectForeColor
        private void SelectForeColor()
        {
            // 다이얼로그 변수 선언 및 초기화, 속성지정
            ColorDialog ColorDlg = new ColorDialog();
            ColorDlg.AllowFullOpen = true;
            ColorDlg.AnyColor = true;
            ColorDlg.SolidColorOnly = false;
            ColorDlg.Color = Color.Black;

            // 다어일로그 호출 및 폰트색 적용
            if (ColorDlg.ShowDialog() == DialogResult.OK)
            {
                string colorstr =
                    string.Format("#{0:X2}{1:X2}{2:X2}", ColorDlg.Color.R, ColorDlg.Color.G, ColorDlg.Color.B);
                this.webBrowser1.Document.ExecCommand("ForeColor", false, colorstr);
            }
        }
        #endregion

        #region SelectBackColor
        private void SelectBackColor()
        {
            // 다이얼로그 변수 선언 및 초기화, 속성지정
            ColorDialog ColorDlg = new ColorDialog();
            ColorDlg.AllowFullOpen = true;
            ColorDlg.AnyColor = true;
            ColorDlg.SolidColorOnly = false;
            ColorDlg.Color = Color.White;

            if (ColorDlg.ShowDialog() == DialogResult.OK)
            {
                string colorstr =
                    string.Format("#{0:X2}{1:X2}{2:X2}", ColorDlg.Color.R, ColorDlg.Color.G, ColorDlg.Color.B);
                this.webBrowser1.Document.ExecCommand("BackColor", false, colorstr);
            }
        }
        #endregion
        #endregion

        #region Toll Strip 버튼 이벤트
        /// <summary>
        /// Called when the bold button on the tool strip is pressed.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void boldButton_Click(object sender, EventArgs e)
        {
            Bold();
        }

        /// <summary>
        /// Called when the italic button on the tool strip is pressed.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void italicButton_Click(object sender, EventArgs e)
        {
            Italic();
        }

        /// <summary>
        /// Called when the underline button on the tool strip is pressed.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void underlineButton_Click(object sender, EventArgs e)
        {
            Underline();
        }

        /// <summary>
        /// Called when the foreground color button on the tool strip is pressed.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void colorButton_Click(object sender, EventArgs e)
        {
            SelectForeColor();
        }

        /// <summary>
        /// Called when the background color button on the tool strip is pressed.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void backColorButton_Click(object sender, EventArgs e)
        {
            SelectBackColor();
        }

        /// <summary>
        /// Called when the outdent button on the toolstrip is clicked.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void outdentButton_Click(object sender, EventArgs e)
        {
            Outdent();
        }

        /// <summary>
        /// Called when the indent button on the toolstrip is clicked.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void indentButton_Click(object sender, EventArgs e)
        {
            Indent();
        }

        /// <summary>
        /// Event handler for the ordered list toolbar button
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void orderedListButton_Click(object sender, EventArgs e)
        {
            OrderedList();
        }

        /// <summary>
        /// Event handler for the unordered list toolbar button
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void unorderedListButton_Click(object sender, EventArgs e)
        {
            UnorderedList();
        }

        /// <summary>
        /// Event handler for the left HAlign toolbar button.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void justifyLeftButton_Click(object sender, EventArgs e)
        {
            JustifyLeft();
        }

        /// <summary>
        /// Event handler for the center justify toolbar button.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void justifyCenterButton_Click(object sender, EventArgs e)
        {
            JustifyCenter();
        }

        /// <summary>
        /// Event handler for the right justify toolbar button.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void justifyRightButton_Click(object sender, EventArgs e)
        {
            JustifyRight();
        }

        /// <summary>
        /// Event handler for the full justify toolbar button.
        /// </summary>
        /// <param name="sender">the sender</param>
        /// <param name="e">EventArgs</param>
        private void justifyFullButton_Click(object sender, EventArgs e)
        {
            JustifyFull();
        }
        #endregion

        #region 각 조건에맞게 Internet Explorer ExecCommand 명령어 호출 부분
        /// <summary>
        /// 문자 선택(text selection)의 번호있는 목록(OL)과 보통 블럭간의 전환을 한다.
        /// </summary>
        public void OrderedList()
        {
            webBrowser1.Document.ExecCommand("InsertOrderedList", false, null);
        }

        /// <summary>
        /// 문자 선택(text selection)을 번호있는 목록과 일반 블럭 양식을 서로 교차시킨다
        /// </summary>
        public void UnorderedList()
        {
            webBrowser1.Document.ExecCommand("InsertUnorderedList", false, null);
        }

        /// <summary>
        /// 문자 선택(text selection)이 위치한 불럭에서 왼똑에 위치시킨다.
        /// </summary>
        public void JustifyLeft()
        {
            webBrowser1.Document.ExecCommand("JustifyLeft", false, null);
        }

        /// <summary>
        /// 문자 선택(text selection)이 위치한 불럭에서 오른쪽에 위치시킨다.
        /// </summary>
        public void JustifyRight()
        {
            webBrowser1.Document.ExecCommand("JustifyRight", false, null);
        }

        /// <summary>
        /// 문자 선택(text selection)이 위치한 불럭에서 중앙에 위치시킨다.
        /// </summary>
        public void JustifyCenter()
        {
            webBrowser1.Document.ExecCommand("JustifyCenter", false, null);
        }

        /// <summary>
        /// 문자 선택(text selection)이 위치한 불럭에서 꽉채움 상태로 만든다.
        /// </summary>
        public void JustifyFull()
        {
            webBrowser1.Document.ExecCommand("JustifyFull", false, null);
        }

        /// <summary>
        /// 현재의 선책을 굵은 글자(bold)나 굵지않은 글자로 전환한다.
        /// </summary>
        public void Bold()
        {
            webBrowser1.Document.ExecCommand("Bold", false, null);
        }

        /// <summary>
        /// 문자 선택(text selection)에서 이태릭(italic) 문자와 보통 문자간 전환한다.
        /// </summary>
        public void Italic()
        {
            webBrowser1.Document.ExecCommand("Italic", false, null);
        }

        /// <summary>
        /// 현재 선택 문자에서 밑줄 그어진 부분과 밑줄 없는 부분 사이를 전환한다.
        /// </summary>
        public void Underline()
        {
            webBrowser1.Document.ExecCommand("Underline", false, null);
        }

        /// <summary>
        /// 현재 선택 문자를 한 증가분 만큼 뒤로 들여쓰기 한다.
        /// </summary>
        public void Indent()
        {
            webBrowser1.Document.ExecCommand("Indent", false, null);
        }

        /// <summary>
        /// 문자 선택(text selection)의 현위치에서 들어쓰기 한 증가분 만큼 왼쪽으로 내어쓰기 한다.
        /// </summary>
        public void Outdent()
        {
            webBrowser1.Document.ExecCommand("Outdent", false, null);
        }
        #endregion

        #region 폰트속성 변경/반환
        /// <summary>
        /// 폰트사이즈를 설정하거나 반환하는 함수
        /// </summary>
        [Browsable(false)]
        public FontSize FontSize
        {
            get
            {
                switch ((FontSizeComboBox.Text))
                {
                    case "1":
                        return FontSize.One;
                    case "2":
                        return FontSize.Two;
                    case "3":
                        return FontSize.Three;
                    case "4":
                        return FontSize.Four;
                    case "5":
                        return FontSize.Five;
                    case "6":
                        return FontSize.Six;
                    case "7":
                        return FontSize.Seven;
                    default:
                        return FontSize.NA;
                }
            }
            set
            {
                int sz;
                switch (value)
                {
                    case FontSize.One:
                        sz = 1;
                        break;
                    case FontSize.Two:
                        sz = 2;
                        break;
                    case FontSize.Three:
                        sz = 3;
                        break;
                    case FontSize.Four:
                        sz = 4;
                        break;
                    case FontSize.Five:
                        sz = 5;
                        break;
                    case FontSize.Six:
                        sz = 6;
                        break;
                    case FontSize.Seven:
                        sz = 7;
                        break;
                    default:
                        sz = 7;
                        break;
                }
                webBrowser1.Document.ExecCommand("FontSize", false, sz.ToString());
            }
        }

        /// <summary>
        /// 폰트명을 설정하거나 반환하는 함수
        /// </summary>
        [Browsable(false)]
        public FontFamily FontName
        {
            get
            {
                string name = doc.queryCommandValue("FontName") as string;
                if (name == null) return null;
                return new FontFamily(name);
            }
            set
            {
                if (value != null)
                    webBrowser1.Document.ExecCommand("FontName", false, value.Name);
            }
        }
        #endregion

        #region 추가부분(텍스트 내용을 변수에 저장, 또는 변수로부터 가져오기)
        public String GetDocumentText()
        {
            try
            {
                String GetDocumentText = this.webBrowser1.DocumentText.ToString();
                return GetDocumentText;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
            }
        }

        public void SetDocumentText(String strText)
        {
            try
            {
                //this.webBrowser1.DocumentText = strText;
                this.webBrowser1.Document.OpenNew(true);
                this.webBrowser1.Document.Write(strText);
                
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void Clear()
        {
            this.webBrowser1.Document.ExecCommand("SelectAll", true, null);
            this.webBrowser1.Document.ExecCommand("Delete", true, null);
        }
        #endregion
    }

    public enum FontSize
    {
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        NA
    }
}
