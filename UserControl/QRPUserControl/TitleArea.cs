﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가참조
using System.Resources;

namespace QRPUserControl
{
    public partial class TitleArea : UserControl
    {
        public TitleArea()
        {
            InitializeComponent();
            
        }

        private void TitleArea_Load(object sender, EventArgs e)
        {
            // 타이틀 배경 이미지 설정
            Bitmap img = new Bitmap(Properties.Resources.titlearea_full);
            BackgroundImage = img;
            AutoSize = false;
            Height = 40;
            Width = 1070;
            
            // Label Background 투명처리
            this.LB_Title.Appearance.BackColor = Color.Transparent;

            // 타이틀 이미지 설정
            img = new Bitmap(Properties.Resources.lbl_title);
            this.PB_Title.Image = img;
            this.PB_Title.AutoSize = true;

            //Text 위치 조정
            this.LB_Title.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            this.LB_Title.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
        }

        #region 속성창에서 타이틀 정의
        // Text
        [Description("Set Title Label Text"), Category("TitleText"), Browsable(true)]
        public String TextName
        {
            get
            {
                return LB_Title.Text;
            }
            set
            {
                LB_Title.Text = value;
            }
        }
        // Font
        [Description("Set Title Label Text Font"), Category("TitleText"), Browsable(true)]
        public Font FontName
        {
            get
            { return LB_Title.Font; }
            set
            { LB_Title.Font = value; }
        }
        // Text Color
        [Description("Set Title Label Text Color"), Category("TitleText"), Browsable(true)]
        public Color TextColor
        {
            get
            {
                return LB_Title.Appearance.ForeColor;
            }
            set
            {
                this.LB_Title.Appearance.ForeColor = value;
            }
        }
        #endregion

        #region 코드로 타이틀을 정할때 쓰이는 함수
        public void mfSetLabelText(String TitleName, String FontName, int FontSize)
        {
            // Text와 Font 설정
            this.LB_Title.Text = TitleName;
            this.LB_Title.Font = new Font(FontName, FontSize, FontStyle.Bold);

            // Text위치 조정
            this.LB_Title.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            this.LB_Title.Appearance.TextVAlign = Infragistics.Win.VAlign.Middle;
        }
        #endregion
    }
}
