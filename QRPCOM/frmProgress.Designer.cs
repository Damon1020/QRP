﻿namespace QRPCOM
{
    partial class frmProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.uActivityIndicator = new Infragistics.Win.UltraActivityIndicator.UltraActivityIndicator();
            this.uLabelFunction = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTimeTitle = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTime = new Infragistics.Win.Misc.UltraLabel();
            this.timerProgress = new System.Windows.Forms.Timer(this.components);
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.picLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // uActivityIndicator
            // 
            this.uActivityIndicator.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uActivityIndicator.CausesValidation = true;
            this.uActivityIndicator.Location = new System.Drawing.Point(112, 132);
            this.uActivityIndicator.Name = "uActivityIndicator";
            this.uActivityIndicator.Size = new System.Drawing.Size(276, 24);
            this.uActivityIndicator.TabIndex = 1;
            this.uActivityIndicator.TabStop = true;
            // 
            // uLabelFunction
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            appearance2.BackColor2 = System.Drawing.Color.Transparent;
            appearance2.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance2.TextHAlignAsString = "Center";
            this.uLabelFunction.Appearance = appearance2;
            this.uLabelFunction.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelFunction.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelFunction.Location = new System.Drawing.Point(216, 100);
            this.uLabelFunction.Name = "uLabelFunction";
            this.uLabelFunction.Size = new System.Drawing.Size(81, 16);
            this.uLabelFunction.TabIndex = 2;
            this.uLabelFunction.Visible = false;
            // 
            // uLabelTimeTitle
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            appearance3.BackColor2 = System.Drawing.Color.Transparent;
            appearance3.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance3.FontData.BoldAsString = "True";
            appearance3.TextHAlignAsString = "Center";
            this.uLabelTimeTitle.Appearance = appearance3;
            this.uLabelTimeTitle.AutoSize = true;
            this.uLabelTimeTitle.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelTimeTitle.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelTimeTitle.Location = new System.Drawing.Point(264, 180);
            this.uLabelTimeTitle.Name = "uLabelTimeTitle";
            this.uLabelTimeTitle.Size = new System.Drawing.Size(63, 16);
            this.uLabelTimeTitle.TabIndex = 3;
            this.uLabelTimeTitle.Text = "경과시간 : ";
            // 
            // uLabelTime
            // 
            appearance1.BackColor = System.Drawing.Color.Transparent;
            appearance1.BackColor2 = System.Drawing.Color.Transparent;
            appearance1.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance1.TextHAlignAsString = "Center";
            this.uLabelTime.Appearance = appearance1;
            this.uLabelTime.AutoSize = true;
            this.uLabelTime.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelTime.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.None;
            this.uLabelTime.Location = new System.Drawing.Point(327, 180);
            this.uLabelTime.Name = "uLabelTime";
            this.uLabelTime.Size = new System.Drawing.Size(0, 0);
            this.uLabelTime.TabIndex = 4;
            // 
            // timerProgress
            // 
            this.timerProgress.Tick += new System.EventHandler(this.timerProgress_Tick);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Font = new System.Drawing.Font("굴림", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uButtonClose.Location = new System.Drawing.Point(368, 172);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(16, 20);
            this.uButtonClose.TabIndex = 5;
            this.uButtonClose.Text = "X";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // picLogo
            // 
            this.picLogo.BackgroundImage = global::QRPCOM.Properties.Resources.STS_Logo;
            this.picLogo.Location = new System.Drawing.Point(256, 0);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(144, 32);
            this.picLogo.TabIndex = 7;
            this.picLogo.TabStop = false;
            // 
            // frmProgress
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(400, 201);
            this.ControlBox = false;
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uLabelTime);
            this.Controls.Add(this.uLabelTimeTitle);
            this.Controls.Add(this.uLabelFunction);
            this.Controls.Add(this.uActivityIndicator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmProgress";
            this.Load += new System.EventHandler(this.frmProgress_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProgress_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraActivityIndicator.UltraActivityIndicator uActivityIndicator;
        private Infragistics.Win.Misc.UltraLabel uLabelFunction;
        private Infragistics.Win.Misc.UltraLabel uLabelTimeTitle;
        private Infragistics.Win.Misc.UltraLabel uLabelTime;
        //private System.Windows.Forms.Timer timerProgress;
        public System.Windows.Forms.Timer timerProgress;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private System.Windows.Forms.PictureBox picLogo;

    }
}