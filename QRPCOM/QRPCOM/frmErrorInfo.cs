﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Resources;

namespace QRPCOM
{
    public partial class frmErrorInfo : Form
    {
        private Exception m_SystemExeption;

        public frmErrorInfo()
        {
            InitializeComponent();
        }

        public frmErrorInfo(Exception SystemException)
        {
            InitializeComponent();
            //m_Form = objForm;
            //m_strMethodName = strMethodName;
            m_SystemExeption = SystemException;
        }

        private void frmErrorInfo_Load(object sender, EventArgs e)
        {
            try
            {
                QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                this.Icon = Properties.Resources.qrpi;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinLabel winLabel = new QRPCOM.QRPUI.WinLabel();
                winLabel.mfSetLabel(this.uLabelErrorMethod, "Error Method", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelMessage, "Error Message", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                QRPCOM.QRPUI.WinButton btn = new QRPCOM.QRPUI.WinButton();
                btn.mfSetButton(this.uButtonClose, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);

                this.uTextErrorMethod.Text = m_SystemExeption.StackTrace;
                this.uTextErrorMessage.Text = m_SystemExeption.Message;
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }



    }
}
