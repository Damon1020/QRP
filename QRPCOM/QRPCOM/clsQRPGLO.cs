﻿/*----------------------------------------------------------------------*/
/* 시스템명     : QRP                                                   */
/* 모듈(분류)명 : QRP UI Library                                        */
/* 프로그램ID   : clsQRPGLO.cs                                          */
/* 프로그램명   : QRP 시스템 전역변수 및 Browser 관련                   */
/* 작성자       : 류근철                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-05 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//추가 참조
using System.Windows.Forms;
using System.Reflection;
using System.Net;
using System.IO;
using System.Collections;
using System.Data;
using System.Resources;

using System.EnterpriseServices;
using System.Configuration;
using System.Threading;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

using System.Drawing;
using System.Runtime.InteropServices;

using System.Xml.Linq;

namespace QRPCOM
{
    namespace QRPGLO
    {
        /// <summary>
        /// 툴바 활성화 Interface
        /// </summary>
        public interface IToolbar
        {
            void mfSearch();
            void mfSave();
            void mfDelete();
            void mfCreate();
            void mfPrint();
            void mfExcel();
        }

        /// <summary>
        /// 시스템 관련 전역변수
        /// </summary>
        public class QRPGlobal
        {
            //응용프로그램 실행 경로
            //private static string m_strRootPath = System.Environment.CurrentDirectory.ToString();
            private static string m_strRootPath = Application.StartupPath.ToString();

            //BL 및 UI 관련화일 위치 전역변수
            private string m_strUIDLLPath = "\\UI";

            //Core 관련화일 위치 전역변수
            private string m_strCoreDLLPath = "\\Core";

            //RPT 관련화일 위치 전역변수
            private string m_strRPTDLLPath = "\\RPT";

            //그리드에서 행추가 및 행수정시 RowSelector에 표시하는 이미지
            private System.Drawing.Bitmap m_imgModifyCellImage = new System.Drawing.Bitmap(Properties.Resources.grid_modifycell);

            //System관련 리소스
            private string m_strSystemInfoRes = m_strRootPath+"\\SysResources.resources";

            //System관련 리소스
            private string m_strToolInfoRes = m_strRootPath+"\\ToolResources.resources";

            //Control명의 다국어지원을 위한 리소스
            private string m_strControlLangRes = m_strRootPath+"\\LangResources.resources";

            //메시지명의 다국어지원을 위한 리소스
            private string m_strMessageLangRes = m_strRootPath+"\\MsgResources.resources";

            //로그인 유저 정보
            private static string m_strServerPath;      //로그인 서버경로
            private static string m_strPlantCode;       //로그인 유저 공장코드
            private static string m_strPlantName;       //로그인 유저 공장명
            private static string m_strRoutingCode;     //로그인 유저 공정흐름코드
            private static string m_strRoutingName;     //로그인 유저 공정흐름명
            private static string m_strUserID;          //로그인 유저ID
            private static string m_strUserName;        //로그인 유저명
            private static string m_strUserIP;          //로그인 유저IP
            private static string m_strUserDeptCode;    //로그인 유저 부서코드
            private static string m_strUserDeptName;    //로그인 유저 부서명
            private static string m_strLang;            //로그인 유저 선택언어
            private static string m_strEmail;           //로그인 유저 이메일

            private static string m_strSystemFontName;  //시스템에 적용할 폰트명

            public string RootPath
            {
                get { return QRPGlobal.m_strRootPath; }
            }

            public string UIDLLPath
            {
                get { return m_strUIDLLPath; }
            }

            public string CoreDLLPath
            {
                get { return m_strCoreDLLPath; }
            }

            public string RPTDLLPath
            {
                get { return m_strRPTDLLPath; }
            }

            public System.Drawing.Bitmap ModifyCellImage
            {
                get { return m_imgModifyCellImage; }
            }

            public string SystemInfoRes
            {
                get { return m_strSystemInfoRes; }
            }

            public string ToolInfoRes
            {
                get { return m_strToolInfoRes; }
            }

            public string ControlLangRes
            {
                get { return m_strControlLangRes; }
            }

            public string MessageLangRes
            {
                get { return m_strMessageLangRes; }
            }

            public string ServerPath
            {
                get { return QRPGlobal.m_strServerPath; }
                set { QRPGlobal.m_strServerPath = value; }
            }

            public string PlantCode
            {
                get { return QRPGlobal.m_strPlantCode; }
                set { QRPGlobal.m_strPlantCode = value; }
            }

            public string PlantName
            {
                get { return QRPGlobal.m_strPlantName; }
                set { QRPGlobal.m_strPlantName = value; }
            }

            public string RoutingCode
            {
                get { return QRPGlobal.m_strRoutingCode; }
                set { QRPGlobal.m_strRoutingCode = value; }
            }

            public string RoutingName
            {
                get { return QRPGlobal.m_strRoutingName; }
                set { QRPGlobal.m_strRoutingName = value; }
            }

            public string UserID
            {
                get { return QRPGlobal.m_strUserID; }
                set { QRPGlobal.m_strUserID = value; }
            }

            public string UserName
            {
                get { return QRPGlobal.m_strUserName; }
                set { QRPGlobal.m_strUserName = value; }
            }

            public string UserIP
            {
                get { return QRPGlobal.m_strUserIP; }
                set { QRPGlobal.m_strUserIP = value; }
            }

            public string UserDeptCode
            {
                get { return QRPGlobal.m_strUserDeptCode; }
                set { QRPGlobal.m_strUserDeptCode = value; }
            }

            public string UserDeptName
            {
                get { return QRPGlobal.m_strUserDeptName; }
                set { QRPGlobal.m_strUserDeptName = value; }
            }

            public string Lang
            {
                get { return QRPGlobal.m_strLang; }
                set { QRPGlobal.m_strLang = value; }
            }

            public string SystemFontName
            {
                get { return QRPGlobal.m_strSystemFontName; }
                set { QRPGlobal.m_strSystemFontName = value; }
            }

            public string Email
            {
                get { return QRPGlobal.m_strEmail; }
                set { QRPGlobal.m_strEmail = value; }
            }

            /// <summary>
            /// 로그인사용자 PC의 IP주소 얻기
            /// </summary>
            /// <returns></returns>
            public string mfGetUserIP()
            {
                try
                {
                    string strHostname = Dns.GetHostName();
                    IPAddress[] arrAddressIP = Dns.GetHostAddresses(strHostname);
                    string strAddress = string.Empty;
                    string[] arrAddress = new string[arrAddressIP.Length];

                    for (int i = 0; i <= arrAddressIP.Length - 1; i++)
                    {
                        //arrAddress[i] = arrAddressIP[i].ToString();
                        if (!arrAddressIP[i].IsIPv6LinkLocal && !arrAddressIP[i].IsIPv6Multicast && !arrAddressIP[i].IsIPv6SiteLocal)
                        {
                            strAddress = arrAddressIP[i].ToString();
                            break;
                        }
                    }

                    return strAddress;
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            ///  System관련 리소스 생성
            /// </summary>
            /// <param name="dtControlLang"></param>
            public void mfMakeSystemInfoResource(DataTable dtSystemInfo)
            {
                try
                {
                    IResourceWriter resWrite = new ResourceWriter(m_strSystemInfoRes);
                    for (int i = 0; i < dtSystemInfo.Rows.Count; i++)
                        resWrite.AddResource(dtSystemInfo.Rows[i]["NameKey"].ToString(), dtSystemInfo.Rows[i]["NameValue"].ToString());
                    resWrite.Close();
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            ///  사용자 권한 및 화면별 툴바 활성화 리소스 생성
            /// </summary>
            /// <param name="dtControlLang"></param>
            public void mfMakeToolInfoResource(DataTable dtSystemInfo)
            {
                IResourceReader rs = null;
                IResourceWriter resWrite = null;
                int i;
                try
                {
                    DataTable dtOrign = new DataTable();
                    dtOrign.Columns.Add("NameKey");
                    dtOrign.Columns.Add("NameValue");

                    bool bolCheck = false;
                    string strBrowserPath = Application.ExecutablePath;
                    int intPos = strBrowserPath.LastIndexOf(@"\");
                    //string strResFile = strBrowserPath.Substring(0, intPos + 1) + m_strToolInfoRes;
                    string strResFile = m_strToolInfoRes;
                    FileInfo file = new FileInfo(strResFile);

                    //툴바관련 리소스화일이 있는 경우
                    if (file.Exists)
                    {
                        try
                        {
                            //기존 리소스값 다시 Write하기 위해 DataTable에 저장
                            //IResourceReader rs = new ResourceReader(m_strToolInfoRes);
                            rs = new ResourceReader(m_strToolInfoRes);
                        }
                        catch
                        {
                            System.IO.File.Delete(m_strToolInfoRes);

                            //IResourceWriter resWrite = new ResourceWriter(m_strToolInfoRes);
                            resWrite = new ResourceWriter(m_strToolInfoRes);

                            for (i = 0; i < dtSystemInfo.Rows.Count; i++)
                            {
                                resWrite.AddResource(dtSystemInfo.Rows[i]["NameKey"].ToString(), dtSystemInfo.Rows[i]["NameValue"].ToString());
                            }
                            resWrite.Close();
                            return;
                        }

                        IDictionaryEnumerator en = rs.GetEnumerator();
                        DataRow dr;
                        while (en.MoveNext())
                        {
                            dr = dtOrign.NewRow();
                            dr["NameKey"] = en.Key.ToString();
                            dr["NameValue"] = en.Value.ToString();
                            dtOrign.Rows.Add(dr);
                        }
                        rs.Close();

                        //리소스 값 저장
                        //IResourceWriter resWrite = new ResourceWriter(m_strToolInfoRes);
                        resWrite = new ResourceWriter(m_strToolInfoRes);
                        
                        for (i = 0; i < dtSystemInfo.Rows.Count; i++)
                        {
                            //기존 리소스에 값이 있는지 체크한다.
                            int j;
                            for (j = 0; j < dtOrign.Rows.Count; j++)
                            {
                                if (dtOrign.Rows[j]["NameKey"].ToString() == dtSystemInfo.Rows[i]["NameKey"].ToString())
                                {
                                    bolCheck = true;
                                    break;
                                }
                            }
                            //없으면 리소스에 추가
                            if (bolCheck == false)
                                resWrite.AddResource(dtSystemInfo.Rows[i]["NameKey"].ToString(), dtSystemInfo.Rows[i]["NameValue"].ToString());
                            //있으면 기존리소스값 변경
                            else
                                dtOrign.Rows[j]["NameValue"] = dtSystemInfo.Rows[i]["NameValue"].ToString();
                            bolCheck = false;
                        }

                        //기존리소스 추가
                        for (i = 0; i < dtOrign.Rows.Count; i++)
                        {
                            resWrite.AddResource(dtOrign.Rows[i]["NameKey"].ToString(), dtOrign.Rows[i]["NameValue"].ToString());
                        }
                        resWrite.Close();
                    }
                    else
                    {
                        //IResourceWriter resWrite = new ResourceWriter(m_strToolInfoRes);
                        resWrite = new ResourceWriter(m_strToolInfoRes);

                        for (i = 0; i < dtSystemInfo.Rows.Count; i++)
                        {
                            resWrite.AddResource(dtSystemInfo.Rows[i]["NameKey"].ToString(), dtSystemInfo.Rows[i]["NameValue"].ToString());
                        }
                        resWrite.Close();
                    }
                }
                catch (Exception ex)
                {
                    //System.IO.File.Delete(m_strToolInfoRes);
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            ///  Control명의 다국어지원을 위한 리소스 생성
            /// </summary>
            /// <param name="dtControlLang"></param>
            public void mfMakeControlLangResource(DataTable dtControlLang)
            {
                try
                {
                    IResourceWriter resWrite = new ResourceWriter(m_strControlLangRes);
                    for (int i = 0; i < dtControlLang.Rows.Count; i++)
                        resWrite.AddResource(dtControlLang.Rows[i]["NameKey"].ToString(), dtControlLang.Rows[i]["NameLang"].ToString());
                    resWrite.Close();
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            ///  Message의 다국어지원을 위한 리소스 생성
            /// </summary>
            /// <param name="dtControlLang"></param>
            public void mfMakeMessageLangResource(DataTable dtMessageLang)
            {
                try
                {
                    IResourceWriter resWrite = new ResourceWriter(m_strMessageLangRes);
                    for (int i = 0; i < dtMessageLang.Rows.Count; i++)
                        resWrite.AddResource(dtMessageLang.Rows[i]["MessageKey"].ToString(), dtMessageLang.Rows[i]["MessageLang"].ToString());
                    resWrite.Close();
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            #region QRP 시스템 정보 팝업창
            /// <summary>
            /// QRP 시스템 정보 팝업창 열기
            /// </summary>
            /// <param name="frmUI"></param>
            public void mfOpenQRPSystemInfo(Form MdiForm)
            {
                try
                {
                    frmQRPInfo frm = new frmQRPInfo();
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.ShowDialog();
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            /// QRP 시스템 정보 팝업창 닫기
            /// </summary>
            public void mfCloseQRPSystemInfo()
            {
                try
                {
                    //Assembly _App = Assembly.LoadFrom(m_strRootPath + m_strCoreDLLPath + "\\QRPCOM.dll");
                    Assembly _App = Assembly.LoadFrom(m_strRootPath + "\\QRPCOM.dll");

                    Type frmType;
                    Form frm;
                    frmType = _App.GetType("frmQRPInfo");
                    frm = (Form)Activator.CreateInstance(frmType);
                    frm.Close();
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }
            #endregion

        }

        #region Browser 관련 Method
        public class QRPBrowser
        {
            //툴바 버튼
            private object m_objToolButton = null;

            /// <summary>
            /// 메뉴에서 선택한 Form을 여는 함수
            /// </summary>
            /// <param name="strModuleID"></param>
            /// <param name="strProgramID"></param>
            /// <param name="MDIParent"></param>
            /// <returns></returns>
            //public string mfOpenMenuForm(string strModuleID, string strProgramID, string strProgramName, Form MDIParent)
            public bool mfOpenMenuForm(string strModuleID, string strProgramID, string strProgramName, Form MDIParent)
            {
                try
                {
                    QRPGLO.QRPGlobal Path = new QRPCOM.QRPGLO.QRPGlobal();

                    //프로그램 Root Path
                    DirectoryInfo diRoot = new DirectoryInfo(Path.RootPath);

                    //UI관련 Dll 위치 Path
                    DirectoryInfo diUI = new DirectoryInfo(Path.RootPath + Path.UIDLLPath);

                    //모듈UI관련 File Full Path
                    string strUIFile = strModuleID + ".UI.dll";
                    FileInfo fiUIFullPath = new FileInfo(Path.RootPath + Path.UIDLLPath + "\\" + strUIFile);

                    //폴더가 있는지 체크하고 없는 경우 폴더를 생성한다. 
                    if (FindFolder(diRoot, Path.UIDLLPath) == false)
                        diRoot.CreateSubdirectory(Path.UIDLLPath);

                    //Open하려는 화일이 있는지 체크하고, 있는 경우 화일을 연다.
                    if (FindFile(diUI, fiUIFullPath.Name.ToString()) == false)
                    {
                        return false;
                    }
                    else
                    {
                        Assembly _App = Assembly.LoadFrom(fiUIFullPath.FullName);
                        string _st = fiUIFullPath.Name.ToString().Replace(".dll", "") + "." + strProgramID;

                        Type frmType;
                        Form frm;

                        ////선택한 Menu에 대한 Form의 Instance를 만들어 MDIChild로 Form을 띄운다.
                        frmType = _App.GetType(_st);
                        frm = (Form)Activator.CreateInstance(frmType);

                        frm.MdiParent = MDIParent;
                        frm.ControlBox = false;
                        frm.Dock = DockStyle.Fill;
                        frm.AutoScaleMode = AutoScaleMode.None;

                        //스크롤 정의//
                        //frm.AutoScroll = true;
                        //frm.AutoScrollMargin = new Size(5, 5);
                        //frm.AutoScrollMinSize = new Size(100, 500);
                        //frm.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                        ///////////////

                        frm.Icon = Properties.Resources.qrpi;
                        frm.FormBorderStyle = FormBorderStyle.None;

                        //if (MDIParent.WindowState == FormWindowState.Maximized)
                        //    frm.WindowState = FormWindowState.Maximized;
                        //else
                        frm.WindowState = FormWindowState.Normal;
                        frm.Text = strProgramName;

                        frm.Show();

                        //선택한 화면에 대한 다국어 설정
                        mfSetFormLanguage(frm);

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            /// 메뉴에서 선택한 Form을 여는 함수 : 외부프로그램으로부터 인자를 받아 실행하는 경우
            /// </summary>
            /// <param name="strModuleID"></param>
            /// <param name="strProgramID"></param>
            /// <param name="MDIParent"></param>
            /// <returns></returns>
            //public string mfOpenMenuForm(string strModuleID, string strProgramID, string strProgramName, Form MDIParent)
            public bool mfOpenMenuForm(string strModuleID, string strProgramID, string strProgramName, Form MDIParent, string strArgument)
            {
                try
                {
                    QRPGLO.QRPGlobal Path = new QRPCOM.QRPGLO.QRPGlobal();

                    //프로그램 Root Path
                    DirectoryInfo diRoot = new DirectoryInfo(Path.RootPath);

                    //UI관련 Dll 위치 Path
                    DirectoryInfo diUI = new DirectoryInfo(Path.RootPath + Path.UIDLLPath);

                    //모듈UI관련 File Full Path
                    string strUIFile = strModuleID + ".UI.dll";
                    FileInfo fiUIFullPath = new FileInfo(Path.RootPath + Path.UIDLLPath + "\\" + strUIFile);

                    //폴더가 있는지 체크하고 없는 경우 폴더를 생성한다. 
                    if (FindFolder(diRoot, Path.UIDLLPath) == false)
                        diRoot.CreateSubdirectory(Path.UIDLLPath);

                    //Open하려는 화일이 있는지 체크하고, 있는 경우 화일을 연다.
                    if (FindFile(diUI, fiUIFullPath.Name.ToString()) == false)
                    {
                        return false;
                    }
                    else
                    {
                        Assembly _App = Assembly.LoadFrom(fiUIFullPath.FullName);
                        string _st = fiUIFullPath.Name.ToString().Replace(".dll", "") + "." + strProgramID;

                        Type frmType;
                        Form frm;

                        ////선택한 Menu에 대한 Form의 Instance를 만들어 MDIChild로 Form을 띄운다.
                        frmType = _App.GetType(_st);
                        frm = (Form)Activator.CreateInstance(frmType);

                        //스크롤 정의//
                        frm.AutoScroll = true;
                        //frm.AutoScrollMargin = new Size(5, 5);
                        //frm.AutoScrollMinSize = new Size(100, 500);
                        //////////////

                        frm.MdiParent = MDIParent;
                        frm.ControlBox = false;
                        frm.Dock = DockStyle.Fill;
                        frm.Icon = Properties.Resources.qrpi;
                        frm.FormBorderStyle = FormBorderStyle.None;
                        frm.WindowState = FormWindowState.Normal;
                        frm.Text = strProgramName;
                        frm.Tag = strArgument;
                        frm.AutoScaleMode = AutoScaleMode.None;

                        frm.Show();

                        //선택한 화면에 대한 다국어 설정
                        mfSetFormLanguage(frm);

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ":" + ex.InnerException + ":" + ex.Source);
                    return false;
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            /// 메뉴에서 선택한 Form을 여는 함수 --> 여기서부터 시작 (Method에 넘겨줄 인자를 param 또는 다른 Type으로 처리해야 함)
            /// </summary>
            /// <param name="strModuleID"></param>
            /// <param name="strProgramID"></param>
            /// <param name="MDIParent"></param>
            /// <returns></returns>
            public Boolean mfOpenFormWithParam(string strModuleID, string strProgramID, Form MDIParent, string strMethodName)
            {
                try
                {
                    QRPGLO.QRPGlobal Path = new QRPCOM.QRPGLO.QRPGlobal();

                    //프로그램 Root Path
                    DirectoryInfo diRoot = new DirectoryInfo(Path.RootPath);

                    //UI관련 Dll 위치 Path
                    DirectoryInfo diUI = new DirectoryInfo(Path.RootPath + Path.UIDLLPath);

                    //모듈UI관련 File Full Path
                    string strUIFile = strModuleID + ".UI.dll";
                    FileInfo fiUIFullPath = new FileInfo(Path.RootPath + Path.UIDLLPath + "\\" + strUIFile);

                    //폴더가 있는지 체크하고 없는 경우 폴더를 생성한다. 
                    if (FindFolder(diRoot, Path.UIDLLPath) == false)
                        diRoot.CreateSubdirectory(Path.UIDLLPath);

                    //Open하려는 화일이 있는지 체크하고, 있는 경우 화일을 연다.
                    if (FindFile(diUI, fiUIFullPath.Name.ToString()) == false)
                        return false;
                    else
                    {
                        Assembly _App = Assembly.LoadFrom(fiUIFullPath.FullName);
                        string _st = fiUIFullPath.Name.ToString().Replace(".dll", "") + "." + strProgramID;
                        Type frmType;
                        Form frm;

                        //선택한 Menu에 대한 Form의 Instance를 만들어 MDIChild로 Form을 띄운다.
                        frmType = _App.GetType(_st);
                        frm = (Form)Activator.CreateInstance(frmType);

                        frm.MdiParent = MDIParent;
                        frm.ControlBox = false;
                        frm.Dock = DockStyle.Fill;
                        frm.FormBorderStyle = FormBorderStyle.None;
                        frm.WindowState = FormWindowState.Normal;
                        frm.Show();


                        //////////////메소드 생성
                        ////////////objMethod = objType.GetMethod("getTest");

                        //////////////objType에 해당하는 개체 인스턴스 생성..
                        //////////////이부분은 objAsm.CreateInstance("ClassLibrary1.Class1") 처럼 직접 생성을해도됩니다.
                        ////////////objObj = Activator.CreateInstance(objType);


                        //////////////해당 클래스의 Method를 Invoke()함..
                        //////////////invoke의 첫번째 인자는 생성된 객체.. 두번째 인자는 Parameter의 배열
                        ////////////MessageBox.Show(objMethod.Invoke(objObj, objPara).ToString());

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                    throw (ex);
                }
                finally
                {
                }
            }


            #region 화면별 컨트롤에 대한 다국어 설정
            public void mfSetFormLanguage(Form frm)
            {
                try
                {
                    XDocument xDoc = new XDocument();
                    xDoc = XDocument.Load(Application.StartupPath + "\\MultiLang.xml");

                    XElement xel = (from myConfig in xDoc.Elements("Root").Elements("Lang").Elements("Program")
                                    where (string)myConfig.Attribute("ProgramID").Value == frm.Name
                                    select myConfig).First();

                    if (xel != null)
                    {
                        foreach (Control ctl in frm.Controls)
                        {
                            mfSetFormLanguage(ctl, xel);
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            public void mfSetFormLanguage(Form frm, Control ctls)
            {
                try
                {
                    XDocument xDoc = new XDocument();
                    xDoc = XDocument.Load(Application.StartupPath + "\\MultiLang.xml");

                    XElement xel = (from myConfig in xDoc.Elements("Root").Elements("Lang").Elements("Program")
                                    where (string)myConfig.Attribute("ProgramID").Value == frm.Name
                                    select myConfig).First();

                    if (xel != null)
                    {
                        mfSetFormLanguage(ctls, xel);
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            private void mfSetFormLanguage(Control ctls, XElement xNode)
            {
                try
                {
                    QRPGlobal SysRes = new QRPGlobal();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    XElement xctl;
                    try
                    {
                        xctl = (from el in xNode.Elements("Control")
                                where (string)el.Attribute("ControlName").Value == ctls.Name
                                select el).First();
                    }
                    catch
                    {
                        xctl = null;
                    }

                    if (xctl != null)
                    {
                        if (ctls.GetType().Equals(typeof(Infragistics.Win.UltraWinGrid.UltraGrid)))
                        {
                            foreach (Infragistics.Win.UltraWinGrid.UltraGridColumn col in ((Infragistics.Win.UltraWinGrid.UltraGrid)ctls).DisplayLayout.Bands[0].Columns)
                            {
                                col.Header.Caption = (from name in xctl.Elements("ControlKey")
                                                      where name.Attribute("ControlKey").Value == col.Key
                                                      select name.Element("Lang").Attribute(m_resSys.GetString("SYS_LANG")).Value).First();


                            }
                        }
                        else if (ctls.GetType().Equals(typeof(Infragistics.Win.UltraWinTabControl.UltraTabControl)))
                        {
                            foreach (Infragistics.Win.UltraWinTabControl.UltraTab uTab in ((Infragistics.Win.UltraWinTabControl.UltraTabControl)ctls).Tabs)
                            {
                                uTab.Text = (from name in xctl.Elements("ControlKey")
                                             where name.Attribute("ControlKey").Value == uTab.Key
                                             select name.Element("Lang").Attribute(m_resSys.GetString("SYS_LANG")).Value).First();
                            }
                        }
                        else if (ctls.GetType().Equals(typeof(Infragistics.Win.UltraWinEditors.UltraOptionSet)))
                        {
                            foreach (Infragistics.Win.ValueListItem uOp in ((Infragistics.Win.UltraWinEditors.UltraOptionSet)ctls).Items)
                            {
                                uOp.DisplayText = (from name in xctl.Elements("ControlKey")
                                                   where name.Attribute("ControlKey").Value.ToString() == uOp.DataValue.ToString()
                                                   select name.Element("Lang").Attribute(m_resSys.GetString("SYS_LANG")).Value.ToString()).First();
                            }
                        }
                        // 중문 추가 12.09.10
                        else if (ctls.GetType().Equals(typeof(Infragistics.Win.UltraWinGrid.UltraCombo)))
                        {
                            foreach (Infragistics.Win.UltraWinGrid.UltraGridColumn col in ((Infragistics.Win.UltraWinGrid.UltraCombo)ctls).DisplayLayout.Bands[0].Columns)
                            {
                                col.Header.Caption = (from name in xctl.Elements("ControlKey")
                                                      where name.Attribute("ControlKey").Value == col.Key
                                                      select name.Element("Lang").Attribute(m_resSys.GetString("SYS_LANG")).Value).First();
                            }
                        }

                        else
                        {
                            if (ctls.GetType().Equals(typeof(QRPUserControl.TitleArea)))
                            {
                                try
                                {
                                    ((QRPUserControl.TitleArea)ctls).mfSetLabelText((from name in xctl.Elements("ControlKey").Elements("Lang")
                                                                                     select name.Attribute(m_resSys.GetString("SYS_LANG")).Value.ToString()).First().ToString(), m_resSys.GetString("SYS_FONTNAME"), 9);
                                }
                                catch { }
                            }                            
                            else
                            {
                                try
                                {
                                    ctls.Text = (from name in xctl.Elements("ControlKey").Elements("Lang")
                                                 select name.Attribute(m_resSys.GetString("SYS_LANG")).Value.ToString()).First();
                                }
                                catch { }
                            }
                        }
                    }

                    if (ctls.HasChildren)
                    {
                        foreach (Control _ctl in ctls.Controls)
                        {
                            mfSetFormLanguage(_ctl, xNode);
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }            
            #endregion


            /// <summary>
            /// .NET Remoting을 위한 채널 및 Proxy 생성
            /// </summary>
            /// <param name="TypeName"></param>
            /// <param name="strClassName"></param>
            public void mfRegisterChannel(Type TypeName, string strClassName)
            {
                string RemoteServer = "";
                try
                {
                    WellKnownClientTypeEntry[] entry = RemotingConfiguration.GetRegisteredWellKnownClientTypes();
                    foreach (WellKnownClientTypeEntry E in entry)
                    {
                        if (E.TypeName.ToString() == TypeName.FullName.ToString())
                            return;
                    }

                    //채널 생성
                    bool bolMakeChannel = false;
                    foreach (IChannel chn in ChannelServices.RegisteredChannels)
                    {
                        if (chn.ChannelName == "http")
                        {
                            bolMakeChannel = true;
                            break;
                        }
                    }
                    if (bolMakeChannel == false)
                    {
                        HttpChannel channel = new HttpChannel();
                        ChannelServices.RegisterChannel(channel, true);
                    }

                    //RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString();
                    //윗줄 주석처리 후 재 정의 12.09.11
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    RemoteServer = config.AppSettings.Settings["RemoteServer"].Value.ToString();


                    //RemotingConfiguration.CustomErrorsMode = CustomErrorsModes.Off; //신규추가
                    RemotingConfiguration.RegisterWellKnownClientType(TypeName, RemoteServer + strClassName + ".rem");
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            /// .NET Remoting 인증 설정
            /// </summary>
            /// <param name="obj"></param>
            public void mfCredentials(object obj)
            {
                try
                {
                    //char[] sep = { ';' };
                    //string[] cre = ConfigurationManager.AppSettings["Credentials"].ToString().Split(sep);
                    IDictionary Props = ChannelServices.GetChannelSinkProperties(obj);
                    //Props["credentials"] = new NetworkCredential(cre[0], cre[1]);
                    Props["credentials"] = CredentialCache.DefaultCredentials;
                    Props["preauthenticate"] = true;
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            #region IIS연결상태 체크 및 서버정보 변경
            /// <summary>
            /// QRP IIS서버 상태 체크
            /// </summary>
            /// <returns></returns>
            public bool mfCheckQRPServer()
            {
                string RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString() + "menu.rem?wsdl";
                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(RemoteServer);
                httpReq.AllowAutoRedirect = false;

                try
                {
                    HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse();
                    //MessageBox.Show(httpRes.StatusCode.ToString());
                    if (httpRes.StatusCode == HttpStatusCode.OK)
                    {
                        httpRes.Close();
                        return true;
                    }
                    else
                    {
                        httpRes.Close();
                        bool bolCheck = ChangeQRPServer();
                        if (bolCheck == false)
                        {
                            QRPGlobal SysRes = new QRPGlobal();
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                            msg.mfSetMessageBox(QRPCOM.QRPUI.MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "시스템정보", "시스템정보", "메뉴에 대한 화면이 없습니다. 전산실 또는 ERP추진팀으로 문의해 주시기 바랍니다.", Infragistics.Win.HAlign.Right);
                        }
                        return bolCheck;
                    }

                }
                catch (System.Exception ex)
                {
                    return false;
                }
                finally
                {

                }
            }

            /// <summary>
            /// 주서버 다운으로 주서버주소와 부서버주소 변경
            /// </summary>
            /// <returns></returns>
            private bool ChangeQRPServer()
            {
                bool bolConnectServer = false;
                try
                {
                    string strServerKey = "RemoteServer";
                    string strRepServerKey = "RemoteRepServer";
                    string strServerValue = "";
                    string strRepServerValue = "";
                    //app.config에 서버경로, 언어, 폰트를 저장한다.
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    //QRP 주 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    if (config.AppSettings.Settings.AllKeys.Contains(strServerKey))
                        strServerValue = config.AppSettings.Settings[strServerKey].Value;

                    //QRP 서브 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    if (config.AppSettings.Settings.AllKeys.Contains(strRepServerKey))
                        strRepServerValue = config.AppSettings.Settings[strRepServerKey].Value;

                    if (strServerValue != "" && strRepServerValue != "")
                    {
                        config.AppSettings.Settings[strServerKey].Value = strRepServerValue;
                        config.AppSettings.Settings[strRepServerKey].Value = strServerValue;
                        config.Save();
                    }


                    //QRP 서버주소를 변경한 경우 QRPUpdater.exe.config에도 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    //QRPUpdater.exe 파일이 있는경우에만 수행
                    string[] strupdate = Directory.GetFiles(Application.StartupPath, "QRPUpdater.exe");

                    if (strupdate.Length > 0)
                    {
                        System.Configuration.Configuration updateconfig = ConfigurationManager.OpenExeConfiguration("QRPUpdater.exe");
                        //QRP 주 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                        if (updateconfig.AppSettings.Settings.AllKeys.Contains(strServerKey))
                            updateconfig.AppSettings.Settings[strServerKey].Value = strRepServerValue;
                        else
                            updateconfig.AppSettings.Settings.Add(strServerKey, strRepServerValue);

                        //QRP 서브 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                        if (updateconfig.AppSettings.Settings.AllKeys.Contains(strRepServerKey))
                            updateconfig.AppSettings.Settings[strRepServerKey].Value = strServerValue;
                        else
                            updateconfig.AppSettings.Settings.Add(strRepServerKey, strServerValue);

                        updateconfig.Save();
                    }

                    //바뀐 대체 서버로 연결체크
                    for (int i = 0; i < 3; i++)
                    {
                        string RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString() + "menu.rem?wsdl";
                        HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(RemoteServer);
                        httpReq.AllowAutoRedirect = false;
                        HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse();
                        //MessageBox.Show(httpRes.StatusCode.ToString());
                        if (httpRes.StatusCode == HttpStatusCode.OK)
                        {
                            bolConnectServer = true;
                            break;
                        }
                    }
                    return bolConnectServer;

                }
                catch (System.Exception ex)
                {
                    return bolConnectServer;
                }
                finally
                {
                }
            }

            #endregion

            /// <summary>
            /// Form에 대한 툴바버튼 활성여부 처리
            /// </summary>
            /// <param name="mdiForm"></param>
            /// <param name="bolSearch"></param>
            /// <param name="bolSave"></param>
            /// <param name="bolDelete"></param>
            /// <param name="bolCreate"></param>
            /// <param name="bolPrint"></param>
            /// <param name="bolExcel"></param>
            public void mfActiveToolBar(Form mdiForm,
                                        bool bolSearch, bool bolSave, bool bolDelete, bool bolCreate, bool bolPrint, bool bolExcel)
            {
                try
                {
                    Infragistics.Win.Misc.UltraPanel panel = null;
                    foreach (Control obj in mdiForm.Controls)
                    {
                        if (obj.Name == "uPanelToolBar")
                        {
                            panel = (Infragistics.Win.Misc.UltraPanel)obj;
                        }
                    }
                    //검색버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxSearchOn");
                    if (m_objToolButton != null)
                    {
                        if (bolSearch == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxSearchOff");
                    if (m_objToolButton != null)
                    {
                        if (bolSearch == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    //저장버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxSaveOn");
                    if (m_objToolButton != null)
                    {
                        if (bolSave == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxSaveOff");
                    if (m_objToolButton != null)
                    {
                        if (bolSave == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    //삭제버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxDelOn");
                    if (m_objToolButton != null)
                    {
                        if (bolDelete == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxDelOff");
                    if (m_objToolButton != null)
                    {
                        if (bolDelete == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    //생성버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxInsertOn");
                    if (m_objToolButton != null)
                    {
                        if (bolCreate == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxInsertOff");
                    if (m_objToolButton != null)
                    {
                        if (bolCreate == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    //출력버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxPrintOn");
                    if (m_objToolButton != null)
                    {
                        if (bolPrint == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxPrintOff");
                    if (m_objToolButton != null)
                    {
                        if (bolPrint == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    ////엑셀버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxExcelOn");
                    if (m_objToolButton != null)
                    {
                        if (bolExcel == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxExcelOff");
                    if (m_objToolButton != null)
                    {
                        if (bolExcel == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }


            /// <summary>
            /// Form에 대한 툴바버튼 활성여부 처리
            /// </summary>
            /// <param name="mdiForm"></param>
            /// <param name="bolSearch"></param>
            /// <param name="bolSave"></param>
            /// <param name="bolDelete"></param>
            /// <param name="bolCreate"></param>
            /// <param name="bolPrint"></param>
            /// <param name="bolExcel"></param>
            public void mfActiveToolBar(Form mdiForm,
                                        bool bolSearch, bool bolSave, bool bolDelete, bool bolCreate, bool bolPrint, bool bolExcel, string strUserID, string strProgramID)
            {
                try
                {
                    DataTable dtAuth = mfSetToolbarProgramAuth(strUserID, strProgramID);

                    //권한정보가 없는 경우
                    if (dtAuth.Rows.Count <= 0)
                    {
                        mfActiveToolBar(mdiForm, bolSearch, bolSave, bolDelete, bolCreate, bolPrint, bolExcel);
                        return;
                    }

                    //권한정보가 있는 경우
                    Infragistics.Win.Misc.UltraPanel panel = null;
                    foreach (Control obj in mdiForm.Controls)
                    {
                        if (obj.Name == "uPanelToolBar")
                        {
                            panel = (Infragistics.Win.Misc.UltraPanel)obj;
                        }
                    }
                    //검색버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxSearchOn");
                    if (m_objToolButton != null)
                    {
                        if (bolSearch == true && dtAuth.Rows[0]["SearchFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxSearchOff");
                    if (m_objToolButton != null)
                    {
                        if (bolSearch == true && dtAuth.Rows[0]["SearchFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    //저장버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxSaveOn");
                    if (m_objToolButton != null)
                    {
                        if (bolSave == true && dtAuth.Rows[0]["SaveFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxSaveOff");
                    if (m_objToolButton != null)
                    {
                        if (bolSave == true && dtAuth.Rows[0]["SaveFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    //삭제버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxDelOn");
                    if (m_objToolButton != null)
                    {
                        if (bolDelete == true && dtAuth.Rows[0]["DeleteFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxDelOff");
                    if (m_objToolButton != null)
                    {
                        if (bolDelete == true && dtAuth.Rows[0]["DeleteFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    //생성버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxInsertOn");
                    if (m_objToolButton != null)
                    {
                        if (bolCreate == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxInsertOff");
                    if (m_objToolButton != null)
                    {
                        if (bolCreate == true) ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    //출력버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxPrintOn");
                    if (m_objToolButton != null)
                    {
                        if (bolPrint == true && dtAuth.Rows[0]["PrintFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxPrintOff");
                    if (m_objToolButton != null)
                    {
                        if (bolPrint == true && dtAuth.Rows[0]["PrintFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }

                    ////엑셀버튼 활성여부
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxExcelOn");
                    if (m_objToolButton != null)
                    {
                        if (bolExcel == true && dtAuth.Rows[0]["ExcelFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                    }
                    m_objToolButton = null;
                    FindChildControlInContainer(panel, "uPictureBoxExcelOff");
                    if (m_objToolButton != null)
                    {
                        if (bolExcel == true && dtAuth.Rows[0]["ExcelFlag"].ToString() == "T") ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = false;
                        else ((Infragistics.Win.UltraWinEditors.UltraPictureBox)m_objToolButton).Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            private DataTable mfSetToolbarProgramAuth(string strUserID, string strProgramID)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("SearchFlag", typeof(string));
                dt.Columns.Add("SaveFlag", typeof(string));
                dt.Columns.Add("DeleteFlag", typeof(string));
                dt.Columns.Add("PrintFlag", typeof(string));
                dt.Columns.Add("ExcelFlag", typeof(string));
                try
                {
                    QRPCOM.QRPGLO.QRPGlobal glo = new QRPCOM.QRPGLO.QRPGlobal();
                    ResourceSet rs = new ResourceSet(glo.ToolInfoRes);
                    string strSearchFlag = rs.GetString(strUserID + "|" + strProgramID + "|" + "SearchFlag");
                    string strSaveFlag = rs.GetString(strUserID + "|" + strProgramID + "|" + "SaveFlag");
                    string strDeleteFlag = rs.GetString(strUserID + "|" + strProgramID + "|" + "DeleteFlag");
                    string strPrintFlag = rs.GetString(strUserID + "|" + strProgramID + "|" + "PrintFlag");
                    string strExcelFlag = rs.GetString(strUserID + "|" + strProgramID + "|" + "ExcelFlag");

                    DataRow dr;
                    dr = dt.NewRow();
                    dr["SearchFlag"] = strSearchFlag;
                    dr["SaveFlag"] = strSaveFlag;
                    dr["DeleteFlag"] = strDeleteFlag;
                    dr["PrintFlag"] = strPrintFlag;
                    dr["ExcelFlag"] = strExcelFlag;
                    dt.Rows.Add(dr);

                    rs.Close();
                    return dt;
                }
                catch (System.Exception ex)
                {
                    return dt;
                }
                finally
                {
                }
            }

            /// <summary>
            /// 지정폴더에 하위폴더가 있는지 체크
            /// </summary>
            /// <param name="diRoot"></param>
            /// <param name="strSubFolderName"></param>
            /// <returns></returns>
            private bool FindFolder(DirectoryInfo diRoot, string strSubFolderName)
            {
                try
                {
                    DirectoryInfo[] arrDiRoot = diRoot.GetDirectories();

                    foreach (DirectoryInfo D in arrDiRoot)
                    {
                        if (D.Name == strSubFolderName.Substring(1).ToString())
                        {
                            return true;
                        }
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            /// 지정폴더에 화일이 있는지 체크
            /// </summary>
            /// <param name="diFolder"></param>
            /// <param name="strFileName"></param>
            /// <returns></returns>
            private bool FindFile(DirectoryInfo diFolder, string strFileName)
            {
                try
                {
                    FileInfo[] FileList = diFolder.GetFiles();
                    Boolean bolFindFile = false;

                    foreach (FileInfo File in FileList)
                    {
                        if (File.Name == strFileName)
                        {
                            bolFindFile = true;
                            break;
                        }
                    }
                    return bolFindFile;
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            /// 컨테이너 Control 안에서 특정 Control를 찾는 함수
            /// </summary>
            /// <param name="objContainer"></param>
            /// <param name="strFindControlName"></param>
            private void FindChildControlInContainer(object objContainer, string strFindControlName)
            {
                try
                {
                    //인자로 받은 컨트롤의 모든 자식 컨트롤들에 대해서 루프를 수행 
                    foreach (object tmpControl in ((System.Windows.Forms.Control)objContainer).Controls)
                    {
                        if (((System.Windows.Forms.Control)tmpControl).HasChildren)
                        {
                            //재귀호출
                            this.FindChildControlInContainer(tmpControl, strFindControlName);
                        }
                        else
                        {
                            if ((tmpControl as System.Windows.Forms.Control).Name == strFindControlName)
                            {
                                m_objToolButton = tmpControl as System.Windows.Forms.Control;
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }
        }
        #endregion

        #region 진행 팝업창
        public class QRPProgressBar
        {
            private frmProgress frmProgressPopup; // = new frmProgress();   //팝업창 Instance
            private System.Threading.Thread threadPopup = null;         //팝업창 처리 Thread
            private Form m_frmForm;                                     //팝업창 호출 Form
            private string m_strFunctionName;                           //팝업창 처리기능명

            public QRPProgressBar()
            {
                frmProgressPopup = new frmProgress();
            }

            /// <summary>
            /// 팝업창을 처리하기위한 Thread 생성
            /// </summary>
            /// <returns></returns>
            public Thread mfStartThread()
            {
                threadPopup = new System.Threading.Thread((OpenPopup)); //Thread생성
                //MessageBox.Show("OpenPopup 끝" + DateTime.Now.ToString());
                //threadPopup = new System.Threading.Thread(() => { using (var frm = new frmProgress()) frm.ShowDialog(); });
                //threadPopup.SetApartmentState(ApartmentState.STA);
                return threadPopup;
            }

            private delegate void Popup(Form frmForm, string strFunctionName);

            /// <summary>
            /// 진행 팝업창 열기
            /// </summary>
            /// <param name="frmForm"></param>
            /// <param name="strFunctioName"></param>
            public void mfOpenProgressPopup(Form frmForm, string strFunctioName)
            {
                try
                {
                    QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    string m_strProgressName_Chg = string.Empty;
                    if (m_resSys.GetString("SYS_LANG").Equals("CHN"))
                    {
                        m_strProgressName_Chg = "처리중...";
                    }
                    else
                    {
                        m_strProgressName_Chg = strFunctioName;
                    }                    

                    if (frmForm.InvokeRequired)
                    {
                        Popup pop = new Popup(mfOpenProgressPopup);
                        frmForm.Invoke(pop, new object[] { frmForm, m_strProgressName_Chg });
                    }
                    else
                    {
                        // 원래 코드
                        m_frmForm = frmForm;
                        m_strFunctionName = m_strProgressName_Chg;
                        m_frmForm.Cursor = Cursors.WaitCursor;
                        threadPopup.Start();
                    }
                    //MessageBox.Show("Thread시작" + DateTime.Now.ToString());
                }
                catch //(Exception ex)
                {
                    //throw (ex);
                }
                finally
                {
                }
            }

            public void mfStartThread(Form frmForm, string strFunctioName)
            {
                try
                {
                    threadPopup = new System.Threading.Thread((OpenPopup)); //Thread생성
                    m_frmForm = frmForm;
                    m_strFunctionName = strFunctioName;
                    m_frmForm.Cursor = Cursors.WaitCursor;
                    threadPopup.Start();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 진행 팝업창 열기
            /// </summary>
            private void OpenPopup()
            {
                //////////MessageBox.Show("OpenPopup 시작" + DateTime.Now.ToString());

                ////////frmProgressPopup.TopMost = true;
                ////////frmProgressPopup.ProgressName = m_strFunctionName;
                ////////frmProgressPopup.StartPosition = FormStartPosition.CenterScreen;
                //////////frmProgressPopup.MdiParent = m_frmForm; // m_mdiform;
                //////////frmProgressPopup.Left = Convert.ToInt32((m_frmForm.Width - frmProgressPopup.Width) / 2);
                //////////frmProgressPopup.Top = Convert.ToInt32((m_frmForm.Height - frmProgressPopup.Height) / 2);
                //////////frmProgressPopup.Show();

                ////////frmProgressPopup.ShowDialog();
                ////////frmProgressPopup.Refresh();

                //////////MessageBox.Show("OpenPopup 폼띄우기" + DateTime.Now.ToString());

                frmProgress frm = new frmProgress();
                frmProgressPopup = frm;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ProgressName = m_strFunctionName;
                frm.ShowDialog();
            }

            private delegate void EndPopup(Form MdiForm);
            /// <summary>
            /// Thread 종료 및 진행 팝업창 닫기
            /// </summary>
            public void mfCloseProgressPopup(Form MdiForm)
            {
                #region 2차 수정 코드
                if (frmProgressPopup != null)
                {
                    MdiForm.Cursor = Cursors.Default;
                    frmProgressPopup.CloseThis();
                }
                #endregion

                #region 1차 수정 코드 - 실패

                //if (MdiForm.InvokeRequired)
                //{
                //    try
                //    {
                //        EndPopup pop = new EndPopup(mfCloseProgressPopup);
                //        MdiForm.Invoke(pop, new object[] { MdiForm });
                //    }
                //    catch 
                //    {

                //    }
                //}
                //else
                //{
                //    try
                //    {
                //        if (MdiForm != null)
                //            MdiForm.Cursor = Cursors.Default;

                //        threadPopup.Abort();
                //    }
                //    catch //(ThreadAbortException ex)
                //    {
                //    }
                //}
                #endregion

                #region 원래 코드

                //////////try
                //////////{
                //////////    //while (frmProgressPopup.strFormState.Equals("F"))
                //////////    //{
                //////////    //    if (frmProgressPopup.strFormState.Equals("T"))
                //////////    //        break;
                //////////    //}

                //////////    if (m_frmForm != null)
                //////////    {
                //////////        m_frmForm.Cursor = Cursors.Default;
                //////////        //threadPopup.Abort();
                //////////    }
                //////////    //if (threadPopup.ThreadState == ThreadState.Running)
                //////////    //{
                //////////    //Thread.Sleep(1000);
                //////////    //frmProgressPopup.Dispose();
                //////////    threadPopup.Abort();
                //////////    //}
                //////////    //threadPopup.Interrupt();
                //////////    //frmProgressPopup.Close();

                //////////}
                //////////catch //(Exception ex)
                //////////{
                //////////    //throw (ex);
                //////////}
                //////////finally
                //////////{
                //////////}
                #endregion

            }
        }
        #endregion

        #region 트랜잭션 처리시 Return 처리
        public class TransErrRtn
        {
            private int intErrNum;              //에러번호
            private string strErrMessage;       //에러메세지    
            private ArrayList arrReturnValue = new ArrayList();   //반환할 결과값
            private string strSystemMessage;
            private string strSystemStackTrace;
            private string strSystemInnerException;
            private string strInterfaceResultCode;
            private string strInterfaceResultMessage;

            public int ErrNum
            {
                get { return intErrNum; }
                set { intErrNum = value; }
            }

            public string ErrMessage
            {
                get { return strErrMessage; }
                set { strErrMessage = value; }
            }

            public string SystemMessage
            {
                get { return strSystemMessage; }
                set { strSystemMessage = value; }
            }

            public string SystemStackTrace
            {
                get { return strSystemStackTrace; }
                set { strSystemStackTrace = value; }
            }

            public string SystemInnerException
            {
                get { return strSystemInnerException; }
                set { strSystemInnerException = value; }
            }

            public string InterfaceResultCode
            {
                get { return strInterfaceResultCode; }
                set { strInterfaceResultCode = value; }
            }

            public string InterfaceResultMessage
            {
                get { return strInterfaceResultMessage; }
                set { strInterfaceResultMessage = value; }
            }

            public TransErrRtn()
            {
                intErrNum = 0;
                strErrMessage = "";
                arrReturnValue.Clear();
                strSystemMessage = "";
                strSystemStackTrace = "";
                strSystemInnerException = "";
                strInterfaceResultCode = "";
                strInterfaceResultMessage = "";
            }

            /// <summary>
            /// 리턴값 배열 초기화
            /// </summary>
            public void mfInitReturnValue()
            {
                arrReturnValue.Clear();
            }

            /// <summary>
            /// 리턴값 배열에 값을 추가
            /// </summary>
            /// <param name="strValue"></param>
            public void mfAddReturnValue(string strValue)
            {
                arrReturnValue.Add(strValue);
            }

            /// <summary>
            /// 리턴갑 배열에 값을 삭제
            /// </summary>
            /// <param name="intIndex"></param>
            public void mfDeleteReturnValue(int intIndex)
            {
                arrReturnValue.Remove(intIndex);
            }

            /// <summary>
            /// 리턴값 배열 얻기
            /// </summary>
            /// <returns></returns>
            public ArrayList mfGetReturnValue()
            {
                return arrReturnValue;
            }

            /// <summary>
            /// 리턴갑 배열중 특정값 얻기
            /// </summary>
            /// <param name="intIndex"></param>
            /// <returns></returns>
            public string mfGetReturnValue(int intIndex)
            {
                return arrReturnValue[intIndex].ToString();
            }

            /// <summary>
            /// 에러메세지 구조체 정보를 문자열로 변환
            /// </summary>
            /// <param name="Err">트랜잭션처리정보 구조체</param>
            /// <returns>Encoding값</returns>
            public string mfEncodingErrMessage(TransErrRtn Err)
            {
                string strErr = "";
                string strErrSep = "<Err>";
                string strOutSep = "<OUT>";
                try
                {
                    strErr = Err.intErrNum.ToString() + strErrSep +
                         Err.strErrMessage + strErrSep +
                         Err.strSystemMessage + strErrSep +
                         Err.strSystemStackTrace + strErrSep +
                         Err.strSystemInnerException + strErrSep +
                         Err.strInterfaceResultCode + strErrSep +       //추가
                         Err.strInterfaceResultMessage + strErrSep;     //추가

                    if (Err.arrReturnValue.Count > 0)
                    {
                        //strErr = strErr + "OUTPUT";
                        for (int i = 0; i < Err.arrReturnValue.Count; i++)
                        {
                            strErr = strErr + Err.arrReturnValue[i].ToString() + strOutSep;
                        }
                    }
                    return strErr;

                    ////strErr = Err.intErrNum.ToString() + strErrSep +
                    ////         Err.strErrMessage + strErrSep +
                    ////         Err.strSystemMessage + strErrSep +
                    ////         Err.strSystemStackTrace + strErrSep +
                    ////         Err.strSystemInnerException + strErrSep;

                    ////if (Err.arrReturnValue.Count > 0)
                    ////{
                    ////    strErr = strErr + "OUTPUT";
                    ////    for (int i = 0; i < Err.arrReturnValue.Count; i++)
                    ////    {
                    ////        strErr = strErr + Err.arrReturnValue[i].ToString() + strOutSep;
                    ////    }
                    ////}
                    ////return strErr;

                }
                catch (Exception ex)
                {
                    return strErr;
                }
                finally
                {
                }
            }

            /// <summary>
            /// 에러메시지 문자를 구조체로 변환
            /// </summary>
            /// <param name="strErr">트랜잭션처리정보 문자열</param>
            /// <returns>Decoding값</returns>
            public TransErrRtn mfDecodingErrMessage(string strErr)
            {
                TransErrRtn errMsg = new TransErrRtn();
                try
                {
                    string[] arrErrSep = { "<Err>" };
                    string[] arrOutSep = { "<OUT>" };

                    string[] arrErrMsg = strErr.Split(arrErrSep, StringSplitOptions.None);

                    errMsg.intErrNum = Convert.ToInt32(arrErrMsg[0]);
                    errMsg.strErrMessage = arrErrMsg[1];
                    errMsg.strSystemMessage = arrErrMsg[2];
                    errMsg.strSystemStackTrace = arrErrMsg[3];
                    errMsg.strSystemInnerException = arrErrMsg[4];
                    errMsg.strInterfaceResultCode = arrErrMsg[5];       //추가
                    errMsg.strInterfaceResultMessage = arrErrMsg[6];    //추가

                    string[] arrOutput = arrErrMsg[7].Split(arrOutSep, StringSplitOptions.None);
                    for (int i = 0; i < arrOutput.Length; i++)
                        errMsg.mfAddReturnValue(arrOutput[i]);

                    return errMsg;

                    ////string[] arrErrSep = { "<Err>" };
                    ////string[] arrOutSep = { "<OUT>" };

                    ////string[] arrErrMsg = strErr.Split(arrErrSep, StringSplitOptions.None);

                    ////errMsg.intErrNum = Convert.ToInt32(arrErrMsg[0]);
                    ////errMsg.strErrMessage = arrErrMsg[1];
                    ////errMsg.strSystemMessage = arrErrMsg[2];
                    ////errMsg.strSystemStackTrace = arrErrMsg[3];
                    ////errMsg.strSystemInnerException = arrErrMsg[4];

                    ////string[] arrOutput = arrErrMsg[5].Split(arrOutSep, StringSplitOptions.None);


                    ////return errMsg;
                }
                catch (Exception ex)
                {
                    return errMsg;
                }
                finally
                {
                }
            }
        }
        #endregion
    }
}
