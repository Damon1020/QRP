﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QRPCOM
{
    public partial class frmProgress : Form
    {

        private string m_strProgressName;
        private DateTime m_datStartDate;
        public string strFormState { get; set; }
        public string ProgressName
        {
            set { m_strProgressName = value; }
        }

        public frmProgress()
        {
            InitializeComponent();
        }

        private void frmProgress_Load(object sender, EventArgs e)
        {
            try
            {
                this.BackgroundImage = Properties.Resources.progress;

                uLabelTimeTitle.Visible = false;

                strFormState = "F";

                uLabelFunction.Text = m_strProgressName;
                uLabelFunction.AutoSize = true;

                uActivityIndicator.AnimationEnabled = true;
                uActivityIndicator.MarqueeAnimationStyle = Infragistics.Win.UltraActivityIndicator.MarqueeAnimationStyle.BounceBack;
                uActivityIndicator.AnimationSpeed = 30;
                uActivityIndicator.MarqueeMarkerWidth = 100;
                uActivityIndicator.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
                uActivityIndicator.ViewStyle = Infragistics.Win.UltraActivityIndicator.ActivityIndicatorViewStyle.Aero;

                m_datStartDate = DateTime.Now;
                //////timerProgress.Interval = 500; //1000;
                //////timerProgress.Start();
                
                this.Cursor = Cursors.WaitCursor;
                this.uActivityIndicator.Select();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void frmProgress_FormClosing(object sender,
            FormClosingEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Default;
                //timerProgress.Stop();            
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void timerProgress_Tick(object sender, EventArgs e)
        {
            try
            {
                //DateTime datCurDate = DateTime.Now;
                //TimeSpan timSpendTime = DateTime.Now.Subtract(m_datStartDate);

                //string strSpendDate = "";
                //if (timSpendTime.Hours > 0)
                //    strSpendDate = timSpendTime.Hours.ToString() + "시";
                //if (timSpendTime.Minutes > 0)
                //    strSpendDate = strSpendDate + timSpendTime.Minutes.ToString() + "분";
                //if (timSpendTime.Seconds > 0)
                //    strSpendDate = strSpendDate + timSpendTime.Seconds.ToString() + "초";

                //uLabelTime.Text = strSpendDate;
                strFormState = "T";
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }            
        }

        // 2차 수정 코드용 formclose method
        public void CloseThis()
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.Close();
            });
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.Default;
                this.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

    }
}
