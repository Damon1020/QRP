﻿namespace QRPCOM
{
    partial class frmErrorInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uTextErrorMethod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelErrorMethod = new Infragistics.Win.Misc.UltraLabel();
            this.uTextErrorMessage = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMessage = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uTextErrorMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextErrorMessage)).BeginInit();
            this.SuspendLayout();
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(272, 276);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 14;
            this.uButtonClose.Text = "확인";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // uTextErrorMethod
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextErrorMethod.Appearance = appearance2;
            this.uTextErrorMethod.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextErrorMethod.Location = new System.Drawing.Point(111, 172);
            this.uTextErrorMethod.Multiline = true;
            this.uTextErrorMethod.Name = "uTextErrorMethod";
            this.uTextErrorMethod.ReadOnly = true;
            this.uTextErrorMethod.Size = new System.Drawing.Size(247, 100);
            this.uTextErrorMethod.TabIndex = 18;
            // 
            // uLabelErrorMethod
            // 
            this.uLabelErrorMethod.Location = new System.Drawing.Point(6, 172);
            this.uLabelErrorMethod.Name = "uLabelErrorMethod";
            this.uLabelErrorMethod.Size = new System.Drawing.Size(101, 20);
            this.uLabelErrorMethod.TabIndex = 17;
            this.uLabelErrorMethod.Text = "Error Method";
            // 
            // uTextErrorMessage
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextErrorMessage.Appearance = appearance19;
            this.uTextErrorMessage.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextErrorMessage.Location = new System.Drawing.Point(111, 8);
            this.uTextErrorMessage.Multiline = true;
            this.uTextErrorMessage.Name = "uTextErrorMessage";
            this.uTextErrorMessage.ReadOnly = true;
            this.uTextErrorMessage.Size = new System.Drawing.Size(247, 160);
            this.uTextErrorMessage.TabIndex = 20;
            // 
            // uLabelMessage
            // 
            this.uLabelMessage.Location = new System.Drawing.Point(6, 8);
            this.uLabelMessage.Name = "uLabelMessage";
            this.uLabelMessage.Size = new System.Drawing.Size(101, 20);
            this.uLabelMessage.TabIndex = 19;
            this.uLabelMessage.Text = "Error Message";
            // 
            // frmErrorInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(363, 308);
            this.Controls.Add(this.uTextErrorMessage);
            this.Controls.Add(this.uLabelMessage);
            this.Controls.Add(this.uTextErrorMethod);
            this.Controls.Add(this.uLabelErrorMethod);
            this.Controls.Add(this.uButtonClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmErrorInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QRP Error Info";
            this.Load += new System.EventHandler(this.frmErrorInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uTextErrorMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextErrorMessage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextErrorMethod;
        private Infragistics.Win.Misc.UltraLabel uLabelErrorMethod;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextErrorMessage;
        private Infragistics.Win.Misc.UltraLabel uLabelMessage;
    }
}