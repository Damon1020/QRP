﻿/*----------------------------------------------------------------------*/
/* 시스템명     : QRP                                                   */
/* 모듈(분류)명 : QRP UI Library                                        */
/* 프로그램ID   : clsQRPGLO.cs                                          */
/* 프로그램명   : QRP 첨부화일 Upload/Download 관련                     */
/* 작성자       : 류근철                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-05 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;
using System.IO;
using System.Net;

namespace QRPCOM
{
    public partial class frmWebClientFile : Form
    {
        private string m_strUploadPage = "upload.aspx";
        private string m_strWebURL;             //화일을 처기하기 위한 Web URI
        private string m_strServerPath;         //화일을 저장하는 물리적인 Root경로
        private string m_strFolder;             //화일이 위치하는 Sub 폴더
        
        private string m_strFileAction;         //화일을 처리하는 기능 ("" : 저장, "DEL": 삭제, "CPY" : 복사)
        private ArrayList m_arrSourceFileName = new ArrayList();     //복사 또는 삭제하는 경우 원본이 있는 경로 및 화일명
        private ArrayList m_arrTargetFileName = new ArrayList();     //복사 또는 삭제하는 경우 원본이 있는 경로 및 화일명

        private string m_strUserID;             //화일서버 폴더를 접근할 수 있는 사용자ID
        private string m_strPassword;           //화일서버 폴더를 접근할 수 있는 사용자암호
        private string m_strDownloadClientPath; //화일을 Download받을 Client 경로
        private bool m_bolUseProgView = true;   //Upload 및 Download하면서 진행상태화면을 보여줄지 여부

        private bool m_bolDownloadComplete = false; //Download완료 여부

        private ArrayList m_arrFileName = new ArrayList();
        private ArrayList m_arrFileSize = new ArrayList();

        private int m_intUploadIndex;         //현재 Upload화일 Index
        private long m_lngFileTotalSize;      //Upload하려는 화일 총용량
        //private long m_lngFileUploadSize;     //현재 Upload한 화일의 총용량
        //private long m_lngFileUploadCurSize;  //현재 Upload화일의 용량
        //private long m_lngFileUploadPreSize;  //이전에 Upload한 화일 용량
        private long m_lngFileUploadCount;    //현재 Upload한 화일 갯수

        public bool DownloadComplete
        {
            get { return m_bolDownloadComplete; }
            set { m_bolDownloadComplete = value; }
        }

        public frmWebClientFile()
        {
            InitializeComponent();
        }

        
        private void frmWebClientFile_Load(object sender, EventArgs e)
        {
            try
            {
                this.BackgroundImage = Properties.Resources.download;

                //폼이 Load될 때 다운받을 Path가 지정되어 있지 않으면 Upload시킴
                if (m_strDownloadClientPath == "")
                {
                    if (m_strFileAction == "")
                        mfFileUpload();
                    else if (m_strFileAction == "CPY")
                    {
                        mfFileUploadNoProgView();
                        this.DialogResult = DialogResult.OK;
                    }
                }
                else
                    mfFileDownload();

                //this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                this.DialogResult = DialogResult.No;
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일서버 연결을 위한 연결정보 저장
        /// </summary>
        /// <param name="arrFile"></param>
        /// <param name="strDownloadClientPath"></param>
        /// <param name="strWebURL"></param>
        /// <param name="strServerPath"></param>
        /// <param name="strFolder"></param>
        /// <param name="strUserID"></param>
        /// <param name="strPassword"></param>
        public void mfInitSetSystemFileInfo(ArrayList arrFile, string strDownloadClientPath, string strWebURL, string strServerPath, string strFolder, string strUserID, string strPassword)
                                          //string strSystemAccessInfoCode, string strSystemFilePathCode)
        {
            try
            {
                m_arrFileName = arrFile;
                m_strDownloadClientPath = strDownloadClientPath;

                //Upload할 정보 저장 : (시스템연결정보) 및 (첨부화일 폴더정보) DB에서 읽어오기
                m_strWebURL = strWebURL;            // "http://10.60.24.173/QRP_STS_DEV/UploadFile/";
                m_strServerPath = @strServerPath;   //@"d:\QRP_STS_Dll\UploadFile\";
                m_strFolder = strFolder + "\\";           //@"ISODoc\Detail\";
                m_strUserID = strUserID;            // "administrator";
                m_strPassword = strPassword;        // "jinjujin";
                m_intUploadIndex = 0;
                m_bolDownloadComplete = false;

                m_strFileAction = "";
                //m_strCopySourcePath = "";
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }


        /// <summary>
        /// 화일서버 연결을 위한 연결정보 저장
        /// </summary>
        /// <param name="arrFile"></param>
        /// <param name="strDownloadClientPath"></param>
        /// <param name="strWebURL"></param>
        /// <param name="strServerPath"></param>
        /// <param name="strFolder"></param>
        /// <param name="strUserID"></param>
        /// <param name="strPassword"></param>
        public void mfInitSetSystemFileCopyInfo(ArrayList arrCopyTargetServerFile, string strWebURL, ArrayList arrCopySourceServerFile, string strServerPath, string strFolder, string strUserID, string strPassword)
        //string strSystemAccessInfoCode, string strSystemFilePathCode)
        {
            try
            {
                //m_arrFileName = arrFile;
                //m_strDownloadClientPath = strDownloadClientPath;

                //Upload할 정보 저장 : (시스템연결정보) 및 (첨부화일 폴더정보) DB에서 읽어오기
                m_strWebURL = strWebURL;            // "http://10.60.24.173/QRP_STS_DEV/UploadFile/";
                m_strServerPath = @strServerPath;   //@"d:\QRP_STS_Dll\UploadFile\";
                m_strFolder = strFolder + "\\";           //@"ISODoc\Detail\";
                m_strUserID = strUserID;            // "administrator";
                m_strPassword = strPassword;        // "jinjujin";
                m_intUploadIndex = 0;
                m_bolDownloadComplete = false;

                m_strFileAction = "CPY";
                //복사할 원본이 있는 폴더 + 화일명
                m_arrSourceFileName = arrCopySourceServerFile;
                //복사할 폴더 + 화일명
                m_arrTargetFileName = arrCopyTargetServerFile;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일서버 연결을 위한 연결정보 저장
        /// </summary>
        /// <param name="arrFile"></param>
        /// <param name="strDownloadClientPath"></param>
        /// <param name="strWebURL"></param>
        /// <param name="strServerPath"></param>
        /// <param name="strFolder"></param>
        /// <param name="strUserID"></param>
        /// <param name="strPassword"></param>
        public void mfInitSetSystemFileMoveInfo(ArrayList arrCopyTargetServerFolder, string strWebURL, ArrayList arrCopySourceServerFolder, string strServerPath, string strFolder, string strUserID, string strPassword)
        //string strSystemAccessInfoCode, string strSystemFilePathCode)
        {
            try
            {
                //m_arrFileName = arrFile;
                //m_strDownloadClientPath = strDownloadClientPath;

                //Upload할 정보 저장 : (시스템연결정보) 및 (첨부화일 폴더정보) DB에서 읽어오기
                m_strWebURL = strWebURL;            // "http://10.60.24.173/QRP_STS_DEV/UploadFile/";
                m_strServerPath = @strServerPath;   //@"d:\QRP_STS_Dll\UploadFile\";
                m_strFolder = strFolder + "\\";           //@"ISODoc\Detail\";
                m_strUserID = strUserID;            // "administrator";
                m_strPassword = strPassword;        // "jinjujin";
                m_intUploadIndex = 0;
                m_bolDownloadComplete = false;

                m_strFileAction = "MOVE";
                //복사할 원본이 있는 폴더 + 화일명
                m_arrSourceFileName = arrCopySourceServerFolder;
                //복사할 폴더 + 화일명
                m_arrTargetFileName = arrCopyTargetServerFolder;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일서버 연결을 위한 연결정보 저장
        /// </summary>
        /// <param name="arrFile"></param>
        /// <param name="strDownloadClientPath"></param>
        /// <param name="strWebURL"></param>
        /// <param name="strServerPath"></param>
        /// <param name="strFolder"></param>
        /// <param name="strUserID"></param>
        /// <param name="strPassword"></param>
        public void mfInitSetSystemFileDeleteInfo(string strWebURL, ArrayList arrDeleteSourceServerFile, string strUserID, string strPassword)
        //string strSystemAccessInfoCode, string strSystemFilePathCode)
        {
            try
            {
                //m_arrFileName = arrFile;
                //m_strDownloadClientPath = strDownloadClientPath;

                //Upload할 정보 저장 : (시스템연결정보) 및 (첨부화일 폴더정보) DB에서 읽어오기
                m_strWebURL = strWebURL;            // "http://10.60.24.173/QRP_STS_DEV/UploadFile/";
                //m_strServerPath = @strServerPath;   //@"d:\QRP_STS_Dll\UploadFile\";
                //m_strFolder = strFolder + "\\";           //@"ISODoc\Detail\";
                m_strUserID = strUserID;            // "administrator";
                m_strPassword = strPassword;        // "jinjujin";
                m_intUploadIndex = 0;
                m_bolDownloadComplete = false;

                m_strFileAction = "DEL";
                //삭제할 폴더 + 화일명
                m_arrSourceFileName = arrDeleteSourceServerFile;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일을 WebClient를 통해 Upload함
        /// </summary>
        private void mfFileUpload()
        {
            try
            {
                //m_arrFileName = arrFile;
                //InitSystemInfo(strSystemAccessInfoCode, strSystemFilePathCode);
                uTextTotalFileCount.Text = m_arrFileName.Count.ToString();

                //Upload하려는 화일의 총용량을 구한다.
                for (int i = 0; i < m_arrFileName.Count; i++)
                {
                    FileInfo file = new FileInfo(m_arrFileName[i].ToString());
                    m_lngFileTotalSize = m_lngFileTotalSize + file.Length;
                }

                //Upload화일 총진행율을 보여줄 ProgressBar를 설정함.
                uProgressTotalProg.Minimum = 0;
                uProgressTotalProg.Maximum = 50 * m_arrFileName.Count;

                ////총용량을 컨트롤에 보여준다.
                //if (m_txtTotalSize != null)
                //    m_txtTotalSize.Text = (m_lngFileTotalSize / 1000).ToString("#,###");

                //UploadURL 지정
                Uri uri = new Uri(m_strWebURL + m_strUploadPage);

                //WebClient를 통해 화일을 Upload한다.
                WebClient wclient = new WebClient();

                wclient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                wclient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadFileProgress);
                wclient.UploadFileCompleted += new UploadFileCompletedEventHandler(UploadFileComplete);
                
                wclient.QueryString.Add("path", m_strServerPath);
                wclient.QueryString.Add("folder", m_strFolder);
                wclient.QueryString.Add("action", "");
                wclient.QueryString.Add("copysourcepath", "");
                wclient.QueryString.Add("copytargetpath", "");
                ////wclient.QueryString.Add("action", m_strFileAction);
                ////if (m_arrCopySourceFileName.Count > 0)
                ////    wclient.QueryString.Add("copysourcepath", m_arrCopySourceFileName[m_intUploadIndex].ToString());
                ////else
                ////    wclient.QueryString.Add("copysourcepath", "");

                NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                wclient.Credentials = cred;
                wclient.UploadFileAsync(uri, "POST", m_arrFileName[m_intUploadIndex].ToString());

                //Upload한 화일 갯수를 보여준다.
                m_lngFileUploadCount++;
                uTextFileCount.Text = m_lngFileUploadCount.ToString();

                //Upload중인 화일 총진행율을 보여줄 ProgressBar를 설정함.
                uProgressCurProg.Minimum = 0;
                uProgressCurProg.Maximum = 50; //Convert.ToInt32(m_arrUploadFileSize[m_intUploadIndex]);

                //Upload중인 화일명을 보여준다.
                FileInfo fileName = new FileInfo(m_arrFileName[m_intUploadIndex].ToString());
                uTextFileName.Text = fileName.Name.ToString();
            }
            catch (WebException ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일을 WebClient를 통해 Upload함
        /// </summary>
        public void mfFileUploadNoProgView()
        {
            try
            {
                m_bolUseProgView = false;

                //UploadURL 지정
                Uri uri = new Uri(m_strWebURL + m_strUploadPage);

                //WebClient를 통해 화일을 Upload한다.
                WebClient wclient = new WebClient();
                //if (m_strFileAction == "")
                //{
                    wclient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadFileProgress);
                    wclient.UploadFileCompleted += new UploadFileCompletedEventHandler(UploadFileComplete);
                //}

                wclient.QueryString.Add("path", m_strServerPath);
                wclient.QueryString.Add("folder", m_strFolder);
                ////wclient.QueryString.Add("action", "");
                ////wclient.QueryString.Add("copysourcepath", "");
                wclient.QueryString.Add("action", m_strFileAction);
                if (m_arrSourceFileName.Count > 0)
                    wclient.QueryString.Add("copysourcepath", m_arrSourceFileName[m_intUploadIndex].ToString());
                else
                    wclient.QueryString.Add("copysourcepath", "");

                if (m_arrTargetFileName.Count > 0)
                    wclient.QueryString.Add("copytargetpath", m_arrTargetFileName[m_intUploadIndex].ToString());
                else
                    wclient.QueryString.Add("copytargetpath", "");

                NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                wclient.Credentials = cred;
                if (m_strFileAction == "")
                    wclient.UploadFileAsync(uri, "POST", m_arrFileName[m_intUploadIndex].ToString());
                else
                {
                    string strBrowserPath = Application.ExecutablePath;
                    wclient.UploadFileAsync(uri, "POST", strBrowserPath);
                }
            }
            catch (WebException ex)
            {
            }
            finally
            {
            }
        }
        
        //MSDN : 업로드가 완료되었을 때 ProgressPercentage는 50%가 됩니다. 
        //MSDN : 다운로드가 완료되었을 때 ProgressPercentage는 100%가 됩니다. 
        /// <summary>
        /// 화일을 Upload하는 동안 발생하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void UploadFileProgress(Object sender, UploadProgressChangedEventArgs e)
        {
            try
            {
                if (m_bolUseProgView == true)
                {

                    if (e.ProgressPercentage < 50)
                    {
                        //현재 화일의 진행율을 보여줌 
                        uProgressCurProg.Value = e.ProgressPercentage;
                        //전체 진행율을 보여줌
                        uProgressTotalProg.Value = m_intUploadIndex * 50 + e.ProgressPercentage;
                    }
                    else
                    {
                        uProgressCurProg.Value = 50;
                        uProgressTotalProg.Value = m_intUploadIndex * 50;
                    }
                }
            }
            catch (WebException ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일을 Upload를 완료할때 발생하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void UploadFileComplete(Object sender, UploadFileCompletedEventArgs e)
        {
            System.Threading.AutoResetEvent waiter = (System.Threading.AutoResetEvent)e.UserState; ;
            try
            {
                string reply = System.Text.Encoding.UTF8.GetString(e.Result);
                ////Console.WriteLine(reply);
                m_intUploadIndex++;
                //m_lngFileUploadPreSize = 0;

                //일반 Upload인 경우
                if (m_strFileAction == "")
                {
                    //Upload 화일이 남아있는 경우
                    if (m_intUploadIndex < m_arrFileName.Count)
                    {
                        Uri uri = new Uri(m_strWebURL + m_strUploadPage);
                        //m_lngFileUploadCurSize = 0;   

                        //WebClient를 통해 화일을 Upload한다.
                        WebClient wclient = new WebClient();
                        wclient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadFileProgress);
                        wclient.UploadFileCompleted += new UploadFileCompletedEventHandler(UploadFileComplete);
                        
                        wclient.QueryString.Add("path", m_strServerPath);
                        wclient.QueryString.Add("folder", m_strFolder);
                        wclient.QueryString.Add("action", "");
                        wclient.QueryString.Add("copysourcepath", "");
                        wclient.QueryString.Add("copytargetpath", "");

                        NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                        wclient.Credentials = cred;
                        wclient.UploadFileAsync(uri, "POST", m_arrFileName[m_intUploadIndex].ToString());

                        if (m_bolUseProgView == true)
                        {
                            //Upload한 화일 갯수를 보여준다.
                            m_lngFileUploadCount++;
                            uTextFileCount.Text = m_lngFileUploadCount.ToString();

                            //Upload중인 화일 총진행율을 보여줄 ProgressBar를 설정함.
                            uProgressCurProg.Minimum = 0;
                            uProgressCurProg.Maximum = 50; // Convert.ToInt32(m_arrUploadFileSize[m_intUploadIndex]);
                            uProgressCurProg.Value = 0;

                            //Upload중인 화일명을 보여준다.
                            FileInfo fileName = new FileInfo(m_arrFileName[m_intUploadIndex].ToString());
                            uTextFileName.Text = fileName.Name.ToString();
                        }
                    }
                    //모든 화일을 Upload한 경우
                    else
                    {
                        this.Close();
                    }
                }
                //복사나 삭제인 경우
                else
                {
                    //Upload 화일이 남아있는 경우
                    if (m_intUploadIndex < m_arrSourceFileName.Count)
                    {
                        Uri uri = new Uri(m_strWebURL + m_strUploadPage);
                        //m_lngFileUploadCurSize = 0;   

                        //WebClient를 통해 화일을 Upload한다.
                        WebClient wclient = new WebClient();
                        wclient.UploadProgressChanged += new UploadProgressChangedEventHandler(UploadFileProgress);
                        wclient.UploadFileCompleted += new UploadFileCompletedEventHandler(UploadFileComplete);

                        wclient.QueryString.Add("path", m_strServerPath);
                        wclient.QueryString.Add("folder", m_strFolder);
                        wclient.QueryString.Add("action", m_strFileAction);
                        if (m_arrSourceFileName.Count > 0)
                            wclient.QueryString.Add("copysourcepath", m_arrSourceFileName[m_intUploadIndex].ToString());
                        else
                            wclient.QueryString.Add("copysourcepath", "");

                        if (m_arrTargetFileName.Count > 0)
                            wclient.QueryString.Add("copytargetpath", m_arrTargetFileName[m_intUploadIndex].ToString());
                        else
                            wclient.QueryString.Add("copytargetpath", "");

                        NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                        wclient.Credentials = cred;
                        //wclient.UploadFileAsync(uri, "POST", m_arrFileName[m_intUploadIndex].ToString());
                        string strBrowserPath = Application.ExecutablePath;
                        wclient.UploadFileAsync(uri, "POST", strBrowserPath);

                        if (m_bolUseProgView == true)
                        {
                            //Upload한 화일 갯수를 보여준다.
                            m_lngFileUploadCount++;
                            uTextFileCount.Text = m_lngFileUploadCount.ToString();

                            //Upload중인 화일 총진행율을 보여줄 ProgressBar를 설정함.
                            uProgressCurProg.Minimum = 0;
                            uProgressCurProg.Maximum = 50; // Convert.ToInt32(m_arrUploadFileSize[m_intUploadIndex]);
                            uProgressCurProg.Value = 0;

                            //Upload중인 화일명을 보여준다.
                            FileInfo fileName = new FileInfo(m_arrFileName[m_intUploadIndex].ToString());
                            uTextFileName.Text = fileName.Name.ToString();
                        }
                    }
                    //모든 화일을 Upload한 경우
                    else
                    {
                        this.Close();
                    }
                }
            }
            catch (WebException ex)
            {

            }
            finally
            {
                // If this thread throws an exception, make sure that
                // you let the main application thread resume.
                //waiter.Set();
            }
        }

        private byte[] GetData(string filePath) 
        { 
            FileStream stream = File.OpenRead(filePath); 
            byte[] data = new byte[stream.Length]; 
            stream.Read(data, 0, data.Length); 
            stream.Close(); 
            return data; 
        }

        /// <summary>
        /// 화일을 WebClient를 통해 Download함
        /// </summary>
        private void mfFileDownload()
        {
            try
            {
                uTextTotalFileCount.Text = m_arrFileName.Count.ToString();

                if (!Directory.Exists(m_strDownloadClientPath))
                    Directory.CreateDirectory(m_strDownloadClientPath);

                //Upload화일 총진행율을 보여줄 ProgressBar를 설정함.
                uProgressTotalProg.Minimum = 0;
                uProgressTotalProg.Maximum = 100 * m_arrFileName.Count;

                //UploadURL 지정
                Uri uri = new Uri(m_strWebURL + m_strFolder + m_arrFileName[m_intUploadIndex].ToString());

                //WebClient를 통해 화일을 Upload한다.
                WebClient wclient = new WebClient();
                wclient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadFileProgress);
                wclient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);

                NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                wclient.Credentials = cred;
                wclient.DownloadFileAsync(uri, m_strDownloadClientPath + m_arrFileName[m_intUploadIndex].ToString());

                ////////////Upload한 화일 갯수를 보여준다.
                //////////m_lngFileUploadCount++;
                //////////if (m_txtUploadFileCount != null)
                //////////    m_txtUploadFileCount.Text = m_lngFileUploadCount.ToString();

                //Download중인 화일 총진행율을 보여줄 ProgressBar를 설정함.
                uProgressCurProg.Minimum = 0;
                uProgressCurProg.Maximum = 100; //Convert.ToInt32(m_arrUploadFileSize[m_intUploadIndex]);

                //Download중인 화일명을 보여준다.
                FileInfo file = new FileInfo(m_arrFileName[m_intUploadIndex].ToString());
                uTextFileName.Text = file.Name.ToString();

            }
            catch (WebException ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일을 WebClient를 통해 Download함
        /// </summary>
        public void mfFileDownloadNoProgView()
        {
            try
            {
                m_bolUseProgView = false;

                if (!Directory.Exists(m_strDownloadClientPath))
                    Directory.CreateDirectory(m_strDownloadClientPath);

                //UploadURL 지정
                Uri uri = new Uri(m_strWebURL + m_strFolder + m_arrFileName[m_intUploadIndex].ToString());

                //WebClient를 통해 화일을 Upload한다.
                WebClient wclient = new WebClient();
                wclient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadFileProgress);
                wclient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);

                NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                wclient.Credentials = cred;
                wclient.DownloadFileAsync(uri, m_strDownloadClientPath + m_arrFileName[m_intUploadIndex].ToString());
            }
            catch (WebException ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일을 Download하는 동안 발생하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DownloadFileProgress(Object sender, DownloadProgressChangedEventArgs e)
        {
            try
            {
                //진행상태화면을 보여줄 경우만 처리함.
                if (m_bolUseProgView == true)
                {
                    //현재 화일의 진행율을 보여줌 
                    uProgressCurProg.Value = e.ProgressPercentage;

                    //전체 진행율을 보여줌
                    uProgressTotalProg.Value = m_intUploadIndex * 100 + e.ProgressPercentage;
                }
            }
            catch (WebException ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일을 Download를 완료할때 발생하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DownloadFileCompleted(Object sender, AsyncCompletedEventArgs e)
        {
            System.Threading.AutoResetEvent waiter = (System.Threading.AutoResetEvent)e.UserState; ;
            try
            {
                //string reply = System.Text.Encoding.UTF8.GetString(e.Result);
                //Console.WriteLine(reply);
                m_intUploadIndex++;
                //m_lngFileUploadPreSize = 0;

                //Upload 화일이 남아있는 경우
                if (m_intUploadIndex < m_arrFileName.Count)
                {
                    Uri uri = new Uri(m_strWebURL + m_strFolder + m_arrFileName[m_intUploadIndex].ToString());
                    //m_lngFileUploadCurSize = 0;

                    //WebClient를 통해 화일을 Upload한다.
                    WebClient wclient = new WebClient();
                    wclient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadFileProgress);
                    wclient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);
                    //wclient.QueryString.Add("path", m_strServerPath);
                    //wclient.QueryString.Add("folder", m_strFolder);

                    NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                    wclient.Credentials = cred;
                    wclient.DownloadFileAsync(uri, m_strDownloadClientPath + m_arrFileName[m_intUploadIndex].ToString());

                    //진행상태화면을 보여줄 경우만 처리함.
                    if (m_bolUseProgView == true)
                    {
                        //Upload한 화일 갯수를 보여준다.
                        m_lngFileUploadCount++;
                        uTextFileCount.Text = m_lngFileUploadCount.ToString();

                        //Download중인 화일 총진행율을 보여줄 ProgressBar를 설정함.
                        uProgressCurProg.Minimum = 0;
                        uProgressCurProg.Maximum = 100; // Convert.ToInt32(m_arrUploadFileSize[m_intUploadIndex]);
                        uProgressCurProg.Value = 0;

                        //Upload중인 화일명을 보여준다.
                        FileInfo file = new FileInfo(m_arrFileName[m_intUploadIndex].ToString());
                        uTextFileName.Text = file.Name.ToString();
                    }
                }
                //모든 화일을 Upload한 경우
                else
                {
                    m_bolDownloadComplete = true;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (WebException ex)
            {

            }
            finally
            {
                // If this thread throws an exception, make sure that
                // you let the main application thread resume.
                //waiter.Set();
            }
        }

        /// <summary>
        /// 화일을 비동기 방식으로 전송 // 프로그래스 바 안보임 // 외부에서 showdialog없이 사용
        /// </summary>
        public bool mfFileUpload_NonAssync()
        {
            try
            {

                //UploadURL 지정
                Uri uri = new Uri(m_strWebURL + m_strUploadPage);

                if (m_strFileAction.Equals(string.Empty))
                {
                    for (int i = 0; i < m_arrFileName.Count; i++)
                    {
                        //WebClient를 통해 화일을 Upload한다.
                        WebClient wclient = new WebClient();

                        wclient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                        wclient.QueryString.Add("path", m_strServerPath);
                        wclient.QueryString.Add("folder", m_strFolder);
                        wclient.QueryString.Add("action", m_strFileAction);

                        NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                        wclient.Credentials = cred;

                        FileInfo fileName = new FileInfo(m_arrFileName[i].ToString());
                        wclient.UploadFile(uri, "POST", m_arrFileName[i].ToString());
                        wclient.Dispose();
                    }
                }
                else
                {
                    for (int i = 0; i < m_arrSourceFileName.Count; i++)
                    {
                        WebClient wclient = new WebClient();

                        wclient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                        wclient.QueryString.Add("path", m_strServerPath);
                        wclient.QueryString.Add("folder", m_strFolder);
                        wclient.QueryString.Add("action", m_strFileAction);

                        NetworkCredential cred = new NetworkCredential(m_strUserID, m_strPassword);
                        wclient.Credentials = cred;

                        if (m_arrSourceFileName.Count > 0)
                            wclient.QueryString.Add("copysourcepath", m_arrSourceFileName[i].ToString());
                        else
                            wclient.QueryString.Add("copysourcepath", "");

                        if (m_arrTargetFileName.Count > 0)
                            wclient.QueryString.Add("copytargetpath", m_arrTargetFileName[i].ToString());
                        else
                            wclient.QueryString.Add("copytargetpath", "");

                        string strBrowserPath = Application.ExecutablePath;
                        wclient.UploadFile(uri, "POST", strBrowserPath);
                        wclient.Dispose();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw (ex);
            }
            finally
            {
            }
        }

    }
}
