﻿namespace QRPCOM
{
    partial class frmWebClientFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.uProgressCurProg = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this.uProgressTotalProg = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this.uTextFileName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTotalFileCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFileCount = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel1 = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTotalFileCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileCount)).BeginInit();
            this.SuspendLayout();
            // 
            // uProgressCurProg
            // 
            this.uProgressCurProg.Location = new System.Drawing.Point(204, 124);
            this.uProgressCurProg.Name = "uProgressCurProg";
            this.uProgressCurProg.Size = new System.Drawing.Size(188, 20);
            this.uProgressCurProg.TabIndex = 5;
            this.uProgressCurProg.Text = "[Formatted]";
            // 
            // uProgressTotalProg
            // 
            this.uProgressTotalProg.Location = new System.Drawing.Point(204, 160);
            this.uProgressTotalProg.Name = "uProgressTotalProg";
            this.uProgressTotalProg.Size = new System.Drawing.Size(188, 20);
            this.uProgressTotalProg.TabIndex = 6;
            this.uProgressTotalProg.Text = "[Formatted]";
            // 
            // uTextFileName
            // 
            appearance4.TextHAlignAsString = "Left";
            this.uTextFileName.Appearance = appearance4;
            this.uTextFileName.Location = new System.Drawing.Point(224, 84);
            this.uTextFileName.Name = "uTextFileName";
            this.uTextFileName.ReadOnly = true;
            this.uTextFileName.Size = new System.Drawing.Size(168, 21);
            this.uTextFileName.TabIndex = 11;
            // 
            // uTextTotalFileCount
            // 
            appearance3.TextHAlignAsString = "Right";
            this.uTextTotalFileCount.Appearance = appearance3;
            this.uTextTotalFileCount.Location = new System.Drawing.Point(360, 60);
            this.uTextTotalFileCount.Name = "uTextTotalFileCount";
            this.uTextTotalFileCount.ReadOnly = true;
            this.uTextTotalFileCount.Size = new System.Drawing.Size(32, 21);
            this.uTextTotalFileCount.TabIndex = 14;
            // 
            // uTextFileCount
            // 
            appearance2.TextHAlignAsString = "Right";
            this.uTextFileCount.Appearance = appearance2;
            this.uTextFileCount.Location = new System.Drawing.Point(316, 60);
            this.uTextFileCount.Name = "uTextFileCount";
            this.uTextFileCount.ReadOnly = true;
            this.uTextFileCount.Size = new System.Drawing.Size(32, 21);
            this.uTextFileCount.TabIndex = 13;
            // 
            // ultraLabel1
            // 
            appearance1.BackColor = System.Drawing.Color.White;
            this.ultraLabel1.Appearance = appearance1;
            this.ultraLabel1.Location = new System.Drawing.Point(348, 64);
            this.ultraLabel1.Name = "ultraLabel1";
            this.ultraLabel1.Size = new System.Drawing.Size(12, 16);
            this.ultraLabel1.TabIndex = 15;
            this.ultraLabel1.Text = "/";
            // 
            // frmWebClientFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //this.BackgroundImage = global::QRPCOM.Properties.Resources.download;
            this.ClientSize = new System.Drawing.Size(401, 201);
            this.ControlBox = false;
            this.Controls.Add(this.ultraLabel1);
            this.Controls.Add(this.uTextTotalFileCount);
            this.Controls.Add(this.uTextFileCount);
            this.Controls.Add(this.uTextFileName);
            this.Controls.Add(this.uProgressTotalProg);
            this.Controls.Add(this.uProgressCurProg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmWebClientFile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmWebClientFile_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTotalFileCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFileCount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar uProgressCurProg;
        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar uProgressTotalProg;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFileName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTotalFileCount;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFileCount;
        private Infragistics.Win.Misc.UltraLabel ultraLabel1;

    }
}