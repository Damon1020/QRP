﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Mail;

namespace QRPCOM
{
    class QRPEML
    {
        /// <summary>
        /// SMTP로 메일 보내기
        /// </summary>
        /// <param name="strSMTPServer">SMTP서버 주소</param>
        /// <param name="intPort">SMTP서버 Port</param>
        /// <param name="strFromEMail">보내는 사람 이메일 주소</param>
        /// <param name="strFromUserName">보내는 사람 이름</param>
        /// <param name="strToEMail">받는 사람 이메일 주소</param>
        /// <param name="strSubject">메일 제목</param>
        /// <param name="strBody">메일 내용</param>
        /// <param name="arrAttchFileList">첨부화일</param>
        public void mfSendMailSMTP(string strSMTPServer, int intPort, string strFromEMail, string strFromUserName, 
                                   string strToEMail, string strSubject, string strBody, System.Collections.ArrayList arrAttchFileList)
        {
            try
            {
                SmtpClient SmtpServer = new SmtpClient(strSMTPServer);
                SmtpServer.Port = intPort;

                MailMessage mail = new MailMessage();
                mail.From = new System.Net.Mail.MailAddress(strFromEMail, strFromUserName, System.Text.Encoding.UTF8);
                mail.To.Add(strToEMail);
                mail.Subject = strSubject;
                mail.Body = strBody;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                System.Net.Mail.Attachment attach;
                for (int i = 0; i < arrAttchFileList.Count; i++)
			    {
                    attach = new System.Net.Mail.Attachment(arrAttchFileList[i].ToString());
                    mail.Attachments.Add(attach);
			    }

                SmtpServer.Send(mail);

                mail.Dispose();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }
    }
}
