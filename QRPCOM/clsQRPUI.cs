﻿/*----------------------------------------------------------------------*/
/* 시스템명     : QRP                                                   */
/* 모듈(분류)명 : QRP UI Library                                        */
/* 프로그램ID   : clsQRPUI.cs                                           */
/* 프로그램명   : QRP UI 컨트롤 공통Method                              */
/* 작성자       : 류근철                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-05 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//추가 참조
//using System.Collections;
using System.Drawing;
using System.Data;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Threading;
using System.Resources;

//using System.EnterpriseServices;
//using System.Configuration;
//using System.Net;
//using System.Threading;

//using System.Runtime.Remoting;
//using System.Runtime.Remoting.Channels;
//using System.Runtime.Remoting.Channels.Http;

using Infragistics.Win;
using Infragistics.Shared;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTabControl;

using Microsoft.Win32;
using System.Xml.Linq;

namespace QRPCOM
{
    namespace QRPUI
    {
        #region Enum 및 구조체 선언
        /// <summary>
        /// GroupBox 유형을 지정하는 Enum
        /// </summary>
        public enum GroupBoxType
        {
            LIST = 1,
            INFO = 2,
            DETAIL = 3,
            CHART = 4
        }

        /// <summary>
        /// MessageBox 유형을 지정하는 Enum
        /// </summary>
        public enum MessageBoxType
        {
            Information = 1,
            Warning = 2,
            Error = 3,
            YesNo = 4,
            YesNoCancel = 5,
            AgreeReject = 6,
            ReqAgreeSave = 7
        }

        /// <summary>
        /// UltraTree에 추가할 그리드 열에 대한 속성
        /// </summary>
        public struct GridColumn_TreeNode
        {
            public string strKey; //그리드 컬럼의 Key
            public string strText; //그리드 컬럼의 헤더명
            public bool bolVisible; //그리드 컬럼의 표시여부
            public Infragistics.Win.DefaultableBoolean AllowSorting; //그리드 컬럼의 정렬여부
            public Infragistics.Win.DefaultableBoolean ShowSortIndicator; //그리드 컬럼의 정렬 헤더 표시 여부
            public Infragistics.Win.UltraWinTree.SortType ColSortType; //그리드 컬럼의 정렬유형
            public Infragistics.Win.UltraWinTree.ColumnAutoSizeMode ColAutoSizeType; //그리드 컬럼의 자동크기 조정 유형
            public Infragistics.Win.HAlign ColHAlign; //그리드 컬럼의 수평정렬
            public Infragistics.Win.VAlign ColVAlign; //그리드 컬럼의 수직정렬

            public GridColumn_TreeNode(string mKey, string mText, bool mVisible,
                                       Infragistics.Win.DefaultableBoolean mAllowSorting,
                                       Infragistics.Win.DefaultableBoolean mShowSortIndicator,
                                       Infragistics.Win.UltraWinTree.SortType mColSortType,
                                       Infragistics.Win.UltraWinTree.ColumnAutoSizeMode mColAutoSizeType,
                                       Infragistics.Win.HAlign mColHAlign,
                                       Infragistics.Win.VAlign mColVAlign)
            {
                this.strKey = mKey;
                this.strText = mText;
                this.bolVisible = mVisible;
                this.AllowSorting = mAllowSorting;
                this.ShowSortIndicator = mShowSortIndicator;
                this.ColSortType = mColSortType;
                this.ColAutoSizeType = mColAutoSizeType;
                this.ColHAlign = mColHAlign;
                this.ColVAlign = mColVAlign;
            }
        }
        #endregion

        #region UltraGroupBox Control 설정
        public class WinGroupBox
        {
            /// <summary>
            /// UltraGroupBox 초기화하는 함수
            /// </summary>
            /// <param name="GroupBox">GroupBox 이름</param>
            /// <param name="strType">GroupBox 유형 --> enum으로 처리??</param>
            /// <param name="strText">GroupBox 제목</param>
            /// <param name="strFontName">GroupBox 제목 폰트</param>
            /// <param name="gbViewStyle">GroupBox ViewStyle</param>
            /// <param name="gbHeaderPosition">GroupBox Title 위치</param>
            /// <param name="gbBorderStyle">GroupBox 외곽선 스타일</param>
            /// <param name="gbCaptionAlignment">GroupBox 제목 수평 위치</param>
            /// <param name="gbVerticalOrentation">GroupBox 제목의 수직높이 조정</param>
            public void mfSetGroupBox(Infragistics.Win.Misc.UltraGroupBox GroupBox,
                                      GroupBoxType GroupBoxType,
                                      string strText,
                                      string strFontName,
                                      Infragistics.Win.Misc.GroupBoxViewStyle gbViewStyle,
                                      Infragistics.Win.Misc.GroupBoxHeaderPosition gbHeaderPosition,
                                      Infragistics.Win.Misc.GroupBoxBorderStyle gbBorderStyle,
                                      Infragistics.Win.Misc.GroupBoxCaptionAlignment gbCaptionAlignment,
                                      Infragistics.Win.Misc.GroupBoxVerticalTextOrientation gbVerticalOrentation)
            {
                try
                {
                    ImageList imgList = new ImageList();
                    imgList.Images.Add("LIST", Properties.Resources.gb_list);
                    imgList.Images.Add("INFO", Properties.Resources.gb_info);
                    imgList.Images.Add("DETAIL", Properties.Resources.gb_detail);
                    imgList.Images.Add("CHART", Properties.Resources.gb_graph);

                    // == 유형에 따른 ICON처리 == //
                    if (GroupBoxType == GroupBoxType.CHART)
                    {
                        GroupBox.HeaderAppearance.Image = imgList.Images["CHART"];
                    }
                    else if (GroupBoxType == GroupBoxType.LIST)
                    {
                        GroupBox.HeaderAppearance.Image = imgList.Images["LIST"];
                    }
                    else if (GroupBoxType == GroupBoxType.INFO)
                    {
                        GroupBox.HeaderAppearance.Image = imgList.Images["INFO"];
                    }
                    else if (GroupBoxType == GroupBoxType.DETAIL)
                    {
                        GroupBox.HeaderAppearance.Image = imgList.Images["DETAIL"];
                    }

                    // == GroupBox 제목, 폰트설정 == //
                    GroupBox.Text = strText;
                    GroupBox.Appearance.FontData.Name = strFontName;
                    //GroupBox.Font = new System.Drawing.Font(strFontName, 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));

                    // == ViewStyle == //
                    GroupBox.ViewStyle = gbViewStyle;

                    // == HeaderPosition == //
                    GroupBox.HeaderPosition = gbHeaderPosition;

                    // == Border Style == //
                    GroupBox.BorderStyle = gbBorderStyle;

                    // == CaptionAlignment == //
                    GroupBox.CaptionAlignment = gbCaptionAlignment;

                    // == Vertical Orentation == //
                    GroupBox.VerticalTextOrientation = gbVerticalOrentation;

                    // == 공통속성 == //
                    GroupBox.HeaderAppearance.BackColor = Color.CornflowerBlue;
                    GroupBox.HeaderAppearance.BackColor2 = Color.White;
                    GroupBox.HeaderAppearance.BackGradientStyle = GradientStyle.Horizontal;
                    GroupBox.HeaderAppearance.BorderColor = Color.RoyalBlue;
                    GroupBox.Appearance.BackColor = Color.Transparent;
                    GroupBox.Appearance.BackColor2 = Color.Transparent;
                    GroupBox.HeaderBorderStyle = UIElementBorderStyle.Rounded3;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }
        }
        #endregion

        #region UltraGrid Control 설정
        public class WinGrid
        {
            /// <summary>
            /// 그리드 초기화하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="bolHiddenTopGroupBox">그리드 상단의 GroupBox 안보이게 처리 여부</param>
            /// <param name="ColAutoLoadStyle">그리드 컬럼에 대해 Binding시 자동생성처리 Style</param>
            /// <param name="ColAutoFitStyle">그리드 컬럼 자동 설정</param>
            /// <param name="bolEdit">그리드 편집여부</param>
            /// <param name="bolCaptionView">그리드 상단 Caption 보이게 처리하는지 여부</param>
            /// <param name="MergedCellStyle">그리드 셀에 대한 Merget Styple</param>
            /// <param name="bolUseFixedHeader">그리드 고정열 사용여부</param>
            /// <param name="FixedIndicator">그리드 고정열에 대한 Header셀에 표시형태</param>
            /// <param name="ColSelectType">그리드 컬럼선택 유형</param>
            /// <param name="bolColUseFilter">그리드 컬럼의 Filter사용 여부</param>
            /// <param name="FilterIndicator">그리드 Filter열에 대한 Header셀에 표시형태</param>
            /// <param name="intHeaderTextOrientDegree">그리드 Header 텍스트에 대한 기울기</param>
            /// <param name="bolUseMultiLine">그리드 표시내용에 대한 다중라인 사용여부</param>
            /// <param name="strFontName">그리드 폰트</param>
            public void mfInitGeneralGrid(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                          bool bolHiddenTopGroupBox,
                                          Infragistics.Win.UltraWinGrid.NewColumnLoadStyle ColAutoLoadStyle,
                                          Infragistics.Win.UltraWinGrid.AutoFitStyle ColAutoFitStyle,
                                          bool bolEdit,
                                          Infragistics.Win.DefaultableBoolean bolCaptionView,
                                          Infragistics.Win.UltraWinGrid.MergedCellStyle MergedCellStyle,
                                          bool bolUseFixedHeader,
                                          Infragistics.Win.UltraWinGrid.FixedHeaderIndicator FixedIndicator,
                                          Infragistics.Win.UltraWinGrid.SelectType ColSelectType,
                                          Infragistics.Win.DefaultableBoolean bolColUseFilter,
                                          Infragistics.Win.UltraWinGrid.FilterUIType FilterIndicator,
                                          Infragistics.Win.UltraWinGrid.AllowAddNew AllowAddNewRow,
                                          int intHeaderTextOrientDegree,
                                          bool bolUseMultiLine,
                                          string strFontName)
            {
                try
                {
                    Grid.KeyActionMappings.Add(new GridKeyActionMapping(Keys.Z, UltraGridAction.UndoCell, 0, 0, 0, SpecialKeys.Ctrl));
                    //Grid.KeyActionMappings.Add(new GridKeyActionMapping(Keys.Escape, UltraGridAction.UndoRow, UltraGridState.Row, UltraGridState.Row, 0, 0));

                    //그리드 컬럼 Clear시킴
                    Grid.DisplayLayout.ClearGroupByColumns();

                    //그리드 상단의 GroupBox 표시여부 (기본값 = true)
                    Grid.DisplayLayout.GroupByBox.Hidden = bolHiddenTopGroupBox;
                    
                    //그리드 컬럼 자동생성 해지 (기본값 = Hide)
                    Grid.DisplayLayout.NewColumnLoadStyle = ColAutoLoadStyle;

                    //그리드 컬럼 폭크기 자동 변경처리 여부 (기본값 = None)
                    Grid.DisplayLayout.AutoFitStyle = ColAutoFitStyle;
                    //Grid.DisplayLayout.AutoFitStyle = AutoFitStyle.
                    //그리드 편집여부 (기본값 = false)
                    if (bolEdit == false)
                        Grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;
                    else
                        Grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;

                    //그리드 캡션 안보이게 설정 (기본값 = False)
                    Grid.DisplayLayout.CaptionVisible = bolCaptionView;

                    //그리드 전체 Cell Merge 설정 (기본값 = Never)
                    Grid.DisplayLayout.Override.MergedCellStyle = MergedCellStyle;

                    //그리드 고정열 사용여부 지정 (기본값 = true)
                    Grid.DisplayLayout.UseFixedHeaders = bolUseFixedHeader;

                    //그리드 고정열 헤더표시 모양 지정 (기본값 = Default)
                    if (bolUseFixedHeader == true)
                        Grid.DisplayLayout.Override.FixedHeaderIndicator = FixedIndicator;
                    else
                        Grid.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;

                    //그리드 헤더컬럼을 선택시 Sorting처리여부 (기본값 = None)
                    Grid.DisplayLayout.Override.SelectTypeCol = ColSelectType;
                    if (ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.None || ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.Default)
                        Grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
                    else if (ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.Single || ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag)
                        Grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortSingle;
                    else if (ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.Extended || ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag)
                        Grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;

                    //그리드 Filter 기능 여부 (기본값 = false)
                    Grid.DisplayLayout.Override.AllowRowFiltering = bolColUseFilter;   //기본값 = false
                    if (bolColUseFilter == Infragistics.Win.DefaultableBoolean.True || bolColUseFilter == Infragistics.Win.DefaultableBoolean.Default)
                        Grid.DisplayLayout.Override.FilterUIType = FilterIndicator;         //기본값 = Default

                    //그리드 헤더컬럼 Text회전 처리 (기본값 = 0)
                    Grid.DisplayLayout.Override.ColumnHeaderTextOrientation = new TextOrientationInfo(intHeaderTextOrientDegree, Infragistics.Win.TextFlowDirection.Horizontal);

                    //셀 내용에 대해 Multi-Line 여부 (기본값 = false)
                    if (bolUseMultiLine == true)
                    {
                        Grid.DisplayLayout.Override.CellMultiLine = Infragistics.Win.DefaultableBoolean.True; //셀 내용에 대해 Multi-Line 여부 (기본값 = false)                
                        Grid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
                        Grid.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.AutoFree;
                    }

                    Grid.DisplayLayout.Override.HeaderAppearance.FontData.Name = strFontName;
                    Grid.DisplayLayout.Override.CellAppearance.FontData.Name = strFontName;

                    //자동 행추가 속성 설정 및 RowSelect 속성 설정(편집이 가능할 때만 설정가능)
                    if (bolEdit == true)
                    {
                        Grid.DisplayLayout.Override.AllowAddNew = AllowAddNewRow;


                        Grid.DisplayLayout.Override.AddRowCellAppearance.BackColor = Color.LightYellow;
                        Grid.DisplayLayout.Override.AddRowCellAppearance.ForeColor = Color.Black;

                        Grid.DisplayLayout.Override.RowSelectors = DefaultableBoolean.True;
                        Grid.DisplayLayout.Override.RowSelectorStyle = HeaderStyle.WindowsVista;

                        Grid.DisplayLayout.Override.RowSelectorHeaderStyle = RowSelectorHeaderStyle.ExtendFirstColumn;
                        Grid.DisplayLayout.Override.RowSelectorNumberStyle = RowSelectorNumberStyle.VisibleIndex;
                        Grid.DisplayLayout.Override.RowSelectorAppearance.TextHAlign = HAlign.Center;

                        Grid.DisplayLayout.Override.ActiveCellRowSelectorAppearance.BackColor = Color.Red;
                        Grid.DisplayLayout.Override.ActiveCellRowSelectorAppearance.FontData.Name = strFontName;

                        //셀에 복사 및 붙여넣기 기능이 되도록 Clipboard 기능 설정
                        Grid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.Copy | Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.Paste
                                                                               | Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.Undo;
                    }
                    else
                    {
                        //셀에 복사 및 붙여넣기 기능이 되도록 Clipboard 기능 설정
                        Grid.DisplayLayout.Override.AllowMultiCellOperations = Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.Copy | Infragistics.Win.UltraWinGrid.AllowMultiCellOperation.Paste;

                    }

                    //그리드 속성에서 고정값으로 속성을 설정
                    mfSetFixedPropertyOfGrid(Grid);
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 그리드 속성에서 고정값으로 속성을 설정하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            public void mfSetFixedPropertyOfGrid(Infragistics.Win.UltraWinGrid.UltraGrid Grid)
            {
                try
                {
                    //////// (고정 속성값 처리) ////////
                    //헤더 스타일 지정
                    Grid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsVista;

                    //행 선택에 대한 Style처리
                    Grid.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;
                    
                    //행 삭제 속성 False 처리
                    Grid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;

                    //그리드 Font 크기 = 9) : 안되므로 UI에서 처리
                    //Grid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                    //Grid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                    //Grid.DisplayLayout.Appearance.FontData.SizeInPoints = 9;

                    //헤더 정렬 지정
                    Grid.DisplayLayout.Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    Grid.DisplayLayout.Override.HeaderAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                    
                    //헤더 배경색 및 글짜색 지정
                    Grid.DisplayLayout.Override.HeaderAppearance.BackColor = System.Drawing.Color.FromArgb(106, 160, 206);
                    Grid.DisplayLayout.Override.HeaderAppearance.ForeColor = System.Drawing.Color.FromArgb(50, 50, 50);
                    Grid.DisplayLayout.Override.HeaderAppearance.FontData.Bold = DefaultableBoolean.True;
                    //Grid.DisplayLayout.Override.HeaderAppearance.BackColor2 = Color.SteelBlue;

                    //선택할 셀의 컬럼헤더 글자색 지정
                    Grid.DisplayLayout.Override.ActiveCellColumnHeaderAppearance.ForeColor = Color.DimGray;

                    //홀수줄 짝수줄 색깔 지정
                    Grid.DisplayLayout.Override.RowAppearance.BackColor = System.Drawing.Color.White;
                    //Grid.DisplayLayout.Override.RowAlternateAppearance.BackColor = System.Drawing.Color.AliceBlue; //System.Drawing.Color.FromArgb(234, 239, 249);
                    Grid.DisplayLayout.Override.RowAlternateAppearance.BackColor = System.Drawing.Color.MintCream;

                    //그리드 행에 MouseOver시 색깔 지정 (요기 처리해야 함)
                    Grid.DisplayLayout.Override.HotTrackRowAppearance.BackColor = System.Drawing.Color.WhiteSmoke;
                    Grid.DisplayLayout.Override.HotTrackRowAppearance.BackGradientStyle = GradientStyle.Vertical;
                    Grid.DisplayLayout.Override.HotTrackRowAppearance.ForeColor = System.Drawing.Color.FromArgb(64, 64, 64);

                    //고정 컬럼 색깔 지정
                    Grid.DisplayLayout.Override.FixedCellAppearance.BackColor = System.Drawing.Color.FromArgb(201, 216, 247);
                    Grid.DisplayLayout.Override.FixedRowSelectorAppearance.BackColor = Color.Red;                   

                    //그리드 내부선 색 지정
                    Grid.DisplayLayout.Override.CellAppearance.BorderColor = System.Drawing.Color.FromArgb(125, 138, 158);
                    Grid.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Default;
                    Grid.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Default;

                    //Tab Key를 누르면 다음 셀로 이동처리
                    //Grid.DisplayLayout.TabNavigation = Infragistics.Win.UltraWinGrid.TabNavigation.NextCell;
                    Grid.DisplayLayout.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;

                    //GridKeyActionMapping mapping = new GridKeyActionMapping(Keys.Enter, UltraGridAction.NextCellByTab, (UltraGridState)0, UltraGridState.InEdit, SpecialKeys.All, (SpecialKeys)0);

                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping11 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Enter, Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping12 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Enter, Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping21 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Down, Infragistics.Win.UltraWinGrid.UltraGridAction.BelowCell, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping22 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Down, Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping31 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Up, Infragistics.Win.UltraWinGrid.UltraGridAction.AboveCell, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping32 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Up, Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode, 0, 0, 0, 0);
                    //Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping41 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Tab, Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell, 0, 0, 0, 0);
                    //Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping42 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Tab, Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping51 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Left, Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping52 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Left, Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping61 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Right, Infragistics.Win.UltraWinGrid.UltraGridAction.NextCell, 0, 0, 0, 0);
                    Infragistics.Win.UltraWinGrid.GridKeyActionMapping keyMapping62 = new Infragistics.Win.UltraWinGrid.GridKeyActionMapping(Keys.Right, Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode, 0, 0, 0, 0);


                    Grid.KeyActionMappings.Add(keyMapping11);
                    Grid.KeyActionMappings.Add(keyMapping12);
                    //Grid.KeyActionMappings.Add(keyMapping21);
                    Grid.KeyActionMappings.Add(keyMapping22);
                    //Grid.KeyActionMappings.Add(keyMapping31);
                    Grid.KeyActionMappings.Add(keyMapping32);
                    //Grid.KeyActionMappings.Add(keyMapping41);
                    //Grid.KeyActionMappings.Add(keyMapping42);
                    Grid.KeyActionMappings.Add(keyMapping51);
                    Grid.KeyActionMappings.Add(keyMapping52);
                    Grid.KeyActionMappings.Add(keyMapping61);
                    Grid.KeyActionMappings.Add(keyMapping62);

                    //Grid.KeyActionMappings.Add(mapping);


                    //스크롤이 최하단으로 내려갔을 때 빈공간이 없도록 설정 
                    Grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;

                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }


            /// <summary>
            /// 임시용 : 참조한 Method가 없어지면 삭제 필요 그리드 헤더에 Merge처리 해야하는 Group Header 설정
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="intBandIndex">Group을 적용할 Band Index</param>
            /// <param name="strColKey">Group 컬럼의 Key</param>
            /// <param name="strColText">Group 컬럼의 Caption</param>
            /// <param name="bolColMandatory">Group 컬럼의 필수여부</param>
            public Infragistics.Win.UltraWinGrid.UltraGridGroup mfSetGridGroup(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                       int intBandIndex,
                                       string strColKey,
                                       string strColText,
                                       bool bolColMandatory)
            {
                Infragistics.Win.UltraWinGrid.UltraGridGroup group = null;
                try
                {
                    Grid.DisplayLayout.Bands[intBandIndex].RowLayoutStyle = RowLayoutStyle.GroupLayout;

                    group = Grid.DisplayLayout.Bands[intBandIndex].Groups.Add(strColKey, strColText);

                    if (bolColMandatory == true)
                    {
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Red;
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Crimson;
                        //group.CellAppearance.ForeColor = System.Drawing.Color.Crimson;
                        group.RowLayoutGroupInfo.Column.Header.Appearance.ForeColor = System.Drawing.Color.Crimson;
                    }

                    //Grid.DisplayLayout.Bands[intBandIndex].Groups[strColKey].CellAppearance.TextHAlign = HAlign.Center;
                    Grid.DisplayLayout.Override.GroupByColumnAppearance.TextHAlign = HAlign.Center;
                    Grid.DisplayLayout.Override.CellAppearance.TextHAlign = HAlign.Center;

                    //그리드 헤더에 Group이 있는 경우
                    //if (bolUseGroupHeader == true)
                    //    Grid.DisplayLayout.Bands[intBandIndex].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                    return group;
                }
                catch (Exception ex)
                {
                    return group;
                }
                finally
                {
                }
            }


            /// <summary>
            /// 그리드 헤더에 Merge처리 해야하는 Group Header 설정
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="intBandIndex">Group을 적용할 Band Index</param>
            /// <param name="strColKey">Group 컬럼의 Key</param>
            /// <param name="strColText">Group 컬럼의 Caption</param>
            /// <param name="bolColMandatory">Group 컬럼의 필수여부</param>
            public Infragistics.Win.UltraWinGrid.UltraGridGroup mfSetGridGroup(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                       int intBandIndex,
                                       string strColKey,
                                       string strColText,
                                       int intOriginX,
                                       int intOriginY,
                                       int intSpanX,
                                       int intSpanY,
                                       bool bolColMandatory)
            {
                Infragistics.Win.UltraWinGrid.UltraGridGroup group = null;
                try
                {
                    Grid.DisplayLayout.Bands[intBandIndex].RowLayoutStyle = RowLayoutStyle.GroupLayout;

                    group = Grid.DisplayLayout.Bands[intBandIndex].Groups.Add(strColKey, strColText);

                    group.RowLayoutGroupInfo.OriginX = intOriginX;
                    group.RowLayoutGroupInfo.OriginY = intOriginY;
                    group.RowLayoutGroupInfo.SpanX = intSpanX;
                    group.RowLayoutGroupInfo.SpanY = intSpanY;

                    if (bolColMandatory == true)
                    {
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Red;
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Crimson;
                        //group.CellAppearance.ForeColor = System.Drawing.Color.Crimson;
                        group.RowLayoutGroupInfo.Column.Header.Appearance.ForeColor = System.Drawing.Color.Crimson;
                    }

                    //Grid.DisplayLayout.Bands[intBandIndex].Groups[strColKey].CellAppearance.TextHAlign = HAlign.Center;
                    Grid.DisplayLayout.Override.GroupByColumnAppearance.TextHAlign = HAlign.Center;
                    Grid.DisplayLayout.Override.CellAppearance.TextHAlign = HAlign.Center;

                    //그리드 헤더에 Group이 있는 경우
                    //if (bolUseGroupHeader == true)
                    //    Grid.DisplayLayout.Bands[intBandIndex].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                    return group;
                }
                catch (Exception ex)
                {
                    return group;
                }
                finally
                {
                }
            }

            /// <summary>
            /// 그리드에 열 추가 및 설정하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="strColKey">컬럼의 Key</param>
            /// <param name="strColText">컬럼의 Caption</param>
            /// <param name="bolColHidden">컬럼에 대한 Hidden여부</param>
            /// <param name="bolUseFixedCol">컬럼에 대한 고정열 여부</param>
            /// <param name="ColAllowEdit">컬럼에 대한 편집허용 속성</param>
            /// <param name="intColWidth">컬럼 폭</param>
            /// <param name="bolColMandatory">컬럼에 대한 필수항목 표시여부</param>
            /// <param name="intColMaxLength">컬럼의 Cell에 대한 최대길이</param>
            /// <param name="ColHAlign">컬럼의 Cell에 대한 수평정렬</param>
            /// <param name="ColVAlign">컬럼의 Cell에 대한 수직정렬</param>
            /// <param name="ColMergeStyle">컬럼의 Cell에 대한 Merge속성</param>
            /// <param name="ColStyle">컬럼의 Cell에 대한 Style속성</param>
            /// <param name="strColFormat">컬럼의 Cell에 대한 Format</param>
            /// <param name="strColMaskInput">컬럼의 Cell에 대한 MaskInput</param>
            public void mfSetGridColumn(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                        int intBandIndex,
                                        string strColKey,
                                        string strColText,
                                        bool bolUseFixedCol,
                                        Infragistics.Win.UltraWinGrid.Activation ColAllowEdit,
                                        int intColWidth,
                                        bool bolColMandatory,
                                        bool bolColHidden,
                                        int intColMaxLength,
                                        Infragistics.Win.HAlign ColHAlign,
                                        Infragistics.Win.VAlign ColVAlign,
                                        Infragistics.Win.UltraWinGrid.MergedCellStyle ColMergeStyle,
                                        Infragistics.Win.UltraWinGrid.ColumnStyle ColStyle,
                                        string strColFormat,
                                        string strColMaskInput,
                                        string strColDefaultValue)
            {
                try
                {
                    //컬럼 Key, 컬럼명 지정(필수)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns.Add(strColKey, strColText);
                    //컬럼 Hidden여부  (기본값 = false)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Hidden = bolColHidden;
                    //컬럼 고정열 처리 (기본값 = false)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Fixed = bolUseFixedCol;

                    //컬럼 셀내 편집여부 (기본값 = AllowEdit)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellActivation = ColAllowEdit;
                    //편집불가(NoEdit)면 컬럼색을 변경.
                    if (ColAllowEdit == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        Grid.DisplayLayout.Bands[0].Columns[strColKey].CellAppearance.BackColor = System.Drawing.Color.Gainsboro;

                    //컬럼 폭          (기본값 = 100)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Width = intColWidth;
                    //컬럼 필수항목 표시(기본값 = false)
                    if (bolColMandatory == true)
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Red;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Crimson;

                    //컬럼 입력 최대 길이
                    if (intColMaxLength > 0)
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MaxLength = intColMaxLength;

                    //컬럼 셀내 정렬처리 (기본값 = Left)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextHAlign = ColHAlign;
                    //컬럼 셀내 정렬처리 (기본값 = Middle)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextVAlign = ColVAlign;

                    //컬럼 Merge 속성 지정 (기본값 = Never)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MergedCellStyle = ColMergeStyle;

                    //컬럼 표시 유형
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Style = ColStyle;

                    //CheckBox일때 헤더에 CheckBox 처리
                    if (ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox)
                    {
                        //Display a checkbox in the column header, where column's data type isBoolean,DefaultableBoolean, or Nullable Boolean.
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;

                        //Aligns the Header checkbox to the right of the Header caption
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;

                        //The checkbox and the cell values are kept in synch to affect only the RowsCollection 
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;
                        
                        //체크박스열인 경우 필터기능을 안보이도록 처리한다.
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AllowRowFiltering = DefaultableBoolean.False;

                        //2단계 체크박스로 속성처리
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].DataType = typeof(Boolean);
                        
                    }
                    //버튼일때 버튼 이미지
                    else if (ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton || ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.Button)
                    {   
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.Image = Properties.Resources.btn_Zoom;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.ImageHAlign = HAlign.Center;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.ImageVAlign = VAlign.Middle;
                    }

                    //컬럼 : 날짜, 시간, 정수, 실수 인 경우 Format 지정
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Format = strColFormat;
                    ////컬럼 : 문자인 경우 Format 지정 yyyy-mm-dd / hh:mm:ss
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MaskInput = strColMaskInput;
                    
                    //컬럼 : 행추가시 기본값 설정
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].DefaultCellValue = strColDefaultValue;

                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 그리드에 열에 대한 속성을 변경하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="strColKey">컬럼의 Key</param>
            /// <param name="strColText">컬럼의 Caption</param>
            /// <param name="bolColHidden">컬럼에 대한 Hidden여부</param>
            /// <param name="bolUseFixedCol">컬럼에 대한 고정열 여부</param>
            /// <param name="ColAllowEdit">컬럼에 대한 편집허용 속성</param>
            /// <param name="intColWidth">컬럼 폭</param>
            /// <param name="bolColMandatory">컬럼에 대한 필수항목 표시여부</param>
            /// <param name="intColMaxLength">컬럼의 Cell에 대한 최대길이</param>
            /// <param name="ColHAlign">컬럼의 Cell에 대한 수평정렬</param>
            /// <param name="ColVAlign">컬럼의 Cell에 대한 수직정렬</param>
            /// <param name="ColMergeStyle">컬럼의 Cell에 대한 Merge속성</param>
            /// <param name="ColStyle">컬럼의 Cell에 대한 Style속성</param>
            /// <param name="strColFormat">컬럼의 Cell에 대한 Format</param>
            /// <param name="strColMaskInput">컬럼의 Cell에 대한 MaskInput</param>
            public void mfChangeGridColumnStyle(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                        int intBandIndex,
                                        string strColKey,
                                        string strColText,
                                        bool bolUseFixedCol,
                                        Infragistics.Win.UltraWinGrid.Activation ColAllowEdit,
                                        int intColWidth,
                                        bool bolColMandatory,
                                        bool bolColHidden,
                                        int intColMaxLength,
                                        Infragistics.Win.HAlign ColHAlign,
                                        Infragistics.Win.VAlign ColVAlign,
                                        Infragistics.Win.UltraWinGrid.MergedCellStyle ColMergeStyle,
                                        Infragistics.Win.UltraWinGrid.ColumnStyle ColStyle,
                                        string strColFormat,
                                        string strColMaskInput,
                                        string strColDefaultValue)
            {
                try
                {
                    //컬럼 Key, 컬럼명 지정(필수)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Caption = strColText;
                    //컬럼 Hidden여부  (기본값 = false)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Hidden = bolColHidden;
                    //컬럼 고정열 처리 (기본값 = false)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Fixed = bolUseFixedCol;

                    //컬럼 셀내 편집여부 (기본값 = AllowEdit)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellActivation = ColAllowEdit;
                    //편집불가(NoEdit)면 컬럼색을 변경.
                    if (ColAllowEdit == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        Grid.DisplayLayout.Bands[0].Columns[strColKey].CellAppearance.BackColor = System.Drawing.Color.Gainsboro;

                    //컬럼 폭          (기본값 = 100)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Width = intColWidth;
                    //컬럼 필수항목 표시(기본값 = false)
                    if (bolColMandatory == true)
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Red;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Crimson;

                    //컬럼 입력 최대 길이
                    if (intColMaxLength > 0)
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MaxLength = intColMaxLength;

                    //컬럼 셀내 정렬처리 (기본값 = Left)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextHAlign = ColHAlign;
                    //컬럼 셀내 정렬처리 (기본값 = Middle)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextVAlign = ColVAlign;

                    //컬럼 Merge 속성 지정 (기본값 = Never)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MergedCellStyle = ColMergeStyle;

                    //컬럼 표시 유형
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Style = ColStyle;

                    //CheckBox일때 헤더에 CheckBox 처리
                    if (ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox)
                    {
                        //Display a checkbox in the column header, where column's data type isBoolean,DefaultableBoolean, or Nullable Boolean.
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;

                        //Aligns the Header checkbox to the right of the Header caption
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;

                        //The checkbox and the cell values are kept in synch to affect only the RowsCollection 
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;

                        //체크박스열인 경우 필터기능을 안보이도록 처리한다.
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AllowRowFiltering = DefaultableBoolean.False;

                        //2단계 체크박스로 속성처리
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].DataType = typeof(Boolean);

                    }
                    //버튼일때 버튼 이미지
                    else if (ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton || ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.Button)
                    {
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.Image = Properties.Resources.btn_Zoom;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.ImageHAlign = HAlign.Center;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.ImageVAlign = VAlign.Middle;
                    }

                    //컬럼 : 날짜, 시간, 정수, 실수 인 경우 Format 지정
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Format = strColFormat;
                    ////컬럼 : 문자인 경우 Format 지정 yyyy-mm-dd / hh:mm:ss
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MaskInput = strColMaskInput;

                    //컬럼 : 행추가시 기본값 설정
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].DefaultCellValue = strColDefaultValue;

                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 그리드에 열 추가 및 Group관련 설정하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="strColKey">컬럼의 Key</param>
            /// <param name="strColText">컬럼의 Caption</param>
            /// <param name="bolColHidden">컬럼에 대한 Hidden여부</param>
            /// <param name="bolUseFixedCol">컬럼에 대한 고정열 여부</param>
            /// <param name="ColAllowEdit">컬럼에 대한 편집허용 속성</param>
            /// <param name="intColWidth">컬럼 폭</param>
            /// <param name="bolColMandatory">컬럼에 대한 필수항목 표시여부</param>
            /// <param name="intColMaxLength">컬럼의 Cell에 대한 최대길이</param>
            /// <param name="ColHAlign">컬럼의 Cell에 대한 수평정렬</param>
            /// <param name="ColVAlign">컬럼의 Cell에 대한 수직정렬</param>
            /// <param name="ColMergeStyle">컬럼의 Cell에 대한 Merge속성</param>
            /// <param name="ColStyle">컬럼의 Cell에 대한 Style속성</param>
            /// <param name="strColFormat">컬럼의 Cell에 대한 Format</param>
            /// <param name="strColMaskInput">컬럼의 Cell에 대한 MaskInput</param>
            /// <param name="strColDefaultValue">컬럼의 행추가시 Cell의 기본값</param>
            /// <param name="intOriginX">컬럼의 수평적 위치(Colnum Index)</param>
            /// <param name="intOriginY">컬럼의 수직적 위치(Row Index)</param>
            /// <param name="intSpanX">컬럼의 수평적 Merge수(Col Merge)</param>
            /// <param name="intSpanY">컬럼의 수직적 Merge수(Row Merge)</param>
            /// <param name="Group"></param>
            public void mfSetGridColumn(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                        int intBandIndex,
                                        string strColKey,
                                        string strColText,
                                        bool bolUseFixedCol,
                                        Infragistics.Win.UltraWinGrid.Activation ColAllowEdit,
                                        int intColWidth,
                                        bool bolColMandatory,
                                        bool bolColHidden,
                                        int intColMaxLength,
                                        Infragistics.Win.HAlign ColHAlign,
                                        Infragistics.Win.VAlign ColVAlign,
                                        Infragistics.Win.UltraWinGrid.MergedCellStyle ColMergeStyle,
                                        Infragistics.Win.UltraWinGrid.ColumnStyle ColStyle,
                                        string strColFormat,
                                        string strColMaskInput,
                                        string strColDefaultValue,
                                        int intOriginX,
                                        int intOriginY,
                                        int intSpanX,
                                        int intSpanY,
                                        Infragistics.Win.UltraWinGrid.UltraGridGroup Group)
            {
                try
                {
                    //컬럼 Key, 컬럼명 지정(필수)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns.Add(strColKey, strColText);
                    //컬럼 Hidden여부  (기본값 = false)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Hidden = bolColHidden;
                    //컬럼 고정열 처리 (기본값 = false)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Fixed = bolUseFixedCol;

                    //컬럼 셀내 편집여부 (기본값 = AllowEdit)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellActivation = ColAllowEdit;
                    //편집불가(NoEdit)면 컬럼색을 변경.
                    if (ColAllowEdit == Infragistics.Win.UltraWinGrid.Activation.NoEdit)
                        Grid.DisplayLayout.Bands[0].Columns[strColKey].CellAppearance.BackColor = System.Drawing.Color.Gainsboro;

                    //컬럼 폭          (기본값 = 100)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Width = intColWidth;
                    //컬럼 필수항목 표시(기본값 = false)
                    if (bolColMandatory == true)
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.ForeColor = System.Drawing.Color.Red;

                    //컬럼 입력 최대 길이
                    if (intColMaxLength > 0)
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MaxLength = intColMaxLength;

                    //컬럼 셀내 정렬처리 (기본값 = Left)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextHAlign = ColHAlign;
                    //컬럼 셀내 정렬처리 (기본값 = Middle)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextVAlign = ColVAlign;

                    //컬럼 Merge 속성 지정 (기본값 = Never)
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MergedCellStyle = ColMergeStyle;

                    //컬럼 표시 유형
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Style = ColStyle;

                    //CheckBox일때 헤더에 CheckBox 처리
                    if (ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox)
                    {
                        //Display a checkbox in the column header, where column's data type isBoolean,DefaultableBoolean, or Nullable Boolean.
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxVisibility = HeaderCheckBoxVisibility.WhenUsingCheckEditor;

                        //Aligns the Header checkbox to the right of the Header caption
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxAlignment = HeaderCheckBoxAlignment.Center;

                        //The checkbox and the cell values are kept in synch to affect only the RowsCollection 
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.CheckBoxSynchronization = HeaderCheckBoxSynchronization.RowsCollection;

                        //체크박스열인 경우 필터기능을 안보이도록 처리한다.
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AllowRowFiltering = DefaultableBoolean.False;

                        //2단계 체크박스로 속성처리
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].DataType = typeof(Boolean);

                    }
                    //버튼일때 버튼 이미지
                    else if (ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton || ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.Button)
                    {
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.Image = Properties.Resources.btn_Zoom;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.ImageHAlign = HAlign.Center;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellButtonAppearance.ImageVAlign = VAlign.Middle;
                    }

                    //컬럼 : 날짜, 시간, 정수, 실수 인 경우 Format 지정
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Format = strColFormat;
                    ////컬럼 : 문자인 경우 Format 지정 yyyy-mm-dd / hh:mm:ss
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MaskInput = strColMaskInput;
                    //컬럼 : 행추가시 기본값 설정
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].DefaultCellValue = strColDefaultValue;

                    //컬럼 : Group Header에 대한 Merge 또는 포함할 Group 지정처리
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.OriginX = intOriginX;
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.OriginY = intOriginY;
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.SpanX = intSpanX;
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.SpanY = intSpanY+1;

                    ////////if (intSpanY > 1)
                    if (Group == null)
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.MinimumLabelSize = new Size(Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Width, Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Height * intSpanY);
                    else
                    //if (Group != null)
                    {
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.ParentGroup = Group;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.ParentGroup.CellAppearance.TextHAlign = HAlign.Center;
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.ParentGroup.CellAppearance.ForeColor = Color.Crimson;
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 그리드 컬럼에 콤보박스로 설정하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="intBandIndex"></param>
            /// <param name="strColKey"></param>
            /// <param name="ValueListStyle"></param>
            /// <param name="strTopKey"></param>
            /// <param name="strTopValue"></param>
            /// <param name="dtValueList"></param>
            public void mfSetGridColumnValueList(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                                 int intBandIndex,
                                                 string strColKey,
                                                 Infragistics.Win.ValueListDisplayStyle ValueListStyle,
                                                 string strTopKey,
                                                 string strTopValue,
                                                 System.Data.DataTable dtValueList)
            {
                try
                {
                    //string strValueListColKey = strColKey + "_List";
                    //int intSelectValueListValue = 0;

                    ////그리드에 ValueList Collection 추가
                    //Grid.DisplayLayout.ValueLists.Add(strValueListColKey);

                    ////ValueList의 Style 지정
                    //Grid.DisplayLayout.ValueLists[strValueListColKey].DisplayStyle = ValueListStyle;

                    ////Value List에 상단값 설정
                    //if (strTopKey != "" || strTopValue != "")
                    //    Grid.DisplayLayout.ValueLists[strValueListColKey].ValueListItems.Add(strTopKey, strTopValue);

                    ////추가적인 Value List 값 설정
                    //foreach (DataRow dr in dtValueList.Rows)
                    //{
                    //    Grid.DisplayLayout.ValueLists[strValueListColKey].ValueListItems.Add(dr[0].ToString(), dr[1].ToString());
                    //}

                    ////그리드 컬럼에 ValueList 지정            
                    //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = Grid.DisplayLayout.ValueLists[strValueListColKey];



                    Infragistics.Win.ValueList uValueList = new Infragistics.Win.ValueList();

                    ////ValueList의 Style 지정
                    uValueList.DisplayStyle = ValueListStyle;

                    // DropDown 리스트 각Item의 높이 설정
                    uValueList.ItemHeight = 15;
                    // Dropdown 리스트에 한번에 보여지는 최대 Item 갯수;
                    uValueList.MaxDropDownItems = 5;

                    // TopValue 설정
                    QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    string strChgTopVal = string.Empty;
                    switch (strTopValue)
                    {
                        case "전체":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "全部";
                                    break;
                                case "ENG":
                                    strChgTopVal = "ALL";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        case "선택":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "选择";
                                    break;
                                case "ENG":
                                    strChgTopVal = "Select";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        default:
                            strChgTopVal = strTopValue;
                            break;
                    }

                    //Value List에 상단값 설정
                    if (strTopKey != "" || strTopValue != "")
                        uValueList.ValueListItems.Add(strTopKey, strChgTopVal);

                    //추가적인 Value List 값 설정
                    for (int i = 0; i < dtValueList.Rows.Count; i++)
                    {
                        uValueList.ValueListItems.Add(dtValueList.Rows[i][0].ToString(), dtValueList.Rows[i][1].ToString());
                    }
                    uValueList.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                    //uValueList.FormatFilteredItems = DefaultableBoolean.True;
                    
                    //그리드 콤보에서 Like검색 속성지정
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
                    
                    //그리드 컬럼에 ValueList 지정 
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = uValueList;
                    
                    

                }
                catch (Exception ex)
                {
                }
                finally
                {
                }

            }

            /// <summary>
            /// 그리드 컬럼에 콤보박스로 설정하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="intBandIndex"></param>
            /// <param name="strColKey"></param>
            /// <param name="ValueListStyle"></param>
            /// <param name="strTopKey"></param>
            /// <param name="strTopValue"></param>
            /// <param name="dtValueList"></param>
            public void mfSetGridColumnValueGridList(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                                 int intBandIndex,
                                                 string strColKey,
                                                 Infragistics.Win.ValueListDisplayStyle ValueListStyle,
                                                 string strDropDownGridHKey,
                                                 string strDropDownGridHName,
                                                 string strKeyName,
                                                 string strValueName,
                                                 System.Data.DataTable dtValueList)
            {
                try
                {
                    string strValueListColKey = strColKey + "_List";

                    //그리드에 ValueList Collection 추가
                    Grid.DisplayLayout.ValueLists.Add(strValueListColKey);

                    //ValueList의 Style 지정
                    Grid.DisplayLayout.ValueLists[strValueListColKey].DisplayStyle = ValueListStyle;

                    //Value List에 상단값 설정
                    //if (strTopKey != "" || strTopValue != "")
                    //    Grid.DisplayLayout.ValueLists[strValueListColKey].ValueListItems.Add(strTopKey, strTopValue);

                    UltraDropDown uDropDown = new UltraDropDown();
                    
                    #region Grid DropDown 속성 지정
                    uDropDown.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsVista;
                    //헤더 정렬 지정
                    uDropDown.DisplayLayout.Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    uDropDown.DisplayLayout.Override.HeaderAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    //헤더 배경색 및 글짜색 지정
                    uDropDown.DisplayLayout.Override.HeaderAppearance.BackColor = System.Drawing.Color.FromArgb(106, 160, 206);
                    uDropDown.DisplayLayout.Override.HeaderAppearance.ForeColor = System.Drawing.Color.FromArgb(50, 50, 50);
                    uDropDown.DisplayLayout.Override.HeaderAppearance.FontData.Bold = DefaultableBoolean.True;
                    
                    //선택할 셀의 컬럼헤더 글자색 지정
                    uDropDown.DisplayLayout.Override.ActiveCellColumnHeaderAppearance.ForeColor = Color.DimGray;

                    //홀수줄 짝수줄 색깔 지정
                    uDropDown.DisplayLayout.Override.RowAppearance.BackColor = System.Drawing.Color.White;
                    uDropDown.DisplayLayout.Override.RowAlternateAppearance.BackColor = System.Drawing.Color.MintCream;

                    //그리드 행에 MouseOver시 색깔 지정 (요기 처리해야 함)
                    uDropDown.DisplayLayout.Override.HotTrackRowAppearance.BackColor = System.Drawing.Color.WhiteSmoke;
                    uDropDown.DisplayLayout.Override.HotTrackRowAppearance.BackGradientStyle = GradientStyle.Vertical;
                    uDropDown.DisplayLayout.Override.HotTrackRowAppearance.ForeColor = System.Drawing.Color.FromArgb(64, 64, 64);

                    //그리드 내부선 색 지정
                    uDropDown.DisplayLayout.Override.CellAppearance.BorderColor = System.Drawing.Color.FromArgb(125, 138, 158);
                    uDropDown.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Default;
                    uDropDown.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Default;

                    //스크롤이 최하단으로 내려갔을 때 빈공간이 없도록 설정 
                    uDropDown.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;

                    //그리드 컬럼 자동생성 해지 (기본값 = Hide)
                    uDropDown.DisplayLayout.NewColumnLoadStyle = NewColumnLoadStyle.Hide;
                    //uDropDown.DisplayLayout.Override.AllowRowFiltering = DefaultableBoolean.True
                    #endregion

                    //컬럼설정
                    char[] strSeparator = { ',' };
                    string[] arrKey = strDropDownGridHKey.Split(strSeparator);
                    string[] arrName = strDropDownGridHName.Split(strSeparator);
                    for (int i = 0; i < arrKey.Count(); i++)
                    {
                        uDropDown.DisplayLayout.Bands[0].Columns.Add(arrKey[i], arrName[i]);
                    }

                    uDropDown.Visible = false;
                    uDropDown.DataSource = dtValueList;
                    uDropDown.ValueMember = strKeyName;
                    uDropDown.DisplayMember = strValueName;
                    Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = uDropDown;
                    
                    //추가적인 Value List 값 설정
                    //foreach (DataRow dr in dtValueList.Rows)
                    //{
                    //    Grid.DisplayLayout.ValueLists[strValueListColKey].ValueListItems.Add(dr[0].ToString(), dr[1].ToString());
                    //}

                    ////그리드 컬럼에 ValueList 지정            
                    //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = Grid.DisplayLayout.ValueLists[strValueListColKey];
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }

            }


            /// <summary>
            /// 그리드 Cell에 콤보박스로 설정하는 함수
            /// </summary>
            /// <param name="Grid"></param>
            /// <param name="intRowIndex"></param>
            /// <param name="strColKey"></param>
            /// <param name="strTopKey"></param>
            /// <param name="strTopValue"></param>
            /// <param name="dtValueList"></param>
            public void mfSetGridCellValueList(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                                 int intRowIndex,
                                                 string strColKey,
                                                 string strTopKey,
                                                 string strTopValue,
                                                 System.Data.DataTable dtValueList)
            {
                try
                {
                    //int intSelectValueListValue = 0;
                    //Infragistics.Win.ValueList valList = new Infragistics.Win.ValueList();

                    ////Value List에 상단값 설정
                    //if (strTopKey != "" && strTopValue != "")
                    //    valList.ValueListItems.Add(strTopKey, strTopValue);

                    ////추가적인 Value List 값 설정
                    //foreach (DataRow dr in dtValueList.Rows)
                    //    valList.ValueListItems.Add(dr[0].ToString(), dr[1].ToString());

                    ////특정 Cell에 ValueList 할당
                    //Grid.Rows[intRowIndex].Cells[strColKey].ValueList = valList;


                    Infragistics.Win.ValueList uValueList = new Infragistics.Win.ValueList();

                    // DropDown 리스트 각Item의 높이 설정
                    uValueList.ItemHeight = 15;
                    // Dropdown 리스트에 한번에 보여지는 최대 Item 갯수;
                    uValueList.MaxDropDownItems = 5;

                    ////ValueList의 Style 지정
                    //uValueList.DisplayStyle = ValueListStyle;

                    // TopValue 설정
                    QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    string strChgTopVal = string.Empty;
                    switch (strTopValue)
                    {
                        case "전체":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "全部";
                                    break;
                                case "ENG":
                                    strChgTopVal = "ALL";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        case "선택":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "选择";
                                    break;
                                case "ENG":
                                    strChgTopVal = "Select";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        default:
                            strChgTopVal = strTopValue;
                            break;
                    }

                    //Value List에 상단값 설정
                    if (strTopKey != "" || strTopValue != "")
                        uValueList.ValueListItems.Add(strTopKey, strChgTopVal);

                    //추가적인 Value List 값 설정
                    for (int i = 0; i < dtValueList.Rows.Count; i++)
                    {
                        uValueList.ValueListItems.Add(dtValueList.Rows[i][0].ToString(), dtValueList.Rows[i][1].ToString());
                    }
                    uValueList.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                    //그리드 컬럼에 ValueList 지정 
                    Grid.Rows[intRowIndex].Cells[strColKey].ValueList = uValueList;

                }
                catch (Exception ex)
                {
                }
                finally
                {
                }

            }

            /// <summary>
            /// 그리드 Cell에 콤보박스로 설정하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="intBandIndex"></param>
            /// <param name="strColKey"></param>
            /// <param name="ValueListStyle"></param>
            /// <param name="strTopKey"></param>
            /// <param name="strTopValue"></param>
            /// <param name="dtValueList"></param>
            public void mfSetGridCellValueGridList(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                                 int intBandIndex,
                                                 int intRowIndex,
                                                 string strColKey,
                                                 Infragistics.Win.ValueListDisplayStyle ValueListStyle,
                                                 string strDropDownGridHKey,
                                                 string strDropDownGridHName,
                                                 string strKeyName,
                                                 string strValueName,
                                                 System.Data.DataTable dtValueList)
            {
                try
                {
                    #region 수정부분 %% ValueList 이름이 동일해서 바인딩이 안된 문제 수정

                    string strValueListColKey = "";
                    string strcol = "_List";
                    Boolean blCheck = false;
                    int intz = 0;
                    while (blCheck == false)
                    {
                        if (!Grid.DisplayLayout.ValueLists.Exists(strColKey + strcol))
                        {
                            strValueListColKey = strColKey + strcol;
                            blCheck = true;
                        }
                        intz += 1;
                        strcol += intz.ToString();
                    }
                    #endregion


                    //그리드에 ValueList Collection 추가
                    Grid.DisplayLayout.ValueLists.Add(strValueListColKey);

                    //ValueList의 Style 지정
                    Grid.DisplayLayout.ValueLists[strValueListColKey].DisplayStyle = ValueListStyle;

                    //Value List에 상단값 설정
                    //if (strTopKey != "" || strTopValue != "")
                    //    Grid.DisplayLayout.ValueLists[strValueListColKey].ValueListItems.Add(strTopKey, strTopValue);

                    UltraDropDown uDropDown = new UltraDropDown();

                    #region Grid DropDown 속성 지정
                    uDropDown.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsVista;
                    //헤더 정렬 지정
                    uDropDown.DisplayLayout.Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    uDropDown.DisplayLayout.Override.HeaderAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    //헤더 배경색 및 글짜색 지정
                    uDropDown.DisplayLayout.Override.HeaderAppearance.BackColor = System.Drawing.Color.FromArgb(106, 160, 206);
                    uDropDown.DisplayLayout.Override.HeaderAppearance.ForeColor = System.Drawing.Color.FromArgb(50, 50, 50);
                    uDropDown.DisplayLayout.Override.HeaderAppearance.FontData.Bold = DefaultableBoolean.True;

                    //선택할 셀의 컬럼헤더 글자색 지정
                    uDropDown.DisplayLayout.Override.ActiveCellColumnHeaderAppearance.ForeColor = Color.DimGray;

                    //홀수줄 짝수줄 색깔 지정
                    uDropDown.DisplayLayout.Override.RowAppearance.BackColor = System.Drawing.Color.White;
                    uDropDown.DisplayLayout.Override.RowAlternateAppearance.BackColor = System.Drawing.Color.MintCream;

                    //그리드 행에 MouseOver시 색깔 지정 (요기 처리해야 함)
                    uDropDown.DisplayLayout.Override.HotTrackRowAppearance.BackColor = System.Drawing.Color.WhiteSmoke;
                    uDropDown.DisplayLayout.Override.HotTrackRowAppearance.BackGradientStyle = GradientStyle.Vertical;
                    uDropDown.DisplayLayout.Override.HotTrackRowAppearance.ForeColor = System.Drawing.Color.FromArgb(64, 64, 64);

                    //그리드 내부선 색 지정
                    uDropDown.DisplayLayout.Override.CellAppearance.BorderColor = System.Drawing.Color.FromArgb(125, 138, 158);
                    uDropDown.DisplayLayout.Override.BorderStyleCell = UIElementBorderStyle.Default;
                    uDropDown.DisplayLayout.Override.BorderStyleRow = UIElementBorderStyle.Default;

                    //스크롤이 최하단으로 내려갔을 때 빈공간이 없도록 설정 
                    uDropDown.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;

                    //그리드 컬럼 자동생성 해지 (기본값 = Hide)
                    uDropDown.DisplayLayout.NewColumnLoadStyle = NewColumnLoadStyle.Hide;
                    #endregion

                    //컬럼설정
                    char[] strSeparator = { ',' };
                    string[] arrKey = strDropDownGridHKey.Split(strSeparator);
                    string[] arrName = strDropDownGridHName.Split(strSeparator);
                    for (int i = 0; i < arrKey.Count(); i++)
                    {
                        uDropDown.DisplayLayout.Bands[0].Columns.Add(arrKey[i], arrName[i]);
                    }

                    uDropDown.Visible = false;
                    uDropDown.DataSource = dtValueList;
                    uDropDown.ValueMember = strKeyName;
                    uDropDown.DisplayMember = strValueName;

                    Grid.Rows[intRowIndex].Cells[strColKey].ValueList = uDropDown;
                    //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = uDropDown;

                    //추가적인 Value List 값 설정
                    //foreach (DataRow dr in dtValueList.Rows)
                    //{
                    //    Grid.DisplayLayout.ValueLists[strValueListColKey].ValueListItems.Add(dr[0].ToString(), dr[1].ToString());
                    //}

                    ////그리드 컬럼에 ValueList 지정            
                    //Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = Grid.DisplayLayout.ValueLists[strValueListColKey];
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }

            }

            /// <summary>
            /// 그리드에 새로운 행을 추가하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            /// <param name="intBandIndex"></param>
            public void mfAddRowGrid(Infragistics.Win.UltraWinGrid.UltraGrid Grid, int intBandIndex)
            {
                try
                {
                    DataTable dtGrid = new DataTable("AddRow");
                    foreach (Infragistics.Win.UltraWinGrid.UltraGridColumn grdCol in Grid.DisplayLayout.Bands[intBandIndex].Columns)
                    {
                        if (grdCol.Key != "Check")
                        {
                            DataColumn grdAddRowCol = new DataColumn(grdCol.Key, grdCol.DataType);
                            dtGrid.Columns.Add(grdAddRowCol);
                        }
                    }
                    dtGrid.Rows.Clear();
                    DataRow dr;
                    dr = dtGrid.NewRow();

                    Grid.DataSource = dtGrid;
                    Grid.DataBind();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }
            
            /// <summary>
            /// 그리드의 체크박스 열에 대해 모두 Check처리하는 함수
            /// </summary>
            /// <param name="Grid"></param>
            /// <param name="intBandIndex"></param>
            /// <param name="strCheckBoxColName"></param>
            public void mfSetAllCheckedGridColumn(Infragistics.Win.UltraWinGrid.UltraGrid Grid, int intBandIndex, string strCheckBoxColName)
            {
                try
                {
                    for (int i = 0; i < Grid.Rows.Count; i++)
                        Grid.Rows[i].Cells[strCheckBoxColName].Value = true;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 그리드의 체크박스 열에 대해 모두 UnCheck처리하는 함수
            /// </summary>
            /// <param name="Grid"></param>
            /// <param name="intBandIndex"></param>
            /// <param name="strCheckBoxColName"></param>
            public void mfSetAllUnCheckedGridColumn(Infragistics.Win.UltraWinGrid.UltraGrid Grid, int intBandIndex, string strCheckBoxColName)
            {
                try
                {
                    for (int i = 0; i < Grid.Rows.Count; i++)
                        Grid.Rows[i].Cells[strCheckBoxColName].Value = false;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 그리드 정보를 Excel로 다운로드 처리하는 함수
            /// </summary>
            /// <param name="Grid">그리드 Object</param>
            public void mfDownLoadGridToExcel(Infragistics.Win.UltraWinGrid.UltraGrid Grid)
            {
                try
                {
                    //Excel Export 생성
                    Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter GridExport = new Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter();

                    System.IO.Stream streamFile = null;
                    System.Windows.Forms.SaveFileDialog saveFile = new SaveFileDialog();
                    string strSavePath;

                    saveFile.Filter = "excel files (*.xls)|*.xls"; //excel files (*.xlsx)|*.xlsx";
                    saveFile.FilterIndex = 2;
                    saveFile.RestoreDirectory = true;

                    //폴더 및 화일선택
                    if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        if ((streamFile = saveFile.OpenFile()) != null)
                        {
                            strSavePath = saveFile.FileName;
                            streamFile.Close();
                            //Excel로 Export 처리
                            GridExport.Export(Grid, strSavePath, Infragistics.Excel.WorkbookFormat.Excel97To2003);
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// Grid에 신규행 및 수정행에 대한 이미지표시를 Clear하는 함수
            /// </summary>
            /// <param name="Grid"></param>
            public void mfClearRowSeletorGrid(Infragistics.Win.UltraWinGrid.UltraGrid Grid)
            {
                try
                {
                    for (int i = 0; i < Grid.Rows.Count; i++)
                    {
                        Grid.Rows[i].RowSelectorAppearance.Image = null;
                        //RowSelector에 편집표시를 없애기 위함.
                        Grid.ActiveRow = Grid.Rows[i];
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// Grid의 행의 모든 Cell에 데이터가 있는지 여부를 체크하는 함수
            /// </summary>
            /// <param name="Grid"></param>
            /// <param name="intBandIndex"></param>
            /// <param name="intRowIndex"></param>
            /// <returns></returns>
            public bool mfCheckCellDataInRow(Infragistics.Win.UltraWinGrid.UltraGrid Grid, int intBandIndex, int intRowIndex)
            {
                try
                {
                    bool bolCheckCell = true;
                    for (int i = 0; i < Grid.DisplayLayout.Bands[intBandIndex].Columns.Count; i++)
                    {
                        if (Grid.Rows[intRowIndex].Cells[i].Value.ToString() != "" && 
                            Grid.DisplayLayout.Bands[intBandIndex].Columns[i].Key.ToString() != "Check" &&
                            Grid.DisplayLayout.Bands[intBandIndex].Columns[i].Key.ToString() != "UseFlag")
                        {
                            bolCheckCell = false;
                            break;
                        }
                    }
                    return bolCheckCell;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                }
            }

            /// <summary>
            /// Form내에 Grid컨트롤에 대해 컬럼의 폭 및 순서를 레지스트리에 저장함.
            /// </summary>
            /// <param name="frm"></param>
            public void mfSaveGridColumnProperty(Form frm)
            {
                try
                {
                    foreach (Control ctrl in frm.Controls)
                    {
                        if (ctrl.GetType().ToString() == "Infragistics.Win.UltraWinGrid.UltraGrid")
                        {
                            UltraGrid grd = (UltraGrid)ctrl;
                            //RegistryKey 쓰기//
                            RegistryKey reg = Registry.LocalMachine.CreateSubKey("Software").CreateSubKey("QRPPRO").CreateSubKey(frm.Name.ToString() + "_" + grd.Name.ToString());
                            for (int i = 0; i < grd.DisplayLayout.Bands[0].Columns.Count; i++)
                            {
                                reg.SetValue(grd.DisplayLayout.Bands[0].Columns[i].Key.ToString(), grd.DisplayLayout.Bands[0].Columns[i].Width.ToString() + "|" + grd.DisplayLayout.Bands[0].Columns[i].Header.VisiblePosition.ToString());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
                finally
                {
                }
            }

            /// <summary>
            /// Form내에 Grid컨트롤에 대해 저장된 컬럼의 폭 및 순서를 적용함.
            /// </summary>
            /// <param name="frm"></param>
            public void mfLoadGridColumnProperty(Form frm)
            {
                try
                {
                    foreach (Control ctrl in frm.Controls)
                    {
                        if (ctrl.GetType().ToString() == "Infragistics.Win.UltraWinGrid.UltraGrid")
                        {
                            UltraGrid grd = (UltraGrid)ctrl;
                            //RegistryKey 읽기//
                            RegistryKey reg = Registry.LocalMachine.CreateSubKey("Software").CreateSubKey("QRPPRO").CreateSubKey(frm.Name.ToString() + "_" + grd.Name.ToString());
                            char[] strSeparator = { '|' };
                            for (int i = 0; i < grd.DisplayLayout.Bands[0].Columns.Count; i++)
                            {
                                string[] arrRegValue = Convert.ToString(reg.GetValue(grd.DisplayLayout.Bands[0].Columns[i].Key.ToString())).Split(strSeparator);
                                if (arrRegValue.Count() > 0)
                                {
                                    grd.DisplayLayout.Bands[0].Columns[grd.DisplayLayout.Bands[0].Columns[i].Key.ToString()].Width = Convert.ToInt32(arrRegValue[0]);
                                    grd.DisplayLayout.Bands[0].Columns[grd.DisplayLayout.Bands[0].Columns[i].Key.ToString()].Header.VisiblePosition = Convert.ToInt32(arrRegValue[1]);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 그리드 컬럼의 열Width을 열내용에 따라 자동 조절
            /// </summary>
            /// <param name="Grid"></param>
            /// <param name="intBandIndex"></param>
            public void mfSetAutoResizeColWidth(Infragistics.Win.UltraWinGrid.UltraGrid Grid, int intBandIndex)
            {
                try
                {
                    for (int i = 0; i < Grid.DisplayLayout.Bands[intBandIndex].Columns.Count; i++)
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[i].Width =
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[i].CalculateAutoResizeWidth(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.AllRowsInBand, true);
                        //Grid.DisplayLayout.Bands[intBandIndex].Columns[i].CalculateAutoResizeWidth(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.VisibleRows, true);
                        Grid.DisplayLayout.Bands[intBandIndex].Columns[i].MinWidth =
                            Grid.DisplayLayout.Bands[intBandIndex].Columns[i].CalculateAutoResizeWidth(Infragistics.Win.UltraWinGrid.PerformAutoSizeType.VisibleRows, true);

                    Grid.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;

                }
                catch (System.Exception ex)
                {
                	
                }
                finally
                {
                }
            }

            /// <summary>
            /// 최대 계층레벨을 구한다.
            /// </summary>
            /// <param name="dtOriginalData"></param>
            /// <param name="ParentColName"></param>
            /// <returns></returns>
            private int CalculateLevel(DataTable dtOriginalData, string ParentColName)
            {
                int intHighestLevel = 0;
                try
                {
                    //' pass all rows in DataTable
                    foreach (DataRow thisRow in dtOriginalData.Rows)
                    {
                        //' recursively look up the tree and count the levels
                        int intLevel = 0;
                        string strlookID = thisRow[ParentColName].ToString();
                        if (strlookID == null) strlookID = "";
                        while (strlookID != "")
                        {
                            //' find parent
                            DataRow parentRow;
                            object[] strFind = new object[1];
                            strFind[0] = strlookID;
                            parentRow = dtOriginalData.Rows.Find(strFind);
                            //' set to look for parent or throw error if no parent found
                            if (parentRow == null)
                                //MessageBox.Show("No Parent Found for " + lookID.ToString)
                                strlookID = ""; //null;
                            else
                            {
                                strlookID = parentRow[ParentColName].ToString();
                                intLevel = intLevel + 1;
                            }
                        }
                        //' apply level to this row and set Highest Level
                        thisRow["HierarchyLevel"] = intLevel;
                        if (intLevel > intHighestLevel) intHighestLevel = intLevel;
                    }
                    //' return Highest Level to caller
                    return intHighestLevel;
                }
                catch (System.Exception ex)
                {
                    return intHighestLevel;
                }
                finally
                {
                }
            }


            /// <summary>
            /// 계층형태 Grid를 표시하기 위한 DataSet 생성
            /// </summary>
            /// <param name="dtOriginalData"></param>
            /// <param name="strCodeColName"></param>
            /// <param name="strParentColName"></param>
            /// <returns></returns>
            public DataSet mfCreateHierarchicalDataSet(DataTable dtOriginalData, string strCodeColName, string strParentColName)
            {
                //' declare a new DataSet
                DataSet dsHierarchicalDataSet = new DataSet("HierarchicalDataSet");
                try
                {
                    char[] spliter = { '|' };
                    string[] arrPk = strCodeColName.Split(spliter);
                    string[] arrParent = strParentColName.Split(spliter);
                    //
                    DataColumn[] orgKeys = new DataColumn[arrPk.Length];
                    for (int i = 0; i < arrPk.Length; i++)
                    {
                        orgKeys[i] = dtOriginalData.Columns[arrPk[i]];
                    }
                    dtOriginalData.PrimaryKey = orgKeys;

                    //' add "level" column to originalDataTable
                    DataColumn dclevelColumn = new DataColumn("HierarchyLevel");
                    dclevelColumn.DataType = typeof(Int32);
                    dtOriginalData.Columns.Add(dclevelColumn);

                    ////' calculate hierarchy level
                    int intHighestLevel = CalculateLevel(dtOriginalData, strParentColName);

                    //' pass DataTable for each hierarchical level
                    int intLevel = 0;
                    for (intLevel = 0; intLevel <= intHighestLevel; intLevel++)
                    {
                        //' pass DataTable and extract rows for this level
                        DataRow[] drLevelDataRows;
                        drLevelDataRows = dtOriginalData.Select("HierarchyLevel=" + intLevel.ToString());
                        if (drLevelDataRows != null)
                        {
                            DataTable dtLevelDataTable = new DataTable();
                            dtLevelDataTable = dtOriginalData.Clone();
                            dtLevelDataTable.TableName = "HierarchyLevel" + intLevel.ToString();

                            foreach (DataRow dataRow in drLevelDataRows)
                            {
                                DataRow newDataRow = dtLevelDataTable.NewRow();
                                foreach (DataColumn dataColumn in dtOriginalData.Columns)
                                {
                                    newDataRow[dataColumn.ColumnName] = dataRow[dataColumn.ColumnName];
                                }
                                dtLevelDataTable.Rows.Add(newDataRow);
                            }
                            //' create a primary key and add to the data table
                            DataColumn[] Keys = new DataColumn[arrPk.Length];
                            for (int i = 0; i < arrPk.Length; i++)
                            {
                                Keys[i] = dtLevelDataTable.Columns[arrPk[i]];
                            }

                            dtLevelDataTable.PrimaryKey = Keys;

                            //' add table to DataSet
                            dsHierarchicalDataSet.Tables.Add(dtLevelDataTable);


                            //' if this is >= level 1 then create and add relationship
                            if (intLevel >= 1)
                            {
                                int intParentLevel = intLevel - 1;
                                string strThisLevel = "HierarchyLevel" + intLevel.ToString();
                                string strParentLevel = "HierarchyLevel" + intParentLevel.ToString();

                                DataColumn[] dcParent = new DataColumn[arrPk.Length];
                                for (int i = 0; i < arrPk.Length; i++)
                                {
                                    dcParent[i] = dsHierarchicalDataSet.Tables[strParentLevel].Columns[arrPk[i]];
                                }
                                DataColumn[] dcChild = new DataColumn[arrParent.Length];
                                for (int i = 0; i < arrParent.Length; i++)
                                {
                                    dcChild[i] = dsHierarchicalDataSet.Tables[strThisLevel].Columns[arrParent[i]];
                                }
                                DataRelation relLevel = new DataRelation(strThisLevel
                                                                         , dcParent
                                                                         , dcChild, true);
                                relLevel.Nested = true;
                                dsHierarchicalDataSet.Relations.Add(relLevel);
                            }
                        }
                    }
                    return dsHierarchicalDataSet;
                }
                catch (System.Exception ex)
                {
                    return dsHierarchicalDataSet;
                }
                finally
                {
                }
            }

        }
        #endregion

        #region UltraLabel Control 설정
        public class WinLabel
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="Label"></param>
            /// <param name="strLabelText"></param>
            /// <param name="strFontName"></param>
            public void mfSetLabel(Infragistics.Win.Misc.UltraLabel Label,
                                   string strLabelText,
                                   string strFontName, 
                                   bool bolUseTitleImage,
                                   bool bolMandantoryFlag)
            {
                try
                {
                    Label.Appearance.BackColor = System.Drawing.Color.GhostWhite; //System.Drawing.Color.Transparent;
                    Label.Appearance.BorderColor = System.Drawing.Color.GhostWhite; //System.Drawing.Color.Transparent;
                    Label.Appearance.BorderColor2 = System.Drawing.Color.Lavender;

                    Label.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded3;
                    Label.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.RaisedSoft;

                    Label.Text = strLabelText;
                    Label.Appearance.FontData.Name = strFontName;
                    Label.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                    Label.Appearance.TextVAlign = VAlign.Middle;
                    Label.Appearance.TextHAlign = HAlign.Left;

                    //label앞에 이미지 사용여부
                    if (bolUseTitleImage == true)
                    {
                        ImageList img = new ImageList();
                        img.Images.Add("TITLE", Properties.Resources.lbl_title);
                        Label.Appearance.Image = img.Images["TITLE"];
                        Label.ImageSize = new Size(6, 6);
                        Label.Appearance.ImageVAlign = VAlign.Middle;
                    }
                    
                    //필수입력항목에 대한 Label여부
                    if (bolMandantoryFlag == true)
                    {
                        Label.Appearance.FontData.Bold = DefaultableBoolean.True;
                        Label.Appearance.ForeColor = System.Drawing.Color.DarkRed;
                    }
                    
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }
        }
        #endregion

        #region UltraComboEditor Control 설정 (1열 콤보박스)
        public class WinComboEditor
        {
            /// <summary>
            /// 콤보에디터 함수: DataTable 이용한 입력
            /// </summary>
            /// <param name="ComboEditor">UltraComboEditor Object</param>
            /// <param name="bolMandatory">필수여부 선택</param>
            /// <param name="bolReadOnly">콤보박스에서 글씨수정 여부</param>
            /// <param name="DisplayStyle">콤보박스 DisplayStyle 설정</param>
            /// <param name="strFontName">콤보박스 폰트</param>
            /// <param name="bolSuggest">Suggest 사용여부 선택</param>
            /// <param name="bolUseMultiSelect">콤보박스 다중선택 여부</param>
            /// <param name="strMultiValueSeparator">콤보박스 다중선택값 구분자</param>
            /// <param name="bolDropDownResize">콤보 Drop-Down 사이즈 조정여부</param>
            /// <param name="DisplayStyle">콤보 Drop-Down 사이즈 조정 Indicator 모양</param>
            /// <param name="bolAutoDropDownWidth">콤보 Drop-Down 폭 자동조절 여부</param>
            /// <param name="intDropDownListWidth">콤보 Drop-Down 폭</param>
            /// <param name="HAlign">콤보박스 및 Drop-Down 텍스트 정렬</param>
            /// <param name="strDefaultValue">콤보에디터의 기본 값</param>
            /// <param name="strTopKey">콤보에디터의 최상값(Key)</param>
            /// <param name="strTopValue">콤보에디터의 최상값(Text)</param>
            /// <param name="strKeyRow">Key값이 될 필드</param>
            /// <param name="strValueRow">Value값이 될 필드</param>
            /// <param name="dtTable">데이터 테이블</param>
            public void mfSetComboEditor(Infragistics.Win.UltraWinEditors.UltraComboEditor ComboEditor,
                                         bool bolMandatory,
                                         bool bolReadOnly,
                                         Infragistics.Win.EmbeddableElementDisplayStyle DisplayStyle,
                                         string strFontName,
                                         bool bolSuggest,
                                         bool bolUseMultiSelect,
                                         string strMultiValueSeparator,
                                         bool bolDropDownResize,
                                         Infragistics.Win.DropDownResizeHandleStyle ResizeHandleStyle,
                                         bool bolAutoDropDownWidth,
                                         int intDropDownListWidth,
                                         Infragistics.Win.HAlign TextHAlign,
                                         string strDefaultValue,
                                         string strTopKey,
                                         string strTopValue,
                                         string strKeyRow,
                                         string strValueRow,
                                         System.Data.DataTable dtTable)
            {
                try
                {
                    //ComboEditor초기화
                    ComboEditor.ValueList.Reset();

                    //필수여부
                    if (bolMandatory == true)
                        //ComboEditor.Appearance.BackColor = System.Drawing.Color.LavenderBlush;
                        ComboEditor.Appearance.BackColor = System.Drawing.Color.PowderBlue;
                    else
                        ComboEditor.Appearance.BackColor = System.Drawing.Color.White;

                    // 콤보박스에서 글씨수정 여부
                    if (bolReadOnly == true)
                        ComboEditor.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
                    else
                        ComboEditor.DropDownStyle = Infragistics.Win.DropDownStyle.DropDown;

                    // 콤보에디터 스타일 //
                    ComboEditor.DisplayStyle = DisplayStyle;

                    // 콤보에디터 폰트 지정 //
                    ComboEditor.Appearance.FontData.Name = strFontName;

                    // Suggest 사용 여부
                    if (bolSuggest == true)
                    {
                        ComboEditor.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
                        ComboEditor.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
                    }
                    else
                    {
                        ComboEditor.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
                        ComboEditor.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Default;
                    }

                    // 멀티선택가능 설정 //
                    if (bolUseMultiSelect == true)
                    {
                        ComboEditor.CheckedListSettings.CheckBoxStyle = Infragistics.Win.CheckStyle.CheckBox;
                        ComboEditor.CheckedListSettings.CheckBoxAlignment = System.Drawing.ContentAlignment.MiddleCenter;
                        ComboEditor.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
                        ComboEditor.CheckedListSettings.ListSeparator = strMultiValueSeparator + " ";
                        ComboEditor.CheckedListSettings.ItemCheckArea = Infragistics.Win.ItemCheckArea.CheckBox;
                    }

                    // 콤보박스 Drop-Down Resize 여부
                    if (bolDropDownResize == true)
                    {
                        ComboEditor.ValueList.DropDownResizeHandleStyle = ResizeHandleStyle;
                        ComboEditor.ValueList.DropDownResizeHandleAppearance.BackColor = System.Drawing.Color.Blue;
                    }

                    // 콤보박스 Drop-Down 폭 지정
                    if (bolAutoDropDownWidth == false)
                        ComboEditor.DropDownListWidth = intDropDownListWidth;


                    // 콤보박스 Text에 대한 수평정렬
                    ComboEditor.Appearance.TextHAlign = TextHAlign;
                    ComboEditor.ValueList.Appearance.TextHAlign = TextHAlign;

                    //상단값 DataTable에 저장처리하여 콤보박스 상단에 위치시킴//
                    System.Data.DataTable dt = new DataTable();
                    System.Data.DataColumn dcKey = new DataColumn(strKeyRow, typeof(string));
                    System.Data.DataColumn dcValue = new DataColumn(strValueRow, typeof(string));
                    dt.Columns.Add(dcKey);
                    dt.Columns.Add(dcValue);

                    // 데이터 테이블 생성//
                    DataRow dr;
                    dr = dt.NewRow();
                    dr[strKeyRow] = strTopKey;

                    // TopValue 설정
                    QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    string strChgTopVal = string.Empty;
                    switch (strTopValue)
                    {
                        case "전체":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "全部";
                                    break;
                                case "ENG":
                                    strChgTopVal = "ALL";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        case "선택":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "选择";
                                    break;
                                case "ENG":
                                    strChgTopVal = "Select";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        default:
                            strChgTopVal = strTopValue;
                            break;
                    }

                    dr[strValueRow] = strChgTopVal;
                    dt.Rows.Add(dr);
                    ComboEditor.Items.Add(dt.Rows[0][strKeyRow].ToString(), dt.Rows[0][strValueRow].ToString());

                    //생성된 데이터 테이블에 데이터 매칭//
                    foreach (System.Data.DataRow drItem in dtTable.Rows)
                    {
                        ComboEditor.Items.Add(drItem[strKeyRow].ToString(), drItem[strValueRow].ToString());
                    }

                    // 기본값 설정//
                    ComboEditor.Value = strDefaultValue;

                    // 리스트 아이템 Height 지정//
                    ComboEditor.ValueList.ItemHeight = 20;

                    ComboEditor.Appearance.FontData.SizeInPoints = 9;
                    ComboEditor.ValueList.Appearance.FontData.SizeInPoints = 9;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 콤보에디터 함수: DataTable 이용한 입력
            /// </summary>
            /// <param name="ComboEditor">UltraComboEditor Object</param>
            /// <param name="bolMandatory">필수여부 선택</param>
            /// <param name="bolReadOnly">콤보박스에서 글씨수정 여부</param>
            /// <param name="DisplayStyle">콤보박스 DisplayStyle 설정</param>
            /// <param name="strFontName">콤보박스 폰트</param>
            /// <param name="bolSuggest">Suggest 사용여부 선택</param>
            /// <param name="bolUseMultiSelect">콤보박스 다중선택 여부</param>
            /// <param name="strMultiValueSeparator">콤보박스 다중선택값 구분자</param>
            /// <param name="bolDropDownResize">콤보 Drop-Down 사이즈 조정여부</param>
            /// <param name="DisplayStyle">콤보 Drop-Down 사이즈 조정 Indicator 모양</param>
            /// <param name="bolAutoDropDownWidth">콤보 Drop-Down 폭 자동조절 여부</param>
            /// <param name="intDropDownListWidth">콤보 Drop-Down 폭</param>
            /// <param name="HAlign">콤보박스 및 Drop-Down 텍스트 정렬</param>
            /// <param name="strDefaultValue">콤보에디터의 기본 값</param>
            /// <param name="arrKeyRows">콤보박스에 적용할 Key값 배열 </param>
            /// <param name="arrValueRows">콤보박스에 적용할 Text값 배열 </param>
            public void mfSetComboEditor(Infragistics.Win.UltraWinEditors.UltraComboEditor ComboEditor,
                                         bool bolMandatory,
                                         bool bolReadOnly,
                                         Infragistics.Win.EmbeddableElementDisplayStyle DisplayStyle,
                                         string strFontName,
                                         bool bolSuggest,
                                         bool bolUseMultiSelect,
                                         string strMultiValueSeparator,
                                         bool bolDropDownResize,
                                         Infragistics.Win.DropDownResizeHandleStyle ResizeHandleStyle,
                                         bool bolAutoDropDownWidth,
                                         int intDropDownListWidth,
                                         Infragistics.Win.HAlign TextHAlign,
                                         string strDefaultValue,
                                         System.Collections.ArrayList arrKeyRows,
                                         System.Collections.ArrayList arrValueRows)
            {
                try
                {
                    //필수여부
                    if (bolMandatory == true)
                        //ComboEditor.Appearance.BackColor = System.Drawing.Color.LavenderBlush;
                        ComboEditor.Appearance.BackColor = System.Drawing.Color.PowderBlue;
                    else
                        ComboEditor.Appearance.BackColor = System.Drawing.Color.White;

                    // 콤보박스에서 글씨수정 여부
                    if (bolReadOnly == true)
                        ComboEditor.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
                    else
                        ComboEditor.DropDownStyle = Infragistics.Win.DropDownStyle.DropDown;

                    // 콤보에디터 스타일 //
                    ComboEditor.DisplayStyle = DisplayStyle;

                    // 콤보에디터 폰트 지정 //
                    ComboEditor.Appearance.FontData.Name = strFontName;

                    // Suggest 사용 여부
                    if (bolSuggest == true)
                    {
                        ComboEditor.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;
                        ComboEditor.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
                    }
                    else
                    {
                        ComboEditor.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
                        ComboEditor.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Default;
                    }

                    // 멀티선택가능 설정 //
                    if (bolUseMultiSelect == true)
                    {
                        ComboEditor.CheckedListSettings.CheckBoxStyle = Infragistics.Win.CheckStyle.CheckBox;
                        ComboEditor.CheckedListSettings.CheckBoxAlignment = System.Drawing.ContentAlignment.MiddleCenter;
                        ComboEditor.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
                        ComboEditor.CheckedListSettings.ListSeparator = strMultiValueSeparator + " ";
                        ComboEditor.CheckedListSettings.ItemCheckArea = Infragistics.Win.ItemCheckArea.CheckBox;
                    }

                    // 콤보박스 Drop-Down Resize 여부
                    if (bolDropDownResize == true)
                    {
                        ComboEditor.ValueList.DropDownResizeHandleStyle = ResizeHandleStyle;
                        ComboEditor.ValueList.DropDownResizeHandleAppearance.BackColor = System.Drawing.Color.Blue;
                    }

                    // 콤보박스 Drop-Down 폭 지정
                    if (bolAutoDropDownWidth == false)
                        ComboEditor.DropDownListWidth = intDropDownListWidth;


                    // 콤보박스 Text에 대한 수평정렬
                    ComboEditor.Appearance.TextHAlign = TextHAlign;
                    ComboEditor.ValueList.Appearance.TextHAlign = TextHAlign;

                    //상단값 DataTable에 저장처리하여 콤보박스 상단에 위치시킴//
                    //string strKeyRow = "Key";
                    //string strValueRow = "Value";

                    //DataTable dt = new DataTable();
                    //DataColumn dcKey = new DataColumn(strKeyRow, typeof(string));
                    //DataColumn dcValue = new DataColumn(strValueRow, typeof(string));
                    //dt.Columns.Add(dcKey);
                    //dt.Columns.Add(dcValue);

                    // 데이터 테이블 생성//
                    //DataRow dr;
                    //dr = dt.NewRow();
                    //dr[strKeyRow] = strTopKey;
                    //dr[strValueRow] = strTopValue;
                    //dt.Rows.Add(dr);
                    //ComboEditor.Items.Add(dt.Rows[0][strKeyRow].ToString(), dt.Rows[0][strValueRow].ToString());

                    //생성된 데이터 테이블에 데이터 매칭//
                    for (int idx = 0; idx < arrKeyRows.Count; idx++)
                    {
                        ComboEditor.Items.Add(arrKeyRows[idx].ToString(), arrValueRows[idx].ToString());
                    }

                    // 기본값 설정//
                    ComboEditor.Value = strDefaultValue;

                    // 리스트 아이템 Height 지정//
                    ComboEditor.ValueList.ItemHeight = 20;

                    ComboEditor.Appearance.FontData.SizeInPoints = 9;
                    ComboEditor.ValueList.Appearance.FontData.SizeInPoints = 9;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }
        }
        #endregion

        #region UltraCombo Control 설정 (그리드 콤보박스)
        public class WinComboGrid
        {
            /// <summary>
            /// 콤보 그리드 초기화하는 함수
            /// </summary>
            /// <param name="ComboBox">ComboBox Object</param>
            /// <param name="bolMandatory">필수여부 선택</param>
            /// <param name="bolReadOnly">콤보박스에서 글씨수정 여부</param>
            /// <param name="bolSuggest">Suggest 사용여부</param>
            /// <param name="bolDropDownResize">콤보 Drop-Down 사이즈 조정여부</param>
            /// <param name="strValueMember">데이터 테이블의 키 값</param>
            /// <param name="strDisplayMember">데이터테이블의 텍스트 값</param>
            /// <param name="strDefaultValue">콤보박스의 기본값</param>
            /// <param name="DisplayStyle">콤보박스 DisplayStyle 설정</param>
            /// <param name="bolHiddenTopGroupBox">그리드 상단의 GroupBox 안보이게 처리 여부</param>
            /// <param name="ColAutoLoadStyle">그리드 컬럼에 대해 Binding시 자동생성처리 Style</param>
            /// <param name="ColAutoFitStyle">그리드 컬럼 자동 설정</param>
            /// <param name="bolCaptionView">그리드 상단 Caption 보이게 처리하는지 여부</param>
            /// <param name="MergedCellStyle">그리드 셀에 대한 Merget Styple</param>
            /// <param name="bolUseFixedHeader">그리드 고정열 사용여부</param>
            /// <param name="FixedIndicator">그리드 고정열에 대한 Header셀에 표시형태</param>
            /// <param name="ColSelectType">그리드 컬럼선택 유형</param>
            /// <param name="bolColUseFilter">그리드 컬럼의 Filter사용 여부</param>
            /// <param name="FilterIndicator">그리드 Filter열에 대한 Header셀에 표시형태</param>
            /// <param name="intHeaderTextOrientDegree">그리드 Header 텍스트에 대한 기울기</param>
            /// <param name="bolUseMultiLine">그리드 표시내용에 대한 다중라인 사용여부</param>
            /// <param name="strFontName">그리드 폰트</param>
            public void mfInitGeneralComboGrid(Infragistics.Win.UltraWinGrid.UltraCombo ComboBox,
                                               bool bolMandatory,
                                               bool bolReadOnly,
                                               bool bolSuggest,
                                               bool bolDropDownResize,
                                               string strValueMember,
                                               string strDisplayMember,
                                               string strDefaultValue,
                                               Infragistics.Win.EmbeddableElementDisplayStyle DisplayStyle,
                                               bool bolHiddenTopGroupBox,
                                               Infragistics.Win.UltraWinGrid.NewColumnLoadStyle ColAutoLoadStyle,
                                               Infragistics.Win.UltraWinGrid.AutoFitStyle ColAutoFitStyle,
                                               Infragistics.Win.DefaultableBoolean bolCaptionView,
                                               Infragistics.Win.UltraWinGrid.MergedCellStyle MergedCellStyle,
                                               bool bolUseFixedHeader,
                                               Infragistics.Win.UltraWinGrid.FixedHeaderIndicator FixedIndicator,
                                               Infragistics.Win.UltraWinGrid.SelectType ColSelectType,
                                               Infragistics.Win.DefaultableBoolean bolColUseFilter,
                                               Infragistics.Win.UltraWinGrid.FilterUIType FilterIndicator,
                                               int intHeaderTextOrientDegree,
                                               bool bolUseMultiLine,
                                               string strFontName)
            {
                try
                {
                    //필수여부
                    if (bolMandatory == true)
                        ComboBox.Appearance.BackColor = System.Drawing.Color.LavenderBlush;
                    else
                        ComboBox.Appearance.BackColor = System.Drawing.Color.White;

                    // 콤보박스에서 글씨수정 여부
                    if (bolReadOnly == true)
                        ComboBox.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDownList;
                    else
                        ComboBox.DropDownStyle = Infragistics.Win.UltraWinGrid.UltraComboStyle.DropDown;

                    // Suggest 사용 여부//
                    if (bolSuggest == true)
                    {
                        ComboBox.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Default;
                        ComboBox.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Default;
                    }
                    else
                    {
                        ComboBox.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Default;
                        ComboBox.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Default;
                    }

                    //Drop-Down 리스트 크기 조절
                    if (bolDropDownResize == true)
                    {
                        ComboBox.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.DiagonalResize;
                        ComboBox.DropDownResizeHandleAppearance.BackColor = System.Drawing.Color.Blue;
                    }

                    //Value, Text 멤버 설정//
                    ComboBox.ValueMember = strValueMember;
                    ComboBox.DisplayMember = strDisplayMember;

                    //기본값 성정//
                    ComboBox.Value = strDefaultValue;

                    // 콤보에디터 스타일 //
                    ComboBox.DisplayStyle = DisplayStyle;

                    //그리드 컬럼 Clear시킴
                    ComboBox.DisplayLayout.ClearGroupByColumns();
                    //그리드 상단의 GroupBox 표시여부 (기본값 = true)
                    ComboBox.DisplayLayout.GroupByBox.Hidden = bolHiddenTopGroupBox;
                    //그리드 컬럼 자동생성 해지 (기본값 = Hide)
                    ComboBox.DisplayLayout.NewColumnLoadStyle = ColAutoLoadStyle;
                    //그리드 컬럼 폭크기 자동 변경처리 여부 (기본값 = None)
                    ComboBox.DisplayLayout.AutoFitStyle = ColAutoFitStyle;

                    //그리드 편집여부 (기본값 = false)
                    ComboBox.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.CellSelect;

                    //그리드 캡션 안보이게 설정 (기본값 = False)
                    ComboBox.DisplayLayout.CaptionVisible = bolCaptionView;
                    //그리드 전체 Cell Merge 설정 (기본값 = Never)
                    ComboBox.DisplayLayout.Override.MergedCellStyle = MergedCellStyle;
                    //그리드 고정열 사용여부 지정 (기본값 = true)
                    ComboBox.DisplayLayout.UseFixedHeaders = bolUseFixedHeader;

                    //그리드 고정열 헤더표시 모양 지정 (기본값 = Default)
                    if (bolUseFixedHeader == true)
                        ComboBox.DisplayLayout.Override.FixedHeaderIndicator = FixedIndicator;
                    else
                        ComboBox.DisplayLayout.Override.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None;

                    //그리드 헤더컬럼을 선택시 Sorting처리여부 (기본값 = None)
                    ComboBox.DisplayLayout.Override.SelectTypeCol = ColSelectType;
                    if (ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.None || ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.Default)
                        ComboBox.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;
                    else if (ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.Single || ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.SingleAutoDrag)
                        ComboBox.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;
                    else if (ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.Extended || ColSelectType == Infragistics.Win.UltraWinGrid.SelectType.ExtendedAutoDrag)
                        ComboBox.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortMulti;

                    //그리드 Filter 기능 여부 (기본값 = false)
                    ComboBox.DisplayLayout.Override.AllowRowFiltering = bolColUseFilter;   //기본값 = false
                    if (bolColUseFilter == Infragistics.Win.DefaultableBoolean.True || bolColUseFilter == Infragistics.Win.DefaultableBoolean.Default)
                        ComboBox.DisplayLayout.Override.FilterUIType = FilterIndicator;         //기본값 = Default

                    //그리드 헤더컬럼 Text회전 처리 (기본값 = 0)
                    ComboBox.DisplayLayout.Override.ColumnHeaderTextOrientation = new TextOrientationInfo(intHeaderTextOrientDegree, TextFlowDirection.Horizontal);

                    //셀 내용에 대해 Multi-Line 여부 (기본값 = false)
                    if (bolUseMultiLine == true)
                    {
                        ComboBox.DisplayLayout.Override.CellMultiLine = Infragistics.Win.DefaultableBoolean.True; //셀 내용에 대해 Multi-Line 여부 (기본값 = false)                
                        ComboBox.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
                        ComboBox.DisplayLayout.Override.RowSizing = Infragistics.Win.UltraWinGrid.RowSizing.AutoFree;
                    }

                    ComboBox.DisplayLayout.Override.HeaderAppearance.FontData.Name = strFontName;
                    ComboBox.DisplayLayout.Override.CellAppearance.FontData.Name = strFontName;
                    
                    ComboBox.Appearance.FontData.SizeInPoints = 9;

                    //그리드 속성에서 고정값으로 속성을 설정
                    mfSetFixedPropertyOfComboGrid(ComboBox);
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 콤보 그리드 속성에서 고정값으로 속성을 설정하는 함수
            /// </summary>
            /// <param name="ComboBox">ComboBox Object</param>
            public void mfSetFixedPropertyOfComboGrid(Infragistics.Win.UltraWinGrid.UltraCombo ComboBox)
            {
                try
                {
                    //////// (고정 속성값 처리) ////////
                    //헤더 스타일 지정
                    ComboBox.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsVista;

                    //행 선택에 대한 Style처리
                    ComboBox.DisplayLayout.Override.RowSelectorStyle = Infragistics.Win.HeaderStyle.WindowsVista;

                    //그리드 Font 지정(기본값 = 돋움, 10)
                    ComboBox.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 10;
                    ComboBox.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 10;

                    //헤더 정렬 지정
                    ComboBox.DisplayLayout.Override.HeaderAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
                    ComboBox.DisplayLayout.Override.HeaderAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                    //헤더 배경색 지정
                    //Grid.DisplayLayout.Override.HeaderAppearance.BackColor = Color.SteelBlue;
                    //Grid.DisplayLayout.Override.HeaderAppearance.BackColor2 = Color.SteelBlue;

                    //홀수줄 색깔 지정
                    ComboBox.DisplayLayout.Override.RowAlternateAppearance.BackColor = Color.Snow;
                    //System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(255)));
                    ComboBox.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007;
                    ComboBox.DisplayLayout.Override.CellAppearance.BorderColor = System.Drawing.Color.Red;

                    //Tab Key를 누르면 다음 셀로 이동처리
                    ComboBox.DisplayLayout.TabNavigation = Infragistics.Win.UltraWinGrid.TabNavigation.NextCell;
                    ComboBox.DisplayLayout.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;

                    //스크롤이 최하단으로 내려갔을 때 빈공간이 없도록 설정 
                    ComboBox.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
                    //셀에 복사 및 붙여넣기 기능이 되도록 Clipboard 기능 설정
                    //ComboBox.DisplayLayout.Override.AllowMultiCellOperations = AllowMultiCellOperation.Copy | AllowMultiCellOperation.Paste;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 콤보 그리드 헤더에 Merge처리 해야하는 Group Header 설정
            /// </summary>
            /// <param name="ComboBox">ComboBox Object</param>
            /// <param name="intBandIndex">Group을 적용할 Band Index</param>
            /// <param name="strColKey">Group 컬럼의 Key</param>
            /// <param name="strColText">Group 컬럼의 Caption</param>
            /// <param name="bolGroupTextBold">Group 컬럼의 Caption에 대한 굵게 표시 여부</param>
            /// <param name="GroupTextColor">Group 컬럼의 Cpation에 대한 글자색</param>
            /// <param name="bolUseGroupHeader">Group 헤더 사용여부</param>
            public void mfSetComboGridGroup(Infragistics.Win.UltraWinGrid.UltraCombo ComboBox,
                                            int intBandIndex,
                                            string strColKey,
                                            string strColText,
                                            Infragistics.Win.DefaultableBoolean bolGroupTextBold,
                                            System.Drawing.Color GroupTextColor,
                                            bool bolUseGroupHeader)
            {
                try
                {
                    Infragistics.Win.UltraWinGrid.UltraGridGroup group = ComboBox.DisplayLayout.Bands[intBandIndex].Groups.Add(strColKey, strColText);
                    group.Header.Appearance.FontData.Bold = bolGroupTextBold;
                    group.Header.Appearance.ForeColor = GroupTextColor;
                    //그리드 헤더에 Group이 있는 경우
                    if (bolUseGroupHeader == true)
                        ComboBox.DisplayLayout.Bands[intBandIndex].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 콤보 그리드에 열 추가 및 설정하는 함수
            /// </summary>
            /// <param name="ComboBox">ComboBox Object</param>
            /// <param name="intBandIndex">컬럼의 Band Index</param>
            /// <param name="strColKey">컬럼의 Key</param>
            /// <param name="strColText">컬럼의 Caption</param>        
            /// <param name="bolUseFixedCol">컬럼에 대한 고정열 여부</param>
            /// <param name="intColWidth">컬럼 폭</param>
            /// <param name="bolColHidden">컬럼에 대한 Hidden여부</param>
            /// <param name="bolColHeaderBold">컬럼의 Caption에 대한 굵게표시 여부</param>
            /// <param name="ColHAlign">컬럼의 Cell에 대한 수평정렬</param>
            /// <param name="ColVAlign">컬럼의 Cell에 대한 수직정렬</param>
            /// <param name="ColMergeStyle">컬럼의 Cell에 대한 Merge속성</param>
            /// <param name="ColStyle">컬럼의 Cell에 대한 Style속성</param>
            /// <param name="strColFormat">컬럼의 Cell에 대한 Format</param>
            /// <param name="strColMaskInput">컬럼의 Cell에 대한 MaskInput</param>
            public void mfSetComboGridColumn(Infragistics.Win.UltraWinGrid.UltraCombo ComboBox,
                                             int intBandIndex,
                                             string strColKey,
                                             string strColText,
                                             bool bolUseFixedCol,
                                             int intColWidth,
                                             bool bolColHidden,
                                             Infragistics.Win.DefaultableBoolean bolColHeaderBold,
                                             Infragistics.Win.HAlign ColHAlign,
                                             Infragistics.Win.VAlign ColVAlign,
                                             Infragistics.Win.UltraWinGrid.MergedCellStyle ColMergeStyle,
                                             Infragistics.Win.UltraWinGrid.ColumnStyle ColStyle,
                                             string strColFormat,
                                             string strColMaskInput)
            {
                try
                {
                    //다중 선택을 위한 체크박스인 경우
                    if (ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox)
                    {
                        UltraGridColumn c = ComboBox.DisplayLayout.Bands[0].Columns.Add();
                        c.Key = strColKey;
                        c.Header.Caption = string.Empty;
                        //This allows end users to select / unselect ALL items
                        c.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
                        c.DataType = typeof(bool);
                        //Move the checkbox column to the first position.
                        c.Header.VisiblePosition = 0;
                        ComboBox.CheckedListSettings.CheckStateMember = strColKey;
                        ComboBox.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
                    }
                    else
                    {
                        //컬럼 Key, 컬럼명 지정(필수)
                        ComboBox.DisplayLayout.Bands[intBandIndex].Columns.Add(strColKey, strColText);
                        ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Style = ColStyle;
                    }

                    //컬럼 Hidden여부  (기본값 = false)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Hidden = bolColHidden;
                    //컬럼 고정열 처리 (기본값 = false)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Fixed = bolUseFixedCol;

                    //컬럼 폭          (기본값 = 100)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Width = intColWidth;

                    //컬럼 헤더 글씨 굵게 표시 (기본값 = true)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.FontData.Bold = bolColHeaderBold;

                    //컬럼 셀내 정렬처리 (기본값 = Left)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextHAlign = ColHAlign;

                    //컬럼 셀내 정렬처리 (기본값 = Middle)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextVAlign = ColVAlign;

                    //컬럼 Merge 속성 지정 (기본값 = Never)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MergedCellStyle = ColMergeStyle;

                    //컬럼 : 날짜, 시간, 정수, 실수 인 경우 Format 지정
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Format = strColFormat;
                    ////컬럼 : 문자인 경우 Format 지정 yyyy-mm-dd / hh:mm:ss
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MaskInput = strColMaskInput;

                    //컬럼 : 행추가시 기본값 설정
                    //ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].DefaultCellValue = strColDefaultValue;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 콤보 그리드에 열 추가 및 Group관련 설정하는 함수
            /// </summary>
            /// <param name="ComboBox">ComboBox Object</param>
            /// <param name="intBandIndex">컬럼의 Band Index</param>
            /// <param name="strColKey">컬럼의 Key</param>
            /// <param name="strColText">컬럼의 Caption</param>
            /// <param name="bolUseFixedCol">컬럼에 대한 고정열 여부</param>                
            /// <param name="intColWidth">컬럼 폭</param>
            /// <param name="bolColHidden">컬럼에 대한 Hidden여부</param>
            /// <param name="bolColHeaderBold">컬럼의 Caption에 대한 굵게표시 여부</param>
            /// <param name="ColHAlign">컬럼의 Cell에 대한 수평정렬</param>
            /// <param name="ColVAlign">컬럼의 Cell에 대한 수직정렬</param>
            /// <param name="ColMergeStyle">컬럼의 Cell에 대한 Merge속성</param>
            /// <param name="ColStyle">컬럼의 Cell에 대한 Style속성</param>
            /// <param name="strColFormat">컬럼의 Cell에 대한 Format</param>
            /// <param name="strColMaskInput">컬럼의 Cell에 대한 MaskInput</param>
            /// <param name="intOriginX">컬럼의 수평적 위치(Colnum Index)</param>
            /// <param name="intOriginY">컬럼의 수직적 위치(Row Index)</param>
            /// <param name="intSpanX">컬럼의 수평적 Merge수(Col Merge)</param>
            /// <param name="intSpanY">컬럼의 수직적 Merge수(Row Merge)</param>
            /// <param name="Group">컬럼이 속한 Group Object</param>
            public void mfSetComboGridColumn(Infragistics.Win.UltraWinGrid.UltraCombo ComboBox,
                                             int intBandIndex,
                                             string strColKey,
                                             string strColText,
                                             bool bolUseFixedCol,
                                             int intColWidth,
                                             bool bolColHidden,
                                             Infragistics.Win.DefaultableBoolean bolColHeaderBold,
                                             Infragistics.Win.HAlign ColHAlign,
                                             Infragistics.Win.VAlign ColVAlign,
                                             Infragistics.Win.UltraWinGrid.MergedCellStyle ColMergeStyle,
                                             Infragistics.Win.UltraWinGrid.ColumnStyle ColStyle,
                                             string strColFormat,
                                             string strColMaskInput,
                                             int intOriginX,
                                             int intOriginY,
                                             int intSpanX,
                                             int intSpanY,
                                             Infragistics.Win.UltraWinGrid.UltraGridGroup Group)
            {
                try
                {
                    //다중 선택을 위한 체크박스인 경우
                    if (ColStyle == Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox)
                    {
                        UltraGridColumn c = ComboBox.DisplayLayout.Bands[0].Columns.Add();
                        c.Key = strColKey;
                        c.Header.Caption = string.Empty;
                        //This allows end users to select / unselect ALL items
                        c.Header.CheckBoxVisibility = HeaderCheckBoxVisibility.Always;
                        c.DataType = typeof(bool);
                        //Move the checkbox column to the first position.
                        c.Header.VisiblePosition = 0;
                        ComboBox.CheckedListSettings.CheckStateMember = strColKey;
                        ComboBox.CheckedListSettings.EditorValueSource = Infragistics.Win.EditorWithComboValueSource.CheckedItems;
                    }
                    else
                    {
                        //컬럼 Key, 컬럼명 지정(필수)
                        ComboBox.DisplayLayout.Bands[intBandIndex].Columns.Add(strColKey, strColText);
                        ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Style = ColStyle;
                    }

                    //컬럼 Hidden여부  (기본값 = false)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Hidden = bolColHidden;

                    //컬럼 고정열 처리 (기본값 = false)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Fixed = bolUseFixedCol;

                    //컬럼 폭          (기본값 = 100)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Width = intColWidth;

                    //컬럼 헤더 글씨 굵게 표시 (기본값 = true)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Appearance.FontData.Bold = bolColHeaderBold;

                    //컬럼 셀내 정렬처리 (기본값 = Left)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextHAlign = ColHAlign;
                    //컬럼 셀내 정렬처리 (기본값 = Middle)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].CellAppearance.TextVAlign = ColVAlign;

                    //컬럼 Merge 속성 지정 (기본값 = Never)
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MergedCellStyle = ColMergeStyle;

                    //컬럼 : 날짜, 시간, 정수, 실수 인 경우 Format 지정
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Format = strColFormat;
                    ////컬럼 : 문자인 경우 Format 지정 yyyy-mm-dd / hh:mm:ss
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].MaskInput = strColMaskInput;
                    //컬럼 : 행추가시 기본값 설정
                    //ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].DefaultCellValue = strColDefaultValue;

                    //컬럼 : Group Header에 대한 Merge 또는 포함할 Group 지정처리
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.OriginX = intOriginX;
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.OriginY = intOriginY;
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.SpanX = intSpanX;
                    ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.SpanY = intSpanY + 1;

                    if (intSpanY > 1)
                        ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.MinimumLabelSize = new Size(ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Width, ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].Header.Height * intSpanY);
                    else
                        ComboBox.DisplayLayout.Bands[intBandIndex].Columns[strColKey].RowLayoutColumnInfo.ParentGroup = Group;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }
        }
        #endregion

        #region UltraTree Control 설정
        public class WinTree
        {
            public void mfInitTree(Infragistics.Win.UltraWinTree.UltraTree Tree,
                                   Infragistics.Win.UltraWinTree.UltraTreeDisplayStyle DisplayStyle,
                                   Infragistics.Win.UltraWinTree.ViewStyle ViewStyle,
                                   Infragistics.Win.UltraWinTree.ScrollBounds ScrollBoundType,
                                   Infragistics.Win.DefaultableBoolean bolUseMultiLine,
                                   string FontName)
            {
                try
                {
                    Tree.DisplayStyle = DisplayStyle;
                    Tree.ViewStyle = ViewStyle; //OutlookExpress;(그리드일때만 적용됨) //.Grid; //.Standard; //.FreeForm;

                    Tree.ScrollBounds = ScrollBoundType;
                    Tree.ScrollBarLook.ShowMinMaxButtons = DefaultableBoolean.True;

                    Tree.Appearance.FontData.Name = FontName;
                    Tree.Appearance.ForeColor = System.Drawing.Color.Black;

                    // 멀티라인 설정
                    Tree.Override.Multiline = bolUseMultiLine;

                    //Tree.Override.AllowCopy = DefaultableBoolean.True;
                    //Tree.Override.AllowPaste = DefaultableBoolean.True;

                    //Tree.Override.Multiline = Infragistics.Win.DefaultableBoolean.True;
                    //TreeNode.Override.Multiline = DefaultableBoolean.True;

                    ////이미지 설정하는 부분
                    ////디자인 부분 이미지 리스트에 넣어놓은 이미지 사용
                    ////기본 이미지 넣기
                    //Tree.ImageList = imageList1;
                    //Tree.Override.NodeAppearance.Image = 0;
                    //Tree.Override.ExpandedNodeAppearance.Image = 1;

                    // 노드 선택됐을때 배경색
                    //Tree.Override.ActiveNodeAppearance.BackColor = System.Drawing.Color.Transparent;
                    //Tree.Override.ActiveNodeAppearance.ForeColor = System.Drawing.Color.Black;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 트리에 노드를 추가하는 함수
            /// </summary>
            /// <param name="Tree">Tree Object</param>
            /// <param name="strKey">노드의 Key</param>
            /// <param name="strNodeName">노드의 Text</param>
            /// <param name="ParentNode">상위노드와 관계되는 노드 Object</param>
            /// <param name="NodeStyle">그리드의 노드 스타일</param>
            /// <param name="bolExpandNode">노드추가후 Expand여부</param>
            /// <returns>트리에 추가한 노드 Object</returns>
            public Infragistics.Win.UltraWinTree.UltraTreeNode mfAddNodeToTree(Infragistics.Win.UltraWinTree.UltraTree Tree,
                                                                               string strKey,
                                                                               string strNodeName,
                                                                               Infragistics.Win.UltraWinTree.UltraTreeNode ParentNode,
                                                                               Infragistics.Win.UltraWinTree.NodeStyle NodeStyle,
                                                                               bool bolExpandNode)
            {
                Infragistics.Win.UltraWinTree.UltraTreeNode TreeNode = new Infragistics.Win.UltraWinTree.UltraTreeNode();
                try
                {
                    //노드 추가하는 부분
                    if (ParentNode == null)
                    {   //최상위 Node를 추가하는 경우
                        TreeNode = Tree.Nodes.Add(strKey, strNodeName);
                    }
                    else
                    {   //인자로 넘겨받은 노드 밑에 하위 노드로 추가하는 경우                
                        TreeNode = ParentNode.Nodes.Add(strKey, strNodeName);
                    }

                    TreeNode.Expanded = bolExpandNode;

                    //Node Style (CheckBox, Option, 기본)
                    TreeNode.Override.NodeStyle = NodeStyle;

                    TreeNode.Override.AllowCopy = DefaultableBoolean.True;

                    ImageList img = new ImageList();
                    img.Images.Add("NODE_OPEN", Properties.Resources.tree_nodeopen);
                    img.Images.Add("NODE_CLOSE", Properties.Resources.tree_nodeclose_jpg);
                    img.Images.Add("FORM_SEL", Properties.Resources.tree_nodeselform);
                    img.Images.Add("FORM_UNSEL", Properties.Resources.tree_nodeunselform);

                    TreeNode.Override.NodeAppearance.Image = img.Images["NODE_CLOSE"];
                    TreeNode.Override.ExpandedNodeAppearance.Image = img.Images["NODE_OPEN"];

                    //if (strCloseImage != null && strOpenImage != null)
                    //{
                    //    //기본이미지와 열렸을경우 이미지 모두 바꾸는 경우
                    //    Bitmap nodeimg = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strCloseImage));
                    //    TreeNode.Override.NodeAppearance.Image = nodeimg;
                    //    Bitmap e_nodeimg = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strOpenImage));
                    //    TreeNode.Override.ExpandedNodeAppearance.Image = e_nodeimg;
                    //}
                    //else if (strCloseImage != null && strOpenImage == null)
                    //{
                    //    //기본이미지만 바꾸는 경우
                    //    Bitmap nodeimg = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strCloseImage));
                    //    TreeNode.Override.NodeAppearance.Image = nodeimg;
                    //}
                    //else if (strCloseImage == null && strOpenImage != null)
                    //{
                    //    //열렸을경우 이미지만 바꾸는 경우
                    //    Bitmap e_nodeimg = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strOpenImage));
                    //    TreeNode.Override.ExpandedNodeAppearance.Image = e_nodeimg;
                    //}

                    return TreeNode;
                }
                catch (Exception ex)
                {
                    return TreeNode;
                }
                finally
                {
                }
            }

            /// <summary>
            /// 트리에 노드를 추가하는 함수 (DataTable을 이용)
            /// </summary>
            /// <param name="Tree">Tree Object</param>
            /// <param name="strKeyColumn">노드에 Key가 되는 DataTable의 컬럼</param>
            /// <param name="strNodeNameColumn">노드에 Text가 되는 DataTable의 컬럼</param>
            /// <param name="strCloseImage">노드를 Collapse할때 이미지</param>
            /// <param name="strOpenImage">노드를 Expand할때 이미지</param>
            /// <param name="NodeStyle">그리드의 노드 스타일</param>
            /// <param name="bolExpandNode">노드추가후 Expand여부</param>
            /// <param name="dtNodeData">노드에 표시할 DataTable</param>
            /// <param name="strRelationKeyColumn">상위노드와 관계가 되는 DataTable의 컬럼 </param>
            public void mfAddNodeToTree(Infragistics.Win.UltraWinTree.UltraTree Tree,
                                        string strKeyColumn,
                                        string strNodeNameColumn,
                                        Infragistics.Win.UltraWinTree.NodeStyle NodeStyle,
                                        bool bolExpandNode,
                                        System.Data.DataTable dtNodeData,
                                        string strRelationKeyColumn)
            {
                try
                {
                    //DataTable에 대해 Loop를 돌면서 Node를 추가한다.
                    ImageList img = new ImageList();
                    img.Images.Add("NODE_OPEN", Properties.Resources.tree_nodeopen);
                    img.Images.Add("NODE_CLOSE", Properties.Resources.tree_nodeclose);
                    img.Images.Add("FORM_SEL", Properties.Resources.tree_nodeselform);
                    img.Images.Add("FORM_UNSEL", Properties.Resources.tree_nodeunselform);

                    foreach (DataRow row in dtNodeData.Rows)
                    {
                        Infragistics.Win.UltraWinTree.UltraTreeNode TreeNode = new Infragistics.Win.UltraWinTree.UltraTreeNode();

                        //다중 키인 경우 쉼표를 구분자로 하여 NodeKey를 설정한다. 
                        char[] strSeparator = { ',' };
                        string[] arrKey = strKeyColumn.Split(strSeparator);
                        string[] arrRelKey = strRelationKeyColumn.Split(strSeparator);
                        string strNodeKey = "";

                        for (int i = 0; i < arrKey.Count(); i++)
                        {
                            if (i == 0)
                                strNodeKey = strNodeKey + row[arrKey[i]].ToString();
                            else
                                strNodeKey = strNodeKey + strSeparator[0] + row[arrKey[i]].ToString();
                        }
                        TreeNode.Key = strNodeKey;
                        TreeNode.Text = row[strNodeNameColumn].ToString();

                        TreeNode.Override.NodeStyle = NodeStyle;
                        //Node Style (CheckBox, Option, 기본)
                        TreeNode.Override.NodeStyle = NodeStyle;
                        TreeNode.Expanded = bolExpandNode;
                        //TreeNode.Override.AllowCopy = DefaultableBoolean.True;

                        //if (strCloseImage != null && strOpenImage != null)
                        //{
                        //    //기본이미지와 열렸을경우 이미지 모두 바꾸는 경우
                        //    Bitmap nodeimg = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strCloseImage));
                        //    TreeNode.Override.NodeAppearance.Image = nodeimg;
                        //    Bitmap e_nodeimg = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strOpenImage));
                        //    TreeNode.Override.ExpandedNodeAppearance.Image = e_nodeimg;
                        //}
                        //else if (strCloseImage != null && strOpenImage == null)
                        //{
                        //    //기본이미지만 바꾸는 경우
                        //    Bitmap nodeimg = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strCloseImage));
                        //    TreeNode.Override.NodeAppearance.Image = nodeimg;
                        //}
                        //else if (strCloseImage == null && strOpenImage != null)
                        //{
                        //    //열렸을경우 이미지만 바꾸는 경우
                        //    Bitmap e_nodeimg = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strOpenImage));
                        //    TreeNode.Override.ExpandedNodeAppearance.Image = e_nodeimg;
                        //}

                        //관계키가 있는 경우 추가하려는 Node의 부모값과 동일한 Key값이 있는지 체크하여 부모Node에 Node를 추가한다.
                        if (strRelationKeyColumn != "")
                        {
                            Infragistics.Win.UltraWinTree.UltraTreeNode ParentTreeNode = new Infragistics.Win.UltraWinTree.UltraTreeNode();
                            string strRelKey = "";
                            for (int i = 0; i < arrRelKey.Count(); i++)
                            {
                                if (i == 0)
                                    strRelKey = strRelKey + row[arrRelKey[i]].ToString();
                                else
                                    strRelKey = strRelKey + strSeparator[0] + row[arrRelKey[i]].ToString();
                            }
                            ParentTreeNode = Tree.GetNodeByKey(strRelKey);
                            if (ParentTreeNode != null)
                            {
                                if (strNodeKey.Contains("frm") == true)
                                {
                                    TreeNode.Override.NodeAppearance.Image = img.Images["FORM_UNSEL"];
                                    TreeNode.Override.SelectedNodeAppearance.Image = img.Images["FORM_SEL"];
                                    TreeNode.Expanded = false;
                                    ParentTreeNode.Nodes.Add(TreeNode);
                                }
                                else
                                {
                                    TreeNode.Override.NodeAppearance.Image = img.Images["NODE_CLOSE"];
                                    TreeNode.Override.ExpandedNodeAppearance.Image = img.Images["NODE_OPEN"];
                                    Tree.Nodes.Add(TreeNode);
                                }
                            }
                            else
                            {
                                if (strNodeKey.Contains("frm") == true)
                                {
                                    TreeNode.Override.NodeAppearance.Image = img.Images["FORM_UNSEL"];
                                    TreeNode.Override.SelectedNodeAppearance.Image = img.Images["FORM_SEL"];
                                    TreeNode.Expanded = false;
                                    Tree.Nodes.Add(TreeNode);
                                }
                                else
                                {
                                    TreeNode.Override.NodeAppearance.Image = img.Images["NODE_CLOSE"];
                                    TreeNode.Override.ExpandedNodeAppearance.Image = img.Images["NODE_OPEN"];
                                    Tree.Nodes.Add(TreeNode);
                                }
                            }
                        }
                        //관계키가 없는 경우 Node를 추가한다.
                        else
                        {
                            TreeNode.Override.NodeAppearance.Image = img.Images["NODE_CLOSE"];
                            TreeNode.Override.ExpandedNodeAppearance.Image = img.Images["NODE_OPEN"];
                            Tree.Nodes.Add(TreeNode);
                        }

                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }

            /// <summary>
            /// 노드밑에 그리드 추가하는 함수
            /// </summary>
            /// <param name="ut_Tree"> WinTree 변수 </param>
            /// <param name="ut_TreeNode"> 그리드가 추가될 WinTreeNode 변수 </param>
            /// <param name="dt"> DataTable </param>
            /// <param name="str_ColumnName"> 헤더명이 들어있는 String 배열 </param>
            /// <param name="int_HiddenColumn"> 컬럼 Hidden 처리를 위한 int 배열 </param>
            /// <param name="ColumnWidth"> 컬럼의 폭 넓이를 지정할 int 변수 </param>
            public void mfAddGridToTree(Infragistics.Win.UltraWinTree.UltraTree Tree,
                //Infragistics.Win.UltraWinTree.UltraTreeNode TreeNode,
                                        GridColumn_TreeNode[] arrGridColumns,
                                        DataTable dtNodeData,
                                        Infragistics.Win.UltraWinTree.AllowCellEdit CellEdit)
            //string[] str_ColumnName,
            //int[] int_HiddenColumn,
            //int ColumnWidth)
            {
                try
                {
                    //ArrayList strCol = new ArrayList(); //->이걸로 받는 방법?? 구조체 및 배열
                    //TreeColumnSet[] arrCol = new TreeColumnSet[4];

                    // WinTree의 ViewStyle변경 FreeForm 이나 Grid 스타일 이외의 것은 그리드가 안나옴//
                    //Tree.ViewStyle = Infragistics.Win.UltraWinTree.ViewStyle.FreeForm;
                    //상단 부분은 예외처리 필요//

                    // 인자로 받은 TreeNode의 컬럼셋 설정 //
                    Infragistics.Win.UltraWinTree.UltraTreeColumnSet columnset = new Infragistics.Win.UltraWinTree.UltraTreeColumnSet();

                    for (int i = 0; i < arrGridColumns.Length; i++)
                    {
                        Infragistics.Win.UltraWinTree.UltraTreeNodeColumn col = new Infragistics.Win.UltraWinTree.UltraTreeNodeColumn();

                        col.Key = arrGridColumns[i].strKey;
                        col.Text = arrGridColumns[i].strText;

                        col.AllowSorting = arrGridColumns[i].AllowSorting; //DefaultableBoolean.Default;
                        col.AutoSizeMode = arrGridColumns[i].ColAutoSizeType; //Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.Default;

                        col.CellAppearance.TextHAlign = arrGridColumns[i].ColHAlign; //HAlign.Default;
                        col.CellAppearance.TextVAlign = arrGridColumns[i].ColVAlign; //VAlign.Default;
                        //col.CellAppearance.TextTrimming = TextTrimming.Default; //??

                        col.ShowSortIndicators = arrGridColumns[i].ShowSortIndicator; //DefaultableBoolean.True;
                        col.SortType = arrGridColumns[i].ColSortType; //Infragistics.Win.UltraWinTree.SortType.Descending;
                        col.Visible = arrGridColumns[i].bolVisible;

                        //셀 편집 속성
                        col.AllowCellEdit = CellEdit;

                        //고정
                        col.TipStyleCell = Infragistics.Win.UltraWinTree.TipStyleCell.Show;

                        //columnset.Columns.Add(str_ColumnName[i]);
                        columnset.Columns.Add(col);
                    }

                    ////////for (int i = 0; i < str_ColumnName.Length; i++)
                    ////////{
                    ////////    Infragistics.Win.UltraWinTree.UltraTreeNodeColumn col = new Infragistics.Win.UltraWinTree.UltraTreeNodeColumn();

                    ////////    col.Key = "";
                    ////////    col.Text = "";

                    ////////    col.AllowSorting = DefaultableBoolean.Default;
                    ////////    col.AutoSizeMode = Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.Default;

                    ////////    col.CellAppearance.TextHAlign = HAlign.Default;
                    ////////    col.CellAppearance.TextVAlign = VAlign.Default;
                    ////////    col.CellAppearance.TextTrimming = TextTrimming.Default; //??

                    ////////    col.ShowSortIndicators = DefaultableBoolean.True;
                    ////////    col.SortType = Infragistics.Win.UltraWinTree.SortType.Descending;
                    ////////    col.Visible = true;

                    ////////    //고정
                    ////////    col.TipStyleCell = Infragistics.Win.UltraWinTree.TipStyleCell.Show;
                    ////////    col.AllowCellEdit = Infragistics.Win.UltraWinTree.AllowCellEdit.Disabled;

                    ////////    //columnset.Columns.Add(str_ColumnName[i]);
                    ////////    columnset.Columns.Add(col);
                    ////////}

                    //// 헤더 배경색 //
                    ////columnset.ColumnHeaderAppearance.BackColor = Color.Blue;

                    //// 헤더 정렬 지정 //
                    //columnset.ColumnHeaderAppearance.TextHAlign = HAlign.Center;
                    //columnset.ColumnHeaderAppearance.TextVAlign = VAlign.Middle;

                    //// 헤더스타일 지정 //
                    //columnset.HeaderStyle = HeaderStyle.WindowsVista;

                    //// 헤더 크기 조절 못하게 설정 //
                    ////columnset.AllowLabelSizing = LayoutSizing.None;

                    //// 헤더 더블클릭시 컬럼텍스트 사이즈에 맞게 사이즈 자동조절 //
                    ////columnset.Columns.ColumnSet.ColumnAutoSizeMode = Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes;

                    //// 인자로 받은 TreeNode에 컬럼셋 설정
                    //ut_TreeNode.Nodes.Override.ColumnSet = columnset;

                    //// 인자로 받은 TreeNode에 그리드 형식으로 들어갈 임시 Node //
                    //UltraTreeNode tempNode = ut_TreeNode.Nodes.Add();

                    //// 임시 Node에 DB에서 받은 정보가 들어있는 DataTable의 정보 저장 //
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    for (int j = 0; j < dt.Columns.Count; j++)
                    //    {
                    //        tempNode.Cells[str_ColumnName[j]].Value = dt.Rows[i][j];
                    //        //tempNode.Cells[str_ColumnName[j]].Column.PerformAutoResize(Infragistics.Win.UltraWinTree.ColumnAutoSizeMode.AllNodes);
                    //        tempNode.Cells[str_ColumnName[j]].Column.LayoutInfo.MinimumCellSize = new Size(ColumnWidth, 0);

                    //        // 컬럼 안보이게 설정 //
                    //        for (int a = 0; a < int_HiddenColumn.Length; a++)
                    //        {
                    //            if (j == int_HiddenColumn[a])
                    //            {
                    //                tempNode.Cells[str_ColumnName[j]].Column.Visible = false;
                    //            }
                    //        }
                    //    }

                    //    // 마지막에 빈 노드 생성을 막기 위한 if문 //
                    //    if (i < dt.Rows.Count - 1)
                    //        tempNode = ut_TreeNode.Nodes.Add();
                    //}

                    //// Node 폰트 설정 //
                    //ut_TreeNode.Nodes.Override.ColumnSet.CellAppearance.FontData.Name = "돋움";
                    //ut_TreeNode.Nodes.Override.ColumnSet.CellAppearance.FontData.SizeInPoints = 10;
                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }
        }
        #endregion

        #region UltraWinTabControl 설정
        public class WinTabControl
        {
            /// <summary>
            /// Tab컨트롤을 초기화하는 함수
            /// </summary>
            /// <param name="Tab"></param>
            /// <param name="TabStyle"></param>
            /// <param name="UseTabCloseButton"></param>
            /// <param name="TabCloseButtonLoc"></param>
            /// <param name="strFontName"></param>
            public void mfInitGeneralTabControl(Infragistics.Win.UltraWinTabControl.UltraTabControl Tab,
                                                Infragistics.Win.UltraWinTabControl.UltraTabControlStyle TabStyle,
                                                Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility UseTabCloseButton,
                                                Infragistics.Win.UltraWinTabs.TabCloseButtonLocation TabCloseButtonLoc,
                                                string strFontName)
            {
                //메뉴Tab Style속성 지정
                Tab.UseOsThemes = DefaultableBoolean.False;
                Tab.Style = TabStyle;

                //탭닫기 버튼 속성 지정
                Tab.TabCloseButtonVisibility = UseTabCloseButton;
                if (UseTabCloseButton != Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never)
                    Tab.CloseButtonLocation = TabCloseButtonLoc;

                Tab.Appearance.FontData.Name = strFontName;

                //고정값 속성
                Tab.UseOsThemes = DefaultableBoolean.False;

                Tab.Appearance.BackColor = Color.WhiteSmoke;
                Tab.Appearance.BackColor2 = Color.White;
                Tab.Appearance.ForeColor = Color.DarkGray;
                Tab.Appearance.BackGradientStyle = GradientStyle.HorizontalBump;
                Tab.Appearance.BackGradientAlignment = GradientAlignment.Client;

                //Tab.ClientAreaAppearance.BackColor = Color.Lavender;
                //Tab.ClientAreaAppearance.BackColor2 = Color.White;
                //Tab.ClientAreaAppearance.BackGradientStyle = GradientStyle.HorizontalBump;
                //Tab.ClientAreaAppearance.BackGradientAlignment = GradientAlignment.Client;

                Tab.ActiveTabAppearance.BackColor = Color.SkyBlue;
                Tab.ActiveTabAppearance.ForeColor = Color.Black;
                //Tab.ActiveTabAppearance.BackGradientStyle = GradientStyle.VerticalBump; ;
                Tab.ActiveTabAppearance.BackGradientAlignment = GradientAlignment.Client;
                Tab.ActiveTabAppearance.BorderColor = Color.Black;

                //Tab.SelectedTabAppearance.BackColor = Color.SkyBlue;
                //Tab.SelectedTabAppearance.ForeColor = Color.Black;
                //Tab.SelectedTabAppearance.BorderColor = Color.Black;

                //Tab.SelectedTabAppearance.ForeColor = Color.Blue;

                //Tab.HotTrackAppearance.BackColor = Color.AliceBlue;
                //Tab.HotTrackAppearance.ForeColor = Color.Silver;
                //Tab.HotTrackAppearance.BorderColor = Color.Black;
                //Tab.UseHotTracking = DefaultableBoolean.True;
                Tab.EndUpdate();
            }
        }
        #endregion

        #region UltraButton Control 설정
        public class WinButton
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="Label"></param>
            /// <param name="strLabelText"></param>
            /// <param name="strFontName"></param>
            public void mfSetButton(Infragistics.Win.Misc.UltraButton Button,
                                   string strButtonText,
                                   string strFontName,
                                   Image imgButton)
            {
                try
                {
                    Button.Text = strButtonText;
                    Button.Appearance.FontData.Name = strFontName;
                    Button.Appearance.FontData.Bold = DefaultableBoolean.True;
                    if (imgButton != null)
                    {
                        Button.Appearance.Image = imgButton;
                    }

                    

                    ////Label.Appearance.BackColor = System.Drawing.Color.GhostWhite; //System.Drawing.Color.Transparent;
                    ////Label.Appearance.BorderColor = System.Drawing.Color.GhostWhite; //System.Drawing.Color.Transparent;
                    ////Label.Appearance.BorderColor2 = System.Drawing.Color.Lavender;

                    ////Label.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Rounded3;
                    ////Label.BorderStyleOuter = Infragistics.Win.UIElementBorderStyle.RaisedSoft;

                    ////Label.Text = strLabelText;
                    ////Label.Appearance.FontData.Name = strFontName;
                    ////Label.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                    ////Label.Appearance.TextVAlign = VAlign.Middle;
                    ////Label.Appearance.TextHAlign = HAlign.Left;

                    //////label앞에 이미지 사용여부
                    ////if (bolUseTitleImage == true)
                    ////{
                    ////    ImageList img = new ImageList();
                    ////    img.Images.Add("TITLE", Properties.Resources.lbl_title);
                    ////    Label.Appearance.Image = img.Images["TITLE"];
                    ////    Label.ImageSize = new Size(6, 6);
                    ////    Label.Appearance.ImageVAlign = VAlign.Middle;
                    ////}

                    //////필수입력항목에 대한 Label여부
                    ////if (bolMandantoryFlag == true)
                    ////{
                    ////    Label.Appearance.FontData.Bold = DefaultableBoolean.True;
                    ////    Label.Appearance.ForeColor = System.Drawing.Color.DarkRed;
                    ////}

                }
                catch (Exception ex)
                {
                }
                finally
                {
                }
            }
        }
        #endregion

        #region UltraWinMessageBox Control 설정
        public class WinMessageBox
        {
            /// <summary>
            /// 사용자 정의 메세지 박스 설정
            /// </summary>
            /// <param name="strType">메세지박스 유형</param>
            /// <param name="strFontName">글자 폰트</param>
            /// <param name="intMinWidth">메세지박스 폼 최소 폭</param>
            /// <param name="intMaxWidth">메세지박스 폼 최대 폭</param>
            /// <param name="MsgBoxStyle">메세지박스 스타일</param>
            /// <param name="strFormName">메세지박스 폼 명</param>
            ///// <param name="HeaderBackColor">메세지박스 헤더 배경색</param>
            ///// <param name="HeaderForeColor">메세지박스 헤더 글자색</param>
            ///// <param name="HeaderBold">메세지박스 헤더 굵게표시여부</param>
            ///// <param name="HeaderHAlign">메세지박스 헤더 수평정렬</param>
            ///// <param name="HeaderVAlign">메세지박스 헤더 수직정렬</param>
            /// <param name="strMsgHeader">메세지박스 헤더명</param>
            /// <param name="strMsgText">메세지박스 메세지글</param>
            /// <param name="ButtonHAlign">메세지박스 버튼 수평정렬</param>
            ///// <param name="ButtonAreaBackColor">메세지박스 버튼영역 배경색</param>
            ///// <param name="ButtonBackColor">메세지박스 버튼 배경색</param>
            ///// <param name="ButtonForeColor">메세지박스 버튼 글자색</param>
            ///// <param name="ButtonBold">메세지박스 버튼 굵게표시여부</param>
            ///// <param name="FooterBackColor">메세지박스 바닥글 배경색</param>
            ///// <param name="FooterForeColor">메세지박스 바닥글 글자색</param>
            ///// <param name="FooterBold">메세지박스 바닥글 굵게표시여부</param>
            ///// <param name="FooterHAlign">메세지박스 바닥글 수평정렬</param>
            ///// <param name="FooterVAlign">메세지박스 바닥글 수직정렬</param>
            ///// <returns>메세지박스 버튼 선택 값</returns>
            public DialogResult mfSetMessageBox(MessageBoxType MsgBoxType,
                                                string strFontName,
                                                int intMinWidth,
                                                int intMaxWidth,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle MsgBoxStyle,
                                                string strFormName,

                                                //System.Drawing.Color HeaderBackColor,
                                                //System.Drawing.Color HeaderForeColor,
                                                //Infragistics.Win.DefaultableBoolean HeaderBold,
                                                //Infragistics.Win.HAlign HeaderHAlign,
                                                //Infragistics.Win.VAlign HeaderVAlign,

                                                string strMsgHeader,
                                                string strMsgText,
                                                Infragistics.Win.HAlign ButtonHAlign)
                                                
                                                //System.Drawing.Color ButtonAreaBackColor,
                                                //System.Drawing.Color ButtonBackColor,
                                                //System.Drawing.Color ButtonForeColor,
                                                //Infragistics.Win.DefaultableBoolean ButtonBold,
                                                //System.Drawing.Color FooterBackColor,
                                                //System.Drawing.Color FooterForeColor,
                                                //Infragistics.Win.DefaultableBoolean FooterBold,
                                                //Infragistics.Win.HAlign FooterHAlign,
                                                //Infragistics.Win.VAlign FooterVAlign)
            {
                Infragistics.Win.UltraMessageBox.UltraMessageBoxInfo MsgInfo = new Infragistics.Win.UltraMessageBox.UltraMessageBoxInfo();
                MsgInfo.Style = MsgBoxStyle;
                MsgInfo.MinimumWidth = intMinWidth;
                MsgInfo.MaximumWidth = intMaxWidth;

                QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                ResourceSet m_sysRes = new ResourceSet(SysRes.SystemInfoRes);

                //버튼명 변경을 위한 변수
                ResourceCustomizer rc;
                rc = Infragistics.Win.Resources.Customizer;
                rc.ResetAllCustomizedStrings();

                //유형에 맞게 메세지 박스 버튼과 아이콘 지정
                if (MsgBoxType == MessageBoxType.Information)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.OK;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Information;
                }
                else if (MsgBoxType == MessageBoxType.Warning)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.OK;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Warning;
                }
                else if (MsgBoxType == MessageBoxType.Error)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.OK;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Error;
                }
                else if (MsgBoxType == MessageBoxType.YesNo)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.YesNo;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Question;
                }
                else if (MsgBoxType == MessageBoxType.YesNoCancel)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.YesNoCancel;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Question;
                }
                else if (MsgBoxType == MessageBoxType.AgreeReject)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.YesNoCancel;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Question;
                    if (m_sysRes.GetString("SYS_LANG").Equals("CHN"))
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "审批");
                        rc.SetCustomizedString("MessageBoxButtonNo", "返还");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "取消");
                    }
                    else if (m_sysRes.GetString("SYS_LANG").Equals("ENG"))
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "Admit");
                        rc.SetCustomizedString("MessageBoxButtonNo", "Return");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "Cancel");
                    }
                    else
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "승인");
                        rc.SetCustomizedString("MessageBoxButtonNo", "반려");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "취소");
                    }
                }
                else if (MsgBoxType == MessageBoxType.ReqAgreeSave)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.YesNoCancel;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Question;
                    if (m_sysRes.GetString("SYS_LANG").Equals("CHN"))
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "审批邀请");
                        rc.SetCustomizedString("MessageBoxButtonNo", "中间保存");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "取消");
                    }
                    else if (m_sysRes.GetString("SYS_LANG").Equals("ENG"))
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "Admit Request");
                        rc.SetCustomizedString("MessageBoxButtonNo", "Interim Save");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "Cancel");
                    }
                    else
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "승인요청");
                        rc.SetCustomizedString("MessageBoxButtonNo", "중간저장");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "취소");
                    }
                }

                // Set the Button to be selected by default
                MsgInfo.Appearance.FontData.Name = strFontName;

                // 폼 제목
                MsgInfo.Caption = strFormName;
                //Icon추가

                // 제목
                //MsgInfo.HeaderAppearance.BackColor = HeaderBackColor;
                MsgInfo.HeaderAppearance.FontData.Name = strFontName;
                MsgInfo.HeaderAppearance.FontData.SizeInPoints = 10;
                //MsgInfo.HeaderAppearance.ForeColor = HeaderForeColor;
                //MsgInfo.HeaderAppearance.FontData.Bold = HeaderBold;
                //MsgInfo.HeaderAppearance.TextHAlign = HeaderHAlign;
                //MsgInfo.HeaderAppearance.TextVAlign = HeaderVAlign;
                //MsgInfo.HeaderAppearance.FontData.Name = strFormName;
                //messageinfo.HeaderAppearance.Image = 
                MsgInfo.Header = strMsgHeader;

                // 내용
                MsgInfo.TextFormatted = strMsgText;

                // 버튼
                MsgInfo.DefaultButton = System.Windows.Forms.MessageBoxDefaultButton.Button1;
                //MsgInfo.ButtonAreaAppearance.BackColor = ButtonAreaBackColor;
                //MsgInfo.ButtonAppearance.BackColor = ButtonBackColor;
                //MsgInfo.ButtonAppearance.ForeColor = ButtonForeColor;
                MsgInfo.ButtonAppearance.FontData.Name = strFontName;
                //MsgInfo.ButtonAppearance.FontData.Bold = ButtonBold;
                MsgInfo.ButtonAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
                MsgInfo.ButtonAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;

                // Footer
                //MsgInfo.FooterAppearance.BackColor = FooterBackColor;
                MsgInfo.FooterAppearance.FontData.Name = strFontName;
                //MsgInfo.FooterAppearance.ForeColor = FooterForeColor;
                //MsgInfo.FooterAppearance.FontData.Bold = FooterBold;
                //MsgInfo.FooterAppearance.TextHAlign = FooterHAlign;
                //MsgInfo.FooterAppearance.TextVAlign = FooterVAlign;
                //messageinfo.HeaderAppearance.Image = this.imageList1.Images[1];
                //string strFooterMsg = "<a href ='www.jarsystems.co.kr'>Copyright ⓒ 2011~2015 JaR Systems(주) Corperation. All right reserved.</a>";
                string strFooterMsg = "Copyright ⓒ 2011~2015 JaR Systems(주) Corperation. All right reserved.";
                MsgInfo.FooterFormatted = strFooterMsg;

                //고정값
                Bitmap img = new Bitmap(Properties.Resources.lbl_title_large);
                MsgInfo.HeaderAppearance.Image = img;
                MsgInfo.HeaderAppearance.ImageVAlign = VAlign.Middle;
                MsgInfo.HeaderAppearance.ImageHAlign = HAlign.Right;

                MsgInfo.HeaderAppearance.BackColor = Color.SteelBlue;
                MsgInfo.HeaderAppearance.ForeColor = Color.White;
                MsgInfo.HeaderAppearance.FontData.Bold = DefaultableBoolean.True;
                MsgInfo.HeaderAppearance.TextHAlign = HAlign.Left;
                MsgInfo.HeaderAppearance.TextVAlign = VAlign.Middle;

                MsgInfo.ButtonAreaAppearance.BackColor = Color.WhiteSmoke;
                MsgInfo.ButtonAppearance.BackColor = Color.Gainsboro;
                MsgInfo.ButtonAppearance.ForeColor = Color.Black;
                MsgInfo.ButtonAppearance.FontData.Bold = DefaultableBoolean.False;

                MsgInfo.FooterAppearance.BackColor = Color.LightSteelBlue;
                MsgInfo.FooterAppearance.ForeColor = Color.Black;
                MsgInfo.FooterAppearance.FontData.Bold = DefaultableBoolean.False;
                MsgInfo.FooterAppearance.TextHAlign = HAlign.Center;
                MsgInfo.FooterAppearance.TextVAlign = VAlign.Middle;


                Infragistics.Win.UltraMessageBox.UltraMessageBoxManager msgManager = new Infragistics.Win.UltraMessageBox.UltraMessageBoxManager();

                // 버튼 정렬 처리 //
                msgManager.ButtonAlignment = ButtonHAlign;

                System.Windows.Forms.DialogResult MsgResult; // = msgManager.ShowMessageBox(MsgInfo);

                Thread.Sleep(300);
                MsgResult = msgManager.ShowMessageBox(MsgInfo);
     
                Thread.Sleep(200);

                MsgInfo.Dispose();
                return MsgResult;
                
            }

            /// <summary>
            /// 사용자 정의 메세지 박스(다국어 적용버젼)
            /// </summary>
            /// <param name="MsgBoxType">박스유형</param>
            /// <param name="intMinWidth">메세지박스 폼 최소폭</param>
            /// <param name="intMaxWidth">메세지박스 폼 최대폭</param>
            /// <param name="MsgBoxStyle">메세지 박스 스타일</param>
            /// <param name="strFormNameCode">폼명코드</param>
            /// <param name="strMsgHeaderCode">헤더코드</param>
            /// <param name="strMsgTextCode">메시지코드</param>
            /// <param name="ButtonHAlign">버튼정렬</param>
            /// <returns></returns>
            public DialogResult mfSetMessageBox(MessageBoxType MsgBoxType,
                                                int intMinWidth,
                                                int intMaxWidth,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle MsgBoxStyle,
                                                string strFormNameCode,
                                                string strMsgHeaderCode,
                                                string strMsgTextCode,
                                                Infragistics.Win.HAlign ButtonHAlign)
            {
                // 리소스 호출을 위한 전역변수
                QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                ResourceSet m_sysRes = new ResourceSet(SysRes.SystemInfoRes);

                // 폰트, 언어 변수 설정
                string strFontName = m_sysRes.GetString("SYS_FONTNAME");
                string strLang = m_sysRes.GetString("SYS_LANG");
                // XML 문서 로드 및 각 FormName, 메세지 헤더, 메세지 별 XElement 생성
                System.Xml.Linq.XDocument xDoc = new System.Xml.Linq.XDocument();
                xDoc = System.Xml.Linq.XDocument.Load(Application.StartupPath + "\\MultiLang_Message.xml");
                Infragistics.Win.UltraMessageBox.UltraMessageBoxInfo MsgInfo = new Infragistics.Win.UltraMessageBox.UltraMessageBoxInfo();
                MsgInfo.Style = MsgBoxStyle;
                MsgInfo.MinimumWidth = intMinWidth;
                MsgInfo.MaximumWidth = intMaxWidth;
                //버튼명 변경을 위한 변수
                ResourceCustomizer rc;
                rc = Infragistics.Win.Resources.Customizer;
                rc.ResetAllCustomizedStrings();
                //유형에 맞게 메세지 박스 버튼과 아이콘 지정
                if (MsgBoxType == MessageBoxType.Information)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.OK;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Information;
                }
                else if (MsgBoxType == MessageBoxType.Warning)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.OK;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Warning;
                }
                else if (MsgBoxType == MessageBoxType.Error)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.OK;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Error;
                }
                else if (MsgBoxType == MessageBoxType.YesNo)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.YesNo;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Question;
                }
                else if (MsgBoxType == MessageBoxType.YesNoCancel)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.YesNoCancel;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Question;
                }
                else if (MsgBoxType == MessageBoxType.AgreeReject)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.YesNoCancel;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Question;
                    if (m_sysRes.GetString("SYS_LANG").Equals("CHN"))
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "审批");
                        rc.SetCustomizedString("MessageBoxButtonNo", "返还");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "取消");
                    }
                    else if (m_sysRes.GetString("SYS_LANG").Equals("ENG"))
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "Admit");
                        rc.SetCustomizedString("MessageBoxButtonNo", "Return");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "Cancel");
                    }
                    else
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "승인");
                        rc.SetCustomizedString("MessageBoxButtonNo", "반려");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "취소");
                    }
                }
                else if (MsgBoxType == MessageBoxType.ReqAgreeSave)
                {
                    MsgInfo.Buttons = System.Windows.Forms.MessageBoxButtons.YesNoCancel;
                    MsgInfo.Icon = System.Windows.Forms.MessageBoxIcon.Question;
                    if (m_sysRes.GetString("SYS_LANG").Equals("CHN"))
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "审批邀请");
                        rc.SetCustomizedString("MessageBoxButtonNo", "中间保存");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "取消");
                    }
                    else if (m_sysRes.GetString("SYS_LANG").Equals("ENG"))
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "Admit Request");
                        rc.SetCustomizedString("MessageBoxButtonNo", "Interim Save");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "Cancel");
                    }
                    else
                    {
                        rc.SetCustomizedString("MessageBoxButtonYes", "승인요청");
                        rc.SetCustomizedString("MessageBoxButtonNo", "중간저장");
                        rc.SetCustomizedString("MessageBoxButtonCancel", "취소");
                    }
                }
                // 폼 제목
                MsgInfo.Appearance.FontData.Name = strFontName;
                MsgInfo.Caption = GetMessge_Text(xDoc, strFormNameCode, strLang);
                // 제목
                MsgInfo.HeaderAppearance.FontData.Name = strFontName;
                MsgInfo.HeaderAppearance.FontData.SizeInPoints = 10;
                MsgInfo.Header = GetMessge_Text(xDoc, strMsgHeaderCode, strLang);
                // 내용
                MsgInfo.TextFormatted = GetMessge_Text(xDoc, strMsgTextCode, strLang);
                // 버튼
                MsgInfo.DefaultButton = System.Windows.Forms.MessageBoxDefaultButton.Button1;
                MsgInfo.ButtonAppearance.FontData.Name = strFontName;
                MsgInfo.ButtonAppearance.TextHAlign = Infragistics.Win.HAlign.Center;
                MsgInfo.ButtonAppearance.TextVAlign = Infragistics.Win.VAlign.Middle;
                // Footer
                MsgInfo.FooterAppearance.FontData.Name = strFontName;
                string strFooterMsg = "Copyright ⓒ 2011~2015 JaR Systems(주) Corperation. All right reserved.";
                MsgInfo.FooterFormatted = strFooterMsg;
                //고정값
                Bitmap img = new Bitmap(Properties.Resources.lbl_title_large);
                MsgInfo.HeaderAppearance.Image = img;
                MsgInfo.HeaderAppearance.ImageVAlign = VAlign.Middle;
                MsgInfo.HeaderAppearance.ImageHAlign = HAlign.Right;
                MsgInfo.HeaderAppearance.BackColor = Color.SteelBlue;
                MsgInfo.HeaderAppearance.ForeColor = Color.White;
                MsgInfo.HeaderAppearance.FontData.Bold = DefaultableBoolean.True;
                MsgInfo.HeaderAppearance.TextHAlign = HAlign.Left;
                MsgInfo.HeaderAppearance.TextVAlign = VAlign.Middle;
                MsgInfo.ButtonAreaAppearance.BackColor = Color.WhiteSmoke;
                MsgInfo.ButtonAppearance.BackColor = Color.Gainsboro;
                MsgInfo.ButtonAppearance.ForeColor = Color.Black;
                MsgInfo.ButtonAppearance.FontData.Bold = DefaultableBoolean.False;
                MsgInfo.FooterAppearance.BackColor = Color.LightSteelBlue;
                MsgInfo.FooterAppearance.ForeColor = Color.Black;
                MsgInfo.FooterAppearance.FontData.Bold = DefaultableBoolean.False;
                MsgInfo.FooterAppearance.TextHAlign = HAlign.Center;
                MsgInfo.FooterAppearance.TextVAlign = VAlign.Middle;

                Infragistics.Win.UltraMessageBox.UltraMessageBoxManager msgManager = new Infragistics.Win.UltraMessageBox.UltraMessageBoxManager();
                // 버튼 정렬 처리 //
                msgManager.ButtonAlignment = ButtonHAlign;
                System.Windows.Forms.DialogResult MsgResult;
                Thread.Sleep(300);
                MsgResult = msgManager.ShowMessageBox(MsgInfo);
                Thread.Sleep(200);
                MsgInfo.Dispose();
                return MsgResult;
            }
            /// <summary>
            /// Xdocument 로 부터 언어에 맞는 메세지 반환하는 메소드
            /// </summary>
            /// <param name="xDoc">Xml Document 변수</param>
            /// <param name="MessageCode">메세지코드</param>
            /// <param name="strLang">언어</param>
            /// <returns></returns>
            private string GetMessge_Text(XDocument xDoc, string MessageCode, string strLang)
            {
                //string strRtnMessage = string.Empty;
                try
                {
                    string strRtnMessage = string.Empty;
                    XElement xel;
                    try
                    {
                        xel = (from myConfig in xDoc.Elements("Root").Elements("Langs").Elements("Message")
                               where (string)myConfig.Attribute("MessageCode").Value == MessageCode
                               select myConfig).First();
                    }
                    catch
                    {
                        xel = null;
                    }
                    if (xel != null)
                        strRtnMessage = (from name in xel.Elements("Lang")
                                         select name.Attribute(strLang).Value.ToString()).First();
                    return strRtnMessage;
                }
                catch (Exception ex)
                {
                    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                    frmErr.ShowDialog();
                    //return strRtnMessage;
                    return ex.Message;
                }
                finally
                {
                }
            }

            /// <summary>
            /// 메세지 코드로 XML에서 메세지 찾는 메소드
            /// </summary>
            /// <param name="MessageCode">메세지코드</param>
            /// <param name="strLang">언어</param>
            /// <returns></returns>
            public string GetMessge_Text(string MessageCode, string strLang)
            {
                //string strRtnMessage = string.Empty;
                try
                {
                    // XML 문서 로드 및 각 FormName, 메세지 헤더, 메세지 별 XElement 생성
                    System.Xml.Linq.XDocument xDoc = new System.Xml.Linq.XDocument();
                    xDoc = System.Xml.Linq.XDocument.Load(Application.StartupPath + "\\MultiLang_Message.xml");

                    string strRtnMessage = string.Empty;
                    XElement xel;
                    try
                    {
                        xel = (from myConfig in xDoc.Elements("Root").Elements("Langs").Elements("Message")
                               where (string)myConfig.Attribute("MessageCode").Value == MessageCode
                               select myConfig).First();
                    }
                    catch
                    {
                        xel = null;
                    }

                    if (xel != null)
                        strRtnMessage = (from name in xel.Elements("Lang")
                                         select name.Attribute(strLang).Value.ToString()).First();

                    return strRtnMessage;
                }
                catch (Exception ex)
                {
                    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                    frmErr.ShowDialog();
                    //return strRtnMessage;
                    return ex.Message;
                }
                finally
                {
                }
            }

        }
        #endregion

        #region 공통Control 설정
        public class CommonControl
        {
            /// <summary>
            /// 컨터이너 컨트롤내에서 특정컨트롤을 찾는 함수
            /// </summary>
            /// <param name="container"></param>
            /// <param name="name"></param>
            /// <returns></returns>
            public Control mfFindControlRecursive(Control Container, string strName)
            {
                if (Container.Name == strName)
                    return Container;

                foreach (Control ctrl in Container.Controls)
                {
                    Control foundCtrl = mfFindControlRecursive(ctrl, strName);
                    if (foundCtrl != null)
                        return foundCtrl;
                }
                return null;
            }

            /// <summary>
            /// 컨테이너 컨트롤에서 하위컨트롤들을 찾아 저장하는 함수
            /// </summary>
            /// <param name="Container"></param>
            /// <returns></returns>
            private Control[] mfFindSubControlRecurvice(Control Container)
            {
                List<Control> foundctrl = new List<Control>();

                foreach (Control ctrl in Container.Controls)
                {
                    foundctrl.Add(ctrl);

                    if (ctrl.Controls.Count > 0)
                        foundctrl.AddRange(mfFindSubControlRecurvice(ctrl));

                }
                return foundctrl.ToArray();
            }

            /// <summary>
            /// 하위컨트롤에 대해 입력값에 대한 Validation 체크
            /// </summary>
            /// <param name="frm"></param>
            /// <returns></returns>
            public bool mfCheckValidValueBeforSave(Form frm)
            {
                try
                {
                    Control[] Subctrl = mfFindSubControlRecurvice(frm);
                    for (int i = 0; i < Subctrl.Count(); i++)
                    {
                        if (Subctrl[i].GetType().ToString() == "Infragistics.Win.UltraWinEditors.UltraComboEditor")
                        {
                            Infragistics.Win.UltraWinEditors.UltraComboEditor combo = (Infragistics.Win.UltraWinEditors.UltraComboEditor)Subctrl[i];
                            if (combo.SelectedItem == null && combo.Items.Count > 0 && combo.Visible == true && combo.ReadOnly == false)
                            {
                                QRPGLO.QRPGlobal SysRes = new QRPGLO.QRPGlobal();
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                                string strLang = m_resSys.GetString("SYS_LANG");
                                msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , msg.GetMessge_Text("M001264", strLang)
                                        , msg.GetMessge_Text("M001135", strLang)
                                        , combo.Text + msg.GetMessge_Text("M001507", strLang)
                                        , Infragistics.Win.HAlign.Right);
                                combo.DropDown();
                                return false;
                            }
                        }
                    }
                    return true;
                }
                catch (System.Exception ex)
                {
                    return false;
                    throw (ex);
                }
                finally
                {
                }
            }

        }
        #endregion
    }
}
