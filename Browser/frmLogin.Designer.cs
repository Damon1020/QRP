﻿namespace QRPBrowser
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPassword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uPicServer = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPicLogIn = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPicClose = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.uPicLogo = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // uComboPlant
            // 
            appearance71.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance71;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.DropDownStyle = Infragistics.Win.DropDownStyle.DropDownList;
            this.uComboPlant.Location = new System.Drawing.Point(100, 300);
            this.uComboPlant.MaxLength = 10;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(128, 21);
            this.uComboPlant.TabIndex = 6;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uTextUserID
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUserID.Appearance = appearance1;
            this.uTextUserID.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextUserID.Location = new System.Drawing.Point(100, 328);
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(128, 21);
            this.uTextUserID.TabIndex = 28;
            this.uTextUserID.AfterEnterEditMode += new System.EventHandler(this.uTextUserID_AfterEnterEditMode);
            this.uTextUserID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uTextUserID_KeyPress);
            // 
            // uTextPassword
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPassword.Appearance = appearance3;
            this.uTextPassword.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextPassword.Location = new System.Drawing.Point(100, 356);
            this.uTextPassword.Name = "uTextPassword";
            this.uTextPassword.PasswordChar = '*';
            this.uTextPassword.Size = new System.Drawing.Size(128, 21);
            this.uTextPassword.TabIndex = 29;
            this.uTextPassword.AfterEnterEditMode += new System.EventHandler(this.uTextPassword_AfterEnterEditMode);
            this.uTextPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.uTextPassword_KeyPress);
            // 
            // uPicServer
            // 
            this.uPicServer.BackColor = System.Drawing.Color.Transparent;
            this.uPicServer.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPicServer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uPicServer.Location = new System.Drawing.Point(240, 304);
            this.uPicServer.Name = "uPicServer";
            this.uPicServer.Size = new System.Drawing.Size(72, 20);
            this.uPicServer.TabIndex = 34;
            this.uPicServer.Click += new System.EventHandler(this.uPicServer_Click);
            // 
            // uPicLogIn
            // 
            this.uPicLogIn.BackColor = System.Drawing.Color.Transparent;
            this.uPicLogIn.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPicLogIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uPicLogIn.Location = new System.Drawing.Point(240, 340);
            this.uPicLogIn.Name = "uPicLogIn";
            this.uPicLogIn.Size = new System.Drawing.Size(72, 32);
            this.uPicLogIn.TabIndex = 35;
            this.uPicLogIn.Click += new System.EventHandler(this.uPicLogIn_Click);
            // 
            // uPicClose
            // 
            this.uPicClose.BackColor = System.Drawing.Color.Transparent;
            this.uPicClose.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPicClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uPicClose.Location = new System.Drawing.Point(321, 8);
            this.uPicClose.Name = "uPicClose";
            this.uPicClose.Size = new System.Drawing.Size(24, 20);
            this.uPicClose.TabIndex = 36;
            this.uPicClose.Click += new System.EventHandler(this.uPicClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(112, 432);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 12);
            this.label1.TabIndex = 38;
            this.label1.Text = "                                ";
            // 
            // uPicLogo
            // 
            this.uPicLogo.AutoSize = true;
            this.uPicLogo.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPicLogo.Image = ((object)(resources.GetObject("uPicLogo.Image")));
            this.uPicLogo.Location = new System.Drawing.Point(4, 4);
            this.uPicLogo.Name = "uPicLogo";
            this.uPicLogo.Size = new System.Drawing.Size(142, 31);
            this.uPicLogo.TabIndex = 39;
            // 
            // frmLogin
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::QRPBrowser.Properties.Resources.Login_QRP_PRO;
            this.ClientSize = new System.Drawing.Size(350, 453);
            this.ControlBox = false;
            this.Controls.Add(this.uPicLogo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.uPicClose);
            this.Controls.Add(this.uPicLogIn);
            this.Controls.Add(this.uPicServer);
            this.Controls.Add(this.uTextPassword);
            this.Controls.Add(this.uTextUserID);
            this.Controls.Add(this.uComboPlant);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QRP Login";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPassword;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPicServer;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPicLogIn;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPicClose;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPicLogo;
    }
}