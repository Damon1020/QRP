﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;

using Infragistics.Win.UltraWinTree;

namespace QRPBrowser
{
    public partial class frmMyMenu : Form
    {
        //리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMyMenu()
        {
            InitializeComponent();
        }

        private void frmMyMenu_Load(object sender, EventArgs e)
        {
            try
            {
                InitGrid();
                InitButton();
                InitMyMenu();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrd = new WinGrid();

                // 메뉴 정보 그리드 기본설정 //
                wGrd.mfInitGeneralGrid(this.uGridSYSMenu, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 메뉴정보 그리드 컬럼 설정 //
                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "ProgramID", "프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "ProgramName", "프로그램명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 170, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "UpperProgramID", "상위프로그램ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 100
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "UpperProgramName", "상위프로그램", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "ModuleID", "모듈ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "UseFlagName", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "MenuLevel", "메뉴레벨", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrd.mfSetGridColumn(this.uGridSYSMenu, 0, "MenuOrder", "메뉴순서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                //컬럼 설정후 그리드의 폰트사이즈를 9로한다.
                this.uGridSYSMenu.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSYSMenu.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                m_resSys.Close();

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonOK, "저장", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Save);
                btn.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);

                m_resSys.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void InitMyMenu()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //메뉴정보 BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                // 서버 연결 체크 //
                if (brwChannel.mfCheckQRPServer() == false) return;

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.MyMenu), "MyMenu");
                QRPSYS.BL.SYSPGM.MyMenu clsMyMenu = new QRPSYS.BL.SYSPGM.MyMenu();
                brwChannel.mfCredentials(clsMyMenu);

                DataTable dtMenu = clsMyMenu.mfReadSYSMyMenu(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));

                QRPCOM.QRPUI.WinTree trv = new QRPCOM.QRPUI.WinTree();
                //Tree메뉴 Clear처리
                this.uTreeMenu.Nodes.Clear();

                trv.mfAddNodeToTree(this.uTreeMenu, "ModuleID,ProgramID", "ProgramName", NodeStyle.Standard, true, dtMenu, "ModuleID,UpperProgramID");

                //권한이 있는 메뉴정보 
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth clsUserAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(clsUserAuth);

                DataTable dtAuth = clsUserAuth.mfReadUserAuth_Module(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), "", m_resSys.GetString("SYS_LANG"));

                DataRow[] drMyAuth = dtAuth.Select("ProgramID LIKE 'frm%'");
                DataTable dtMyAuth = drMyAuth.CopyToDataTable();

                uGridSYSMenu.DataSource = dtMyAuth;
                uGridSYSMenu.DataBind();

                clsMyMenu.Dispose();
                m_resSys.Close();

            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        /// <summary>
        /// 메뉴리스트에서 선택한 메뉴를 트리로 이동하기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonLeft_Click(object sender, EventArgs e)
        {
            try
            {
                bool bolExit = false;
                string strKey = "";
                Infragistics.Win.UltraWinTree.UltraTreeNode TreeNode = new Infragistics.Win.UltraWinTree.UltraTreeNode();

                for (int i = 0; i < this.uGridSYSMenu.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridSYSMenu.Rows[i].Cells["Check"].Value) == true)
                    {
                        //트리에 선택한 메뉴가 있는지 체크
                        foreach (UltraTreeNode n in this.uTreeMenu.Nodes)
                        {
                            strKey = this.uGridSYSMenu.Rows[i].Cells["ModuleID"].Value.ToString() + "," + this.uGridSYSMenu.Rows[i].Cells["ProgramID"].Value.ToString(); // ModuleID,ProgramID", "ProgramName",
                            if (strKey == n.Key.ToString())
                            {
                                bolExit = true;
                                break;
                            }
                        }

                        //노드가 없으면 추가한다.
                        if (bolExit == false)
                        {
                            strKey = this.uGridSYSMenu.Rows[i].Cells["ModuleID"].Value.ToString() + "," + this.uGridSYSMenu.Rows[i].Cells["ProgramID"].Value.ToString(); // ModuleID,ProgramID", "ProgramName",
                            string strNodeName = this.uGridSYSMenu.Rows[i].Cells["ProgramName"].Value.ToString();
                            TreeNode = this.uTreeMenu.Nodes.Add(strKey, strNodeName);
                            TreeNode.ExpandAll();
                        }
                        bolExit = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        /// <summary>
        /// 트리에서 선택한 메뉴를 삭제하기.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonRight_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (Infragistics.Win.UltraWinTree.UltraTreeNode TreeNode in this.uTreeMenu.SelectedNodes)
                {
                    TreeNode.Remove();
                }

            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                // 서버 연결 체크 //
                if (brwChannel.mfCheckQRPServer() == false) return;

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.MyMenu), "MyMenu");
                QRPSYS.BL.SYSPGM.MyMenu clsMyMenu = new QRPSYS.BL.SYSPGM.MyMenu();
                brwChannel.mfCredentials(clsMyMenu);

                DataTable dtSaveMenu = clsMyMenu.mfSetDataInfo();
                DataRow row;
                string[] sep = { "," };

                foreach (UltraTreeNode n in this.uTreeMenu.Nodes)
                {
                    row = dtSaveMenu.NewRow();
                    row["UserID"] = m_resSys.GetString("SYS_USERID");
                    string[] arrArg = n.Key.ToString().Split(sep, StringSplitOptions.None);
                    row["ProgramID"] = arrArg[1];
                    dtSaveMenu.Rows.Add(row);
                }

                DialogResult dir = new DialogResult();

                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000943"
                                            , Infragistics.Win.HAlign.Right);

                if (dir == DialogResult.No)
                {
                    return;
                }

                string rtMSG = clsMyMenu.mfSaveSYSMyMenu(dtSaveMenu, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000953",
                                        Infragistics.Win.HAlign.Right);
                }
                clsMyMenu.Dispose();
                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
