﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Resources;

using System.Configuration;
using System.IO;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;

namespace QRPBrowser
{
    public partial class frmSystemInfo : Form
    {
        private string m_strServerKey = "RemoteServer";     //입력한 서버주소를 저장하는 appsetting name
        private string m_strRepServerKey = "RemoteRepServer";     //입력한 대체서버주소를 저장하는 appsetting name
        private string m_strSelLang = "Lang";               //선택한 언어를 저장하는 appsetting name
        private string m_strSelPlantCode = "PlantCode";     //선택한 공장코드를 저장하는 appsetting name
        private string m_strFontName = "FontName";          //선택한 폰트명을 저장하는 appsetting name
        private string m_strFTP = "ftp";
        private bool m_bolEdit = false;

        public frmSystemInfo()
        {
            InitializeComponent();
        }

        private void frmSystemInfo_Load(object sender, EventArgs e)
        {
            this.Icon = Properties.Resources.qrpi;

            WinLabel lbl = new WinLabel();
            lbl.mfSetLabel(this.uLabelServer, "Main Server Info", "Arial", true, false);
            lbl.mfSetLabel(this.uLabelRepServer, "Sub Server Info", "Arial", true, false);
            lbl.mfSetLabel(this.uLabelLang, "Language", "Arial", true, false);
            lbl.mfSetLabel(this.uLabelFontName, "Font", "Arial", true, false);

            WinButton wButton = new WinButton();
            wButton.mfSetButton(this.uButtonOK, "OK", "Arial", Properties.Resources.btn_OK);
            wButton.mfSetButton(this.uButtonClose, "Close", "Arial", Properties.Resources.btn_Stop);

            // FONT 콤보 자동완성기능
            this.uFontNameSysFont.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.Suggest;
            this.uFontNameSysFont.AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;

            //언어, QRP 서버주소, 폰트명을 App.Config에서 읽어서 화면에 보여준다.
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strSelLang))
                switch (ConfigurationSettings.AppSettings[m_strSelLang].ToString())
                {
                    case "KOR":
                        uOptionLang.CheckedIndex = 0;
                        break;
                    case "CHN":
                        uOptionLang.CheckedIndex = 1;
                        break;
                    case "ENG":
                        uOptionLang.CheckedIndex = 2;
                        break;
                }
            else
            {
                uOptionLang.CheckedIndex = 0;
                config.AppSettings.Settings.Add(m_strSelLang, uOptionLang.CheckedItem.DataValue.ToString());
                config.Save(); //ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("AppSettings");
            }

            if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strServerKey))
                uTextServerIP.Text = ConfigurationSettings.AppSettings[m_strServerKey].ToString();

            if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strRepServerKey))
                uTextRepServerIP.Text = ConfigurationSettings.AppSettings[m_strRepServerKey].ToString();

            if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strFontName))
                uFontNameSysFont.Value = ConfigurationSettings.AppSettings[m_strFontName].ToString();
            else
            {
                switch (uOptionLang.CheckedIndex.ToString())
                {
                    case "0":
                        uFontNameSysFont.Value = "굴림";
                        break;
                    case "1":
                        uFontNameSysFont.Value = "SimSun";
                        break;
                    case "2":
                        uFontNameSysFont.Value = "Arial";
                        break;
                }
                config.AppSettings.Settings.Add(m_strFontName, uFontNameSysFont.Value.ToString());
                config.Save(); //ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("AppSettings");
            }               
        }

        private void uButtonOK_Click(object sender, EventArgs e)
        {
            QRPGlobal SysRes = new QRPGlobal();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
            
            if (uTextServerIP.Text == "")
            {
                
                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                          "M001264", "M001230", "M001343", Infragistics.Win.HAlign.Right);
                uTextServerIP.Focus();
                return;
            }

            if (uTextRepServerIP.Text == "")
            {

                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                          "M001264", "M001230", "M001344", Infragistics.Win.HAlign.Right);
                uTextRepServerIP.Focus();
                return;
            }
            m_resSys.Close();
            SaveAppConfigInfo();
            this.Close();
        }

        //입력한 정보를 App.config에 저장함.
        private void SaveAppConfigInfo()
        {
            try
            {
                if (m_bolEdit == true)
                {
                    //app.config에 서버경로, 언어, 폰트를 저장한다.
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    //언어를 변경한 경우 app.config에 저장함.
                    if (config.AppSettings.Settings.AllKeys.Contains(m_strSelLang))
                        config.AppSettings.Settings[m_strSelLang].Value = uOptionLang.CheckedItem.DataValue.ToString();
                    else
                        config.AppSettings.Settings.Add(m_strSelLang, uOptionLang.CheckedItem.DataValue.ToString());

                    
                    //QRP 주 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    uTextServerIP.Text = uTextServerIP.Text.TrimEnd();
                    if (uTextServerIP.Text.Substring(uTextServerIP.Text.Length - 1, 1) != "/")
                        uTextServerIP.Text = uTextServerIP.Text + "/";
                    if (config.AppSettings.Settings.AllKeys.Contains(m_strServerKey))
                        config.AppSettings.Settings[m_strServerKey].Value = uTextServerIP.Text;
                    else
                        config.AppSettings.Settings.Add(m_strServerKey, uTextServerIP.Text);

                    //QRP 서브 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    uTextRepServerIP.Text = uTextRepServerIP.Text.TrimEnd();
                    if (uTextRepServerIP.Text.Substring(uTextRepServerIP.Text.Length - 1, 1) != "/")
                        uTextRepServerIP.Text = uTextRepServerIP.Text + "/";
                    if (config.AppSettings.Settings.AllKeys.Contains(m_strRepServerKey))
                        config.AppSettings.Settings[m_strRepServerKey].Value = uTextRepServerIP.Text;
                    else
                        config.AppSettings.Settings.Add(m_strRepServerKey, uTextRepServerIP.Text);
                    

                    //QRP 서버주소를 변경한 경우 QRPUpdater.exe.config에도 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    //QRPUpdater.exe 파일이 있는경우에만 수행
                    string[] strupdate = Directory.GetFiles(Application.StartupPath, "QRPUpdater.exe");

                    if (strupdate.Length > 0)
                    {
                        System.Configuration.Configuration updateconfig = ConfigurationManager.OpenExeConfiguration("QRPUpdater.exe");
                        //QRP 주 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                        uTextServerIP.Text = uTextServerIP.Text.TrimEnd();
                        if (uTextServerIP.Text.Substring(uTextServerIP.Text.Length - 1, 1) != "/")
                            uTextServerIP.Text = uTextServerIP.Text + "/";
                        if (updateconfig.AppSettings.Settings.AllKeys.Contains(m_strServerKey))
                            updateconfig.AppSettings.Settings[m_strServerKey].Value = uTextServerIP.Text;
                        else
                            updateconfig.AppSettings.Settings.Add(m_strServerKey, uTextServerIP.Text);

                        //QRP 서브 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                        uTextRepServerIP.Text = uTextRepServerIP.Text.TrimEnd();
                        if (uTextRepServerIP.Text.Substring(uTextRepServerIP.Text.Length - 1, 1) != "/")
                            uTextRepServerIP.Text = uTextRepServerIP.Text + "/";
                        if (updateconfig.AppSettings.Settings.AllKeys.Contains(m_strRepServerKey))
                            updateconfig.AppSettings.Settings[m_strRepServerKey].Value = uTextRepServerIP.Text;
                        else
                            updateconfig.AppSettings.Settings.Add(m_strRepServerKey, uTextRepServerIP.Text);

                        // ftp 주소 변경
                        string[] strSplit = { "/" };
                        string[] strServerIP_arr = this.uTextServerIP.Text.Split(strSplit, StringSplitOptions.RemoveEmptyEntries);
                        if (updateconfig.AppSettings.Settings.AllKeys.Contains(m_strFTP))
                            updateconfig.AppSettings.Settings[m_strFTP].Value = "ftp://" + strServerIP_arr[1] + "/";
                        else
                            updateconfig.AppSettings.Settings.Add(m_strFTP, "ftp://" + strServerIP_arr[1] + "/");

                        updateconfig.Save();
                        ConfigurationManager.RefreshSection(updateconfig.AppSettings.SectionInformation.SectionName);
                    }


                    //폰트명을 변경한 경우 app.config에 저장함
                    if (config.AppSettings.Settings.AllKeys.Contains(m_strFontName))
                        config.AppSettings.Settings[m_strFontName].Value = uFontNameSysFont.Value.ToString();
                    else
                        config.AppSettings.Settings.Add(m_strFontName, uFontNameSysFont.Value.ToString());

                    config.Save();
                    //ConfigurationManager.RefreshSection("appSettings");
                    ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.SectionName);
                    
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void uButtonClose_Click(object sender, EventArgs e)
        {
            SaveAppConfigInfo();
            this.Close();
        }

        #region 편집여부 체크
        private void uTextServerIP_ValueChanged(object sender, EventArgs e)
        {
            m_bolEdit = true;
        }

        private void uOptionLang_ValueChanged(object sender, EventArgs e)
        {
            m_bolEdit = true;
        }

        private void uFontNameSysFont_ValueChanged(object sender, EventArgs e)
        {
            m_bolEdit = true;
        }
        #endregion

    }
}
