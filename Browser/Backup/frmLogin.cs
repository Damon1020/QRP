﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Configuration;
using System.Resources;
using System.IO;
using System.Collections;
using System.Net;

using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using QRPSYS.BL.SYSPGM;
using System.Xml;
using System.Xml.Linq;

using System.Runtime.InteropServices;

namespace QRPBrowser
{
    public partial class frmLogin : Form
    {
        #region 시간설정 관련        
        public struct SYSTEMTIME
        {
            public ushort wYear;
            public ushort wMonth;
            public ushort wDayOfWeek;
            public ushort wDay;
            public ushort wHour;
            public ushort wMinute;
            public ushort wSecond;
            public ushort wMilliseconds;
        }

        [DllImport("kernel32", EntryPoint = "GetSystemTime", SetLastError = true)]
        public extern static void GetSystemTime(ref SYSTEMTIME lpSystemTime);

        [DllImport("kernel32", EntryPoint = "SetSystemTime", SetLastError = true)]
        public extern static uint SetSystemTime(ref SYSTEMTIME lpSystemTime);
        #endregion

        private string m_strServerKey = "RemoteServer";     //입력한 서버주소를 저장하는 appsetting name
        private string m_strSelLang = "Lang";               //선택한 언어를 저장하는 appsetting name
        private string m_strSelPlantCode = "PlantCode";     //선택한 공장코드를 저장하는 appsetting name
        private string m_strFontName = "FontName";          //선택한 폰트명을 저장하는 appsetting name
        private bool m_bolEdit = false;
        public string m_strExtArg;

        //전역변수 관리 전역변수
        private static QRPCOM.QRPGLO.QRPGlobal mGlobalVal = new QRPGlobal();

        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmLogin()
        {
            InitializeComponent();
        }

        public frmLogin(string strExtArg)
        {
            InitializeComponent();
            m_strExtArg = strExtArg;
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            try
            {
                Bitmap img = new Bitmap(Properties.Resources.STS_Logo);
                uPicLogo.AutoSize = true;
                uPicLogo.Image = img;

                // 사번입력 대문자만 가능하도록
                this.uTextUserID.ImeMode = ImeMode.Disable;
                this.uTextUserID.CharacterCasing = CharacterCasing.Upper;

                InitLoginForm();
            }
            catch (Exception ex)
            {
            }
            finally
            {                
            }
        }

        /// <summary>
        /// 로그인 Form초기화
        /// </summary>
        private void InitLoginForm()
        {
            try
            {
                this.Icon = Properties.Resources.qrpi;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //언어, QRP 서버주소, 공장를 App.Config에서 읽어서 화면에 보여준다.
                //////if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strSelLang))
                //////    switch (ConfigurationSettings.AppSettings[m_strSelLang].ToString())
                //////    {
                //////        case "KOR":
                //////            uOptionLang.CheckedIndex = 0;
                //////            break;
                //////        case "CHN":
                //////            uOptionLang.CheckedIndex = 1;
                //////            break;
                //////        case "ENG":
                //////            uOptionLang.CheckedIndex = 2;
                //////            break;
                //////    }
                //////else
                //////    uOptionLang.CheckedIndex = 0;

                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                //언어가 설정되어 있지 않은 경우 기본적으로 KOR로 지정한다.
                string strLang = "";
                if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strSelLang))
                    strLang = ConfigurationSettings.AppSettings[m_strSelLang].ToString();
                else
                {
                    strLang = "CHN";
                    config.AppSettings.Settings.Add(m_strSelLang, strLang);
                    config.Save(); //ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("AppSettings");
                }

                //폰트가 설정되어 있지 않은 경우 기본적으로 "굴림"로 지정한다.
                string strFontName = "";
                if (!ConfigurationSettings.AppSettings.AllKeys.Contains(m_strFontName))
                {
                    strFontName = "simsun";
                    config.AppSettings.Settings.Add(m_strFontName, "simsun");
                    config.Save(); //ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("AppSettings");
                }
                else
                {
                    strFontName = ConfigurationSettings.AppSettings[m_strFontName].ToString();
                }

                //QRP서버정보가 없으면 메세지박스를 보여준다.
                if (!ConfigurationSettings.AppSettings.AllKeys.Contains(m_strServerKey))
                {
                    WinMessageBox msg = new WinMessageBox();
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001340", "M001340", "M001341", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    // 공장정보 BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.LogIn), "LogIn");
                    QRPSYS.BL.SYSPGM.LogIn clsLogIn = new QRPSYS.BL.SYSPGM.LogIn();
                    brwChannel.mfCredentials(clsLogIn);
                    DataTable dtPlant = clsLogIn.mfReadPlantForCombo(strLang);

                    QRPCOM.QRPUI.WinComboEditor combo = new QRPCOM.QRPUI.WinComboEditor();
                    //this.uComboPlant.d
                    combo.mfSetComboEditor(this.uComboPlant, true, true, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", "선택"
                        , "PlantCode", "PlantName", dtPlant);

                    //공장정보가 App.Config에 있는 경우 콤보박스에 매핑시킨다.
                    if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strSelPlantCode))
                    {
                        uComboPlant.Value = ConfigurationSettings.AppSettings[m_strSelPlantCode].ToString();

                        ////for (int i = 0; i < uComboPlant.ValueList.ValueListItems.Count; i++)
                        ////{
                        ////    if (uComboPlant.ValueList.ValueListItems[i].DataValue.ToString() == ConfigurationSettings.AppSettings[m_strSelPlantCode].ToString())
                        ////    {
                        ////        uComboPlant.SelectedIndex = i;
                        ////        break;
                        ////    }
                        ////}
                    }
                    clsLogIn.Dispose();
                    uTextUserID.Focus();
                    m_resSys.Close();
                }
            }
            catch (System.Exception ex)
            {            	
            }
            finally
            {
            }

        }
        /// <summary>
        /// System에 필요한 리소스 생성
        /// </summary>
        private void SetSystemResource()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("NameKey", typeof(string));
                dt.Columns.Add("NameValue", typeof(string));

                
                DataRow dr;
                //서버경로
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_SERVERPATH";
                dr["NameValue"] = mGlobalVal.ServerPath;
                dt.Rows.Add(dr);

                //언어
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_LANG";
                dr["NameValue"] = mGlobalVal.Lang;
                dt.Rows.Add(dr);

                //폰트명
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_FONTNAME";
                dr["NameValue"] = mGlobalVal.SystemFontName;
                dt.Rows.Add(dr);

                //로그인사용자IP
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_USERIP";
                dr["NameValue"] = mGlobalVal.UserIP;
                dt.Rows.Add(dr);

                //로그인사용자ID
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_USERID";
                dr["NameValue"] = mGlobalVal.UserID;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["NameKey"] = "SYS_USERNAME";
                dr["NameValue"] = mGlobalVal.UserName;
                dt.Rows.Add(dr);

                //로그인사용자공장코드
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_PLANTCODE";
                dr["NameValue"] = mGlobalVal.PlantCode;
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["NameKey"] = "SYS_PLANTNAME";
                dr["NameValue"] = mGlobalVal.PlantName;
                dt.Rows.Add(dr);

                //로그인사용자부서코드
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_DEPTCODE";
                dr["NameValue"] = mGlobalVal.UserDeptCode;
                dt.Rows.Add(dr);

                //로그인사용자부서명
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_DEPTNAME";
                dr["NameValue"] = mGlobalVal.UserDeptName;
                dt.Rows.Add(dr);

                //로그인사용자이메일
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_EMAIL";
                dr["NameValue"] = mGlobalVal.Email;
                dt.Rows.Add(dr);

                //DB에서 서버관련 정보를 읽기 추가)
                //mGlobalVal.mfMakeSystemInfoResource(dt);
                mfMakeSystemInfoResource(dt);
            }
            catch (Exception ex)
            {
                //throw (ex);
                MessageBox.Show(ex.Message);
            }
            finally
            {
            }
        }


        private static string m_strRootPath = Application.StartupPath.ToString();
        //System관련 리소스
        private string m_strSystemInfoRes = m_strRootPath + "\\SysResources.resources";

        /// <summary>
        ///  System관련 리소스 생성
        /// </summary>
        /// <param name="dtControlLang"></param>
        public void mfMakeSystemInfoResource(DataTable dtSystemInfo)
        {
            try
            {
                /*
                IResourceWriter resWrite = new ResourceWriter(m_strSystemInfoRes);
                for (int i = 0; i < dtSystemInfo.Rows.Count; i++)
                    resWrite.AddResource(dtSystemInfo.Rows[i]["NameKey"].ToString(), dtSystemInfo.Rows[i]["NameValue"].ToString());
                resWrite.Close();
                 */
                 
                using (System.IO.FileStream fs = new System.IO.FileStream(m_strSystemInfoRes, System.IO.FileMode.OpenOrCreate
                                                                        , System.IO.FileAccess.ReadWrite))
                {
                    IResourceWriter resWrite = new ResourceWriter(fs);
                    for (int i = 0; i < dtSystemInfo.Rows.Count; i++)
                        resWrite.AddResource(dtSystemInfo.Rows[i]["NameKey"].ToString(), dtSystemInfo.Rows[i]["NameValue"].ToString());

                    resWrite.Close();
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmError = new QRPCOM.frmErrorInfo(ex);
                frmError.ShowDialog();
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// QRP System 로그인 함수
        /// </summary>
        private void QRPLogin()
        {
            bool bolPWCheck = false;
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                bool bolLoginProg = true;

                //로그인화면에서 정보를 변경한 경우 App.Config에 저장
                if (m_bolEdit == true)
                {
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                    ////////언어를 변경한 경우 app.config에 저장함.
                    //////if (config.AppSettings.Settings.AllKeys.Contains(m_strSelLang))
                    //////    config.AppSettings.Settings[m_strSelLang].Value = uOptionLang.CheckedItem.DataValue.ToString();
                    //////else
                    //////    config.AppSettings.Settings.Add(m_strSelLang, uOptionLang.CheckedItem.DataValue.ToString());

                    ////////QRP 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    //////uTextServerIP.Text = uTextServerIP.Text.TrimEnd();
                    //////if (uTextServerIP.Text.Substring(uTextServerIP.Text.Length - 1, 1) != "/")
                    //////    uTextServerIP.Text = uTextServerIP.Text + "/";
                    //////if (config.AppSettings.Settings.AllKeys.Contains(m_strServerKey))
                    //////    config.AppSettings.Settings[m_strServerKey].Value = uTextServerIP.Text;
                    //////else
                    //////    config.AppSettings.Settings.Add(m_strServerKey, uTextServerIP.Text);

                    //공장을 변경한 경우 app.config에 저장함.
                    if (config.AppSettings.Settings.AllKeys.Contains(m_strSelPlantCode))
                        config.AppSettings.Settings[m_strSelPlantCode].Value = uComboPlant.Value.ToString();
                    else
                        config.AppSettings.Settings.Add(m_strSelPlantCode, uComboPlant.Value.ToString());

                    config.Save(); //ConfigurationSaveMode.Modified);
                    ConfigurationManager.RefreshSection("AppSettings");
                }

                //필수입력사항 체크
                QRPCOM.QRPUI.WinMessageBox msg = new QRPCOM.QRPUI.WinMessageBox();
                if (uComboPlant.Value.ToString() == "")
                {
                    DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.Warning
                                          , 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                                          , "M001227", "M000266", Infragistics.Win.HAlign.Center);
                    uComboPlant.DropDown();
                    bolLoginProg = false;
                }

                if (uTextUserID.Text == "")
                {
                    DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.Warning
                                          , 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                                          , "M001227", "M000054", Infragistics.Win.HAlign.Center);
                    uTextUserID.Focus();
                    bolLoginProg = false;
                }

                if (uTextPassword.Text == "")
                {
                    DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.Warning
                                          , 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264"
                                          , "M001227", "M001342", Infragistics.Win.HAlign.Center);
                    uTextPassword.Focus();
                    bolLoginProg = false;
                }

                if (bolLoginProg == true)
                {
                    // 로그인 체크 BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.LogIn), "LogIn");
                    QRPSYS.BL.SYSPGM.LogIn clsLogIn = new QRPSYS.BL.SYSPGM.LogIn();
                    brwChannel.mfCredentials(clsLogIn);
                    DataTable dtPlant = clsLogIn.mfCheckLogin(uComboPlant.Value.ToString(), uTextUserID.Text, uTextPassword.Text, "KOR"); //uOptionLang.CheckedItem.DataValue.ToString());

                    if (dtPlant.Rows[0]["RESULTCODE"].ToString() == "OK")
                    {
                        bolPWCheck = true;
                        //mGlobalVal
                        mGlobalVal.ServerPath = ConfigurationSettings.AppSettings[m_strServerKey].ToString();
                        mGlobalVal.UserIP = mGlobalVal.mfGetUserIP();
                        mGlobalVal.PlantCode = dtPlant.Rows[0]["PlantCode"].ToString();
                        mGlobalVal.PlantName = dtPlant.Rows[0]["PlantName"].ToString();
                        mGlobalVal.UserDeptCode = dtPlant.Rows[0]["DeptCode"].ToString();
                        mGlobalVal.UserDeptName = dtPlant.Rows[0]["DeptName"].ToString();
                        mGlobalVal.UserID = dtPlant.Rows[0]["UserID"].ToString();
                        mGlobalVal.UserName = dtPlant.Rows[0]["UserName"].ToString();
                        mGlobalVal.Email = dtPlant.Rows[0]["Email"].ToString();

                        //서버시간으로 개인PC의 시간을 설정한다.
                        SetLocalPCDate(dtPlant.Rows[0]["SystemDate"].ToString());

                        if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strSelLang))
                            mGlobalVal.Lang = ConfigurationSettings.AppSettings[m_strSelLang].ToString();
                        else
                            mGlobalVal.Lang = "CHN";

                        if (ConfigurationSettings.AppSettings.AllKeys.Contains(m_strFontName))
                            mGlobalVal.SystemFontName = ConfigurationSettings.AppSettings[m_strFontName].ToString();
                        else
                            mGlobalVal.SystemFontName = "simsun";

                        //사용자정보를 리소스에 저장함
                        m_resSys.Close();
                        SetSystemResource();

                        //Browser화면 열기
                        //mdiBrowser frm = new mdiBrowser(m_strExtArg);
                        //frm.Show();
                        //this.Hide();                        
                        this.Close();
                    }
                    else
                    {
                        string strLang = m_resSys.GetString("SYS_LANG");
                        DialogResult DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME")
                                              , 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, msg.GetMessge_Text("M001264", strLang)
                                              , msg.GetMessge_Text("M001227", strLang), dtPlant.Rows[0]["RESULT"].ToString(), Infragistics.Win.HAlign.Center);
                        m_resSys.Close();
                        
                        bolPWCheck = false;

                        if (dtPlant.Rows[0]["RESULTCODE"].ToString().Equals("IDNG"))
                        {
                            this.uTextUserID.SelectAll();
                            this.uTextUserID.Focus();
                        }
                        else if (dtPlant.Rows[0]["RESULTCODE"].ToString().Equals("PASSNG"))
                        {
                            this.uTextPassword.SelectAll();
                            this.uTextPassword.Focus();
                        }
                    }
                    clsLogIn.Dispose();
                }
                else
                    m_resSys.Close();

            }

            catch (System.Exception ex)
            {
                bolPWCheck = false;
            }
            finally
            {
                if (bolPWCheck)
                {
                    #region 다국어 XML 파일 생성
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.ProgramLang), "ProgramLang");
                    QRPSYS.BL.SYSLAN.ProgramLang clsProgramLang = new QRPSYS.BL.SYSLAN.ProgramLang();
                    brwChannel.mfCredentials(clsProgramLang);

                    DataTable dtRtn = clsProgramLang.mfReadSYSProgramLangForXML();

                    XmlDocument xmlRtn = new XmlDocument();
                    xmlRtn.LoadXml("<?MultiLanguageDocument for=QRPBrowser.exe ?><Root>" + dtRtn.Rows[0][0].ToString() + "</Root>");

                    XmlNode nodeLang = xmlRtn.CreateElement("Lang");
                    xmlRtn.SelectSingleNode("/Root").AppendChild(nodeLang);
                    XmlNodeList xmlLang = xmlRtn.SelectNodes("/Root/*");

                    foreach (XmlNode node in xmlLang)
                    {
                        if (node.Name.Equals("Program"))
                            xmlRtn.SelectSingleNode("/Root/Lang").AppendChild(node);
                    }

                    FileInfo fileLang = new FileInfo(Application.StartupPath + "\\MultiLang.xml");

                    if (!fileLang.Exists)
                    {
                        xmlRtn.Save(Application.StartupPath + "\\MultiLang.xml");
                        fileLang = new FileInfo(Application.StartupPath + "\\MultiLang.xml");
                        fileLang.IsReadOnly = true;
                    }
                    else
                    {
                        XmlDocument xmlMultiLang = new XmlDocument();
                        xmlMultiLang.Load(fileLang.FullName);

                        DateTime dtOld = DateTime.Parse(xmlMultiLang.SelectSingleNode("Root/MaxModifyDate").InnerText);
                        DateTime dtNew = DateTime.Parse(xmlRtn.SelectSingleNode("Root/MaxModifyDate").InnerText);

                        if (dtNew > dtOld)
                        {
                            fileLang.IsReadOnly = false;
                            fileLang.Delete();
                            xmlRtn.Save(Application.StartupPath + "\\MultiLang.xml");
                            fileLang = new FileInfo(Application.StartupPath + "\\MultiLang.xml");
                            fileLang.IsReadOnly = true;
                        }
                    }

                    clsProgramLang.Dispose();
                    dtRtn.Dispose();
                    #endregion

                    #region 다국어 XML 파일 생성(Message)
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSLAN.Message), "Message");
                    QRPSYS.BL.SYSLAN.Message clsMessage = new QRPSYS.BL.SYSLAN.Message();
                    brwChannel.mfCredentials(clsMessage);

                    DataTable dtMessage = clsMessage.mfReadSYSMessageForXML();

                    XmlDocument xmlRtn_Message = new XmlDocument();
                    xmlRtn_Message.LoadXml("<?MultiLanguageMessageDocument for=QRPBrowser.exe ?><Root>" + dtMessage.Rows[0][0].ToString() + "</Root>");

                    XmlNode nodeMessage = xmlRtn_Message.CreateElement("Langs");
                    xmlRtn_Message.SelectSingleNode("/Root").AppendChild(nodeMessage);
                    XmlNodeList xmlMessage = xmlRtn_Message.SelectNodes("/Root/*");

                    foreach (XmlNode node in xmlMessage)
                    {
                        if (node.Name.Equals("Message"))
                            xmlRtn_Message.SelectSingleNode("/Root/Langs").AppendChild(node);
                    }

                    FileInfo fileMessage = new FileInfo(Application.StartupPath + "\\MultiLang_Message.xml");

                    if (!fileMessage.Exists)
                    {
                        xmlRtn_Message.Save(Application.StartupPath + "\\MultiLang_Message.xml");
                        fileMessage = new FileInfo(Application.StartupPath + "\\MultiLang_Message.xml");
                        fileMessage.IsReadOnly = true;
                    }
                    else
                    {
                        XmlDocument xmlMultiLang_Message = new XmlDocument();
                        xmlMultiLang_Message.Load(fileMessage.FullName);

                        DateTime dtOld = DateTime.Parse(xmlMultiLang_Message.SelectSingleNode("Root/MaxModifyDate").InnerText);
                        DateTime dtNew = DateTime.Parse(xmlRtn_Message.SelectSingleNode("Root/MaxModifyDate").InnerText);

                        if (dtNew > dtOld)
                        {
                            fileMessage.IsReadOnly = false;
                            fileMessage.Delete();
                            xmlRtn_Message.Save(Application.StartupPath + "\\MultiLang_Message.xml");
                            fileMessage = new FileInfo(Application.StartupPath + "\\MultiLang_Message.xml");
                            fileMessage.IsReadOnly = true;
                        }
                    }

                    clsMessage.Dispose();
                    dtMessage.Dispose();
                    #endregion
                }
            }
        }

        /// <summary>
        /// 서버시간으로 개인PC의 시간을 설정한다.
        /// </summary>
        /// <param name="strDate">서버 날짜 및 시간</param>
        private void SetLocalPCDate(string strDate)
        {
            try
            {
                //로컬PC의 날짜와 서버 날짜가 틀리면 서버날짜로 바꾼다.
                SYSTEMTIME LocalTime = new SYSTEMTIME();
                SYSTEMTIME ServerTime = new SYSTEMTIME();

                //로컬PC의 날짜 및 시간을 가지고 온다.
                GetSystemTime(ref LocalTime);

                //GetSystemTime 함수는 국제 표준시(UTC, 그리니치 표준시라고도 함)를 반환합니다. 
                //현지 시간을 가져오려면 표준 시간대 및 UTC 간에 시간을 더하거나 빼야 합니다. 
                //예를 들어, UTC로 24:00(자정)는 뉴욕의 경우 오프셋 5시간을 뺀(UTC–5) 19:00입니다.
                TimeSpan ServerOffset = TimeZoneInfo.Local.GetUtcOffset(DateTimeOffset.Now);
                int utcHour = LocalTime.wHour + ServerOffset.Hours;
                LocalTime.wHour = (ushort)utcHour;

                ushort intServerYear = (ushort)Convert.ToUInt32(strDate.Substring(0, 4));
                ushort intServerMonth = (ushort)Convert.ToUInt32(strDate.Substring(5, 2));
                ushort intServerDate = (ushort)Convert.ToUInt32(strDate.Substring(8, 2));
                ushort intServerHour = (ushort)Convert.ToUInt32(strDate.Substring(11, 2));
                ushort intServerMinite = (ushort)Convert.ToUInt32(strDate.Substring(14, 2));

                //GetSystemTime 함수는 국제 표준시(UTC, 그리니치 표준시라고도 함)를 반환합니다. 
                //현지 시간을 가져오려면 표준 시간대 및 UTC 간에 시간을 더하거나 빼야 합니다. 
                //예를 들어, UTC로 24:00(자정)는 뉴욕의 경우 오프셋 5시간을 뺀(UTC–5) 19:00입니다.


                if (intServerYear != LocalTime.wYear || intServerMonth != LocalTime.wMonth || intServerDate != LocalTime.wDay
                    || intServerHour != LocalTime.wHour || intServerMinite != LocalTime.wMinute)
                {
                    ServerTime.wYear = intServerYear;
                    ServerTime.wMonth = intServerMonth;
                    ServerTime.wDay = intServerDate;
                    ServerTime.wDayOfWeek = LocalTime.wDayOfWeek;

                    int utcServerHour = intServerHour - ServerOffset.Hours;
                    ServerTime.wHour = (ushort)utcServerHour; //intServerHour; // LocalTime.wHour;

                    ServerTime.wMinute = intServerMinite;
                    ServerTime.wSecond = LocalTime.wSecond;
                    ServerTime.wMilliseconds = LocalTime.wMilliseconds;
                    SetSystemTime(ref ServerTime);
                }
            }
            catch (System.Exception ex)
            {
                	
            }
            finally
            {
            }
        }

        //언어를 변경하는 경우
        private void uOptionLang_ValueChanged(object sender, EventArgs e)
        {
            m_bolEdit = true;
        }

        //QRP 서버주소를 변경하는 경우
        private void uTextServerIP_ValueChanged(object sender, EventArgs e)
        {
            m_bolEdit = true;
        }

        //공장을 변경하는 경우
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            m_bolEdit = true;
        }

        //사용자ID 입력후 엔터 처리
        private void uTextUserID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
                QRPLogin();
        }

        //암호 입력후 엔터 처리
        private void uTextPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if ((int)e.KeyChar == 13)
                {
                    QRPLogin();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
            }
        }

        private void uPicServer_Click(object sender, EventArgs e)
        {
            frmSystemInfo frm = new frmSystemInfo();
            frm.ShowDialog();
        }

        private void uPicLogIn_Click(object sender, EventArgs e)
        {
            QRPLogin();
        }

        private void uPicClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
            //Application.Exit();
            //m_frmParent.Close();
        }

        private void uTextUserID_AfterEnterEditMode(object sender, EventArgs e)
        {
            this.uTextUserID.SelectAll();
        }

        private void uTextPassword_AfterEnterEditMode(object sender, EventArgs e)
        {
            this.uTextPassword.SelectAll();
        }
    }
}
