﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QRPBrowser
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Bitmap img = new Bitmap(Properties.Resources.Intro_QRP_pro02);
            uPictureBoxIntro.AutoSize = true;
            uPictureBoxIntro.Image = img;
        }
    }
}
