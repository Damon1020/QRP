﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.IO;
using System.Collections;
using System.Resources;
using System.Data;
using System.Net;
using System.Threading;


namespace QRPBrowser
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException +=
                new ThreadExceptionEventHandler(ThreadExceptionHandler);

            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(UnhandledExceptionHandler);


            //Argument 구분자
            string strExtArg = "";
            string strSpeArg = "|[END]";

            //Argument 문자열로 저장
            foreach (string strArg in args)
            {
                strExtArg = strExtArg + strArg + strSpeArg;
                //arrExtArg.Add(strArg);
                //MessageBox.Show(strArg);  
            }

            Application.Run(new mdiBrowser(strExtArg));

            ////catch (ApplicationException ex)
            ////{
            ////    MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace);
            ////}
            ////catch (Exception ex)
            ////{
            ////    MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace);
            ////}
        }

        #region 비정상종료에 대한 예외처리
        private static void ThreadExceptionHandler(object sender, ThreadExceptionEventArgs args)
        {
            HandleException((Exception)args.Exception);
        }

        private static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            HandleException((Exception)args.ExceptionObject);
        }

        private static void HandleException(Exception e)
        {
            try
            {
                if (e == null) return;

                if (!Directory.Exists(Application.StartupPath + "\\errlog\\"))
                    Directory.CreateDirectory(Application.StartupPath + "\\errlog\\");

                StreamWriter file = new System.IO.StreamWriter(
                Application.StartupPath + "\\errlog\\" + DateTime.Now.ToString("yyyy-MM-dd") + "_error.log",
                false,
                System.Text.Encoding.Default
                );

                file.WriteLine(DateTime.Now.ToString() + "|" + e.StackTrace);
                file.Close();

                MessageBox.Show(
                    "A fatal problem has occurred.\n" + e.Message + "\nin: " + e.GetType(),
                    "Program Stopped",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Stop);
            }
            catch (System.Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion
    }
}
