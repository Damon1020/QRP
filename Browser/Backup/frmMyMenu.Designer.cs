﻿namespace QRPBrowser
{
    partial class frmMyMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            this.uGridSYSMenu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTreeMenu = new Infragistics.Win.UltraWinTree.UltraTree();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uButtonLeft = new Infragistics.Win.Misc.UltraButton();
            this.uButtonRight = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSYSMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTreeMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // uGridSYSMenu
            // 
            this.uGridSYSMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSYSMenu.DisplayLayout.Appearance = appearance5;
            this.uGridSYSMenu.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSYSMenu.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSYSMenu.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSYSMenu.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridSYSMenu.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSYSMenu.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridSYSMenu.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSYSMenu.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSYSMenu.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSYSMenu.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridSYSMenu.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSYSMenu.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSYSMenu.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSYSMenu.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridSYSMenu.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSYSMenu.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSYSMenu.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridSYSMenu.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridSYSMenu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSYSMenu.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridSYSMenu.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridSYSMenu.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSYSMenu.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridSYSMenu.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSYSMenu.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSYSMenu.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSYSMenu.Location = new System.Drawing.Point(276, 4);
            this.uGridSYSMenu.Name = "uGridSYSMenu";
            this.uGridSYSMenu.Size = new System.Drawing.Size(364, 228);
            this.uGridSYSMenu.TabIndex = 11;
            this.uGridSYSMenu.Text = "ultraGrid1";
            // 
            // uTreeMenu
            // 
            this.uTreeMenu.Location = new System.Drawing.Point(0, 0);
            this.uTreeMenu.Name = "uTreeMenu";
            this.uTreeMenu.Size = new System.Drawing.Size(244, 232);
            this.uTreeMenu.TabIndex = 12;
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(460, 236);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 14;
            this.uButtonOK.Text = "저장";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(552, 236);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 13;
            this.uButtonClose.Text = "닫기";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // uButtonLeft
            // 
            this.uButtonLeft.Location = new System.Drawing.Point(248, 76);
            this.uButtonLeft.Name = "uButtonLeft";
            this.uButtonLeft.Size = new System.Drawing.Size(24, 28);
            this.uButtonLeft.TabIndex = 15;
            this.uButtonLeft.Text = "<";
            this.uButtonLeft.Click += new System.EventHandler(this.uButtonLeft_Click);
            // 
            // uButtonRight
            // 
            this.uButtonRight.Location = new System.Drawing.Point(248, 108);
            this.uButtonRight.Name = "uButtonRight";
            this.uButtonRight.Size = new System.Drawing.Size(24, 28);
            this.uButtonRight.TabIndex = 16;
            this.uButtonRight.Text = ">";
            this.uButtonRight.Click += new System.EventHandler(this.uButtonRight_Click);
            // 
            // frmMyMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 269);
            this.Controls.Add(this.uButtonRight);
            this.Controls.Add(this.uButtonLeft);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uTreeMenu);
            this.Controls.Add(this.uGridSYSMenu);
            this.Name = "frmMyMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "My Menu";
            this.Load += new System.EventHandler(this.frmMyMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSYSMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTreeMenu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSYSMenu;
        private Infragistics.Win.UltraWinTree.UltraTree uTreeMenu;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.Misc.UltraButton uButtonLeft;
        private Infragistics.Win.Misc.UltraButton uButtonRight;
    }
}