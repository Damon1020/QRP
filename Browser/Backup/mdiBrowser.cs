﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//참조 추가
using System.EnterpriseServices;
using System.IO;
using System.Resources;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using QRPSYS.BL.SYSPGM;

using Infragistics.Win.UltraWinTree;
using Infragistics.Win.UltraWinExplorerBar;
using Infragistics.Win;

using System.Collections;
using System.Threading;
using System.Net;
using System.Diagnostics;

using System.Configuration;

namespace QRPBrowser
{
    public partial class mdiBrowser : Form
    {
        //전역변수 관리 전역변수
        //private static QRPCOM.QRPGLO.QRPGlobal mGlobalVal = new QRPGlobal();
        QRPGlobal SysRes = new QRPGlobal();
        public string m_strExtenalArg;

        private DialogResult m_dr = DialogResult.OK;
        private bool m_bolQRPRunFlag = false;

        public mdiBrowser()
        {
            InitializeComponent();
        }

        public mdiBrowser(string strExtArg)
        {
            InitializeComponent();
            m_strExtenalArg = strExtArg;
        }

        private void mdiBrowser_Load(object sender, EventArgs e)
        {
            try
            {
                ////91번 <-> 92번 변경 주석처리
                bool bolConnectServer = false;
                //3번 연결시도한다.
                for (int i = 0; i < 3; i++)
                {
                    if (CheckQRPServer())
                    {
                        bolConnectServer = true;
                        break;
                    }
                    else
                        Thread.Sleep(1000);
                        //5초 기다렸다가 다시 연결을 시도한다.
                        //Thread.Sleep(5000);
                }

                //연결실패시 다른 서버 접근하도록 변경
                if (bolConnectServer == false)
                {
                    bolConnectServer = ChangeQRPServer();
                }

                //DownloadQRPUpdater(); // --> QRPUpdate.exe 실행
                m_bolQRPRunFlag = true;

                ////bool bolConnectServer = true;   //강제 설정

                if (m_bolQRPRunFlag == false || bolConnectServer == false)
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001264", "M001345", "M001346", Infragistics.Win.HAlign.Right);
                    m_resSys.Close();
                    frmLogin frmLog = new frmLogin(m_strExtenalArg);
                    m_dr = frmLog.ShowDialog();

                    if (m_dr == DialogResult.No)
                    {
                        this.Close();
                        return;
                    }

                    //Application.Exit();
                }
                else
                {

                    frmLogin frmLog = new frmLogin(m_strExtenalArg);
                    m_dr = frmLog.ShowDialog();

                    if (m_dr == DialogResult.No)
                    {
                        this.Close();
                        return;
                    }

                    //로그인 접속 이력정보를 저장함.
                    SaveUserLoginCount();

                    //Browser 시작 위치
                    this.StartPosition = FormStartPosition.CenterScreen;
                    this.Top = 0;

                    //Function Key설정
                    this.KeyPreview = true;
                    this.KeyUp += new System.Windows.Forms.KeyEventHandler(FunctionKeyEvent);

                    //Browser Control초기화
                    InitToolbar();
                    InitExplorerBarMenu();
                    InitTreeMenu();
                    InitTabMenu();
                    InitStatusBar();


                    //메인폼 띄우기                
                    //폼이 들어간 컨테이너인 Panel 설정////////////////

                    //////////////////                    //uPanelForm.AutoScroll = true;
                    //uPanelForm.BorderStyle = UIElementBorderStyle.Solid;
                    //uPanelForm.Location = new Point(uSplitterTree.Left + uSplitterTree.Width, uPictureBoxToolbar.Height + uTabMenu.Top + uTabMenu.Height);
                    //uPanelForm.Size = new Size(this.Width - uSplitterTree.Left - uSplitterTree.Width - 180, this.Height - uPictureBoxToolbar.Height - uTabMenu.Height - uStatusBar.Height - 370);
                    //uPanelForm.AutoScrollMargin = new Size(5, 5);
                    //uPanelForm.AutoScrollMinSize = new Size(100, 500);//////////////////////////////////

                    Form frm = new frmMain();

                    frm.MdiParent = this;
                    frm.ControlBox = false;
                    frm.Dock = DockStyle.Fill;

                    //frm.AutoScroll = true;
                    //frm.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;

                    frm.FormBorderStyle = FormBorderStyle.None;
                    frm.Icon = Properties.Resources.qrpi;
                    frm.WindowState = FormWindowState.Normal;

                    //메인폼 Panel안으로 추가//
                    //uPanelForm.ClientArea.Controls.Add(frm);
                    ///////////////////////////

                    frm.Show();

                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //넘어온 Argument가 있는 경우
                    if (m_strExtenalArg != "")
                    {
                        string[] sep = { "|" };
                        string[] arrArg = m_strExtenalArg.Split(sep, StringSplitOptions.None);

                        //인자에 시스템명 및 연동코드가 있는 경우 화면이동 처리
                        if (arrArg.Count() > 2)
                        {
                            string strIfCode1 = arrArg[0].ToString();
                            string strIfCode2 = arrArg[1].ToString();
                            string strIfCode3 = arrArg[2].ToString();

                            if (strIfCode1.ToUpper() == "QRP" && strIfCode2.ToUpper() != "DEBUG" && strIfCode2.ToUpper() != "")
                            //if (strIfCode.ToUpper() != "DEBUG"  && strIfCode != "")
                            {
                                //실행ID에 따라 DB에서 띄울 화면정보를 가지고 온다.
                                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

                                //외부시스템연동정보 가져오기
                                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.ExtInterfaceInfo), "ExtInterfaceInfo");
                                QRPSYS.BL.SYSPGM.ExtInterfaceInfo clsExtIf = new QRPSYS.BL.SYSPGM.ExtInterfaceInfo();
                                brwChannel.mfCredentials(clsExtIf);

                                //MessageBox.Show(strIfCode1 + ":" + strIfCode2 + ":" + strIfCode3 + ":" + m_resSys.GetString("SYS_PLANTCODE")); // mGlobalVal.PlantCode);

                                DataTable dtExtIf = clsExtIf.mfReadExtInterfaceInfoDetail(m_resSys.GetString("SYS_PLANTCODE"), strIfCode3);

                                //외뷧스템연동정보가 없는 경우 메세지 처리
                                if (dtExtIf.Rows.Count <= 0)
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001264", "M001345", "M001348", Infragistics.Win.HAlign.Right);
                                }
                                else
                                {
                                    QRPCOM.QRPGLO.QRPBrowser SelMenu = new QRPCOM.QRPGLO.QRPBrowser();
                                    if (SelMenu.mfOpenMenuForm(dtExtIf.Rows[0]["ModuleID"].ToString(), dtExtIf.Rows[0]["ProgramID"].ToString(), dtExtIf.Rows[0]["ProgramName"].ToString(), this, m_strExtenalArg) == false)
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001347", "M001347", "M001349", Infragistics.Win.HAlign.Right);
                                        return;
                                    }
                                    else
                                    {
                                        this.uTabMenu.Tabs.Add(dtExtIf.Rows[0]["ModuleID"].ToString() + "," + dtExtIf.Rows[0]["ProgramID"].ToString(), dtExtIf.Rows[0]["ProgramName"].ToString());
                                        this.uTabMenu.SelectedTab = uTabMenu.Tabs[dtExtIf.Rows[0]["ModuleID"].ToString() + "," + dtExtIf.Rows[0]["ProgramID"].ToString()];
                                    }
                                }

                                clsExtIf.Dispose();
                            }
                        }
                    }
                    m_resSys.Close();
                }

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }


        #region IIS연결상태 체크 및 서버정보 변경
        /// <summary>
        /// QRP IIS서버 상태 체크
        /// </summary>
        /// <returns></returns>
        private bool CheckQRPServer()
        {

            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            string RemoteServer = config.AppSettings.Settings["RemoteServer"].Value.ToString() + "menu.rem?wsdl";

            //string RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString() + "menu.rem?wsdl";
            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(RemoteServer);
            httpReq.AllowAutoRedirect = false;
            try
            {
                HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse();
                //MessageBox.Show(httpRes.StatusCode.ToString());
                if (httpRes.StatusCode == HttpStatusCode.OK)
                {
                    httpRes.Close();
                    return true;
                }
                else
                {
                    httpRes.Close();
                    return false;
                }
                
            }
            catch (System.Exception ex)
            {
                return false;
            }
            finally
            {
                
            }
        }
        
        /// <summary>
        /// 주서버 다운으로 주서버주소와 부서버주소 변경
        /// </summary>
        /// <returns></returns>
        private bool ChangeQRPServer()
        {
            bool bolConnectServer = false;
            try
            {
                string strServerKey = "RemoteServer";
                string strRepServerKey = "RemoteRepServer";
                string strServerValue = "";
                string strRepServerValue = "";
                string strFTP = "ftp";
                //app.config에 서버경로, 언어, 폰트를 저장한다.
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                //QRP 주 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                if (config.AppSettings.Settings.AllKeys.Contains(strServerKey))
                    strServerValue = config.AppSettings.Settings[strServerKey].Value;

                //QRP 서브 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                if (config.AppSettings.Settings.AllKeys.Contains(strRepServerKey))
                    strRepServerValue = config.AppSettings.Settings[strRepServerKey].Value;

                if (strServerValue != "" && strRepServerValue != "")
                {
                    config.AppSettings.Settings[strServerKey].Value = strRepServerValue;
                    config.AppSettings.Settings[strRepServerKey].Value = strServerValue;
                    config.Save();
                    System.Configuration.ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.SectionName);
                }


                //QRP 서버주소를 변경한 경우 QRPUpdater.exe.config에도 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                //QRPUpdater.exe 파일이 있는경우에만 수행
                string[] strupdate = Directory.GetFiles(Application.StartupPath, "QRPUpdater.exe");

                if (strupdate.Length > 0)
                {
                    System.Configuration.Configuration updateconfig = ConfigurationManager.OpenExeConfiguration("QRPUpdater.exe");
                    //QRP 주 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    if (updateconfig.AppSettings.Settings.AllKeys.Contains(strServerKey))
                        updateconfig.AppSettings.Settings[strServerKey].Value = strRepServerValue;
                    else
                        updateconfig.AppSettings.Settings.Add(strServerKey, strRepServerValue);

                    //QRP 서브 서버주소를 변경한 경우 app.config에 저장함. (주소 맨끝에 /가 없으면 붙여준다.)
                    if (updateconfig.AppSettings.Settings.AllKeys.Contains(strRepServerKey))
                        updateconfig.AppSettings.Settings[strRepServerKey].Value = strServerValue;
                    else
                        updateconfig.AppSettings.Settings.Add(strRepServerKey, strServerValue);

                    // ftp 주소 변경
                    string[] strSplit = { "/" };
                    string[] strServerIP_arr = strRepServerValue.Split(strSplit, StringSplitOptions.RemoveEmptyEntries);
                    if (updateconfig.AppSettings.Settings.AllKeys.Contains(strFTP))
                        updateconfig.AppSettings.Settings[strFTP].Value = "ftp://" + strServerIP_arr[1] + "/";
                    else
                        updateconfig.AppSettings.Settings.Add(strFTP, "ftp://" + strServerIP_arr[1] + "/");

                    updateconfig.Save();
                    System.Configuration.ConfigurationManager.RefreshSection(updateconfig.AppSettings.SectionInformation.SectionName);
                }

                //바뀐 대체 서버로 연결체크
                for (int i = 0; i < 3; i++)
                {
                    if (CheckQRPServer())
                    {
                        bolConnectServer = true;
                        break;
                    }
                }
                return bolConnectServer;

            }
            catch (System.Exception ex)
            {
                return bolConnectServer;
            }
            finally
            {
            }
        }

        #endregion

        #region 다운로드

        /// <summary>
        /// QRPUpdater를 Download 받음
        /// </summary>
        private void DownloadQRPUpdater()
        {
            try
            {
                ArrayList m_arrDownloadDLLFile = new ArrayList();  //다운로드받을 DLL화일
                NetworkCredential cred;

                //공장코드 가지고 오기
                QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                string strBrowserPath = Application.ExecutablePath;
                int intPos = strBrowserPath.LastIndexOf(@"\");
                string m_strQRPBrowserPath = strBrowserPath.Substring(0, intPos + 1) + "QRPUpdater.exe";

                //실행프로그램 Client경로 지정
                string m_strExePath = Application.ExecutablePath;
                intPos = m_strExePath.LastIndexOf(@"\");
                m_strExePath = m_strExePath.Substring(0, intPos + 1);

                //화일서버 연결정보 가져오기
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                brwChannel.mfCredentials(clsSysAccess);
                
                //QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo("data source=10.60.24.173;user id=qrpusr;password=jinjujin;Initial Catalog=QRP_STS_DEV;persist security info=true");

                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S01");

                //화일서버 연결정보가 없으면 메세지 띄우고 끝
                if (dtSysAccess.Rows.Count > 0)
                {
                    string m_strWebURL = dtSysAccess.Rows[0]["SystemAddressPath"].ToString();
                    string m_strUserID = dtSysAccess.Rows[0]["AccessID"].ToString();
                    string m_strPassword = dtSysAccess.Rows[0]["AccessPassword"].ToString();

                    //////////////////////////////////////////////////////////////////////
                    // QRP System 화일서버 경로 Parsing                     //2012.09.12//
                    //////////////////////////////////////////////////////////////////////
                    string[] strSeparator = { "/", "//" };
                    System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    string strCur_ServerPath = config.AppSettings.Settings["RemoteServer"].Value.ToString();
                    string[] strArr_ServerPath = strCur_ServerPath.Split(strSeparator, StringSplitOptions.RemoveEmptyEntries);
                    string[] strArr_SvrFilePath = m_strWebURL.Split(strSeparator, StringSplitOptions.RemoveEmptyEntries);
                    m_strWebURL = "http://" + strArr_ServerPath[1] + "/" + strArr_SvrFilePath[2] + "/" + strArr_SvrFilePath[3] + "/";

                    ///////////////////////////////////////////////

                    string strDownloadFile;
                    string[] sepFolder = { "/\">" };           //화일정보중에서 Sub폴더정보를 Parsing하기 위한 구분자
                    string[] sepDetailFolder = { "</A>" };     //Sub폴더정보중에서 상세폴더정보를 Parsing하기 위한 구분자
                    string[] sepFile = { "<A HREF=" };         //화일정보중에서 화일명을 Parsing하기 위한 구분자
                    string[] sepLine = { "<br>" };         //화일정보중에서 화일명을 Parsing하기 위한 구분자
                    string[] sepSpace = { "   " };             //화일정보중에서 시간정보를 Parsing하기 위한 구분자
                    string[] sepFileLast = { "\">" };
                    Uri uri;

                    //권한설정
                    cred = new NetworkCredential(m_strUserID, m_strPassword);

                    //WebClient로 QRPDLL폴더의 화일정보를 가지고 온다.
                    WebClient wclient = new WebClient();
                    wclient.Encoding = System.Text.Encoding.UTF8;
                    wclient.Credentials = cred;
                    uri = new Uri(m_strWebURL);
                    strDownloadFile = wclient.DownloadString(uri);
                    wclient.Dispose();

                    //가상디렉토리의 Sub폴더를 Parsing함.
                    string[] arrFullInfo = strDownloadFile.Split(sepLine, StringSplitOptions.None);
                    for (int i = 2; i < arrFullInfo.Count() - 1; i++)
                    {
                        string[] arrFolderInfo = arrFullInfo[i].Substring(2).Split(sepFolder, StringSplitOptions.None);
                        //폴더인 경우
                        if (arrFolderInfo.Count() > 1)
                        {
                            //arrFolderInfo = arrFolderInfo[1].Split(sepDetailFolder, StringSplitOptions.None);
                            //m_arrSourceFolder.Add(arrFolderInfo[0]);
                        }
                        //화일인 경우
                        else
                        {
                            string strFileFullInfo = arrFolderInfo[0];
                            //최종수정일 Parsing
                            string[] arrSourceFileLine = strFileFullInfo.Split(sepFile, StringSplitOptions.None);
                            string[] arrSourceFileDate = arrSourceFileLine[0].Split(sepSpace, StringSplitOptions.None);
                            //DateTime datSourceFileDate = Convert.ToDateTime(Convert.ToDateTime(arrSourceFileDate[0]).ToString("yyyy-MM-dd hh:mm:ss"));
                            DateTime datSourceFileDate = Convert.ToDateTime(ConvertDateTimeToString(arrSourceFileDate[0]));
                            //화일명 Parsing
                            //string[] sepFolderFile = { m_arrSourceFolder[i].ToString() + "/" };
                            string[] arrSourceFile = arrSourceFileLine[1].Split(sepFileLast, StringSplitOptions.None);
                            arrSourceFile = arrSourceFile[1].Split(sepDetailFolder, StringSplitOptions.None);
                            string strSourceFileName = arrSourceFile[0];

                            //Local에 화일이 없거나 Local에 있는 Dll화일의 최종수정일과 비교하여 틀리면 Download받음.
                            string strClientFile = m_strExePath + strSourceFileName;
                            FileInfo strClientFileInfo = new FileInfo(strClientFile);

                            //QRPUpdater.exe는 제외한다 => 자기 자신이기 때문에 실행중 업데이트 못받음 : QRPBrowse에서 실행받도록 한다.
                            if (strSourceFileName == "QRPUpdater.exe")
                            {
                                if (strClientFileInfo.Exists == false || strClientFileInfo.LastWriteTime < datSourceFileDate)
                                {
                                    m_arrDownloadDLLFile.Add(strSourceFileName);
                                    break;
                                }
                            }
                        }
                    }

                    //QRPUpdater.exe를 다운받는다.
                    if (m_arrDownloadDLLFile.Count > 0)
                    {
                        wclient = new WebClient();
                        wclient.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);
                        wclient.Encoding = System.Text.Encoding.UTF8;
                        wclient.Credentials = cred;
                        uri = new Uri(m_strWebURL + m_arrDownloadDLLFile[0]);

                        string strDownloadPath = "";
                        if (m_arrDownloadDLLFile[0].ToString().Contains("/"))
                        {
                            strDownloadPath = m_strExePath + m_arrDownloadDLLFile[0].ToString().Replace("/", "\\");
                            //폴더가 없는 경우 폴더 생성
                            string[] sep = { "/" };
                            string[] arrDownloadFolder = m_arrDownloadDLLFile[0].ToString().Split(sep, StringSplitOptions.None);
                            string strDownloadFolder = m_strExePath + arrDownloadFolder[0] + "\\";
                            if (!Directory.Exists(strDownloadFolder))
                                Directory.CreateDirectory(strDownloadFolder);
                        }
                        else
                            strDownloadPath = m_strExePath + m_arrDownloadDLLFile[0].ToString();

                        //Download시작
                        wclient.DownloadFileAsync(uri, strDownloadPath);
                    }

                    //QRPUpdater.exe를 다운받을 필요가 없으면 QRPUpdate.exe를 실행한다.
                    else
                        RunQRPUpdater();

                }
                //clsSysAccess.Dispose();
                m_resSys.Close();
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        private string ConvertDateTimeToString(string strOldDate)
        {
            try
            {
                string strNewDate = string.Empty;

                if (strOldDate.Contains("오전") || strOldDate.Contains("오후"))
                {
                    string[] sepSpace = { "   ", " ", "  " };
                    string[] sepTime = { ":" };
                    string[] arrDate = strOldDate.Split(sepSpace, StringSplitOptions.None);
                    string[] arrTime = arrDate[2].Split(sepTime, StringSplitOptions.None);

                    if (arrDate[1].Equals("오후") && !arrTime[0].Equals("12"))
                        arrTime[0] = (Convert.ToInt32(arrTime[0]) + 12).ToString();
                    else if (arrDate[1].Equals("오전") && arrTime[0].Equals("12"))
                        arrTime[0] = "0";

                    string strNewTime = string.Empty;
                    for (int i = 0; i < 2; i++)
                        strNewTime = strNewTime + arrTime[i] + ":";

                    strNewTime = strNewTime.Substring(0, strNewTime.Length - 1);

                    //strNewDate = arrDate[0] + " " + arrTime[0] + ":" + arrTime[1] + ":" + arrTime[2];
                    strNewDate = arrDate[0] + " " + strNewTime;
                }
                else
                {
                    strNewDate = strOldDate;
                }

                return strNewDate;
            }
            catch (System.Exception ex)
            {
                return "Error Exception";
            }
            finally
            {
            }
        }

        /// <summary>
        /// QRPUpdate.exe 화일을 Download완료할때 발생하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DownloadFileCompleted(Object sender, AsyncCompletedEventArgs e)
        {
            System.Threading.AutoResetEvent waiter = (System.Threading.AutoResetEvent)e.UserState; ;
            try
            {
                RunQRPUpdater();
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }

        /// <summary>
        /// QRPUpdater를 실행시킴
        /// </summary>
        private void RunQRPUpdater()
        {
            try
            {
                string strSpeArg = "|[END]";
                string strUpdateExePath = Application.ExecutablePath;
                int intPos = strUpdateExePath.LastIndexOf(@"\");
                strUpdateExePath = strUpdateExePath.Substring(0, intPos + 1) + "QRPUpdater.exe";

                //QRPUpdater를 다운받으면 이를 실행한다.
                //외부에서 Argument가 없이 실행이 되는 경우(QRPBrowser.exe를 실행하는 경우)  => QRPUpdater 실행
                if (m_strExtenalArg == "")
                {
                    //QRPUpdater.exe 화일이 있는 경우 QRPUpdater.exe 실행
                    FileInfo file = new FileInfo(strUpdateExePath);
                    if (file.Exists)
                    {
                        System.Diagnostics.Process.Start(strUpdateExePath, m_strExtenalArg);
                        m_dr = DialogResult.No;
                        Application.Exit();
                    }
                    //QRPUpdater.exe 화일이 없는 경우 QRPBrowser.exe 실행
                    else
                    {
                        m_bolQRPRunFlag = true;
                    }
                }

                //외부에서 Argument를 넘겨주고 실행이 되는 경우
                else
                {
                    string[] sep = { strSpeArg };
                    string[] strArg = m_strExtenalArg.Split(sep, StringSplitOptions.None);

                    if (strArg.Count() > 0)
                    {
                        string[] strSep = { "|" };
                        string[] strArgElement = strArg[0].ToString().Split(strSep, StringSplitOptions.None);

                        //QRP가 아닌 타시스템에서 실행되는 경우 => QRPUpdate후 다시 실행
                        if (strArgElement[0].ToString() != "QRP")
                        {
                            //QRP가 아닌 외부PGM에서 Argument를 넘겨 실행이 되는 경우 : QRPUpdater 실행 => QRP실행
                            //외부PGM Argument Format : SYSTEM명|실행ID|인자1|인자2|인자3 
                            // ==> (인자에 공백이 있는 경우 인자값 양쪽을 ""으로 감싼다)
                            //QRPUpdater.exe 화일이 있는 경우 QRPUpdater.exe 실행
                            FileInfo file = new FileInfo(strUpdateExePath);
                            if (file.Exists)
                            {
                                m_strExtenalArg = "QRP|" + m_strExtenalArg;
                                System.Diagnostics.Process.Start(strUpdateExePath, m_strExtenalArg);
                                m_dr = DialogResult.No;
                                Application.Exit();
                            }
                            //QRPUpdater.exe 화일이 없는 경우 QRPBrowser.exe 실행
                            else
                            {
                                m_bolQRPRunFlag = true;
                            }
                        }
                        //QRP에서 실행하는 경우
                        else
                        {
                            //디버그인 경우 
                            //MessageBox.Show(strArg[0]);
                            if (strArgElement[1].ToString() == "DEBUG")
                                //InitLoginForm();
                                m_bolQRPRunFlag = true;
                            else
                            {
                                //타시스템에서 호출한 경우 Browser를 실행하여 화면이동하게 처리
                                if (strArgElement[1].ToString() == "")
                                    m_bolQRPRunFlag = true;
                                else
                                {
                                    if (strArgElement[1].ToString() != "QRP")
                                    {                                        
                                        //타시스템에서 넘어온 ID 및 암호로 로그인 체크 필요?????
                                        //this.Close();
                                    }
                                    m_bolQRPRunFlag = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }
        #endregion

        #region 사용자 로그인 및 프로그램 실행 이력정보
        private void SaveUserLoginCount()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // Call BL
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.LogIn), "Login");
                QRPSYS.BL.SYSPGM.LogIn clsLogin = new QRPSYS.BL.SYSPGM.LogIn();
                brwChannel.mfCredentials(clsLogin);

                string strRtn = clsLogin.mfSaveUserLoginCount(m_resSys.GetString("SYS_PLANTCODE"),
                                                              m_resSys.GetString("SYS_USERID"),
                                                              m_resSys.GetString("SYS_USERIP"),
                                                              m_resSys.GetString("SYS_USERID"));
                clsLogin.Dispose();
                m_resSys.Close();
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        private void SaveUserPGMRunCount(string strProgramID)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // Call BL
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.LogIn), "Login");
                QRPSYS.BL.SYSPGM.LogIn clsLogin = new QRPSYS.BL.SYSPGM.LogIn();
                brwChannel.mfCredentials(clsLogin);

                string strRtn = clsLogin.mfSaveUserPGMRunCount(m_resSys.GetString("SYS_PLANTCODE"),
                                                               m_resSys.GetString("SYS_USERID"), 
                                                               strProgramID,
                                                               m_resSys.GetString("SYS_USERIP"),
                                                               m_resSys.GetString("SYS_USERID"));
                clsLogin.Dispose();
                m_resSys.Close();
            }
            catch (System.Exception ex)
            {

            }
            finally
            {
            }
        }
        #endregion

        #region Browser 초기화
        /// <summary>
        /// 툴바 및 툴바버튼 초기화
        /// </summary>
        private void InitToolbar()
        {
            try
            {
                //툴바를 초기화 한다.                
                System.Drawing.Bitmap imgToolbar = new Bitmap(Properties.Resources.toolbar_on);
                this.uPictureBoxToolbar.Image = imgToolbar;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (m_resSys.GetString("SYS_LANG") == "KOR")
                {
                    //툴바 버튼을 초기화 한다.
                    SetToolBarImage(uPictureBoxSearchOn, Properties.Resources.toolbar_on_search, 230, false);
                    SetToolBarImage(uPictureBoxSearchOff, Properties.Resources.toolbar_off_search, 230, true);

                    SetToolBarImage(uPictureBoxSaveOn, Properties.Resources.toolbar_on_save, 320, false);
                    SetToolBarImage(uPictureBoxSaveOff, Properties.Resources.toolbar_off_save, 320, true);

                    SetToolBarImage(uPictureBoxDelOn, Properties.Resources.toolbar_on_del, 416, false);
                    SetToolBarImage(uPictureBoxDelOff, Properties.Resources.toolbar_off_del, 416, true);

                    SetToolBarImage(uPictureBoxInsertOn, Properties.Resources.toolbar_on_insert, 512, false);
                    SetToolBarImage(uPictureBoxInsertOff, Properties.Resources.toolbar_off_insert, 512, true);

                    SetToolBarImage(uPictureBoxPrintOn, Properties.Resources.toolbar_on_print, 608, false);
                    SetToolBarImage(uPictureBoxPrintOff, Properties.Resources.toolbar_off_print, 608, true);

                    SetToolBarImage(uPictureBoxExcelOn, Properties.Resources.toolbar_on_excel, 704, false);
                    SetToolBarImage(uPictureBoxExcelOff, Properties.Resources.toolbar_off_excel, 704, true);

                    SetToolBarImage(uPictureBoxExitOn, Properties.Resources.toolbar_on_exit, 800, true);
                }
                else if (m_resSys.GetString("SYS_LANG") == "CHN")
                {
                    //툴바 버튼을 초기화 한다.
                    SetToolBarImage(uPictureBoxSearchOn, Properties.Resources.toolbar_on_search_chn, 230, false);
                    SetToolBarImage(uPictureBoxSearchOff, Properties.Resources.toolbar_off_search_chn, 230, true);

                    SetToolBarImage(uPictureBoxSaveOn, Properties.Resources.toolbar_on_save_chn, 320, false);
                    SetToolBarImage(uPictureBoxSaveOff, Properties.Resources.toolbar_off_save_chn, 320, true);

                    SetToolBarImage(uPictureBoxDelOn, Properties.Resources.toolbar_on_del_chn, 416, false);
                    SetToolBarImage(uPictureBoxDelOff, Properties.Resources.toolbar_off_del_chn, 416, true);

                    SetToolBarImage(uPictureBoxInsertOn, Properties.Resources.toolbar_on_Insert_chn, 512, false);
                    SetToolBarImage(uPictureBoxInsertOff, Properties.Resources.toolbar_off_Insert_chn, 512, true);

                    SetToolBarImage(uPictureBoxPrintOn, Properties.Resources.toolbar_on_print_chn, 608, false);
                    SetToolBarImage(uPictureBoxPrintOff, Properties.Resources.toolbar_off_print_chn, 608, true);

                    SetToolBarImage(uPictureBoxExcelOn, Properties.Resources.toolbar_on_excel_chn, 704, false);
                    SetToolBarImage(uPictureBoxExcelOff, Properties.Resources.toolbar_off_excel_chn, 704, true);

                    SetToolBarImage(uPictureBoxExitOn, Properties.Resources.toolbar_on_exit_chn, 800, true);

                }
                m_resSys.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 툴바 버튼 설정
        /// </summary>
        /// <param name="imgToolBar"></param>
        /// <param name="imgResource"></param>
        /// <param name="intLeftPos"></param>
        /// <param name="bolVisible"></param>
        private void SetToolBarImage(Infragistics.Win.UltraWinEditors.UltraPictureBox imgToolBar,
                                     //string strToolResource,
                                     Bitmap imgResource,
                                     int intLeftPos,
                                     bool bolVisible)
        {
            try
            {
                imgToolBar.Image = imgResource;
                imgToolBar.Left = intLeftPos;
                imgToolBar.Visible = bolVisible;
                imgToolBar.Top = 11;
                imgToolBar.AutoSize = true;
                imgToolBar.BorderStyle = UIElementBorderStyle.None;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상위메뉴 보여주기
        /// </summary>
        private void InitExplorerBarMenu()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uExplorerBarMenu.Dock = DockStyle.Fill;
                this.uExplorerBarMenu.Style = UltraExplorerBarStyle.OutlookNavigationPane;

                //최상단에 My Menu 추가
                this.uExplorerBarMenu.Groups.Add("QRPFAV", "My Menu");

                //Root메뉴를 읽어 브라우져메뉴에 표시함.
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                DataTable dt = null;

                //사용자권한을 체크하여 브라우져 메뉴에 표시함.
                //brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                //QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                //brwChannel.mfCredentials(UAuth);
                //dt = UAuth.mfReadUserAuth_Root(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));
                
                ////if (dt.Rows.Count == 0)
                ////{
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                QRPSYS.BL.SYSPGM.Menu Menu = new QRPSYS.BL.SYSPGM.Menu();
                brwChannel.mfCredentials(Menu);
                dt = Menu.mfReadRootMenu(m_resSys.GetString("SYS_LANG"));
                ////}

                foreach(DataRow dr in dt.Rows)
                {
                    this.uExplorerBarMenu.Groups.Add(dr["ProgramID"].ToString(), dr["ProgramName"].ToString());
                }

                for (int i = 0; i <= this.uExplorerBarMenu.Groups.Count - 1; i++)
                    this.uExplorerBarMenu.Groups[i].Settings.Style = GroupStyle.ControlContainer;

                this.uTreeMenu.Parent = this.uExplorerBarMenu.Groups[0].Container;
                this.uTreeMenu.Dock = DockStyle.Fill;

                Menu.Dispose();
                m_resSys.Close();
                //My Menu 메뉴 추가//

            }
            catch (Exception ex)
            {
                StreamWriter stmLog = new StreamWriter("c:\\log.txt", true);
                stmLog.WriteLine(DateTime.Now.ToString() + ":" + ex.Message);
                stmLog.WriteLine(DateTime.Now.ToString() + ":" + ex.StackTrace);
                stmLog.Close();
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tree메뉴 설정
        /// </summary>
        private void InitTreeMenu()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //Tree메뉴 초기화
                QRPCOM.QRPUI.WinTree trv = new WinTree();
                trv.mfInitTree(this.uTreeMenu, Infragistics.Win.UltraWinTree.UltraTreeDisplayStyle.Standard, Infragistics.Win.UltraWinTree.ViewStyle.Standard, Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill, DefaultableBoolean.True, m_resSys.GetString("SYS_FONTNAME"));


                //메뉴정보 BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.MyMenu), "MyMenu");
                QRPSYS.BL.SYSPGM.MyMenu clsMyMenu = new QRPSYS.BL.SYSPGM.MyMenu();
                brwChannel.mfCredentials(clsMyMenu);

                DataTable dtMenu = clsMyMenu.mfReadSYSMyMenu(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));
                //Tree메뉴 Clear처리
                this.uTreeMenu.Nodes.Clear();
                trv.mfAddNodeToTree(this.uTreeMenu, "ModuleID,ProgramID", "ProgramName", NodeStyle.Standard, true, dtMenu, "ModuleID,UpperProgramID");

                clsMyMenu.Dispose();
                m_resSys.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tab메뉴 설정
        /// </summary>
        private void InitTabMenu()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinTabControl tab = new WinTabControl();
                tab.mfInitGeneralTabControl(this.uTabMenu,
                                            Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage,
                                            Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.WhenSelected,
                                            Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.Tab,
                                            m_resSys.GetString("SYS_FONTNAME"));
                m_resSys.Close();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Status Bar 초기화
        /// </summary>
        private void InitStatusBar()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //QRPGlobal SysRes = new QRPGlobal();
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                uStatusBar.Panels["ServerPath"].Text = m_resSys.GetString("SYS_SERVERPATH");
                uStatusBar.Panels["Lang"].Text = m_resSys.GetString("SYS_LANG");
                uStatusBar.Panels["PlantName"].Text = m_resSys.GetString("SYS_PLANTNAME");
                uStatusBar.Panels["UserInfo"].Text = m_resSys.GetString("SYS_USERNAME") + "(" + m_resSys.GetString("SYS_DEPTNAME") + ")";
                m_resSys.Close();

            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 모듈메뉴 선택시 해당메뉴 보여주기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExplorerBarMenu_GroupClick(object sender, GroupEventArgs e)
        {
            try
            {
                //My Menu인 경우
                if (e.Group.Key == "QRPFAV")
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    //메뉴정보 BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.MyMenu), "MyMenu");
                    QRPSYS.BL.SYSPGM.MyMenu clsMyMenu = new QRPSYS.BL.SYSPGM.MyMenu();
                    brwChannel.mfCredentials(clsMyMenu);

                    DataTable dtMenu = clsMyMenu.mfReadSYSMyMenu(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));

                    QRPCOM.QRPUI.WinTree trv = new QRPCOM.QRPUI.WinTree();
                    //Tree메뉴 Clear처리
                    this.uTreeMenu.Nodes.Clear();

                    trv.mfAddNodeToTree(this.uTreeMenu, "ModuleID,ProgramID", "ProgramName", NodeStyle.Standard, true, dtMenu, "ModuleID,UpperProgramID");
                    clsMyMenu.Dispose();
                    m_resSys.Close();
                }
                //모듈인 경우
                else
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                    // 서버 연결 체크 //
                    if (brwChannel.mfCheckQRPServer() == false) return;

                    ////==== 권한 미적용 메뉴 정보 ====////
                    //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.Menu), "Menu");
                    //////QRPSYS.BL.SYSPGM.Menu Menu = new QRPSYS.BL.SYSPGM.Menu();
                    //////brwChannel.mfCredentials(Menu);
                    ////////선택한 모듈의 메뉴정보를 읽어 Tree메뉴에 보여줌.
                    //////DataTable dt = Menu.mfReadModuleMenu(m_resSys.GetString("SYS_USERID"), e.Group.Key.ToString(), m_resSys.GetString("SYS_LANG"));
                    //// ============================= ////

                    ////==== 권한 적용 메뉴 정보 ====////
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                    QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                    brwChannel.mfCredentials(UAuth);
                    //선택한 모듈의 메뉴정보를 읽어 사용자권한에 따른 Tree메뉴에 보여줌.
                    DataTable dt = UAuth.mfReadUserAuth_Module(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), e.Group.Key.ToString(), m_resSys.GetString("SYS_LANG"));
                    //// ============================= ////

                    QRPCOM.QRPUI.WinTree trv = new QRPCOM.QRPUI.WinTree();
                    //Tree메뉴 Clear처리
                    this.uTreeMenu.Nodes.Clear();

                    trv.mfAddNodeToTree(this.uTreeMenu, "ModuleID,ProgramID", "ProgramName", NodeStyle.Standard, true, dt, "ModuleID,UpperProgramID");

                    UAuth.Dispose();
                    m_resSys.Close();
                }

                this.uTreeMenu.Parent = this.uExplorerBarMenu.Groups[e.Group.Index].Container;
                this.uTreeMenu.Dock = DockStyle.Fill;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 메뉴트리선택시 화면인 경우 화면Show 및 Tree추가
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraTreeMenu_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (UltraTreeNode SelNode in this.uTreeMenu.SelectedNodes)
                //foreach (UltraTreeNode SelNode in e.NewSelections)
                {
                    // 선택한 메뉴에서 Form인 경우에만 선택한 Form을 Open함.
                    if (SelNode.Key.Contains("frm") == true)
                    {
                        //선택한 메뉴의 Form이 이미 열려져 있는지 체크함
                        for (int i = 0; i < this.uTabMenu.Tabs.Count; i++)
                        {
                            //이미 열려있으면 해당Form을 보이게하고, 툴바처리를 함.
                            if (this.uTabMenu.Tabs[i].Key.ToString() == SelNode.Key.ToString())
                            {
                                foreach (Form obj in MdiChildren)
                                {
                                    if (SelNode.Key.EndsWith(obj.Name) == true)
                                    {
                                        obj.Select();
                                        this.uTabMenu.SelectedTab = this.uTabMenu.Tabs[SelNode.Key.ToString()];
                                        return;
                                    }
                                }
                            }
                        }

                        QRPCOM.QRPGLO.QRPBrowser SelMenu = new QRPCOM.QRPGLO.QRPBrowser();
                        // 서버 연결 체크 //
                        if (SelMenu.mfCheckQRPServer() == false) return;

                        char[] sep = new char[1];
                        sep[0] = ',';
                        string[] strSelNodeKey = SelNode.Key.Split(sep);

                        //string aa = SelMenu.mfOpenMenuForm(strSelNodeKey[0].ToString(), strSelNodeKey[1].ToString(), SelNode.Text, this);
                        if (SelMenu.mfOpenMenuForm(strSelNodeKey[0].ToString(), strSelNodeKey[1].ToString(), SelNode.Text, this, m_strExtenalArg) == false)
                        {
                            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001347", "M001347", "M001349", Infragistics.Win.HAlign.Right);
                            m_resSys.Close();
                            return;
                        }
                        else
                        {
                            this.uTabMenu.Tabs.Add(SelNode.Key.ToString(), SelNode.Text.ToString());
                            this.uTabMenu.SelectedTab = uTabMenu.Tabs[SelNode.Key.ToString()];
                            SaveUserPGMRunCount(strSelNodeKey[1].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 메뉴Tab에서 Tab을 닫을 경우 화면도 닫히게 처리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraTabMenu_TabClosing(object sender, Infragistics.Win.UltraWinTabControl.TabClosingEventArgs e)
        {
            try
            {
                char[] strSeparator = { ',' };
                string[] arrKey = e.Tab.Key.Split(strSeparator);
                foreach (Form f in MdiChildren)
                {
                    if (f.Name.ToString() == arrKey[1].ToString())
                    {
                        f.Close();
                        uTabMenu.Tabs.Remove(e.Tab);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 메뉴Tab에서 Tab을 선택할 경우 화면이동 처리
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTabMenu_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            try
            {
                char[] strSeparator = { ',' };
                string[] arrKey = e.Tab.Key.Split(strSeparator);
                foreach (Form f in MdiChildren)
                {
                    if (f.Name.ToString() == arrKey[1].ToString())
                    {
                        f.Select();
                        break;
                    }
                }

                if (arrKey.Length > 0)
                    uStatusBar.Panels["FormName"].Text = "화면ID : " + arrKey[1];
                else
                    uStatusBar.Panels["FormName"].Text = "";

                UltraTreeNode SelNode = null;
                SelNode = this.uTreeMenu.GetNodeByKey(e.Tab.Key.ToString());

                if (SelNode != null)
                {
                    this.uTreeMenu.ActiveNode = SelNode;
                    SelNode.Selected = true;
                    this.uTreeMenu.Focus();

                    ////if (arrKey.Length > 0)
                    ////    uStatusBar.Panels["FormName"].Text = "화면ID : " + arrKey[1];
                    ////else
                    ////    uStatusBar.Panels["FormName"].Text = "";

                    //this.uTreeMenu.ActiveNode.BringIntoView();
                }

                //foreach (UltraTreeNode SelNode in this.uTreeMenu.no)
                //{
                //    if (SelNode.Key.ToString() == arrKey[1].ToString())
                //    {
                //        this.uTreeMenu.ActiveNode = SelNode;
                //        break;
                //    }
                //}
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        #region 툴바 버튼 
        /// <summary>
        /// 툴바 검색버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultrapicSearchOn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MdiChildren.Length > 1)
                    ((IToolbar)this.ActiveMdiChild).mfSearch();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 툴바 저장버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultrapicSaveOn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MdiChildren.Length > 1)
                    ((IToolbar)this.ActiveMdiChild).mfSave();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 툴바 삭제버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultrapicDelOn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MdiChildren.Length > 1)
                    ((IToolbar)this.ActiveMdiChild).mfDelete();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 툴바 생성버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultrapicInsertOn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MdiChildren.Length > 1)
                    ((IToolbar)this.ActiveMdiChild).mfCreate();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 툴바 출력버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultrapicPrintOn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MdiChildren.Length > 1)
                    ((IToolbar)this.ActiveMdiChild).mfPrint();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 툴바 엑셀버튼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultrapicExcelOn_Click(object sender, EventArgs e)
        {
            try
            {
                if (MdiChildren.Length > 1)
                    ((IToolbar)this.ActiveMdiChild).mfExcel();
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uPictureBoxExitOn_Click(object sender, EventArgs e)
        {
            ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////QRPCOM.QRPUI.WinMessageBox msgBox = new WinMessageBox();
            //////QRPGlobal SysRes = new QRPGlobal();
            //////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////DialogResult msgResult = msgBox.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
            ////                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////                                                 "시스템 종료", "시스템 종료", "QRP 시스템을 종료하겠습니까?", HAlign.Right);
            ////m_resSys.Close();
            ////if (msgResult == DialogResult.Yes)
            ////{
            ////    m_bolToolBarExitFlag = true;
            ////    Application.Exit();
            ////}
            this.Close();
        }

        //Function Key 설정
        private void FunctionKeyEvent(object sender, KeyEventArgs e)
        {
            try
            {
                //Help
                if (e.KeyCode == Keys.F1)
                {
                    string strBrowserPath = Application.ExecutablePath;
                    int intPos = strBrowserPath.LastIndexOf(@"\");

                    //--Help Local 연동 소스--//
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    string m_strQRPHelpPath = "";
                    if (m_resSys.GetString("SYS_LANG").Equals("KOR"))
                    {
                        m_strQRPHelpPath = strBrowserPath.Substring(0, intPos + 1) + "QRPHelp_KOR.chm";
                    }
                    else if (m_resSys.GetString("SYS_LANG").Equals("CHN"))
                    {
                        m_strQRPHelpPath = strBrowserPath.Substring(0, intPos + 1) + "QRPHelp_CHN.chm";
                    }
                    else if (m_resSys.GetString("SYS_LANG").Equals("ENG"))
                    {
                        m_strQRPHelpPath = strBrowserPath.Substring(0, intPos + 1) + "QRPHelp_ENG.chm";
                    }
                    
                    //string m_strQRPHelpPath = strBrowserPath.Substring(0, intPos + 1) + "QRPHelp.chm";

                    FileInfo fileHelpInfo = new FileInfo(m_strQRPHelpPath);
                    if (fileHelpInfo.Exists)
                    {
                        if (uTabMenu.Tabs.Count <= 0)
                        {
                            System.Windows.Forms.Help.ShowHelp(this, m_strQRPHelpPath);
                        }
                        else
                        {

                            string[] sep = { "," };
                            string[] arrArg = uTabMenu.SelectedTab.Key.Split(sep, StringSplitOptions.None);
                            string m_strQRPHelpForm = arrArg[1] + ".htm";
                            System.Windows.Forms.Help.ShowHelp(this, m_strQRPHelpPath, HelpNavigator.Topic, m_strQRPHelpForm);


                            //////////FileInfo fileHelpInfo = new FileInfo(m_strQRPHelpFormFile);

                            //////////if (fileHelpInfo.Exists)
                            //////////System.Windows.Forms.Help.ShowHelp(this, m_strQRPHelpPath, HelpNavigator.Topic, m_strQRPHelpFormFile1);
                            ////////System.Windows.Forms.Help.ShowHelp(this, m_strQRPHelpPath, HelpNavigator.Topic, m_strQRPHelpFormFile);
                            //////////else
                            //////////    System.Windows.Forms.Help.ShowHelp(this, m_strQRPHelpPath, HelpNavigator.Topic, "UnderConst.htm");
                            ////////////////////////////////
                        }
                    }
                }

                //검색
                if (e.KeyCode == Keys.F2 && uPictureBoxSearchOn.Visible == true)
                    ultrapicSearchOn_Click(sender, e);

                //저장
                else if (e.KeyCode == Keys.F3 && uPictureBoxSaveOn.Visible == true)
                    ultrapicSaveOn_Click(sender, e);

                //삭제
                else if (e.KeyCode == Keys.F4 && uPictureBoxDelOn.Visible == true)
                    ultrapicDelOn_Click(sender, e);

                //신규
                else if (e.KeyCode == Keys.F5 && uPictureBoxInsertOn.Visible == true)
                    ultrapicInsertOn_Click(sender, e);

                //출력
                else if (e.KeyCode == Keys.F6 && uPictureBoxPrintOn.Visible == true)
                    ultrapicPrintOn_Click(sender, e);

                //엑셀
                else if (e.KeyCode == Keys.F7 && uPictureBoxExitOn.Visible == true)
                    ultrapicExcelOn_Click(sender, e);

                //종료
                else if (e.KeyCode == Keys.F8)
                    uPictureBoxExitOn_Click(sender, e);
                //ultrapicExcelOn_Click(sender, e);

                //창 수직 정렬
                else if (e.KeyCode == Keys.F10)
                {
                    Form objLast = null;
                    foreach (Form obj in MdiChildren)
                    {
                        obj.ControlBox = true;
                        obj.FormBorderStyle = FormBorderStyle.Sizable;
                        obj.Dock = DockStyle.None;
                        objLast = obj;
                    }
                    //objLast.Select();
                    LayoutMdi(MdiLayout.TileVertical);
                }

                //창 수평 정렬
                else if (e.KeyCode == Keys.F11)
                {
                    foreach (Form obj in MdiChildren)
                    {
                        obj.ControlBox = true;
                        obj.FormBorderStyle = FormBorderStyle.Sizable;
                        obj.Dock = DockStyle.None;
                    }
                    LayoutMdi(MdiLayout.TileHorizontal);
                }

                //원래대로
                else if (e.KeyCode == Keys.F12)
                {
                    foreach (Form obj in MdiChildren)
                    {
                        obj.ControlBox = false;
                        obj.FormBorderStyle = FormBorderStyle.None;
                        obj.Dock = DockStyle.Fill;
                    }
                }
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        } 

        //툴바 버튼에 대한 마우스 커서 설정
        private void ultrapicSearchOn_MouseMove(object sender, MouseEventArgs e)
        {
            uPictureBoxSearchOn.Cursor = Cursors.Hand;
        }

        private void ultrapicSearchOn_MouseLeave(object sender, EventArgs e)
        {
            uPictureBoxSearchOn.Cursor = Cursors.Default;
        }

        private void ultrapicSaveOn_MouseMove(object sender, MouseEventArgs e)
        {
            uPictureBoxSaveOn.Cursor = Cursors.Hand;
        }

        private void ultrapicSaveOn_MouseLeave(object sender, EventArgs e)
        {
            uPictureBoxSaveOn.Cursor = Cursors.Default;
        }

        private void ultrapicDelOn_MouseMove(object sender, MouseEventArgs e)
        {
            uPictureBoxDelOn.Cursor = Cursors.Hand;
        }

        private void ultrapicDelOn_MouseLeave(object sender, EventArgs e)
        {
            uPictureBoxDelOn.Cursor = Cursors.Default;
        }

        private void ultrapicInsertOn_MouseMove(object sender, MouseEventArgs e)
        {
            uPictureBoxInsertOn.Cursor = Cursors.Hand;
        }

        private void ultrapicInsertOn_MouseLeave(object sender, EventArgs e)
        {
            uPictureBoxInsertOn.Cursor = Cursors.Default;
        }

        private void ultrapicPrintOn_MouseMove(object sender, MouseEventArgs e)
        {
            uPictureBoxPrintOn.Cursor = Cursors.Hand;
        }

        private void ultrapicPrintOn_MouseLeave(object sender, EventArgs e)
        {
            uPictureBoxPrintOn.Cursor = Cursors.Default;
        }

        private void ultrapicExcelOn_MouseMove(object sender, MouseEventArgs e)
        {
            uPictureBoxExcelOn.Cursor = Cursors.Hand;
        }

        private void ultrapicExcelOn_MouseLeave(object sender, EventArgs e)
        {
            uPictureBoxExcelOn.Cursor = Cursors.Default;
        }

        private void ultrapicExitOn_MouseMove(object sender, MouseEventArgs e)
        {
            uPictureBoxExitOn.Cursor = Cursors.Hand;
        }

        private void ultrapicExitOn_MouseLeave(object sender, EventArgs e)
        {
            uPictureBoxExitOn.Cursor = Cursors.Default;
        }

        private void mdiBrowser_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_dr == DialogResult.No)
            {
                e.Cancel = false;
            }
            else
            {
                //if (m_bolToolBarExitFlag == true) return;

                //uPictureBoxExitOn_Click(sender, e);
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msgBox = new WinMessageBox();
                //QRPGlobal SysRes = new QRPGlobal();
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DialogResult msgResult = msgBox.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                 "M001350", "M001350", "M001351", HAlign.Right);
                m_resSys.Close();
                if (msgResult == DialogResult.Yes)
                    e.Cancel = false;
                else
                {
                    e.Cancel = true;
                }
            }

        }
        #endregion

        private void mdiBrowser_MaximumSizeChanged(object sender, EventArgs e)
        {
            foreach (Form frmChild in this.MdiChildren)
            {
                
            }
        }

        private void mdiBrowser_MaximizedBoundsChanged(object sender, EventArgs e)
        {
            
        }

        private void mdiBrowser_ResizeEnd(object sender, EventArgs e)
        {
            
        }

        private void mdiBrowser_SizeChanged(object sender, EventArgs e)
        {
            //if (this.WindowState == FormWindowState.Maximized)
            //{
            //    foreach (Form frmChild in this.MdiChildren)
            //    {
            //        frmChild.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
            //        frmChild.AutoScroll = false;
            //    }
            //}
            //else if (this.WindowState == FormWindowState.Normal)
            //{
            //    foreach (Form frmChild in this.MdiChildren)
            //    {
            //        frmChild.Anchor = AnchorStyles.Left | AnchorStyles.Top;
            //        frmChild.AutoScroll = true;
            //    }
            //}
        }

        private void uTreeMenu_DoubleClick(object sender, EventArgs e)
        {
            //My Menu 등록창 처리
            if (uExplorerBarMenu.SelectedGroup.Key.ToString() == "QRPFAV")
            {
                frmMyMenu frm = new frmMyMenu();
                frm.Icon = Properties.Resources.qrpi;
                //frm.MdiParent = this;
                frm.ShowDialog();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //메뉴정보 BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.MyMenu), "MyMenu");
                QRPSYS.BL.SYSPGM.MyMenu clsMyMenu = new QRPSYS.BL.SYSPGM.MyMenu();
                brwChannel.mfCredentials(clsMyMenu);

                DataTable dtMenu = clsMyMenu.mfReadSYSMyMenu(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));

                QRPCOM.QRPUI.WinTree trv = new QRPCOM.QRPUI.WinTree();
                //Tree메뉴 Clear처리
                this.uTreeMenu.Nodes.Clear();

                trv.mfAddNodeToTree(this.uTreeMenu, "ModuleID,ProgramID", "ProgramName", NodeStyle.Standard, true, dtMenu, "ModuleID,UpperProgramID");
                clsMyMenu.Dispose();
                m_resSys.Close();
            }
        }

    }
}
