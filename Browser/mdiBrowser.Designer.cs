﻿namespace QRPBrowser
{
    partial class mdiBrowser
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel1 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel2 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel3 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel4 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel5 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel6 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel7 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel8 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinStatusBar.UltraStatusPanel ultraStatusPanel9 = new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mdiBrowser));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.uPictureBoxExcelOff = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxExcelOn = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxPrintOff = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxPrintOn = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxInsertOff = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxInsertOn = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxDelOff = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxDelOn = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxSaveOff = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxSaveOn = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxSearchOff = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxSearchOn = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPanelToolBar = new Infragistics.Win.Misc.UltraPanel();
            this.uPictureBoxToolbarSpace = new System.Windows.Forms.PictureBox();
            this.uPictureBoxExitOn = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uPictureBoxToolbar = new System.Windows.Forms.PictureBox();
            this.uPanelTab = new Infragistics.Win.Misc.UltraPanel();
            this.uTabMenu = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.uTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uStatusBar = new Infragistics.Win.UltraWinStatusBar.UltraStatusBar();
            this.uPanelTree = new Infragistics.Win.Misc.UltraPanel();
            this.uExplorerBarMenu = new Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar();
            this.uTreeMenu = new Infragistics.Win.UltraWinTree.UltraTree();
            this.uSplitterTree = new Infragistics.Win.Misc.UltraSplitter();
            this.uPanelToolBar.ClientArea.SuspendLayout();
            this.uPanelToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uPictureBoxToolbarSpace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uPictureBoxToolbar)).BeginInit();
            this.uPanelTab.ClientArea.SuspendLayout();
            this.uPanelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabMenu)).BeginInit();
            this.uTabMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uStatusBar)).BeginInit();
            this.uPanelTree.ClientArea.SuspendLayout();
            this.uPanelTree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uExplorerBarMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTreeMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // uPictureBoxExcelOff
            // 
            this.uPictureBoxExcelOff.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxExcelOff.Location = new System.Drawing.Point(640, 44);
            this.uPictureBoxExcelOff.Name = "uPictureBoxExcelOff";
            this.uPictureBoxExcelOff.Size = new System.Drawing.Size(32, 36);
            this.uPictureBoxExcelOff.TabIndex = 20;
            this.toolTip.SetToolTip(this.uPictureBoxExcelOff, "엑셀(F7)");
            // 
            // uPictureBoxExcelOn
            // 
            this.uPictureBoxExcelOn.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxExcelOn.Location = new System.Drawing.Point(704, 44);
            this.uPictureBoxExcelOn.Name = "uPictureBoxExcelOn";
            this.uPictureBoxExcelOn.Size = new System.Drawing.Size(96, 36);
            this.uPictureBoxExcelOn.TabIndex = 19;
            this.toolTip.SetToolTip(this.uPictureBoxExcelOn, "엑셀(F7)");
            this.uPictureBoxExcelOn.MouseLeave += new System.EventHandler(this.ultrapicExcelOn_MouseLeave);
            this.uPictureBoxExcelOn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ultrapicExcelOn_MouseMove);
            this.uPictureBoxExcelOn.Click += new System.EventHandler(this.ultrapicExcelOn_Click);
            // 
            // uPictureBoxPrintOff
            // 
            this.uPictureBoxPrintOff.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxPrintOff.Location = new System.Drawing.Point(564, 44);
            this.uPictureBoxPrintOff.Name = "uPictureBoxPrintOff";
            this.uPictureBoxPrintOff.Size = new System.Drawing.Size(32, 36);
            this.uPictureBoxPrintOff.TabIndex = 18;
            this.toolTip.SetToolTip(this.uPictureBoxPrintOff, "출력(F6)");
            // 
            // uPictureBoxPrintOn
            // 
            this.uPictureBoxPrintOn.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxPrintOn.Location = new System.Drawing.Point(608, 44);
            this.uPictureBoxPrintOn.Name = "uPictureBoxPrintOn";
            this.uPictureBoxPrintOn.Size = new System.Drawing.Size(96, 36);
            this.uPictureBoxPrintOn.TabIndex = 17;
            this.toolTip.SetToolTip(this.uPictureBoxPrintOn, "출력(F6)");
            this.uPictureBoxPrintOn.MouseLeave += new System.EventHandler(this.ultrapicPrintOn_MouseLeave);
            this.uPictureBoxPrintOn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ultrapicPrintOn_MouseMove);
            this.uPictureBoxPrintOn.Click += new System.EventHandler(this.ultrapicPrintOn_Click);
            // 
            // uPictureBoxInsertOff
            // 
            this.uPictureBoxInsertOff.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxInsertOff.Location = new System.Drawing.Point(488, 44);
            this.uPictureBoxInsertOff.Name = "uPictureBoxInsertOff";
            this.uPictureBoxInsertOff.Size = new System.Drawing.Size(32, 36);
            this.uPictureBoxInsertOff.TabIndex = 16;
            this.toolTip.SetToolTip(this.uPictureBoxInsertOff, "신규(F5)");
            // 
            // uPictureBoxInsertOn
            // 
            this.uPictureBoxInsertOn.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxInsertOn.Location = new System.Drawing.Point(512, 44);
            this.uPictureBoxInsertOn.Name = "uPictureBoxInsertOn";
            this.uPictureBoxInsertOn.Size = new System.Drawing.Size(96, 36);
            this.uPictureBoxInsertOn.TabIndex = 15;
            this.toolTip.SetToolTip(this.uPictureBoxInsertOn, "신규(F5)");
            this.uPictureBoxInsertOn.MouseLeave += new System.EventHandler(this.ultrapicInsertOn_MouseLeave);
            this.uPictureBoxInsertOn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ultrapicInsertOn_MouseMove);
            this.uPictureBoxInsertOn.Click += new System.EventHandler(this.ultrapicInsertOn_Click);
            // 
            // uPictureBoxDelOff
            // 
            this.uPictureBoxDelOff.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxDelOff.Location = new System.Drawing.Point(412, 44);
            this.uPictureBoxDelOff.Name = "uPictureBoxDelOff";
            this.uPictureBoxDelOff.Size = new System.Drawing.Size(32, 36);
            this.uPictureBoxDelOff.TabIndex = 14;
            this.toolTip.SetToolTip(this.uPictureBoxDelOff, "삭제(F4)");
            // 
            // uPictureBoxDelOn
            // 
            this.uPictureBoxDelOn.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxDelOn.Location = new System.Drawing.Point(416, 44);
            this.uPictureBoxDelOn.Name = "uPictureBoxDelOn";
            this.uPictureBoxDelOn.Size = new System.Drawing.Size(96, 36);
            this.uPictureBoxDelOn.TabIndex = 13;
            this.toolTip.SetToolTip(this.uPictureBoxDelOn, "삭제(F4)");
            this.uPictureBoxDelOn.MouseLeave += new System.EventHandler(this.ultrapicDelOn_MouseLeave);
            this.uPictureBoxDelOn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ultrapicDelOn_MouseMove);
            this.uPictureBoxDelOn.Click += new System.EventHandler(this.ultrapicDelOn_Click);
            // 
            // uPictureBoxSaveOff
            // 
            this.uPictureBoxSaveOff.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxSaveOff.Location = new System.Drawing.Point(336, 44);
            this.uPictureBoxSaveOff.Name = "uPictureBoxSaveOff";
            this.uPictureBoxSaveOff.Size = new System.Drawing.Size(96, 36);
            this.uPictureBoxSaveOff.TabIndex = 12;
            this.toolTip.SetToolTip(this.uPictureBoxSaveOff, "저장(F3)");
            // 
            // uPictureBoxSaveOn
            // 
            this.uPictureBoxSaveOn.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxSaveOn.Location = new System.Drawing.Point(336, 44);
            this.uPictureBoxSaveOn.Name = "uPictureBoxSaveOn";
            this.uPictureBoxSaveOn.Size = new System.Drawing.Size(96, 36);
            this.uPictureBoxSaveOn.TabIndex = 11;
            this.toolTip.SetToolTip(this.uPictureBoxSaveOn, "저장(F3)");
            this.uPictureBoxSaveOn.MouseLeave += new System.EventHandler(this.ultrapicSaveOn_MouseLeave);
            this.uPictureBoxSaveOn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ultrapicSaveOn_MouseMove);
            this.uPictureBoxSaveOn.Click += new System.EventHandler(this.ultrapicSaveOn_Click);
            // 
            // uPictureBoxSearchOff
            // 
            this.uPictureBoxSearchOff.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxSearchOff.Location = new System.Drawing.Point(260, 44);
            this.uPictureBoxSearchOff.Name = "uPictureBoxSearchOff";
            this.uPictureBoxSearchOff.Size = new System.Drawing.Size(32, 36);
            this.uPictureBoxSearchOff.TabIndex = 10;
            this.toolTip.SetToolTip(this.uPictureBoxSearchOff, "검색(F2)");
            // 
            // uPictureBoxSearchOn
            // 
            this.uPictureBoxSearchOn.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxSearchOn.Location = new System.Drawing.Point(224, 44);
            this.uPictureBoxSearchOn.Name = "uPictureBoxSearchOn";
            this.uPictureBoxSearchOn.Size = new System.Drawing.Size(96, 36);
            this.uPictureBoxSearchOn.TabIndex = 9;
            this.toolTip.SetToolTip(this.uPictureBoxSearchOn, "검색(F2)");
            this.uPictureBoxSearchOn.MouseLeave += new System.EventHandler(this.ultrapicSearchOn_MouseLeave);
            this.uPictureBoxSearchOn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ultrapicSearchOn_MouseMove);
            this.uPictureBoxSearchOn.Click += new System.EventHandler(this.ultrapicSearchOn_Click);
            // 
            // uPanelToolBar
            // 
            // 
            // uPanelToolBar.ClientArea
            // 
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxToolbarSpace);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxExitOn);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxExcelOff);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxExcelOn);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxPrintOff);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxPrintOn);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxInsertOff);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxInsertOn);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxDelOff);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxDelOn);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxSaveOff);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxSaveOn);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxSearchOff);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxSearchOn);
            this.uPanelToolBar.ClientArea.Controls.Add(this.uPictureBoxToolbar);
            this.uPanelToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.uPanelToolBar.Location = new System.Drawing.Point(0, 0);
            this.uPanelToolBar.Name = "uPanelToolBar";
            this.uPanelToolBar.Size = new System.Drawing.Size(1264, 55);
            this.uPanelToolBar.TabIndex = 4;
            // 
            // uPictureBoxToolbarSpace
            // 
            this.uPictureBoxToolbarSpace.BackgroundImage = global::QRPBrowser.Properties.Resources.toolbar_back;
            this.uPictureBoxToolbarSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uPictureBoxToolbarSpace.Location = new System.Drawing.Point(1052, 0);
            this.uPictureBoxToolbarSpace.Name = "uPictureBoxToolbarSpace";
            this.uPictureBoxToolbarSpace.Size = new System.Drawing.Size(212, 55);
            this.uPictureBoxToolbarSpace.TabIndex = 22;
            this.uPictureBoxToolbarSpace.TabStop = false;
            // 
            // uPictureBoxExitOn
            // 
            this.uPictureBoxExitOn.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureBoxExitOn.Location = new System.Drawing.Point(800, 44);
            this.uPictureBoxExitOn.Name = "uPictureBoxExitOn";
            this.uPictureBoxExitOn.Size = new System.Drawing.Size(32, 36);
            this.uPictureBoxExitOn.TabIndex = 21;
            this.uPictureBoxExitOn.MouseLeave += new System.EventHandler(this.ultrapicExitOn_MouseLeave);
            this.uPictureBoxExitOn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ultrapicExitOn_MouseMove);
            this.uPictureBoxExitOn.Click += new System.EventHandler(this.uPictureBoxExitOn_Click);
            // 
            // uPictureBoxToolbar
            // 
            this.uPictureBoxToolbar.Dock = System.Windows.Forms.DockStyle.Left;
            this.uPictureBoxToolbar.Location = new System.Drawing.Point(0, 0);
            this.uPictureBoxToolbar.Name = "uPictureBoxToolbar";
            this.uPictureBoxToolbar.Size = new System.Drawing.Size(1052, 55);
            this.uPictureBoxToolbar.TabIndex = 2;
            this.uPictureBoxToolbar.TabStop = false;
            // 
            // uPanelTab
            // 
            // 
            // uPanelTab.ClientArea
            // 
            this.uPanelTab.ClientArea.Controls.Add(this.uTabMenu);
            this.uPanelTab.Dock = System.Windows.Forms.DockStyle.Top;
            this.uPanelTab.Location = new System.Drawing.Point(0, 55);
            this.uPanelTab.Name = "uPanelTab";
            this.uPanelTab.Size = new System.Drawing.Size(1264, 24);
            this.uPanelTab.TabIndex = 6;
            // 
            // uTabMenu
            // 
            this.uTabMenu.AllowTabMoving = true;
            appearance1.BackColor = System.Drawing.Color.DarkGray;
            this.uTabMenu.Appearance = appearance1;
            this.uTabMenu.Controls.Add(this.uTabSharedControlsPage1);
            this.uTabMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uTabMenu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uTabMenu.Location = new System.Drawing.Point(0, 0);
            this.uTabMenu.Name = "uTabMenu";
            this.uTabMenu.SharedControlsPage = this.uTabSharedControlsPage1;
            this.uTabMenu.Size = new System.Drawing.Size(1264, 24);
            this.uTabMenu.TabIndex = 0;
            this.uTabMenu.TabClosing += new Infragistics.Win.UltraWinTabControl.TabClosingEventHandler(this.ultraTabMenu_TabClosing);
            this.uTabMenu.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.uTabMenu_SelectedTabChanged);
            // 
            // uTabSharedControlsPage1
            // 
            this.uTabSharedControlsPage1.Location = new System.Drawing.Point(1, 20);
            this.uTabSharedControlsPage1.Name = "uTabSharedControlsPage1";
            this.uTabSharedControlsPage1.Size = new System.Drawing.Size(1260, 1);
            // 
            // uStatusBar
            // 
            this.uStatusBar.Location = new System.Drawing.Point(0, 721);
            this.uStatusBar.Name = "uStatusBar";
            appearance2.FontData.BoldAsString = "False";
            appearance2.TextHAlignAsString = "Center";
            ultraStatusPanel1.Appearance = appearance2;
            ultraStatusPanel1.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            ultraStatusPanel1.Key = "ServerTitle";
            ultraStatusPanel1.Text = "Server";
            ultraStatusPanel1.Width = 50;
            appearance3.TextHAlignAsString = "Center";
            ultraStatusPanel2.Appearance = appearance3;
            ultraStatusPanel2.BorderStyle = Infragistics.Win.UIElementBorderStyle.Rounded3;
            ultraStatusPanel2.Key = "ServerPath";
            ultraStatusPanel2.Width = 250;
            ultraStatusPanel3.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            ultraStatusPanel3.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.AutoStatusText;
            ultraStatusPanel3.Width = 180;
            appearance4.TextHAlignAsString = "Center";
            ultraStatusPanel4.Appearance = appearance4;
            ultraStatusPanel4.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            ultraStatusPanel4.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Date;
            appearance5.TextHAlignAsString = "Center";
            ultraStatusPanel5.Appearance = appearance5;
            ultraStatusPanel5.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            ultraStatusPanel5.Style = Infragistics.Win.UltraWinStatusBar.PanelStyle.Time;
            appearance6.TextHAlignAsString = "Center";
            ultraStatusPanel6.Appearance = appearance6;
            ultraStatusPanel6.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            ultraStatusPanel6.Key = "Lang";
            ultraStatusPanel6.Width = 50;
            appearance7.TextHAlignAsString = "Center";
            ultraStatusPanel7.Appearance = appearance7;
            ultraStatusPanel7.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            ultraStatusPanel7.Key = "PlantName";
            ultraStatusPanel7.Width = 150;
            appearance8.TextHAlignAsString = "Center";
            ultraStatusPanel8.Appearance = appearance8;
            ultraStatusPanel8.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            ultraStatusPanel8.Key = "UserInfo";
            ultraStatusPanel8.Width = 200;
            ultraStatusPanel9.BorderStyle = Infragistics.Win.UIElementBorderStyle.WindowsVista;
            ultraStatusPanel9.Key = "FormName";
            ultraStatusPanel9.Width = 150;
            this.uStatusBar.Panels.AddRange(new Infragistics.Win.UltraWinStatusBar.UltraStatusPanel[] {
            ultraStatusPanel1,
            ultraStatusPanel2,
            ultraStatusPanel3,
            ultraStatusPanel4,
            ultraStatusPanel5,
            ultraStatusPanel6,
            ultraStatusPanel7,
            ultraStatusPanel8,
            ultraStatusPanel9});
            this.uStatusBar.Size = new System.Drawing.Size(1264, 25);
            this.uStatusBar.TabIndex = 11;
            this.uStatusBar.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2003;
            // 
            // uPanelTree
            // 
            // 
            // uPanelTree.ClientArea
            // 
            this.uPanelTree.ClientArea.Controls.Add(this.uExplorerBarMenu);
            this.uPanelTree.ClientArea.Controls.Add(this.uTreeMenu);
            this.uPanelTree.Dock = System.Windows.Forms.DockStyle.Left;
            this.uPanelTree.Location = new System.Drawing.Point(0, 79);
            this.uPanelTree.Name = "uPanelTree";
            this.uPanelTree.Size = new System.Drawing.Size(190, 642);
            this.uPanelTree.TabIndex = 13;
            // 
            // uExplorerBarMenu
            // 
            this.uExplorerBarMenu.Location = new System.Drawing.Point(2, 20);
            this.uExplorerBarMenu.Name = "uExplorerBarMenu";
            this.uExplorerBarMenu.Size = new System.Drawing.Size(186, 364);
            this.uExplorerBarMenu.TabIndex = 1;
            this.uExplorerBarMenu.GroupClick += new Infragistics.Win.UltraWinExplorerBar.GroupClickEventHandler(this.ExplorerBarMenu_GroupClick);
            // 
            // uTreeMenu
            // 
            this.uTreeMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.uTreeMenu.Location = new System.Drawing.Point(0, 0);
            this.uTreeMenu.Name = "uTreeMenu";
            this.uTreeMenu.Size = new System.Drawing.Size(190, 16);
            this.uTreeMenu.TabIndex = 0;
            this.uTreeMenu.Click += new System.EventHandler(this.ultraTreeMenu_Click);
            this.uTreeMenu.DoubleClick += new System.EventHandler(this.uTreeMenu_DoubleClick);
            // 
            // uSplitterTree
            // 
            this.uSplitterTree.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            appearance9.BackColor = System.Drawing.Color.Tomato;
            this.uSplitterTree.ButtonAppearance = appearance9;
            this.uSplitterTree.Location = new System.Drawing.Point(190, 79);
            this.uSplitterTree.Name = "uSplitterTree";
            this.uSplitterTree.RestoreExtent = 190;
            this.uSplitterTree.Size = new System.Drawing.Size(5, 642);
            this.uSplitterTree.TabIndex = 15;
            // 
            // mdiBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 746);
            this.Controls.Add(this.uSplitterTree);
            this.Controls.Add(this.uPanelTree);
            this.Controls.Add(this.uStatusBar);
            this.Controls.Add(this.uPanelTab);
            this.Controls.Add(this.uPanelToolBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "mdiBrowser";
            this.Text = "QRP Browser";
            this.Load += new System.EventHandler(this.mdiBrowser_Load);
            this.MaximizedBoundsChanged += new System.EventHandler(this.mdiBrowser_MaximizedBoundsChanged);
            this.SizeChanged += new System.EventHandler(this.mdiBrowser_SizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mdiBrowser_FormClosing);
            this.ResizeEnd += new System.EventHandler(this.mdiBrowser_ResizeEnd);
            this.MaximumSizeChanged += new System.EventHandler(this.mdiBrowser_MaximumSizeChanged);
            this.uPanelToolBar.ClientArea.ResumeLayout(false);
            this.uPanelToolBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uPictureBoxToolbarSpace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uPictureBoxToolbar)).EndInit();
            this.uPanelTab.ClientArea.ResumeLayout(false);
            this.uPanelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTabMenu)).EndInit();
            this.uTabMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uStatusBar)).EndInit();
            this.uPanelTree.ClientArea.ResumeLayout(false);
            this.uPanelTree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uExplorerBarMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTreeMenu)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.ToolTip toolTip;
        private Infragistics.Win.Misc.UltraPanel uPanelToolBar;
        private System.Windows.Forms.PictureBox uPictureBoxToolbar;
        private Infragistics.Win.Misc.UltraPanel uPanelTab;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxSearchOn;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxSearchOff;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxExitOn;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxExcelOff;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxExcelOn;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxPrintOff;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxPrintOn;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxInsertOff;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxInsertOn;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxDelOff;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxDelOn;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxSaveOff;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureBoxSaveOn;
        private System.Windows.Forms.PictureBox uPictureBoxToolbarSpace;
        private Infragistics.Win.UltraWinStatusBar.UltraStatusBar uStatusBar;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabMenu;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage uTabSharedControlsPage1;
        private Infragistics.Win.Misc.UltraPanel uPanelTree;
        private Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar uExplorerBarMenu;
        private Infragistics.Win.UltraWinTree.UltraTree uTreeMenu;
        private Infragistics.Win.Misc.UltraSplitter uSplitterTree;
    }
}



