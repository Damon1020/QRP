﻿namespace QRPBrowser
{
    partial class frmSystemInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.ValueListItem valueListItem1 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem2 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.ValueListItem valueListItem3 = new Infragistics.Win.ValueListItem();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.uOptionLang = new Infragistics.Win.UltraWinEditors.UltraOptionSet();
            this.uTextServerIP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uFontNameSysFont = new Infragistics.Win.UltraWinEditors.UltraFontNameEditor();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            this.uLabelServer = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLang = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFontName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRepServer = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRepServerIP = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uOptionLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextServerIP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uFontNameSysFont)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepServerIP)).BeginInit();
            this.SuspendLayout();
            // 
            // uOptionLang
            // 
            this.uOptionLang.BorderStyle = Infragistics.Win.UIElementBorderStyle.None;
            valueListItem1.DataValue = "KOR";
            valueListItem1.DisplayText = "KOR";
            valueListItem2.DataValue = "CHN";
            valueListItem2.DisplayText = "CHN";
            valueListItem3.DataValue = "ENG";
            valueListItem3.DisplayText = "ENG";
            this.uOptionLang.Items.AddRange(new Infragistics.Win.ValueListItem[] {
            valueListItem1,
            valueListItem2,
            valueListItem3});
            this.uOptionLang.Location = new System.Drawing.Point(140, 76);
            this.uOptionLang.Name = "uOptionLang";
            this.uOptionLang.Size = new System.Drawing.Size(144, 16);
            this.uOptionLang.TabIndex = 33;
            this.uOptionLang.ValueChanged += new System.EventHandler(this.uOptionLang_ValueChanged);
            // 
            // uTextServerIP
            // 
            appearance1.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextServerIP.Appearance = appearance1;
            this.uTextServerIP.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextServerIP.Location = new System.Drawing.Point(136, 20);
            this.uTextServerIP.Name = "uTextServerIP";
            this.uTextServerIP.Size = new System.Drawing.Size(236, 21);
            this.uTextServerIP.TabIndex = 32;
            this.uTextServerIP.ValueChanged += new System.EventHandler(this.uTextServerIP_ValueChanged);
            // 
            // uFontNameSysFont
            // 
            this.uFontNameSysFont.Location = new System.Drawing.Point(140, 100);
            this.uFontNameSysFont.Name = "uFontNameSysFont";
            this.uFontNameSysFont.Size = new System.Drawing.Size(128, 21);
            this.uFontNameSysFont.TabIndex = 34;
            this.uFontNameSysFont.Text = "ultraFontNameEditor1";
            this.uFontNameSysFont.ValueChanged += new System.EventHandler(this.uFontNameSysFont_ValueChanged);
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(104, 132);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 35;
            this.uButtonOK.Text = "OK";
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(196, 132);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 36;
            this.uButtonClose.Text = "Close";
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // uLabelServer
            // 
            this.uLabelServer.Location = new System.Drawing.Point(8, 20);
            this.uLabelServer.Name = "uLabelServer";
            this.uLabelServer.Size = new System.Drawing.Size(124, 20);
            this.uLabelServer.TabIndex = 37;
            this.uLabelServer.Text = "ultraLabel1";
            // 
            // uLabelLang
            // 
            this.uLabelLang.Location = new System.Drawing.Point(8, 72);
            this.uLabelLang.Name = "uLabelLang";
            this.uLabelLang.Size = new System.Drawing.Size(124, 20);
            this.uLabelLang.TabIndex = 38;
            this.uLabelLang.Text = "ultraLabel1";
            // 
            // uLabelFontName
            // 
            this.uLabelFontName.Location = new System.Drawing.Point(8, 100);
            this.uLabelFontName.Name = "uLabelFontName";
            this.uLabelFontName.Size = new System.Drawing.Size(124, 20);
            this.uLabelFontName.TabIndex = 39;
            this.uLabelFontName.Text = "ultraLabel2";
            // 
            // uLabelRepServer
            // 
            this.uLabelRepServer.Location = new System.Drawing.Point(8, 48);
            this.uLabelRepServer.Name = "uLabelRepServer";
            this.uLabelRepServer.Size = new System.Drawing.Size(124, 20);
            this.uLabelRepServer.TabIndex = 41;
            this.uLabelRepServer.Text = "ultraLabel1";
            // 
            // uTextRepServerIP
            // 
            appearance2.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepServerIP.Appearance = appearance2;
            this.uTextRepServerIP.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextRepServerIP.Location = new System.Drawing.Point(136, 48);
            this.uTextRepServerIP.Name = "uTextRepServerIP";
            this.uTextRepServerIP.Size = new System.Drawing.Size(236, 21);
            this.uTextRepServerIP.TabIndex = 40;
            // 
            // frmSystemInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(377, 169);
            this.Controls.Add(this.uLabelRepServer);
            this.Controls.Add(this.uTextRepServerIP);
            this.Controls.Add(this.uLabelFontName);
            this.Controls.Add(this.uLabelLang);
            this.Controls.Add(this.uLabelServer);
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uFontNameSysFont);
            this.Controls.Add(this.uOptionLang);
            this.Controls.Add(this.uTextServerIP);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSystemInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "System Info";
            this.Load += new System.EventHandler(this.frmSystemInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uOptionLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextServerIP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uFontNameSysFont)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRepServerIP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraOptionSet uOptionLang;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextServerIP;
        private Infragistics.Win.UltraWinEditors.UltraFontNameEditor uFontNameSysFont;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.Misc.UltraLabel uLabelServer;
        private Infragistics.Win.Misc.UltraLabel uLabelLang;
        private Infragistics.Win.Misc.UltraLabel uLabelFontName;
        private Infragistics.Win.Misc.UltraLabel uLabelRepServer;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRepServerIP;
    }
}