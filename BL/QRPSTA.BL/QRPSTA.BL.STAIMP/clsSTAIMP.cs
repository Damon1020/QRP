﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 통계관리                                              */
/* 프로그램ID   : clsSTAIMP.cs                                          */
/* 프로그램명   : 수입검사통계관리                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-10-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Using 추가
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPSTA.BL.STAIMP
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]       //,10, 1048576)]  //enable, minPoolSize, maxPoolSize
    [Serializable]
    [System.EnterpriseServices.Description("ImportStaticD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ImportStaticD : ServicedComponent
    {

        /// <summary>
        /// 수입검사 통계분석
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strMaterialCode"></param>
        /// <param name="strVendorCode"></param>
        /// <param name="strInspectItemCode"></param>
        /// <param name="strADateFrom"></param>
        /// <param name="strADateTo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSImportStaticD(string strPlantCode, string strMaterialCode, string strVendorCode, string strInspectItemCode
                                                , string strADateFrom, string strADateTo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strADateFrom", ParameterDirection.Input, SqlDbType.Char, strADateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strADateTo", ParameterDirection.Input, SqlDbType.Char, strADateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSImportStaticD", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 그리드 더블클릭으로 검색할때
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strMaterialCode"></param>
        /// <param name="strVendorCode"></param>
        /// <param name="strInspectItemCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqLot(String strPlantCode, String strMaterialCode, String strVendorCode, String strInspectItemCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqLotForStats", dtParam);
                return dt;

            }
            catch(Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 수입검사 규격서에서 수입검사가 완료된 문서만 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strMaterialCode"></param>
        /// <param name="strVendorCode"></param>
        /// <param name="strSpecNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatStatic_InspectItemCombo(String strPlantCode, String strMaterialCode, String strVendorCode, String strSpecNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.VarChar, strSpecNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpec_InspectItem", dtParam);
                return dt;

            }
            catch (Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 규격서에서 수입검사가 완료된 문서만 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strSpecNo">Revision No</param>
        /// <param name="strDataType">데이터 유형</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatStatic_InspectItemCombo(String strPlantCode, String strMaterialCode
                                        , String strVendorCode, String strSpecNo, string strDataType, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.VarChar, strSpecNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, strDataType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpec_InspectItem1", dtParam);
                return dt;

            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 통계분석 X/MR 관리도 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strRev">Revision No</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialGrade">자재등급</param>
        /// <param name="strInspectDateFrom">검색일From</param>
        /// <param name="strInspectDateTo">검색일To</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatStatic_XMR(String strPlantCode, String strMaterialCode, String strRev, String strInspectItemCode
                                            , String strVendorCode, string strMaterialGrade, String strInspectDateFrom, String strInspectDateTo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRev", ParameterDirection.Input, SqlDbType.NVarChar, strRev, 300);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialGrade", ParameterDirection.Input, SqlDbType.VarChar, strMaterialGrade, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatStaticD_XMR", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// XBar/R 관리도 작성용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strMaterialCode"></param>
        /// <param name="strVendorCode"></param>
        /// <param name="strInspectItemCode"></param>
        /// <param name="strInspectDateFrom"></param>
        /// <param name="strInspectDateTo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatStatic_XBar(String strPlantCode, String strMaterialCode, String strVendorCode, String strInspectItemCode, String strInspectDateFrom, String strInspectDateTo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatStaticD_XBar", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 통계분석 X/Bar 관리도 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strRev">Revision No</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialGrade">자재등급</param>
        /// <param name="strInspectDateFrom">검색일From</param>
        /// <param name="strInspectDateTo">검색일To</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatStatic_XBar_Xn(String strPlantCode, String strMaterialCode, String strRev, String strInspectItemCode
                                            , String strVendorCode, string strMaterialGrade, String strInspectDateFrom, String strInspectDateTo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRev", ParameterDirection.Input, SqlDbType.NVarChar, strRev, 300);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialGrade", ParameterDirection.Input, SqlDbType.VarChar, strMaterialGrade, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatStaticD_XBar_Xn", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 수입통계분석화면 자재코드에 따른 Rev 콤보박스 설정 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드/param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSTAIMP_Rev_Combo(string strPlantCode, string strMaterialCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_STAIMP_Rev_Combo", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 통계분석 계수형 데이터 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strRev">Revision No</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialGrade">자재등급</param>
        /// <param name="strInspectDateFrom">검색일From</param>
        /// <param name="strInspectDateTo">검색일To</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatStaticD_Count(String strPlantCode, String strMaterialCode, String strRev, String strInspectItemCode
                                            , String strVendorCode, string strMaterialGrade, String strInspectDateFrom, String strInspectDateTo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRev", ParameterDirection.Input, SqlDbType.NVarChar, strRev, 300);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialGrade", ParameterDirection.Input, SqlDbType.VarChar, strMaterialGrade, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatStaticD_Count", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }
    /// <summary>
    /// XMR관리분석도
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]       //,10, 1048576)]  //enable, minPoolSize, maxPoolSize
    [Serializable]
    [System.EnterpriseServices.Description("ImportStaticCL")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ImportStaticCL : ServicedComponent
    {
        public DataTable mfReadINSMatStaticCL_XMR(String strPlantCode, String strMaterialCode, String strInspectItemCode, String strInspectDate)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strInspectDate, 10);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatStaticCL_XMR_XBar", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]       //,10, 1048576)]  //enable, minPoolSize, maxPoolSize
    [Serializable]
    [System.EnterpriseServices.Description("MatTarget")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatTarget : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("AYear", typeof(string));
                dtRtn.Columns.Add("AQuarter", typeof(string));
                dtRtn.Columns.Add("ConsumableTypeCode", typeof(string));
                dtRtn.Columns.Add("Target", typeof(decimal));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                DataColumn[] dc = new DataColumn[4];
                dc[0] = dtRtn.Columns["PlantCode"];
                dc[1] = dtRtn.Columns["AYear"];
                dc[2] = dtRtn.Columns["AQuarter"];
                dc[3] = dtRtn.Columns["ConsumableTypeCode"];

                dtRtn.PrimaryKey = dc;

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입 품질목표 등록/조회 화면 검색 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strQuarter">분기</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatTarget(string strPlantCode, string strYear, string strQuarter, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, strQuarter, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatTarget", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수입 품질목표 등록/조회 화면 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장할 데이터테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMatTarget(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AQuarter"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ConsumableTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_dblTarget", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["Target"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMatTarget", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break; 
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수입 품질목표 등록/조회 화면 삭제 메소드
        /// </summary>
        /// <param name="dtDelete">삭제정보 담긴 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMatTarget(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["AYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["AQuarter"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["ConsumableTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSMatTarget", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("INSMaterialReport")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class INSMaterialReport : ServicedComponent
    {
        /// <summary>
        /// 수입검사 월별현황 조회(제품타입 기준)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">검색연도</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialInspect_frmSTA0086_D1(string strPlantCode, string strYear)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspect_frmSTA0086_D1", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect(); 
            }
        }

        /// <summary>
        /// 수입검사 월별현황 조회(제품타입, 고객사 기준)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialInspect_frmSTA0086_D2(string strPlantCode, string strYear, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspect_frmSTA0086_D2", dtParam);

                return dtRtn;
            }
            catch(Exception ex)
            {
                return dtRtn;
                throw(ex);
            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect(); 
            }
        }

        /// <summary>
        /// 수입검사 불량현황 수입품질목표 검색 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMatTypeCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspect_frmSTA0091_Target(string strPlantCode, string strYear, string strMatTypeCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strMatType", ParameterDirection.Input, SqlDbType.VarChar, strMatTypeCode, 40);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspect_frmSTA0091_Target", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw(ex);
            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정검사 품질목표 검색(월별)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strConsumableTypeCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspect_frmSTA0085_D1(string strPlantCode, string strYear, string strConsumableTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strConsumableTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspect_frmSTA0085_D1", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 수입검사 종합현황 수입이상 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strConsumableTypeCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspect_frmSTA0085_D2(string strPlantCode, string strYear, string strConsumableTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strConsumableTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspect_frmSTA0085_D2", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
            }
        }
        /// <summary>
        /// 수입검사 종합현황 공정이상 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strConsumableTypeCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspect_frmSTA0085_D3(string strPlantCode, string strYear, string strConsumableTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strConsumableTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspect_frmSTA0085_D3", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
            }
        }

    }
}
