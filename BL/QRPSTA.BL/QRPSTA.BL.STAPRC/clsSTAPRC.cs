﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 통계관리                                              */
/* 프로그램ID   : clsSTAPRC.cs                                          */
/* 프로그램명   : 공정검사통계관리                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-10-29                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Using 추가
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPSTA.BL.STAPRC
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcessStaticD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcessStaticD : ServicedComponent
    {
        /// <summary>
        /// 공정검사 통계분석 조회 SP
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strInspectItemCode">검사항목</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strFromADate">분석일자From</param>
        /// <param name="strToADate">분석일자To</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStaticD(string strPlantCode, string strProductCode, string strInspectItemCode, string strCustomerCode
                                                , string strFromADate, string strToADate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_StrInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromADate", ParameterDirection.Input, SqlDbType.VarChar, strFromADate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToADate", ParameterDirection.Input, SqlDbType.VarChar, strToADate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessStaticD", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 통계분석 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strStackSeq">Stack</param>
        /// <param name="strGeneration">세대</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strADate">분석일</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStaticD_Detail(string strPlantCode, string strProductCode, string strStackSeq, string strGeneration
                                                        , string strInspectItemCode, string strADate, string strProcessCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, strGeneration, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strADate", ParameterDirection.Input, SqlDbType.VarChar, strADate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessStaticDetail", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정검사 X/MR 관리도 검사항목 공정검사 규격서에서 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPackage">Package 코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStatic_InspectItemCombo(string strPlantCode, string strPackage, string strProcessGroupCode, string strProcessCode, string strCustomerCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcInspectSpec_InspectItem", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정 검사 통계분석 X/MR 관리도 검사값 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strStackSeq">Stack</param>
        /// <param name="strGeneration">세대</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strInspectDateFrom">검사일자(From)</param>
        /// <param name="strInspectDateTo">검사일자(To)</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStatic_XMR(string strPlantCode, string strPackage, string strProcessCode, string strProcessGroupCode, string strCustomerCode, string strStackSeq, string strGeneration
                                                    , string strInspectItemCode, string strEquipCode, string strInspectDateFrom, string strInspectDateTo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, strGeneration, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessStaticD_XMR", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정 검사 통계분석 X/Bar 관리도 검사값 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strStackSeq">Stack</param>
        /// <param name="strGeneration">세대</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strInspectDateFrom">검사일자(From)</param>
        /// <param name="strInspectDateTo">검사일자(To)</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStatic_XBar(string strPlantCode, string strPackage, string strProcessCode, string strProcessGroupCode, string strCustomerCode, string strStackSeq, string strGeneration
                                                    , string strInspectItemCode, string strEquipCode, string strInspectDateFrom, string strInspectDateTo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, strGeneration, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessStaticD_XBar", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정 검사 통계분석 X/Bar 관리도 검사값 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strStackSeq">Stack</param>
        /// <param name="strGeneration">세대</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strInspectDateFrom">검사일자(From)</param>
        /// <param name="strInspectDateTo">검사일자(To)</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStatic_XBar_Xn(string strPlantCode, string strPackage, string strProcessCode, string strProcessGroupCode, string strCustomerCode, string strStackSeq, string strGeneration
                                                    , string strInspectItemCode, string strEquipCode, string strInspectDateFrom, string strInspectDateTo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, strGeneration, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessStaticD_XBar_Xn", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정검사 통계분석 검사항목 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strProcessGroupCode">공정그룹</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strDataType">데이터유형</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStatic_InspectItemCombo(string strPlantCode, string strPackage, string strProcessGroupCode, string strProcessCode
                            , string strCustomerCode, string strDataType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, strDataType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcInspectSpec_InspectItem1", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 통계분석 계수형 데이터 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strStackSeq">Stack</param>
        /// <param name="strGeneration">세대</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strInspectDateFrom">검사일자(From)</param>
        /// <param name="strInspectDateTo">검사일자(To)</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStaticD_Count(string strPlantCode, string strPackage, string strProcessCode, string strProcessGroupCode, string strCustomerCode, string strStackSeq, string strGeneration
                                                    , string strInspectItemCode, string strEquipCode, string strInspectDateFrom, string strInspectDateTo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, strGeneration, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessStaticD_Count", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcessStaticCL")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcessStaticCL : ServicedComponent
    {
        /// <summary>
        /// X관리도 그리기 위한 상한/하한 관리한계선 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strInspectDate">검사일</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcessStaticCL_XMR(string strPlantCode, string strProductCode, string strProcessCode, string strInspectItemCode, string strInspectDate)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strInspectDate, 10);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessStaticCL_XMR_XBar", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("INSProcessReport")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class INSProcessReport : ServicedComponent
    {
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0068(string strPlantCode, string strFromInspectDate, string strToInspectDate, string strCustomerCode,
                                                         string strProductActionType, string strProcInspectType, string strPACKAGEGROUP_1, string strPACKAGE,
                                                         string strDProcOperationType, string strInspectTypeCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strFromInspectDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strToInspectDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcInspectType", ParameterDirection.Input, SqlDbType.VarChar, strProcInspectType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strPACKAGEGROUP_1", ParameterDirection.Input, SqlDbType.VarChar, strPACKAGEGROUP_1, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPACKAGE", ParameterDirection.Input, SqlDbType.VarChar, strPACKAGE, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDProcOperationType", ParameterDirection.Input, SqlDbType.VarChar, strDProcOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectTypeCode, 10);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0068", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정검사현황조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInspectDateFrom">검사일From</param>
        /// <param name="strInspectDateTo">검사일To</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strProductActionType">제품구분</param>
        /// <param name="strPackageGroup_1">제품그룹1</param>
        /// <param name="strPackageGroup_2">제품그룹2</param>
        /// <param name="strPackageGroup_3">제품그룹3</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strProcInspectType">공정검사구분</param>
        /// <param name="strInspectResultFlag">판정구분</param>
        /// <param name="strDetailProcessOperationType">공정Type</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strCustomerProductSpec">고객사제품코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strInspectTypeCode">검사유형</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0067(string strPlantCode, string strInspectDateFrom, string strInspectDateTo, string strCustomerCode,
                                                         string strProductActionType, string strPackageGroup_1, string strPackageGroup_2, string strPackageGroup_3, string strPackage,
                                                         string strProcInspectType, string strInspectResultFlag, string strDetailProcessOperationType, string strProcessCode,
                                                         string strCustomerProductSpec, string strLotNo, string strInspectTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_1", ParameterDirection.Input, SqlDbType.VarChar, strPackageGroup_1, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_2", ParameterDirection.Input, SqlDbType.VarChar, strPackageGroup_2, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_3", ParameterDirection.Input, SqlDbType.VarChar, strPackageGroup_3, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcInspectType", ParameterDirection.Input, SqlDbType.VarChar, strProcInspectType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, strInspectResultFlag, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.VarChar, strDetailProcessOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerProductSpec", ParameterDirection.Input, SqlDbType.NVarChar, strCustomerProductSpec, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0067", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정검사현황조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInspectDateFrom">검사일From</param>
        /// <param name="strInspectDateTo">검사일To</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strProductActionType">제품구분</param>
        /// <param name="strPackageGroup_1">제품그룹1</param>
        /// <param name="strPackageGroup_2">제품그룹2</param>
        /// <param name="strPackageGroup_3">제품그룹3</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strProcInspectType">공정검사구분</param>
        /// <param name="strInspectResultFlag">판정구분</param>
        /// <param name="strDetailProcessOperationType">공정Type</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strCustomerProductSpec">고객사제품코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strInspectTypeCode">검사유형</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0067_PSTS(string strPlantCode, string strInspectDateFrom, string strInspectDateTo, string strCustomerCode,
                                                         string strProductActionType, string strPackageGroup_1, string strPackageGroup_2, string strPackageGroup_3, string strPackage,
                                                         string strProcInspectType, string strInspectResultFlag, string strDetailProcessOperationType, string strProcessCode,
                                                         string strCustomerProductSpec, string strLotNo, string strInspectTypeCode,string strEquipCode,string strInspectItem, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_1", ParameterDirection.Input, SqlDbType.VarChar, strPackageGroup_1, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_2", ParameterDirection.Input, SqlDbType.VarChar, strPackageGroup_2, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_3", ParameterDirection.Input, SqlDbType.VarChar, strPackageGroup_3, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcInspectType", ParameterDirection.Input, SqlDbType.VarChar, strProcInspectType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, strInspectResultFlag, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.VarChar, strDetailProcessOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerProductSpec", ParameterDirection.Input, SqlDbType.NVarChar, strCustomerProductSpec, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItem", ParameterDirection.Input, SqlDbType.VarChar, strInspectItem, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0067_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS현황조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromReqDate">의뢰일From</param>
        /// <param name="strToReqDate">의뢰일To</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strProductActionType">제품구분</param>
        /// <param name="strDetailProcessOperationType">공정Type</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0069(string strPlantCode, string strFromReqDate, string strToReqDate, string strCustomerCode,
                                                         string strProductActionType, string strDetailProcessOperationType, string strProcessCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromReqDate", ParameterDirection.Input, SqlDbType.VarChar, strFromReqDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToReqDate", ParameterDirection.Input, SqlDbType.VarChar, strToReqDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.VarChar, strDetailProcessOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0069", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// QCN현황조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromReqDate">검사일From</param>
        /// <param name="strToReqDate">검사일To</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strProductActionType">제품구분</param>
        /// <param name="strDetailProcessOperationType">공정Type</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0070(string strPlantCode, string strYearMonth, string strCustomerCode,
                                                         string strProductActionType, string strDetailProcessOperationType, string strProcessCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYearMonth", ParameterDirection.Input, SqlDbType.VarChar, strYearMonth, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.VarChar, strDetailProcessOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0070", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 월별품질 종합현황 : 공정Type 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strProductActionType">제품Type</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0071_D1(string strPlantCode, string strYear, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0071_D1", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 월별품질 종합현황 : 공정Type 및 Package별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strProductActionType">제품Type</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0071_D2(string strPlantCode, string strYear, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0071_D2", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 월별품질 종합현황 : 공정Type 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strProductActionType">제품Type</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0071_D1_PSTS(string strPlantCode, string strYear, string strProductActionType, string strCustomerCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0071_D1_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 월별품질 종합현황 : 공정Type 및 Package별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strProductActionType">제품Type</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0071_D2_PSTS(string strPlantCode, string strYear, string strProductActionType, string strCustomerCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0071_D2_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 일별품질 종합현황 : 공정Type별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strMonth">조회개월</param>
        /// <param name="strProductActionType">제품Type</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0072_D1(string strPlantCode, string strYear, string strMonth, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0072_D1", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 일별품질 종합현황 : 공정Type 및 Package별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strMonth">조회개월</param>
        /// <param name="strProductActionType">제품Type</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0072_D2(string strPlantCode, string strYear, string strMonth, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0072_D2", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 일별품질 종합현황 : 공정Type별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strMonth">조회개월</param>
        /// <param name="strProductActionType">제품Type</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0072_D1_PSTS(string strPlantCode, string strYear, string strMonth, string strProductActionType, string strCustomerCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dtRtn = sql.mfExecReadStoredProc_frmSTA0072(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0072_D1_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 일별품질 종합현황 : 공정Type 및 Package별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strMonth">조회개월</param>
        /// <param name="strProductActionType">제품Type</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0072_D2_PSTS(string strPlantCode, string strYear, string strMonth, string strProductActionType, string strCustomerCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0072_D2_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 종합현황 귀책부서별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회연도</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0073_D1(string strPlantCode, string strYear, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0073_D1", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 종합현황 고객사별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회연도</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0073_D2(string strPlantCode, string strYear, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0073_D2", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS 종합현황 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strSearchType">조회선택</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0074_D1(string strPlantCode, string strYear, string strSearchType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strSearchType", ParameterDirection.Input, SqlDbType.NVarChar, strSearchType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "QRP.up_Select_INSProcInspect_frmSTA0074_D1", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// CCS 종합현황 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strSearchType">조회선택</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0074_D1_PSTS(string strPlantCode, string strYear, string strSearchType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strSearchType", ParameterDirection.Input, SqlDbType.NVarChar, strSearchType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "QRP.up_Select_INSProcInspect_frmSTA0074_D1_PSTS", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// CCS 종합현황 고객사별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strSearchType">조회선택</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0074_D2(string strPlantCode, string strYear, string strSearchType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strSearchType", ParameterDirection.Input, SqlDbType.NVarChar, strSearchType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "QRP.up_Select_INSProcInspect_frmSTA0074_D2", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// CCS 종합현황 고객사별 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strSearchType">조회선택</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0074_D2_PSTS(string strPlantCode, string strYear, string strSearchType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strSearchType", ParameterDirection.Input, SqlDbType.NVarChar, strSearchType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "QRP.up_Select_INSProcInspect_frmSTA0074_D2_PSTS", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 품질일보 그리드 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDate">선택날짜</param>
        /// <param name="strProcessActionType">공정타입</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0075_D1(string strPlantCode, string strDate, string strProcessActionType)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDate", ParameterDirection.Input, SqlDbType.VarChar, strDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.NVarChar, strProcessActionType, 40);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "QRP.up_Select_INSProcInspect_frmSTA0075_D1", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 품질일보 그리드 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDate">선택날짜</param>
        /// <param name="strProcessActionType">제품구분</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strPACKAGE">PACKAGE</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0075_D1_PSTS(string strPlantCode, string strDate,
                                                                    string strProcessActionType,string strCustomerCode, string strPACKAGE, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                //DB 연결
                sql.mfConnect();

                //Parameter 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);                   // 공장
                sql.mfAddParamDataRow(dtParam, "@i_strDate", ParameterDirection.Input, SqlDbType.VarChar, strDate, 10);                             // 검색일
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.NVarChar, strProcessActionType, 40);  // 제품구분
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);             // 고객
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPACKAGE, 40);                       // PACKAGE
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);                              // 사용언어

                //SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0075_D1_PSTS", dtParam);

                //정보 Return
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// Daily품질일보 QCN현황 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDate"></param>
        /// <param name="strProcessActionType"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0075_D2(string strPlantCode, string strDate, string strProcessActionType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDate", ParameterDirection.Input, SqlDbType.VarChar, strDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.NVarChar, strProcessActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0075_D2", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// Daily품질일보 QCN현황 검색
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDate">검색일</param>
        /// <param name="strProcessActionType">제품구분</param>
        /// <param name="strCustomerCode">고객</param>
        /// <param name="strPACKAGE">PACKAGE</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0075_D2_PSTS(string strPlantCode, string strDate,
                                                                    string strProcessActionType, string strCustomerCode, string strPACKAGE, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDate", ParameterDirection.Input, SqlDbType.VarChar, strDate, 10);                             // 검색일
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.NVarChar, strProcessActionType, 40);  // 제품구분
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);             // 고객
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPACKAGE, 40);                       // PACKAGE
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0075_D2_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정 품질실적 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">해당년도</param>
        /// <param name="strMonth">월</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strProductActionType">제품구분/param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0076(string strPlantCode, string strYear, string strMonth, string strCustomerCode, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                //@i_strProductActionType
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0076", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
        
        /// <summary>
        /// 공정품질실적 월간 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strCustomerCode"></param>
        /// <param name="strProductActionType"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0076_D1(string strPlantCode, string strYear, string strMonth, string strCustomerCode, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0076_D1", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정품질실적 월간 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strCustomerCode"></param>
        /// <param name="strProductActionType"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0076_D1_PSTS(string strPlantCode, string strYear, string strMonth, string strCustomerCode, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0076_D1_PSTS", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }


        /// <summary>
        /// 공정품질실적 연간(분기별) 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strCustomerCode"></param>
        /// <param name="strProductActionType"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0076_D2(string strPlantCode, string strYear, string strMonth, string strCustomerCode, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0076_D2", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정품질실적 연간(분기별) 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strCustomerCode"></param>
        /// <param name="strProductActionType"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0076_D2_PSTS(string strPlantCode, string strYear, string strMonth, string strCustomerCode, string strProductActionType)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0076_D2_PSTS", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }


        /// <summary>
        /// 주별 품질 지수 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">검색 연도</param>
        /// <param name="strMonth">검색월</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0077_D1(string strPlantCode, string strYear, string strMonth, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0077_D1", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 주별 품질 지수 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">검색 연도</param>
        /// <param name="strMonth">검색월</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0077_D1_PSTS(string strPlantCode, string strYear, string strMonth, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0077_D1_PSTS", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        ///  주별 품질지수 하단 그리드 조회(고객사 제외)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0077_D2(string strPlantCode, string strYear, string strMonth)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0077_D2", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        ///  주별 품질지수 하단 그리드 조회(고객사 제외)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0077_D2_PSTS(string strPlantCode, string strYear, string strMonth,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0077_D2_PSTS", dtParam);
                return dt;
            }
            catch(Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정 Loss현황
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">검색 연도</param>
        /// <param name="strMonth">검색 월</param>
        /// <param name="strProcessOperationType">공정타입</param>
        /// <param name="strDataType">항목유형</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0078(string strPlantCode, string strYear, string strMonth, string strProcessOperationType, string strDataType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessOperationType", ParameterDirection.Input, SqlDbType.VarChar, strProcessOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, strDataType, 1);
                sql.mfAddParamDataRow(dtParam, "@I_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0078", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 공정 Loss현황
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">검색 연도</param>
        /// <param name="strMonth">검색 월</param>
        /// <param name="strProcessOperationType">공정타입</param>
        /// <param name="strDataType">항목유형</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0078_PSTS(string strPlantCode, string strYear, string strMonth, string strProcessOperationType, string strDataType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessOperationType", ParameterDirection.Input, SqlDbType.VarChar, strProcessOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, strDataType, 1);
                sql.mfAddParamDataRow(dtParam, "@I_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0078_PSTS", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dt.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 특성 Data 공정품질실적
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strProcessActionType"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0079(string strPlantCode, string strYear, string strMonth, string strProcessActionType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar,strProcessActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0079", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// QCN 현황 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0080(string strPlantCode, string strYear, string strMonth, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0080",dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// QCN 현황 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromDate">From일자</param>
        /// <param name="strToDate">To일자</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0080_PSTS(string strPlantCode, string strFromDate, string strToDate, 
                                                                string strQCNITR, string strCustomerCode,string strPACKAGE,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNITR", ParameterDirection.Input, SqlDbType.VarChar, strQCNITR, 3);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPACKAGE, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0080_PSTS", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// CCS불량현황 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0081(string strPlantCode, string strYear, string strMonth, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang,3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0081", dtParam);
                return dtRtn;
            }
            catch(Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// CCS불량현황 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0081_PSTS(string strPlantCode, string strYear, string strMonth, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0081_PSTS", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// CCS 품질실적 월간, 전년도 통계 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strProductActionType"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0082_D1(string strPlantCode, string strYear, string strMonth, string strProductActionType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0082_D1", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// CCS 품질실적 월간, 전년도 통계 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strYear">년도</param>
        /// <param name="strMonth">월</param>
        /// <param name="strProductActionType">제품구분</param>
        /// <param name="strCustomerCode">고객</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0082_D1_PSTS(string strPlantCode, string strYear, string strMonth, string strProductActionType, string strCustomerCode ,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0082_D1_PSTS", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// CCS 품질실적 월간 총합 통계 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strYear"></param>
        /// <param name="strMonth"></param>
        /// <param name="strProductActionType"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0082D2(string strPlantCode, string strYear, string strMonth, string strProductActionType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0082_D2", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// CCS 품질실적 월간 총합 통계 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strYear">년도</param>
        /// <param name="strMonth">월</param>
        /// <param name="strProductActionType">제품구분</param>
        /// <param name="strCustomerCode">고객</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspect_frmSTA0082_D2_PSTS(string strPlantCode, string strYear, string strMonth, 
                                                                    string strProductActionType, string strCustomerCode ,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.VarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspect_frmSTA0082_D2_PSTS", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcTarget")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcTarget : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼 설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("AYear", typeof(string));
                dtRtn.Columns.Add("AQuarter", typeof(string));
                dtRtn.Columns.Add("DetailProcessOperationType", typeof(string));
                dtRtn.Columns.Add("ProductionType", typeof(string));
                dtRtn.Columns.Add("Target", typeof(decimal));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                DataColumn[] dc = new DataColumn[5];
                dc[0] = dtRtn.Columns["PlantCode"];
                dc[1] = dtRtn.Columns["AYear"];
                dc[2] = dtRtn.Columns["AQuarter"];
                dc[3] = dtRtn.Columns["DetailProcessOperationType"];
                dc[4] = dtRtn.Columns["ProductionType"];

                dtRtn.PrimaryKey = dc;

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

       #region 공정

        /// <summary>
        /// 공정 품질목표 등록/조회 화면 검색 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strQuarter">분기</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcTarget(string strPlantCode, string strYear, string strQuarter, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, strQuarter, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcTarget", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정 품질목표 등록/조회 화면 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장할 데이터테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcTarget(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AQuarter"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["DetailProcessOperationType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductionType", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ProductionType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_dblTarget", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["Target"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcTarget", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정 품질목표 등록/조회 화면 삭제 메소드
        /// </summary>
        /// <param name="dtDelete">삭제정보 담긴 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcTarget(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["AYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["AQuarter"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.NVarChar, dtDelete.Rows[i]["DetailProcessOperationType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductionType", ParameterDirection.Input, SqlDbType.NVarChar, dtDelete.Rows[i]["ProductionType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcTarget", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion
        
        #region QCN

        /// <summary>
        /// QCN 품질목표 등록/조회 화면 검색 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strQuarter">분기</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSQCNTarget(string strPlantCode, string strYear, string strQuarter, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, strQuarter, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSQCNTarget", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// QCN 품질목표 등록/조회 화면 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장할 데이터테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSQCNTarget(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AQuarter"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["DetailProcessOperationType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductionType", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ProductionType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_dblTarget", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["Target"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSQCNTarget", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// QCN 품질목표 등록/조회 화면 삭제 메소드
        /// </summary>
        /// <param name="dtDelete">삭제정보 담긴 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSQCNTarget(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["AYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["AQuarter"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.NVarChar, dtDelete.Rows[i]["DetailProcessOperationType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductionType", ParameterDirection.Input, SqlDbType.NVarChar, dtDelete.Rows[i]["ProductionType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSQCNTarget", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region CCS

        /// <summary>
        /// CCS 품질목표 등록/조회 화면 검색 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">조회년도</param>
        /// <param name="strQuarter">분기</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSCCSTarget(string strPlantCode, string strYear, string strQuarter, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, strQuarter, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSTarget", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// CCS 품질목표 등록/조회 화면 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장할 데이터테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSCCSTarget(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AQuarter"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["DetailProcessOperationType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductionType", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ProductionType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_dblTarget", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["Target"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSTarget", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// CCS 품질목표 등록/조회 화면 삭제 메소드
        /// </summary>
        /// <param name="dtDelete">삭제정보 담긴 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSCCSTarget(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["AYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["AQuarter"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.NVarChar, dtDelete.Rows[i]["DetailProcessOperationType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductionType", ParameterDirection.Input, SqlDbType.NVarChar, dtDelete.Rows[i]["ProductionType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSCCSTarget", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

    }
}
