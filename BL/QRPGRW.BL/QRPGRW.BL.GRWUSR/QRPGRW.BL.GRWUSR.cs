﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.Runtime.InteropServices;
using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
Authentication = AuthenticationOption.None,
ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPGRW.BL.GRWUSR
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("GRWUser")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class GRWUser : ServicedComponent
    {
        /// <summary>
        /// 결재선 사용자 조회
        /// </summary>
        /// <param name="strCompanyCode">회사코드</param>
        /// <param name="strUserName">사용자명</param>
        /// <param name="strDeptName">부서명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadGRWUser(string strCompanyCode, string strUserName, string strDeptName)
        {
            SQLS sql = new SQLS();
            DataTable dtUser = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strCompanyCode", ParameterDirection.Input, SqlDbType.VarChar, strCompanyCode, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strUserName", ParameterDirection.Input, SqlDbType.NVarChar, strUserName, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strDeptName", ParameterDirection.Input, SqlDbType.NVarChar, strDeptName, 100);

                dtUser = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_GRWUser", dtParam);

                return dtUser;
            }
            catch (Exception ex)
            {
                return dtUser;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 결재선 사용자 조회 - 단일 조회 -
        /// </summary>
        /// <param name="strCompanyCode">회사코드</param>
        /// <param name="strUserID">Brains ID</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadGRWUserD(string strCompanyCode, string strUserID)
        {
            SQLS sql = new SQLS();
            DataTable dtUser = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strCompanyCode", ParameterDirection.Input, SqlDbType.VarChar, strCompanyCode, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                dtUser = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_GRWUserDetail", dtParam);

                return dtUser;
            }
            catch (Exception ex)
            {
                return dtUser;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// PSTS 사용자 조회
        /// </summary>
        /// <param name="strCdKind"></param>
        /// <param name="strCdSearchType"></param>
        /// <param name="strCdCompany"></param>
        /// <param name="strCdUserKey"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadGRWUserInfoPSTS(string strCdKind, string strCdSearchType, string strCdCompany, string strCdUserKey)
        {
            QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
            DataTable dtRtn = grw.dtUserInfo(strCdKind, strCdSearchType, strCdCompany, strCdUserKey);

            return dtRtn;
        }

        [AutoComplete(false)]
        public string mfSaveSetDraftForm(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strFmpf, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            System.IO.StreamWriter log = new System.IO.StreamWriter("D:/" + "GRWUserMfSaveSetDraftForm.txt", true);
            log.WriteLine("--begin--");
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                #region Header
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLegacyKey", ParameterDirection.Input, SqlDbType.VarChar, strLegacyKey, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strFmpf", ParameterDirection.Input, SqlDbType.VarChar, strFmpf, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strDocLevel", ParameterDirection.Input, SqlDbType.VarChar, dtFormInfo.Rows[0]["DocLevel"].ToString(), 40);
                sql.mfAddParamDataRow(dtParam, "@i_strSaveTerm", ParameterDirection.Input, SqlDbType.VarChar, dtFormInfo.Rows[0]["SaveTerm"].ToString(), 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDocClassName", ParameterDirection.Input, SqlDbType.VarChar, dtFormInfo.Rows[0]["DocClassName"].ToString(), 40);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_GRWSetDraftForm", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                log.WriteLine("1--ErrRtn:" + ErrRtn);
                log.WriteLine("1--ErrRtn.ErrNum:" + ErrRtn.ErrNum);

                if (!ErrRtn.ErrNum.Equals(0))
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                #endregion

                #region Delete Detail
                if (sql.SqlCon.State == ConnectionState.Closed)
                    sql.mfConnect();
                trans = sql.SqlCon.BeginTransaction();
                dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLegacyKey", ParameterDirection.Input, SqlDbType.VarChar, strLegacyKey, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strFmpf", ParameterDirection.Input, SqlDbType.VarChar, strFmpf, 100);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_GRWSetDraftFormDetail", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                log.WriteLine("2--ErrRtn:" + ErrRtn);
                log.WriteLine("2--ErrRtn.ErrNum:" + ErrRtn.ErrNum);
                if (!ErrRtn.ErrNum.Equals(0))
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }
                #endregion

                log.WriteLine("---3--1---");
                #region SendLine
                log.WriteLine("dtSendLine.Rows.Count:"+dtSendLine.Rows.Count);
                for (int i = 0; i < dtSendLine.Rows.Count; i++)
                {
                    log.WriteLine("SqlCon.State--3--1--:"+sql.SqlCon.State.ToString());
                    if (sql.SqlCon.State == ConnectionState.Closed)
                        sql.mfConnect();

                    log.WriteLine("SqlCon.State--3--2--:" + sql.SqlCon.State.ToString());

                    trans = sql.SqlCon.BeginTransaction();
                    log.WriteLine("---3--2--");
                    dtParam = sql.mfSetParamDataTable();
                    log.WriteLine("---3--3--");
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    log.WriteLine("---3--4-strLegacyKey-" + strLegacyKey);
                    sql.mfAddParamDataRow(dtParam, "@i_strLegacyKey", ParameterDirection.Input, SqlDbType.VarChar, strLegacyKey, 100);
                    log.WriteLine("---3--5-strFmpf-" + strFmpf);
                    sql.mfAddParamDataRow(dtParam, "@i_strFmpf", ParameterDirection.Input, SqlDbType.VarChar, strFmpf, 100);
                    log.WriteLine("---3--6-CD_USERID-");
                    sql.mfAddParamDataRow(dtParam, "@i_strCD_USERKEY", ParameterDirection.Input, SqlDbType.VarChar, dtSendLine.Rows[i]["CD_USERID"].ToString(), 100);
                    log.WriteLine("---3--7-CD_KIND-");
                    sql.mfAddParamDataRow(dtParam, "@i_strCD_KIND", ParameterDirection.Input, SqlDbType.VarChar, dtSendLine.Rows[i]["CD_KIND"].ToString(), 100);
                    log.WriteLine("---3--8-NO_EMPLOYEE-");
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSendLine.Rows[i]["NO_EMPLOYEE"].ToString(), 20);
                    log.WriteLine("---3--9-strUserIP-" + strUserIP);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    log.WriteLine("---3--10-");
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_GRWSetDraftFormDetail", dtParam);
                    log.WriteLine(strErrRtn);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    log.WriteLine("3--ErrRtn:" + ErrRtn);
                    log.WriteLine("3--ErrRtn.ErrNum:" + ErrRtn.ErrNum);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                #endregion

                #region CcLine

                log.WriteLine("dtCcLine.Rows.Count:" + dtCcLine.Rows.Count);
                for (int i = 0; i < dtCcLine.Rows.Count; i++)
                {
                    log.WriteLine("--4--1--");
                    if(sql.SqlCon.State == ConnectionState.Closed)
                        sql.mfConnect();
                    trans = sql.SqlCon.BeginTransaction();
                    sql.mfSetParamDataTable();
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLegacyKey", ParameterDirection.Input, SqlDbType.VarChar, strLegacyKey, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFmpf", ParameterDirection.Input, SqlDbType.VarChar, strFmpf, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCD_USERKEY", ParameterDirection.Input, SqlDbType.VarChar, dtCcLine.Rows[i]["CD_USERKEY"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCD_KIND", ParameterDirection.Input, SqlDbType.VarChar, dtCcLine.Rows[i]["CD_KIND"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, dtCcLine.Rows[i]["UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_GRWSetDraftFormDetail", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    log.WriteLine("4--ErrRtn:" + ErrRtn);
                    log.WriteLine("4--ErrRtn.ErrNum:" + ErrRtn.ErrNum);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                #endregion

                return strErrRtn;
            }
            catch (Exception ex)
            {
                log.WriteLine("ex.InnerException.ToString():" + ex.InnerException.ToString());
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                log.WriteLine("ex.Message" + ex.Message);
                ErrRtn.SystemMessage = ex.Message;
                log.WriteLine("ex.StackTrace:" + ex.StackTrace);
                ErrRtn.SystemStackTrace = ex.StackTrace;
                log.WriteLine("ErrRtn.mfEncodingErrMessage(ErrRtn):" + ErrRtn.mfEncodingErrMessage(ErrRtn));
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                log.WriteLine("--end--");
                log.Close();
            }
        }

        [AutoComplete(false)]
        public string mfSaveSetDraftForm_F(string strLegacyKey, string strFmpf, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                #region Header
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLegacyKey", ParameterDirection.Input, SqlDbType.VarChar, strLegacyKey, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strFmpf", ParameterDirection.Input, SqlDbType.VarChar, strFmpf, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_GRWSetDraftForm_F", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                #endregion

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 결재문서 승인
        /// </summary>
        /// <param name="strLegacyKey"></param>
        /// <param name="strDOCNO"></param>
        /// <param name="strFmpf"></param>
        /// <param name="strCD_USERKEY"></param>
        /// <param name="strCD_KIND"></param>
        /// <param name="strTYPE"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveGRWApproval(string strLegacyKey, string strDOCNO, string strFmpf, string strCD_USERKEY)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLegacyKey", ParameterDirection.Input, SqlDbType.VarChar, strLegacyKey, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strDOCNO", ParameterDirection.Input, SqlDbType.VarChar, strDOCNO, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strFmpf", ParameterDirection.Input, SqlDbType.VarChar, strFmpf, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strCD_USERKEY", ParameterDirection.Input, SqlDbType.VarChar, strCD_USERKEY, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, "QRPWebServer", 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, "QRPWebServer", 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_GRWApproval", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();

                    //결제 승인문서가 설비폐기에 관한문서일 경우 MDM에 송신
                    if (strFmpf.Equals("WF_STS_EQPDEL"))
                    {
                        //설비정보BL호출
                        QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();

                        //승인된 폐기설비 MDM으로 송신 매서드 실행
                       string strRtn = clsEquip.mfSaveEquipDiscard_MDM("", "", strDOCNO);
                    }
                    // 결제 승인문서가 설비인증의뢰에 관한 문서일 경우 MDM에 송신
                    else if (strFmpf.Equals("WF_STS_QRPGIAN"))
                    {
                        string[] strSeperator = new string[] { "||" };
                        string[] strarr = strDOCNO.Split(strSeperator, StringSplitOptions.RemoveEmptyEntries);
                        string strPlantCode = strarr[0];
                        string strDocCode = strarr[1];

                        QRPEQU.BL.EQUCCS.EQUEquipCertiH clsEquCerti = new QRPEQU.BL.EQUCCS.EQUEquipCertiH();

                        string strRtn = clsEquCerti.mfSave_MDMIF_QRP_MCH_SPEC(strPlantCode, strDocCode, "N", "QRPWebServer", "QRPWebServer");
                    }
                }
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 결재문서 반려
        /// </summary>
        /// <param name="strLegacyKey"></param>
        /// <param name="strDOCNO"></param>
        /// <param name="strFmpf"></param>
        /// <param name="strCD_USERKEY"></param>
        /// <param name="strCD_KIND"></param>
        /// <param name="strTYPE"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveGRWReject(string strLegacyKey, string strDOCNO, string strFmpf, string strCD_USERKEY)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLegacyKey", ParameterDirection.Input, SqlDbType.VarChar, strLegacyKey, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strDOCNO", ParameterDirection.Input, SqlDbType.VarChar, strDOCNO, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strFmpf", ParameterDirection.Input, SqlDbType.VarChar, strFmpf, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strCD_USERKEY", ParameterDirection.Input, SqlDbType.VarChar, strCD_USERKEY, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, "QRPWebServer", 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, "QRPWebServer", 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_GRWReject", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

       
    }
}
