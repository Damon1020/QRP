﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 통문관리                                              */
/* 프로그램ID   : clsEQUCAR.cs                                          */
/* 프로그램명   : 통문관리                                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-09-05                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
              Authentication = AuthenticationOption.None,
             ImpersonationLevel = ImpersonationLevelOption.Impersonate)]


namespace QRPEQU.BL.EQUCAR
{

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CarryOutH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CarryOutH : ServicedComponent
    {

        /// <summary>
        /// 반출등록 컬럼셋팅
        /// </summary>
        /// <returns>컬럼셋팅 된 데이터테이블</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtCarrtOutH = new DataTable();
            try
            {
                dtCarrtOutH.Columns.Add("PlantCode", typeof(string));
                dtCarrtOutH.Columns.Add("DocCode", typeof(string));
                dtCarrtOutH.Columns.Add("CarryOutWriteDate", typeof(string));
                dtCarrtOutH.Columns.Add("RepairGICode", typeof(string));
                dtCarrtOutH.Columns.Add("COutChargeID", typeof(string));
                dtCarrtOutH.Columns.Add("AssetChargeID", typeof(string));
                dtCarrtOutH.Columns.Add("ECOutGubunCode", typeof(string));
                dtCarrtOutH.Columns.Add("CarryOutEtcDesc", typeof(string));
                dtCarrtOutH.Columns.Add("CarryOutReason", typeof(string));
                dtCarrtOutH.Columns.Add("VendorCode", typeof(string));
                dtCarrtOutH.Columns.Add("VendorChargeName", typeof(string));
                dtCarrtOutH.Columns.Add("COutVendorTel", typeof(string));
                dtCarrtOutH.Columns.Add("ECInGubunCode", typeof(string));
                dtCarrtOutH.Columns.Add("CarryInEstDate", typeof(string));
                dtCarrtOutH.Columns.Add("CarryInNoReason", typeof(string));
                dtCarrtOutH.Columns.Add("CarryOutAdmitID", typeof(string));
                
                //dtCarrtOutH.Columns.Add("CarryOutConfirmID", typeof(string));
                //dtCarrtOutH.Columns.Add("CarryOutConfirmDate", typeof(string));
                //dtCarrtOutH.Columns.Add("CarryInConfirmID", typeof(string));
                //dtCarrtOutH.Columns.Add("CarryInConfirmDate", typeof(string));


                return dtCarrtOutH;
            }
            catch (Exception ex)
            {
                return dtCarrtOutH;
                throw (ex);

            }
            finally
            {
                dtCarrtOutH.Dispose();
            }
        }

        /// <summary>
        /// 반출승인 컬럼셋팅
        /// </summary>
        /// <returns>컬럼셋팅 된 데이터테이블</returns>
        public DataTable mfSetDataInfoAdmit()
        {
            DataTable dtCarryOut = new DataTable();
            try
            {
                dtCarryOut.Columns.Add("PlantCode",typeof(string));
                dtCarryOut.Columns.Add("DocCode", typeof(string));
                dtCarryOut.Columns.Add("Seq", typeof(string));
                dtCarryOut.Columns.Add("AdmitDate", typeof(string));
                dtCarryOut.Columns.Add("AdmitID", typeof(string));
                dtCarryOut.Columns.Add("CarryID", typeof(string));
                dtCarryOut.Columns.Add("CarryDate", typeof(string));
                

                return dtCarryOut;
            }
            catch (Exception ex)
            {
                return dtCarryOut;
                throw (ex);
            }
            finally
            {
                dtCarryOut.Dispose();
            }
        }
        // -----------------------------------------반 출 등 록 ----------------------------------------//
        /// <summary>
        /// 반출등록 등록
        /// </summary>
        /// <param name="dtCarryOutH">반출등록 헤더</param>
        /// <param name="dtCarryOutD">반출등록 상세</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveCarryOutH(DataTable dtCarryOutH,DataTable dtCarryOutD,string strUserIP,string strUserID)
        {
            string strErrRtn = "";
            string strDocCode = "";
            
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                //공장코드 저장//
                string strPlantCode = dtCarryOutH.Rows[0]["PlantCode"].ToString();

                //--디비연결 --//
                sql.mfConnect();
                SqlTransaction trans;
                //--Transaction 시작 --//
                trans = sql.SqlCon.BeginTransaction();

                //---- 파라미터 값 저장 -----/
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["DocCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["RepairGICode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strCarryOutWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["CarryOutWriteDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCOutChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["COutChargeID"].ToString(), 20);
                //sql.mfAddParamDataRow(dtParam, "@i_strAssetChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["AssetChargeID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strECOutGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["ECOutGubunCode"].ToString(), 5);
                sql.mfAddParamDataRow(dtParam, "@i_strCarryOutEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryOutH.Rows[0]["CarryOutEtcDesc"].ToString(), 100);
                sql.mfAddParamDataRow(dtParam, "@i_strCarryOutReason", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryOutH.Rows[0]["CarryOutReason"].ToString(), 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["VendorCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorChargeName", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryOutH.Rows[0]["VendorChargeName"].ToString(), 50);
                sql.mfAddParamDataRow(dtParam, "@i_strCOutVendorTel", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryOutH.Rows[0]["COutVendorTel"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strECInGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["ECInGubunCode"].ToString(), 5);
                sql.mfAddParamDataRow(dtParam, "@i_strCarryInEstDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["CarryInEstDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCarryOutAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutH.Rows[0]["CarryOutAdmitID"].ToString(), 10);

                sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                sql.mfAddParamDataRow(dtParam,"@o_strDocCode", ParameterDirection.Output, SqlDbType.VarChar,20);
                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //--- 프로시저 실행 --/
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUCarryOutH", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //리턴값 저장
                strDocCode = ErrRtn.mfGetReturnValue(0);
                
                
                //---- 성공여부에 따라 Rollback,Commit ---/
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    //----성공 시  상세 저장 ----///
                    if (dtCarryOutD.Rows.Count > 0)
                    {
                        CarryOutD clsCarryD = new CarryOutD();
                        strErrRtn = clsCarryD.mfSaveCarryOutD(strPlantCode, strDocCode, dtCarryOutD, strUserIP, strUserID, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        //--상세저장 실패시 롤백처리 --//
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                        
                        
                    }
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //-- 디비종료 --//
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        //----------------------------------반 출 승 인 -------------------------------//
        /// <summary>
        /// 반출승인 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCarryOutAdmit(string strPlantCode, string strFromDate, string strToDate, string strLang)
        {
            DataTable dtCarryOut = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // -- 디비 연결 
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                // -- 파라미터 값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                // -- 프로시저 실행
                dtCarryOut = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUCarryOutAdmit", dtParam);
                

                return dtCarryOut;
            }
            catch (Exception ex)
            {
                return dtCarryOut;
                throw (ex);
            }
            finally
            {
                // -- 디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtCarryOut.Dispose();
            }
        }

        /// <summary>
        /// 반출승인 승인
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDocCode">문서코드</param>
        /// <param name="strAdmitID">승인자</param>
        /// <param name="strAdmitDate">승인일</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveCarryOutAdmit(DataTable dtCarryOutAdmit,string strUserIP,string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                // --디비연결
                sql.mfConnect();

                SqlTransaction trans;
                // -- Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtCarryOutAdmit.Rows.Count; i++)
                {
                    
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // -- 파라미터값저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutAdmit.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutAdmit.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryOutAdmit.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutAdmit.Rows[i]["AdmitID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.DateTime, dtCarryOutAdmit.Rows[i]["AdmitDate"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUCarryOutAdmit", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    
                }
                if (ErrRtn.ErrNum == 0)
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                // -- 디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        //-------------------------------------- 반 출 확 인 -------------------------------------//
        /// <summary>
        /// 반출확인 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDocCode">문서코드</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>반출확인정보</returns>
        [AutoComplete]
        public DataTable mfReadCarryOutConfirm(string strPlantCode, string strDocCode, string strFromDate, string strToDate, string strLang)
        {
            DataTable dtCarryOut = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비오픈
                sql.mfConnect();

                //파라미터 값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저 실행
                dtCarryOut = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUCarryOutConfirm", dtParam);
                //정보리턴
                return dtCarryOut;
            }
            catch (Exception ex)
            {
                return dtCarryOut;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtCarryOut.Dispose();
            }
        }

        /// <summary>
        /// 반출확인 확인
        /// </summary>
        /// <param name="dtCarryOutConfirm">반출확인정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>확인처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveCarryOutConfirm(DataTable dtCarryOutConfirm, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();

                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for(int i =0; i<dtCarryOutConfirm.Rows.Count;i++)
                {
                    
                    DataTable dtParm = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParm, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                  
                    sql.mfAddParamDataRow(dtParm, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtCarryOutConfirm.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParm, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar,dtCarryOutConfirm.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParm, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int,dtCarryOutConfirm.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParm, "@i_strCarryOutID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutConfirm.Rows[i]["CarryID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParm, "@i_strCarryOutDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutConfirm.Rows[i]["CarryDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParm, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP,15);
                    sql.mfAddParamDataRow(dtParm, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID, 20);

                    sql.mfAddParamDataRow(dtParm, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUCarryOutConfirm", dtParm);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    
                }

                if (ErrRtn.ErrNum == 0)
                {
                    trans.Commit();
                }
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.Source;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }

        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CarryOutD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CarryOutD : ServicedComponent
    {
        /// <summary>
        /// 반출 상세 데이터 컬럼셋
        /// </summary>
        /// <returns>반출상세에 필요한 컬럼 셋</returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtCarryOutD = new DataTable();
            try
            {
                dtCarryOutD.Columns.Add("Seq", typeof(string));
                dtCarryOutD.Columns.Add("ItemGubunCode", typeof(string));
                dtCarryOutD.Columns.Add("ItemCode", typeof(string));
                dtCarryOutD.Columns.Add("ItemName", typeof(string));
                dtCarryOutD.Columns.Add("LotNo", typeof(string));
                dtCarryOutD.Columns.Add("ItemSpec", typeof(string));
                dtCarryOutD.Columns.Add("CarryOutQty", typeof(string));
                dtCarryOutD.Columns.Add("UnitCode", typeof(string));
                dtCarryOutD.Columns.Add("CarryOutEtcDesc", typeof(string));
                dtCarryOutD.Columns.Add("CarryOutAdmitFlag", typeof(string));
                dtCarryOutD.Columns.Add("CarryOutConfirmFlag", typeof(string));
                dtCarryOutD.Columns.Add("CarryInConfirmFlag", typeof(string));

                dtCarryOutD.Columns.Add("InventoryCode", typeof(string));

                return dtCarryOutD;
            }
            catch (Exception ex)
            {
                return dtCarryOutD;
                throw (ex);
            }
            finally
            {
                dtCarryOutD.Dispose();
            }
        }

        /// <summary>
        /// 반출등록 상세저장
        /// </summary>
        /// <param name="strPlantcode">공장코드</param>
        /// <param name="strDocCode">문서코드</param>
        /// <param name="dtCarryOutD">저장할 상세정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과 값</returns>
        [AutoComplete(false)]
        public string mfSaveCarryOutD(string strPlantcode, string strDocCode, DataTable dtCarryOutD, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                for (int i = 0; i < dtCarryOutD.Rows.Count; i++)
                {
                    //----- 파라미터 값 저장---------//
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantcode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryOutD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strItemGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutD.Rows[i]["ItemGubunCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemCode", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryOutD.Rows[i]["ItemCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryOutD.Rows[i]["ItemName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutD.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryOutD.Rows[i]["ItemSpec"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_intCarryOutQty", ParameterDirection.Input, SqlDbType.Decimal, dtCarryOutD.Rows[i]["CarryOutQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCarryOutEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryOutD.Rows[i]["CarryOutEtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryOutD.Rows[i]["InventoryCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //-----프로시저 실행---///
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUCarryOutD", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    //---처리 실패시 For문을 종료 시킨다---//
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                } 
                //처리결과 리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CarryInD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CarryInD : ServicedComponent
    {
        /// <summary>
        /// 반입상세
        /// </summary>
        /// <returns>반입상세 데이터 Set</returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtCarryIn = new DataTable();
            try
            {
                dtCarryIn.Columns.Add("PlantCode", typeof(string));
                dtCarryIn.Columns.Add("DocCode", typeof(string));
                dtCarryIn.Columns.Add("Seq", typeof(string));
                dtCarryIn.Columns.Add("CarryInSeq", typeof(string));
                dtCarryIn.Columns.Add("CarryInDate", typeof(string));
                dtCarryIn.Columns.Add("CarryInChargeID", typeof(string));
                dtCarryIn.Columns.Add("CarryInQty", typeof(string));
                dtCarryIn.Columns.Add("InventoryCode", typeof(string));

                dtCarryIn.Columns.Add("GRDate", typeof(string));
                dtCarryIn.Columns.Add("GRChargeID", typeof(string));
                dtCarryIn.Columns.Add("GRQty", typeof(string));
                dtCarryIn.Columns.Add("GRFlag", typeof(string));
                dtCarryIn.Columns.Add("GRCancelID", typeof(string));
                dtCarryIn.Columns.Add("GRCancelDate", typeof(string));
                dtCarryIn.Columns.Add("GRCancelFlag", typeof(string));
                dtCarryIn.Columns.Add("GRCancelReason", typeof(string));

                return dtCarryIn;
            }
            catch (Exception ex)
            {
                return dtCarryIn;
                throw (ex);
            }
            finally
            {
                dtCarryIn.Dispose();
            }
        }

        /// <summary>
        /// 반입확인 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDocCode">문서코드</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>반출확인된 정보</returns>
        [AutoComplete]
        public DataTable mfReadCarryInD(string strPlantCode , string strItemGubunCode, string strDocCode,string strFromDate,string strToDate,string strLang)
        {
            DataTable dtCarryInD = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                // - -디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //- - 파라미터 값 저장
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strItemGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strItemGubunCode, 10);
                sql.mfAddParamDataRow(dtParam,"@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar,strDocCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar,strFromDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar,strToDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                // - - 프로시저 실행
                dtCarryInD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUCarryInDConfirm", dtParam);

                // - - 정보리턴
                return dtCarryInD;
            }
            catch (Exception ex)
            {
                return dtCarryInD;
                throw (ex);
            }
            finally
            {
                // - - 디비 종료
                sql.mfDisConnect();
                sql.Dispose();
                dtCarryInD.Dispose();
            }
        }

        /// <summary>
        /// 반입확인 확인
        /// </summary>
        /// <param name="dtCarryInD">반입확인할 정보</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveCarrInD(DataTable dtCarryInD,string strUserIP,string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strCheck = "";
            try
            {
                // -- 디비연결
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtCarryInD.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();


                    ////////////////////////////// 1. Web반출 저장 ///////////////////////////////////////

                    //--  파라미터 값 저장 - //
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intCarryInSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInD.Rows[i]["CarryInSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["GRDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["GRChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["InventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_numGRQty", ParameterDirection.Input, SqlDbType.Decimal, dtCarryInD.Rows[i]["GRQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["GRFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRCancelReason", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryInD.Rows[i]["GRCancelReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //-- 프로시저 실행 --//
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_EQUCarryInD_GR_WEB", dtParam);

                    //--- Deconding --//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;                        
                    }

                    ////////////////////////////// 2. QRPDB저장 ///////////////////////////////////////

                    //--BeginTransaction 시작
                    trans = sql.SqlCon.BeginTransaction();
                    dtParam = sql.mfSetParamDataTable();

                    //--  파라미터 값 저장 - //
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intCarryInSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInD.Rows[i]["CarryInSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["GRDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["GRChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["InventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_numGRQty", ParameterDirection.Input, SqlDbType.Decimal, dtCarryInD.Rows[i]["GRQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["GRFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //-- 프로시저 실행 --//
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUCarryInD_GR", dtParam);

                    //--- Deconding --//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // -- 실패시 Rollback 처리 후 for문을 나간다. -- //
                    if (ErrRtn.ErrNum != 0)
                    {
                        strCheck = "Fail";
                        trans.Rollback();

                    }

                    else
                    {
                        strCheck = "Succes";
                        trans.Commit();
                    }

                    //////////////////////// 3. 실패했을경우 WEB다시 전송 //////////////////////////////
                    if (strCheck == "Fail")
                    {
                        //--  파라미터 값 저장 - //
                        dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["DocCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInD.Rows[i]["Seq"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_intCarryInSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInD.Rows[i]["CarryInSeq"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["GRDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strGRChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["GRChargeID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["InventoryCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_numGRQty", ParameterDirection.Input, SqlDbType.Decimal, dtCarryInD.Rows[i]["GRQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.VarChar, "C", 1);    //반입실패일 경우는 C로 넘김
                        sql.mfAddParamDataRow(dtParam, "@i_strGRCancelReason", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryInD.Rows[i]["GRCancelReason"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //-- 프로시저 실행 --//
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_EQUCarryInD_GR_WEB", dtParam);

                        //--- Deconding --//
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            return strErrRtn;
                        }

                    }
                }


                //-- 처리 결과 리턴 --//
                return strErrRtn;

                ////////// -- 디비연결
                ////////sql.mfConnect();
                ////////SqlTransaction trans;

                //////////--BeginTransaction 시작
                ////////trans = sql.SqlCon.BeginTransaction();

                ////////for (int i = 0; i < dtCarryInD.Rows.Count; i++)
                ////////{
                    
                ////////    DataTable dtParam = sql.mfSetParamDataTable();
                ////////    //--  파라미터 값 저장 - //
                ////////    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                ////////    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["PlantCode"].ToString(), 10);
                ////////    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["DocCode"].ToString(), 20);
                ////////    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInD.Rows[i]["Seq"].ToString());
                ////////    sql.mfAddParamDataRow(dtParam, "@i_intCarryInQty", ParameterDirection.Input, SqlDbType.Decimal, dtCarryInD.Rows[i]["CarryInQty"].ToString());
                ////////    sql.mfAddParamDataRow(dtParam, "@i_strCarryInDate", ParameterDirection.Input, SqlDbType.DateTime, dtCarryInD.Rows[i]["CarryInDate"].ToString(), 10);
                ////////    sql.mfAddParamDataRow(dtParam, "@i_strCarryInChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["CarryInChargeID"].ToString(), 20);
                ////////    sql.mfAddParamDataRow(dtParam, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInD.Rows[i]["InventoryCode"].ToString(), 10);
                ////////    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                ////////    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                ////////    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                ////////    //-- 프로시저 실행 --//
                ////////    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUCarryInDConfirm", dtParam);

                ////////    //--- Deconding --//
                ////////    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn); 

                ////////    // -- 실패시 Rollback 처리 후 for문을 나간다. -- //
                ////////    if (ErrRtn.ErrNum != 0)
                ////////    {
                ////////        trans.Rollback(); 
                ////////        break;
                ////////    } 
                    
                ////////} 
                //////////-- 성공시 Commit 처리 한다 --//
                ////////if(ErrRtn.ErrNum == 0)
                ////////{
                ////////    trans.Commit();
                ////////}


                //////////-- 처리 결과 리턴 --//
                ////////return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //-- 디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        #region 반입취소


        /// <summary>
        /// 반입취소를위한 컬럼정보
        /// </summary>
        /// <returns>반입취소 컬럼정보</returns>
        public DataTable mfDataSetCarryInD_Cancel()
        {
            DataTable dtCancel = new DataTable();

            try
            {
                dtCancel.Columns.Add("PlantCode", typeof(string));
                dtCancel.Columns.Add("DocCode", typeof(string));
                dtCancel.Columns.Add("Seq", typeof(string));
                dtCancel.Columns.Add("CarryInSeq", typeof(string));
                dtCancel.Columns.Add("GRCancelID", typeof(string));
                dtCancel.Columns.Add("GRCancelDate", typeof(string));
                dtCancel.Columns.Add("GRCancelReason", typeof(string));
                dtCancel.Columns.Add("ItemGubunCode", typeof(string));
                dtCancel.Columns.Add("GRInventoryCode", typeof(string));
                dtCancel.Columns.Add("ItemCode", typeof(string));
                dtCancel.Columns.Add("LotNo", typeof(string));
                dtCancel.Columns.Add("GRQty", typeof(string));
                dtCancel.Columns.Add("UnitCode", typeof(string));
                dtCancel.Columns.Add("GRFlag", typeof(string));

                return dtCancel;
            }
            catch (Exception ex)
            {
                return dtCancel;
                throw (ex);
            }
            finally
            {
                dtCancel.Dispose();
            }
        }
        

        /// <summary>
        /// 반입취소가능정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strItemGubunCode">구성품구분코드</param>
        /// <param name="strDocCode">문서코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>반입취소가능 정보</returns>
        [AutoComplete]
        public DataTable mfReadCarryInD_Cancel(string strPlantCode, string strFromDate, string strToDate, string strItemGubunCode, string strDocCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCancel = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar,strFromDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar,strToDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strItemGubunCode", ParameterDirection.Input, SqlDbType.VarChar,strItemGubunCode,2);
                sql.mfAddParamDataRow(dtParam,"@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar,strDocCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                dtCancel = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUCarryInD_GRCancel", dtParam);

                //반입취소가능 정보 리턴
                return dtCancel;
            }
            catch (Exception ex)
            {
                return dtCancel;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 반입취소
        /// </summary>
        /// <param name="dtCarryInDCancel">반입취소정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveCarryInD_Cancel(DataTable dtCarryInDCancel, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            string strCheck = "";
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                

                for (int i = 0; i < dtCarryInDCancel.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();


                    ////////////////////////////// 1. Web반출 저장 ///////////////////////////////////////

                    //--  파라미터 값 저장 - //
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInDCancel.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intCarryInSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInDCancel.Rows[i]["CarryInSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRCancelDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRCancelID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_numGRQty", ParameterDirection.Input, SqlDbType.Decimal, dtCarryInDCancel.Rows[i]["GRQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRCancelReason", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRCancelReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                     
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //-- 프로시저 실행 --//
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_EQUCarryInD_GR_WEB", dtParam);

                    //--- Deconding --//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;
                    }

                    ////////////////////////////// 2. QRPDB저장 ///////////////////////////////////////
                    //트랜젝션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    //반입취소 파라미터 정보 저장
                    DataTable dtParamQRP = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParamQRP, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamQRP, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["PlantCode"].ToString(), 10);           //공장
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["DocCode"].ToString(), 20);               //문서번호
                    sql.mfAddParamDataRow(dtParamQRP, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInDCancel.Rows[i]["Seq"].ToString());                               //순번
                    sql.mfAddParamDataRow(dtParamQRP, "@i_intCarryInSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInDCancel.Rows[i]["CarryInSeq"].ToString());                 //반입순번
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strGRCancelID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRCancelID"].ToString(), 20);         //반입취소자
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strGRCancelDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRCancelDate"].ToString(), 10);     //반입취소일자
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strGRCancelReason", ParameterDirection.Input, SqlDbType.NVarChar, dtCarryInDCancel.Rows[i]["GRCancelReason"].ToString(), 100);//반입취소사유
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strItemGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["ItemGubunCode"].ToString(), 2);    //구성품구분
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strGRInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRInventoryCode"].ToString(), 10);//반입창고
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["ItemCode"].ToString(), 20);             //구성품코드
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["LotNo"].ToString(), 40);                   //LotNo
                    sql.mfAddParamDataRow(dtParamQRP, "@i_intGRQty", ParameterDirection.Input, SqlDbType.Decimal, dtCarryInDCancel.Rows[i]["GRQty"].ToString());                       //반입수량
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["UnitCode"].ToString(), 10);             //단위

                    sql.mfAddParamDataRow(dtParamQRP, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamQRP, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParamQRP, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //반입취소 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUCarryInD_GRCancel", dtParamQRP);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // -- 실패시 Rollback 처리 후 for문을 나간다. -- //
                    if (ErrRtn.ErrNum != 0)
                    {
                        strCheck = "Fail";
                        trans.Rollback();
                    }

                    else
                    {
                        strCheck = "Succes";
                        trans.Commit();
                    }

                    //////////////////////// 3. 실패했을경우 WEB다시 전송 //////////////////////////////
                    if (strCheck == "Fail")
                    {
                        //--  파라미터 값 저장 - //
                        dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["DocCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInDCancel.Rows[i]["Seq"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_intCarryInSeq", ParameterDirection.Input, SqlDbType.Int, dtCarryInDCancel.Rows[i]["CarryInSeq"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRCancelDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strGRChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRCancelID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRInventoryCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_numGRQty", ParameterDirection.Input, SqlDbType.Decimal, dtCarryInDCancel.Rows[i]["GRQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strGRCancelReason", ParameterDirection.Input, SqlDbType.VarChar, dtCarryInDCancel.Rows[i]["GRCancelReason"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //-- 프로시저 실행 --//
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_EQUCarryInD_GR_WEB", dtParam);

                        //--- Deconding --//
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                         
                        if (ErrRtn.ErrNum != 0)
                        {
                            return strErrRtn;
                        }

                    }
                }

                ////처리결과가 성공일 경우 커밋처리를 한다.
                ////if (ErrRtn.ErrNum.Equals(0))
                ////    trans.Commit();

                //처리결과정보
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        #endregion

    }

}
