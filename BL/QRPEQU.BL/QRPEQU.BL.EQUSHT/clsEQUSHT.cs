﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 전산SHEET정보                                         */
/* 프로그램ID   : clsEQUSHT.cs                                          */
/* 프로그램명   : SHEET관리                                             */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2012-05-02                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
              Authentication = AuthenticationOption.None,
             ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPEQU.BL.EQUSHT
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    //[ObjectPooling(true)]
    [ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000 * 5)]
    [Serializable]
    [System.EnterpriseServices.Description("ASSY1")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ASSY1 : ServicedComponent
    {
        #region StaticInfo

        /// <summary>
        /// 데이터 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataInfo_StaticInfo()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("EquipCode", typeof(string));
                dtDataInfo.Columns.Add("WriteDate", typeof(string));
                dtDataInfo.Columns.Add("Seq", typeof(string));
                dtDataInfo.Columns.Add("Process", typeof(string));
                dtDataInfo.Columns.Add("Criteria", typeof(string));
                dtDataInfo.Columns.Add("DecayTime", typeof(string));
                dtDataInfo.Columns.Add("Balance", typeof(string));
                dtDataInfo.Columns.Add("EtcDesc", typeof(string));


                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 데이터 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfDelDataInfo_StaticInfo()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("EquipCode", typeof(string));


                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 정전기관리정보 조회
        /// </summary>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>정전기관리정보</returns>
        [AutoComplete]
        public DataTable mfReadStaticInfo(string strEquipCode, string strFromDate, string strToDate,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtStaticInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtStaticInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUStaticInfo", dtParam);

                return dtStaticInfo;
            }
            catch (Exception ex)
            {
                return dtStaticInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtStaticInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 정전기관리정보 저장
        /// </summary>
        /// <param name="dtStaticInfo">정전기관리정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveStaticInfo(DataTable dtStaticInfo,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtStaticInfo.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtStaticInfo.Rows[i]["DocCode"].ToString(), 20);     //문서번호
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtStaticInfo.Rows[i]["EquipCode"].ToString(), 20); //설비
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtStaticInfo.Rows[i]["WriteDate"].ToString(), 10); //작성일
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtStaticInfo.Rows[i]["Seq"].ToString());                     //순번
                    sql.mfAddParamDataRow(dtParam, "@i_strProcess", ParameterDirection.Input, SqlDbType.VarChar, dtStaticInfo.Rows[i]["Process"].ToString(), 40);     //공정
                    sql.mfAddParamDataRow(dtParam, "@i_strCriteria", ParameterDirection.Input, SqlDbType.NVarChar, dtStaticInfo.Rows[i]["Criteria"].ToString(), 100); //기준
                    sql.mfAddParamDataRow(dtParam, "@i_strDecayTime", ParameterDirection.Input, SqlDbType.VarChar, dtStaticInfo.Rows[i]["DecayTime"].ToString(), 30); //DecayTime
                    sql.mfAddParamDataRow(dtParam, "@i_strBalance", ParameterDirection.Input, SqlDbType.NVarChar, dtStaticInfo.Rows[i]["Balance"].ToString(), 50);    //Balance
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtStaticInfo.Rows[i]["EtcDesc"].ToString(), 100);   //비고

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUStaticInfo", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 정전기관리정보 삭제
        /// </summary>
        /// <param name="strDocCode">문서번호</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteStaticInfo(DataTable dtDelStaticInfo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDelStaticInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelStaticInfo.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelStaticInfo.Rows[i]["EquipCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUStaticInfo", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally 
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region LaserPowerCheck

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_Laser()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("InspectDate", typeof(string));
                dtDataInfo.Columns.Add("InspectTime", typeof(string));
                dtDataInfo.Columns.Add("40UV", typeof(string));
                dtDataInfo.Columns.Add("40Meter", typeof(string));
                dtDataInfo.Columns.Add("60UV", typeof(string));
                dtDataInfo.Columns.Add("60Meter", typeof(string));
                dtDataInfo.Columns.Add("80UV", typeof(string));
                dtDataInfo.Columns.Add("80Meter", typeof(string));
                dtDataInfo.Columns.Add("100UV", typeof(string));
                dtDataInfo.Columns.Add("100Meter", typeof(string));
                dtDataInfo.Columns.Add("120UV", typeof(string));
                dtDataInfo.Columns.Add("120Meter", typeof(string));
                dtDataInfo.Columns.Add("140UV", typeof(string));
                dtDataInfo.Columns.Add("140Meter", typeof(string));
                dtDataInfo.Columns.Add("160UV", typeof(string));
                dtDataInfo.Columns.Add("160Meter", typeof(string));
                dtDataInfo.Columns.Add("180UV", typeof(string));
                dtDataInfo.Columns.Add("180Meter", typeof(string));
                dtDataInfo.Columns.Add("200UV", typeof(string));
                dtDataInfo.Columns.Add("200Meter", typeof(string));
                dtDataInfo.Columns.Add("InspectUserID", typeof(string));
                dtDataInfo.Columns.Add("UseLaserTime", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_Laser()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// LaserPowerCheck 조회
        /// </summary>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>LaserPowerCheck정보</returns>
        [AutoComplete]
        public DataTable mfReadLaserPowerCheck(string strFromDate, string strToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQULaserPowerCheck", dtParam);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// LaserPowerCheck 저장
        /// </summary>
        /// <param name="dtDataInfo"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveLaserPowerCheck(DataTable dtDataInfo,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);           //문서번호
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["InspectDate"].ToString(), 10);   //점검일
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTime", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["InspectTime"].ToString(), 10);   //점검시간
                    sql.mfAddParamDataRow(dtParam, "@i_str40UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["40UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str40Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["40Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str60UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["60UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str60Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["60Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str80UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["80UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str80Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["80Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str100UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["100UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str100Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["100Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str120UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["120UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str120Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["120Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str140UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["140UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str140Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["140Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str160UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["160UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str160Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["160Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str180UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["180UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str180Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["180Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str200UV", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["200UV"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str200Meter", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["200Meter"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["InspectUserID"].ToString(), 20);//점검자서명
                    sql.mfAddParamDataRow(dtParam, "@i_strUseLaserTime", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["UseLaserTime"].ToString(), 10); //Laser사용시간

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQULaserPowerCheck", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// LaserPowerCheck 삭제
        /// </summary>
        /// <param name="dtDelDataInfo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteLaserPowerCheck(DataTable dtDelDataInfo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDelDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelDataInfo.Rows[i]["DocCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQULaserPowerCheck", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region WheelChange

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_Wheel()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("WriteDate", typeof(string));
                dtDataInfo.Columns.Add("G1", typeof(string));
                dtDataInfo.Columns.Add("G2", typeof(string));
                dtDataInfo.Columns.Add("InitialThickness", typeof(string));
                dtDataInfo.Columns.Add("WareMount", typeof(string));
                dtDataInfo.Columns.Add("SerialNo", typeof(string));
                dtDataInfo.Columns.Add("1P", typeof(string));
                dtDataInfo.Columns.Add("2P", typeof(string));
                dtDataInfo.Columns.Add("3P", typeof(string));
                dtDataInfo.Columns.Add("4P", typeof(string));
                dtDataInfo.Columns.Add("5P", typeof(string));
                dtDataInfo.Columns.Add("InspectUserID", typeof(string));
                dtDataInfo.Columns.Add("EtcDesc", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_Wheel()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// WheelChange 조회
        /// </summary>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadWheelChange(string strFromDate,string strToDate,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Selete_EQUWheelChange", dtParam);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// WheelChange 저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveWheelChange(DataTable dtDataInfo, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);                   //문서번호
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["WriteDate"].ToString(), 10);               //작성일
                    sql.mfAddParamDataRow(dtParam, "@i_strG1", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["G1"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strG2", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["G2"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInitialThickness", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["InitialThickness"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWareMount", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["WareMount"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["SerialNo"].ToString(), 40);                 //고유번호
                    sql.mfAddParamDataRow(dtParam, "@i_str1P", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["1P"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str2P", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["2P"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str3P", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["3P"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str4P", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["4P"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_str5P", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["5P"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["InspectUserID"].ToString(), 20);       //점검자
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["EtcDesc"].ToString(), 100);                 //비고

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUWheelChange", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// WheelChange 저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteWheelChange(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);                   //문서번호
                    
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUWheelChange", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        #endregion

        #region TemperatureCheck

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_Temperature()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("WriteDate", typeof(string));
                dtDataInfo.Columns.Add("EquipCode", typeof(string));
                dtDataInfo.Columns.Add("MeasurementRegion", typeof(string));
                dtDataInfo.Columns.Add("SetTemperature", typeof(string));
                dtDataInfo.Columns.Add("SurveyTemperature", typeof(string));
                dtDataInfo.Columns.Add("CheckValue", typeof(string));
                dtDataInfo.Columns.Add("Contents", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_Temperature()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }


        /// <summary>
        /// 온도Check List 조회
        /// </summary>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadTemperatureCheck(string strFromDate, string strToDate, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUTemperatureCheck", dtParam);


                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 온도Check List 저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveTemperatureCheck(DataTable dtDataInfo,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasurementRegion", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["MeasurementRegion"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strSetTemperature", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["SetTemperature"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSurveyTemperature", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["SurveyTemperature"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckValue", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["CheckValue"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strContents", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Contents"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUTemperatureCheck", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 온도Check List 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteTemperatureCheck(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUTemperatureCheck", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region MasterWafer

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_Wafer()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("MeasurementDate", typeof(string));
                dtDataInfo.Columns.Add("MeasurementTime", typeof(string));
                dtDataInfo.Columns.Add("NowThickness", typeof(string));
                dtDataInfo.Columns.Add("Pass", typeof(string));
                dtDataInfo.Columns.Add("Fail", typeof(string));
                dtDataInfo.Columns.Add("Measurement", typeof(string));
                dtDataInfo.Columns.Add("MeasurementValue", typeof(string));
                dtDataInfo.Columns.Add("CheckUserID", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_Wafer()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }


        /// <summary>
        /// MasterWafer 측정정보 조회
        /// </summary>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMasterWafer(string strFromDate, string strToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUMasterWafer", dtParam);


                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// MasterWafer 측정정보 저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveMasterWafer(DataTable dtDataInfo, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasurementDate", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["MeasurementDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasurementTime", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["MeasurementTime"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strNowThickness", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["NowThickness"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPass", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Pass"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strFail", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Fail"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasurement", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["Measurement"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasurementValue", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["MeasurementValue"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckUserID", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["CheckUserID"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUMasterWafer", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MasterWafer 측정정보 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteMasterWafer(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUMasterWafer", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }





        #endregion

        #region InspectIPG

        #region InspectIPG 컬럼설정

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_IPGH()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocYear", typeof(string));
                dtDataInfo.Columns.Add("DocMonth", typeof(string));
                dtDataInfo.Columns.Add("EtcDesc", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_IPGH()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocYear", typeof(string));
                dtDataInfo.Columns.Add("DocMonth", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_IPGD()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocYear", typeof(string));
                dtDataInfo.Columns.Add("DocMonth", typeof(string));
                dtDataInfo.Columns.Add("Axis", typeof(string));
                dtDataInfo.Columns.Add("PeriodGubun", typeof(string));
                dtDataInfo.Columns.Add("Grind-1-1", typeof(string));
                dtDataInfo.Columns.Add("Grind-1-2", typeof(string));
                dtDataInfo.Columns.Add("Grind-2-1", typeof(string));
                dtDataInfo.Columns.Add("Grind-2-2", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_IPGD()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocYear", typeof(string));
                dtDataInfo.Columns.Add("DocMonth", typeof(string));
                dtDataInfo.Columns.Add("Axis", typeof(string));
                dtDataInfo.Columns.Add("PeriodGubun", typeof(string));


                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        #endregion


        /// <summary>
        /// IPG 점검 일지 헤더조회
        /// </summary>
        /// <param name="strDocYear">검색년도</param>
        /// <param name="strDocMonth">검색월</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectIPGH(string strDocYear,string strDocMonth,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strDocYear", ParameterDirection.Input, SqlDbType.Char, strDocYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strDocMonth", ParameterDirection.Input, SqlDbType.VarChar, strDocMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUInspectIPGH", dtParam);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// IPG 점검 일지 상세조회
        /// </summary>
        /// <param name="strDocYear">검색년도</param>
        /// <param name="strDocMonth">검색월</param>
        /// <param name="strPeriodGubun">주기구분</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectIPGD(string strDocYear, string strDocMonth, string strPeriodGubun, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strDocYear", ParameterDirection.Input, SqlDbType.Char, strDocYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strDocMonth", ParameterDirection.Input, SqlDbType.VarChar, strDocMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strPeriodGubun", ParameterDirection.Input, SqlDbType.VarChar, strPeriodGubun, 3);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUInspectIPGD", dtParam);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// IPG 점검 일지 헤더저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveInspectIPGH(DataTable dtDataInfoH, DataTable dtDataInfoD, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfoH.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocYear", ParameterDirection.Input, SqlDbType.Char, dtDataInfoH.Rows[i]["DocYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocMonth", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfoH.Rows[i]["DocMonth"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfoH.Rows[i]["EtcDesc"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUInspectIPGH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        strErrRtn = mfSaveInspectIPGD(dtDataInfoD, strUserIP, strUserID, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// IPG 점검 일지 상세저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveInspectIPGD(DataTable dtDataInfo, string strUserIP, string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocYear", ParameterDirection.Input, SqlDbType.Char, dtDataInfo.Rows[i]["DocYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocMonth", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocMonth"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strAxis", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["Axis"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPeriodGubun", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["PeriodGubun"].ToString(), 3);

                    sql.mfAddParamDataRow(dtParam, "@i_strGrind11", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Grind-1-1"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strGrind12", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Grind-1-2"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strGrind21", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Grind-2-1"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strGrind22", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Grind-2-2"].ToString(), 50);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUInspectIPGD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// IPG 점검 일지 헤더 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteInspectIPGH(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocYear", ParameterDirection.Input, SqlDbType.Char, dtDataInfo.Rows[i]["DocYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocMonth", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocMonth"].ToString(), 2);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUInspectIPGH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// IPG 점검 일지 상세 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteInspectIPGD(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocYear", ParameterDirection.Input, SqlDbType.Char, dtDataInfo.Rows[i]["DocYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocMonth", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocMonth"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strAxis", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["Axis"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPeriodGubun", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["PeriodGubun"].ToString(), 3);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUInspectIPGD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region SurveyUVEquipModel

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_UVEquipModel()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("UVEquipModel", typeof(string));
                dtDataInfo.Columns.Add("MeasurementDate", typeof(string));
                dtDataInfo.Columns.Add("Gubun", typeof(string));
                dtDataInfo.Columns.Add("WEK1", typeof(string));
                dtDataInfo.Columns.Add("WEK2", typeof(string));
                dtDataInfo.Columns.Add("WEK3", typeof(string));
                dtDataInfo.Columns.Add("WEK4", typeof(string));
                dtDataInfo.Columns.Add("WEK5", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_UVEquipModel()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }


        /// <summary>
        /// UV설비 광도/광량 실측 기록 Sheet 조회
        /// </summary>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strUVEquipModel">UV설비모델</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSurveyUVEquipModel(string strFromDate, string strToDate, string strUVEquipModel,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUVEquipModel", ParameterDirection.Input, SqlDbType.NVarChar, strUVEquipModel, 50);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSurveyUVEquipModel", dtParam);

                return dtDataInfo;

            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// UV설비 광도/광량 실측 기록 Sheet 저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveSurveyUVEquipModel(DataTable dtDataInfo, DataTable dtDelDataInfo ,string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                if (dtDelDataInfo.Rows.Count > 0)
                {
                    strErrRtn = mfDeleteSurveyUVEquipModel(dtDelDataInfo, sql.SqlCon, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUVEquipModel", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["UVEquipModel"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasurementDate", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["MeasurementDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGubun", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["Gubun"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParam, "@i_strWEK1", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["WEK1"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWEK2", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["WEK2"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWEK3", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["WEK3"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWEK4", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["WEK4"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWEK5", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["WEK5"].ToString(), 50);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSurveyUVEquipModel", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// UV설비 광도/광량 실측 기록 Sheet 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteSurveyUVEquipModel(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUSurveyUVEquipModel", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// UV설비 광도/광량 실측 기록 Sheet 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteSurveyUVEquipModel(DataTable dtDataInfo,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUSurveyUVEquipModel", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        #endregion

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    //[ObjectPooling(true)]
    [ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000 * 5)]
    [Serializable]
    [System.EnterpriseServices.Description("ASSY2")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ASSY2 : ServicedComponent
    {

        #region CleanerRubber

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_CleanerH()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("EquipCode", typeof(string));
                dtDataInfo.Columns.Add("QUA1", typeof(string));
                dtDataInfo.Columns.Add("QUA2", typeof(string));
                dtDataInfo.Columns.Add("QUA3", typeof(string));
                dtDataInfo.Columns.Add("QUA4", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_CleanerD()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("EquipCode", typeof(string));
                dtDataInfo.Columns.Add("WriteDate", typeof(string));
                dtDataInfo.Columns.Add("Legend", typeof(string));
                dtDataInfo.Columns.Add("Contents", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_Cleaner()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("EquipCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// CLEANER RUBBER 교체주기 설비조회
        /// </summary>
        /// <param name="strGubun"> 공백이면 설비목록, 공백이아니면 날짜별 설비리스트</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCleanerRubberH(string strGubun)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtPram = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtPram, "@i_strGubun", ParameterDirection.Input, SqlDbType.VarChar, strGubun, 1);

                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUCleanerRubberH",dtPram);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// CLEANER RUBBER 교체주기 설비교체내용 조회
        /// </summary>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCleanerRubberD(string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtPram = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtPram,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtPram,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUCleanerRubberD", dtPram);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// CLEANER RUBBER 교체주기 설비 저장
        /// </summary>
        /// <param name="dtDataInfo">설비교체주기정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCleanerRubberH(DataTable dtDataInfo,DataTable dtDelDataInfo, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                if (dtDelDataInfo.Rows.Count > 0)
                {
                  strErrRtn = mfDeleteCleanerRubberH(dtDelDataInfo, sql.SqlCon, trans);
                  ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                  
                  if (ErrRtn.ErrNum != 0)
                  {
                      trans.Rollback();
                      return strErrRtn;
                  }
                }
                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["EquipCode"].ToString(), 20);   //설비코드
                    sql.mfAddParamDataRow(dtParam, "@i_strQUA1", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["QUA1"].ToString(), 10);   //교체주기
                    sql.mfAddParamDataRow(dtParam, "@i_strQUA2", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["QUA2"].ToString(), 10);   //교체주기
                    sql.mfAddParamDataRow(dtParam, "@i_strQUA3", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["QUA3"].ToString(), 10);   //교체주기
                    sql.mfAddParamDataRow(dtParam, "@i_strQUA4", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["QUA4"].ToString(), 10);   //교체주기

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUCleanerRubberH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// CLEANER RUBBER 교체주기 설비교체내용 저장
        /// </summary>
        /// <param name="dtDataInfo">설비교체내용정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCleanerRubberD(DataTable dtDataInfo, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);   //문서번호
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["EquipCode"].ToString(), 20);   //설비코드
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["WriteDate"].ToString(), 10);   //작성일
                    sql.mfAddParamDataRow(dtParam, "@i_strLegend", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["Legend"].ToString(), 1);   //범례
                    sql.mfAddParamDataRow(dtParam, "@i_strContents", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Contents"].ToString(), 100);   //교체내용

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUCleanerRubberD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// CLEANER RUBBER 교체주기 설비 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteCleanerRubberH(DataTable dtDataInfo,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["EquipCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUCleanerRubberH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                    
                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// CLEANER RUBBER 교체주기 설비교체내용 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteCleanerRubberD(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["EquipCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUCleanerRubberD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region HistoryCard

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_History()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("Target", typeof(string));
                dtDataInfo.Columns.Add("TargetCode", typeof(string));
                dtDataInfo.Columns.Add("SerialNo", typeof(string));
                dtDataInfo.Columns.Add("TargetName", typeof(string));
                dtDataInfo.Columns.Add("WriteDate", typeof(string));
                dtDataInfo.Columns.Add("Cause", typeof(string));
                dtDataInfo.Columns.Add("ActionContents", typeof(string));
                dtDataInfo.Columns.Add("WorkerID", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_History()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 금형/설비 이력카드정보 조회
        /// </summary>
        /// <param name="strTarget">타겟조회 E : 설비 D: 금형 </param>
        /// <param name="strTargetCode">타겟코드 : 설비코드, 패키지</param>
        /// <param name="strSerial">Serial : 설비Serial , LotNo </param>
        /// <param name="strTargetName">타겟명 : 설비명, 금형명</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadHistoryCard(string strTarget, string strTargetCode ,string strSerialNo, string strTargetName,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strTarget", ParameterDirection.Input, SqlDbType.VarChar, strTarget, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strTargetCode", ParameterDirection.Input, SqlDbType.VarChar, strTargetCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.VarChar, strSerialNo, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strTargetName", ParameterDirection.Input, SqlDbType.NVarChar, strTargetName, 50);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUHistoryCard", dtParam);


                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형/설비 이력카드정보 저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveHistoryCard(DataTable dtDataInfo, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strTarget", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["Target"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strTargetCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["TargetCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["SerialNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strTargetName", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["TargetName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCause", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Cause"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionContents", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["ActionContents"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["WorkerID"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUHistoryCard", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형/설비 이력카드정보 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteHistoryCard(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUHistoryCard", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        #endregion

        #region DeviceChgMoldH

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_MoldH()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("EquipCode", typeof(string));
                dtDataInfo.Columns.Add("Device", typeof(string));
                dtDataInfo.Columns.Add("Seq", typeof(string));
                dtDataInfo.Columns.Add("ChangeTime", typeof(string));
                dtDataInfo.Columns.Add("LotNoTop", typeof(string));
                dtDataInfo.Columns.Add("LotNoBtm", typeof(string));
                dtDataInfo.Columns.Add("WorkerID", typeof(string));
                dtDataInfo.Columns.Add("AdmitUserID", typeof(string));
                dtDataInfo.Columns.Add("LotNo", typeof(string));
                dtDataInfo.Columns.Add("Exterior", typeof(string));
                dtDataInfo.Columns.Add("XRY", typeof(string));
                dtDataInfo.Columns.Add("AirVentLeakage", typeof(string));
                dtDataInfo.Columns.Add("RunningStartTime", typeof(string));
                dtDataInfo.Columns.Add("AdmitStatusCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_Mold()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("EquipCode", typeof(string));
                dtDataInfo.Columns.Add("Device", typeof(string));
                dtDataInfo.Columns.Add("Seq", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }


        /// <summary>
        /// 품종교체점검 조회
        /// </summary>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strDevice">품종</param>
        /// <param name="strStatusCode">승인상태</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgMoldH(string strEquipCode, string strDevice, string strStatusCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtPram = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtPram, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtPram, "@i_strDevice", ParameterDirection.Input, SqlDbType.NVarChar, strDevice, 50);
                sql.mfAddParamDataRow(dtPram, "@i_strStatusCode", ParameterDirection.Input, SqlDbType.VarChar, strStatusCode, 2);

                sql.mfAddParamDataRow(dtPram, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDeviceChgMoldH", dtPram);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 품종교체점검 헤더 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strDevice">품종</param>
        /// <param name="strSeq">순번</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgMoldH_Detail(string strPlantCode ,string strEquipCode, string strDevice, string strSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtPram = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtPram, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtPram, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtPram, "@i_strDevice", ParameterDirection.Input, SqlDbType.NVarChar, strDevice, 50);
                sql.mfAddParamDataRow(dtPram, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, strSeq);

                sql.mfAddParamDataRow(dtPram, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDeviceChgMoldH_Detail", dtPram);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 품종교체점검 저장
        /// </summary>
        /// <param name="dtDataInfo">설비교체주기정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveDeviceChgMoldH(DataTable dtDataInfo, DataTable dtDataInfoD,string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDevice", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Device"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDataInfo.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strChangeTime", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["ChangeTime"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNoTop", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["LotNoTop"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNoBtm", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["LotNoBtm"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["WorkerID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitUserID", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["AdmitUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strExterior", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Exterior"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strXRY", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["XRY"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAirVentLeakage", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["AirVentLeakage"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strRunningStartTime", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["RunningStartTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["AdmitStatusCode"].ToString(), 2);


                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_intSeq", ParameterDirection.Output, SqlDbType.Int);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUDeviceChgMoldH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        string strSeq = ErrRtn.mfGetReturnValue(0);

                        if (dtDataInfoD.Rows.Count > 0)
                        {
                            strErrRtn = mfDeleteDeviceChgMoldD(dtDataInfo.Rows[i]["EquipCode"].ToString(), dtDataInfo.Rows[i]["Device"].ToString(), strSeq, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }

                            strErrRtn = mfSaveDeviceChgMoldD(dtDataInfoD, strSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        
                    }

                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 품종교체점검 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDeviceChgMoldH(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDevice", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Device"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDataInfo.Rows[i]["Seq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUDeviceChgMoldH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region DeviceChgMoldD

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_MoldD()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("EquipCode", typeof(string));
                dtDataInfo.Columns.Add("Device", typeof(string));
                dtDataInfo.Columns.Add("Seq", typeof(string));
                dtDataInfo.Columns.Add("RowNum", typeof(string));
                dtDataInfo.Columns.Add("InspectRegion", typeof(string));
                dtDataInfo.Columns.Add("InspectCriteria", typeof(string));
                dtDataInfo.Columns.Add("InspectPoint", typeof(string));
                dtDataInfo.Columns.Add("Status", typeof(string));
                dtDataInfo.Columns.Add("Action", typeof(string));
                dtDataInfo.Columns.Add("FinalStatus", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }


        /// <summary>
        /// 품종교체점검 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strDevice">품종</param>
        /// <param name="strSeq">순번</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgMoldD(string strPlantCode, string strEquipCode, string strDevice, string strSeq, string strAdmitStatus,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtPram = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtPram, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtPram, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtPram, "@i_strDevice", ParameterDirection.Input, SqlDbType.NVarChar, strDevice, 50);
                sql.mfAddParamDataRow(dtPram, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, strSeq);
                sql.mfAddParamDataRow(dtPram, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, strAdmitStatus,2);

                sql.mfAddParamDataRow(dtPram, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDeviceChgMoldD", dtPram);

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 품종교체점검 저장
        /// </summary>
        /// <param name="dtDataInfo">설비교체주기정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveDeviceChgMoldD(DataTable dtDataInfo, string strSeq ,string strUserIP, string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDevice", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Device"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, strSeq);
                    sql.mfAddParamDataRow(dtParam, "@i_intRowNum", ParameterDirection.Input, SqlDbType.Int, dtDataInfo.Rows[i]["RowNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectRegion", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["InspectRegion"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectCriteria", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["InspectCriteria"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectPoint", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["InspectPoint"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strStatus", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Status"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAction", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Action"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFinalStatus", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["FinalStatus"].ToString(), 100);
                   

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUDeviceChgMoldD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }

                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 품종교체점검 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDeviceChgMoldD(string strEquipCode, string strDevice,string strSeq ,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strDevice", ParameterDirection.Input, SqlDbType.NVarChar, strDevice, 50);
                sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, strSeq);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUDeviceChgMoldD", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;
                    

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        #endregion

        #region InspectMoldDevice

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_InspectMold()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("RowNum", typeof(string));
                dtDataInfo.Columns.Add("InspectRegion", typeof(string));
                dtDataInfo.Columns.Add("InspectCriteria", typeof(string));
                dtDataInfo.Columns.Add("InspectPoint", typeof(string));
                dtDataInfo.Columns.Add("Status", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_InspectMold()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("RowNum", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }


        /// <summary>
        /// 점검항목 조회
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectMoldDevice()
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                //DataTable dtPram = sql.mfSetParamDataTable();


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUInspectMoldDevice");

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 품종교체 점검항목 저장
        /// </summary>
        /// <param name="dtDataInfo">저장할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveInspectMoldDevice(DataTable dtDataInfo, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_intRowNum", ParameterDirection.Input, SqlDbType.Int, dtDataInfo.Rows[i]["RowNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectRegion", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["InspectRegion"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectCriteria", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["InspectCriteria"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectPoint", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["InspectPoint"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strStatus", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Status"].ToString(), 50);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUInspectMoldDevice", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 품종교체 점검항목 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteInspectMoldDevice(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_intRowNum", ParameterDirection.Input, SqlDbType.Int, dtDataInfo.Rows[i]["RowNum"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUInspectMoldDevice", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region MoldPackageLot

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_PackageLot()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));
                dtDataInfo.Columns.Add("Package", typeof(string));
                dtDataInfo.Columns.Add("LotNo", typeof(string));
                dtDataInfo.Columns.Add("Name", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 데이터컬럼정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDelDataInfo_PackageLot()
        {
            DataTable dtDataInfo = new DataTable();

            try
            {
                dtDataInfo.Columns.Add("DocCode", typeof(string));

                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// MOLD Package별 Lot조회
        /// </summary>
        /// <param name="strPackage"></param>
        /// <param name="strLotNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMoldPackageLot(string strPackage, string strLotNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDataInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dtDataInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUMoldPackageLot", dtParam);


                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDataInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// MOLD Package별 Lot저장
        /// </summary>
        /// <param name="dtDataInfo"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMoldPackageLot(DataTable dtDataInfo, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strName", ParameterDirection.Input, SqlDbType.NVarChar, dtDataInfo.Rows[i]["Name"].ToString(), 50);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUMoldPackageLot", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MOLD Package별 Lot 삭제
        /// </summary>
        /// <param name="dtDataInfo">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteMoldPackageLot(DataTable dtDataInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDataInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDataInfo.Rows[i]["DocCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUMoldPackageLot", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

    }
}
