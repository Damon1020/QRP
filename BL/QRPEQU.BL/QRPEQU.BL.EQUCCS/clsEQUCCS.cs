﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비변경점관리                                        */
/* 모듈(분류)명 : 설비인증의뢰요청                                      */
/* 프로그램ID   : clsMASEQU.cs                                          */
/* 프로그램명   : 설비관리기준정보                                      */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2011-07-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
Authentication = AuthenticationOption.None,
ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPEQU.BL.EQUCCS
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EQUEquipCertiH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    public class EQUEquipCertiH : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public EQUEquipCertiH()
        {
        }

        public EQUEquipCertiH(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 설비 인증 정보 헤더 데이터테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtCertiH = new DataTable();
            try
            {
                dtCertiH.Columns.Add("PlantCode", typeof(String));
                dtCertiH.Columns.Add("DocCode", typeof(String));
                dtCertiH.Columns.Add("EquipCode", typeof(String));
                dtCertiH.Columns.Add("EquipName", typeof(String));
                dtCertiH.Columns.Add("ProcessCode", typeof(String));
                dtCertiH.Columns.Add("Subject", typeof(String));
                dtCertiH.Columns.Add("Object", typeof(String));
                dtCertiH.Columns.Add("SetupChargeID", typeof(String));
                dtCertiH.Columns.Add("SetupDeptCode", typeof(String));
                dtCertiH.Columns.Add("SetupFinishDate", typeof(String));
                dtCertiH.Columns.Add("UseDeptCode", typeof(String));
                dtCertiH.Columns.Add("CertiReqDate", typeof(String));
                dtCertiH.Columns.Add("CertiReqID", typeof(String));
                dtCertiH.Columns.Add("EquipCertiReqTypeCode", typeof(String));
                dtCertiH.Columns.Add("EquipCertiReqTypeName", typeof(String));
                dtCertiH.Columns.Add("EquipStandardFlag", typeof(String));
                dtCertiH.Columns.Add("WorkStandardFlag", typeof(String));
                dtCertiH.Columns.Add("TechStandardFlag", typeof(String));
                dtCertiH.Columns.Add("AdmitStatusCode", typeof(String));
                dtCertiH.Columns.Add("RejectReason", typeof(String));
                dtCertiH.Columns.Add("CertiResultCode", typeof(String));
                dtCertiH.Columns.Add("CertiChargeID", typeof(String));
                dtCertiH.Columns.Add("CertiDate", typeof(String));
                dtCertiH.Columns.Add("CertiAdmitID", typeof(String));
                dtCertiH.Columns.Add("CertiAdmitDate", typeof(String));
                dtCertiH.Columns.Add("EtcDesc", typeof(String));
                dtCertiH.Columns.Add("MTReasonCode", typeof(String));
                dtCertiH.Columns.Add("EquipReasonCode", typeof(String));
                dtCertiH.Columns.Add("MTReasonEtcDesc", typeof(String));
                dtCertiH.Columns.Add("EquipReasonEtcDesc", typeof(String));
                dtCertiH.Columns.Add("MoveDeptCode", typeof(String));
                dtCertiH.Columns.Add("OriginDeptCode", typeof(String));
                dtCertiH.Columns.Add("IncDecEtcDesc", typeof(String));
                dtCertiH.Columns.Add("MESTFlag", typeof(String));
                dtCertiH.Columns.Add("MESTDate", typeof(String));
                dtCertiH.Columns.Add("WriteComplete", typeof(String));
                dtCertiH.Columns.Add("ModelName", typeof(String));
                dtCertiH.Columns.Add("SerialNo", typeof(String));
                dtCertiH.Columns.Add("VendorCode", typeof(String));
                dtCertiH.Columns.Add("GWTFlag", typeof(string));
                dtCertiH.Columns.Add("MDMTFlag", typeof(string));
                dtCertiH.Columns.Add("Package", typeof(string));
                return dtCertiH;
            }
            catch (Exception ex)
            {
                return dtCertiH;
                throw (ex);
            }
            finally
            {
                dtCertiH.Dispose();
            }
        }


        /// <summary>
        /// 설비인증 정보 헤더 (공장,문서번호) 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDocCode">문서번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비인증 정보 헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipCertiH_Detail(string strPlantCode, string strDocCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCertiH = new DataTable();

            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //설비인증 정보 헤더 (공장,문서번호) 검색 프로시저실행
                dtCertiH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiH_Detail", dtParam);

                //설비인증요청 헤더 조회 정보
                return dtCertiH;
            }
            catch (Exception ex)
            {
                return dtCertiH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCertiH.Dispose();
            }
        }


        /// <summary>
        /// 설비 인증정보 검색
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEQUEquipCertiH(String strPlantCode, String strSetupCompleteDateFrom, String strSetupCompleteDateTo,String strEquipCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCertiH = new DataTable();
            try 
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSetupCompleteDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strSetupCompleteDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSetupCompleteDateTo", ParameterDirection.Input, SqlDbType.VarChar, strSetupCompleteDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                 
                dtCertiH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiH", dtParam);

                return dtCertiH;
            }
            catch(Exception ex) 
            {
                return dtCertiH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCertiH.Dispose();
            }
        }

        /// <summary>
        /// 설비인증시 선택한 설비의 하위설비(SubEquip)검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEQUEquipCertiSubEquip(string strPlantCode, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSubEquip = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtSubEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiSubEquip", dtParam);
                return dtSubEquip;
            }
            catch (Exception ex)
            {
                return dtSubEquip;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtSubEquip.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 규격서 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strPackage"></param>
        /// <param name="strProcessCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOProcInspectD(string strPlantCode, string strPackage, string strProcessCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcInspectD_EquipCerti", dtParam);
                return dtRtn;
            }
            catch(Exception ex)
            {
                return dtRtn;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 인증 접수/평가 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strProcessCode"></param>
        /// <param name="strEquipCertiReqTypeCode"></param>
        /// <param name="strEquipCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEQUEquipCertiHForAdmit(String strPlantCode, String strProcessCode, String strEquipCertiReqTypeCode, String strEquipCode, String strCertiReqDateFrom
                                                        , String strCertiReqDateTo, String strLang)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            DataTable dtCertiH = new DataTable();
            try 
            {
                sql.mfConnect();      //실행용
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCertiReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCertiReqTypeCode, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCertiReqDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strCertiReqDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCertiReqDateTo", ParameterDirection.Input, SqlDbType.VarChar, strCertiReqDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtCertiH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiHForAdmit", dtParam);   //실행용
                //dtCertiH = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUEquipCertiHForAdmit", dtParam); //디버깅용

                return dtCertiH;
            }
            catch(Exception ex) 
            {
                return dtCertiH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCertiH.Dispose();
            }
        }


        /// <summary>
        /// 설비인증 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="SaveFile"></param>
        /// <param name="DelFile"></param>
        /// <param name="SaveCertiItem"></param>
        /// <param name="DelCertiItem"></param>
        /// <param name="SaveCertiP"></param>
        /// <param name="DelCertiP"></param>
        /// <param name="SaveEquSetup"></param>
        /// <param name="DelEquSetup"></param>
        /// <param name="SaveCondition"></param>
        /// <param name="DelCondition"></param>
        /// <param name="SaveDMat"></param>
        /// <param name="DelDMat"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEQUEquipCertiH(DataTable dt, DataTable SaveFile, DataTable DelFile, DataTable SaveCertiItem, DataTable dtCertiItemQC, DataTable DelCertiItem, DataTable SaveCertiP
                                            , DataTable DelCertiP, DataTable SaveEquSetup, DataTable DelEquSetup, DataTable SaveCondition, DataTable DelCondition
                                            , DataTable SaveDMat, DataTable DelDMat, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strDoc = "";


            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSubject", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Subject"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strObject", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Object"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strSetupChargeID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SetupChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strSetupDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SetupDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSetupFinishDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SetupFinishDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["UseDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiReqDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiReqID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiReqID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCertiReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["EquipCertiReqTypeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipStandardFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["EquipStandardFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkStandardFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WorkStandardFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strTechStandardFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["TechStandardFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AdmitStatusCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strRejectReason", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["RejectReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiResultCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiResultCode"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiChargeID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiAdmitID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiAdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTReasonCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MTReasonCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipReasonCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["EquipReasonCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTReasonEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MTReasonEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipReasonEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EquipReasonEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strOriginDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["OriginDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoveDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MoveDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strIncDecEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["IncDecEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strGWTFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["GWTFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@o_strDocCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipCertiH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        strDoc = strErrRtn;
                        String strDocCode = ErrRtn.mfGetReturnValue(0);
                        //저장 성공시 삭제 먼저 처리
                        if (DelFile.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipCertiFile delFile = new EQUEquipCertiFile();
                            strErrRtn = delFile.mfDelCertiFile(DelFile, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveFile.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipCertiFile saveFile = new EQUEquipCertiFile();
                            strErrRtn = saveFile.mfSaveCertiFile(SaveFile, strUserIP, strUserID, sql, trans, strDocCode);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (DelCertiItem.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipCertiItem delItem = new EQUEquipCertiItem();
                            strErrRtn = delItem.mfDelCertiItem(DelCertiItem, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveCertiItem.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipCertiItem saveItem = new EQUEquipCertiItem();
                            strErrRtn = saveItem.mfSaveCertiItem(SaveCertiItem, strUserIP, strUserID, sql, trans, strDocCode);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (dtCertiItemQC.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipCertiItemQC saveItemQC = new EQUEquipCertiItemQC();
                            strErrRtn = saveItemQC.mfSaveCertiItemQC(dtCertiItemQC, strUserIP, strUserID, sql, trans, strDocCode);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (DelCertiP.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipCertiP delCertiP = new EQUEquipCertiP();
                            strErrRtn = delCertiP.mfDelCertiP(DelCertiP, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveCertiP.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipCertiP saveCertiP = new EQUEquipCertiP();
                            strErrRtn = saveCertiP.mfSaveCertiP(SaveCertiP, strUserIP, strUserID, sql, trans, strDocCode);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (DelCondition.Rows.Count > 0)
                        {
                            EQUCCS.EQUSetupCondition delCondition = new EQUSetupCondition();
                            strErrRtn = delCondition.mfDelSetupCondition(DelCondition, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveCondition.Rows.Count > 0)
                        {
                            EQUCCS.EQUSetupCondition saveCondition = new EQUSetupCondition();
                            strErrRtn = saveCondition.mfSaveSetupCondition(SaveCondition, strUserIP, strUserID, sql, trans, strDocCode);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (DelDMat.Rows.Count > 0)
                        {
                            EQUCCS.EQUSetupDurableMat delDMat = new EQUSetupDurableMat();
                            strErrRtn = delDMat.mfDelDurableMat(DelDMat, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveDMat.Rows.Count > 0)
                        {
                            EQUCCS.EQUSetupDurableMat saveDMat = new EQUSetupDurableMat();
                            strErrRtn = saveDMat.mfSaveDurableMat(SaveDMat, strUserIP, strUserID, sql, trans, strDocCode);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (DelEquSetup.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipSetup delEquSetup = new EQUEquipSetup();
                            strErrRtn = delEquSetup.mfDelEquipSetup(DelEquSetup, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveEquSetup.Rows.Count > 0)
                        {
                            EQUCCS.EQUEquipSetup saveEquSetup = new EQUEquipSetup();
                            strErrRtn = saveEquSetup.mfSaveEquipSetup(SaveEquSetup, strUserIP, strUserID, sql, trans, strDocCode);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        //if (dtCertiItemQC.Rows.Count > 0)
                        //{
                        //    EQUCCS.EQUEquipCertiItemQC saveCertiItemQC = new EQUEquipCertiItemQC();
                        //    strErrRtn = saveCertiItemQC.mfSaveCertiItemQC(dtCertiItemQC, strUserIP, strUserID, sql, trans, strDoc);
                        //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        //    if (ErrRtn.ErrNum != 0)
                        //    {
                        //        trans.Rollback();
                        //        break;
                        //    }
                        //}
                        

                        ////////EQUCertiItemQC테이블에도 똑같이 저장해줌 : DelCertiItem 데이터테이블을 이용함(모든값이 동일하기 때문)
                        ////////EquCertiItemQC테이블 구조 변경(공정검사 규격서에서 가지고 오도록 변경)
                        //////if (DelCertiItem.Rows.Count > 0)
                        //////{
                        //////    EQUCCS.EQUEquipCertiItemQC delItemQC = new EQUEquipCertiItemQC();
                        //////    strErrRtn = delItemQC.mfDelCertiItemQC(DelCertiItem, sql, trans);
                        //////    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        //////    if (ErrRtn.ErrNum != 0)
                        //////    {
                        //////        trans.Rollback();
                        //////        break;
                        //////    }
                        //////}
                        //////if (SaveCertiItem.Rows.Count > 0)
                        //////{
                        //////    EQUCCS.EQUEquipCertiItemQC saveItemQC = new EQUEquipCertiItemQC();
                        //////    strErrRtn = saveItemQC.mfSaveCertiItemQC(SaveCertiItem, strUserIP, strUserID, sql, trans, strDocCode);
                        //////    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        //////    if (ErrRtn.ErrNum != 0)
                        //////    {
                        //////        trans.Rollback();
                        //////        break;
                        //////    }
                        //////}

                        strErrRtn = strDoc;
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }

            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비인증 관리 승인/반려 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="SaveItem"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEQUEquipCertiHForAdmit(DataTable dt, DataTable SaveItemQC, DataTable DelItemQC, DataTable dtFileInfo,string strUserIP, string strUserID, string strFormName)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();        //실행용

                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (!dt.Rows[i]["WriteComplete"].ToString().Equals("T"))
                    {
                        trans = sql.SqlCon.BeginTransaction();  //실행용
                        //trans = m_SqlConnDebug.BeginTransaction();          //디버깅용

                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AdmitStatusCode"].ToString(), 2);
                        sql.mfAddParamDataRow(dtParam, "@i_strCertiResultCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiResultCode"].ToString(), 2);
                        sql.mfAddParamDataRow(dtParam, "@i_strRejectReason", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["RejectReason"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strCertiAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiAdmitID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strCertiAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiAdmitDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strCertiChargeID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiChargeID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strCertiDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CertiDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strMDMTFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["MDMTFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipCertiHForAdmit", dtParam);        //실행용
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUEquipCertiHForAdmit", dtParam);        //디버깅용

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        else
                        {
                            if (DelItemQC.Rows.Count > 0)
                            {
                                EQUCCS.EQUEquipCertiItemQC delItemQC = new EQUEquipCertiItemQC();
                                strErrRtn = delItemQC.mfDelCertiItemQC(DelItemQC, sql, trans);
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                            if (SaveItemQC.Rows.Count > 0)
                            {
                                EQUCCS.EQUEquipCertiItemQC saveItemQC = new EQUEquipCertiItemQC();
                                //strErrRtn = saveItemQC.mfSaveCertiItemForAdmin(SaveItemQC, strUserIP, strUserID, sql.SqlCon, trans);
                                strErrRtn = saveItemQC.mfSaveCertiItemQCForAdmin(SaveItemQC, strUserIP, strUserID, sql, trans);
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            if (dtFileInfo.Rows.Count > 0)
                            {
                                EQUCCS.EQUEquipCertiFile clsFile = new EQUEquipCertiFile();
                                //strErrRtn = saveItemQC.mfSaveCertiItemForAdmin(SaveItemQC, strUserIP, strUserID, sql.SqlCon, trans);
                                strErrRtn = clsFile.mfSaveCertiFile(dtFileInfo, strUserIP, strUserID, sql, trans,dt.Rows[i]["DocCode"].ToString());
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }

                            }

                            if(ErrRtn.ErrNum == 0)
                                trans.Commit();
                        }
                    }
                    //DB상 작성완료 상태일 경우
                    else
                    {
                        ErrRtn.ErrNum = 0;
                    }

                    #region MES I/F 주석처리
                    ////////QRP에 성공적으로 저장되었으면 MES로 전송
                    //////if (ErrRtn.ErrNum == 0)
                    //////{
                    //////    if (dt.Rows[i]["AdmitStatusCode"].ToString() == "FN" && dt.Rows[i]["MESTFlag"].ToString() == "F")
                    //////    {
                    //////        QRPSYS.BL.SYSPGM.SystemAccessInfo SysAcc = new QRPSYS.BL.SYSPGM.SystemAccessInfo();

                    //////        DataTable dtSysAcc = SysAcc.mfReadSystemAccessInfoDetail(dt.Rows[i]["PlantCode"].ToString(), "S04");
                    //////        DataTable dtSysAcc = SysAcc.mfReadSystemAccessInfoDetail(dt.Rows[i]["PlantCode"].ToString(), "S07");

                    //////        QRPMES.IF.Tibrv tib = new QRPMES.IF.Tibrv(dtSysAcc); //("1", dtSysAcc);
                    //////        DataTable dtResult = tib.EQP_CONFIRM_REQ(strFormName, dt.Rows[i]["CertiChargeID"].ToString(), dt.Rows[i]["EquipCode"].ToString(), strUserIP);
                    //////        if (dtResult.Rows.Count > 0)
                    //////        {
                    //////            if (dtResult.Rows[0][0].ToString() == "0")
                    //////            {
                    //////                sql.mfConnect();

                    //////                //MES로 성공적으로 전송했으면 MESTFlag 처리
                    //////                SqlTransaction transt;
                    //////                transt = sql.SqlCon.BeginTransaction(); //실행용
                    //////                //transt = m_SqlConnDebug.BeginTransaction();          //디버깅용

                    //////                DataTable dtParamt = sql.mfSetParamDataTable();
                    //////                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    //////                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    //////                sql.mfAddParamDataRow(dtParamt, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    //////                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    //////                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    //////                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //////                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_EQUEquipCertiHMESFlag", dtParamt); // 실행용 
                    //////                //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, transt, "up_Update_EQUEquipCertiHMESFlag", dtParamt);  //디버깅용

                    //////                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    //////                if (ErrRtn.ErrNum != 0)
                    //////                    transt.Rollback();
                    //////                else
                    //////                    transt.Commit();
                    //////            }
                    //////            //성공,실패면 MES오류 메세지를 처리한다.

                    //////            ErrRtn.InterfaceResultCode = dtResult.Rows[0][0].ToString();
                    //////            ErrRtn.InterfaceResultMessage = dtResult.Rows[0][1].ToString();
                    //////            //MES
                    //////            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);

                    //////        }
                    //////    }
                    //////}
                    #endregion
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 설비 인증 요청 삭제
        /// </summary>
        /// <param name="dtH"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteEquipCertiH(DataTable dtH)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtH.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    //CertiFile
                    EQUEquipCertiFile CFile = new EQUEquipCertiFile();
                    strErrRtn = CFile.mfDelCertiFileAll(dtH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    //CertiItem
                    EQUEquipCertiItem CItem = new EQUEquipCertiItem();
                    strErrRtn = CItem.mfDelCertiItemAll(dtH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    //CertiP
                    EQUEquipCertiP CertiP = new EQUEquipCertiP();
                    strErrRtn = CertiP.mfDelCertiPAll(dtH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    //Setup
                    EQUEquipSetup Setup = new EQUEquipSetup();
                    strErrRtn = Setup.mfDelEquipSetupAll(dtH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    //SetupCondition
                    EQUSetupCondition Condition = new EQUSetupCondition();
                    strErrRtn = Condition.mfDelSetupConditionAll(dtH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    //SetupDurableMat
                    EQUSetupDurableMat DMat = new EQUSetupDurableMat();
                    strErrRtn = DMat.mfDelDurableMatAll(dtH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    //CertiItemQC
                    EQUEquipCertiItemQC QC = new EQUEquipCertiItemQC();
                    strErrRtn = QC.mfDelCertiItemQCAll(dtH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtH.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiH", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            { 
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        /// <summary>
        /// 설비인증요층 // 결재연동
        /// </summary>
        /// <param name="dtHeader">EQUEquipCertiH DataTable</param>
        /// <param name="dtSendLine">결재선 정보 DataTable</param>
        /// <param name="dtCcLine">통보선 정보 DataTable</param>
        /// <param name="dtFormInfo">FormInfo DataTable</param>
        /// <param name="strUserIP">IP</param>
        /// <param name="strUserID">ID</param>
        /// <returns>DataTable
        ///             CD_CODE : "00"
        ///             CD_STATUS</returns>
        [AutoComplete(false)]
        public DataTable mfEquipGRWApproval(DataTable dtHeader, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        {
            DataTable dtRtn = new DataTable();
            QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
            DataTable dtFileInfo = grw.mfSetFileInfoDataTable();

            try
            {
                //Legacykey
                string strPlantCode = dtHeader.Rows[0]["PlantCode"].ToString();
                string strDocCode = dtHeader.Rows[0]["DocCode"].ToString();
                string strLegacyKey = strPlantCode + "||" + strDocCode;
                string strEquipCode = dtHeader.Rows[0]["EquipCode"].ToString();
                string strEquipName = dtHeader.Rows[0]["EquipName"].ToString();
                string strModelName = dtHeader.Rows[0]["ModelName"].ToString();
                string strSerialNo = dtHeader.Rows[0]["SerialNo"].ToString();
                string strVendorCode = dtHeader.Rows[0]["VendorCode"].ToString();
                string strReason = dtHeader.Rows[0]["EquipCertiReqTypeName"].ToString();
                string strPurpose = dtHeader.Rows[0]["Object"].ToString();

                string strFilePath = "EQUEquipCertiFile\\";
                EQUEquipCertiFile file = new EQUEquipCertiFile();
                DataTable dtfile = file.mfReadCertiFile(strPlantCode, strDocCode, "KOR");

                #region CreateDataTable

                //FileInfo
                foreach (DataRow dr in dtfile.Rows)
                {
                    DataRow _dr = dtFileInfo.NewRow();

                    _dr["PlantCode"] = strPlantCode;
                    _dr["NM_FILE"] = dr["FileName"].ToString();
                    _dr["NM_LOCATION"] = strFilePath;

                    //전자결재 시 첨부파일 에러로 인하여 일단 주석처리. 추후 수정 필요 2012.10.09 Yoon 
                    //dtFileInfo.Rows.Add(_dr);
                }
                #endregion

                dtRtn = grw.Equip(strLegacyKey, dtSendLine, dtCcLine, strEquipCode, strEquipName, strModelName, strVendorCode,
                    strSerialNo, strReason, strPurpose, dtFormInfo, dtFileInfo, strUserIP, strUserID);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }


            return dtRtn;
        }

        /// <summary>
        /// 설비인증시 MDM에 QUALYN Flag 전송
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDocCode">의뢰번호</param>
        /// <param name="strQUALYN">Y:설비인증접수평가시 | N:설비인증의뢰요청</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSave_MDMIF_QRP_MCH_SPEC(string strPlantCode, string strDocCode, string strQUALYN, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strQUALYN", ParameterDirection.Input, SqlDbType.Char, strQUALYN, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_MDMIF_QRP_MCH_SPEC", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EQUEquipSetup")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

  
    public class EQUEquipSetup : ServicedComponent
    {
        /// <summary>
        /// 설비 가동 조건 검색
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            
            DataTable dtEquipSetup = new DataTable();

            try
            {
                dtEquipSetup.Columns.Add("PlantCode", typeof(String));
                dtEquipSetup.Columns.Add("DocCode", typeof(String));
                dtEquipSetup.Columns.Add("Seq", typeof(int));
                dtEquipSetup.Columns.Add("SetupDate", typeof(String));
                dtEquipSetup.Columns.Add("ProgressDesc", typeof(String));

                return dtEquipSetup;
            }
            catch(Exception ex)
            {
                return dtEquipSetup;
                throw (ex);
            }
            finally
            {
                dtEquipSetup.Dispose();
            }
        }

        /// <summary>
        /// 설비 가동조건 검색
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipSetup(String strPlantCode, String strDocCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipSetup = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtEquipSetup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipSetup", dtParam);

            
                return dtEquipSetup;
            }
            catch(Exception ex)
            {
                return dtEquipSetup;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipSetup.Dispose();
            }
        }

        /// <summary>
        /// 설비인증의뢰 현황 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDocCode"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipSetup(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, string strDocCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strSetupDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SetupDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProgressDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ProgressDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipSetup", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;

                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {

            }
        }

        /// <summary>
        /// 설비 setup현황 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelEquipSetup(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipSetup", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {

            }
        }


        /// <summary>
        /// 그리드 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelEquipSetupAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipSetupAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {

            }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EQUSetupDurableMat")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    public class EQUSetupDurableMat : ServicedComponent
    {
        /// <summary>
        /// 금형치공구현황 데이터테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtDurableMat = new DataTable();

            try
            {
                dtDurableMat.Columns.Add("PlantCode", typeof(String));
                dtDurableMat.Columns.Add("DocCode", typeof(String));
                dtDurableMat.Columns.Add("Seq", typeof(int));
                dtDurableMat.Columns.Add("DurableMatCode", typeof(String));
                dtDurableMat.Columns.Add("EtcDesc", typeof(String));

                return dtDurableMat;
            }
            catch(Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                dtDurableMat.Dispose();
            }
        }


        /// <summary>
        /// 금형/치공구 조회용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDurableMat(String strPlantCode, String strDocCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableMat = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSetupDurableMat", dtParam);
            
                return dtDurableMat;
            }
            catch(Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }

        /// <summary>
        /// 그리드에서 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="DurableMatCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDurable(String strPlantCode, String DurableMatCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurable = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDurable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatName", dtParam);

                return dtDurable;
            }
            catch(Exception ex)
            {
                return dtDurable;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDurable.Dispose();
            }
        }

        /// <summary>
        /// 금형 치공구 현황 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDocCode"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveDurableMat(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, String strDocCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSetupDurableMat", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }


        /// <summary>
        /// 금형 치공구 현황 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelDurableMat(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUSetupDurableMat", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelDurableMatAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUSetupDurableMatAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }



    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EQUSetupCondition")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    public class EQUSetupCondition : ServicedComponent
    {

        /// <summary>
        /// Setup 현황 데이터 테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtSetupCondition = new DataTable();
            try
            {
                dtSetupCondition.Columns.Add("PlantCode", typeof(String));
                dtSetupCondition.Columns.Add("DocCode", typeof(String));
                dtSetupCondition.Columns.Add("Seq", typeof(int));
                dtSetupCondition.Columns.Add("SpecStandard", typeof(String));
                dtSetupCondition.Columns.Add("Parameter", typeof(String));
                dtSetupCondition.Columns.Add("WorkCondition", typeof(String));
                dtSetupCondition.Columns.Add("EtcDesc", typeof(String));

                return dtSetupCondition;
            }
            catch(Exception ex)
            {
                return dtSetupCondition;
                throw (ex);
            }
            finally
            {
                dtSetupCondition.Dispose();
            }
        }

        /// <summary>
        /// Setup현황 조회
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSetupCondition(String strPlantCode, String strDocCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSetupCondition = new DataTable();

            try
            {
                sql.mfConnect();
            DataTable dtParam = sql.mfSetParamDataTable();


            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
            sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
            sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

            dtSetupCondition = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSetupCondition", dtParam);
            
            return dtSetupCondition;
            }

            catch(Exception ex)
            {
                return dtSetupCondition;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtSetupCondition.Dispose();
            }
        }
        /// <summary>
        /// 설비인증요청 가종조건 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDocCode"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveSetupCondition(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, string strDocCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecStandard", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["SpecStandard"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strParameter", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Parameter"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkCondition", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["WorkCondition"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSetupCondition", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }

        /// <summary>
        /// 설비setup가동조건 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>

        [AutoComplete(false)]
        public string mfDelSetupCondition(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUSetupCondition", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }


        /// <summary>
        /// 그리드 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelSetupConditionAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUSetupConditionAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EQUEquipCertiItem")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    public class EQUEquipCertiItem : ServicedComponent
    {
        /// <summary>
        /// 설비인증항목 데이터테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtCertiItem = new DataTable();

            try
            {
                dtCertiItem.Columns.Add("PlantCode", typeof(String));
                dtCertiItem.Columns.Add("DocCode", typeof(String));
                dtCertiItem.Columns.Add("Seq", typeof(int));
                dtCertiItem.Columns.Add("CertiItem", typeof(String));
                dtCertiItem.Columns.Add("SampleSize", typeof(String));
                dtCertiItem.Columns.Add("JudgeStandard", typeof(String));
                dtCertiItem.Columns.Add("EvalResultCode", typeof(String));
                dtCertiItem.Columns.Add("JudgeResultCode", typeof(String));

                return dtCertiItem;
            }
            catch(Exception ex)
            {
                return dtCertiItem;
                throw (ex);
            }
            finally
            {
                dtCertiItem.Dispose();
            }
        }
        /// <summary>
        /// 설비인증항목 평가결과 검색
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCertiItem(String strPlantCode, String strDocCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCertiItem = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dtCertiItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiItem", dtParam);
        
                return dtCertiItem;
            }
            catch (System.Exception ex)
            {
                return dtCertiItem;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCertiItem.Dispose();
            }
        }

        /// <summary>
        /// 설비인증항목 및 평가결과 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDocCode"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCertiItem(DataTable dt, string strUserIP, string strUserID,  SQLS sql, SqlTransaction trans, string strDocCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiItem", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["CertiItem"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strSampleSize", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["SampleSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strJudgeStandard", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["JudgeStandard"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEvalResultCode", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EvalResultCode"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strJudgeResultCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["JudgeResultCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipCertiItem", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {}
        }


        /// <summary>
        /// 설비인증 접수, 평가 저장용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCertiItemForAdmin(DataTable dt, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            SQLS sql = new SQLS();
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEvalResultCode", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EvalResultCode"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strJudgeResultCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["JudgeResultCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUEquipCertiItemForAdmit", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비인증항목 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelCertiItem(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiItem", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelCertiItemAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiItemAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EQUEquipCertiFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    public class EQUEquipCertiFile : ServicedComponent
    {
        /// <summary>
        /// 설비인증 의뢰요청 첨부파일 데이터테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtCertiFile = new DataTable();
            try
            {
                dtCertiFile.Columns.Add("PlantCode", typeof(String));
                dtCertiFile.Columns.Add("DocCode", typeof(String));
                dtCertiFile.Columns.Add("Seq", typeof(int));
                dtCertiFile.Columns.Add("Subject", typeof(String));
                dtCertiFile.Columns.Add("FileName", typeof(String));
                dtCertiFile.Columns.Add("EtcDesc", typeof(String));

                return dtCertiFile;
            }
            catch(Exception ex)
            {
                return dtCertiFile;
                throw(ex);
            }
            finally
            {
                dtCertiFile.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadACertiFileDocSeq(String strPlantCode, String strDocCode)
        {
            SQLS sql = new SQLS();
            DataTable dtSeq = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);

                dtSeq = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiFileSeq", dtParam);

                return dtSeq;
            }
            catch (Exception ex)
            {
                return dtSeq;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtSeq.Dispose();
            } 
        }
         /// <summary>
        /// 설비인증 의뢰요청 첨부파일 검색
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCertiFile(String strPlantCode, String strDocCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCertiFile = new DataTable();

            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtCertiFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiFile", dtParam);
                
                return dtCertiFile;
            }
            catch(Exception ex)
            {
                return dtCertiFile;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCertiFile.Dispose();
            }
        }

        /// <summary>
        /// 첨부파일 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDocCode"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCertiFile(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, string strDocCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strSubject", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Subject"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipCertiFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }
        /// <summary>
        /// 첨부파일 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelCertiFile(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally 
            {
            }
        }
        /// <summary>
        /// 그리드 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelCertiFileAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiFileAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EQUEquipCertiP")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    public class EQUEquipCertiP : ServicedComponent
    {
        /// <summary>
        /// 설비인증 의뢰요청 책임자 데이터테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtCertiP = new DataTable();
            try
            {
                dtCertiP.Columns.Add("PlantCode", typeof(String));
                dtCertiP.Columns.Add("DocCode", typeof(String));
                dtCertiP.Columns.Add("StandardChargeID", typeof(String));

                return dtCertiP;
            }
            catch(Exception ex)
            {
                return dtCertiP;
                throw(ex);
            }
            finally
            {
                dtCertiP.Dispose();
            }
        }
        /// <summary>
        /// 설비인증 의뢰요청 책임자 검색
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCertiP(String strPlantCode, String strDocCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCertiP = new DataTable();

            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtCertiP = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiP", dtParam);
                
                return dtCertiP;
            }
            catch(Exception ex)
            {
                return dtCertiP;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCertiP.Dispose();
            }
        }

        /// <summary>
        /// 설비인증의뢰요청 표준담당자 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDocCode"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCertiP(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, String strDocCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStandardChargeID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StandardChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipCertiP", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }

        /// <summary>
        /// 표준담당자 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
         

        [AutoComplete(false)]
        public string mfDelCertiP(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiPAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }


        [AutoComplete(false)]
        public string mfDelCertiPAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiPAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }
    }

    #region 설비폐기등록

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipDiscardFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipDiscardFile : ServicedComponent
    {

        /// <summary>
        /// 설비폐기등록 헤더조회
        /// </summary>
        /// <param name="strPlantcode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비폐기정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipDiscardH(string strPlantcode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquip = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantcode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //설비폐기정보 헤더조회 프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipDiscardH", dtParam);

                //설비폐기정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquip.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비폐기등록 조회 (설비를 선택할시 정보가 있으면 자동으로 조회)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <returns>설비폐기등록정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipDiscardFile(string strPlantCode, string strEquipCode)
        {
            DataTable dtEquipDiscardFile = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                //프로시저실행

                dtEquipDiscardFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipDiscardFile", dtParam);

                //설비폐기정보 리턴
                return dtEquipDiscardFile;
            }
            catch (Exception ex)
            {
                return dtEquipDiscardFile;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipDiscardFile.Dispose();
            }
        }


        /// <summary>
        /// 설비폐기등록
        /// </summary>
        /// <param name="dtEquipDiscard"></param>
        /// <param name="dtEquipDiscardFileDel"></param>
        /// <param name="dtEquipDiscardFile"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipDiscardFile(DataTable dtEquipDiscard ,DataTable dtEquipDiscardFile,string strUserIP,string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();
                // 트랜젝션 시작  //
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDiscard.Rows[0]["PlantCode"].ToString(), 10);     //공장
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDiscard.Rows[0]["EquipCode"].ToString(), 20);     //설비
                sql.mfAddParamDataRow(dtParam, "@i_strDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDiscard.Rows[0]["DiscardDate"].ToString(), 10); //폐기일
                sql.mfAddParamDataRow(dtParam, "@i_strDiscardChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDiscard.Rows[0]["DiscardChargeID"].ToString(), 20); //폐기담당자
                sql.mfAddParamDataRow(dtParam, "@i_strDiscardReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipDiscard.Rows[0]["DiscardReason"].ToString(), 1000);  //폐기사유
                sql.mfAddParamDataRow(dtParam, "@i_intPurchasePrice", ParameterDirection.Input, SqlDbType.Float, dtEquipDiscard.Rows[0]["PurchasePrice"].ToString());           //설비도입가
                sql.mfAddParamDataRow(dtParam, "@i_intRemainPrice", ParameterDirection.Input, SqlDbType.Float, dtEquipDiscard.Rows[0]["RemainPrice"].ToString());           //설비잔존가

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15); 
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipDiscardH", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //-- 처리결과에 따라서 롤백 하거나 커밋을한다 --//
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    //--- 처리성공일 경우 첨부 파일이 있는경우 저장한다. ---//



                    string strPlantCode = dtEquipDiscard.Rows[0]["PlantCode"].ToString();
                    string strEquipCode = dtEquipDiscard.Rows[0]["EquipCode"].ToString();

                    // 첨부파일 삭제 //

                    strErrRtn = mfDeleteEquipDiscardFile(strPlantCode, strEquipCode, sql.SqlCon, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                    if (dtEquipDiscardFile.Rows.Count > 0)
                    {

                        strErrRtn = mfSaveEquipDiscardFile(strPlantCode, strEquipCode, dtEquipDiscardFile, strUserIP, strUserID, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                    trans.Commit();
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 첨부파일 저장
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="dtEquipDiscardFile">첨부파일정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveEquipDiscardFile(string strPlantCode ,string strEquipCode, DataTable dtEquipDiscardFile, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i < dtEquipDiscardFile.Rows.Count; i++)
                {
                    //파라미터값저장
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode,10);
                    sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode,20);
                    sql.mfAddParamDataRow(dtParam,"@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipDiscardFile.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strSubject", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipDiscardFile.Rows[i]["Subject"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileNmae", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipDiscardFile.Rows[i]["FileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipDiscardFile.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUEquipDiscardFile", dtParam);

                    // 처리결과에 따라서 진행하거나 break한다. //
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    { break; }

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비폐기등록삭제(설비정보폐기등록취소,설비폐기테이블에있는 정보삭제)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteEquipDiscardFile(string strPlantCode,string strEquipCode,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect();

                //트랜젝션 시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode , 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode , 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP , 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID , 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                
                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipDiscardFile", dtParam);

                //처리결과에 따라서 롤백하거나 커밋을 한다. //
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 첨부파일 삭제
        /// </summary>
        /// <param name="dtEquipDiscardFileDel">첨부파일정보</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipDiscardFile(string strPlantCode ,string strEquipCode,SqlConnection sqlcon, SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
            
                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUEquipDiscardFileD", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                // 처리 실패시 멈추고 리턴시킨다 //
                if (ErrRtn.ErrNum != 0)
                { return strErrRtn; }
                

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public DataTable mfSaveEquipDiscardGRWApproval(DataTable dtHeader, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        {
            DataTable dtRtn = new DataTable();
            QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
            DataTable dtFileInfo = grw.mfSetFileInfoDataTable();

            try
            {
                string strPlantCode = dtHeader.Rows[0]["PlantCode"].ToString();
                string strEquipCode = dtHeader.Rows[0]["EquipCode"].ToString();
                string strLegacyKey = strPlantCode + "||" + strEquipCode;

                string strStationName = dtHeader.Rows[0]["StationName"].ToString();
                string strGRDate = dtHeader.Rows[0]["GRDate"].ToString();
                string strSerialNo = dtHeader.Rows[0]["SerialNo"].ToString();
                string strPurchasePrice = dtHeader.Rows[0]["PurchasePrice"].ToString();
                string strRemainPrice = dtHeader.Rows[0]["RemainPrice"].ToString();
                string strReason = dtHeader.Rows[0]["DiscardReason"].ToString();

                string strFilePath = "EQUEquipDiscardFile\\";

                DataTable dtfile = mfReadEquipDiscardFile_File(strPlantCode, strEquipCode);

                foreach (DataRow dr in dtfile.Rows)
                {
                    DataRow _dr = dtFileInfo.NewRow();
                    _dr["PlantCode"] = strPlantCode;
                    _dr["NM_FILE"] = dr["FileName"].ToString();
                    _dr["NM_LOCATION"] = strFilePath;

                    dtFileInfo.Rows.Add(_dr);
                }

                dtRtn = grw.EquipDiscard(strLegacyKey, dtSendLine, dtCcLine, strStationName, strGRDate, strEquipCode, strSerialNo,
                    strPurchasePrice, strRemainPrice, strReason, dtFormInfo, dtFileInfo, strUserIP, strUserID);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }

            return dtRtn;
        }

        [AutoComplete]
        public DataTable mfReadEquipDiscardFile_File(string strPlantCode, string strEquipCode)
        {
            DataTable dtEquipDiscardFile = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 10);
                //프로시저실행

                dtEquipDiscardFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipDiscardFile_File", dtParam);

                //설비폐기정보 리턴
                return dtEquipDiscardFile;
            }
            catch (Exception ex)
            {
                return dtEquipDiscardFile;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipDiscardFile.Dispose();
            }
        }
    }

    #endregion

    #region 금형치공구페기등록

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableMatDiscardFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DurableMatDiscardFile : ServicedComponent
    {
        /// <summary>
        /// 금형치공구정보 폐기등록컬럼셋
        /// </summary>
        /// <returns>금형치공구정보에등록할 컬럼정보</returns>
        public DataTable mfDataSetInfoH()
        {
            DataTable dtDataInfo = new DataTable();
            try
            {
                dtDataInfo.Columns.Add("PlantCode", typeof(string));
                dtDataInfo.Columns.Add("DurableMatCode", typeof(string));
                dtDataInfo.Columns.Add("DiscardDate", typeof(string));
                dtDataInfo.Columns.Add("DiscardChargeID", typeof(string));
                dtDataInfo.Columns.Add("DiscardReason", typeof(string));


                return dtDataInfo;
            }
            catch (Exception ex)
            {
                return dtDataInfo;
                throw (ex);
            }
            finally
            {
                dtDataInfo.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구페기등록 첨부파일컬럼셋
        /// </summary>
        /// <returns>첨부파일에필요한컬럼정보</returns>
        public DataTable mfDatasetInfoD()
        {
            DataTable dtDataInfoD = new DataTable();
            try
            {
                dtDataInfoD.Columns.Add("Seq", typeof(string));
                dtDataInfoD.Columns.Add("Subject", typeof(string));
                dtDataInfoD.Columns.Add("FileName", typeof(string));
                dtDataInfoD.Columns.Add("EtcDesc", typeof(string));


                return dtDataInfoD;
            }
            catch (Exception ex)
            {
                return dtDataInfoD;
                throw (ex);
            }
            finally
            {
                dtDataInfoD.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구폐기 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <returns>금형치공구폐기정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatDiscardFile(string strPlantCode, string strDurableMatCode)
        {
            DataTable dtDurableMatDiscardFile = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();
                //파라미터 값저장

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);

                // 금형치공구폐기 조회 프로시저 실행
                dtDurableMatDiscardFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableMatDiscardFile", dtParam);

                // 금형치공구폐기정보 리턴
                return dtDurableMatDiscardFile;
            }
            catch (Exception ex)
            {
                return dtDurableMatDiscardFile;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatDiscardFile.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구폐기등록
        /// </summary>
        /// <param name="dtDurableMatDiscard">금형치공구정보 폐기등록정보</param>
        /// <param name="dtDurableDiscardFile">금형치공구 첨부파일정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과리턴</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatDiscardFile(DataTable dtDurableMatDiscard, DataTable dtDurableDiscardFile, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();

                /// 파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatDiscard.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatDiscard.Rows[0]["DurableMatCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatDiscard.Rows[0]["DiscardDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDiscardChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatDiscard.Rows[0]["DiscardChargeID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strDiscardReason", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMatDiscard.Rows[0]["DiscardReason"].ToString(), 1000);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Input, SqlDbType.VarChar, 8000);


                //금형치공구 (금형치공구정보 폐기등록) 프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableMatDiscardFileH", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 프로시저 실행 결과에 따라서 롤백처리하거나 커밋한다. //
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {

                    //공장, 금형치공구코드를 저장한다 //
                    string strPlantCode = dtDurableMatDiscard.Rows[0]["PlantCode"].ToString();
                    string strDurableMatCode = dtDurableMatDiscard.Rows[0]["DurableMatCode"].ToString();

                    // 첨부파일 저장전 삭제를한다. //
                    strErrRtn = mfDeleteDurableMatDiscardFile(strPlantCode, strDurableMatCode, sql.SqlCon, trans);

                    // -- Decoding --//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    // 처리 실패시 롤백처리한다 //
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                    // 치리결과가 성공일시 첨부파일이 있을경우 저장한다 //
                    if (dtDurableDiscardFile.Rows.Count > 0)
                    {
                        //첨부파일을 저장한다 //
                        strErrRtn = mfSaveDurableMatDiscardFile(strPlantCode, strDurableMatCode, dtDurableDiscardFile, strUserIP, strUserID, sql.SqlCon, trans);

                        // -- Decoding -- //
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //-- 처리결과 실패시 롤백처리 후 에러를 리턴한다 ..
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                    //-- 모든 처리가 성공시 커밋시킨다 --//
                    trans.Commit();

                }
                //처리결과 리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구폐기 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과리턴</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableMatDiscardFile(string strPlantCode, string strDurableMatCode, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                // --  파라미터 값 저장 --..
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 금형치공구폐기 삭제 프로시져 실행 //
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_DMMDurableMatDiscardFileH", dtParam);

                // Decoding //
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 처리결과가 실패면 롤백처리, 성공시 커밋처리를 한다 //
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 첨부파일 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableMatDiscardFile(string strPlantCode, string strDurableMatCode, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                // -- 파라미터값저장 -- //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // -- 금형치공구첨부파일 삭제 프로시저 실행 //
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_DMMDurableMatDiscardFile", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 첨부파일 등록
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="dtDurableMatDiscardFile">첨부파일정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatDiscardFile(string strPlantCode, string strDurableMatCode, DataTable dtDurableMatDiscardFile, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                for (int i = 0; i < dtDurableMatDiscardFile.Rows.Count; i++)
                {
                    // 파라미터 값저장 //
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableMatDiscardFile.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strSubject", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMatDiscardFile.Rows[i]["Subject"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMatDiscardFile.Rows[i]["FileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMatDiscardFile.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strCreateIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strCreateID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // -- 금형치공구 첨부파일 등록 프로시저 실행 --//
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_DMMDurableMatDiscardFile", dtParam);

                    //-- Decoding -- //
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리실패시 for문을 멈추고 리턴시킨다 //
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                // 처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }

    #endregion


    #region 품종교체점검등록

        #region 품종교체점검등록 헤더
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DeviceChgResultH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DeviceChgResultH : ServicedComponent
    {
        /// <summary>
        /// 품종교체교체실적 헤더컬럼셋
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtDeviceChgResult = new DataTable();
            try
            {
                dtDeviceChgResult.Columns.Add("PlantCode", typeof(string));
                dtDeviceChgResult.Columns.Add("DeviceChgDocCode", typeof(string));
                dtDeviceChgResult.Columns.Add("EquipCode", typeof(string));
                dtDeviceChgResult.Columns.Add("ProcessCode", typeof(string));
                dtDeviceChgResult.Columns.Add("ChgCheckDate", typeof(string));
                dtDeviceChgResult.Columns.Add("WorkGubunCode", typeof(string));
                dtDeviceChgResult.Columns.Add("ChgCheckID", typeof(string));
                dtDeviceChgResult.Columns.Add("StdNumber", typeof(string));
                dtDeviceChgResult.Columns.Add("VersionNum", typeof(string));
                dtDeviceChgResult.Columns.Add("CCSReqFlag", typeof(string));
                dtDeviceChgResult.Columns.Add("EtcDesc", typeof(string));

                return dtDeviceChgResult;
            }
            catch (Exception ex)
            {
                return dtDeviceChgResult;
                throw (ex);
            }
            finally
            {
                dtDeviceChgResult.Dispose();
            }
        }

        /// <summary>
        /// 설비품종교체점검실적헤더
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비품종교체점검실적헤더 정보</returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgResultH(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtDeviceChgResultH = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 실행
                dtDeviceChgResultH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDeviceChgResultH", dtParam);

                //정보리턴
                return dtDeviceChgResultH;
            }
            catch (Exception ex)
            {
                return dtDeviceChgResultH;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDeviceChgResultH.Dispose();
            }

        }


        /// <summary>
        /// 설비품종교체점검실적헤더 저장
        /// </summary>
        /// <param name="dtDeviceChgResultH">저장할 헤더정보</param>
        /// <param name="dtDeviceChgResultD">저장할 상세정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveDeviceChgResultH(DataTable dtDeviceChgResultH, DataTable dtDeviceChgResultD, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect();
                //  트랜젝션 시작 //
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                // 파라미터값저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,100);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode" , ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultH.Rows[0]["PlantCode"].ToString(),10);
                sql.mfAddParamDataRow(dtParam,"@i_strDeviceChgDocCode" , ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultH.Rows[0]["DeviceChgDocCode"].ToString(),20);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode" , ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultH.Rows[0]["EquipCode"].ToString(),20);
                sql.mfAddParamDataRow(dtParam,"@i_strProcessCode" , ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultH.Rows[0]["ProcessCode"].ToString(),10);
                sql.mfAddParamDataRow(dtParam,"@i_StrChgCheckDate" , ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultH.Rows[0]["ChgCheckDate"].ToString(),10);
                sql.mfAddParamDataRow(dtParam,"@i_strWorkGubunCode" , ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultH.Rows[0]["WorkGubunCode"].ToString(),1);
                sql.mfAddParamDataRow(dtParam,"@i_strChgCheckID" , ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultH.Rows[0]["ChgCheckID"].ToString(),20);
                sql.mfAddParamDataRow(dtParam,"@i_strStdNumber" , ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultH.Rows[0]["StdNumber"].ToString(),10);
                sql.mfAddParamDataRow(dtParam,"@i_intVersionNum" , ParameterDirection.Input, SqlDbType.Int,dtDeviceChgResultH.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParam,"@i_bolCCSReqFlag" , ParameterDirection.Input, SqlDbType.Bit,dtDeviceChgResultH.Rows[0]["CCSReqFlag"].ToString());
                sql.mfAddParamDataRow(dtParam,"@i_strEtcDesc" , ParameterDirection.Input, SqlDbType.NVarChar,dtDeviceChgResultH.Rows[0]["EtcDesc"].ToString(),1000);
                sql.mfAddParamDataRow(dtParam,"@i_strUserIP" , ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                sql.mfAddParamDataRow(dtParam,"@i_strUserID" , ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                sql.mfAddParamDataRow(dtParam,"@o_strDeviceChgDocCode" , ParameterDirection.Output, SqlDbType.VarChar,20);
                sql.mfAddParamDataRow(dtParam,"@ErrorMessage" , ParameterDirection.Output, SqlDbType.VarChar,8000);
                    
                //  프로시저 실행 //    

                strErrRtn =   sql.mfExecTransStoredProc(sql.SqlCon, trans,"up_Update_EQUDeviceChgResultH", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    //  처리실패시 롤백처리를 한다.  //
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    // 처리 성공시 상세정보를 저장한다 //
                    if (dtDeviceChgResultD.Rows.Count > 0)
                    {
                        string strDeviceChgDocCode = ErrRtn.mfGetReturnValue(0);
                        DeviceChgResultD clsDeviceChgResultD = new DeviceChgResultD();
                        strErrRtn = clsDeviceChgResultD.mfSaveDeviceChgResultD(dtDeviceChgResultD, strDeviceChgDocCode, strUserIP, strUserID, sql.SqlCon, trans);
                        // Decoding //
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 처리결과가 실패시 롤백후 처리결과 리턴 //
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    // 모든처리가 성공시 커밋처리를 한다 //
                    trans.Commit();
                }

                // 처리결과 리턴 //
                return strErrRtn;
                     
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비품종교체점검등록 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDeviceChgDocCode">품종교체문서코드</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDeviceChgResult(string strPlantCode, string strDeviceChgDocCode)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                // 디비연결
                sql.mfConnect();
                //트랜젝션 시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                //  파라미터값 저장  //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDeviceChgDocCode", ParameterDirection.Input, SqlDbType.VarChar,strDeviceChgDocCode,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //프로시저실행 //
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUDeviceChgResult", dtParam);

                //Decoding //
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // -- 처리결과에 따라 롤백처리 , 커밋처리를 한다 //
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }

                // 처리결과 값을 리턴한다.
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    

    }

        #endregion

        #region 품종교체점검등록 상세
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DeviceChgResultD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DeviceChgResultD : ServicedComponent
    {
        
        /// <summary>
        /// 설비품종교체점검실적 상세 컬럼셋
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtDeviceChgResultD = new DataTable();
            try
            {
                dtDeviceChgResultD.Columns.Add("PlantCode", typeof(string));
                dtDeviceChgResultD.Columns.Add("Seq", typeof(string));
                dtDeviceChgResultD.Columns.Add("PMInspectGubun", typeof(string));
                dtDeviceChgResultD.Columns.Add("PMInspectName", typeof(string));
                dtDeviceChgResultD.Columns.Add("PMInspectCriteria", typeof(string));
                dtDeviceChgResultD.Columns.Add("LevelCode", typeof(string));
                dtDeviceChgResultD.Columns.Add("DeviceChgPointCode", typeof(string));
                dtDeviceChgResultD.Columns.Add("PMStatus", typeof(string));
                dtDeviceChgResultD.Columns.Add("ActionDesc", typeof(string));
                dtDeviceChgResultD.Columns.Add("FinalStatus", typeof(string));
                


                return dtDeviceChgResultD;
            }
            catch (Exception ex)
            {
                return dtDeviceChgResultD;
                throw (ex);
            }
            finally
            {
                dtDeviceChgResultD.Dispose();
            }

        }

        
        /// <summary>
        /// 품종교체점검등록상세 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strEquipTypeCode">설비유형코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>품종교체점검등록상세 정보</returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgResultD(string strPlantCode,string strEquipCode,string strEquipTypeCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDeviceChgResultD = new DataTable();
            try
            {
                // 디비 연결 //
                sql.mfConnect();

                // 파라미터 값 저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipTypeCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                //프로시저 실행
                dtDeviceChgResultD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDeviceChgResultD", dtParam);
  

                //설비품종교체점검등록 상세 정보
                return dtDeviceChgResultD;
            }
            catch (Exception ex)
            {
                return dtDeviceChgResultD;
                throw (ex);
            }
            finally
            {
                // 디비 종료 /./
                sql.mfDisConnect();
                sql.Dispose();
                dtDeviceChgResultD.Dispose();
            }
        }


        /// <summary>
        /// 설비품종교체정보상세 저장
        /// </summary>
        /// <param name="dtDeviceChgResultD">저장할 설비점검 상세</param>
        /// <param name="strDeviceChgDocCode">품종교체문서코드</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveDeviceChgResultD(DataTable dtDeviceChgResultD, string strDeviceChgDocCode, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDeviceChgResultD.Rows.Count; i++)
                {
                    // 파라미터값저장 ////
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultD.Rows[i]["PlantCode"].ToString(),10);
                    sql.mfAddParamDataRow(dtParam,"@i_strDeviceChgDocCode", ParameterDirection.Input, SqlDbType.VarChar,strDeviceChgDocCode,20);
                    sql.mfAddParamDataRow(dtParam,"@i_intSeq", ParameterDirection.Input, SqlDbType.Int,dtDeviceChgResultD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam,"@i_strPMInspectGubun", ParameterDirection.Input, SqlDbType.NVarChar,dtDeviceChgResultD.Rows[i]["PMInspectGubun"].ToString(),100);
                    sql.mfAddParamDataRow(dtParam,"@i_strPMInspectName", ParameterDirection.Input, SqlDbType.NVarChar,dtDeviceChgResultD.Rows[i]["PMInspectName"].ToString(),100);
                    sql.mfAddParamDataRow(dtParam,"@i_strPMInspectCriteria", ParameterDirection.Input, SqlDbType.NVarChar,dtDeviceChgResultD.Rows[i]["PMInspectCriteria"].ToString(),100);
                    sql.mfAddParamDataRow(dtParam,"@i_strLevelCode", ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultD.Rows[i]["LevelCode"].ToString(),1);
                    sql.mfAddParamDataRow(dtParam,"@i_strDeviceChgPointCode", ParameterDirection.Input, SqlDbType.VarChar,dtDeviceChgResultD.Rows[i]["DeviceChgPointCode"].ToString(),5);
                    sql.mfAddParamDataRow(dtParam,"@i_strPMStatus", ParameterDirection.Input, SqlDbType.NVarChar,dtDeviceChgResultD.Rows[i]["PMStatus"].ToString(),100);
                    sql.mfAddParamDataRow(dtParam,"@i_strActionDesc", ParameterDirection.Input, SqlDbType.NVarChar,dtDeviceChgResultD.Rows[i]["ActionDesc"].ToString(),1000);
                    sql.mfAddParamDataRow(dtParam,"@i_strFinalStatus", ParameterDirection.Input, SqlDbType.NVarChar,dtDeviceChgResultD.Rows[i]["FinalStatus"].ToString(),100);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUDeviceChgResultD", dtParam);

                    // Decoding //
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리 결과 실패시 for문을 빠져나간다 //
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                    
                }

                // 처리결과 리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }
    }

        #endregion

    #endregion

    #region 설비변경등록

        #region 설비변경등록 헤더
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipCCSH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipCCSH : ServicedComponent
    {

        /// <summary>
        /// 설비변경등록 컬럼셋
        /// </summary>
        /// <returns>설비변경등록 컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtEquipCCSH = new DataTable();
            try
            {
                dtEquipCCSH.Columns.Add("PlantCode", typeof(string));
                dtEquipCCSH.Columns.Add("CCSCode", typeof(string));
                dtEquipCCSH.Columns.Add("EquipCode", typeof(string));
                dtEquipCCSH.Columns.Add("CCSDate", typeof(string));
                dtEquipCCSH.Columns.Add("ProcessCode", typeof(string));
                dtEquipCCSH.Columns.Add("EquipCCSTypeCode", typeof(string));
                dtEquipCCSH.Columns.Add("CCSFlag", typeof(string));
                dtEquipCCSH.Columns.Add("SetupDocNum", typeof(string));
                dtEquipCCSH.Columns.Add("WorkGubunCode", typeof(string));
                dtEquipCCSH.Columns.Add("CCSFinishDate", typeof(string));
                dtEquipCCSH.Columns.Add("CCSChargeID", typeof(string));
                dtEquipCCSH.Columns.Add("InspectStandard", typeof(string));
                dtEquipCCSH.Columns.Add("InspectResult", typeof(string));
                dtEquipCCSH.Columns.Add("InspectQty", typeof(string));
                dtEquipCCSH.Columns.Add("Object", typeof(string));
                dtEquipCCSH.Columns.Add("EtcDesc", typeof(string));
                dtEquipCCSH.Columns.Add("EquipCertiDocCode", typeof(string));



                return dtEquipCCSH;
            }
            catch (Exception ex)
            {
                return dtEquipCCSH;
                throw (ex);
            }
            finally
            {
                dtEquipCCSH.Dispose();
            }
        }

        /// <summary>
        /// 설비변경등록 헤더 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strFromDate">변경일(시작일)</param>
        /// <param name="strToDate">변경일(종료일)</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비변경등록헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipCCSH(string strPlantCode,string strEquipCode,string strFromDate,string strToDate,string strLang)
        {
            DataTable dtEquipCCSH = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // 디비오픈
                sql.mfConnect();

                // 파라미터갑 저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar,strFromDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar,strToDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                // 설비변경등록 헤더 조회 프로시저 실행 //
                dtEquipCCSH = sql.mfExecReadStoredProc(sql.SqlCon,"up_Select_EQUEquipCCSH", dtParam);


                // 설비변경등록 헤더 정보 리턴//
                return dtEquipCCSH;
            }
            catch (Exception ex)
            {
                return dtEquipCCSH;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipCCSH.Dispose();
            }
        }


        /// <summary>
        /// 설비변경등록 헤더 저장
        /// </summary>
        /// <param name="dtEquipCCSH">설비변경등록헤더저장정보</param>
        /// <param name="dtEquipCCSD">설비변경등록상세저장정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과 값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipCCSH(DataTable dtEquipCCSH,DataTable dtEquipCCSD, string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                // 디비연결 //
                sql.mfConnect();

                // 트랜젝션 시작 //
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();

                // 파라미터정보  저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["PlantCode"].ToString(),10);              //공장
                sql.mfAddParamDataRow(dtParam, "@i_strCCSCode", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["CCSCode"].ToString(),20);                  //변경점코드
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["EquipCode"].ToString(),20);              //설비코드
                sql.mfAddParamDataRow(dtParam, "@i_strCCSDate", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["CCSDate"].ToString(),10);                  //변경일
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["ProcessCode"].ToString(),10);          //공정코드
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCCSTypeCode", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["EquipCCSTypeCode"].ToString(),5); //설비변경점유형코드
                sql.mfAddParamDataRow(dtParam, "@i_bolCCSFlag", ParameterDirection.Input, SqlDbType.Bit,dtEquipCCSH.Rows[0]["CCSFlag"].ToString());                         //CCS대상여부
                sql.mfAddParamDataRow(dtParam, "@i_strSetupDocNum", ParameterDirection.Input, SqlDbType.NVarChar,dtEquipCCSH.Rows[0]["SetupDocNum"].ToString(),100);        //Setup문서번호
                sql.mfAddParamDataRow(dtParam, "@i_strWorkGubunCode", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["WorkGubunCode"].ToString(),1);       //작업조코드
                sql.mfAddParamDataRow(dtParam, "@i_strCCSFinishDate", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["CCSFinishDate"].ToString(),10);      //변경완료일
                sql.mfAddParamDataRow(dtParam, "@i_strCCSChargeID", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["CCSChargeID"].ToString(),20);          //변경담당자ID
                sql.mfAddParamDataRow(dtParam, "@i_strInspectStandard", ParameterDirection.Input, SqlDbType.NVarChar,dtEquipCCSH.Rows[0]["InspectStandard"].ToString(),1000);//검사기준
                sql.mfAddParamDataRow(dtParam, "@i_strInspectResult", ParameterDirection.Input, SqlDbType.NVarChar,dtEquipCCSH.Rows[0]["InspectResult"].ToString(),1000);   //검사결과
                sql.mfAddParamDataRow(dtParam, "@i_intInspectQty", ParameterDirection.Input, SqlDbType.Decimal,dtEquipCCSH.Rows[0]["InspectQty"].ToString());               //검사수량
                sql.mfAddParamDataRow(dtParam, "@i_strObject", ParameterDirection.Input, SqlDbType.NVarChar,dtEquipCCSH.Rows[0]["Object"].ToString(),1000);                 //목적
                sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar,dtEquipCCSH.Rows[0]["EtcDesc"].ToString(),1000);               //특이사항
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCertiDocCode", ParameterDirection.Input, SqlDbType.VarChar,dtEquipCCSH.Rows[0]["EquipCertiDocCode"].ToString(),20);//설비인증문서코드
                
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                sql.mfAddParamDataRow(dtParam, "@o_strCCSCode", ParameterDirection.Output, SqlDbType.VarChar,20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);


                // 설비변경등록 헤더 저장 프로시저 실행 
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon,trans,"up_Update_EQUEquipCCSH",dtParam);

                // Decoding //
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 처리실패시 트랜 롤백을 한다 .
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                // 처리 성공시 상세를 삭제,저장 후 모든처리가 성공시 트랜 커밋을 한다. 
                else
                {
                    // -- 공장 코드 , OUPUT하여 받은 설비변경점코드 저장 
                    string strPlant = dtEquipCCSH.Rows[0]["PlantCode"].ToString();
                    string strCCSCode = ErrRtn.mfGetReturnValue(0);

                    EquipCCSD clsCCSD = new EquipCCSD();

                    //-- 설비변경등록 상세 삭제 매서드를 실행한다 
                    strErrRtn = clsCCSD.mfDeleteEquipCCSD(strPlant, strCCSCode, sql.SqlCon, trans);

                     //설비변경등록 상세 리턴값 DeCoding  
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    
                    // 삭제 처리결과가 실패시 트랜롤백한 후 에러메세지를 리턴한다 ..
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                    // 삭제가 성공시 상세를 저장한다 . 
                    if (dtEquipCCSD.Rows.Count > 0)
                    {
                        // 설비변경등록 상세 저장 매서드 호출 
                        strErrRtn = clsCCSD.mfSaveEquipCCSD(strPlant,strCCSCode, dtEquipCCSD, strUserIP, strUserID, sql.SqlCon, trans);

                        //  Decoding  //
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //-- 상세 저장 실패시 트랜롤백 처리 후 에러메세지를 리턴한다 .
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    // 모든 처리가 성공시 트랜 커밋을 한다.
                    trans.Commit();
                }

                //처리결과 정보 리턴 
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료 //
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비변경등록 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCCSCode">변경점코드</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipCCSH(string strPlantCode,string strCCSCode)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결 
                sql.mfConnect();

                //트랜젝션 시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                // 파라미터값 저장 
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strCCSCode", ParameterDirection.Input, SqlDbType.VarChar,strCCSCode,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);
                
                //설비변경등록 삭제 프로시저 실행 
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCCSH", dtParam);

                // Decoding //
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 처리 실패시 트랜롤백처리를 한다.
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                // 처리 성공시 트랜커밋을 한다.
                else
                {
                    trans.Commit();
                }

                //처리 결과 정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                // 디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


    }

        #endregion

        #region 설비변경등록 상세
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipCCSH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipCCSD : ServicedComponent
    {
        /// <summary>
        /// 설비변경등록 상세 컬럼셋
        /// </summary>
        /// <returns>설비변경등록 상세 컬럼정보</returns>
        public DataTable mfSetDateaInfo()
        {
            DataTable dtEquipCCSD = new DataTable();

            try
            {
                dtEquipCCSD.Columns.Add("Seq", typeof(string));
                dtEquipCCSD.Columns.Add("CCSDate", typeof(string));
                dtEquipCCSD.Columns.Add("PreCCS", typeof(string));
                dtEquipCCSD.Columns.Add("PostCCS", typeof(string));

                //설비변경등록 상세 컬럼정보
                return dtEquipCCSD;
            }
            catch (Exception ex)
            {
                return dtEquipCCSD;
                throw (ex);
            }
            finally
            {
                dtEquipCCSD.Dispose();
            }
        }


        /// <summary>
        /// 설비변경등록 상세 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCCSCode">변경점코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비변경등록 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipCCSD(string strPlantCode,string strCCSCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipCCSD = new DataTable();

            try
            {
                // 디비 연결 //
                sql.mfConnect();

                // 파라미터값 저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strCCSCode", ParameterDirection.Input, SqlDbType.VarChar,strCCSCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                // 설비변경등록 상세 정보 조회 프로시저 실행 ..
                dtEquipCCSD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCCSD", dtParam);

                //설비변경등록 상세 정보 리턴
                return dtEquipCCSD;

            }
            catch (Exception ex)
            {
                return dtEquipCCSD;
                throw (ex);
            }
            finally
            {
                // 디비종료 //
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipCCSD.Dispose();
            }
        }


        /// <summary>
        /// 설비변경등록 상세 저장
        /// </summary>
        /// <param name="dtEquipCCSD">설비변경등록 상세 저장정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리 결과 정보</returns>
        [AutoComplete(false)]
        public string mfSaveEquipCCSD(string strPlantCode,string strCCSCode,  DataTable dtEquipCCSD, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i < dtEquipCCSD.Rows.Count; i++)
                {
                    // 파라미터 정보 저장
                    DataTable dtPram = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtPram, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtPram, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);                                //공장
                    sql.mfAddParamDataRow(dtPram, "@i_strCCSCode", ParameterDirection.Input, SqlDbType.VarChar, strCCSCode, 20);                                    //변경점코드
                    sql.mfAddParamDataRow(dtPram, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipCCSD.Rows[i]["Seq"].ToString());                     //순번
                    sql.mfAddParamDataRow(dtPram, "@i_strCCSDate", ParameterDirection.Input, SqlDbType.VarChar, dtEquipCCSD.Rows[i]["CCSDate"].ToString(), 10);     //변경일
                    sql.mfAddParamDataRow(dtPram, "@i_strPreCCS", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipCCSD.Rows[i]["PreCCS"].ToString(), 1000);    //변경전
                    sql.mfAddParamDataRow(dtPram, "@i_strPostCCS", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipCCSD.Rows[i]["PostCCS"].ToString(), 1000);  //변경후
                    sql.mfAddParamDataRow(dtPram, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtPram, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtPram, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    // 설비변경등록 상세저장 프로시저 실행 
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUEquipCCSD", dtPram);

                    // Decoding //
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리 실패시 for문을 멈춘다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                  
                }

                // 처리 정보 리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비변경등록 상세 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCCSCode">변경점코드</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipCCSD(string strPlantCode, string strCCSCode, SqlConnection sqlcon, SqlTransaction trans)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                // 파라미터 정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strCCSCode", ParameterDirection.Input, SqlDbType.VarChar,strCCSCode,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                // 설비변경점정보상세삭제 프로시저 실행 
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUEquipCCSD", dtParam);

                // Decoding //
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 처리 결과 실패시 정보를 리턴한다.
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);

            }
            finally
            {
                sql.Dispose();
            }
        }

    }
        #endregion

    #endregion


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EQUEquipCertiItemQC")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    public class EQUEquipCertiItemQC : ServicedComponent
    {
        /// <summary>
        /// 설비인증항목 데이터테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtCertiItem = new DataTable();

            try
            {
                dtCertiItem.Columns.Add("PlantCode", typeof(String));
                dtCertiItem.Columns.Add("DocCode", typeof(String));
                dtCertiItem.Columns.Add("Seq", typeof(int));
                dtCertiItem.Columns.Add("CertiItem", typeof(String));
                dtCertiItem.Columns.Add("SampleSize", typeof(String));
                dtCertiItem.Columns.Add("JudgeStandard", typeof(String));
                dtCertiItem.Columns.Add("EvalResultCode", typeof(String));
                dtCertiItem.Columns.Add("JudgeResultCode", typeof(String));
                dtCertiItem.Columns.Add("UpperSpec", typeof(string));
                dtCertiItem.Columns.Add("LowerSpec", typeof(string));

                return dtCertiItem;
            }
            catch (Exception ex)
            {
                return dtCertiItem;
                throw (ex);
            }
            finally
            {
                dtCertiItem.Dispose();
            }
        }
        /// <summary>
        /// 설비인증항목 평가결과 검색
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        
        [AutoComplete]
        public DataTable mfReadCertiItemQC(String strPlantCode, String strDocCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCertiItem = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dtCertiItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipCertiItemQC", dtParam);

                return dtCertiItem;
            }
            catch (System.Exception ex)
            {
                return dtCertiItem;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCertiItem.Dispose();
            }
        }

        /// <summary>
        /// 설비인증항목 및 평가결과 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDocCode"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCertiItemQC(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, string strDocCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, strDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiItem", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["CertiItem"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strSampleSize", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["SampleSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strJudgeStandard", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["JudgeStandard"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEvalResultCode", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EvalResultCode"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strJudgeResultCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["JudgeResultCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUpperSpec", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["UpperSpec"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strLowerSpec", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LowerSpec"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipCertiItemQC", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }


        /// <summary>
        /// 설비인증 접수, 평가 저장용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCertiItemQCForAdmin(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCertiItem", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["CertiItem"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strSampleSize", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["SampleSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strJudgeStandard", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["JudgeStandard"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEvalResultCode", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EvalResultCode"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strJudgeResultCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["JudgeResultCode"].ToString(), 2);
                    //sql.mfAddParamDataRow(dtParam, "@i_strUpperSpec", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["UpperSpec"].ToString(), 1000);
                    //sql.mfAddParamDataRow(dtParam, "@i_strLowerSpec", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LowerSpec"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUEquipCertiItemQC", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }


        /// <summary>
        /// 설비인증항목 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelCertiItemQC(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiItemQC", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDelCertiItemQCAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUEquipCertiItemQCAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

    }
}
