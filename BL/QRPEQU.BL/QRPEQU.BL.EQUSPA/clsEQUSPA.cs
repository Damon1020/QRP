﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;
using QRPDB;


[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]


namespace QRPEQU.BL.EQUSPA
{
    /// <summary>
    /// SparePart 재고 관련 저장 클래스

    /// </summary>

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SAVESparePartStock")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SAVESparePartStock : ServicedComponent
    {

        /// <summary>
        /// 1. 기초재고등록저장

        /// </summary>
        /// <param name="dtSPStock">SparePart 현 재고 테이블</param>
        /// <param name="dtSPStockMoveHist">SparePart 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveBasicStock
            (
                DataTable dtSPStock
                , DataTable dtSPStockMoveHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. SparePart 현 재고 테이블 (EQUSPStock) -------------------//
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new SPStock();

                if (dtSPStock.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. SparePart 재고이력 테이블 (EQUSPStockMoveHist) -------------------//
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new SPStockMoveHist();

                if (dtSPStockMoveHist.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    } 
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }




        /// <summary>
        /// 2. 입고정보 저장

        /// </summary>
        /// <param name="dtSPGR">SparePart 입고확인 테이블</param>
        /// <param name="dtSPStock">SparePart 현 재고 테이블</param>
        /// <param name="dtSPStockMoveHist">SparePart 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveSPGR
            (
                DataTable dtSPGR
                ,DataTable dtSPStock
                , DataTable dtSPStockMoveHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 입고확인 테이블 (EQUSPGR) -------------------//
                QRPEQU.BL.EQUSPA.SPGR clsSPGR = new SPGR();

                if (dtSPGR.Rows.Count > 0)
                {
                    strErrRtn = clsSPGR.mfSaveSPGR(dtSPGR, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. SparePart 현 재고 테이블 (EQUSPStock) -------------------//
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new SPStock();

                if (dtSPStock.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 3. SparePart 재고이력 테이블 (EQUSPStockMoveHist) -------------------//
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new SPStockMoveHist();

                if (dtSPStockMoveHist.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 3. 자재출고 저장

        /// </summary>
        /// <param name="dtSPSPTransferH">SparePart 자재출고 헤더 테이블</param>
        /// <param name="dtSPSPTransferD">SparePart 자재출고 아이템 테이블</param>
        /// <param name="dtSPStock">SparePart 현 재고 테이블</param>
        /// <param name="dtSPStockMoveHist">SparePart 재고 이동 이력 테이블</param>
        /// <param name="dtSPChgStandby">SparePart 현 대기 재고 테이블</param>
        /// <param name="strStockFlag">재고 적용여부 플래그</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveTransfer
            (
                DataTable dtSPTransferH
                , DataTable dtSPTransferD
                , DataTable dtSPChgStandby
                , DataTable dtSPStock
                , DataTable dtSPStockMoveHist
                , string strStockFlag
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재출고 헤더 테이블 (EQUSPSPTransferH) -------------------//
                QRPEQU.BL.EQUSPA.SPTransferH clsSPTransferH = new SPTransferH();

                if (dtSPTransferH.Rows.Count > 0)
                {
                    strErrRtn = clsSPTransferH.mfSaveSPTransferH(dtSPTransferH, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    strTransferCode = ErrRtn.mfGetReturnValue(0);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                for (int i = 0; i < dtSPTransferD.Rows.Count; i++)
                {
                    dtSPTransferD.Rows[i]["TransferCode"] = strTransferCode;
                }

                //---------------- 2. 자재출고 아이템 테이블 (EQUSPSPTransferD) -------------------//
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new SPTransferD();

                //--- 삭제 ---//
                strErrRtn = clsSPTransferD.mfDeleteSPTransferD(dtSPTransferD, strTransferCode, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                //--- 저장 ---//
                if (dtSPTransferD.Rows.Count > 0)
                {
                    strErrRtn = clsSPTransferD.mfSaveSPTransferD(dtSPTransferD, strTransferCode, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //------------------- 재고 적용여부가 F 일 경우는 자재출고 헤더/아이템 테이블에만 저장 한다 -----------------///
                if (strStockFlag == "F")
                {
                    if (ErrRtn.ErrNum == 0)
                    {
                        trans.Commit();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }



                //---------------- 3. 대기 재고 테이블 (EQUSPChgStandby) -------------------//
                QRPEQU.BL.EQUSPA.SPChgStandby clsSPChgStandby = new SPChgStandby();

                if (dtSPChgStandby.Rows.Count > 0)
                {
                    strErrRtn = clsSPChgStandby.mfSaveSPChgStandby(dtSPChgStandby, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 4. SparePart 현 재고 테이블 (EQUSPStock) -------------------//
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new SPStock();


                if (dtSPStock.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 5. SparePart 재고이력 테이블 (EQUSPStockMoveHist) -------------------//
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new SPStockMoveHist();

                if (dtSPStockMoveHist.Rows.Count > 0)
                {

                    //for (int i = 0; i < dtSPStockMoveHist.Rows.Count; i++)
                    //{
                    //    dtSPStockMoveHist.Rows[i]["DocCode"] = strTransferCode;
                    //} 

                    for (int i = 0; i < dtSPStockMoveHist.Rows.Count; i++)
                    {
                        dtSPStockMoveHist.Rows[i]["DocCode"] = strTransferCode;
                    }

                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 6. MASEquipSPBOM 테이블의 ChgStandbyQty(교체수량)을 업데이트 -------------------//
                if (dtSPTransferD.Rows.Count > 0)
                {
                    strErrRtn = clsSPTransferD.mfSaveMASEquipSPBOM_ChgStandbyQty(dtSPTransferD, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;

                
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 4. 자재교체 저장

        /// </summary>
        /// <param name="dtSPChgStandbyInput">SparePart 현 대기 재고 테이블 : 설비에 투입될 데이터</param>
        /// <param name="dtSPChgStandbyOut">SparePart 현 대기 재고 테이블 : 설비에서 교체될 데이터</param>
        /// <param name="dtSPTransferD">SparePart BOM 테이블에 갱신할 데이터</param> 
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveSparePartChange
            (
                DataTable dtSPTransferD
                , DataTable dtSPChgStandbyInput
                , DataTable dtSPChgStandbyOut
                , DataTable dtEquipSPBOM
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재출고 아이템 테이블 (EQUSPSPTransferD) -------------------//
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new SPTransferD();

                //--- 저장 ---//
                if (dtSPTransferD.Rows.Count > 0)
                {
                    strErrRtn = clsSPTransferD.mfSaveSPTransferD(dtSPTransferD, "", strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. 대기 재고 테이블 (EQUSPChgStandby) :설비에 투입될 데이터-------------------//
                QRPEQU.BL.EQUSPA.SPChgStandby clsSPChgStandby = new SPChgStandby();

                if (dtSPChgStandbyInput.Rows.Count > 0)
                {
                    strErrRtn = clsSPChgStandby.mfSaveSPChgStandby(dtSPChgStandbyInput, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 3. 대기 재고 테이블 (EQUSPChgStandby) :설비에서 교체될 데이터-------------------//

                if (dtSPChgStandbyOut.Rows.Count > 0)
                {
                    strErrRtn = clsSPChgStandby.mfSaveSPChgStandby(dtSPChgStandbyOut, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }



                //---------------- 4. SparePart BOM 테이블 (MASEquipSTBOM) -------------------//

                if (dtEquipSPBOM.Rows.Count > 0)
                {

                    strErrRtn = clsSPTransferD.mfSaveMASEquipSPBOM_Chg_Transfer(dtEquipSPBOM, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 5. 자재반납 저장

        /// </summary>
        /// <param name="dtSPTransferD">자재반납정보 아이템 테이블</param> 
        /// <param name="dtSPChgStandby">SparePart 현 대기 재고 테이블 </param>
        /// <param name="dtSPEQUSPStock">SparePart 현 재고 테이블 </param>
        /// <param name="dtSPStockMoveHist">SparePart 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveSparePartReturn
            (
                DataTable dtSPTransferD
                , DataTable dtSPChgStandby
                , DataTable dtSPStock
                , DataTable dtSPStockMoveHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재출고 아이템 테이블 (EQUSPSPTransferD) -------------------//
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new SPTransferD();

                //--- 저장 ---//
                if (dtSPTransferD.Rows.Count > 0)
                {
                    strErrRtn = clsSPTransferD.mfSaveSPTransferD(dtSPTransferD, "", strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. 대기 재고 테이블 (EQUSPChgStandby) :설비에 투입될 데이터-------------------//
                QRPEQU.BL.EQUSPA.SPChgStandby clsSPChgStandby = new SPChgStandby();

                if (dtSPChgStandby.Rows.Count > 0)
                {
                    strErrRtn = clsSPChgStandby.mfSaveSPChgStandby(dtSPChgStandby, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 3. SparePart 현 재고 테이블 (EQUSPStock) -------------------//
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new SPStock();

                if (dtSPStock.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 4. SparePart 재고이력 테이블 (EQUSPStockMoveHist) -------------------//
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new SPStockMoveHist();

                if (dtSPStockMoveHist.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }




                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 6. 자재이동 저장

        /// </summary>
        /// <param name="dtSPStock_Minus">SparePart 현 재고 테이블 : 현재창고 - 데이터</param>
        /// <param name="dtSPStock_Plus">SparePart 현 재고 테이블 : 이동창고 + 데이터</param>
        /// <param name="dtSPStockMoveHist_Minus">SparePart 재고 이동 이력 테이블 : 현재창고 - 데이터</param>
        /// <param name="dtSPStockMoveHist_Plus">SparePart 재고 이동 이력 테이블 : 이동창고 + 데이터</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveMove
            (
                DataTable dtSPStock_Minus
                , DataTable dtSPStock_Plus
                , DataTable dtSPStockMoveHist_Minus
                , DataTable dtSPStockMoveHist_Plus
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. SparePart 현 재고 테이블 (EQUSPStock) : 현재창고 - 데이터-------------------//
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new SPStock();


                if (dtSPStock_Minus.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock_Minus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. SparePart 현 재고 테이블 (EQUSPStock) : 현재창고 + 데이터-------------------//

                if (dtSPStock_Plus.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock_Plus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 3. SparePart 재고이력 테이블 (EQUSPStockMoveHist)  : 현재창고 - 데이터-------------------//
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new SPStockMoveHist();

                if (dtSPStockMoveHist_Minus.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist_Minus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn; 
                    }
                }

                //---------------- 4. SparePart 재고이력 테이블 (EQUSPStockMoveHist)  : 현재창고 + 데이터-------------------//

                if (dtSPStockMoveHist_Plus.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist_Plus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 7. 자재폐기 저장

        /// </summary>
        /// <param name="dtSPDiscard">자재폐기 테이블</param>
        /// <param name="dtSPStock">SparePart 현 재고 테이블</param>
        /// <param name="dtSPStockMoveHist">SparePart 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveDiscard
            (
                DataTable dtSPDiscard
                , DataTable dtSPStock
                , DataTable dtSPStockMoveHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strDiscardCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재출고 헤더 테이블 (EQUSPSPTransferH) : 재고이력은 프로시져에서 처리함-------------------//
                QRPEQU.BL.EQUSPA.SPDiscard clsSPDiscard = new SPDiscard();

                if (dtSPDiscard.Rows.Count > 0)
                {
                    strErrRtn = clsSPDiscard.mfSaveSPDiscard(dtSPDiscard, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //strDiscardCode = ErrRtn.mfGetReturnValue(0);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }             


                //---------------- 2. SparePart 현 재고 테이블 (EQUSPStock) -------------------//
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new SPStock();


                if (dtSPStock.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }



                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 7-1. 자재폐기 삭제

        /// </summary>
        /// <param name="dtSPDiscard">자재폐기 테이블</param>
        /// <param name="dtSPStock">SparePart 현 재고 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfDeleteDiscard
            (
                DataTable dtSPDiscard
                , DataTable dtSPStock
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strDiscardCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재출고 헤더 테이블 (EQUSPSPTransferH) : 재고이력은 프로시져에서 처리함-------------------//
                QRPEQU.BL.EQUSPA.SPDiscard clsSPDiscard = new SPDiscard();

                if (dtSPDiscard.Rows.Count > 0)
                {
                    strErrRtn = clsSPDiscard.mfDeleteSPDiscard(dtSPDiscard, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //strDiscardCode = ErrRtn.mfGetReturnValue(0);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                 

                //---------------- 2. SparePart 현 재고 테이블 (EQUSPStock) -------------------//
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new SPStock();


                if (dtSPStock.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }



                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 8. 자재실사 저장

        /// </summary>
        /// <param name="dtSPStockTakeH">SparePart 자재실사 헤더 테이블</param>
        /// <param name="dtSPStockTakeD">SparePart 자재실사 아이템 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveStockTake
            (
                DataTable dtSPStockTakeH
                , DataTable dtSPStockTakeD
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strStockTakeCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재실사 헤더 테이블 (EQUSPStockTakeH) -------------------//
                QRPEQU.BL.EQUSPA.SPStockTakeH clsSPStockTakeH = new SPStockTakeH();

                if (dtSPStockTakeH.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockTakeH.mfSaveSPStockTakeH(dtSPStockTakeH, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    strStockTakeCode = ErrRtn.mfGetReturnValue(0);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                for (int i = 0; i < dtSPStockTakeD.Rows.Count; i++)
                {
                    dtSPStockTakeD.Rows[i]["StockTakeCode"] = strStockTakeCode;
                }

                //---------------- 2. 자재실사 아이템 테이블 (EQUSPStockTakeD) -------------------//
                QRPEQU.BL.EQUSPA.SPStockTakeD clsSPStockTakeD = new SPStockTakeD();

                //--- 삭제 ---//
                strErrRtn = clsSPStockTakeD.mfDeleteSPStockTakeD(dtSPStockTakeD, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                //--- 저장 ---//
                if (dtSPStockTakeD.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockTakeD.mfSaveSPStockTakeD(dtSPStockTakeD, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 9. 자재실사 확정 저장

        /// </summary>
        /// <param name="dtSPStockTakeH">SparePart 자재출고 헤더 테이블</param>
        /// <param name="dtSPStockTakeD">SparePart 자재출고 아이템 테이블</param>
        /// <param name="dtSPStock">SparePart 현 재고 테이블</param>
        /// <param name="dtSPStockMoveHist">SparePart 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveStockTake_Confirm
            (
                DataTable dtSPStockTakeH
                , DataTable dtSPStockTakeD
                , DataTable dtSPStock
                , DataTable dtSPStockMoveHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재실사 헤더 테이블 (EQUSPStockTakeH) -------------------//
                QRPEQU.BL.EQUSPA.SPStockTakeH clsSPStockTakeH = new SPStockTakeH();

                if (dtSPStockTakeH.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockTakeH.mfSaveSPStockTakeH(dtSPStockTakeH, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. 자재실사 아이템 테이블 (EQUSPStockTakeD) -------------------//
                QRPEQU.BL.EQUSPA.SPStockTakeD clsSPStockTakeD = new SPStockTakeD();

                //--- 삭제 ---//
                strErrRtn = clsSPStockTakeD.mfDeleteSPStockTakeD(dtSPStockTakeD, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                //--- 저장 ---//
                if (dtSPStockTakeD.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockTakeD.mfSaveSPStockTakeD(dtSPStockTakeD, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }



                //---------------- 3. SparePart 현 재고 테이블 (EQUSPStock) -------------------//
                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new SPStock();


                if (dtSPStock.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 4. SparePart 재고이력 테이블 (EQUSPStockMoveHist) -------------------//
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new SPStockMoveHist();

                if (dtSPStockMoveHist.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        
    }




    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPStock")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPStock : ServicedComponent
    {
        #region STS

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPStock = new DataTable();

            try
            {
                dtSPStock.Columns.Add("PlantCode", typeof(String));
                dtSPStock.Columns.Add("SPInventoryCode", typeof(String));
                dtSPStock.Columns.Add("SparePartCode", typeof(String));
                dtSPStock.Columns.Add("Qty", typeof(String));
                dtSPStock.Columns.Add("UnitCode", typeof(String));

                dtSPStock.Columns.Add("ChgInventoryCode", typeof(String));
                dtSPStock.Columns.Add("ChgQty", typeof(String));

                dtSPStock.Columns.Add("UserIP", typeof(String));
                dtSPStock.Columns.Add("UserID", typeof(String));

                return dtSPStock;
            }
            catch (Exception ex)
            {
                return dtSPStock;
                throw (ex);
            }
            finally
            {
                dtSPStock.Dispose();
            }
        }

        /// <summary>
        /// SparePart재고현황조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strSPInventoryCode">창고</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strSparePartCode">SparePartCode</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart재고현황정보</returns>
        [AutoComplete]
        public DataTable mfReadSPStockState(string strPlantCode, string strSPInventoryCode,string strEquipCode,string strSparePartCode, string strLang)
        {
            DataTable dtSparePartState = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar,strSPInventoryCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar,strSparePartCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                dtSparePartState = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPStockState", dtParam);

                //SparePart재고현황 정보
                return dtSparePartState;
                
            }
            catch (Exception ex)
            {
                return dtSparePartState;
                throw (ex);
            }
            finally
            { 
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtSparePartState.Dispose();
            }
        }

        /// <summary>
        /// SparePart 현재재고 테이블 저장

        /// </summary>
        /// <param name="dtSPStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPStock(DataTable dtSPStock, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
            
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPStock.Rows.Count; i++)
                {                   

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStock.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStock.Rows[i]["SPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStock.Rows[i]["SparePartCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParamter, "@i_intQty", ParameterDirection.Input, SqlDbType.Int, dtSPStock.Rows[i]["Qty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStock.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPStock", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 현재고가 없는 SparePart 리스트조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadMASSparePart_InputSPStock(String strPlantCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePart_InputSPStock", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 현재고가 없는 SparePart 리스트조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadSPStock_Detail(String strPlantCode, String strSPInventoryCode, String strSparePartCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strSPInventoryCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, strSparePartCode, 20);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPStock_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 그리드 상에서 공장, 창고코드 선택시 해당되는 SP정보 가져옴 
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strSPInventoryCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadSPStock_SPCombo(String strPlantCode, String strSPInventoryCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strSPInventoryCode, 10);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPStock_SPCombo", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }
        #endregion

        #region PSTS

        /// <summary>
        /// SparePart재고현황조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strSPInventoryCode">창고</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strSparePartCode">SparePartCode</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart재고현황정보</returns>
        [AutoComplete]
        public DataTable mfReadSPStockState_PSTS(string strPlantCode, string strSPInventoryCode, string strEquipCode, string strSparePartCode, string strLang)
        {
            DataTable dtSparePartState = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strSPInventoryCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, strSparePartCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtSparePartState = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPStockState_PSTS", dtParam);

                //SparePart재고현황 정보
                return dtSparePartState;

            }
            catch (Exception ex)
            {
                return dtSparePartState;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtSparePartState.Dispose();
            }
        }

        #endregion
    }




    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPStockMoveHist")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPStockMoveHist : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPStockMoveHist = new DataTable();

            try
            {
                dtSPStockMoveHist.Columns.Add("MoveGubunCode", typeof(String));
                dtSPStockMoveHist.Columns.Add("DocCode", typeof(String));
                dtSPStockMoveHist.Columns.Add("MoveDate", typeof(String));
                dtSPStockMoveHist.Columns.Add("MoveChargeID", typeof(String));
                dtSPStockMoveHist.Columns.Add("PlantCode", typeof(String));
                dtSPStockMoveHist.Columns.Add("SPInventoryCode", typeof(String));
                dtSPStockMoveHist.Columns.Add("EquipCode", typeof(String));
                dtSPStockMoveHist.Columns.Add("SparePartCode", typeof(String));
                dtSPStockMoveHist.Columns.Add("MoveQty", typeof(String));
                dtSPStockMoveHist.Columns.Add("UnitCode", typeof(String));

                dtSPStockMoveHist.Columns.Add("UserIP", typeof(String));
                dtSPStockMoveHist.Columns.Add("UserID", typeof(String));

                return dtSPStockMoveHist;
            }
            catch (Exception ex)
            {
                return dtSPStockMoveHist;
                throw (ex);
            }
            finally
            {
                dtSPStockMoveHist.Dispose();
            }
        }


        /// <summary>
        /// SparePart재고이력조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strSparePartCode">SparePartCode</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart재고이력정보</returns>
        [AutoComplete]
        public DataTable mfReadSPStockMoveHist(string strPlantCode, string strSparePartCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSPStockMoveHist = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar,strSparePartCode,20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                dtSPStockMoveHist = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPStockMoveHist_Display", dtParam);

                //SparePart이력 정보
                return dtSPStockMoveHist;
            }
            catch (Exception ex)
            {
                return dtSPStockMoveHist;
                throw (ex);
            }
            finally
            {
                //디비연결해제
                sql.mfDisConnect();
                sql.Dispose();
                dtSPStockMoveHist.Dispose();
            }
        }



        /// <summary>
        /// SparePart 재고이동 이력 테이블 저장
        /// </summary>
        /// <param name="dtSPStockMoveHistk">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPStockMoveHist(DataTable dtSPStockMoveHist, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPStockMoveHist.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strMoveGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockMoveHist.Rows[i]["MoveGubunCode"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockMoveHist.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strMoveDate", ParameterDirection.Input, SqlDbType.Char, dtSPStockMoveHist.Rows[i]["MoveDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strMoveChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockMoveHist.Rows[i]["MoveChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockMoveHist.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockMoveHist.Rows[i]["SPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockMoveHist.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockMoveHist.Rows[i]["SparePartCode"].ToString(), 20);


                    sql.mfAddParamDataRow(dtParamter, "@i_intMoveQty", ParameterDirection.Input, SqlDbType.Int, dtSPStockMoveHist.Rows[i]["MoveQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockMoveHist.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPStockMoveHist", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {

            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPGR")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPGR : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPStock = new DataTable();
             
            try
            {
                dtSPStock.Columns.Add("PlantCode", typeof(String));
                dtSPStock.Columns.Add("GRCode", typeof(String));
                dtSPStock.Columns.Add("GRSeq", typeof(String));
                dtSPStock.Columns.Add("GRDate", typeof(String));
                dtSPStock.Columns.Add("SparePartCode", typeof(String));
                dtSPStock.Columns.Add("GRQty", typeof(String));
                dtSPStock.Columns.Add("UnitCode", typeof(String));
                dtSPStock.Columns.Add("GRConfirmFlag", typeof(String));
                dtSPStock.Columns.Add("GRConfirmDate", typeof(String));
                dtSPStock.Columns.Add("GRConfirmID", typeof(String));
                dtSPStock.Columns.Add("GRSPInventoryCode", typeof(String));
                dtSPStock.Columns.Add("PONumber", typeof(String));
                dtSPStock.Columns.Add("UserIP", typeof(String));
                dtSPStock.Columns.Add("UserID", typeof(String));

                return dtSPStock;
            }
            catch (Exception ex)
            {
                return dtSPStock;
                throw (ex);
            }
            finally
            {
                dtSPStock.Dispose();
            }
        }


        /// <summary>
        /// SparePart 현재재고 테이블 저장

        /// </summary>
        /// <param name="dtSPStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPGR(DataTable dtSPGR, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPGR.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["GRCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intGRSeq", ParameterDirection.Input, SqlDbType.Int, dtSPGR.Rows[i]["GRSeq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["GRDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intGRQty", ParameterDirection.Input, SqlDbType.Int, dtSPGR.Rows[i]["GRQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["GRConfirmFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["GRConfirmDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRConfirmID", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["GRConfirmID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["GRSPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPONumber", ParameterDirection.Input, SqlDbType.VarChar, dtSPGR.Rows[i]["PONumber"].ToString(), 50);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strGRCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPGR", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                     
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            } 


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 현재고가 없는 SparePart 리스트조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPGR(String strPlantCode, String strFromGRDate, String strToGRDate, String strGRConfirmFlag, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromGRDate", ParameterDirection.Input, SqlDbType.VarChar, strFromGRDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToGRDate", ParameterDirection.Input, SqlDbType.VarChar, strToGRDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strGRConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, strGRConfirmFlag, 1);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPGR", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }
    }



    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPTransferH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPTransferH : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPTransferH = new DataTable();

            try
            {
                dtSPTransferH.Columns.Add("PlantCode", typeof(String));
                dtSPTransferH.Columns.Add("TransferCode", typeof(String));
                dtSPTransferH.Columns.Add("TransferDate", typeof(String));
                dtSPTransferH.Columns.Add("TransferChargeID", typeof(String));
                dtSPTransferH.Columns.Add("EtcDesc", typeof(String));
                dtSPTransferH.Columns.Add("StockFlag", typeof(String));

                dtSPTransferH.Columns.Add("UserIP", typeof(String));
                dtSPTransferH.Columns.Add("UserID", typeof(String));

                return dtSPTransferH;
            }
            catch (Exception ex)
            {
                return dtSPTransferH;
                throw (ex);
            }
            finally
            {
                dtSPTransferH.Dispose();
            }
        }

        /// <summary>
        /// 자재출고 헤더정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPTransferH(String strPlantCode, String strFromTransferDate, String strToTransferDate, String strStockFlag ,String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromTransferDate", ParameterDirection.Input, SqlDbType.VarChar, strFromTransferDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToTransferDate", ParameterDirection.Input, SqlDbType.VarChar, strToTransferDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockFlag", ParameterDirection.Input, SqlDbType.VarChar, strStockFlag, 1);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPTransferH", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 자재출고 헤더정보 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strTransferCode">출고번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>자재출고 헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPTransferH_Detail(String strPlantCode, String strTransferCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, strTransferCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPTransferH_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// SparePart 자재출고 테이블 저장(헤더)
        /// </summary>
        /// <param name="dtSPTransferH">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPTransferH(DataTable dtSPTransferH, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPTransferH.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferH.Rows[i]["TransferCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferH.Rows[i]["TransferDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferH.Rows[i]["TransferChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferH.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferH.Rows[i]["StockFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strTransferCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPTransferH", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


    }



    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPTransferD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPTransferD : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPTransferD = new DataTable();

            try
            {
                dtSPTransferD.Columns.Add("PlantCode", typeof(String));
                dtSPTransferD.Columns.Add("TransferCode", typeof(String));
                dtSPTransferD.Columns.Add("TransferSeq", typeof(String));
                dtSPTransferD.Columns.Add("TransferSPInventoryCode", typeof(String));
                dtSPTransferD.Columns.Add("TransferSPCode", typeof(String));
                dtSPTransferD.Columns.Add("TransferQty", typeof(String));
                dtSPTransferD.Columns.Add("ChgEquipCode", typeof(String));
                dtSPTransferD.Columns.Add("ChgSparePartCode", typeof(String));
                dtSPTransferD.Columns.Add("InputQty", typeof(String));
                dtSPTransferD.Columns.Add("UnitCode", typeof(String));
                dtSPTransferD.Columns.Add("TransferEtcDesc", typeof(String));
                dtSPTransferD.Columns.Add("ChgFlag", typeof(String));
                dtSPTransferD.Columns.Add("ChgDate", typeof(String));
                dtSPTransferD.Columns.Add("ChgChargeID", typeof(String));
                dtSPTransferD.Columns.Add("ChgEtcDesc", typeof(String));
                dtSPTransferD.Columns.Add("ReturnFlag", typeof(String));
                dtSPTransferD.Columns.Add("ReturnDate", typeof(String));
                dtSPTransferD.Columns.Add("ReturnChargeID", typeof(String));
                dtSPTransferD.Columns.Add("ReturnSPInventoryCode", typeof(String));
                dtSPTransferD.Columns.Add("ReturnEtcDesc", typeof(String));
                dtSPTransferD.Columns.Add("DeleteFlag", typeof(String));

                dtSPTransferD.Columns.Add("UserIP", typeof(String));
                dtSPTransferD.Columns.Add("UserID", typeof(String));

                return dtSPTransferD;
            }
            catch (Exception ex)
            {
                return dtSPTransferD;
                throw (ex);
            }
            finally
            {
                dtSPTransferD.Dispose();
            }
        }

        /// <summary>
        /// 자재출고 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strTransferCode">출고번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPTransferD(String strPlantCode, String strTransferCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, strTransferCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPTransferD", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// SparePart 자재출고 테이블 저장(아이템)
        /// </summary>
        /// <param name="dtSPTransferD">저장할데이터항목</param>
        /// <param name="strTransferCode">자재출고번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPTransferD(DataTable dtSPTransferD, string strTransferCode, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPTransferD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["PlantCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParamter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, strTransferCode, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["TransferCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intTransferSeq", ParameterDirection.Input, SqlDbType.Int, dtSPTransferD.Rows[i]["TransferSeq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["TransferSPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferSPCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["TransferSPCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intTransferQty", ParameterDirection.Input, SqlDbType.Int, dtSPTransferD.Rows[i]["TransferQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgEquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgSparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intInputQty", ParameterDirection.Input, SqlDbType.Int, dtSPTransferD.Rows[i]["InputQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["TransferEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ReturnFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ReturnDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ReturnChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ReturnSPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ReturnEtcDesc"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParamter, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["DeleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPTransferD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// SparePartBOM 테이블 저장(MASEquipSPBOM)  --> 점검결과 전용
        /// </summary>
        /// <param name="dtSPTransferD">저장할데이터항목</param>
        /// <param name="strTransferCode">자재출고번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveMASEquipSPBOM_Chg(DataTable dtSPTransferD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPTransferD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferSPCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["TransferSPCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intTransferQty", ParameterDirection.Input, SqlDbType.Int, dtSPTransferD.Rows[i]["TransferQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgEquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgSparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, "", 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipSPBOM_Chg", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// SparePartBOM 테이블 교체대기수량 업데이트(MASEquipSPBOM)
        /// </summary>
        /// <param name="dtSPTransferD">저장할데이터항목</param>
        /// <param name="strTransferCode">자재출고번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveMASEquipSPBOM_ChgStandbyQty(DataTable dtSPTransferD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPTransferD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferSPCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["TransferSPCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intTransferQty", ParameterDirection.Input, SqlDbType.Int, dtSPTransferD.Rows[i]["TransferQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgEquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgSparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, "", 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipSPBOM_ChgStandbyQty", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// SparePartBOM 테이블 저장(MASEquipSPBOM) --> 자재교체전용
        /// </summary>
        /// <param name="dtSPTransferD">저장할데이터항목</param>
        /// <param name="strTransferCode">자재출고번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveMASEquipSPBOM_Chg_Transfer(DataTable dtSPTransferD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPTransferD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferSPCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["TransferSPCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intTransferQty", ParameterDirection.Input, SqlDbType.Int, dtSPTransferD.Rows[i]["TransferQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgEquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["ChgSparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, "", 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipSPBOM_Chg_Transfer", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// SparePart 자재출고 테이블 삭제(아이템)
        /// </summary>
        /// <param name="dtSPTransferD">저장할데이터항목</param>
        /// <param name="strTransferCode">자재출고번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteSPTransferD(DataTable dtSPTransferD, string strTransferCode, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPTransferD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPTransferD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, strTransferCode, 20);
                    
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUSPTransferD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 자재교체 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPTransferD_Chg(String strPlantCode, String strEquipCode, string strChgFlag, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strChgFlag", ParameterDirection.Input, SqlDbType.VarChar, strChgFlag, 1);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPTransferD_Chg", dtParmter);
                //정보리턴
                return dtTable; 
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 자재반납 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPTransferD_Return(String strPlantCode, String strFromChgDate, string strToChgDate, string strReturnFlag, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromChgDate", ParameterDirection.Input, SqlDbType.VarChar, strFromChgDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToChgDate", ParameterDirection.Input, SqlDbType.VarChar, strToChgDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strReturnFlag", ParameterDirection.Input, SqlDbType.VarChar, strReturnFlag, 1);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPTransferD_Return", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            } 
        }


    }
     


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPChgStandby")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPChgStandby : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPChgStandby = new DataTable();

            try
            {
                dtSPChgStandby.Columns.Add("PlantCode", typeof(String));
                dtSPChgStandby.Columns.Add("SparePartCode", typeof(String));
                dtSPChgStandby.Columns.Add("ChgQty", typeof(String));
                dtSPChgStandby.Columns.Add("UnitCode", typeof(String));

                dtSPChgStandby.Columns.Add("UserIP", typeof(String));
                dtSPChgStandby.Columns.Add("UserID", typeof(String));

                return dtSPChgStandby;
            }
            catch (Exception ex)
            {
                return dtSPChgStandby;
                throw (ex);
            }
            finally
            {
                dtSPChgStandby.Dispose();
            }
        }


        /// <summary>
        /// SparePart 대기 재고 테이블 저장

        /// </summary>
        /// <param name="dtSPChgStandby">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPChgStandby(DataTable dtSPChgStandby, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPChgStandby.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPChgStandby.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPChgStandby.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intChgQty", ParameterDirection.Input, SqlDbType.Int, dtSPChgStandby.Rows[i]["ChgQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPChgStandby.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPChgStandby", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPDiscard")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPDiscard : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPDiscard = new DataTable();

            try
            {
                dtSPDiscard.Columns.Add("PlantCode", typeof(String));
                dtSPDiscard.Columns.Add("DiscardCode", typeof(String));
                dtSPDiscard.Columns.Add("DiscardDate", typeof(String));
                dtSPDiscard.Columns.Add("DiscardChargeID", typeof(String));
                dtSPDiscard.Columns.Add("DiscardReason", typeof(String));
                dtSPDiscard.Columns.Add("SPInventoryCode", typeof(String));
                dtSPDiscard.Columns.Add("SparePartCode", typeof(String));
                dtSPDiscard.Columns.Add("DiscardQty", typeof(String));
                dtSPDiscard.Columns.Add("UnitCode", typeof(String));

                dtSPDiscard.Columns.Add("UserIP", typeof(String));
                dtSPDiscard.Columns.Add("UserID", typeof(String));

                return dtSPDiscard;
            }
            catch (Exception ex)
            {
                return dtSPDiscard;
                throw (ex);
            }
            finally
            {
                dtSPDiscard.Dispose();
            }
        }


        /// <summary>
        /// SparePart 자재폐기 테이블 저장

        /// </summary>
        /// <param name="dtSPDiscard">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPDiscard(DataTable dtSPDiscard, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPDiscard.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["DiscardCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["DiscardDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["DiscardChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardReason", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["DiscardReason"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["SPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intDiscardQty", ParameterDirection.Input, SqlDbType.Int, dtSPDiscard.Rows[i]["DiscardQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strDiscardCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPDiscard", dtParamter);
                     
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// SparePart 자재폐기 테이블 삭제

        /// </summary>
        /// <param name="dtSPStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteSPDiscard(DataTable dtSPDiscard, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPDiscard.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["DiscardCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["DiscardDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["DiscardChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardReason", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["DiscardReason"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["SPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intDiscardQty", ParameterDirection.Input, SqlDbType.Int, dtSPDiscard.Rows[i]["DiscardQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPDiscard.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUSPDiscard", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 자재폐기  조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPDiscard(String strPlantCode, String strFromDiscardDate, string strToDiscardDate, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDiscardDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, strToDiscardDate, 10);
          
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPDiscard", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPStockTakeH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPStockTakeH : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("StockTakeCode", typeof(String));
                dtTable.Columns.Add("SPInventoryCode", typeof(String));
                dtTable.Columns.Add("StockTakeYear", typeof(String));
                dtTable.Columns.Add("StockTakeMonth", typeof(String));

                dtTable.Columns.Add("StockTakeDate", typeof(String));
                dtTable.Columns.Add("StockTakeChargeID", typeof(String));
                dtTable.Columns.Add("StockTakeEtcDesc", typeof(String));
                dtTable.Columns.Add("StockTakeConfirmDate", typeof(String));
                dtTable.Columns.Add("StockTakeConfirmID", typeof(String));
                dtTable.Columns.Add("ConfirmEtcDesc", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }



        /// <summary>
        /// SparePart 자재실사 테이블 저장(헤더)
        /// </summary>
        /// <param name="dtSPStockTakeH">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPStockTakeH(DataTable dtSPStockTakeH, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPStockTakeH.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["StockTakeCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["SPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeYear", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["StockTakeYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeMonth", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["StockTakeMonth"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["StockTakeDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["StockTakeChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["StockTakeEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["StockTakeConfirmDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeConfirmID", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["StockTakeConfirmID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strConfirmEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeH.Rows[i]["ConfirmEtcDesc"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strStockTakeCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000); 
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPStockTakeH", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                     
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재실사 헤더정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strSPInventoryCode">창고코드</param>
        /// <param name="strStockTakeYear">실사년도</param>
        /// <param name="strStockTakeMonth">실사월</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>자재실사 헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPStockTakeH(String strPlantCode, String strSPInventoryCode, String strStockTakeYear, String strStockTakeMonth, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strSPInventoryCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeYear", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeYear, 4);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeMonth", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeMonth, 2);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPStockTakeH", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPStockTakeD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPStockTakeD : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("StockTakeCode", typeof(String));
                dtTable.Columns.Add("SPInventoryCode", typeof(String));
                dtTable.Columns.Add("SparePartCode", typeof(String));
                dtTable.Columns.Add("CurStockQty", typeof(String));
                dtTable.Columns.Add("StockTakeQty", typeof(String));
                dtTable.Columns.Add("StockConfirmQty", typeof(String));
                dtTable.Columns.Add("UnitCode", typeof(String));
                dtTable.Columns.Add("EtcDesc", typeof(String));               

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }



        /// <summary>
        /// SparePart 자재실사 테이블 저장(아이템)
        /// </summary>
        /// <param name="dtSPStockTakeD">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveSPStockTakeD(DataTable dtSPStockTakeD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPStockTakeD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeD.Rows[i]["StockTakeCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeD.Rows[i]["SPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeD.Rows[i]["SparePartCode"].ToString(),20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intCurStockQty", ParameterDirection.Input, SqlDbType.Int, dtSPStockTakeD.Rows[i]["CurStockQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_intStockTakeQty", ParameterDirection.Input, SqlDbType.Int, dtSPStockTakeD.Rows[i]["StockTakeQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_intStockConfirmQty", ParameterDirection.Input, SqlDbType.Int, dtSPStockTakeD.Rows[i]["StockConfirmQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeD.Rows[i]["EtcDesc"].ToString(), 1000);
                   
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUSPStockTakeD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// SparePart 자재실사 테이블 삭제(아이템)
        /// </summary>
        /// <param name="dtSPStockTakeD">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteSPStockTakeD(DataTable dtSPStockTakeD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtSPStockTakeD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPStockTakeD.Rows[i]["StockTakeCode"].ToString(), 20);
                    
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUSPStockTakeD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재실사 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strSPInventoryCode">창고코드</param>
        /// <param name="strStockTakeYear">실사년도</param>
        /// <param name="strStockTakeMonth">실사월</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPStockTakeD(String strPlantCode, String strSPInventoryCode, String strStockTakeYear, String strStockTakeMonth, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strSPInventoryCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeYear", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeYear, 4);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeMonth", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeMonth, 2);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPStockTakeD", dtParmter);
                //정보리턴
                return dtTable; 
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 자재실사확정 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStockTakeCode">실사번호</param>

        /// <param name="strLang">사용언어</param>
        /// <returns>자재실사확정 아이템정보</returns>
        [AutoComplete]
        public DataTable mfReadEQUSPStockTakeD_Confirm(String strPlantCode, String strStockTakeCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeCode", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUSPStockTakeD_Confirm", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }
    }
}
