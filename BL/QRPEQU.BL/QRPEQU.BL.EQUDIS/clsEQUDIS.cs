﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비조회관리                                          */
/* 모듈(분류)명 :                                                       */
/* 프로그램ID   : clsEQUDIS.cs                                          */
/* 프로그램명   :                                                       */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-01-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//참조추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;


[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]


namespace QRPEQU.BL.EQUDIS
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairDIS")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class RepairDIS : ServicedComponent
    {
        /// <summary>
        /// 설비상태관리 이력조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비소분류</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strSearchType">검색종류</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비상태관리이력정보</returns>
        [AutoComplete]
        public DataTable mfReadRepairReq_History(string strPlantCode,string strStationCode,string strEquipLocCode,
                                                string strProcessGroupCode,string strEquipLargeTypeCode,
                                                string strEquipGroupCode,string strEquipCode,string strFromDate,string strToDate,
                                                string strSearchType,string strLang)
        {
            DataTable dtHistory = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();


                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSearchType", ParameterDirection.Input, SqlDbType.VarChar, strSearchType, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtHistory = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_History", dtParam);

                return dtHistory;
            }
            catch (Exception ex)
            {
                return dtHistory;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtHistory.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 월간 정지유형 현황조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strRepairStartDate">기간</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strReasonCode">다운코드</param>
        /// <param name="strQRPCheck">QRP/전체 Flag</param>
        /// <param name="strUserID">정비사</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>월간 정지유형 현황정보</returns>
        [AutoComplete]
        public DataTable mfReadRepairReq_Year(string strPlantCode,string strRepairStartDate,string strStationCode,string strEquipLocCode,string strProcessGroupCode,
                                                string strLargeTypeCode , string strEquipGroupCode,string strReasonCode,string strQRPCheck,string strUserID,string strLang)
                                                                                
        {
            SQLS sql = new SQLS();
            DataTable dtYear = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strRepairStartDate", ParameterDirection.Input, SqlDbType.VarChar,strRepairStartDate,7);
                sql.mfAddParamDataRow(dtParam,"@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar,strStationCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipLocCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strProcessGroupCode,40);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar,strLargeTypeCode,40);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipGroupCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strReasonCode", ParameterDirection.Input, SqlDbType.VarChar,strReasonCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strQRPCheck", ParameterDirection.Input, SqlDbType.VarChar,strQRPCheck,20);
                sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //월간 정지유형 현황조회 프로시저 실행
                dtYear = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_Year", dtParam);

                //월간 정지유형 현황정보
                return dtYear;
            }
            catch(Exception ex)
            {
                return dtYear;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtYear.Dispose();
                sql.Dispose();
            }

        }



        /// <summary>
        /// 정지유형 현황조회(일간)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <returns>PM Total건수 정보</returns>
        [AutoComplete]
        public DataTable mfReadRepairReq_Month
            (string strPlantCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroup
            , string strEquipLargeTypeCode
            , string strEquipGroupCode
            , string strUserID
            , string strFromDate
            , string strDownCode
            , string strQRPCheck
            , string strLang)
        {
            DataTable dtPMChk = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairStartDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReasonCode", ParameterDirection.Input, SqlDbType.VarChar, strDownCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strQRPCheck", ParameterDirection.Input, SqlDbType.VarChar, strQRPCheck, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 20);
                //PM체크결과 PM완료여부조회 프로시저 실행
                dtPMChk = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_Month", dtParam);

                return dtPMChk;
            }
            catch (Exception ex)
            {
                return dtPMChk;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMChk.Dispose();
                sql.Dispose();
            }
        }




        /// <returns></returns>
        /// <summary>
        /// 설비상태관리 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipLoc">위치</param>
        /// <param name="strEquipType">유형</param>
        /// <param name="strEquipState">설비상태</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strDownCode">다운코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비수리 요청/접수/결과 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadRepair_MESDB(string strPlantCode,string strEquipLoc, string strEquipType, string strEquipState, string strEquipCode, string strProcessGroupCode ,string strDownCode,string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLoc, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipType", ParameterDirection.Input, SqlDbType.VarChar, strEquipType, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipState", ParameterDirection.Input, SqlDbType.VarChar, strEquipState, 5);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParame, "@i_strDownCode", ParameterDirection.Input, SqlDbType.VarChar, strDownCode, 100);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_MESDB", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_onRepair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("PMDIS")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class PMDIS : ServicedComponent
    {


        /// <summary>
        /// PM계획대비실적조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strYear">계획년도</param>
        /// <param name="strStationCode">StationCode</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <param name="strPMPlanDate">예방점검일</param>
        /// <returns>PM계획대비실적정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_DISPlanResult
            (string strPlantCode, 
             string strPlanYear,
            string strStationCode,
            string strEquipLocCode,
            string strProcessGroupCode,
            string strEquipLargeTypeCode,
            string strEquipGroupCode,
            string strWorkerID,
            string strPMPlanDate)
        {
            SQLS sql = new SQLS();
            DataTable dtPMPlan = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char,strPlanYear,4);
                sql.mfAddParamDataRow(dtParam,"@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar,strStationCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipLocCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strProcessGroupCode,40);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipLargeTypeCode,40);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipGroupCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strWorkID", ParameterDirection.Input, SqlDbType.VarChar,strWorkerID,20);
                sql.mfAddParamDataRow(dtParam,"@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar,strPMPlanDate,7);

                //PM계획대비실적조회 프로시저실행
                dtPMPlan = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_DISPlanResult", dtParam);

                //PM계획대비실적정보
                return dtPMPlan;
            }
            catch (Exception ex)
            {
                return dtPMPlan;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMPlan.Dispose();
                sql.Dispose();
            }
        }



        /// <summary>
        /// PM체크 이력조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>PM체크이력정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanDHist(
                                            string strPlantCode ,
                                            string strEquipCode,
                                            string strFromDate,
                                            string strToDate,
                                            string strLang
                                            )
        {
            DataTable dtPMPlanDHist = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar,strFromDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar,strToDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //PM체크이력조회 프로시저 실행
                dtPMPlanDHist = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanDHist", dtParam);

                //PM체크이력조회정보
                return dtPMPlanDHist;
            }
            catch (Exception ex)
            {
                return dtPMPlanDHist;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMPlanDHist.Dispose();
                sql.Dispose();
            }
       
        }

        #region MyJob
        /// <summary>
        /// 마이잡 월별 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">계획년도</param>
        /// <param name="strMonth>계획적용일</param>
        /// <param name="strUserID">정비사</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과 정보</returns>
        [AutoComplete]
        public DataTable mfReadMyJob_Month(string strPlantCode, string strYear, string strMonth, string strUserID, string strLang)
        {
            SQLS sql = new SQLS();        //실행용
            DataTable dtTable = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();  //실행용

                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUMyJob_Month", dtParam);   //실행용

                return dtTable;

            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 마이잡 주간별 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strYear">계획년도</param>
        /// <param name="strMonth">검색월</param>
        /// <param name="strWeek">검색주</param>
        /// <param name="strUserID">정비사</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과 정보</returns>
        [AutoComplete]
        public DataTable mfReadMyJob_Week(string strPlantCode, string strYear, string strMonth, string strWeek, string strUserID, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMyJobWeek = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 연결정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);   //공장
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.Char, strYear, 4);                 //계획년도
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);            //월
                sql.mfAddParamDataRow(dtParam, "@i_strWeek", ParameterDirection.Input, SqlDbType.VarChar, strWeek, 1);              //주
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);         //정비사
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);              //사용언어

                //MyJob주별 검색 프로시저실행
                dtMyJobWeek = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUMyJob_Week", dtParam);

                //점검결과정보 리턴
                return dtMyJobWeek;

            }
            catch (Exception ex)
            {
                return dtMyJobWeek;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMyJobWeek.Dispose();
            }
        }

        #endregion

        #region 설비보전계획,설비점검일지

        /// <summary>
        /// 설비보전계획표(설비계획)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPMPlanDate">점검월,일</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_MonthPlan(string strPlantCode, string strPlanYear, string strPMPlanDate, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMonthPlan = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 7);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //설비보전계획표(설비계획) 프로시저실행
                dtMonthPlan = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_MonthPlan", dtParam);

                //설비계획 정보리턴
                return dtMonthPlan;
            }
            catch (Exception ex)
            {
                return dtMonthPlan;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtMonthPlan.Dispose();
                sql.Dispose();
            }


        }

        /// <summary>
        /// 설비점검일지(설비점검결과)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPMPlanDate">점검월,일</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_MonthResult(string strPlantCode, string strPlanYear, string strPMPlanDate, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable MonthResult = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 7);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //설비보전계획표(설비계획) 프로시저실행
                MonthResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_MonthResult", dtParam);

                //설비계획 정보리턴
                return MonthResult;
            }
            catch (Exception ex)
            {
                return MonthResult;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                MonthResult.Dispose();
                sql.Dispose();
            }


        }

        #endregion


        #region PM체크 결과조회


        /// <summary>
        /// PM체크결과 Total건수 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <returns>PM Total건수 정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_TotalCount
            (string strPlantCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroup
            , string strEquipLargeTypeCode
            , string strEquipGroupCode
            , string strWorkerID
            , string strPlanYear
            , string strFromDate
            , string strToDate)
        {
            DataTable dtPMChk = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strWorkerID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);

                //PM체크결과 PM완료여부조회 프로시저 실행
                dtPMChk = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_TotalCount", dtParam);

                return dtPMChk;
            }
            catch (Exception ex)
            {
                return dtPMChk;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMChk.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// PM체크결과 설비조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_Equip(string strPlantCode, string strStationCode,
                                    string strEquipLocCode, string strProcessGroup,
                                    string strEquipLargeTypeCode, string strEquipGroupCode, string strWorkerID)
        {
            SQLS sql = new SQLS();
            DataTable dtPMResult = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strWorkerID, 20);

                //PM체크결과 설비조회 프로시저 실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_Equip", dtParam);

                //PM체크결과 설비정보
                return dtPMResult;
            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMResult.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// PM체크결과 정비사조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <returns>정비사정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_Worker(string strPlantCode, string strStationCode,
                                    string strEquipLocCode, string strProcessGroup,
                                    string strEquipLargeTypeCode, string strEquipGroupCode, string strWorkerID)
        {
            SQLS sql = new SQLS();
            DataTable dtPMResult = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strWorkerID, 20);

                //PM체크결과 설비조회 프로시저 실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_Worker", dtParam);

                //PM체크결과 설비정보
                return dtPMResult;
            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMResult.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// PM체크결과 PM미완료설비조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strSearch">조회구분자 E : 설비별 , U : 정비사별</param>
        /// <returns>PM미완료설비정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_PMChk(string strPlantCode, string strStationCode,
                                    string strEquipLocCode, string strProcessGroup,
                                    string strEquipLargeTypeCode, string strEquipGroupCode, string strWorkerID, string strPlanYear, string strFromDate, string strToDate, string strSearch)
        {
            DataTable dtPMChk = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strWorkerID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSearch", ParameterDirection.Input, SqlDbType.VarChar, strSearch, 1);

                //PM체크결과 PM완료여부조회 프로시저 실행
                dtPMChk = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_PMChk", dtParam);

                return dtPMChk;
            }
            catch (Exception ex)
            {
                return dtPMChk;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMChk.Dispose();
                sql.Dispose();
            }
        }




        /// <summary>
        /// PM체크결과 그리드 셀클릭시 셀의해당하는 대상 PM상세조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strTagetCode">검색대상(설비or정비사)</param>
        /// <param name="strTaget">구분자코드(E : 설비 ,U : 정비사)</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strFormDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>PM정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_Detail(string strPlantCode, string strTagetCode, string strTaget, string strPlanYear, string strFormDate, string strToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPM = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strTagetCode", ParameterDirection.Input, SqlDbType.VarChar, strTagetCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strTaget", ParameterDirection.Input, SqlDbType.VarChar, strTaget, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFormDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //TagetCode(설비or정비사)의 기간안에있는 PM조회프로시저 실행
                dtPM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_Detail", dtParam);

                //PM정보
                return dtPM;
            }
            catch (Exception ex)
            {
                return dtPM;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPM.Dispose();
                sql.Dispose();
            }
        }

        #endregion
    }
}
