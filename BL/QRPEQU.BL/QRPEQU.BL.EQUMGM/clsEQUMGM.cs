﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 점검관리                                              */
/* 프로그램ID   : clsEQUMGM.cs                                          */
/* 프로그램명   : 점검관리                                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-08-16                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;


[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
              Authentication = AuthenticationOption.None,
             ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPEQU.BL.EQUMGM
{

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    //[ObjectPooling(true)]
    [ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000*5)]
    [Serializable]
    [System.EnterpriseServices.Description("PMPlan")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class PMPlan : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public PMPlan()
        {
        }

        public PMPlan(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        //'===>헤더정보 EquipGroup 및 EquipWorker로 조회 Method 구성 ===
        /// <summary>
        /// 예방점검계획정보 조회(설비그룹)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <returns>예방점검계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanEquipGroup
            (string strPlantCode
            , string strPlanYear
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroup
            , string strEquipLargeTypeCode
            , string strEquipGroupCode)
        {
            SQLS sql = new SQLS();            //실행용 
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtPMPlan = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect(); //실행용

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParame, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                
                //프로시저 실행
                dtPMPlan = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlan_EquipGroup", dtParame);     //실행용
                //dtPMPlan = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUPMPlan_EquipGroup", dtParame);   //디버깅용

                //정보리턴
                return dtPMPlan;
            }
            catch (Exception ex)
            {
                return dtPMPlan;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMPlan.Dispose();
            }
        }

        /// <summary>
        /// 예방점검계획정보 조회(작업자)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>예방점검계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanEquipWorker(string strPlantCode, string strPlanYear, string strUserID)
        {
            SQLS sql = new SQLS();            //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtPMPlan = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect(); //실행용

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                //프로시저 실행
                dtPMPlan = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlan_EquipWorker", dtParame);     //실행용
                //dtPMPlan = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUPMPlan_EquipWorker", dtParame);   //디버깅용

                //정보리턴
                return dtPMPlan;
            }
            catch (Exception ex)
            {
                return dtPMPlan;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMPlan.Dispose();
            }
        }

        /// <summary>
        /// 예방점검계획정보 조회(일괄)
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <param name="strStationCode">Station코드</param>
        /// <param name="strProcGubunCode">공정구분</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>예방점검계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanBatch(string strPlanYear, string strPlantCode, string strAreaCode, string strStationCode, string strProcGubunCode, string strLang)
        {
            SQLS sql = new SQLS();            //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtPMPlan = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect(); //실행용

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParame, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);

                //프로시저 실행
                dtPMPlan = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlan_Batch", dtParame);     //실행용
                //dtPMPlan = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUPMPlan_Batch", dtParame);   //디버깅용

                //정보리턴
                return dtPMPlan;

            }
            catch (Exception ex)
            {
                return dtPMPlan;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMPlan.Dispose();
            }
        }

        /// <summary>
        /// 예방점검계획정보 조회(일괄)
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <param name="strStationCode">Station코드</param>
        /// <param name="strProcGubunCode">공정구분</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>예방점검계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlan(string strPlanYear, string strPlantCode, string strAreaCode, string strStationCode, string strProcGubunCode, string strLang)
        {
            DataTable dtPMPlan = new DataTable();
            SQLS sql = new SQLS();    //실행용

            try
            {
                //디비오픈
                sql.mfConnect();

                return dtPMPlan;
            }
            catch(Exception ex)
            {
                return dtPMPlan;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMPlan.Dispose();
            }
        }

        


        /////////// <summary>
        /////////// 예방점검정보 저장(그룹별)
        /////////// </summary>
        /////////// <param name="strPlantCode">공장코드</param>
        /////////// <param name="strPlanYear">계획년도</param>
        /////////// <param name="strGroupCode">설비그룹코드</param>
        /////////// <param name="strWriteID">작성자</param>
        /////////// <param name="strWriteDate">작성일</param>
        /////////// <param name="strPMApplyDate">계획적용시작일</param>
        /////////// <param name="strUserIP">사용자아이피</param>
        /////////// <param name="strUserID">사용자아이디</param>
        /////////// <returns>처리결과값</returns>
        ////////[AutoComplete(false)]
        ////////public string mfSavePMPlan(string strPlantCode, string strPlanYear, string strGroupCode, string strWriteID, string strWriteDate, string strPMApplyDate,string strUserIP, string strUserID)
        ////////{
        ////////    SQLS sql = new SQLS();
        ////////    TransErrRtn ErrRtn = new TransErrRtn();
        ////////    string strErrRtn = "";

        ////////    try
        ////////    {
        ////////        sql.mfConnect();
        ////////        SqlTransaction trans;
        ////////        //Transaction시작
        ////////        trans = sql.SqlCon.BeginTransaction();

                

        ////////        strErrRtn = mfDeletePMPlan(strPlantCode, strGroupCode, strPlanYear, sql.SqlCon, trans);
        ////////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////        if (ErrRtn.ErrNum != 0)
        ////////        {
        ////////            trans.Rollback();
        ////////            return strErrRtn;
        ////////        }

        ////////        //파라미터 저장
        ////////        DataTable dtParameter = sql.mfSetParamDataTable();
        ////////        sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

        ////////        sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        ////////        sql.mfAddParamDataRow(dtParameter, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
        ////////        sql.mfAddParamDataRow(dtParameter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);
        ////////        sql.mfAddParamDataRow(dtParameter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, strWriteID, 20);
        ////////        sql.mfAddParamDataRow(dtParameter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteDate, 10);
        ////////        sql.mfAddParamDataRow(dtParameter, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);

        ////////        sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
        ////////        sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

        ////////        sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        ////////        //프로시저 실행
        ////////        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlan", dtParameter);


        ////////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

        ////////        //처리결과 값 처리
        ////////        if (ErrRtn.ErrNum != 0)
        ////////        {
        ////////            trans.Rollback();
        ////////            return strErrRtn;
        ////////        }
        ////////        else
        ////////        {
        ////////            PMPlanD clsPlanD = new PMPlanD();


        ////////            strErrRtn = clsPlanD.mfSavePMPlanD(strPlantCode, strGroupCode, strPMApplyDate, strUserID, strUserIP, sql.SqlCon, trans);

        ////////            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////            if (ErrRtn.ErrNum != 0)
        ////////            {
        ////////                trans.Rollback();
        ////////                return strErrRtn;
        ////////            }

        ////////            trans.Commit();
        ////////        }
        ////////        //처리결과 리턴
        ////////        return strErrRtn;
        ////////    }
        ////////    catch (Exception ex)
        ////////    {
        ////////        ErrRtn.SystemInnerException = ex.InnerException.ToString();
        ////////        ErrRtn.SystemMessage = ex.Message;
        ////////        ErrRtn.SystemStackTrace = ex.StackTrace;
        ////////        return ErrRtn.mfEncodingErrMessage(ErrRtn);
        ////////        throw (ex);
        ////////    }
        ////////    finally
        ////////    {
        ////////        //디비종료
        ////////        sql.mfDisConnect();
        ////////    }
        ////////}

        /////////// <summary>
        /////////// 예방점검정보 저장(설비그룹별)
        /////////// </summary>
        /////////// <param name="strPlantCode">공장코드</param>
        /////////// <param name="strPlanYear">계획년도</param>
        /////////// <param name="strEquipGroupCode">설비그룹코드</param>
        /////////// <param name="strWriteID">작성자</param>
        /////////// <param name="strWriteDate">작성일</param>
        /////////// <param name="strPMApplyDate">계획적용시작일</param>
        /////////// <param name="strUserIP">사용자아이피</param>
        /////////// <param name="strUserID">사용자아이디</param>
        /////////// <returns>처리결과값</returns>
        ////////[AutoComplete(false)]
        ////////public string mfSavePMPlanEquipGroup
        ////////    (string strPlantCode
        ////////    , string strPlanYear
        ////////    , string strEquipGroupCode
        ////////    , string strStationCode
        ////////    , string strEquipLocCode
        ////////    , string strProcessGroupCode
        ////////    , string strEquipLargeTypeCode
        ////////    , string strWriteID
        ////////    , string strWriteDate
        ////////    , string strPMApplyDate
        ////////    , string strUserIP
        ////////    , string strUserID)
        ////////{
        ////////    SQLS sql = new SQLS();    //실행용
        ////////    //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

        ////////    TransErrRtn ErrRtn = new TransErrRtn();
        ////////    string strErrRtn = "";

        ////////    try
        ////////    {
        ////////        sql.mfConnect();  //실행용

        ////////        //////////설비그룹에 대한 설비리스트를 가지고 온다. => 추가//
        ////////        ////////QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();        //실행용
        ////////        //////////QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD(m_SqlConnDebug.ConnectionString);  //디버깅용
        ////////        ////////DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMDForEquipGroup(strPlantCode, strEquipGroupCode, sql.SqlCon);  //실행용
        ////////        //////////DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMDForEquipGroup(strPlantCode, strEquipGroupCode, m_SqlConnDebug);  //디버깅용
        ////////        DataTable dtParameterPMD = sql.mfSetParamDataTable();
        ////////        //파라미터값 저장
        ////////        sql.mfAddParamDataRow(dtParameterPMD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        ////////        sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
        ////////        sql.mfAddParamDataRow(dtParameterPMD, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
        ////////        sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
        ////////        sql.mfAddParamDataRow(dtParameterPMD, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
        ////////        sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
        ////////        //프로시저 호출
        ////////        DataTable dtEquipPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMD_EquipGroup", dtParameterPMD); //실행용
        ////////        //DataTable dtEquipPMD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipPMD_EquipGroup", dtParameterPMD); //디버깅용용


        ////////        SqlTransaction trans;
        ////////        //Transaction시작
        ////////        trans = sql.SqlCon.BeginTransaction();    //실행용
        ////////        //trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

        ////////        strErrRtn = mfDeletePMPlanEquipGroup
        ////////            (strPlantCode                    
        ////////            , strEquipGroupCode
        ////////            , strStationCode
        ////////            , strEquipLocCode
        ////////            , strProcessGroupCode
        ////////            , strEquipLargeTypeCode
        ////////            , strPlanYear
        ////////            , sql.SqlCon
        ////////            , trans);    //실행용
        ////////        //strErrRtn = mfDeletePMPlanEquipGroup(strPlantCode, strEquipGroupCode, strPlanYear, m_SqlConnDebug, trans);      //디버깅용
        ////////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////////        if (ErrRtn.ErrNum != 0)
        ////////        {
        ////////            trans.Rollback();
        ////////            return strErrRtn;
        ////////        }

        ////////        if (dtEquipPMD.Rows.Count <= 0)
        ////////        {
        ////////            trans.Rollback();
        ////////            return strErrRtn;
        ////////        }
        ////////        else
        ////////        {
        ////////            //예방점검계획을 생성한다.
        ////////            DataTable dtPMPlanD = GeneratePMSchedule(strPlantCode, strPlanYear, strPMApplyDate, dtEquipPMD);

        ////////            string strCurEquipCode = "";
        ////////            bool bolSaveHeader = false;

        ////////            //==== 여기부터 시작 (2011.09.27) =====
        ////////            //생성한 예방점검계획을 저장한다.
        ////////            for (int i = 0; i < dtPMPlanD.Rows.Count; i++)
        ////////            {
        ////////                if (strCurEquipCode == "")
        ////////                {
        ////////                    strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
        ////////                    bolSaveHeader = true;
        ////////                }
        ////////                else
        ////////                {
        ////////                    if (strCurEquipCode != dtPMPlanD.Rows[i]["EquipCode"].ToString())
        ////////                    {
        ////////                        strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
        ////////                        bolSaveHeader = true;
        ////////                    }
        ////////                    else
        ////////                        bolSaveHeader = false;
        ////////                }

        ////////                //예방점검 헤더 저장하기
        ////////                if (bolSaveHeader == true)
        ////////                {
        ////////                    //파라미터 저장
        ////////                    DataTable dtParameter = sql.mfSetParamDataTable();
        ////////                    sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

        ////////                    sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        ////////                    sql.mfAddParamDataRow(dtParameter, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
        ////////                    sql.mfAddParamDataRow(dtParameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strCurEquipCode, 20);
        ////////                    sql.mfAddParamDataRow(dtParameter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, strWriteID, 20);
        ////////                    sql.mfAddParamDataRow(dtParameter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteDate, 10);
        ////////                    sql.mfAddParamDataRow(dtParameter, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
        ////////                    sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
        ////////                    sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

        ////////                    sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        ////////                    //프로시저 실행
        ////////                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlan", dtParameter); //실행용
        ////////                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlan", dtParameter);   //디버깅용

        ////////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

        ////////                    //처리결과 값 처리
        ////////                    if (ErrRtn.ErrNum != 0)
        ////////                    {
        ////////                        trans.Rollback();
        ////////                        return strErrRtn;
        ////////                    }
        ////////                } 

        ////////                //예방점검 상세 저장하기
        ////////                //파라미터 저장
        ////////                DataTable dtParameterD = sql.mfSetParamDataTable();
        ////////                sql.mfAddParamDataRow(dtParameterD, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["EquipCode"].ToString(), 20);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtPMPlanD.Rows[i]["Seq"].ToString());

        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPlanDate"].ToString(), 10);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectName", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectName"].ToString(), 100);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectRegion", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectRegion"].ToString(), 100);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectCriteria", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectCriteria"].ToString(), 100);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strPMPeriodCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPeriodCode"].ToString(), 3);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strPMMethod", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMMethod"].ToString(), 100);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strFaultFixMethod", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["FaultFixMethod"].ToString(), 100);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_intStandardManCount", ParameterDirection.Input, SqlDbType.Int, dtPMPlanD.Rows[i]["StandardManCount"].ToString(), 10);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_dblStandardTime", ParameterDirection.Input, SqlDbType.Decimal, dtPMPlanD.Rows[i]["StandardTime"].ToString());
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strLevelCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["LevelCode"].ToString(), 1);

        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strImageFile", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanD.Rows[i]["ImageFile"].ToString(), 1000);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strUnitDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanD.Rows[i]["UnitDesc"].ToString(), 100);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strMeasureValueFlag", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["MeasureValueFlag"].ToString(), 1);


        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
        ////////                sql.mfAddParamDataRow(dtParameterD, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        ////////                //프로시저 실행
        ////////                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlanD", dtParameterD);       //실행용
        ////////                //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlanD", dtParameterD);     //디버깅용
        ////////                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

        ////////                //처리결과 값 처리
        ////////                if (ErrRtn.ErrNum != 0)
        ////////                {
        ////////                    trans.Rollback();
        ////////                    return strErrRtn;
        ////////                }
        ////////            }
        ////////        }
                
        ////////        if(ErrRtn.ErrNum.Equals(0))
        ////////            trans.Commit();
                
        ////////        return strErrRtn;   
        ////////    }
        ////////    catch (System.Exception ex)
        ////////    {
        ////////        ErrRtn.SystemInnerException = ex.InnerException.ToString();
        ////////        ErrRtn.SystemMessage = ex.Message;
        ////////        ErrRtn.SystemStackTrace = ex.StackTrace;
        ////////        return ErrRtn.mfEncodingErrMessage(ErrRtn);
        ////////        throw (ex);
        ////////    }
        ////////    finally
        ////////    {
        ////////        sql.mfDisConnect();
        ////////        sql.Dispose();
        ////////    }
        ////////}


        /// <summary>
        /// 예방점검정보 저장(설비그룹별)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strWriteID">작성자</param>
        /// <param name="strWriteDate">작성일</param>
        /// <param name="strPMApplyDate">계획적용시작일</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strStationCode"></param>
        /// <param name="strEquipLocCode"></param>
        /// <param name="strProcessGroupCode"></param>
        /// <param name="strEquipLargeTypeCode"></param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanEquipGroup
            (string strPlantCode
            , string strPlanYear
            , string strEquipGroupCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroupCode
            , string strEquipLargeTypeCode
            , string strWriteID
            , string strWriteDate
            , string strPMApplyDate
            , string strUserIP
            , string strUserID)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();  //실행용
                
                #region 설비확인

                //////////설비그룹에 대한 설비리스트를 가지고 온다. => 추가//
                ////////QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();        //실행용
                //////////QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD(m_SqlConnDebug.ConnectionString);  //디버깅용
                ////////DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMDForEquipGroup(strPlantCode, strEquipGroupCode, sql.SqlCon);  //실행용
                //////////DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMDForEquipGroup(strPlantCode, strEquipGroupCode, m_SqlConnDebug);  //디버깅용
                DataTable dtParameterPMD = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                //프로시저 호출
                DataTable dtEquipPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMD_EquipGroup", dtParameterPMD); //실행용
                //DataTable dtEquipPMD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipPMD_EquipGroup", dtParameterPMD); //디버깅용용

                if (dtEquipPMD.Rows.Count <= 0)
                {
                    return strErrRtn;
                }

                #endregion

                #region PM체크 재생성시 결과등록여부 조회

                PMPlanD clsPMPlanD = new PMPlanD();
                DataTable dtEquipCheckPMD = new DataTable();


                //파라미터정보저장
                DataTable dtParam_Check = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam_Check, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strEquipLargeType", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);

                //점검계획조정 조회 프로시저 실행
                dtEquipCheckPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_CheckResult_EquipGroup", dtParam_Check);

                #endregion

                SqlTransaction trans;
                //Transaction시작

                #region Delete PMDetail
                ////=============================================10번 나눠서 Delete처리 ===============================================================

                //1. EQUPMPlanD 테이블 10번 나눠서 삭제
                
                //for (int i = 0; i < 10; i++)
                //{
                //    trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted);    //실행용
                //    //trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

                //    //strErrRtn = mfDeletePMPlanDEquipGroup_10
                //    //(strPlantCode
                //    //, strEquipGroupCode
                //    //, strStationCode
                //    //, strEquipLocCode
                //    //, strProcessGroupCode
                //    //, strEquipLargeTypeCode
                //    //, strPlanYear
                //    //, strPMApplyDate
                //    //, sql.SqlCon
                //    //, trans);    //실행용

                //    //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //    //if (ErrRtn.ErrNum != 0)
                //    //{
                //    //    trans.Rollback();
                //    //    return strErrRtn;
                //    //}
                //    //else
                //    //{
                //    //    trans.Commit();
                //    //}
                //}

                for (int i = 0; i < 50; i++ )
                {
                    trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted);    //실행용

                    //파라미터값저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);

                    if(i>40)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "50");
                    else if(i>30)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "40");
                    else if (i > 20)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "30");
                    else if(i > 10)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "20");
                    else
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "10");

                    sql.mfAddParamDataRow(dtParam, "@o_strRowCoutCheck", ParameterDirection.Output, SqlDbType.Char, 1);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMPlanDEquipGroup_Loop", dtParam); //실행용 (sql.sqlCon -> sqlcon으로 변경)

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Commit();
                        string strRtn = ErrRtn.mfGetReturnValue(0) == null ? "" : ErrRtn.mfGetReturnValue(0);
                        if(strRtn.Equals("T"))
                            break;
                    }
                }

                #endregion

                #region Delete PMHeader

                // 2. EQUPMPlan 삭제
                trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted);    //실행용
                //trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

                strErrRtn = mfDeletePMPlanEquipGroup
                    (strPlantCode
                    , strEquipGroupCode
                    , strStationCode
                    , strEquipLocCode
                    , strProcessGroupCode
                    , strEquipLargeTypeCode
                    , strPlanYear
                    , strPMApplyDate
                    , sql.SqlCon
                    , trans);    //실행용

                //strErrRtn = mfDeletePMPlanEquipGroup_10
                //    (strPlantCode
                //    , strEquipGroupCode
                //    , strStationCode
                //    , strEquipLocCode
                //    , strProcessGroupCode
                //    , strEquipLargeTypeCode
                //    , strPlanYear
                //    , sql.SqlCon
                //    , trans);    //실행용

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                ////====================================================================================================================================






                //============================================ 1번에 Delete처리 하는 부분 주석처리 : 주석버튼 네번==============================
                ////////trans = sql.SqlCon.BeginTransaction();    //실행용
                //////////trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

                ////////strErrRtn = mfDeletePMPlanEquipGroup
                ////////    (strPlantCode
                ////////    , strEquipGroupCode
                ////////    , strStationCode
                ////////    , strEquipLocCode
                ////////    , strProcessGroupCode
                ////////    , strEquipLargeTypeCode
                ////////    , strPlanYear
                ////////    , sql.SqlCon
                ////////    , trans);    //실행용

                //////////strErrRtn = mfDeletePMPlanEquipGroup(strPlantCode, strEquipGroupCode, strPlanYear, m_SqlConnDebug, trans);      //디버깅용
                ////////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////////if (ErrRtn.ErrNum != 0)
                ////////{
                ////////    trans.Rollback();
                ////////    return strErrRtn;
                ////////}
                ////////else
                ////////{
                ////////    trans.Commit();
                ////////}
                //===================================================================================================================



                #endregion

                #region Save PM

                //예방점검계획을 생성한다.
                DataTable dtPMPlanD = GeneratePMSchedule(strPlantCode, strPlanYear, strPMApplyDate, dtEquipPMD, dtEquipCheckPMD);

                string strCurEquipCode = "";
                bool bolSaveHeader = false;

                //==== 여기부터 시작 (2011.09.27) =====
                //생성한 예방점검계획을 저장한다.
                for (int i = 0; i < dtPMPlanD.Rows.Count; i++)
                {
                    if (strCurEquipCode == "")
                    {
                        strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
                        bolSaveHeader = true;

                        //trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted); 
                    }
                    else
                    {
                        if (strCurEquipCode != dtPMPlanD.Rows[i]["EquipCode"].ToString())
                        {
                            strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
                            bolSaveHeader = true;
                            
                            //trans.Commit();

                            //trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted); 
                        }
                        else
                            bolSaveHeader = false;
                    }

                    //예방점검 헤더 저장하기
                    if (bolSaveHeader == true)
                    {
                        trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted); 
                        //파라미터 저장
                        DataTable dtParameter = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                        sql.mfAddParamDataRow(dtParameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strCurEquipCode, 20);
                        sql.mfAddParamDataRow(dtParameter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, strWriteID, 20);
                        sql.mfAddParamDataRow(dtParameter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteDate, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlan", dtParameter); //실행용
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlan", dtParameter);   //디버깅용

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리결과 값 처리 
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                        else
                        {
                            trans.Commit();
                        }
                    }

                    //////////예방점검 상세 저장하기
                    //////////파라미터 저장
                    ////////DataTable dtParameterD = sql.mfSetParamDataTable();
                    ////////sql.mfAddParamDataRow(dtParameterD, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["EquipCode"].ToString(), 20);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtPMPlanD.Rows[i]["Seq"].ToString());

                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPlanDate"].ToString(), 10);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectName", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectName"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectRegion", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectRegion"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectCriteria", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectCriteria"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMPeriodCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPeriodCode"].ToString(), 3);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMMethod", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMMethod"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strFaultFixMethod", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["FaultFixMethod"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_intStandardManCount", ParameterDirection.Input, SqlDbType.Int, dtPMPlanD.Rows[i]["StandardManCount"].ToString(), 10);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_dblStandardTime", ParameterDirection.Input, SqlDbType.Decimal, dtPMPlanD.Rows[i]["StandardTime"].ToString());
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strLevelCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["LevelCode"].ToString(), 1);

                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strImageFile", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanD.Rows[i]["ImageFile"].ToString(), 1000);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strUnitDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanD.Rows[i]["UnitDesc"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strMeasureValueFlag", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["MeasureValueFlag"].ToString(), 1);


                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //////////프로시저 실행
                    ////////strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlanD", dtParameterD);       //실행용
                    //////////strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlanD", dtParameterD);     //디버깅용
                    ////////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //////////처리결과 값 처리
                    ////////if (ErrRtn.ErrNum != 0)
                    ////////{
                    ////////    trans.Rollback();
                    ////////    return strErrRtn;
                    ////////}



                }

                

                //if (ErrRtn.ErrNum.Equals(0))
                //    trans.Commit();

                SqlBulkCopy bc = new SqlBulkCopy(sql.SqlCon);
                bc.DestinationTableName = "EQUPMPlanD";
                
                bc.BulkCopyTimeout = 600;
                bc.ColumnMappings.Add("PlantCode", "PlantCode");
                bc.ColumnMappings.Add("PlanYear", "PlanYear");
                bc.ColumnMappings.Add("EquipCode", "EquipCode");
                bc.ColumnMappings.Add("Seq", "Seq");
                bc.ColumnMappings.Add("PMPlanDate", "PMPlanDate");
                bc.ColumnMappings.Add("PMInspectName", "PMInspectName");
                bc.ColumnMappings.Add("PMInspectRegion", "PMInspectRegion");
                bc.ColumnMappings.Add("PMInspectCriteria", "PMInspectCriteria");
                bc.ColumnMappings.Add("PMPeriodCode", "PMPeriodCode");
                bc.ColumnMappings.Add("PMMethod", "PMMethod");
                bc.ColumnMappings.Add("FaultFixMethod", "FaultFixMethod");
                bc.ColumnMappings.Add("StandardManCount", "StandardManCount");
                bc.ColumnMappings.Add("StandardTime", "StandardTime");
                bc.ColumnMappings.Add("LevelCode", "LevelCode");
                bc.ColumnMappings.Add("PMResultCode", "PMResultCode");
                bc.ColumnMappings.Add("ImageFile", "ImageFile");
                bc.ColumnMappings.Add("UnitDesc", "UnitDesc");
                bc.ColumnMappings.Add("PMResultValue", "PMResultValue");
                bc.ColumnMappings.Add("MeasureValueFlag", "MeasureValueFlag");
                bc.WriteToServer(dtPMPlanD);

                #endregion

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 예방점검정보 저장(설비그룹별)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strWriteID">작성자</param>
        /// <param name="strWriteDate">작성일</param>
        /// <param name="strPMApplyDate">계획적용시작일</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strStationCode"></param>
        /// <param name="strEquipLocCode"></param>
        /// <param name="strProcessGroupCode"></param>
        /// <param name="strEquipLargeTypeCode"></param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanEquipGroup_PSTS
            (string strPlantCode
            , string strPlanYear
            , string strEquipGroupCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroupCode
            , string strEquipLargeTypeCode
            , string strWriteID
            , string strWriteDate
            , string strPMApplyDate
            , string strUserIP
            , string strUserID)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();  //실행용

                #region 설비확인

                //////////설비그룹에 대한 설비리스트를 가지고 온다. => 추가//

                DataTable dtParameterPMD = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                //프로시저 호출
                DataTable dtEquipPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMD_EquipGroup_PSTS", dtParameterPMD); //실행용
                //DataTable dtEquipPMD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipPMD_EquipGroup", dtParameterPMD); //디버깅용용
                if (dtEquipPMD.Rows.Count <= 0)
                {
                    return strErrRtn;
                }

                #endregion

                #region PM체크 재생성시 결과등록여부 조회

                PMPlanD clsPMPlanD = new PMPlanD();
                DataTable dtEquipCheckPMD = new DataTable();


                //파라미터정보저장
                DataTable dtParam_Check = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam_Check, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strEquipLargeType", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);

                //점검계획조정 조회 프로시저 실행
                dtEquipCheckPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_CheckResult_EquipGroup", dtParam_Check);
                //dtEquipCheckPMD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUPMPlanD_CheckResult_EquipGroup", dtParam_Check); //디버깅용용
                #endregion

                SqlTransaction trans;
                //Transaction시작
                
                #region Delete PMDetail


                for (int i = 0; i < 60; i++)
                {
                    trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted);    //실행용

                    //파라미터값저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);

                    if (i > 50)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "50");
                    else if (i > 30)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "40");
                    else if (i > 20)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "30");
                    else if (i > 10)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "20");
                    else
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "10");

                    sql.mfAddParamDataRow(dtParam, "@o_strRowCoutCheck", ParameterDirection.Output, SqlDbType.Char, 1);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMPlanDEquipGroup_Loop", dtParam); //실행용 (sql.sqlCon -> sqlcon으로 변경)

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Commit();
                        string strRtn = ErrRtn.mfGetReturnValue(0) == null ? "" : ErrRtn.mfGetReturnValue(0);
                        if (strRtn.Equals("T"))
                            break;
                    }
                }

                #endregion

                #region Delete PMHeader

                // 2. EQUPMPlan 삭제
                trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted);    //실행용

                strErrRtn = mfDeletePMPlanEquipGroup
                    (strPlantCode
                    , strEquipGroupCode
                    , strStationCode
                    , strEquipLocCode
                    , strProcessGroupCode
                    , strEquipLargeTypeCode
                    , strPlanYear
                    , strPMApplyDate
                    , sql.SqlCon
                    , trans);    //실행용


                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }



                #endregion

                #region Save PM

                //예방점검계획을 생성한다.
                DataTable dtPMPlanD = GeneratePMSchedule_PSTS(strPlantCode, strPlanYear, strPMApplyDate, dtEquipPMD, dtEquipCheckPMD);

                string strCurEquipCode = "";
                bool bolSaveHeader = false;

                //==== 여기부터 시작 (2011.09.27) =====
                //생성한 예방점검계획을 저장한다.
                for (int i = 0; i < dtPMPlanD.Rows.Count; i++)
                {
                    if (strCurEquipCode == "")
                    {
                        strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
                        bolSaveHeader = true;

                        //trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted); 
                    }
                    else
                    {
                        if (strCurEquipCode != dtPMPlanD.Rows[i]["EquipCode"].ToString())
                        {
                            strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
                            bolSaveHeader = true;

                            //trans.Commit();

                            //trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted); 
                        }
                        else
                            bolSaveHeader = false;
                    }

                    //예방점검 헤더 저장하기
                    if (bolSaveHeader == true)
                    {
                        trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted);
                        //파라미터 저장
                        DataTable dtParameter = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                        sql.mfAddParamDataRow(dtParameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strCurEquipCode, 20);
                        sql.mfAddParamDataRow(dtParameter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, strWriteID, 20);
                        sql.mfAddParamDataRow(dtParameter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteDate, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlan", dtParameter); //실행용
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlan", dtParameter);   //디버깅용

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리결과 값 처리 
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                        else
                        {
                            trans.Commit();
                        }
                    }



                }



                //if (ErrRtn.ErrNum.Equals(0))
                //    trans.Commit();

                SqlBulkCopy bc = new SqlBulkCopy(sql.SqlCon);
                bc.DestinationTableName = "EQUPMPlanD";

                bc.BulkCopyTimeout = 600;
                bc.ColumnMappings.Add("PlantCode", "PlantCode");
                bc.ColumnMappings.Add("PlanYear", "PlanYear");
                bc.ColumnMappings.Add("EquipCode", "EquipCode");
                bc.ColumnMappings.Add("Seq", "Seq");
                bc.ColumnMappings.Add("PMPlanDate", "PMPlanDate");
                bc.ColumnMappings.Add("PMInspectName", "PMInspectName");
                bc.ColumnMappings.Add("PMInspectRegion", "PMInspectRegion");
                bc.ColumnMappings.Add("PMInspectCriteria", "PMInspectCriteria");
                bc.ColumnMappings.Add("PMPeriodCode", "PMPeriodCode");
                bc.ColumnMappings.Add("PMMethod", "PMMethod");
                bc.ColumnMappings.Add("FaultFixMethod", "FaultFixMethod");
                bc.ColumnMappings.Add("StandardManCount", "StandardManCount");
                bc.ColumnMappings.Add("StandardTime", "StandardTime");
                bc.ColumnMappings.Add("LevelCode", "LevelCode");
                bc.ColumnMappings.Add("PMResultCode", "PMResultCode");
                bc.ColumnMappings.Add("ImageFile", "ImageFile");
                bc.ColumnMappings.Add("UnitDesc", "UnitDesc");
                bc.ColumnMappings.Add("PMResultValue", "PMResultValue");
                bc.ColumnMappings.Add("MeasureValueFlag", "MeasureValueFlag");
                bc.WriteToServer(dtPMPlanD);

                #endregion

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 예방점검정보 저장(작업자별)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strEquipWorkID">작업자아이디</param>
        /// <param name="strWriteID">작성자</param>
        /// <param name="strWriteDate">작성일</param>
        /// <param name="strPMApplyDate">계획적용시작일</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanEquipWorker(string strPlantCode, string strPlanYear, string strEquipWorkID, string strWriteID, string strWriteDate, string strPMApplyDate, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();  //실행용

                //////////설비그룹에 대한 설비리스트를 가지고 온다. => 추가//
                ////////QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                //////////QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD(m_SqlConnDebug.ConnectionString);  //디버깅용
                ////////DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMDForEquipWorker(strPlantCode, strEquipWorkID, sql.SqlCon);
                DataTable dtParameterPMD = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, strEquipWorkID, 20);
                //프로시저 호출
                DataTable dtEquipPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMD_EquipWorker", dtParameterPMD); //실행용

                if (dtEquipPMD.Rows.Count <= 0)
                {                    
                    return strErrRtn;
                }

                PMPlanD clsPMPlanD = new PMPlanD();
                DataTable dtEquipCheckPMD = new DataTable();

                //파라미터정보저장
                DataTable dtParam_Check = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam_Check, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam_Check, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, strEquipWorkID, 20);


                //점검계획조정 조회 프로시저 실행
                dtEquipCheckPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_CheckResult_EquipWorker", dtParam_Check);
              


                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();    //실행용
                //trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

                strErrRtn = mfDeletePMPlanEquipWorker(strPlantCode, strEquipWorkID, strPlanYear, strPMApplyDate, sql.SqlCon, trans);    //실행용
                //strErrRtn = mfDeletePMPlanEquipWorker(strPlantCode, strEquipWorkID, strPlanYear, m_SqlConnDebug, trans);      //디버깅용
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }
                
            
                //예방점검계획을 생성한다.
                DataTable dtPMPlanD = GeneratePMSchedule(strPlantCode, strPlanYear, strPMApplyDate, dtEquipPMD, dtEquipCheckPMD);

                string strCurEquipCode = "";
                bool bolSaveHeader = false;

                //==== 여기부터 시작 (2011.09.27) =====
                //생성한 예방점검계획을 저장한다.
                for (int i = 0; i < dtPMPlanD.Rows.Count; i++)
                {
                    if (strCurEquipCode == "")
                    {
                        strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
                        bolSaveHeader = true;

                        trans = sql.SqlCon.BeginTransaction(); 
                    }
                    else
                    {
                        if (strCurEquipCode != dtPMPlanD.Rows[i]["EquipCode"].ToString())
                        {
                            strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
                            bolSaveHeader = true;

                            trans.Commit();

                            trans = sql.SqlCon.BeginTransaction(); 
                        }
                        else
                            bolSaveHeader = false;
                    }

                    //예방점검 헤더 저장하기
                    if (bolSaveHeader == true)
                    {
                        //파라미터 저장
                        DataTable dtParameter = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                        sql.mfAddParamDataRow(dtParameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strCurEquipCode, 20);
                        sql.mfAddParamDataRow(dtParameter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, strWriteID, 20);
                        sql.mfAddParamDataRow(dtParameter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteDate, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlan", dtParameter); //실행용
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlan", dtParameter);   //디버깅용

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리결과 값 처리
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //////////예방점검 상세 저장하기
                    //////////파라미터 저장
                    ////////DataTable dtParameterD = sql.mfSetParamDataTable();
                    ////////sql.mfAddParamDataRow(dtParameterD, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["EquipCode"].ToString(), 20);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtPMPlanD.Rows[i]["Seq"].ToString());

                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPlanDate"].ToString(), 10);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectName", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectName"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectRegion", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectRegion"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectCriteria", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectCriteria"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMPeriodCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPeriodCode"].ToString(), 3);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strPMMethod", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMMethod"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strFaultFixMethod", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["FaultFixMethod"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_intStandardManCount", ParameterDirection.Input, SqlDbType.Int, dtPMPlanD.Rows[i]["StandardManCount"].ToString(), 10);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_dblStandardTime", ParameterDirection.Input, SqlDbType.Decimal, dtPMPlanD.Rows[i]["StandardTime"].ToString());
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strLevelCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["LevelCode"].ToString(), 1);

                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strImageFile", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanD.Rows[i]["ImageFile"].ToString(), 1000);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strUnitDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanD.Rows[i]["UnitDesc"].ToString(), 100);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strMeasureValueFlag", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["MeasureValueFlag"].ToString(), 1);

                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    ////////sql.mfAddParamDataRow(dtParameterD, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //////////프로시저 실행
                    ////////strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlanD", dtParameterD);
                    //////////strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlanD", dtParameterD);
                    ////////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //////////처리결과 값 처리
                    ////////if (ErrRtn.ErrNum != 0)
                    ////////{
                    ////////    trans.Rollback();
                    ////////    return strErrRtn;
                    ////////}
                }
           

                if(ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                SqlBulkCopy bc = new SqlBulkCopy(sql.SqlCon);
                bc.DestinationTableName = "EQUPMPlanD";
                bc.ColumnMappings.Add("PlantCode", "PlantCode");
                bc.ColumnMappings.Add("PlanYear", "PlanYear");
                bc.ColumnMappings.Add("EquipCode", "EquipCode");
                bc.ColumnMappings.Add("Seq", "Seq");
                bc.ColumnMappings.Add("PMPlanDate", "PMPlanDate");
                bc.ColumnMappings.Add("PMInspectName", "PMInspectName");
                bc.ColumnMappings.Add("PMInspectRegion", "PMInspectRegion");
                bc.ColumnMappings.Add("PMInspectCriteria", "PMInspectCriteria");
                bc.ColumnMappings.Add("PMPeriodCode", "PMPeriodCode");
                bc.ColumnMappings.Add("PMMethod", "PMMethod");
                bc.ColumnMappings.Add("FaultFixMethod", "FaultFixMethod");
                bc.ColumnMappings.Add("StandardManCount", "StandardManCount");
                bc.ColumnMappings.Add("StandardTime", "StandardTime");
                bc.ColumnMappings.Add("LevelCode", "LevelCode");
                bc.ColumnMappings.Add("PMResultCode", "PMResultCode");
                bc.ColumnMappings.Add("ImageFile", "ImageFile");
                bc.ColumnMappings.Add("UnitDesc", "UnitDesc");
                bc.ColumnMappings.Add("PMResultValue", "PMResultValue");
                bc.ColumnMappings.Add("MeasureValueFlag", "MeasureValueFlag");
                bc.BulkCopyTimeout = 600;
                bc.WriteToServer(dtPMPlanD);
                
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// PM기준정보를 이용하여 예방점검을 생성한다.
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strPlanYear"></param>
        /// <param name="strPMApplyDate"></param>
        /// <param name="dtEquipPMD"></param>
        /// <param name="dtEquipCheckPMD"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable GeneratePMSchedule(string strPlantCode, string strPlanYear, string strPMApplyDate, DataTable dtEquipPMD, DataTable dtEquipCheckPMD)
        {
            //예방점검계획상세 저장 DataTable 선언//
            DataTable dtPMPlanD = new DataTable();
            dtPMPlanD.Columns.Add("PlantCode", typeof(string));
            dtPMPlanD.Columns.Add("PlanYear", typeof(string));
            dtPMPlanD.Columns.Add("EquipCode", typeof(string));
            dtPMPlanD.Columns.Add("Seq", typeof(string));
            dtPMPlanD.Columns.Add("PMPlanDate", typeof(string));
            dtPMPlanD.Columns.Add("PMInspectName", typeof(string));
            dtPMPlanD.Columns.Add("PMInspectRegion", typeof(string));
            dtPMPlanD.Columns.Add("PMInspectCriteria", typeof(string));
            dtPMPlanD.Columns.Add("PMPeriodCode", typeof(string));
            dtPMPlanD.Columns.Add("PMMethod", typeof(string));
            dtPMPlanD.Columns.Add("FaultFixMethod", typeof(string));
            dtPMPlanD.Columns.Add("StandardManCount", typeof(int));
            dtPMPlanD.Columns.Add("StandardTime", typeof(double));
            dtPMPlanD.Columns.Add("LevelCode", typeof(string));
            dtPMPlanD.Columns.Add("PMResultCode", typeof(string));
            dtPMPlanD.Columns.Add("ImageFile", typeof(string));
            dtPMPlanD.Columns.Add("UnitDesc", typeof(string));
            dtPMPlanD.Columns.Add("PMResultValue", typeof(string));
            dtPMPlanD.Columns.Add("MeasureValueFlag", typeof(string));


            try
            {

                DateTime datStartDate = Convert.ToDateTime(strPMApplyDate);
                DateTime datNextDate;
                DateTime datEndDate = Convert.ToDateTime(strPlanYear + "-12-31");

                int intSeq = 1;
                int intDiff = 0;
                int intQua = 0;
                DateTime datMonthFirstDate;

                string strPMWeekDay = "";
                string strPMMonthWeek = "";
                string strPMMonthDay = "";
                string strPMYearMonth = "";
                string strPMYearWeek = "";
                string strPMYearDay = "";

                //예장점검을 생성한다.
                for (int i = 0; i < dtEquipPMD.Rows.Count; i++)
                {
                    string strTime = DateTime.Now.ToString("yyyyMMddHHMMss");

                    //설비정보에서 상세일정이 없는 경우 처리
                    if (dtEquipPMD.Rows[i]["PMWeekDay"].ToString() == "")
                        strPMWeekDay = Convert.ToString((int)DayOfWeek.Monday+1);     //없는 경우 월요일로 지정
                    else
                        strPMWeekDay = dtEquipPMD.Rows[i]["PMWeekDay"].ToString();

                    if (dtEquipPMD.Rows[i]["PMMonthWeek"].ToString() == "")
                        strPMMonthWeek = "1";                             //없는 경우 1주차로 지정
                    else
                        strPMMonthWeek = dtEquipPMD.Rows[i]["PMMonthWeek"].ToString();

                    if (dtEquipPMD.Rows[i]["PMMonthDay"].ToString() == "")
                        strPMMonthDay = Convert.ToString((int)DayOfWeek.Monday+1);     //없는 경우 월요일로 지정
                    else
                        strPMMonthDay = dtEquipPMD.Rows[i]["PMMonthDay"].ToString();

                    if (dtEquipPMD.Rows[i]["PMYearMonth"].ToString() == "")
                    {
                        strPMYearMonth = "1";                           //없는 경우 월초로 지정
                        //if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "QUA")         //분기인 경우
                        //else if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "HAF")    //반기인 경우
                        //else if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "YEA")    //년간인 경우
                    }
                    else
                        strPMYearMonth = dtEquipPMD.Rows[i]["PMYearMonth"].ToString();

                    if (dtEquipPMD.Rows[i]["PMYearWeek"].ToString() == "")
                        strPMYearWeek = "1";                             //없는 경우 1주차로 지정
                    else
                        strPMYearWeek = dtEquipPMD.Rows[i]["PMYearWeek"].ToString();

                    if (dtEquipPMD.Rows[i]["PMYearDay"].ToString() == "")
                        strPMYearDay = Convert.ToString((int)DayOfWeek.Monday+1);     //없는 경우 월요일로 지정
                    else
                        strPMYearDay = dtEquipPMD.Rows[i]["PMYearDay"].ToString();


                    //일간점검(DAY)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "DAY")
                    {
                        while (datNextDate <= datEndDate)
                        {
                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'DAY' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                //drEquipPMD["Seq"] =  intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            //다음날을 구함.
                            datNextDate = datNextDate.AddDays(1);
                            intSeq++;
                            
                        }
                    }

                    //주간점검(WEK)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "WEK")
                    {
                        // 요일(1:일요일 7:토요일)
                        // 설비정보에서 지정한 요일과 시작일의 요일의 차이 계산하여 계획시작일을 지정함.
                        intDiff = Convert.ToInt32(strPMWeekDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                        ////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMWeekDay"].ToString()) -
                        ////              Convert.ToInt32(datNextDate.DayOfWeek) + 1;

                        if (intDiff < 0)
                            datNextDate = datNextDate.AddDays(7 + intDiff);
                        else
                            datNextDate = datNextDate.AddDays(intDiff);

                        while (datNextDate <= datEndDate)
                        {
                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'WEK' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                //drEquipPMD["Seq"] = intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            //다음주를 구함.
                            datNextDate = datNextDate.AddDays(7);
                            intSeq++;
                        }
                    }

                    //월간점검(MON)인 경우
                    datNextDate = datStartDate;
                    int intStartDateWeek = 1;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "MON")
                    {
                        while (true)
                        {
                            //1. 적용일의 월에서 첫주차 일자구하기
                            //-- 1-1. 적용일의 월초(1일)이 일요일인 경우 다음 적용일 = 월초(1일)
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            if (datMonthFirstDate.DayOfWeek == DayOfWeek.Sunday)
                                datNextDate = datMonthFirstDate;
                            //intStartDateWeek = 1; //(datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            else
                                datNextDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            //intStartDateWeek = 1; // (datNextDate.DayOfYear - datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek)).DayOfYear) / 7 + 1;

                            // 2. NextDate 설정
                            //-- 2-1. 적용일주차 = PM월주차 인 경우 (현재 주차인 날짜로 NextDate설정)
                            //if (intStartDateWeek == Convert.ToInt32(dtEquip.Rows[i]["PMMonthWeek"].ToString()))
                            //datNextDate = datNextDate;
                            //-- 2-2. 적용일주차 < PM월주차 인 경우 (PM월주차 - 적용일주차만큼 일자를 이동하여 NextDate설정)

                            if (intStartDateWeek < Convert.ToInt32(strPMMonthWeek))
                                datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMMonthWeek) - intStartDateWeek));
                            //-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            else if (intStartDateWeek > Convert.ToInt32(strPMMonthWeek))
                            {
                                datNextDate = datMonthFirstDate.AddMonths(1);
                                if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMMonthWeek) - 1));
                                else
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMMonthWeek) - 1));
                            }
                            intDiff = Convert.ToInt32(strPMMonthDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            ////////if (intStartDateWeek < Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()))
                            ////////    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()) - intStartDateWeek));
                            //////////-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            ////////else if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()))
                            ////////{
                            ////////    datNextDate = datMonthFirstDate.AddMonths(1);
                            ////////    if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                            ////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()) - 1));
                            ////////    else
                            ////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()) - 1));
                            ////////}
                            ////////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthDay"].ToString()) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            datNextDate = datNextDate.AddDays(intDiff);

                            //마지막날이 넘어가면 빠져나간다.
                            if (datNextDate > datEndDate)
                                break;

                            if (datNextDate > datStartDate)
                            {
                                if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'MON' ").Count() <= 0)
                                {
                                    DataRow drEquipPMD;
                                    drEquipPMD = dtPMPlanD.NewRow();
                                    drEquipPMD["PlantCode"] = strPlantCode;
                                    drEquipPMD["PlanYear"] = strPlanYear;
                                    drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                    //drEquipPMD["Seq"] = intSeq;
                                    drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                    drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                    drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                    drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                    drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                    drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                    drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                    drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                    drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                    drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                    drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                    drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                    drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                    drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                    dtPMPlanD.Rows.Add(drEquipPMD);
                                    intSeq++;
                                }
                            }
                            //다음달을 구함.
                            datNextDate = datNextDate.AddMonths(1);
                        }
                    }

                    //분기점검(QUA)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "QUA")
                    {
                        //1. 적용일의 분기월이 분기월 이후인 경우 다음 분기의 분기초월로 넘김
                        //-- 1-1. 적용일의 분기월을 구함.
                        if (datNextDate.Month % 3 > 0) intQua = datNextDate.Month % 3;
                        else intQua = 3;

                        //-- 1-2. 적용일의 월 주차를 구함.
                        datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                        //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                        if (datMonthFirstDate.DayOfWeek != DayOfWeek.Sunday)
                            datMonthFirstDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                        intStartDateWeek = (datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;

                        //-- 1-2-1. 적용일의 분기월 > PM분기월 인 경우 : 다음 분기 월초로 보냄.
                        //-- 1-2-2. 적용일 분기월의 주차 > PM분기월의 주차 인 경우 : 다음 분기 월초로 보냄.
                        //-- 1-2-3. 적용일 분기월의 주차의 요일 > PM분기월의 주차의 요일 인 경우 : 다음 분기 월초로 보냄
                        bool bolQuaSkip = false;
                        
                        //지정한 월을 분기 내의 월순서로 변환
                        string strPMYearMonth_ToQua = "";
                        if (strPMYearMonth == "1" || strPMYearMonth == "4" || strPMYearMonth == "7" || strPMYearMonth == "10")
                            strPMYearMonth_ToQua = "1";
                        else if (strPMYearMonth == "2" || strPMYearMonth == "5" || strPMYearMonth == "8" || strPMYearMonth == "11")
                            strPMYearMonth_ToQua = "2";
                        else if (strPMYearMonth == "3" || strPMYearMonth == "6" || strPMYearMonth == "9" || strPMYearMonth == "12")
                            strPMYearMonth_ToQua = "3";

                        //-- 이하는 strPMYearMonth -> strPMYearMonth_ToQua 로 변경함 --//
                        if (intQua > Convert.ToInt32(strPMYearMonth_ToQua))
                        //if (intQua > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()))
                            bolQuaSkip = true;
                        else
                        {
                            if (intStartDateWeek > Convert.ToInt32(strPMYearWeek))
                                //if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                                bolQuaSkip = true;
                            else
                            {
                                if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(strPMYearDay))
                                //if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString())))
                                    bolQuaSkip = true;
                            }
                        }
                        if (bolQuaSkip == true)
                        //if (intQua > Convert.ToInt32(dtEquip.Rows[i]["PMYearMonth"].ToString())
                        //    || intStartDateWeek > Convert.ToInt32(dtEquip.Rows[i]["PMYearWeek"].ToString())
                        //    || Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(dtEquip.Rows[i]["PMYearDay"].ToString())))
                        {
                            if (datNextDate.Month == 1 || datNextDate.Month == 2 || datNextDate.Month == 3)
                            {
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-04-01");
                                datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToQua) - 1); //추가
                            }
                            else if (datNextDate.Month == 4 || datNextDate.Month == 5 || datNextDate.Month == 6)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-07-01");
                            else if (datNextDate.Month == 7 || datNextDate.Month == 8 || datNextDate.Month == 9)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-10-01");
                            else if (datNextDate.Month == 10 || datNextDate.Month == 11 || datNextDate.Month == 12)
                                datNextDate = Convert.ToDateTime(datNextDate.AddYears(1).Year + "-01-01");
                        }
                        else
                        {
                            datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToQua) - datNextDate.Month);
                            //datNextDate = datNextDate.AddMonths(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()) - datNextDate.Month);
                        }

                        while (true)
                        {

                            //2. 월간계획 로직과 동일//
                            //1. 적용일의 월에서 첫주차 일자구하기
                            //-- 1-1. 적용일의 월초(1일)이 일요일인 경우 다음 적용일 = 월초(1일)
                            intStartDateWeek = 1;
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            if (datMonthFirstDate.DayOfWeek == DayOfWeek.Sunday)
                                datNextDate = datMonthFirstDate;
                            //intStartDateWeek = 1; //(datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            else
                                datNextDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            //intStartDateWeek = 1; // (datNextDate.DayOfYear - datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek)).DayOfYear) / 7 + 1;

                            // 2. NextDate 설정
                            //-- 2-1. 적용일주차 = PM월주차 인 경우 (현재 주차인 날짜로 NextDate설정)
                            //if (intStartDateWeek == Convert.ToInt32(dtEquip.Rows[i]["PMMonthWeek"].ToString()))
                            //datNextDate = datNextDate;
                            //-- 2-2. 적용일주차 < PM월주차 인 경우 (PM월주차 - 적용일주차만큼 일자를 이동하여 NextDate설정)

                            if (intStartDateWeek < Convert.ToInt32(strPMYearWeek))
                                datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - intStartDateWeek));
                            //-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            else if (intStartDateWeek > Convert.ToInt32(strPMYearWeek))
                            {
                                datNextDate = datMonthFirstDate.AddMonths(1);
                                if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - 1));
                                else
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - 1));
                            }
                            intDiff = Convert.ToInt32(strPMYearDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            ////////if (intStartDateWeek < Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            ////////    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - intStartDateWeek));
                            //////////-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            ////////else if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            ////////{
                            ////////    datNextDate = datMonthFirstDate.AddMonths(1);
                            ////////    if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                            ////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            ////////    else
                            ////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            ////////}
                            ////////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString()) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            datNextDate = datNextDate.AddDays(intDiff);

                            //마지막날이 넘어가면 빠져나간다.
                            if (datNextDate > datEndDate)
                                break;

                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'QUA' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                // drEquipPMD["Seq"] = intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            //다음분기의 월초를 구함.
                            //datNextDate = datNextDate.AddMonths(3);
                            if (datNextDate.Month == 1 || datNextDate.Month == 2 || datNextDate.Month == 3)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-04-01");
                            else if (datNextDate.Month == 4 || datNextDate.Month == 5 || datNextDate.Month == 6)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-07-01");
                            else if (datNextDate.Month == 7 || datNextDate.Month == 8 || datNextDate.Month == 9)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-10-01");
                            else if (datNextDate.Month == 10 || datNextDate.Month == 11 || datNextDate.Month == 12)
                                datNextDate = Convert.ToDateTime(datNextDate.AddYears(1).Year + "-01-01");
                            datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToQua) - 1);
                            //datNextDate = datNextDate.AddMonths(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()) - 1);
                            intSeq++;
                        }
                    }

                    //반기점검(HAF)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "HAF")
                    {
                        //1. 적용일의 반기월이 반기월 이후인 경우 다음 반기의 반기초월로 넘김
                        //-- 1-1. 적용일의 반기월을 구함.
                        if (datNextDate.Month % 6 > 0) intQua = datNextDate.Month % 6;
                        else intQua = 6;

                        //-- 1-2. 적용일의 월 주차를 구함.
                        datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                        //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                        if (datMonthFirstDate.DayOfWeek != DayOfWeek.Sunday)
                            datMonthFirstDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                        intStartDateWeek = (datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;

                        //-- 1-2-1. 적용일의 반기월 > PM반기월 인 경우 : 다음 반기 월초로 보냄.
                        //-- 1-2-2. 적용일 반기월의 주차 > PM반기월의 주차 인 경우 : 다음 반기 월초로 보냄.
                        //-- 1-2-3. 적용일 반기월의 주차의 요일 > PM반기월의 주차의 요일 인 경우 : 다음 반기 월초로 보냄
                        bool bolQuaSkip = false;
                        
                        //지정한 월을 반기 내의 월순서로 변환
                        string strPMYearMonth_ToHalf = "";
                        if (strPMYearMonth == "1" || strPMYearMonth == "7")
                            strPMYearMonth_ToHalf = "1";
                        else if (strPMYearMonth == "2" || strPMYearMonth == "8")
                            strPMYearMonth_ToHalf = "2";
                        else if (strPMYearMonth == "3" || strPMYearMonth == "9")
                            strPMYearMonth_ToHalf = "3";
                        else if (strPMYearMonth == "4" || strPMYearMonth == "10")
                            strPMYearMonth_ToHalf = "4";
                        else if (strPMYearMonth == "5" || strPMYearMonth == "11")
                            strPMYearMonth_ToHalf = "5";
                        else if (strPMYearMonth == "6" || strPMYearMonth == "12")
                            strPMYearMonth_ToHalf = "6";

                        //-- 이하는 strPMYearMonth -> strPMYearMonth_ToHalf 로 변경함 --//
                        if (intQua > Convert.ToInt32(strPMYearMonth_ToHalf))
                        //if (intQua > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()))
                            bolQuaSkip = true;
                        else
                        {
                            if (intStartDateWeek > Convert.ToInt32(strPMYearWeek))
                            //if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                                bolQuaSkip = true;
                            else
                            {
                                if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(strPMYearDay)))
                                //if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString())))
                                    bolQuaSkip = true;
                            }
                        }
                        if (bolQuaSkip == true)
                        //if (intQua > Convert.ToInt32(dtEquip.Rows[i]["PMYearMonth"].ToString())
                        //    || intStartDateWeek > Convert.ToInt32(dtEquip.Rows[i]["PMYearWeek"].ToString())
                        //    || Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(dtEquip.Rows[i]["PMYearDay"].ToString())))
                        {
                            if (datNextDate.Month == 1 || datNextDate.Month == 2 || datNextDate.Month == 3 || datNextDate.Month == 4 || datNextDate.Month == 5 || datNextDate.Month == 6)
                            {
                                //if (datNextDate.Month > Convert.ToInt32(strPMYearMonth_ToHalf))
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-07-01");
                                datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToHalf) - 1);    //추가
                            }
                            else if (datNextDate.Month == 7 || datNextDate.Month == 8 || datNextDate.Month == 9 || datNextDate.Month == 10 || datNextDate.Month == 11 || datNextDate.Month == 12)
                                datNextDate = Convert.ToDateTime(datNextDate.AddYears(1).Year + "-01-01");
                        }
                        else
                        {
                            datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToHalf) - datNextDate.Month);
                            //datNextDate = datNextDate.AddMonths(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()) - datNextDate.Month);
                        }

                        while (true)
                        {

                            //2. 월간계획 로직과 동일//
                            //1. 적용일의 월에서 첫주차 일자구하기
                            //-- 1-1. 적용일의 월초(1일)이 일요일인 경우 다음 적용일 = 월초(1일)
                            intStartDateWeek = 1;
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            if (datMonthFirstDate.DayOfWeek == DayOfWeek.Sunday)
                                datNextDate = datMonthFirstDate;
                            //intStartDateWeek = 1; //(datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            else
                                datNextDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            //intStartDateWeek = 1; // (datNextDate.DayOfYear - datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek)).DayOfYear) / 7 + 1;

                            // 2. NextDate 설정
                            //-- 2-1. 적용일주차 = PM월주차 인 경우 (현재 주차인 날짜로 NextDate설정)
                            //if (intStartDateWeek == Convert.ToInt32(dtEquip.Rows[i]["PMMonthWeek"].ToString()))
                            //datNextDate = datNextDate;
                            //-- 2-2. 적용일주차 < PM월주차 인 경우 (PM월주차 - 적용일주차만큼 일자를 이동하여 NextDate설정)
                            if (intStartDateWeek < Convert.ToInt32(strPMYearWeek))
                                datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - intStartDateWeek));
                            //-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            else if (intStartDateWeek > Convert.ToInt32(strPMYearWeek))
                            {
                                datNextDate = datMonthFirstDate.AddMonths(1);
                                if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - 1));
                                else
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - 1));
                            }
                            intDiff = Convert.ToInt32(strPMYearDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            //////////if (intStartDateWeek < Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            //////////    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - intStartDateWeek));
                            ////////////-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            //////////else if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            //////////{
                            //////////    datNextDate = datMonthFirstDate.AddMonths(1);
                            //////////    if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                            //////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            //////////    else
                            //////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            //////////}
                            //////////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString()) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            datNextDate = datNextDate.AddDays(intDiff);

                            //마지막날이 넘어가면 빠져나간다.
                            if (datNextDate > datEndDate)
                                break;

                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'HAF' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                //drEquipPMD["Seq"] = intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            //다음분기의 월초를 구함.
                            if (datNextDate.Month == 1 || datNextDate.Month == 2 || datNextDate.Month == 3 || datNextDate.Month == 4 || datNextDate.Month == 5 || datNextDate.Month == 6)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-07-01");
                            else if (datNextDate.Month == 7 || datNextDate.Month == 8 || datNextDate.Month == 9 || datNextDate.Month == 10 || datNextDate.Month == 11 || datNextDate.Month == 12)
                                datNextDate = Convert.ToDateTime(datNextDate.AddYears(1).Year + "-01-01");
                            datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToHalf) - 1);
                            //datNextDate = datNextDate.AddMonths(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()) - 1);
                            intSeq++;
                        }
                    }

                    //년간점검(YEA)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "YEA")
                    {
                        bool bolQuaSkip = false;
                        //적용일의 월 > PM년월 인 경우 : 다음 해로 보냄
                        if (datNextDate.Month > Convert.ToInt32(strPMYearMonth))
                        //if (datNextDate.Month > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()))
                            bolQuaSkip = true;
                        else
                        {
                            datNextDate = Convert.ToDateTime(datNextDate.Year.ToString() + "-" + strPMYearMonth + "-" + datNextDate.Day.ToString());
                            //datNextDate = Convert.ToDateTime(datNextDate.Year.ToString() + "-" + dtEquipPMD.Rows[i]["PMYearMonth"].ToString() + "-" + datNextDate.Day.ToString());

                            //-- 1-2. 적용일의 월 주차를 구함.
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            if (datMonthFirstDate.DayOfWeek != DayOfWeek.Sunday)
                                datMonthFirstDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            intStartDateWeek = (datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;

                            if (intStartDateWeek > Convert.ToInt32(strPMYearWeek))
                            //if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                                bolQuaSkip = true;
                            else
                            {
                                if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(strPMYearDay)))
                                //if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString())))
                                    bolQuaSkip = true;
                            }
                        }
                        if (bolQuaSkip == false)
                        {
                            //2. 월간계획 로직과 동일//
                            //1. 적용일의 월에서 첫주차 일자구하기
                            //-- 1-1. 적용일의 월초(1일)이 일요일인 경우 다음 적용일 = 월초(1일)
                            intStartDateWeek = 1;
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            if (datMonthFirstDate.DayOfWeek == DayOfWeek.Sunday)
                                datNextDate = datMonthFirstDate;
                            //intStartDateWeek = 1; //(datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            else
                                datNextDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            //intStartDateWeek = 1; // (datNextDate.DayOfYear - datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek)).DayOfYear) / 7 + 1;

                            // 2. NextDate 설정
                            //-- 2-1. 적용일주차 = PM월주차 인 경우 (현재 주차인 날짜로 NextDate설정)
                            //if (intStartDateWeek == Convert.ToInt32(dtEquip.Rows[i]["PMMonthWeek"].ToString()))
                            //datNextDate = datNextDate;
                            //-- 2-2. 적용일주차 < PM월주차 인 경우 (PM월주차 - 적용일주차만큼 일자를 이동하여 NextDate설정)
                            if (intStartDateWeek < Convert.ToInt32(strPMYearWeek))
                                datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - intStartDateWeek));
                            //-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            else if (intStartDateWeek > Convert.ToInt32(strPMYearWeek))
                            {
                                datNextDate = datMonthFirstDate.AddMonths(1);
                                if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - 1));
                                else
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - 1));
                            }
                            intDiff = Convert.ToInt32(strPMYearDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            //////if (intStartDateWeek < Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            //////    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - intStartDateWeek));
                            ////////-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            //////else if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            //////{
                            //////    datNextDate = datMonthFirstDate.AddMonths(1);
                            //////    if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                            //////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            //////    else
                            //////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            //////}
                            //////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString()) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            datNextDate = datNextDate.AddDays(intDiff);

                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'YEA' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                //drEquipPMD["Seq"] = intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            intSeq++;
                        }
                    }
                }

                dtPMPlanD.Rows[3].Delete();

                return dtPMPlanD;
                
            }
            catch (System.Exception ex)
            {
                return dtPMPlanD;
            }
            finally
            {
            }
        }

        /// <summary>
        /// PM기준정보를 이용하여 예방점검을 생성한다.
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strPlanYear"></param>
        /// <param name="strPMApplyDate"></param>
        /// <param name="dtEquipPMD"></param>
        /// <param name="dtEquipCheckPMD"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable GeneratePMSchedule_PSTS(string strPlantCode, string strPlanYear, string strPMApplyDate, DataTable dtEquipPMD, DataTable dtEquipCheckPMD)
        {
            //예방점검계획상세 저장 DataTable 선언//
            DataTable dtPMPlanD = new DataTable();
            dtPMPlanD.Columns.Add("PlantCode", typeof(string));
            dtPMPlanD.Columns.Add("PlanYear", typeof(string));
            dtPMPlanD.Columns.Add("EquipCode", typeof(string));
            dtPMPlanD.Columns.Add("Seq", typeof(string));
            dtPMPlanD.Columns.Add("PMPlanDate", typeof(string));
            dtPMPlanD.Columns.Add("PMInspectName", typeof(string));
            dtPMPlanD.Columns.Add("PMInspectRegion", typeof(string));
            dtPMPlanD.Columns.Add("PMInspectCriteria", typeof(string));
            dtPMPlanD.Columns.Add("PMPeriodCode", typeof(string));
            dtPMPlanD.Columns.Add("PMMethod", typeof(string));
            dtPMPlanD.Columns.Add("FaultFixMethod", typeof(string));
            dtPMPlanD.Columns.Add("StandardManCount", typeof(int));
            dtPMPlanD.Columns.Add("StandardTime", typeof(double));
            dtPMPlanD.Columns.Add("LevelCode", typeof(string));
            dtPMPlanD.Columns.Add("PMResultCode", typeof(string));
            dtPMPlanD.Columns.Add("ImageFile", typeof(string));
            dtPMPlanD.Columns.Add("UnitDesc", typeof(string));
            dtPMPlanD.Columns.Add("PMResultValue", typeof(string));
            dtPMPlanD.Columns.Add("MeasureValueFlag", typeof(string));


            try
            {

                DateTime datStartDate = Convert.ToDateTime(strPMApplyDate);
                DateTime datNextDate;
                DateTime datEndDate = Convert.ToDateTime(strPlanYear + "-12-31");

                int intSeq = 1;
                int intDiff = 0;
                int intQua = 0;
                DateTime datMonthFirstDate;

                string strPMWeekDay = "";
                string strPMMonthWeek = "";
                string strPMMonthDay = "";
                string strPMQuaMonth = "";
                string strPMQuaWeek = "";
                string strPMQuaDay = "";
                string strPMHafMonth = "";
                string strPMHafWeek = "";
                string strPMHafDay = "";
                string strPMYearMonth = "";
                string strPMYearWeek = "";
                string strPMYearDay = "";
                
                //예장점검을 생성한다.
                for (int i = 0; i < dtEquipPMD.Rows.Count; i++)
                {
                    string strTime = DateTime.Now.ToString("yyyyMMddHHMMss");
                    string strEquipCode = dtEquipPMD.Rows[i]["EquipCode"].ToString().Trim();
                    //설비정보에서 상세일정이 없는 경우 처리
                    if (dtEquipPMD.Rows[i]["PMWeekDay"].ToString() == "")
                        strPMWeekDay = Convert.ToString((int)DayOfWeek.Monday + 1);     //없는 경우 월요일로 지정
                    else
                        strPMWeekDay = dtEquipPMD.Rows[i]["PMWeekDay"].ToString();

                    if (dtEquipPMD.Rows[i]["PMMonthWeek"].ToString() == "")
                        strPMMonthWeek = "1";                             //없는 경우 1주차로 지정
                    else
                        strPMMonthWeek = dtEquipPMD.Rows[i]["PMMonthWeek"].ToString();

                    if (dtEquipPMD.Rows[i]["PMMonthDay"].ToString() == "")
                        strPMMonthDay = Convert.ToString((int)DayOfWeek.Monday + 1);     //없는 경우 월요일로 지정
                    else
                        strPMMonthDay = dtEquipPMD.Rows[i]["PMMonthDay"].ToString();

                    // --------------------------- 분기 ------------------------------- //
                    //없는 경우 월초로 지정
                    strPMQuaMonth = dtEquipPMD.Rows[i]["PMQuarterMonth"].ToString().Equals(string.Empty) ? "1" : dtEquipPMD.Rows[i]["PMQuarterMonth"].ToString();
                    
                    //없는 경우 1주차로 지정
                    strPMQuaWeek = dtEquipPMD.Rows[i]["PMQuarterWeek"].ToString().Equals(string.Empty) ? "1" : dtEquipPMD.Rows[i]["PMQuarterWeek"].ToString();
                    
                    //없는 경우 월요일로 지정
                    strPMQuaDay = dtEquipPMD.Rows[i]["PMQuarterDay"].ToString().Equals(string.Empty) ? Convert.ToString((int)DayOfWeek.Monday + 1) : dtEquipPMD.Rows[i]["PMQuarterDay"].ToString();     

                    // --------------------------- 반기 ------------------------------- //

                    //없는 경우 월초로 지정
                    strPMHafMonth = dtEquipPMD.Rows[i]["PMHalfMonth"].ToString().Equals(string.Empty) ? "1" : dtEquipPMD.Rows[i]["PMHalfMonth"].ToString();

                    //없는 경우 1주차로 지정
                    strPMHafWeek = dtEquipPMD.Rows[i]["PMHalfWeek"].ToString().Equals(string.Empty) ? "1" : dtEquipPMD.Rows[i]["PMHalfWeek"].ToString();                             
                    
                    //없는 경우 월요일로 지정
                    strPMHafDay = dtEquipPMD.Rows[i]["PMHalfDay"].ToString().Equals(string.Empty) ? Convert.ToString((int)DayOfWeek.Monday + 1) : dtEquipPMD.Rows[i]["PMHalfDay"].ToString();     

                    // --------------------------- 년간 ------------------------------- //

                    //없는 경우 월초로 지정
                    strPMYearMonth = dtEquipPMD.Rows[i]["PMYearMonth"].ToString().Equals(string.Empty) ? "1" : dtEquipPMD.Rows[i]["PMYearMonth"].ToString();                           

                    //없는 경우 1주차로 지정
                    strPMYearWeek = dtEquipPMD.Rows[i]["PMYearWeek"].ToString().Equals(string.Empty) ? "1" : dtEquipPMD.Rows[i]["PMYearWeek"].ToString();                             

                    //없는 경우 월요일로 지정
                    strPMYearDay = dtEquipPMD.Rows[i]["PMYearDay"].ToString().Equals(string.Empty) ? Convert.ToString((int)DayOfWeek.Monday + 1) : dtEquipPMD.Rows[i]["PMYearDay"].ToString();     
                    
                    // ---------------------------------------------------------------- //
                    
                    #region 일간
                    //일간점검(DAY)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "DAY")
                    {
                        while (datNextDate <= datEndDate)
                        {
                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'DAY' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                //drEquipPMD["Seq"] =  intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            //다음날을 구함.
                            datNextDate = datNextDate.AddDays(1);
                            intSeq++;

                        }
                    }
                    #endregion

                    #region 주간
                    //주간점검(WEK)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "WEK")
                    {
                        // 요일(1:일요일 7:토요일)
                        // 설비정보에서 지정한 요일과 시작일의 요일의 차이 계산하여 계획시작일을 지정함.
                        intDiff = Convert.ToInt32(strPMWeekDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                        ////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMWeekDay"].ToString()) -
                        ////              Convert.ToInt32(datNextDate.DayOfWeek) + 1;

                        if (intDiff < 0)
                            datNextDate = datNextDate.AddDays(7 + intDiff);
                        else
                            datNextDate = datNextDate.AddDays(intDiff);

                        while (datNextDate <= datEndDate)
                        {
                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'WEK' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                //drEquipPMD["Seq"] = intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            //다음주를 구함.
                            datNextDate = datNextDate.AddDays(7);
                            intSeq++;
                        }
                    }

                    #endregion

                    #region 월간
                    //월간점검(MON)인 경우
                    datNextDate = datStartDate;
                    int intStartDateWeek = 1;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "MON")
                    {
                        while (true)
                        {
                            //1. 적용일의 월에서 첫주차 일자구하기
                            //-- 1-1. 적용일의 월초(1일)이 일요일인 경우 다음 적용일 = 월초(1일)
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            if (datMonthFirstDate.DayOfWeek == DayOfWeek.Sunday)
                                datNextDate = datMonthFirstDate;
                            //intStartDateWeek = 1; //(datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            else
                                datNextDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            //intStartDateWeek = 1; // (datNextDate.DayOfYear - datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek)).DayOfYear) / 7 + 1;

                            // 2. NextDate 설정
                            //-- 2-1. 적용일주차 = PM월주차 인 경우 (현재 주차인 날짜로 NextDate설정)
                            //if (intStartDateWeek == Convert.ToInt32(dtEquip.Rows[i]["PMMonthWeek"].ToString()))
                            //datNextDate = datNextDate;
                            //-- 2-2. 적용일주차 < PM월주차 인 경우 (PM월주차 - 적용일주차만큼 일자를 이동하여 NextDate설정)

                            if (intStartDateWeek < Convert.ToInt32(strPMMonthWeek))
                                datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMMonthWeek) - intStartDateWeek));
                            //-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            else if (intStartDateWeek > Convert.ToInt32(strPMMonthWeek))
                            {
                                datNextDate = datMonthFirstDate.AddMonths(1);
                                if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMMonthWeek) - 1));
                                else
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMMonthWeek) - 1));
                            }
                            intDiff = Convert.ToInt32(strPMMonthDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            ////////if (intStartDateWeek < Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()))
                            ////////    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()) - intStartDateWeek));
                            //////////-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            ////////else if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()))
                            ////////{
                            ////////    datNextDate = datMonthFirstDate.AddMonths(1);
                            ////////    if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                            ////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()) - 1));
                            ////////    else
                            ////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthWeek"].ToString()) - 1));
                            ////////}
                            ////////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMMonthDay"].ToString()) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            datNextDate = datNextDate.AddDays(intDiff);

                            //마지막날이 넘어가면 빠져나간다.
                            if (datNextDate > datEndDate)
                                break;

                            if (datNextDate > datStartDate)
                            {
                                if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'MON' ").Count() <= 0)
                                {
                                    DataRow drEquipPMD;
                                    drEquipPMD = dtPMPlanD.NewRow();
                                    drEquipPMD["PlantCode"] = strPlantCode;
                                    drEquipPMD["PlanYear"] = strPlanYear;
                                    drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                    //drEquipPMD["Seq"] = intSeq;
                                    drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                    drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                    drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                    drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                    drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                    drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                    drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                    drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                    drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                    drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                    drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                    drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                    drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                    drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                    dtPMPlanD.Rows.Add(drEquipPMD);
                                    intSeq++;
                                }
                            }
                            //다음달을 구함.
                            datNextDate = datNextDate.AddMonths(1);
                        }
                    }
                    #endregion

                    #region 분기
                    //분기점검(QUA)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "QUA")
                    {
                        //1. 적용일의 분기월이 분기월 이후인 경우 다음 분기의 분기초월로 넘김
                        //-- 1-1. 적용일의 분기월을 구함.
                        if (datNextDate.Month % 3 > 0) intQua = datNextDate.Month % 3;
                        else intQua = 3;

                        //-- 1-2. 적용일의 월 주차를 구함.
                        datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                        //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                        if (datMonthFirstDate.DayOfWeek != DayOfWeek.Sunday)
                            datMonthFirstDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                        intStartDateWeek = (datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;

                        //-- 1-2-1. 적용일의 분기월 > PM분기월 인 경우 : 다음 분기 월초로 보냄.
                        //-- 1-2-2. 적용일 분기월의 주차 > PM분기월의 주차 인 경우 : 다음 분기 월초로 보냄.
                        //-- 1-2-3. 적용일 분기월의 주차의 요일 > PM분기월의 주차의 요일 인 경우 : 다음 분기 월초로 보냄
                        bool bolQuaSkip = false;

                        //지정한 월을 분기 내의 월순서로 변환
                        string strPMYearMonth_ToQua = "";
                        if (strPMQuaMonth == "1" || strPMQuaMonth == "4" || strPMQuaMonth == "7" || strPMQuaMonth == "10")
                            strPMYearMonth_ToQua = "1";
                        else if (strPMQuaMonth == "2" || strPMQuaMonth == "5" || strPMQuaMonth == "8" || strPMQuaMonth == "11")
                            strPMYearMonth_ToQua = "2";
                        else if (strPMQuaMonth == "3" || strPMQuaMonth == "6" || strPMQuaMonth == "9" || strPMQuaMonth == "12")
                            strPMYearMonth_ToQua = "3";

                        //-- 이하는 strPMYearMonth -> strPMYearMonth_ToQua 로 변경함 --//
                        if (intQua > Convert.ToInt32(strPMYearMonth_ToQua))
                            bolQuaSkip = true;
                        else
                        {
                            if (intStartDateWeek > Convert.ToInt32(strPMQuaWeek))
                                bolQuaSkip = true;
                            else
                            {
                                if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(strPMQuaDay))
                                    bolQuaSkip = true;
                            }
                        }
                        if (bolQuaSkip == true)
                        //if (intQua > Convert.ToInt32(dtEquip.Rows[i]["PMYearMonth"].ToString())
                        //    || intStartDateWeek > Convert.ToInt32(dtEquip.Rows[i]["PMYearWeek"].ToString())
                        //    || Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(dtEquip.Rows[i]["PMYearDay"].ToString())))
                        {
                            if (datNextDate.Month == 1 || datNextDate.Month == 2 || datNextDate.Month == 3)
                            {
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-04-01");
                                datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToQua) - 1); //추가
                            }
                            else if (datNextDate.Month == 4 || datNextDate.Month == 5 || datNextDate.Month == 6)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-07-01");
                            else if (datNextDate.Month == 7 || datNextDate.Month == 8 || datNextDate.Month == 9)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-10-01");
                            else if (datNextDate.Month == 10 || datNextDate.Month == 11 || datNextDate.Month == 12)
                                datNextDate = Convert.ToDateTime(datNextDate.AddYears(1).Year + "-01-01");
                        }
                        else
                        {
                            datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToQua) - datNextDate.Month);
                            //datNextDate = datNextDate.AddMonths(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()) - datNextDate.Month);
                        }

                        while (true)
                        {

                            //2. 월간계획 로직과 동일//
                            //1. 적용일의 월에서 첫주차 일자구하기
                            //-- 1-1. 적용일의 월초(1일)이 일요일인 경우 다음 적용일 = 월초(1일)
                            intStartDateWeek = 1;
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            if (datMonthFirstDate.DayOfWeek == DayOfWeek.Sunday)
                                datNextDate = datMonthFirstDate;
                            //intStartDateWeek = 1; //(datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            else
                                datNextDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            //intStartDateWeek = 1; // (datNextDate.DayOfYear - datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek)).DayOfYear) / 7 + 1;

                            // 2. NextDate 설정
                            //-- 2-1. 적용일주차 = PM월주차 인 경우 (현재 주차인 날짜로 NextDate설정)
                            //if (intStartDateWeek == Convert.ToInt32(dtEquip.Rows[i]["PMMonthWeek"].ToString()))
                            //datNextDate = datNextDate;
                            //-- 2-2. 적용일주차 < PM월주차 인 경우 (PM월주차 - 적용일주차만큼 일자를 이동하여 NextDate설정)

                            if (intStartDateWeek < Convert.ToInt32(strPMQuaWeek))
                                datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMQuaWeek) - intStartDateWeek));
                            //-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            else if (intStartDateWeek > Convert.ToInt32(strPMQuaWeek))
                            {
                                datNextDate = datMonthFirstDate.AddMonths(1);
                                if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMQuaWeek) - 1));
                                else
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMQuaWeek) - 1));
                            }
                            intDiff = Convert.ToInt32(strPMQuaDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            ////////if (intStartDateWeek < Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            ////////    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - intStartDateWeek));
                            //////////-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            ////////else if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            ////////{
                            ////////    datNextDate = datMonthFirstDate.AddMonths(1);
                            ////////    if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                            ////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            ////////    else
                            ////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            ////////}
                            ////////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString()) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            datNextDate = datNextDate.AddDays(intDiff);

                            //마지막날이 넘어가면 빠져나간다.
                            if (datNextDate > datEndDate)
                                break;

                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'QUA' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                // drEquipPMD["Seq"] = intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            //다음분기의 월초를 구함.
                            //datNextDate = datNextDate.AddMonths(3);
                            if (datNextDate.Month == 1 || datNextDate.Month == 2 || datNextDate.Month == 3)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-04-01");
                            else if (datNextDate.Month == 4 || datNextDate.Month == 5 || datNextDate.Month == 6)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-07-01");
                            else if (datNextDate.Month == 7 || datNextDate.Month == 8 || datNextDate.Month == 9)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-10-01");
                            else if (datNextDate.Month == 10 || datNextDate.Month == 11 || datNextDate.Month == 12)
                                datNextDate = Convert.ToDateTime(datNextDate.AddYears(1).Year + "-01-01");
                            datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToQua) - 1);
                            //datNextDate = datNextDate.AddMonths(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()) - 1);
                            intSeq++;
                        }
                    }
                    #endregion

                    #region 반기
                    //반기점검(HAF)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "HAF")
                    {
                        //1. 적용일의 반기월이 반기월 이후인 경우 다음 반기의 반기초월로 넘김
                        //-- 1-1. 적용일의 반기월을 구함.
                        if (datNextDate.Month % 6 > 0) intQua = datNextDate.Month % 6;
                        else intQua = 6;

                        //-- 1-2. 적용일의 월 주차를 구함.
                        datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                        //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                        if (datMonthFirstDate.DayOfWeek != DayOfWeek.Sunday)
                            datMonthFirstDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                        intStartDateWeek = (datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;

                        //-- 1-2-1. 적용일의 반기월 > PM반기월 인 경우 : 다음 반기 월초로 보냄.
                        //-- 1-2-2. 적용일 반기월의 주차 > PM반기월의 주차 인 경우 : 다음 반기 월초로 보냄.
                        //-- 1-2-3. 적용일 반기월의 주차의 요일 > PM반기월의 주차의 요일 인 경우 : 다음 반기 월초로 보냄
                        bool bolQuaSkip = false;

                        //지정한 월을 반기 내의 월순서로 변환
                        string strPMYearMonth_ToHalf = "";
                        if (strPMHafMonth == "1" || strPMHafMonth == "7")
                            strPMYearMonth_ToHalf = "1";
                        else if (strPMHafMonth == "2" || strPMHafMonth == "8")
                            strPMYearMonth_ToHalf = "2";
                        else if (strPMHafMonth == "3" || strPMHafMonth == "9")
                            strPMYearMonth_ToHalf = "3";
                        else if (strPMHafMonth == "4" || strPMHafMonth == "10")
                            strPMYearMonth_ToHalf = "4";
                        else if (strPMHafMonth == "5" || strPMHafMonth == "11")
                            strPMYearMonth_ToHalf = "5";
                        else if (strPMHafMonth == "6" || strPMHafMonth == "12")
                            strPMYearMonth_ToHalf = "6";

                        //-- 이하는 strPMYearMonth -> strPMYearMonth_ToHalf 로 변경함 --//
                        if (intQua > Convert.ToInt32(strPMYearMonth_ToHalf))
                            bolQuaSkip = true;
                        else
                        {
                            if (intStartDateWeek > Convert.ToInt32(strPMHafWeek))
                                bolQuaSkip = true;
                            else
                            {
                                if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(strPMHafDay)))
                                    bolQuaSkip = true;
                            }
                        }
                        if (bolQuaSkip == true)
                        //if (intQua > Convert.ToInt32(dtEquip.Rows[i]["PMYearMonth"].ToString())
                        //    || intStartDateWeek > Convert.ToInt32(dtEquip.Rows[i]["PMYearWeek"].ToString())
                        //    || Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(dtEquip.Rows[i]["PMYearDay"].ToString())))
                        {
                            if (datNextDate.Month == 1 || datNextDate.Month == 2 || datNextDate.Month == 3 || datNextDate.Month == 4 || datNextDate.Month == 5 || datNextDate.Month == 6)
                            {
                                //if (datNextDate.Month > Convert.ToInt32(strPMYearMonth_ToHalf))
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-07-01");
                                datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToHalf) - 1);    //추가
                            }
                            else if (datNextDate.Month == 7 || datNextDate.Month == 8 || datNextDate.Month == 9 || datNextDate.Month == 10 || datNextDate.Month == 11 || datNextDate.Month == 12)
                                datNextDate = Convert.ToDateTime(datNextDate.AddYears(1).Year + "-01-01");
                        }
                        else
                        {
                            datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToHalf) - datNextDate.Month);
                            //datNextDate = datNextDate.AddMonths(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()) - datNextDate.Month);
                        }

                        while (true)
                        {

                            //2. 월간계획 로직과 동일//
                            //1. 적용일의 월에서 첫주차 일자구하기
                            //-- 1-1. 적용일의 월초(1일)이 일요일인 경우 다음 적용일 = 월초(1일)
                            intStartDateWeek = 1;
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            if (datMonthFirstDate.DayOfWeek == DayOfWeek.Sunday)
                                datNextDate = datMonthFirstDate;
                            //intStartDateWeek = 1; //(datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            else
                                datNextDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            //intStartDateWeek = 1; // (datNextDate.DayOfYear - datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek)).DayOfYear) / 7 + 1;

                            // 2. NextDate 설정
                            //-- 2-1. 적용일주차 = PM월주차 인 경우 (현재 주차인 날짜로 NextDate설정)
                            //if (intStartDateWeek == Convert.ToInt32(dtEquip.Rows[i]["PMMonthWeek"].ToString()))
                            //datNextDate = datNextDate;
                            //-- 2-2. 적용일주차 < PM월주차 인 경우 (PM월주차 - 적용일주차만큼 일자를 이동하여 NextDate설정)
                            if (intStartDateWeek < Convert.ToInt32(strPMHafWeek))
                                datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMHafWeek) - intStartDateWeek));
                            //-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            else if (intStartDateWeek > Convert.ToInt32(strPMHafWeek))
                            {
                                datNextDate = datMonthFirstDate.AddMonths(1);
                                if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMHafWeek) - 1));
                                else
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMHafWeek) - 1));
                            }
                            intDiff = Convert.ToInt32(strPMHafDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            //////////if (intStartDateWeek < Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            //////////    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - intStartDateWeek));
                            ////////////-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            //////////else if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            //////////{
                            //////////    datNextDate = datMonthFirstDate.AddMonths(1);
                            //////////    if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                            //////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            //////////    else
                            //////////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            //////////}
                            //////////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString()) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            datNextDate = datNextDate.AddDays(intDiff);

                            //마지막날이 넘어가면 빠져나간다.
                            if (datNextDate > datEndDate)
                                break;

                            if (dtEquipCheckPMD.Select("EquipCode = '" + strEquipCode + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'HAF' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                //drEquipPMD["Seq"] = intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            //다음분기의 월초를 구함.
                            if (datNextDate.Month == 1 || datNextDate.Month == 2 || datNextDate.Month == 3 || datNextDate.Month == 4 || datNextDate.Month == 5 || datNextDate.Month == 6)
                                datNextDate = Convert.ToDateTime(datNextDate.Year + "-07-01");
                            else if (datNextDate.Month == 7 || datNextDate.Month == 8 || datNextDate.Month == 9 || datNextDate.Month == 10 || datNextDate.Month == 11 || datNextDate.Month == 12)
                                datNextDate = Convert.ToDateTime(datNextDate.AddYears(1).Year + "-01-01");
                            datNextDate = datNextDate.AddMonths(Convert.ToInt32(strPMYearMonth_ToHalf) - 1);
                            //datNextDate = datNextDate.AddMonths(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()) - 1);
                            intSeq++;
                        }
                    }
                    #endregion

                    #region 년간

                    //년간점검(YEA)인 경우
                    datNextDate = datStartDate;
                    if (dtEquipPMD.Rows[i]["PMPeriodCode"].ToString() == "YEA")
                    {
                        bool bolQuaSkip = false;
                        //적용일의 월 > PM년월 인 경우 : 다음 해로 보냄
                        if (datNextDate.Month > Convert.ToInt32(strPMYearMonth))
                            //if (datNextDate.Month > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearMonth"].ToString()))
                            bolQuaSkip = true;
                        else
                        {
                            datNextDate = Convert.ToDateTime(datNextDate.Year.ToString() + "-" + strPMYearMonth + "-" + datNextDate.Day.ToString());
                            //datNextDate = Convert.ToDateTime(datNextDate.Year.ToString() + "-" + dtEquipPMD.Rows[i]["PMYearMonth"].ToString() + "-" + datNextDate.Day.ToString());

                            //-- 1-2. 적용일의 월 주차를 구함.
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            if (datMonthFirstDate.DayOfWeek != DayOfWeek.Sunday)
                                datMonthFirstDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            intStartDateWeek = (datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;

                            if (intStartDateWeek > Convert.ToInt32(strPMYearWeek))
                                //if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                                bolQuaSkip = true;
                            else
                            {
                                //if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(strPMYearDay)))
                                //    //if (Convert.ToInt32(datNextDate.DayOfWeek + 1) > Convert.ToInt32(Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString())))
                                //    bolQuaSkip = true;
                            }
                        }
                        if (bolQuaSkip == false)
                        {
                            //2. 월간계획 로직과 동일//
                            //1. 적용일의 월에서 첫주차 일자구하기
                            //-- 1-1. 적용일의 월초(1일)이 일요일인 경우 다음 적용일 = 월초(1일)
                            intStartDateWeek = 1;
                            datMonthFirstDate = datNextDate.AddDays(1 - datNextDate.Day);  //월의 1일 날짜 저장
                            if (datMonthFirstDate.DayOfWeek == DayOfWeek.Sunday)
                                datNextDate = datMonthFirstDate;
                            //intStartDateWeek = 1; //(datNextDate.DayOfYear - datMonthFirstDate.DayOfYear) / 7 + 1;
                            //-- 1-2. 적용일의 월초(1일)이 일요일이 아닌 경우 다음 적용일 = (월초+7일)일 일요일 일자
                            else
                                datNextDate = datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek));
                            //intStartDateWeek = 1; // (datNextDate.DayOfYear - datMonthFirstDate.AddDays(7).AddDays(0 - (int)(datMonthFirstDate.AddDays(7).DayOfWeek)).DayOfYear) / 7 + 1;

                            // 2. NextDate 설정
                            //-- 2-1. 적용일주차 = PM월주차 인 경우 (현재 주차인 날짜로 NextDate설정)
                            //if (intStartDateWeek == Convert.ToInt32(dtEquip.Rows[i]["PMMonthWeek"].ToString()))
                            //datNextDate = datNextDate;
                            //-- 2-2. 적용일주차 < PM월주차 인 경우 (PM월주차 - 적용일주차만큼 일자를 이동하여 NextDate설정)
                            if (intStartDateWeek < Convert.ToInt32(strPMYearWeek))
                                datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - intStartDateWeek));
                            //-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            else if (intStartDateWeek > Convert.ToInt32(strPMYearWeek))
                            {
                                datNextDate = datMonthFirstDate.AddMonths(1);
                                if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - 1));
                                else
                                    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(strPMYearWeek) - 1));
                            }
                            intDiff = Convert.ToInt32(strPMYearDay) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            //////if (intStartDateWeek < Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            //////    datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - intStartDateWeek));
                            ////////-- 2-3. 적용일주차 > PM월주차 인 경우 (다음달의 해당주차로 이동하여 NextDate설정)
                            //////else if (intStartDateWeek > Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()))
                            //////{
                            //////    datNextDate = datMonthFirstDate.AddMonths(1);
                            //////    if (datNextDate.DayOfWeek == DayOfWeek.Sunday)
                            //////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            //////    else
                            //////        datNextDate = datNextDate.AddDays(7 * (Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearWeek"].ToString()) - 1));
                            //////}
                            //////intDiff = Convert.ToInt32(dtEquipPMD.Rows[i]["PMYearDay"].ToString()) - (Convert.ToInt32(datNextDate.DayOfWeek) + 1);
                            datNextDate = datNextDate.AddDays(intDiff);

                            if (dtEquipCheckPMD.Select("EquipCode = '" + dtEquipPMD.Rows[i]["EquipCode"] + "' AND PMPlanDate = '" + datNextDate.ToString("yyyy-MM-dd") + "' AND PMPeriodCode = 'YEA' ").Count() <= 0)
                            {
                                DataRow drEquipPMD;
                                drEquipPMD = dtPMPlanD.NewRow();
                                drEquipPMD["PlantCode"] = strPlantCode;
                                drEquipPMD["PlanYear"] = strPlanYear;
                                drEquipPMD["EquipCode"] = dtEquipPMD.Rows[i]["EquipCode"].ToString();
                                //drEquipPMD["Seq"] = intSeq;
                                drEquipPMD["Seq"] = strTime + intSeq.ToString("D7");
                                drEquipPMD["PMPlanDate"] = datNextDate.ToString("yyyy-MM-dd");
                                drEquipPMD["PMInspectName"] = dtEquipPMD.Rows[i]["PMInspectName"];
                                drEquipPMD["PMInspectRegion"] = dtEquipPMD.Rows[i]["PMInspectRegion"];
                                drEquipPMD["PMInspectCriteria"] = dtEquipPMD.Rows[i]["PMInspectCriteria"];
                                drEquipPMD["PMPeriodCode"] = dtEquipPMD.Rows[i]["PMPeriodCode"];
                                drEquipPMD["PMMethod"] = dtEquipPMD.Rows[i]["PMMethod"];
                                drEquipPMD["FaultFixMethod"] = dtEquipPMD.Rows[i]["FaultFixMethod"];
                                drEquipPMD["StandardManCount"] = dtEquipPMD.Rows[i]["StandardManCount"];
                                drEquipPMD["StandardTime"] = dtEquipPMD.Rows[i]["StandardTime"];
                                drEquipPMD["LevelCode"] = dtEquipPMD.Rows[i]["LevelCode"];
                                drEquipPMD["ImageFile"] = dtEquipPMD.Rows[i]["ImageFile"];
                                drEquipPMD["UnitDesc"] = dtEquipPMD.Rows[i]["UnitDesc"];
                                drEquipPMD["MeasureValueFlag"] = dtEquipPMD.Rows[i]["MeasureValueFlag"];
                                dtPMPlanD.Rows.Add(drEquipPMD);
                            }
                            intSeq++;
                        }
                    }
                    #endregion
                }
                    

                dtPMPlanD.Rows[3].Delete();

                return dtPMPlanD;

            }
            catch (System.Exception ex)
            {

                return dtPMPlanD;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장하기전 예방점검정보삭제(설비그룹별) : 1/10씩 삭제
        /// </summary>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanDEquipGroup_10
            (string strPlantCode
            , string strGroupCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroupCode
            , string strEquipLargeTypeCode
            , string strPlanYear
            , string strPMApplyDate
            , SqlConnection sqlcon
            , SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                //sql.mfConnect();


                //BeginTransaction시작
                //trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanDEquipGroup_10", dtParam); //실행용 (sql.sqlCon -> sqlcon으로 변경)
                                                                      
                //strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanEquipGroup", dtParam); //디버깅용용

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과에 따른처리
                if (ErrRtn.ErrNum != 0)
                {
                    //trans.Rollback();
                    return strErrRtn;
                }
                //else
                //{
                //    trans.Commit();
                //}
                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 저장하기전 예방점검정보헤더삭제(설비그룹별) 
        /// </summary>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanEquipGroup_10
            (string strPlantCode
            , string strGroupCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroupCode
            , string strEquipLargeTypeCode
            , string strPlanYear
            , SqlConnection sqlcon
            , SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                //sql.mfConnect();


                //BeginTransaction시작
                //trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanEquipGroup_10", dtParam); //실행용 (sql.sqlCon -> sqlcon으로 변경)
                //strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanEquipGroup", dtParam); //디버깅용용

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과에 따른처리
                if (ErrRtn.ErrNum != 0)
                {
                    //trans.Rollback();
                    return strErrRtn;
                }
                //else
                //{
                //    trans.Commit();
                //}
                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 저장하기전 예방점검정보삭제(설비그룹별)
        /// </summary>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanEquipGroup
            (string strPlantCode
            , string strGroupCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroupCode
            , string strEquipLargeTypeCode
            , string strPlanYear
            , string strPMApplyDate
            , SqlConnection sqlcon
            , SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                //sql.mfConnect();
                 

                //BeginTransaction시작
                //trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                
                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanEquipGroup", dtParam); //실행용 (sql.sqlCon -> sqlcon으로 변경)
                //strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanEquipGroup", dtParam); //디버깅용용

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과에 따른처리
                if (ErrRtn.ErrNum != 0)
                {
                    //trans.Rollback();
                    return strErrRtn;
                }
                //else
                //{
                //    trans.Commit();
                //}
                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 저장하기전 예방점검정보삭제(작업자별)
        /// </summary>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanEquipWorker(string strPlantCode, string strEquipWorkID, string strPlanYear, string strPMApplyDate, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                //sql.mfConnect();


                //BeginTransaction시작
                //trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, strEquipWorkID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                //strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMPlanEquipWork", dtParam); //실행용
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanEquipWork", dtParam); //실행용
                //strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanEquipWork", dtParam); //디버깅용용

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과에 따른처리
                if (ErrRtn.ErrNum != 0)
                {
                    //trans.Rollback();
                    return strErrRtn;
                }
                //else
                //{
                //    trans.Commit();
                //}
                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 예방점검정보삭제(그룹별)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strGroupCode">설비그룹코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlan(string strPlantCode, string strGroupCode, string strPlanYear)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //BeginTransaction시작
                trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMPlan", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과에 따른처리
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 예방점검정보삭제
        /// </summary>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlan_All(string strPlantCode, string strStationCode,string strEquipLocCode,
                                        string strProcessGroupCode, string strEquipLargeTypeCode, string strGroupCode, string strPlanYear, string strPMApplyDate)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();


                for (int i = 0; i < 50; i++)
                {
                    SqlTransaction trans;
                    trans = sql.SqlCon.BeginTransaction(IsolationLevel.ReadCommitted);    //실행용

                    //파라미터값저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);

                    if (i > 40)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "50");
                    else if (i > 30)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "40");
                    else if (i > 20)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "30");
                    else if (i > 10)
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "20");
                    else
                        sql.mfAddParamDataRow(dtParam, "@i_intCount", ParameterDirection.Input, SqlDbType.Int, "10");

                    sql.mfAddParamDataRow(dtParam, "@o_strRowCoutCheck", ParameterDirection.Output, SqlDbType.Char, 1);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMPlanDEquipGroup_Loop", dtParam); //실행용 (sql.sqlCon -> sqlcon으로 변경)

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Commit();
                        i = ErrRtn.mfGetReturnValue(0) == "T" ? 49 : i;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                {
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    strErrRtn = mfDeletePMPlanEquipGroup(strPlantCode, strGroupCode, strStationCode,
                                                            strEquipLocCode, strProcessGroupCode, strEquipLargeTypeCode,
                                                            strPlanYear, strPMApplyDate, sql.SqlCon, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();

                }


                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 예방점검정보 저장(일괄)
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <param name="strStationCode">Station코드</param>
        /// <param name="strProcGubunCode">공정구분코드</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strWriteDate">작성일</param>
        /// <param name="strPMApplyDate"></param>
        /// <param name="strWriteID"></param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanBatch(string strPlantCode, string strPlanYear, string strAreaCode, string strStationCode, string strProcGubunCode, string strWriteID, string strWriteDate, string strPMApplyDate, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            try
            {
                //디비오픈
                sql.mfConnect();  //실행용

                //////////설비그룹에 대한 설비리스트를 가지고 온다. => 추가//
                ////////QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                //////////QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD(m_SqlConnDebug.ConnectionString);  //디버깅용
                ////////DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMDForBatch(strPlantCode, strAreaCode, strStationCode, strProcGubunCode, sql.SqlCon);

                DataTable dtParameterPMD = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);
                //프로시저 호출
                DataTable dtEquipPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMD_Batch", dtParameterPMD); //실행용
                //DataTable dtEquipPMD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipPMD_Batch", dtParameterPMD); //디버깅용

                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();    //실행용
                //trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

                DataTable dtEquipCheckPMD = new DataTable();
                strErrRtn = mfDeletePMPlanBatch(strPlanYear, strPlantCode, strAreaCode, strStationCode, strProcGubunCode, sql.SqlCon, trans);   //실행용
                //strErrRtn = mfDeletePMPlanBatch(strPlanYear, strPlantCode, strAreaCode, strStationCode, strProcGubunCode, m_SqlConnDebug, trans);     //디버깅용

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                if (dtEquipPMD.Rows.Count <= 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    //예방점검계획을 생성한다.
                    DataTable dtPMPlanD = GeneratePMSchedule(strPlantCode, strPlanYear, strPMApplyDate, dtEquipPMD, dtEquipCheckPMD);

                    string strCurEquipCode = "";
                    bool bolSaveHeader = false;

                    //==== 여기부터 시작 (2011.09.27) =====
                    //생성한 예방점검계획을 저장한다.
                    for (int i = 0; i < dtPMPlanD.Rows.Count; i++)
                    {
                        if (strCurEquipCode == "")
                        {
                            strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
                            bolSaveHeader = true;
                        }
                        else
                        {
                            if (strCurEquipCode != dtPMPlanD.Rows[i]["EquipCode"].ToString())
                            {
                                strCurEquipCode = dtPMPlanD.Rows[i]["EquipCode"].ToString();
                                bolSaveHeader = true;
                            }
                            else
                                bolSaveHeader = false;
                        }

                        //예방점검 헤더 저장하기
                        if (bolSaveHeader == true)
                        {
                            //파라미터 저장
                            DataTable dtParameter = sql.mfSetParamDataTable();
                            sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                            sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                            sql.mfAddParamDataRow(dtParameter, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                            sql.mfAddParamDataRow(dtParameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strCurEquipCode, 20);
                            sql.mfAddParamDataRow(dtParameter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, strWriteID, 20);
                            sql.mfAddParamDataRow(dtParameter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteDate, 10);
                            sql.mfAddParamDataRow(dtParameter, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
                            sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                            sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                            sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                            //프로시저 실행
                            strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlan", dtParameter); //실행용
                            //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlan", dtParameter);   //디버깅용

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            //처리결과 값 처리
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }

                        //예방점검 상세 저장하기
                        //파라미터 저장
                        DataTable dtParameterD = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParameterD, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParameterD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["EquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParameterD, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtPMPlanD.Rows[i]["Seq"].ToString());

                        sql.mfAddParamDataRow(dtParameterD, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPlanDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectName", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectName"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectRegion", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectRegion"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strPMInspectCriteria", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMInspectCriteria"].ToString(), 500);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strPMPeriodCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPeriodCode"].ToString(), 3);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strPMMethod", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMMethod"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strFaultFixMethod", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["FaultFixMethod"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParameterD, "@i_intStandardManCount", ParameterDirection.Input, SqlDbType.Int, dtPMPlanD.Rows[i]["StandardManCount"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParameterD, "@i_dblStandardTime", ParameterDirection.Input, SqlDbType.Decimal, dtPMPlanD.Rows[i]["StandardTime"].ToString());
                        sql.mfAddParamDataRow(dtParameterD, "@i_strLevelCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["LevelCode"].ToString(), 1);

                        sql.mfAddParamDataRow(dtParameterD, "@i_strImageFile", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanD.Rows[i]["ImageFile"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strUnitDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanD.Rows[i]["UnitDesc"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strMeasureValueFlag", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["MeasureValueFlag"].ToString(), 1);

                        sql.mfAddParamDataRow(dtParameterD, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParameterD, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParameterD, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlanD", dtParameterD);
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_EQUPMPlanD", dtParameterD);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리결과 값 처리
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                }
                trans.Commit();
                return strErrRtn;  
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        
        /// <summary>
        /// 예방점검정보 삭제 (일괄)
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">area코드</param>
        /// <param name="strStationCode">station코드</param>
        /// <param name="strProcGubunCode">공정구분코드</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanBatch(string strPlanYear, string strPlantCode, string strAreaCode, string strStationCode, string strProcGubunCode)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                //프로시저실행
                sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMPlanBatch", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과값처리
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();
                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 저장전 예방점검정보 삭제 (일괄)
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">area코드</param>
        /// <param name="strStationCode">station코드</param>
        /// <param name="strProcGubunCode">공정구분코드</param>
        /// <param name="Sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanBatch(string strPlanYear, string strPlantCode, string strAreaCode, string strStationCode, string strProcGubunCode,SqlConnection Sqlcon,SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                //sql.mfConnect();
                //SqlTransaction trans;
                //trans = sql.SqlCon.BeginTransaction();

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Delete_EQUPMPlanBatch", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과값처리
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                //else
                //    trans.Commit();
                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
               // sql.mfDisConnect();
                sql.Dispose();
            }
        }





    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("PMPlanD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class PMPlanD : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        /// <summary>
        /// 생성자
        /// </summary>
        public PMPlanD()
        {

        }

        /// <summary>
        /// Debug모드용 생성자
        /// </summary>
        /// <param name="strDBConn"></param>
        public PMPlanD(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            if (m_SqlConnDebug.State != ConnectionState.Open)
                m_SqlConnDebug.Open();
        }

        #region ColumnsSet
        /// <summary>
        /// 데이터테이블 컬럼셋
        /// </summary>
        /// <returns>컬럼만있는 테이블</returns>
        public DataTable mfSetPMPlanDColumn()
        {
            DataTable dtColumn = new DataTable();

            try
            {
                dtColumn.Columns.Add("PlantCode", typeof(string));
                dtColumn.Columns.Add("EquipCode", typeof(string));
                dtColumn.Columns.Add("Package", typeof(string));
                dtColumn.Columns.Add("PlanYear", typeof(string));
                dtColumn.Columns.Add("Seq", typeof(string));
                dtColumn.Columns.Add("PMPlanDate", typeof(string));
                //dtColumn.Columns.Add("PMInspectName", typeof(string));
                //dtColumn.Columns.Add("PMInspectRegion", typeof(string));
                //dtColumn.Columns.Add("PMInspectCriteria", typeof(string));
                //dtColumn.Columns.Add("PMPeriodCode", typeof(string));
                //dtColumn.Columns.Add("PMMethod", typeof(string));
                //dtColumn.Columns.Add("FaultFixMethod", typeof(string));
                //dtColumn.Columns.Add("StandardManCount", typeof(string));
                dtColumn.Columns.Add("ChangeFlag", typeof(string));
                dtColumn.Columns.Add("RepairFlag", typeof(string));
                dtColumn.Columns.Add("PMWorkDate", typeof(string));
                dtColumn.Columns.Add("PMWorkTime", typeof(string));
                dtColumn.Columns.Add("PMWorkID", typeof(string));
                dtColumn.Columns.Add("PMResultCode", typeof(string));
                dtColumn.Columns.Add("PMResultValue", typeof(string));
                dtColumn.Columns.Add("PMWorkDesc", typeof(string));
                dtColumn.Columns.Add("FilePath", typeof(string));

                return dtColumn;

            }
            catch(Exception)
            {
                return dtColumn;
            }
            finally
            {
                dtColumn.Dispose();
            }
        }

        /// <summary>
        /// 데이터테이블 컬럼셋
        /// </summary>
        /// <returns>컬럼만있는 테이블</returns>
        public DataTable mfSetPMPlanD_Revise()
        {
            DataTable dtColumn = new DataTable();

            try
            {
                dtColumn.Columns.Add("PlanYear", typeof(string));
                dtColumn.Columns.Add("PlantCode", typeof(string));
                dtColumn.Columns.Add("EquipCode", typeof(string));
                dtColumn.Columns.Add("PMPeriodCode", typeof(string));
                dtColumn.Columns.Add("PMPlanDate", typeof(string));
                dtColumn.Columns.Add("PMReviseDate", typeof(string));

                return dtColumn;

            }
            catch(Exception)
            {
                return dtColumn;
            }
            finally
            {
                dtColumn.Dispose();
            }
        }
        #endregion

        #region Select
        /// <summary>
        /// 예방점검정보상세조회(일괄)
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <param name="strStationCode">Station코드</param>
        /// <param name="strProcGubunCode">공정구분코드</param>
        /// <param name="strEquipLoc">설비위치코드</param>
        /// <param name="strEquipType">설비유형코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD(string strPlanYear,string strPlantCode,string strAreaCode,string strStationCode,string strProcGubunCode,
                                        string strEquipLoc, string strEquipType,string strLang)
        {
            DataTable dtPMPlanD = new DataTable();
            SQLS sql = new SQLS();        //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            try
            {
                //디비시작
                sql.mfConnect();  //실행용

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char,strPlanYear,4);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLoc, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipType", ParameterDirection.Input, SqlDbType.VarChar, strEquipType, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                
                //프로시저 실행
                dtPMPlanD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanDBatch", dtParam);
                //dtPMPlanD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUPMPlanDBatch", dtParam);  //디버깅용

                //정보값 리턴
                return dtPMPlanD;
            }
            catch(Exception ex)
            {
                return dtPMPlanD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMPlanD.Dispose();
            }
        }

        //'===>헤더정보 EquipGroup 및 EquipWorker로 조회 Method 구성 ===

        /// <summary>
        /// 예방점검상세정보 조회(그룹별)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>예방점검상세</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanDEquipGroup
            (string strPlantCode
            , string strPlanYear
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroupCode
            , string strEquipLargeTypeCode
            ,string strEquipGroupCode
            , string strLang)
        {
            DataTable dtPMPlanD = new DataTable();            
            SQLS sql = new SQLS();        //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            try
            {
                //DB연결
                sql.mfConnect();  //실행용

                //파라미터값저장
                DataTable dtParame = sql.mfSetParamDataTable();
                
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParame, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strProcessGroupCode , 40);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                
                //프로시저 실행
                dtPMPlanD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_EquipGroup", dtParame);
                //dtPMPlanD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUPMPlanD_EquipGroup", dtParame); //디버깅용

                //정보리턴
                return dtPMPlanD;

            }
            catch (Exception ex)
            {
                return dtPMPlanD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMPlanD.Dispose();
            }
        }

        /// <summary>
        /// 예방점검상세정보 조회(작업자별)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strUserID"></param>
        /// <param name="strLang"></param>
        /// <returns>정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanDEquipWorker(string strPlantCode, string strUserID, string strPlanYear, string strLang)
        {
            DataTable dtPMPlanD = new DataTable();

            SQLS sql = new SQLS();        //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            try
            {
                //DB연결
                sql.mfConnect();  //실행용

                //파라미터값저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strEquipWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 실행
                dtPMPlanD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_EquipWorker", dtParame);
                //dtPMPlanD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUPMPlanD_EquipWorker", dtParame); //디버깅용

                //정보리턴
                return dtPMPlanD;

            }
            catch (Exception ex)
            {
                return dtPMPlanD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMPlanD.Dispose();
            }
        }

        /// <summary>
        /// 점검항목 , 계획주기, 설비 조회
        /// </summary>
        [AutoComplete]
        public DataTable mfReadPMPlanD_EquipGroup(string strPlantCode, string strEquipGroupCode, string strStationCode, string strEquipLocCode, string strProcessGroupCode, string strEquipLargeTypeCode, string strLang)
        {
            DataTable dtEquipPMD = new DataTable();
            SQLS sql = new SQLS();    //실행용

            try
            {
                //디비오픈
                sql.mfConnect();

                //////////설비그룹에 대한 설비리스트를 가지고 온다. => 추가//

                DataTable dtParameterPMD = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 10);
                sql.mfAddParamDataRow(dtParameterPMD, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 10);
                //프로시저 호출
                dtEquipPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMD_EquipGroup_PSTS", dtParameterPMD); //실행용


                return dtEquipPMD;
            }
            catch (Exception ex)
            {
                return dtEquipPMD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipPMD.Dispose();
            }
        }


        /// <summary>
        /// 점검계획조정 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strStationCode">StationCode</param>
        /// <param name="strLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strEquipWorkerID">정비사</param>
        /// <param name="strMonth">검색월</param>
        /// <param name="strPMPeriodCode">점검주기코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>예방점검계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_Revise(string strPlanYear, string strPlantCode, string strStationCode, 
                                            string strLocCode, string strProcessGroup,string strEquipLargeTypeCode,
                                            string strEquipGroupCode, string strEquipWorkerID, string strMonth, string strPMPeriodCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPlanD = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeType", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strEquipWorkerID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPeriodCode", ParameterDirection.Input, SqlDbType.VarChar, strPMPeriodCode, 3);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //점검계획조정 조회 프로시저 실행
                dtPlanD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_Revise", dtParam);

                //예방점검계획정보 리턴
                return dtPlanD;
            }
            catch (Exception ex)
            {
                return dtPlanD;
                throw (ex);

            }
            finally
            {
                sql.mfDisConnect();
                dtPlanD.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// PM체크 재생성시 결과등록여부 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strStationCode">StationCode</param>
        /// <param name="strLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <returns>예방점검계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_CheckResult_EquipGroup(string strPlanYear, string strPlantCode, string strStationCode,
                                            string strLocCode, string strProcessGroup, string strEquipLargeTypeCode,
                                            string strEquipGroupCode)
        {
            SQLS sql = new SQLS();
            DataTable dtPlanD = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeType", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);

                //점검계획조정 조회 프로시저 실행
                dtPlanD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_CheckResult_EquipGroup", dtParam);

                //예방점검계획정보 리턴
                return dtPlanD;
            }
            catch (Exception ex)
            {
                return dtPlanD;
                throw (ex);

            }
            finally
            {
                //sql.mfDisConnect();
                dtPlanD.Dispose();
                sql.Dispose();
            }
        }

        #endregion

        #region Insert,Delete
        ///////// <summary>
        ///////// 예방점검상세정보 저장(그룹별)
        ///////// </summary>
        ///////// <param name="strPlantCode">공장코드</param>
        ///////// <param name="strGroupCode">설비그룹코드</param>
        ///////// <param name="strPMApplyDate">계획적용시작일</param>
        ///////// <param name="strUserID">사용자아이디</param>
        ///////// <param name="strUserIP">사용자아이피</param>
        ///////// <param name="sqlcon">SqlConnection</param>
        ///////// <param name="trans">SqlTransaction</param>
        ///////// <returns>처리결과값</returns>
        //////[AutoComplete(false)]
        //////public string mfSavePMPlanD(string strPlantCode, string strGroupCode, string strPMApplyDate, string strUserID, string strUserIP, SqlConnection sqlcon, SqlTransaction trans)
        //////{
        //////    SQLS sql = new SQLS();
        //////    TransErrRtn ErrRtn = new TransErrRtn();
        //////    string strRtn = "";
        //////    try
        //////    {
        //////        //디비연결
        //////        //sql.mfConnect();

        //////        //파라미터 저장
        //////        DataTable dtParame = sql.mfSetParamDataTable();
        //////        sql.mfAddParamDataRow(dtParame, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

        //////        sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        //////        sql.mfAddParamDataRow(dtParame, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);
        //////        sql.mfAddParamDataRow(dtParame, "@i_strStartDate", ParameterDirection.Input, SqlDbType.VarChar, strPMApplyDate, 10);
        //////        sql.mfAddParamDataRow(dtParame, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
        //////        sql.mfAddParamDataRow(dtParame, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

        //////        sql.mfAddParamDataRow(dtParame, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        //////        //프로시저실행
        //////        strRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUPMPlanD", dtParame);

        //////        ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
        //////        if (ErrRtn.ErrNum != 0)
        //////        {
        //////            trans.Rollback();
        //////            return strRtn;
        //////        }
        //////        return strRtn;

        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        ErrRtn.SystemStackTrace = ex.StackTrace;
        //////        ErrRtn.SystemMessage = ex.Message;
        //////        ErrRtn.SystemInnerException = ex.InnerException.ToString();
        //////        return ErrRtn.mfEncodingErrMessage(ErrRtn);
        //////    }
        //////    finally
        //////    {
        //////    }
        //////}


        /// <summary>
        /// 예방점검상세정보 저장(일괄)
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <param name="strStationCode">Station코드</param>
        /// <param name="strProcGubunCode">공정구분코드</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strStartDate">계획적용시작일</param>
        /// <param name="strWirteUser">작성자</param>
        /// <param name="strWriteDate">작성일</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanD(string strPlanYear, string strPlantCode, string strAreaCode, string strStationCode, string strProcGubunCode, string strStartDate, string strWirteUser, string strWriteDate, string strUserID, string strUserIP, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            SQLS sql = new SQLS();
            try
            {

                DataTable dtParam = sql.mfSetParamDataTable();
                
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, strWirteUser, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPMApplyDate", ParameterDirection.Input, SqlDbType.VarChar, strStartDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                
                //프로시저실행           
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUPMPlanDBatch", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                
            }
            finally
            {
                sql.Dispose();
            }
        }
     
        /// <summary>
        /// 예방점검상세정보 삭제
        /// </summary>
        /// <param name="dtPMPlanD">삭제할정보</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanD(DataTable dtPMPlanD)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();

                SqlTransaction trans;

                for (int i = 0; i < dtPMPlanD.Rows.Count; i++)
                {
                    //BeginTransaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    //파라미터값저장
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, dtPMPlanD.Rows[i]["PlanYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strSeq", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["Seq"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMPlanD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 예방점검계획상세정보 날짜 변경
        /// </summary>
        /// <param name="dtPMPlanD">변경할 정보</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanD_Revise(DataTable dtPMPlanD, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();
                
                for (int i = 0; i < dtPMPlanD.Rows.Count; i++)
                {
                    
                    //파라미터 저장
                    DataTable dtParame = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParame, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParame, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, dtPMPlanD.Rows[i]["PlanYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParame, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParame, "@i_strPMPeriodCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPeriodCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParame, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMPlanDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParame, "@i_strPMReviseDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanD.Rows[i]["PMReviseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParame, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParame, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParame, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlanDModify", dtParame);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    //처리결과확인
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region 점검결과 등록
        //--------------------------점검결과 등록------------------------------//

 
        /// <summary>
        /// 점검결과등록 조회
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlanDate">계획일</param>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strUserID">정비사</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strGroupCode">설비그룹</param>
        /// <param name="strAreaCode">Area</param>
        /// <param name="strEquipProcGubunCode">공정구분</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과등록정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanDResult(string strPlanYear, string strPlanDate, string strPlantCode, string strUserID,
                                             string strStationCode, string strEquipLocCode,string strProcessGroup,string strEquipLargeTypeCode,string strGroupCode,
                                            string strAreaCode, string strEquipProcGubunCode,string strLang)
        {
            SQLS sql = new SQLS();        //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtPMResult = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();  //실행용

                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPlanDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipProcGubunCode, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanDResult", dtParam);   //실행용
                //dtPMResult = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_EQUPMPlanDResult", dtParam);   //디버깅용

                return dtPMResult;

            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMResult.Dispose();
            }
        }

        /// <summary>
        /// 점검결과등록(일괄) 조회
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlanDate">계획적용일</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <param name="strStation">Station코드</param>
        /// <param name="strProcGubun">공정구분코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanDResult(string strPlanYear, string strPlanDate, string strPlantCode,string strAreaCode,string strStation,string strProcGubun , string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPMResult = new DataTable();

            try
            {
                //디비오픈
                sql.mfConnect();
                
                //파라미터 값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char,strPlanYear,4);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanDate", ParameterDirection.Input, SqlDbType.VarChar,strPlanDate,10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar,strAreaCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar,strStation,10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar,strProcGubun,10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                //프로시저실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanDBatchResult", dtParam);

                return dtPMResult;
            }
            catch(Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMResult.Dispose();
            }
        }


        /// <summary>
        /// 점검결과등록 등록
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPMWorker">PM입력자</param>
        /// <param name="dtPMPlanResult">점검결과등록할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanDResult(string strPlantCode,string strPlanYear,string strPMWorker,DataTable dtPMPlanResult ,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                SqlTransaction trans;
                //디비 오픈
                sql.mfConnect(); 

                
                for (int i = 0; i < dtPMPlanResult.Rows.Count; i++) 
                {
                    //BeginTransaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    //파라미터값 저장
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["Package"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strSeq", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strPMWorkDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["PMWorkDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMWorkTime", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["PMWorkTime"].ToString(), 8);

                    sql.mfAddParamDataRow(dtParam, "@i_strPMWorkID", ParameterDirection.Input, SqlDbType.VarChar, strPMWorker, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultCode", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanResult.Rows[i]["PMResultCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultValue", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanResult.Rows[i]["PMResultValue"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strChgFlag", ParameterDirection.Input, SqlDbType.Char, dtPMPlanResult.Rows[i]["ChangeFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairFlag", ParameterDirection.Input, SqlDbType.Char, dtPMPlanResult.Rows[i]["RepairFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMWorkDesc", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["PMWorkDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    
                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlanDResult", dtParam); 

                    //처리결과에따라 처리
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //실패 할경우 롤백
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    
                    else
                    {

                        //점검항목 파일 정보 등록
                        if (!dtPMPlanResult.Rows[i]["FilePath"].ToString().Equals(string.Empty))
                        {
                            strErrRtn = mfSavePMPlanDFile(strPlantCode, strPlanYear, dtPMPlanResult.Rows[i]["EquipCode"].ToString(), dtPMPlanResult.Rows[i]["Seq"].ToString(),
                                                            dtPMPlanResult.Rows[i]["FilePath"].ToString(),strUserIP,strUserID,sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }


                        PMResult clsPMResult = new PMResult();

                        strErrRtn = clsPMResult.mfSavePMResult(strPlantCode, strPlanYear, dtPMPlanResult.Rows[i]["EquipCode"].ToString()
                                                                , dtPMPlanResult.Rows[i]["PMPlanDate"].ToString(), dtPMPlanResult.Rows[i]["PMResultCode"].ToString()
                                                                , dtPMPlanResult.Rows[i]["RepairFlag"].ToString(), dtPMPlanResult.Rows[i]["PMResultValue"].ToString(), strUserIP, strUserID, sql.SqlCon, trans);
                        
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                        
                    }

                    // 모든정보가 처리성공시 커밋을 한다.
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                }

                //처리결과값 리턴
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 점검결과등록 임시테이블등록, 파일등록
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPMWorker">PM입력자</param>
        /// <param name="dtPMPlanResult">점검결과등록할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanDTemp(string strPlantCode, string strPlanYear, string strPMWorker, DataTable dtPMPlanResult, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                SqlTransaction trans;
                //디비 오픈
                sql.mfConnect();


                for (int i = 0; i < dtPMPlanResult.Rows.Count; i++)
                {
                    //BeginTransaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    //파라미터값 저장
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["Package"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strSeq", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["PMPlanDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMWorkDate", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["PMWorkDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMWorkTime", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["PMWorkTime"].ToString(), 8);

                    sql.mfAddParamDataRow(dtParam, "@i_strPMWorkID", ParameterDirection.Input, SqlDbType.VarChar, strPMWorker, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultCode", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanResult.Rows[i]["PMResultCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultValue", ParameterDirection.Input, SqlDbType.NVarChar, dtPMPlanResult.Rows[i]["PMResultValue"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strChgFlag", ParameterDirection.Input, SqlDbType.Char, dtPMPlanResult.Rows[i]["ChangeFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairFlag", ParameterDirection.Input, SqlDbType.Char, dtPMPlanResult.Rows[i]["RepairFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMWorkDesc", ParameterDirection.Input, SqlDbType.VarChar, dtPMPlanResult.Rows[i]["PMWorkDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMPlanDTemp", dtParam);


                    //처리결과에따라 처리
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //실패 할경우 롤백
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                    else
                    {
                        ////strErrRtn = mfDeletePMPlanDFile(strPlantCode, strPlanYear, dtPMPlanResult.Rows[i]["EquipCode"].ToString(), dtPMPlanResult.Rows[i]["Seq"].ToString(), sql.SqlCon, trans);

                        ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        ////if (ErrRtn.ErrNum != 0)
                        ////{
                        ////    trans.Rollback();
                        ////    return strErrRtn;
                        ////}
                        //점검항목 파일 정보 등록
                        if (!dtPMPlanResult.Rows[i]["FilePath"].ToString().Equals(string.Empty))
                        {
                            strErrRtn = mfSavePMPlanDFile(strPlantCode, strPlanYear, dtPMPlanResult.Rows[i]["EquipCode"].ToString(), dtPMPlanResult.Rows[i]["Seq"].ToString(),
                                                            dtPMPlanResult.Rows[i]["FilePath"].ToString(), strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }


                        PMResult clsPMResult = new PMResult();

                        strErrRtn = clsPMResult.mfSavePMResult(strPlantCode, strPlanYear, dtPMPlanResult.Rows[i]["EquipCode"].ToString()
                                                                , dtPMPlanResult.Rows[i]["PMPlanDate"].ToString(), dtPMPlanResult.Rows[i]["PMResultCode"].ToString()
                                                                , dtPMPlanResult.Rows[i]["RepairFlag"].ToString(), dtPMPlanResult.Rows[i]["PMResultValue"].ToString(), strUserIP, strUserID, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                    }

                    // 모든정보가 처리성공시 커밋을 한다.
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                }

                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 점검항목 파일 등록
        /// </summary>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMPlanDFile(string strPlantCode,string strPlanYear,string strEquipCode,
                                        string strSeq,string strFilePath, string strUserIP, string strUserID,
                                        SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strSeq", ParameterDirection.Input, SqlDbType.VarChar, strSeq, 30);
                sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, strFilePath, 1000);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUPMPlanDFile", dtParam);

                //처리결과에따라 처리
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //실패 할경우 롤백
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;
                

                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 점검항목 파일 삭제
        /// </summary>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanDFile(string strPlantCode, string strPlanYear, string strEquipCode,
                                        string strSeq, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strSeq", ParameterDirection.Input, SqlDbType.VarChar, strSeq, 30);
                
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_EQUPMPlanDFile", dtParam);

                //처리결과에따라 처리
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //실패 할경우 롤백
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;
                

                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        
        /// <summary>
        /// 점검항목 파일 삭제
        /// </summary>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeletePMPlanDFile_D(string strPlantCode, string strPlanYear, string strEquipCode,string strSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strSeq", ParameterDirection.Input, SqlDbType.VarChar, strSeq, 30);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMPlanDFile", dtParam);

                //처리결과에따라 처리
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //실패 할경우 롤백
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();


                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region MES

        /// <summary>
        /// 설비 및 점검일에 대한 설비PM정보 및 점검결과 기준정보 MES IF 전송
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">점검일자</param>
        /// <returns>	PlantCode,
        ///             EquipCode,
        ///             PMPlanDate,
        ///             PlanYear,
        ///             Seq,
        ///             PMPeriodName,
        ///             PMInspectRegion,
        ///             PMInspectName</returns>
        public DataTable mfReadPMPlanD_MESIF(string strPlantCode, string strEquipCode, string strPMPlanDate)
        {
            SQLS sql = new SQLS();
            DataTable dtPMResult = new DataTable();

            try
            {
                //디비오픈
                sql.mfConnect();

                //파라미터 값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);

                //프로시저실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_MESIF", dtParam);

                return dtPMResult;
            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMResult.Dispose();
            }
        }

        #endregion

        #region MyJob
        /// <summary>
        /// 마이잡 월별 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strYear">계획년도</param>
        /// <param name="strMonth">계획적용일</param>
        /// <param name="strUserID">정비사</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과 정보</returns>
        [AutoComplete]
        public DataTable mfReadMyJob_Month(string strPlantCode, string strYear, string strMonth, string strUserID, string strLang)
        {
            SQLS sql = new SQLS();        //실행용
            DataTable dtTable = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();  //실행용

                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUMyJob_Month", dtParam);   //실행용

                return dtTable;

            } 
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 마이잡 주간별 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strYear">계획년도</param>
        /// <param name="strMonth">검색월</param>
        /// <param name="strWeek">검색주</param>
        /// <param name="strUserID">정비사</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과 정보</returns>
        [AutoComplete]
        public DataTable mfReadMyJob_Week(string strPlantCode, string strYear, string strMonth, string strWeek,string strUserID, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMyJobWeek = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 연결정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);   //공장
                sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.Char, strYear, 4);                 //계획년도
                sql.mfAddParamDataRow(dtParam, "@i_strMonth", ParameterDirection.Input, SqlDbType.VarChar, strMonth, 2);            //월
                sql.mfAddParamDataRow(dtParam, "@i_strWeek", ParameterDirection.Input, SqlDbType.VarChar, strWeek, 1);              //주
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);         //정비사
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);              //사용언어

                //MyJob주별 검색 프로시저실행
                dtMyJobWeek = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUMyJob_Week", dtParam);

                //점검결과정보 리턴
                return dtMyJobWeek;

            }
            catch (Exception ex)
            {
                return dtMyJobWeek;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMyJobWeek.Dispose();
            }
        }

        #endregion

        #region 설비보전계획,설비점검일지

        /// <summary>
        /// 설비보전계획표(설비계획)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPMPlanDate">점검월,일</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_MonthPlan(string strPlantCode, string strPlanYear, string strPMPlanDate, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMonthPlan = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar,strPlanYear,4);
                sql.mfAddParamDataRow(dtParam,"@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar,strPMPlanDate,7);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //설비보전계획표(설비계획) 프로시저실행
                dtMonthPlan = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_MonthPlan", dtParam);

                //설비계획 정보리턴
                return dtMonthPlan;
            }
            catch (Exception ex)
            {
                return dtMonthPlan;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtMonthPlan.Dispose();
                sql.Dispose();
            }


        }

        /// <summary>
        /// 설비점검일지(설비점검결과)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPMPlanDate">점검월,일</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비계획정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_MonthResult(string strPlantCode, string strPlanYear, string strPMPlanDate, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable MonthResult = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 7);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //설비보전계획표(설비계획) 프로시저실행
                MonthResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_MonthResult", dtParam);

                //설비계획 정보리턴
                return MonthResult;
            }
            catch (Exception ex)
            {
                return MonthResult;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                MonthResult.Dispose();
                sql.Dispose();
            }


        }

        #endregion


        #region PM체크 결과조회


        /// <summary>
        /// PM체크결과 Total건수 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <returns>PM Total건수 정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_TotalCount
            (string strPlantCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroup
            , string strEquipLargeTypeCode
            , string strEquipGroupCode
            , string strWorkerID
            , string strPlanYear
            , string strFromDate
            , string strToDate)
            
        {
            DataTable dtPMChk = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strWorkerID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);

                //PM체크결과 PM완료여부조회 프로시저 실행
                dtPMChk = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_TotalCount", dtParam);

                return dtPMChk;
            }
            catch (Exception ex)
            {
                return dtPMChk;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMChk.Dispose();
                sql.Dispose();
            }
        }

         
        /// <summary>
        /// PM체크결과 설비조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_Equip(string strPlantCode,string strStationCode,
                                    string strEquipLocCode,string strProcessGroup,
                                    string strEquipLargeTypeCode,string strEquipGroupCode,string strWorkerID)
        {
            SQLS sql = new SQLS();
            DataTable dtPMResult = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar,strStationCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipLocCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strProcessGroup,40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipLargeTypeCode,40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipGroupCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar,strWorkerID,20);

                //PM체크결과 설비조회 프로시저 실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_Equip", dtParam);

                //PM체크결과 설비정보
                return dtPMResult;
            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMResult.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// PM체크결과 정비사조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <returns>정비사정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_Worker(string strPlantCode, string strStationCode,
                                    string strEquipLocCode, string strProcessGroup,
                                    string strEquipLargeTypeCode, string strEquipGroupCode, string strWorkerID)
        {
            SQLS sql = new SQLS();
            DataTable dtPMResult = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strWorkerID, 20);

                //PM체크결과 설비조회 프로시저 실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_Worker", dtParam);

                //PM체크결과 설비정보
                return dtPMResult;
            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMResult.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// PM체크결과 PM미완료설비조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strSearch">조회구분자 E : 설비별 , U : 정비사별</param>
        /// <returns>PM미완료설비정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_PMChk(string strPlantCode, string strStationCode,
                                    string strEquipLocCode, string strProcessGroup,
                                    string strEquipLargeTypeCode, string strEquipGroupCode, string strWorkerID,string strPlanYear,string strFromDate,string strToDate,string strSearch)
        {
            DataTable dtPMChk = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, strWorkerID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSearch", ParameterDirection.Input, SqlDbType.VarChar, strSearch, 1);

                //PM체크결과 PM완료여부조회 프로시저 실행
                dtPMChk = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_PMChk", dtParam);

                return dtPMChk;
            }
            catch (Exception ex)
            {
                return dtPMChk;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMChk.Dispose();
                sql.Dispose();
            }
        }




        /// <summary>
        /// PM체크결과 그리드 셀클릭시 셀의해당하는 대상 PM상세조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strTagetCode">검색대상(설비or정비사)</param>
        /// <param name="strTaget">구분자코드(E : 설비 ,U : 정비사)</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strFormDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>PM정보</returns>
        [AutoComplete]
        public DataTable mfReadPMPlanD_Detail(string strPlantCode, string strTagetCode, string strTaget, string strPlanYear, string strFormDate, string strToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPM = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strTagetCode", ParameterDirection.Input, SqlDbType.VarChar,strTagetCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strTaget", ParameterDirection.Input, SqlDbType.VarChar,strTaget,1);
                sql.mfAddParamDataRow(dtParam,"@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char,strPlanYear,4);
                sql.mfAddParamDataRow(dtParam,"@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar,strFormDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar,strToDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //TagetCode(설비or정비사)의 기간안에있는 PM조회프로시저 실행
                dtPM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMPlanD_Detail", dtParam);

                //PM정보
                return dtPM;
            }
            catch (Exception ex)
            {
                return dtPM;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPM.Dispose();
                sql.Dispose();
            }
        }

        #endregion

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("PMResult")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class PMResult : ServicedComponent
    {

        /// <summary>
        /// 점검결과 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">계획일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과정보</returns>
        [AutoComplete]
        public DataTable mfReadPMResult(string strPlantCode, string strPlanYear, string strEquipCode, string strPMPlanDate, string strLang)
        {
            SQLS sql = new SQLS();

            DataTable dtPMResult = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMResult", dtParam);

                //정보리턴
                return dtPMResult;
            }
            catch(Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMResult.Dispose();
            }
        }


        /// <summary>
        /// 점검결과 조회 : 마이잡 월별 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">계획일</param>
        /// <param name="strUserID"></param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과정보</returns>
        [AutoComplete]
        public DataTable mfReadPMResult_MyJob_Month
            (string strPlantCode
            , string strEquipCode
            , string strPMPlanDate
            , string strUserID
            , string strLang)
        {
            SQLS sql = new SQLS();

            DataTable dtPMResult = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);                
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMResult_MyJob_Month", dtParam);

                //정보리턴
                return dtPMResult;
            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMResult.Dispose();
            } 
        }

        /// <summary>
        /// 예방점검실적정보 등록
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">계획일</param>
        /// <param name="strPMResultCode">점검결과</param>
        /// <param name="strRepairFlag">수리여부</param>
        /// <param name="strResultValue"></param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSavePMResult(string strPlantCode,string strPlanYear,string strEquipCode,string strPMPlanDate,string strPMResultCode, string strRepairFlag,string strResultValue
                                    ,string strUserIP,string strUserID,SqlConnection Sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPMResultCode", ParameterDirection.Input, SqlDbType.VarChar, strPMResultCode, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strPMResultValue", ParameterDirection.Input, SqlDbType.NVarChar, strResultValue, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairFlag", ParameterDirection.Input, SqlDbType.Char, strRepairFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_EQUPMResult", dtParam);

                //처리결과에 따른처리
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //실패 시 리턴
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }
                //성공 시 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;

                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.Dispose();
            }
        }




        
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableMatRepairH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DurableMatRepairH : ServicedComponent
    {
        #region DataTable
        /// <summary>
        /// 저장에 필요한 컬럼만들기
        /// </summary>
        /// <returns>컬럼만들어있는 데이터 테이블</returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtDurableMatRepairH = new DataTable();

            try
            {
                dtDurableMatRepairH.Columns.Add("PlantCode", typeof(string));
                dtDurableMatRepairH.Columns.Add("RepairGICode", typeof(string));
                dtDurableMatRepairH.Columns.Add("GITypeCode", typeof(string));
                dtDurableMatRepairH.Columns.Add("PlanYear", typeof(string));
                dtDurableMatRepairH.Columns.Add("EquipCode", typeof(string));
                dtDurableMatRepairH.Columns.Add("PMPlanDate", typeof(string));
                dtDurableMatRepairH.Columns.Add("RepairReqCode", typeof(string));
                dtDurableMatRepairH.Columns.Add("RepairGIReqDate", typeof(string));
                dtDurableMatRepairH.Columns.Add("RepairGIReqID", typeof(string));
                dtDurableMatRepairH.Columns.Add("EtcDesc", typeof(string));

                return dtDurableMatRepairH;
            }
            catch(Exception ex)
            {
                return dtDurableMatRepairH;
                throw (ex);
            }
            finally
            {
                dtDurableMatRepairH.Dispose();
            }
        }
        #endregion

        #region Select Method

        /// <summary>
        /// 설비출고등록 헤더 코드조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strGITypeCode">타입</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">점검일</param>
        /// <returns>설비출고등록 헤더 코드정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatReapairHCode(string strPlantCode, string strPlanYear,string strGITypeCode,string strEquipCode, string strPMPlanDate)
        {
            SQLS sql = new SQLS();
            DataTable dtHCode = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strGITypeCode", ParameterDirection.Input, SqlDbType.VarChar,strGITypeCode,2);
                sql.mfAddParamDataRow(dtParam,"@i_strPlanYear", ParameterDirection.Input, SqlDbType.VarChar,strPlanYear,4);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar,strPMPlanDate,10);

                //설비출고등록 헤더 코드조회 프로시저 실행
                dtHCode = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatReapairHCode", dtParam);

                //설비출고등록 헤더 코드정보
                return dtHCode;
            }
            catch (Exception ex)
            {
                return dtHCode;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtHCode.Dispose();
                sql.Dispose();
            }
        }

        #region 설비점검

        /// <summary>
        /// 설비점검출고등록 리스트조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGrouopCode">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strWorker">작업자ID</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strFromToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검출고정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairGIH(string strPlantCode, string strStationCode, string strEquipLocCode,
                                                    string strProcessGrouopCode, string strEquipLargeTypeCode, string strEquipGroupCode,
                                                    string strWorker, string strFromDate, string strFromToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRepairGIH = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파리미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGrouopCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorker", ParameterDirection.Input, SqlDbType.VarChar, strWorker, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strFromToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //설비점검출고등록조회 프로시저 실행
                dtRepairGIH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairGIH", dtParam);

                //설비점검출고등록
                return dtRepairGIH;
            }
            catch (Exception ex)
            {
                return dtRepairGIH;
                throw (ex);
            }
            finally
            { sql.mfDisConnect(); dtRepairGIH.Dispose(); sql.Dispose(); }
        }

        /// <summary>
        /// 설비점검출고등록 조회(그룹)
        /// </summary>
        /// <param name="strPlantCode">공장콛,</param>
        /// <param name="strGroupCode">설비그룹코드</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색끝나는일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검출고정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairH(string strPlantCode, string strGroupCode, string strFromDate, string strToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRepairH = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                //파라미터 값저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtRepairH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepair", dtParam);

                //정보리턴
                return dtRepairH;
            }
            catch (Exception ex)
            {
                return dtRepairH;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtRepairH.Dispose();
            }

        }

        /// <summary>
        /// 설비점검출고등록조회(일괄)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">AreaCode</param>
        /// <param name="StationCode">Station코드</param>
        /// <param name="strEquipLocCode"></param>
        /// <param name="strProcGubunCode">공정구분코드</param>
        /// <param name="strEquipType"></param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색끝나는일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검출고정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairHBatch(string strPlantCode, string strAreaCode, string StationCode, string strProcGubunCode,string strEquipLocCode, string strEquipType,
                                                        string strFromDate, string strToDate, string strLang)
        {
            DataTable dtDurableRepair = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터 값저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, StationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipType, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저 실행
                dtDurableRepair = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairBatch", dtParam);

                return dtDurableRepair;

            }
            catch (Exception ex)
            {
                return dtDurableRepair;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableRepair.Dispose();
            }
        }
        #endregion

        #region Repair

        /// <summary>
        /// 금형치공구교체내역
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구교체내역정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairChg(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                // 프로시저 실행
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairChg", dtParam);

                //금형치공구 교체내역정보 리턴 //
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }

        /// <summary>
        /// 설비수리 출고등록
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromDate">수리완료일</param>
        /// <param name="strToDate">수리완료일</param>
        /// <param name="strArea">Area</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strStation">Station</param>
        /// <param name="strEquipProc">설비공정구분</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strEquipTypeCode">설비유형</param>
        /// <param name="strGIFlag"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairH_Repair(string strPlantCode, string strFromDate, string strToDate, string strArea, string strStation
                                                        , string strEquipProc, string strEquipGroupCode,string strEquipLocCode, string strEquipTypeCode, string strGIFlag, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();
                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strArea", ParameterDirection.Input, SqlDbType.VarChar, strArea, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strStation", ParameterDirection.Input, SqlDbType.VarChar, strStation, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipProc", ParameterDirection.Input, SqlDbType.VarChar, strEquipProc, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strGIFlag", ParameterDirection.Input, SqlDbType.VarChar, strGIFlag, 1);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipTypeCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairH_Repair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 설비수리출고등록헤더조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비수리출고등록헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairGIH_Repair(string strPlantCode,string strFromDate,string strToDate,string strStationCode,string strEquipLocCode,
                                                            string strProcessGroupCode,string strEquipLargeTypeCode,string strEquipGroupCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();
                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strStation", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang , 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairGIH_Repair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 설비수리출고등록 상세정보조회
        /// </summary>
        /// <param name="strRepairReqCode">수리요청코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>상세정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairH_Detail_Repair(string strRepairReqCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairH_Detail_Repair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 치공구 점검출고등록정보 리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromFinishDate">점검완료일 from</param>
        /// <param name="strToFinishDate">점검완료일 to</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strEquipName">설비명</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strDurableMatName">치공구명</param>
        /// <param name="strUserID">정비사코드</param>
        /// <param name="strUserName">정비사명</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>테이블</returns>
        [AutoComplete]
        public DataTable mfReadEQUDurableMatRepairGIH_DMMRepairH
            (String strPlantCode
            , String strFromFinishDate
            , String strToFinishDate
            , String strEquipCode
            , String strEquipName
            , String strDurableMatCode
            , String strDurableMatName
            , String strUserID
            , String strUserName
            , String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromFinishDate", ParameterDirection.Input, SqlDbType.VarChar, strFromFinishDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToFinishDate", ParameterDirection.Input, SqlDbType.VarChar, strToFinishDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipName", ParameterDirection.Input, SqlDbType.NVarChar, strEquipName, 100);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableMatName, 100);
                sql.mfAddParamDataRow(dtParmter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strUserName", ParameterDirection.Input, SqlDbType.NVarChar, strUserName, 100);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairGIH_DMMRepairH", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally 
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 치공구 점검출고등록정보 리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strRepairGICode"></param>
        /// <param name="strLang">사용언어</param>
        /// <returns>테이블</returns>
        [AutoComplete]
        public DataTable mfReadEQUDurableMatRepairGIH_DMMRepairH_Detail
            (String strPlantCode
            , String strRepairGICode
            , String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, strRepairGICode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairGIH_DMMRepairH_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }
        #endregion

        #endregion

        #region Insert/Update/Delete
        /// <summary>
        /// PMPlan/Repair EQUDurableMatRepairGIH Save
        /// </summary>
        /// <param name="dtDurableRepairH">헤더 DataTable</param>
        /// <param name="dtDurableRepairD">상세 DataTable</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리 결과값</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatRepairH(DataTable dtDurableRepairH, DataTable dtDurableRepairD, 
                                                //DataTable dtDurableRepairDel, // PMPlant/Repair 통합으로 주석처리
                                                string strUserIP, string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            string strRepairGICode = "";
            SQLS sql = new SQLS();
            try
            {
                //--디비오픈--//
                sql.mfConnect();
                SqlTransaction trans;

                //--Transaction 시작--//
                trans = sql.SqlCon.BeginTransaction();

                #region Header
                DataTable dtParam = sql.mfSetParamDataTable();

                //--파라미터 값저장--//
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGICode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strGITypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["GITypeCode"].ToString(), 2);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, dtDurableRepairH.Rows[0]["PlanYear"].ToString(), 4);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["EquipCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["PMPlanDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairReqCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGIReqDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGIReqID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableRepairH.Rows[0]["EtcDesc"].ToString(), 100);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //--프로시저 실행--//
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUDurableMatRepairGIH", dtParam);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                
                #endregion

                //--처리결과 실패시--//
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                //--처리결과 성공시--//
                else
                {
                    strRepairGICode = ErrRtn.mfGetReturnValue(0);
                    DurableMatRepairD clsDurableRepairD = new DurableMatRepairD();
                    #region PMPlan/Repair 통합으로 주석처리
                    ////-----행 삭제가 있을 시 처리를 한다-----//
                    //if (dtDurableRepairDel.Rows.Count > 0)
                    //{
                    //    strErrRtn = clsDurableRepairD.mfDeleteDurableMatRepairD(dtDurableRepairDel, sql.SqlCon, trans);
                    //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //    if (ErrRtn.ErrNum != 0)
                    //    {
                    //        trans.Rollback();
                    //        return strErrRtn;
                    //    }
                    //}
                    #endregion

                    #region Detail
                    //------설비점검출고등록 상세 저장--------//
                    if (dtDurableRepairD.Rows.Count > 0)
                    {
                        strErrRtn = clsDurableRepairD.mfSaveDurableMatRepairD(strRepairGICode, dtDurableRepairD, strUserIP, strUserID, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                    #endregion

                    trans.Commit();
                }
                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// PMPlan/Repair EQUDurableMatRepairGIH Save
        /// </summary>
        /// <param name="dtDurableRepairH">헤더 DataTable</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns>처리 결과값</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatRepairH_EquipState(DataTable dtDurableRepairH, string strUserIP, string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            string strRepairGICode = "";
            SQLS sql = new SQLS();
            try
            {

                #region Header
                DataTable dtParam = sql.mfSetParamDataTable();

                //--파라미터 값저장--//
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGICode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strGITypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["GITypeCode"].ToString(), 2);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, dtDurableRepairH.Rows[0]["PlanYear"].ToString(), 4);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["EquipCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["PMPlanDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairReqCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGIReqDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGIReqID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableRepairH.Rows[0]["EtcDesc"].ToString(), 100);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //--프로시저 실행--//
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUDurableMatRepairGIH", dtParam);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                #endregion

                //--처리결과 실패시--//
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }
               
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                
                sql.Dispose();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtDurableMatRepairH"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatRepairH_2(DataTable dtDurableMatRepairH, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {        
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                
                    //--파라미터값저장--//
                    DataTable dtParam = sql.mfSetParamDataTable();

                    //--파라미터 값저장--//
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairH.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairH.Rows[0]["RepairGICode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGITypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairH.Rows[0]["GITypeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, dtDurableMatRepairH.Rows[0]["PlanYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairH.Rows[0]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairH.Rows[0]["PMPlanDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairH.Rows[0]["RepairReqCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairH.Rows[0]["RepairGIReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairH.Rows[0]["RepairGIReqID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMatRepairH.Rows[0]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                     
                    //--프로시저실행--//
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUDurableMatRepairGIH", dtParam);

                    //--처리결과값 구별--//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--처리결과 값에 따른처리--//
                    //--실패시 For문이 멈추고 리턴--//
                    //if (ErrRtn.ErrNum != 0)
                    //{
                    //    break; 
                    //}

              

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtDurableRepairH">헤더 DataTable</param>
        /// <param name="dtDurableRepairD">상세 DataTable</param>
        /// <param name="dtPMUseSPH">정비결과헤더</param>
        /// <param name="dtPMUseSP">정비결과상세정보</param>
        /// <param name="dtSPStockMinus">SP 현재고 테이블: - 처리 정보</param>
        /// <param name="dtSPStockPlus">SP 현재고 테이블: + 처리 정보</param>
        /// <param name="dtSPStockHisMius">SP 재고이력테이블 : - 처리 정보</param>
        /// <param name="dtSPStockHisPlus">SP 재고이력테이블 : + 처리 정보</param>
        /// <param name="dtEQUSPBOM">SP BOM 테이블 : 설비 교체정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리 결과값</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatRepairH_POP(DataTable dtDurableRepairH, DataTable dtDurableRepairD
                                                    , DataTable dtPMUseSPH, DataTable dtPMUseSP 
                                                    , DataTable dtSPStockMinus, DataTable dtSPStockPlus
                                                    , DataTable dtSPStockHisMius, DataTable dtSPStockHisPlus
                                                    , DataTable dtEQUSPBOM,           
                                                    string strUserIP, string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            string strRepairGICode = "";
            SQLS sql = new SQLS();
            try
            {
                //--디비오픈--//
                sql.mfConnect();
                SqlTransaction trans;

                //--Transaction 시작--//
                trans = sql.SqlCon.BeginTransaction();

                if (dtDurableRepairH.Rows.Count > 0)
                {

                    #region Header
                    DataTable dtParam = sql.mfSetParamDataTable();

                    //--파라미터 값저장--//
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGICode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGITypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["GITypeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, dtDurableRepairH.Rows[0]["PlanYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["PMPlanDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairReqCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGIReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[0]["RepairGIReqID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableRepairH.Rows[0]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //--프로시저 실행--//
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUDurableMatRepairGIH", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                    #endregion

                    //--처리결과 실패시--//
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    //--처리결과 성공시--//
                    else
                    {
                        strRepairGICode = ErrRtn.mfGetReturnValue(0);
                        DurableMatRepairD clsDurableRepairD = new DurableMatRepairD();

                        #region Detail
                        //------설비점검출고등록 상세 저장--------//
                        if (dtDurableRepairD.Rows.Count > 0)
                        {
                            strErrRtn = clsDurableRepairD.mfSaveDurableMatRepairD(strRepairGICode, dtDurableRepairD, strUserIP, strUserID, sql.SqlCon, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }
                        #endregion
                    }
                }

                if (dtPMUseSPH.Rows.Count > 0)
                {
                    PMUseSP clsSP = new PMUseSP();
                    strErrRtn = clsSP.mfSavePMUseSPH_POP(dtPMUseSPH, dtPMUseSP
                                   , dtSPStockMinus, dtSPStockPlus
                                   , dtSPStockHisMius, dtSPStockHisPlus
                                   , dtEQUSPBOM, strUserIP, strUserID, sql, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if(ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 신규Lot정보의 설비투입
        /// </summary>
        /// <param name="dtDurableRepairH">헤더 DataTable</param>
        /// <param name="dtDurableRepairD">상세 DataTable</param>
        /// <param name="dtRepairLotHistory">신규Lot정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리 결과값</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatRepairH_RepairLot(DataTable dtDurableRepairH, DataTable dtDurableRepairD,DataTable dtRepairLotHistory,
                                                string strUserIP, string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            string strRepairGICode = "";
            SQLS sql = new SQLS();
            try
            {

               
                for (int i = 0; i < dtDurableRepairH.Rows.Count; i++)
                {
                    #region Header

                    DataTable dtParam = sql.mfSetParamDataTable();

                    //--파라미터 값저장--//
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["RepairGICode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGITypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["GITypeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, dtDurableRepairH.Rows[i]["PlanYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["PMPlanDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["RepairReqCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["RepairGIReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["RepairGIReqID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableRepairH.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //--프로시저 실행--//
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUDurableMatRepairGIH", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                     #endregion

                    //--처리결과 실패시--//
                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;
                    }
                    //--처리결과 성공시--//
                    else
                    {
                        strRepairGICode = ErrRtn.mfGetReturnValue(0);
                        

                        #region Detail
                        //------설비점검출고등록 상세 저장--------//

                        for (int j = 0; j < dtDurableRepairD.Rows.Count; j++)
                        {
                            //수리요청번호가 같은 상세정보를 저장한다.
                            if (dtDurableRepairH.Rows[i]["RepairReqCode"].ToString().Equals(dtDurableRepairD.Rows[j]["RepairReqCode"].ToString()))
                            {
                                //--파라미터값저장--//
                                DataTable dtParamt = sql.mfSetParamDataTable();
                                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["PlantCode"].ToString(), 10);
                                sql.mfAddParamDataRow(dtParamt, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, strRepairGICode, 20);
                                sql.mfAddParamDataRow(dtParamt, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableRepairD.Rows[j]["Seq"].ToString());
                                sql.mfAddParamDataRow(dtParamt, "@i_strRepairGIGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["RepairGIGubunCode"].ToString(), 3);
                                sql.mfAddParamDataRow(dtParamt, "@i_strCurDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["CurDurableMatCode"].ToString(), 20);
                                sql.mfAddParamDataRow(dtParamt, "@i_strCurLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["CurLotNo"].ToString(), 40);
                                sql.mfAddParamDataRow(dtParamt, "@i_intCurQty", ParameterDirection.Input, SqlDbType.Int, dtDurableRepairD.Rows[j]["CurQty"].ToString());
                                sql.mfAddParamDataRow(dtParamt, "@i_strChgDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["ChgDurableInventoryCode"].ToString(), 10);
                                sql.mfAddParamDataRow(dtParamt, "@i_strChgDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["ChgDurableMatCode"].ToString(), 20);
                                sql.mfAddParamDataRow(dtParamt, "@i_strChgLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["ChgLotNo"].ToString(), 40);
                                sql.mfAddParamDataRow(dtParamt, "@i_intChgQty", ParameterDirection.Input, SqlDbType.Int, dtDurableRepairD.Rows[j]["ChgQty"].ToString());
                                sql.mfAddParamDataRow(dtParamt, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["UnitCode"].ToString(), 10);
                                sql.mfAddParamDataRow(dtParamt, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableRepairD.Rows[j]["EtcDesc"].ToString(), 100);
                                sql.mfAddParamDataRow(dtParamt, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairD.Rows[j]["CancelFlag"].ToString(), 1);
                                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                                sql.mfAddParamDataRow(dtParamt, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                                sql.mfAddParamDataRow(dtParamt, "@o_strSeq", ParameterDirection.Output, SqlDbType.VarChar, 100);
                                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                                //--프로시저실행--//
                                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUDurableMatRepairD", dtParamt);

                                //--처리결과값 구별--//
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                //--처리결과 값에 따른처리--//
                                //--실패시 For문이 멈추고 리턴--//
                                if (ErrRtn.ErrNum != 0)
                                {
                                    return strErrRtn;
                                }
                            }

                        }
                        #endregion

                    }
                }

                //설비투입Lot정보가 모두 성공인 경우 설비투입Lot정보의 Flag를 변경 시켜준다.
                if (ErrRtn.ErrNum.Equals(0))
                {
                    

                    strErrRtn = mfSaveReapirLotHistory_WriteFlag(dtRepairLotHistory, strUserIP, strUserID, sqlcon, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        return strErrRtn;
                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 신규 Lot정보 투입시 WriteFlag T 처리
        /// </summary>
        /// <param name="dtRepairLotHistory"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveReapirLotHistory_WriteFlag(DataTable dtRepairLotHistory,string strUserIP,string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i < dtRepairLotHistory.Rows.Count; i++)
                {
                    //파라미터정보 저장
                    DataTable dtParam = sql.mfSetParamDataTable();


                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairLotHistory.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairLotHistory.Rows[i]["RepairReqCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);


                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //신규 Lot정보 투입시 WriteFlag T 처리 프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQURepairLotHistory_WriteFlag", dtParam);

                    //처리 실패시 실패메시지등 리턴
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { sql.Dispose(); }
        }
    

        /// <summary>
        /// 수명관리 정비(치공구교체)
        /// </summary>
        /// <param name="dtDurableRepairH">헤더 DataTable</param>
        /// <param name="dtDurableRepairD">상세 DataTable</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리 결과값</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatRepairH_Spec(DataTable dtDurableRepairH, DataTable dtDurableRepairD,
                                                string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            string strRepairGICode = "";
            SQLS sql = new SQLS();
            try
            {


                for (int i = 0; i < dtDurableRepairH.Rows.Count; i++)
                {
                    #region Header

                    DataTable dtParam = sql.mfSetParamDataTable();

                    //--파라미터 값저장--//
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["RepairGICode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGITypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["GITypeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, dtDurableRepairH.Rows[i]["PlanYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["PMPlanDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["RepairReqCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["RepairGIReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIReqID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairH.Rows[i]["RepairGIReqID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableRepairH.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //--프로시저 실행--//
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUDurableMatRepairGIH", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    #endregion

                    //--처리결과 실패시--//
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                    //--처리결과 성공시--//
                    else
                    {
                        strRepairGICode = ErrRtn.mfGetReturnValue(0);


                        #region Detail

                        //------설비점검출고등록 상세 저장--------//
                        if (dtDurableRepairD.Rows.Count > 0)
                        {
                            DurableMatRepairD clsDurableMatRepairD = new DurableMatRepairD();
                            strErrRtn = clsDurableMatRepairD.mfSaveDurableMatRepairD(strRepairGICode, dtDurableRepairD, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                break;
                            }
                        }

                        #endregion

                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비점검출고등록  삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strRepairGICode">점검출고코드</param>
        /// <returns>처리결과 값</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableMatRepairH(string strPlantCode, string strRepairGICode)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();


            try
            {
                //--디비연결--//
                sql.mfConnect();

                SqlTransaction trans;

                //--Transaction시작--//
                trans = sql.SqlCon.BeginTransaction();

                //--파라미터값 저장--//
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam,"@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, strRepairGICode, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //--프로시저 실행--//
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUDurableMatRepairGIH", dtParam);

                //--처리결과--//
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //--처리실패시 트랜젝션을 끝내고 에러메세지를 리턴한다--//
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                    //-- 처리 성공시 트랜젝션을 커밋한다--//
                else
                {
                    trans.Commit();
                }
                return strErrRtn;

            }
            catch(Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        #endregion
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableMatRepairD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DurableMatRepairD : ServicedComponent
    {
        #region DataTable
        /// <summary>
        /// 저장 , 삭제에 필요한 컬럼추가하기
        /// </summary>
        /// <returns>컬럼추가된 빈데이터테이블</returns>
        public DataTable mfsetDataInfo()
        {
            DataTable dtSetDataColumn = new DataTable();
            try
            {
                dtSetDataColumn.Columns.Add("PlantCode", typeof(string));
                dtSetDataColumn.Columns.Add("RepairGICode", typeof(string));
                dtSetDataColumn.Columns.Add("Seq", typeof(string));
                dtSetDataColumn.Columns.Add("RepairGIGubunCode", typeof(string));
                dtSetDataColumn.Columns.Add("CurDurableMatCode", typeof(string));
                dtSetDataColumn.Columns.Add("CurLotNo", typeof(string));
                dtSetDataColumn.Columns.Add("CurQty", typeof(string));
                dtSetDataColumn.Columns.Add("ChgDurableInventoryCode", typeof(string));
                dtSetDataColumn.Columns.Add("ChgDurableMatCode", typeof(string));
                dtSetDataColumn.Columns.Add("ChgLotNo", typeof(string));
                dtSetDataColumn.Columns.Add("UnitCode", typeof(string));
                dtSetDataColumn.Columns.Add("ChgQty", typeof(string));
                dtSetDataColumn.Columns.Add("EtcDesc", typeof(string));
                dtSetDataColumn.Columns.Add("CancelFlag", typeof(string));

                return dtSetDataColumn;
            }
            catch(Exception ex)
            {
                return dtSetDataColumn;
                throw (ex);
            }
            finally
            {
                dtSetDataColumn.Dispose();
            }
        }
        #endregion

        #region Select Method

        #region PMPlan
        /// <summary>
        /// 설비점검출고등록 상세 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strRepairGICode">점검출고코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검출고상세정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairD(string strPlantCode, string strEquipCode, string strRepairGICode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableMatRepairD = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar,strRepairGICode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);
                //프로시저실행
                dtDurableMatRepairD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairD", dtParam);
                //정보 리턴
                return dtDurableMatRepairD;

            }
            catch (Exception ex)
            {
                return dtDurableMatRepairD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatRepairD.Dispose();
            }
        }


        /// <summary>
        /// 점검결과 조회 : 마이잡 월별 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">정비사</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">계획일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairD_MyJob_Month
            (string strPlantCode
            , string strEquipCode
            , string strPMPlanDate
            , string strUserID
            , string strLang)
        {
            SQLS sql = new SQLS();

            DataTable dtPMResult = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairD_MyJob_Month", dtParam);

                //정보리턴
                return dtPMResult;
            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMResult.Dispose();
            }
        }

        #endregion

        #region Common
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <param name="strRepairGICode"></param>
        /// <param name="strRepairReqCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDurableMatRepairD_Repair(string strPlantCode, string strEquipCode, string strRepairGICode, string strRepairReqCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableMatRepairD = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, strRepairGICode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtDurableMatRepairD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUDurableMatRepairD_Repair", dtParam);
                //정보 리턴
                return dtDurableMatRepairD;

            }
            catch (Exception ex)
            {
                return dtDurableMatRepairD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatRepairD.Dispose();
            }
        }
        #endregion

        #endregion

        #region Insert/Update/Delete Method
        /// <summary>
        /// 설비점검출고등록 상세 등록
        /// </summary>
        /// <param name="strRepairGICode">점검출고코드</param>
        /// <param name="dtDurableMatRepairD">상세정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatRepairD(string strRepairGICode, DataTable dtDurableMatRepairD, string strUserIP, string strUserID, SqlConnection Sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i < dtDurableMatRepairD.Rows.Count; i++)
                {
                    //--파라미터값저장--//
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, strRepairGICode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableMatRepairD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["RepairGIGubunCode"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParam, "@i_strCurDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["CurDurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCurLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["CurLotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_intCurQty", ParameterDirection.Input, SqlDbType.Int, dtDurableMatRepairD.Rows[i]["CurQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strChgDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["ChgDurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strChgDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["ChgDurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strChgLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["ChgLotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_intChgQty", ParameterDirection.Input, SqlDbType.Int, dtDurableMatRepairD.Rows[i]["ChgQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["UnitCode"].ToString(),10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMatRepairD.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["CancelFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strSeq", ParameterDirection.Output, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //--프로시저실행--//
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_EQUDurableMatRepairD", dtParam);

                    //--처리결과값 구별--//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--처리결과 값에 따른처리--//
                    //--실패시 For문이 멈추고 리턴--//
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }

                }
                
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// %사용안합% 설비점검출고등록 상세삭제
        /// </summary>
        /// <param name="dtDurableRepairDel">삭제할 정보</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableMatRepairD(DataTable dtDurableRepairDel, SqlConnection Sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i<dtDurableRepairDel.Rows.Count; i++)
                {
                    //--파라미터값저장--//
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtDurableRepairDel.Rows[i]["PlantCode"].ToString(),10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableRepairDel.Rows[i]["RepairGICode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableRepairDel.Rows[i]["Seq"].ToString());
                    
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //--프로시저 실행--//
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Delete_EQUDurableMatRepairD", dtParam);

                    //--처리결과값 처리--//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--처리실패시 for문을 빠져나간다.--//
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //--처리결과 리턴--//
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검출고등록 상세 등록
        /// </summary>
        /// <param name="strRepairGICode">점검출고코드</param>
        /// <param name="dtDurableMatRepairD">상세정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatRepairD_POP(string strRepairGICode, DataTable dtDurableMatRepairD, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDurableMatRepairD.Rows.Count; i++)
                {
                    //--파라미터값저장--//
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, strRepairGICode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableMatRepairD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairGIGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["RepairGIGubunCode"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParam, "@i_strCurDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["CurDurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCurLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["CurLotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_intCurQty", ParameterDirection.Input, SqlDbType.Int, dtDurableMatRepairD.Rows[i]["CurQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strChgDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["ChgDurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strChgDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["ChgDurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strChgLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["ChgLotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_intChgQty", ParameterDirection.Input, SqlDbType.Int, dtDurableMatRepairD.Rows[i]["ChgQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMatRepairD.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatRepairD.Rows[i]["CancelFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strSeq", ParameterDirection.Output, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //--프로시저실행--//
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUDurableMatRepairD", dtParam);

                    //--처리결과값 구별--//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //--처리결과 값에 따른처리--//
                    //--실패시 For문이 멈추고 리턴--//
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        #endregion
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("PMUseSP")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class PMUseSP : ServicedComponent
    {
        /// <summary>
        /// DataSetInfo
        /// </summary>
        /// <returns>컬럼만있는 빈 테이블</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtPMUseSP = new DataTable();
            try
            {
                dtPMUseSP.Columns.Add("Seq", typeof(string));
                dtPMUseSP.Columns.Add("CurSparePartCode", typeof(string));
                dtPMUseSP.Columns.Add("CurInputQty", typeof(string));
                dtPMUseSP.Columns.Add("ChgSPInventoryCode", typeof(string));
                dtPMUseSP.Columns.Add("ChgSparePartCode", typeof(string));
                dtPMUseSP.Columns.Add("ChgInputQty", typeof(string));
                dtPMUseSP.Columns.Add("ChgDesc", typeof(string));
                dtPMUseSP.Columns.Add("CancelFlag", typeof(string));

                return dtPMUseSP;

            }
            catch (Exception ex)
            {
                return dtPMUseSP;
                throw (ex);
            }
            finally
            {
                dtPMUseSP.Dispose();
            }
        }


        /// <summary>
        /// 헤더정보 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetHeaderDataInfo()
        {
            DataTable dtHeader = new DataTable();
            try
            {
                dtHeader.Columns.Add("PlantCode",typeof(string));
                dtHeader.Columns.Add("PlanYear",typeof(string));
                dtHeader.Columns.Add("EquipCode",typeof(string));
                dtHeader.Columns.Add("PMPlanDate",typeof(string));
                dtHeader.Columns.Add("PMUseDate",typeof(string));
                dtHeader.Columns.Add("SPUseChargeID",typeof(string));
                dtHeader.Columns.Add("SPUseEtcDesc",typeof(string));

                return dtHeader;
            }
            catch (Exception ex)
            {
                return dtHeader;
                throw(ex);
            }
            finally
            {
                dtHeader.Dispose();
            }
        }
       
        /// <summary>
        /// 정비결과등록 조회(Area,Station,EquipProcGubun)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <param name="strStationCode">Station코드</param>
        /// <param name="strProcGubun">공정구분코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup"></param>
        /// <param name="strEquipLargeTypeCode">설비유형</param>
        /// <param name="strWorkerID"></param>
        /// <param name="strFromDate">FromDate</param>
        /// <param name="strToDate">ToDate</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>정비결과정보</returns>
        [AutoComplete]
        public DataTable mfReadPMUseSP(string strPlantCode, string strWorkerID, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode,
                                        string strEquipGroupCode,string strAreaCode, string strProcGubun, 
                                        string strFromDate, string strToDate, string strLang)
        {
            DataTable dtPMUseSP = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorker", ParameterDirection.Input, SqlDbType.VarChar, strWorkerID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubun, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtPMUseSP = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMUseSPH", dtParam);
                //정보리턴
                return dtPMUseSP;
            }
            catch (Exception ex)
            {
                return dtPMUseSP;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMUseSP.Dispose();
            }
        
        }

        /// <summary>
        /// 점검결과 조회 : 마이잡 월별 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">정비사</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">계획일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과정보</returns>
        [AutoComplete]
        public DataTable mfReadPMUseSP_MyJob_Month
            (string strPlantCode
            , string strEquipCode
            , string strPMPlanDate
            , string strUserID
            , string strLang)
        {
            SQLS sql = new SQLS();

            DataTable dtPMResult = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMUseSP_MyJob_Month", dtParam);

                //정보리턴
                return dtPMResult;
            }
            catch (Exception ex)
            {
                return dtPMResult;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMResult.Dispose();
            }
        }
        
        /// <summary>
        /// 정비결과등록 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">점검일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>정비결과등록 상세</returns>
        [AutoComplete]
        public DataTable mfReadPMUseSP(string strPlantCode, string strEquipCode, string strPMPlanDate, string strLang)
        {
            DataTable dtPMUseSPD = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //-- 디비오픈 --//
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                //-- 파라미터값 저장 --//
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //-- 프로시저 실행 --//
                dtPMUseSPD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUPMUseSP", dtParam);

                //-- 정보리턴 --//
                return dtPMUseSPD;

            }
            catch (Exception ex)
            {
                return dtPMUseSPD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtPMUseSPD.Dispose();
            }

        }


        /// <summary>
        /// 정비결등록
        /// </summary>
        /// <param name="dtPMUseSPH">정비결과헤더</param>
        /// <param name="dtPMUseSP">정비결과상세정보</param>
        /// <param name="dtSPStockMinus">SP 현재고 테이블: - 처리 정보</param>
        /// <param name="dtSPStockPlus">SP 현재고 테이블: + 처리 정보</param>
        /// <param name="dtSPStockHisMius">SP 재고이력테이블 : - 처리 정보</param>
        /// <param name="dtSPStockHisPlus">SP 재고이력테이블 : + 처리 정보</param>
        /// <param name="dtEQUSPBOM">SP BOM 테이블 : 설비 교체정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSavePMUseSPH(DataTable dtPMUseSPH, DataTable dtPMUseSP 
                                   , DataTable dtSPStockMinus, DataTable dtSPStockPlus
                                   , DataTable dtSPStockHisMius, DataTable dtSPStockHisPlus
                                   , DataTable dtEQUSPBOM
                                   , string strUserIP, string strUserID )
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //정보저장 
                string strPlantCode = dtPMUseSPH.Rows[0]["PlantCode"].ToString();   //공장
                string strPlanYear = dtPMUseSPH.Rows[0]["PlanYear"].ToString();     //계획년도
                string strEquipCode = dtPMUseSPH.Rows[0]["EquipCode"].ToString();   //설비코드
                string strPMPlanDate = dtPMUseSPH.Rows[0]["PMPlanDate"].ToString(); //점검일
                string strUseDate = dtPMUseSPH.Rows[0]["PMUseDate"].ToString();     //사용일
                string strSPUseChargID = dtPMUseSPH.Rows[0]["SPUseChargeID"].ToString();//사용담당자
                string strSPUseEtcDesc = dtPMUseSPH.Rows[0]["SPUseEtcDesc"].ToString(); //특이사항

                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                #region Header 저장
                //-- 파라미터 값 저장 --//
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam,"@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,100);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char,strPlanYear,4);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar,strPMPlanDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strPMUseDate", ParameterDirection.Input, SqlDbType.VarChar,strUseDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strSPUseChargeID", ParameterDirection.Input, SqlDbType.VarChar,strSPUseChargID,20);
                sql.mfAddParamDataRow(dtParam,"@i_strSPUseEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar,strSPUseEtcDesc,100);
                
                sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP, 15);
                sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID, 20);
                
                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);
                
                //-- 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon,trans,"up_Update_EQUPMUseSPH",dtParam);
                

                //-- 처리결과 판단
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                #endregion

                // -- 실패 할경우 RollBack
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    
                }
                    //-- 성공시 상세 저장 및 재고처리
                else
                {
                    //상세저장
                    if (dtPMUseSP.Rows.Count > 0)
                    {
                        strErrRtn = mfSavePMUseSP(strPlantCode, strPlanYear, strEquipCode, strPMPlanDate, dtPMUseSP, strUserIP, strUserID, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        // -- 상세저장 실패시 RollbackTran --//
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    #region 재고관련

                    //1.SparePart 현 재고 테이블 (EQUStock)  교체수량 - 
                    QRPEQU.BL.EQUSPA.SPStock clsSPstock = new QRPEQU.BL.EQUSPA.SPStock();
                    if (dtSPStockMinus.Rows.Count > 0)
                    {
                        strErrRtn = clsSPstock.mfSaveSPStock(dtSPStockMinus, strUserIP, strUserID, sql, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 재고 테이블 저장 처리결과가 실패시 롤백 후 에러메세지 리턴
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //2.SparePart 재고 이력 테이블(EQUSPStockMoveHist)
                    QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                    if (dtSPStockHisMius.Rows.Count > 0)
                    {
                        strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockHisMius, strUserIP, strUserID, sql, trans);
                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //SP재고 이력 테이블 저장 처리결과가 실패시 롤백 후 에러메세제ㅣ
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //3.SParePartBOM테이블 
                    QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                    if ( dtEQUSPBOM.Rows.Count > 0 )
                    {
                        strErrRtn = clsSPTransferD.mfSaveMASEquipSPBOM_Chg(dtEQUSPBOM, strUserIP, strUserID, sql, trans);
                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //SparePartBOM테이블 저장 실패시 롤백
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //4. SparePart 현 재고 테이블 현재창고 + 데이터
                    if (dtSPStockPlus.Rows.Count > 0)
                    {
                        strErrRtn = clsSPstock.mfSaveSPStock(dtSPStockPlus, strUserIP, strUserID, sql, trans);
                        
                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //5. SparePart 재고 이력 테이블 (현재창고 + 데이터)
                    if (dtSPStockHisPlus.Rows.Count > 0)
                    {
                        strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockHisPlus, strUserIP, strUserID, sql, trans);

                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                    #endregion

                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
            	ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw(ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();

            }
        }


        /// <summary>
        /// 정비결등록
        /// </summary>
        /// <param name="dtPMUseSPH">정비결과헤더</param>
        /// <param name="dtPMUseSP">정비결과상세정보</param>
        /// <param name="dtSPStockMinus">SP 현재고 테이블: - 처리 정보</param>
        /// <param name="dtSPStockPlus">SP 현재고 테이블: + 처리 정보</param>
        /// <param name="dtSPStockHisMius">SP 재고이력테이블 : - 처리 정보</param>
        /// <param name="dtSPStockHisPlus">SP 재고이력테이블 : + 처리 정보</param>
        /// <param name="dtEQUSPBOM">SP BOM 테이블 : 설비 교체정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sql">SQLS</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSavePMUseSPH_POP(DataTable dtPMUseSPH, DataTable dtPMUseSP
                                   , DataTable dtSPStockMinus, DataTable dtSPStockPlus
                                   , DataTable dtSPStockHisMius, DataTable dtSPStockHisPlus
                                   , DataTable dtEQUSPBOM
                                   , string strUserIP, string strUserID,SQLS sql,SqlTransaction trans)
        {
            
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //정보저장 
                string strPlantCode = dtPMUseSPH.Rows[0]["PlantCode"].ToString();   //공장
                string strPlanYear = dtPMUseSPH.Rows[0]["PlanYear"].ToString();     //계획년도
                string strEquipCode = dtPMUseSPH.Rows[0]["EquipCode"].ToString();   //설비코드
                string strPMPlanDate = dtPMUseSPH.Rows[0]["PMPlanDate"].ToString(); //점검일
                string strUseDate = dtPMUseSPH.Rows[0]["PMUseDate"].ToString();     //사용일
                string strSPUseChargID = dtPMUseSPH.Rows[0]["SPUseChargeID"].ToString();//사용담당자
                string strSPUseEtcDesc = dtPMUseSPH.Rows[0]["SPUseEtcDesc"].ToString(); //특이사항


                #region Header 저장
                //-- 파라미터 값 저장 --//
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPMUseDate", ParameterDirection.Input, SqlDbType.VarChar, strUseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSPUseChargeID", ParameterDirection.Input, SqlDbType.VarChar, strSPUseChargID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strSPUseEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, strSPUseEtcDesc, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //-- 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUPMUseSPH", dtParam);


                //-- 처리결과 판단
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                #endregion

                // -- 실패 할경우 RollBack
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();

                }
                //-- 성공시 상세 저장 및 재고처리
                else
                {
                    //상세저장
                    if (dtPMUseSP.Rows.Count > 0)
                    {
                        strErrRtn = mfSavePMUseSP(strPlantCode, strPlanYear, strEquipCode, strPMPlanDate, dtPMUseSP, strUserIP, strUserID, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        // -- 상세저장 실패시 RollbackTran --//
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    #region 재고관련

                    //1.SparePart 현 재고 테이블 (EQUStock)  교체수량 - 
                    QRPEQU.BL.EQUSPA.SPStock clsSPstock = new QRPEQU.BL.EQUSPA.SPStock();
                    if (dtSPStockMinus.Rows.Count > 0)
                    {
                        strErrRtn = clsSPstock.mfSaveSPStock(dtSPStockMinus, strUserIP, strUserID, sql, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 재고 테이블 저장 처리결과가 실패시 롤백 후 에러메세지 리턴
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //2.SparePart 재고 이력 테이블(EQUSPStockMoveHist)
                    QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();
                    if (dtSPStockHisMius.Rows.Count > 0)
                    {
                        strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockHisMius, strUserIP, strUserID, sql, trans);
                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //SP재고 이력 테이블 저장 처리결과가 실패시 롤백 후 에러메세제ㅣ
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //3.SParePartBOM테이블 
                    QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                    if (dtEQUSPBOM.Rows.Count > 0)
                    {
                        strErrRtn = clsSPTransferD.mfSaveMASEquipSPBOM_Chg(dtEQUSPBOM, strUserIP, strUserID, sql, trans);
                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //SparePartBOM테이블 저장 실패시 롤백
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //4. SparePart 현 재고 테이블 현재창고 + 데이터
                    if (dtSPStockPlus.Rows.Count > 0)
                    {
                        strErrRtn = clsSPstock.mfSaveSPStock(dtSPStockPlus, strUserIP, strUserID, sql, trans);

                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //5. SparePart 재고 이력 테이블 (현재창고 + 데이터)
                    if (dtSPStockHisPlus.Rows.Count > 0)
                    {
                        strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockHisPlus, strUserIP, strUserID, sql, trans);

                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                    #endregion

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                
            }
            finally
            {

            }
        }


        /// <summary>
        /// 정비결과등록 상세 저장
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">계획일</param>
        /// <param name="dtPMUseSP">SP관한정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSavePMUseSP(string strPlantCode,string strPlanYear,string strEquipCode,string strPMPlanDate, DataTable dtPMUseSP,string strUserIP,string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                for (int i = 0; i < dtPMUseSP.Rows.Count; i++ )
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    //-- 파라미터값저장 --//
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar, strPMPlanDate, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtPMUseSP.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCurSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMUseSP.Rows[i]["CurSparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intCurInputQty", ParameterDirection.Input, SqlDbType.Int, dtPMUseSP.Rows[i]["CurInputQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strChgSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMUseSP.Rows[i]["ChgSPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strChgSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMUseSP.Rows[i]["ChgSparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intChgInputQty", ParameterDirection.Input, SqlDbType.Int, dtPMUseSP.Rows[i]["ChgInputQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strChgDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtPMUseSP.Rows[i]["ChgDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.Char, dtPMUseSP.Rows[i]["CancelFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //-- 프로시저 실행 --//
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQUPMUseSP", dtParam);

                    //-- 처리결과 --//
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //-- 실패했을 경우 break--//
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }

        }

        /// <summary>
        /// 정비결과등록 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strPMPlanDate">계획일</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeletePMUseSP(string strPlantCode,string strPlanYear,string strEquipCode,string strPMPlanDate,string strUserIP,string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                // -- 파라미터 저장 -- //
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char,strPlanYear,4);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strPMPlanDate", ParameterDirection.Input, SqlDbType.VarChar,strPMPlanDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Input, SqlDbType.VarChar,8000);
                // -- 프로시저 실행 --//
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQUPMUseSP", dtParam);

                //-------- 처리결과----------//
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //--- 실패시 Rollback----//
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                //-- 성공시 Commit ---//
                else
                {
                    trans.Commit();
                }
                // 처리결과 리턴 //
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }
}
   