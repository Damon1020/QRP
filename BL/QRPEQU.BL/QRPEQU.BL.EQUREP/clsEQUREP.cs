﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;
using QRPDB;


[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPEQU.BL.EQUREP
{
    

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairReq")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class RepairReq : ServicedComponent
    {
        
        private SqlConnection m_DBConn;

        public RepairReq()
        {
        }
        public RepairReq(string strConn)
        {
            m_DBConn = new SqlConnection(strConn);
            m_DBConn.Open();
        }
        /// <summary>
        /// MES데이터 정보
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetMESData()
        {
            //MES로보낼 데이터 정보
            DataTable dtMESInfo = new DataTable();

            try
            {
                dtMESInfo.Columns.Add("EQPID", typeof(string));         //설비코드
                dtMESInfo.Columns.Add("EQPSTATE", typeof(string));      //설비상태
                dtMESInfo.Columns.Add("PRODUCTSPECID", typeof(string)); //품종교체시제품코드
                dtMESInfo.Columns.Add("MACHINERECIPE", typeof(string)); //설비Recipe
                dtMESInfo.Columns.Add("REASONCODE", typeof(string));    //Down이나품종교체코드
                dtMESInfo.Columns.Add("EMERGENCYFLAG", typeof(string)); //긴급처리여부
                dtMESInfo.Columns.Add("COMMENT", typeof(string));       //코멘트
                dtMESInfo.Columns.Add("TOOLLIST", typeof(string));      //

                return dtMESInfo;
            }
            catch (Exception ex)
            {
                return dtMESInfo;
                throw (ex);
            }
            finally
            {
                dtMESInfo.Dispose();
            }
        }

        #region Select Method
        /// <summary>
        /// 수리결과등록 리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromDate">From날짜</param>
        /// <param name="strToDate">To날짜</param>
        /// <param name="strArea">AreaCode</param>
        /// <param name="strStation">StationCode</param>
        /// <param name="strEquipProc">설비공정구분코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>수리결과등록 리스트</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairResult(string strPlantCode, string strFromDate, string strToDate, string strArea, string strStation, string strEquipProc, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strArea", ParameterDirection.Input, SqlDbType.VarChar, strArea, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strStation", ParameterDirection.Input, SqlDbType.VarChar, strStation, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipProc", ParameterDirection.Input, SqlDbType.VarChar, strEquipProc, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_Result", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_Receipt", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 设备状态管理，TOOLKIT管理
        /// </summary>
        /// <param name="strEquipCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEQURepairReq_ToolValue(string strEquipCode)
        {
            SQLS sql = new SQLS();
            DataTable dtToolValue = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);

                dtToolValue = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUTOOLKIT_Value", dtParam);

                return dtToolValue;
            }
            catch (Exception ex)
            {
                return dtToolValue;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtToolValue.Dispose();
            }
        }

        /// <summary>
        /// 수리결과등록 상세조회
        /// </summary>
        /// <param name="strRepairReqCode">수리요청번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>수리결과 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairResult_Detail(string strRepairReqCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_Result_Detail", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_Receipt", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 수리요청 리스트
        /// </summary>
        /// <param name="strPlantCode">공장번호</param>
        /// <param name="strFromDate">From날짜</param>
        /// <param name="strToDate">To날짜</param>
        /// <param name="strArea">AreaCode</param>
        /// <param name="strStation">StationCode</param>
        /// <param name="strEquipProc">설비공정구분코드</param>
        /// <param name="strReceipt">접수여부(T/F)</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>수리요청 리스트</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairRequest(string strPlantCode, string strFromDate, string strToDate, string strArea, string strStation, string strEquipProc, string strReceipt, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strArea", ParameterDirection.Input, SqlDbType.VarChar, strArea, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strStation", ParameterDirection.Input, SqlDbType.VarChar, strStation, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipProc", ParameterDirection.Input, SqlDbType.VarChar, strEquipProc, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strReceipt", ParameterDirection.Input, SqlDbType.VarChar, strReceipt, 1);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_Receipt", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_Receipt", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 이미 수리요청 된 설비인지 확인
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>수리요청이 있는경우 수리요청 상세정보 리턴</returns>
        [AutoComplete]
        public DataTable mfCheckEquipOnRepair(string strPlantCode, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_onRepair", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_onRepair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 설비수리등록 리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromDate">From날짜</param>
        /// <param name="strToDate">To날짜</param>
        /// <param name="strArea">AreaCode</param>
        /// <param name="strStation">StationCode</param>
        /// <param name="strEquipProc">설비공정구분코드</param>
        /// <param name="strEquipGroupCode">설비(점검)그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비수리 요청/접수/결과 등록된 설비 리스트</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairTotal(string strPlantCode, string strFromDate, string strToDate, string strArea, string strStation, string strEquipProc, string strEquipGroupCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strArea", ParameterDirection.Input, SqlDbType.VarChar, strArea, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strStation", ParameterDirection.Input, SqlDbType.VarChar, strStation, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipProc", ParameterDirection.Input, SqlDbType.VarChar, strEquipProc, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_Total", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_onRepair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 설비수리등록 상세조회
        /// </summary>
        /// <param name="strRepairReqCode">설비수리요청번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비수리 요청/접수/결과 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairTotal_Detail(string strRepairReqCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_Total_Detail", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_onRepair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        #endregion

        #region Insert(Update) Method
        [AutoComplete(false)]
        public string mfSaveEquipRepairRequest(
                                                string strRepairReqCode,
                                                string strPlantCode, string strEquipCode, 
                                                string strRepairDate, string strRepairTime,
                                                string strRepairUserID, string strFaultDate, 
                                                string strFaultTime, string strProductCode, 
                                                string strRepairReason, string strUrgentFlag,
                                                string strUserIP, string strUserID
                                            )
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            //SqlTransaction trans = m_DBConn.BeginTransaction();
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairDate", ParameterDirection.Input, SqlDbType.VarChar, strRepairDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairTime", ParameterDirection.Input, SqlDbType.VarChar, strRepairTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairUserID", ParameterDirection.Input, SqlDbType.VarChar, strRepairUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strFaultDate", ParameterDirection.Input, SqlDbType.VarChar, strFaultDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strFaultTime", ParameterDirection.Input, SqlDbType.VarChar, strFaultTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strUrgentFlag", ParameterDirection.Input, SqlDbType.VarChar, strUrgentFlag, 1);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReason", ParameterDirection.Input, SqlDbType.NVarChar, strRepairReason, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                
                sql.mfAddParamDataRow(dtParameter, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairReq_Request", dtParameter);
                //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Update_EQURepairReq_Request", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                
                //처리결과 값 처리
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                return strErrRtn;
                
            }
            catch(Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveEquipRepairRequest_Down(
                                                string strRepairReqCode,
                                                string strPlantCode, string strEquipCode,
                                                string strRepairDate, string strRepairTime,
                                                string strRepairUserID, string strFaultDate,
                                                string strFaultTime, string strProductCode,
                                                string strRepairReason, string strUrgentFlag,string strDownCode,
                                                string strAutoAcceptFlag, DataTable dtStateList,
                                                string strUserIP, string strUserID,string strFormName
                                            )
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strErrChk = "";
            try
            {
                sql.mfConnect();
                //Transaction시작
                //SqlTransaction trans = m_DBConn.BeginTransaction();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strDownCode", ParameterDirection.Input, SqlDbType.VarChar, strDownCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairDate", ParameterDirection.Input, SqlDbType.VarChar, strRepairDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairTime", ParameterDirection.Input, SqlDbType.VarChar, strRepairTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairUserID", ParameterDirection.Input, SqlDbType.VarChar, strRepairUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strFaultDate", ParameterDirection.Input, SqlDbType.VarChar, strFaultDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strFaultTime", ParameterDirection.Input, SqlDbType.VarChar, strFaultTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strUrgentFlag", ParameterDirection.Input, SqlDbType.VarChar, strUrgentFlag, 1);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReason", ParameterDirection.Input, SqlDbType.NVarChar, strRepairReason, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParameter, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairReq_Request_Down", dtParameter);
                //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Update_EQURepairReq_Request", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    strRepairReqCode = ErrRtn.mfGetReturnValue(0);

                    trans.Commit();

                    strErrChk = "M";

                    //시스템연결정보 BL 호출
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    //시스템연결정보 조회매서드실행
                    //DataTable dtAcce = clsAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S04");   //Live
                    DataTable dtAcce = clsAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S07");      //Test

                    //MES로연 결
                    QRPMES.IF.Tibrv clsTiv = new QRPMES.IF.Tibrv(dtAcce);

                    DataTable dt = new DataTable();
                    
                    //설비 상태 변경 요청 매서드 실행
                    DataTable dtEquipState = clsTiv.EQP_CHANGESTATE_REQ(strFormName, strPlantCode, strRepairUserID, strAutoAcceptFlag == "T" ? "Y" : "N", "", dtStateList, dt, strUserID, strUserIP);
                    TransErrRtn ErrRtnn = new TransErrRtn();
                    string strErr = "";

                    if (dtEquipState.Rows.Count > 0)
                    {
                        
                        //처리결과가 성공일 경우 테이블의 MESFlag 업데이트
                        if (dtEquipState.Rows[0]["returncode"].ToString().Equals("0"))
                        {
                            strErrChk = "O";
                            strErr = mfSaveEquipRepair_MESIFFlag(strPlantCode, strRepairReqCode, "REQ", strUserIP, strUserID);
                        }
                        //처리실패시 등록한정보 삭제하거나 널값처리
                        else
                        {
                            strErrChk = "X";
                            strErr = mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "REQ", strUserIP, strUserID);
                        }

                        ErrRtnn = ErrRtnn.mfDecodingErrMessage(strErr);
                        if (ErrRtnn.ErrNum.Equals(0))
                            strErrChk = "";

                        //성공이나 실패시 MES코드와 메세지를 EnCoding 한다.
                        ErrRtn.InterfaceResultCode = dtEquipState.Rows[0][0].ToString();
                        ErrRtn.InterfaceResultMessage = dtEquipState.Rows[0][1].ToString();

                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    }
                    //처리실패시 등록한정보 삭제하거나 널값처리
                    else
                    {
                        strErrChk = "X";
                        strErr = mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "REQ", strUserIP, strUserID);
                        
                        ErrRtnn = ErrRtnn.mfDecodingErrMessage(strErr);
                        if (ErrRtnn.ErrNum.Equals(0))
                            strErrChk = "";
                    }
                    
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                //M: MES처리전, O : 처리성공, X : 처리실패
                if (strErrChk.Equals("M") || strErrChk.Equals("O") || strErrChk.Equals("X"))
                {
                    //MES처리도중 실패되거나 처리실패후 정보삭제중 실패되면 실행 
                    if (strErrChk.Equals("M") || strErrChk.Equals("X")) 
                        mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "REQ", strUserIP, strUserID);

                    //MES처리성공후 MESFlag업데이트가 실패되었을 경우
                    if(strErrChk.Equals("O"))
                        mfSaveEquipRepair_MESIFFlag(strPlantCode, strRepairReqCode, "REQ", strUserIP, strUserID);
                }

                //trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveEquipRepairResult(
                                                string strRepairReqCode, string strRepairStartDate, string strRepairStartTime,
                                                string strRepairEndDate, string strRepairEndTime, string strRepairUserID,
                                                string strRepairResultCode, string strDownCode,string strDownCodeName, string strCCSReqFlag,
                                                string strRepairDesc, string strUserIP, string strUserID
                                             )
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            //SqlTransaction trans = m_DBConn.BeginTransaction();
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairStartDate", ParameterDirection.Input, SqlDbType.VarChar, strRepairStartDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairStartTime", ParameterDirection.Input, SqlDbType.VarChar, strRepairEndTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairEndDate", ParameterDirection.Input, SqlDbType.VarChar, strRepairEndDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairEndTime", ParameterDirection.Input, SqlDbType.VarChar, strRepairEndTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairUserID", ParameterDirection.Input, SqlDbType.VarChar, strRepairUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairResultCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairResultCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strDownCode", ParameterDirection.Input, SqlDbType.VarChar, strDownCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strDownCodeName", ParameterDirection.Input, SqlDbType.NVarChar, strDownCodeName, 250);
                sql.mfAddParamDataRow(dtParameter, "@i_strCCSReqFlag", ParameterDirection.Input, SqlDbType.VarChar, strCCSReqFlag, 1);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairDesc", ParameterDirection.Input, SqlDbType.NVarChar, strRepairDesc, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairReq_Result", dtParameter);
                //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Update_EQURepairReq_Request", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveEquipRepairRequest_Receipt(DataTable dtRepair, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            //SqlTransaction trans = m_DBConn.BeginTransaction();
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                for (int i = 0; i < dtRepair.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParameter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepair.Rows[i]["RepairReqCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParameter, "@i_strHeadFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepair.Rows[i]["HeadFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParameter, "@i_strStageDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtRepair.Rows[i]["StageDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParameter, "@i_strReceiptDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtRepair.Rows[i]["ReceiptDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairReq_Receipt", dtParameter);
                    //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Update_EQURepairReq_Request", dtParameter);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 값 처리
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                trans.Commit();

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        #endregion

        #region Delete(Update) Method
        [AutoComplete(false)]
        public string mfDeleteEquipRepairRequest(string strRepairReqCode)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            //SqlTransaction trans = m_DBConn.BeginTransaction();
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);

                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQURepairReq_Request", dtParameter);
                //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Delete_EQURepairReq_Request", dtParameter);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfDeleteEquipRepairRequest_Receipt(DataTable dtRepair, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            //SqlTransaction trans = m_DBConn.BeginTransaction();
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                for (int i = 0; i < dtRepair.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParameter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepair.Rows[i]["RepairReqCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQURepairReq_Receipt", dtParameter);
                    //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Update_EQURepairReq_Request", dtParameter);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 값 처리
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                trans.Commit();

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfDeleteEquipRepairResult(string strRepairReqCode, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            //SqlTransaction trans = m_DBConn.BeginTransaction();
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQURepairReq_Result", dtParameter);
                //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Update_EQURepairReq_Request", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region MESSelect
        /// <summary>
        /// 설비수리등록 상세조회
        /// </summary>
        /// <param name="strRepairReqCode">설비수리요청번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비수리 요청/접수/결과 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepair_MESDB(string strEquipLoc, string strEquipType, string strEquipState, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLoc, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipType", ParameterDirection.Input, SqlDbType.VarChar, strEquipType, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipState", ParameterDirection.Input, SqlDbType.VarChar, strEquipState, 5);
                sql.mfAddParamDataRow(dtParame, "@i_strEuqipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_MESDB", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_onRepair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 설비수리등록 상세조회
        /// </summary>
        /// <param name="strRepairReqCode">설비수리요청번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비수리 요청/접수/결과 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepair_MESDB_Detail(string strPlantCode, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_MESDB_Detail", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_onRepair", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 설비수리등록 상세조회
        /// </summary>
        /// <param name="strRepairReqCode">설비수리요청번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비수리 요청/접수/결과 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepair_MESDBInfo(string strPlantCode, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipRepairReq_MESDBInfo", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        #endregion

        #region MESUpdate

        /// <summary>
        /// MES처리성공시 MESFlag갱신
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strRepairReqCode">수리요청번호</param>
        /// <param name="strReqCode">REQ : 요청 , REC : 접수 , RER : 결과등록</param>
        ///<param name="strUserIP">사용자아이피</param>
        ///<param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveEquipRepair_MESIFFlag(string strPlantCode,string strRepairReqCode,string strReqCode,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn ="";

            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParamt = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamt, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParamt, "@i_strReqCode", ParameterDirection.Input, SqlDbType.VarChar, strReqCode, 10);
                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairReq_MESIFFlag", dtParamt);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (!ErrRtn.ErrNum.Equals(0))
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// MES처리 실패시 등록하거나 업데이트한 정보 널처리
        /// </summary>
        /// <param name="strPlantcode">공장</param>
        /// <param name="strrepairReqCode">수리요청번호</param>
        /// <param name="strReqCode">REQ : 요청 , REC : 접수 , RER : 결과등록 , CHG : 품종교체팝업</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveEquipRepair_InitReqInfo(string strPlantcode, string strrepairReqCode, string strReqCode, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantcode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strrepairReqCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqCode", ParameterDirection.Input, SqlDbType.VarChar, strReqCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepair_InitData", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region Select MDMRecipe


        /// <summary>
        /// 품종교체 팝업창에 있는 Recipe정보
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strProductCode">제품코드</param>
        /// <returns>레시피정보</returns>
        [AutoComplete]
        public DataTable mfReadRepairReq_MDMRecipePOP(string strPlantCode, string strEquipCode, string strProductCode)
        {
            DataTable dtRecipe = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar,strProductCode,20);

                //품종교체 팝업창의 레시피정보조회 프로시저 실행
                dtRecipe = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASRecipe_RepairPOP", dtParam);

                //해당조건의 Recipe정보
                return dtRecipe;
            }
            catch (Exception ex)
            {
                return dtRecipe;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                dtRecipe.Dispose();
                sql.Dispose();
            }
        }


        #endregion


        /// <summary>
        /// 정지유형 현황조회(일간)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비점검그룹</param>
        /// <param name="strWorkerID">정비사</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <returns>PM Total건수 정보</returns>
        [AutoComplete]
        public DataTable mfReadEQURepairReq_Month
            (string strPlantCode
            , string strStationCode
            , string strEquipLocCode
            , string strProcessGroup
            , string strEquipLargeTypeCode
            , string strEquipGroupCode
            , string strUserID            
            , string strFromDate 
            , string strDownCode
            , string strQRPCheck
            , string strLang)
        {
            DataTable dtPMChk = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairStartDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReasonCode", ParameterDirection.Input, SqlDbType.VarChar, strDownCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strQRPCheck", ParameterDirection.Input, SqlDbType.VarChar, strQRPCheck, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 20);
                //PM체크결과 PM완료여부조회 프로시저 실행
                dtPMChk = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_Month", dtParam);

                return dtPMChk;
            }
            catch (Exception ex) 
            {
                return dtPMChk; 
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMChk.Dispose();
                sql.Dispose();
            }
        }


        [AutoComplete(false)]
        public string mfinsertBatText(string strcode, string strtext)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            //SqlTransaction trans = m_DBConn.BeginTransaction();
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@code", ParameterDirection.Input, SqlDbType.VarChar, strcode, 50);
                sql.mfAddParamDataRow(dtParameter, "@text", ParameterDirection.Input, SqlDbType.VarChar, strtext, 100);

                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_insertbattext", dtParameter);
                //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Update_EQURepairReq_Request", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #region 설비상태등록
        
        /// <summary>
        /// 설비상태등록
        /// </summary>
        /// <param name="strReceive">구분자</param>
        /// <param name="strRepairReqCode">수리요청번호</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strRequesrDate">발생일</param>
        /// <param name="strRequestTime">발생시간</param>
        /// <param name="strRequestUserID">요청자</param>
        /// <param name="strFaultDate">고장발생일</param>
        /// <param name="strFaultTime">고장발생시간</param>
        /// <param name="strProductCode">교체제품코드</param>
        /// <param name="strRequestReason">요청사유</param>
        /// <param name="strUrgentFlag">긴급처리</param>
        /// <param name="strDownCode">DownCode</param>
        /// <param name="strAutoAcceptFlag">자동접수여부</param>
        /// <param name="strRepairEndDate">결과등록일</param>
        /// <param name="strRepairEndTime">결과등록시간</param>
        /// <param name="strRepairUserID">수리자</param>
        /// <param name="strRepairResultCode">수리결과코드</param>
        /// <param name="strResultDownCode">수리다운코드</param>
        /// <param name="strResultDownCodeName">수리다운코드명</param>
        /// <param name="strCCSReqFlag">CCS여부</param>
        /// <param name="strRepairDesc">수리결과비고</param>
        /// <param name="dtRepair">수리접수 정보</param>
        /// <param name="dtStateList">MES로보낼 자료</param>
        /// <param name="strFormName">FormName</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveEquipRepair_EquipState(string strReceive,string strRepairReqCode,
                                                string strPlantCode, string strEquipCode,
                                                string strRequesrDate, string strRequestTime,
                                                string strRequestUserID, string strFaultDate,
                                                string strFaultTime, string strProductCode,
                                                string strRequestReason, string strUrgentFlag, string strDownCode,
                                                string strAutoAcceptFlag,
                                                string strRepairEndDate, string strRepairEndTime, string strRepairUserID,
                                                string strRepairResultCode, string strResultDownCode, string strResultDownCodeName, string strCCSReqFlag,
                                                string strRepairDesc, string strImportantFlag, DataTable dtAccept, DataTable dtStateList, string strFormName, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";
            string strErrChk = "";
            //MES결과등록에 필요한 변수
            string strReqCode = "";
            try
            {
                //디비연결
                sql.mfConnect();

                //트랜젝션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                

                //RepairReqCode 유무
                string strReq = strRepairReqCode;
                string strUser = "";

                #region 수리요청

                //수리요청코드가 없는 경우 신규등록
                if (strRepairReqCode.Equals(string.Empty))
                {
                    //수리요청 정보 등록 매서드 실행
                    strErrRtn = mfSaveEquipRepairRequest_ReqNone("", strPlantCode, strEquipCode, strRequesrDate, strRequestTime, strRequestUserID, strFaultDate, strFaultTime,
                                                strProductCode, strRequestReason, strUrgentFlag, strDownCode, strAutoAcceptFlag, strUserIP, strUserID, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 실패시 롤백 후 리턴
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        //수리요청코드 저장
                        strRepairReqCode = ErrRtn.mfGetReturnValue(0);
                        //strReqCode = "REQ";
                        //strUser = strRequestUserID;
                    }
                }

                #endregion

                #region 수리접수

                if (strReceive.Equals("Request"))
                {
                    //수리접수 등록매서드 실행
                    strErrRtn = mfSaveEquipRepairReceipt_ReqNone(strRepairReqCode, dtAccept, strUserIP, strUserID, sql.SqlCon, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 실패시 롤백후 리턴
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        strReqCode = "REC";
                        if (dtAccept.Rows.Count > 0)
                            strUser = dtAccept.Rows[0]["ReceiptUserID"].ToString();
                        else
                            strUser = strUserID;
                    }
                }

                #endregion

                #region 수리결과등록

                // 접수상태가 Accept 인 경우 수리결과등록 매서드 실행
                if (strReceive.Equals("Accept"))
                {
                    //수리요청번호가 없는 경우 수리접수등록 매서드 실행
                    if (strReq.Equals(string.Empty))
                    {
                        //수리접수 등록매서드 실행
                        strErrRtn = mfSaveEquipRepairReceipt_ReqNone(strRepairReqCode, dtAccept, strUserIP, strUserID, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리결과 실패시 롤백후 리턴
                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //수리결과등록매서드 실행
                    strErrRtn = mfSaveEquipRepairResult_ReqNone(strRepairReqCode, strRepairEndDate, strRepairEndTime, strRepairUserID, strRepairResultCode, strResultDownCode,
                                                                strResultDownCodeName, strCCSReqFlag, strRepairDesc, strImportantFlag, strUserIP, strUserID, sql.SqlCon, trans);
                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과학인
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        strReqCode = "RER";
                        strUser = strRepairUserID;
                    }
                }
                
                #endregion

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                #region MES
               
                if (!strRepairReqCode.Equals(string.Empty) && ErrRtn.ErrNum.Equals(0))
                {
                    strErrChk = "M";

                    //시스템연결정보 BL 호출
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    
                    //시스템연결정보 조회매서드실행
                    //DataTable dtAcce = clsAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S04"); //Live
                    DataTable dtAcce = clsAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S07");   //Test

                    //MES로연 결
                    QRPMES.IF.Tibrv clsTiv = new QRPMES.IF.Tibrv(dtAcce);

                    DataTable dt = new DataTable();
                   
                    //설비 상태 변경 요청 매서드 실행
                    DataTable dtEquipState = clsTiv.EQP_CHANGESTATE_REQ(strFormName, strPlantCode, strUser, strAutoAcceptFlag == "T" ? "Y" : "N", "", dtStateList, dt, strUserID, strUserIP);

                    TransErrRtn ErrRtnn = new TransErrRtn();
                    string strMsg = "";
                    if (dtEquipState.Rows.Count > 0)
                    {
                        //처리결과가 성공일 경우 테이블의 MESFlag 업데이트
                        if (dtEquipState.Rows[0]["returncode"].ToString().Equals("0"))
                        {
                            strErrChk = "O";
                            strMsg = mfSaveEquipRepair_MESIFFlag(strPlantCode, strRepairReqCode, strReqCode, strUserIP, strUserID);
                        }
                        //처리실패시 등록한정보 삭제하거나 널값처리
                        else
                        {
                            strErrChk = "X";
                            strMsg = mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, strReqCode, strUserIP, strUserID);

                        }

                        ErrRtnn = ErrRtnn.mfDecodingErrMessage(strMsg);
                        if (ErrRtnn.ErrNum.Equals(0))
                            strErrChk = "";

                        //성공이나 실패시 MES코드와 메세지를 EnCoding 한다.
                        ErrRtn.InterfaceResultCode = dtEquipState.Rows[0][0].ToString();
                        ErrRtn.InterfaceResultMessage = dtEquipState.Rows[0][1].ToString();
                        ErrRtn.mfAddReturnValue(strRepairReqCode);
                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    }
                    //처리실패시 등록한정보 삭제하거나 널값처리
                    else
                    {
                        strErrChk = "X";
                        strMsg = mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, strReqCode, strUserIP, strUserID);
                        
                        ErrRtnn = ErrRtnn.mfDecodingErrMessage(strMsg);
                        if (ErrRtnn.ErrNum.Equals(0))
                            strErrChk = "";
                    }

                }

                #endregion

                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                //M: MES처리전, O : 처리성공, X : 처리실패
                if (strErrChk.Equals("M") || strErrChk.Equals("O") || strErrChk.Equals("X"))
                {
                    //MES처리도중 실패되거나 처리실패후 정보삭제중 실패되면 실행 
                    if (strErrChk.Equals("M") || strErrChk.Equals("X"))
                        mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, strReqCode, strUserIP, strUserID);

                    //MES처리성공후 MESFlag업데이트가 실패되었을 경우
                    if (strErrChk.Equals("O"))
                        mfSaveEquipRepair_MESIFFlag(strPlantCode, strRepairReqCode, strReqCode, strUserIP, strUserID);
                }

                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비수리요청등록
        /// </summary>
        /// <param name="strRepairReqCode"></param>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <param name="strRepairDate"></param>
        /// <param name="strRepairTime"></param>
        /// <param name="strRepairUserID"></param>
        /// <param name="strFaultDate"></param>
        /// <param name="strFaultTime"></param>
        /// <param name="strProductCode"></param>
        /// <param name="strRepairReason"></param>
        /// <param name="strUrgentFlag"></param>
        /// <param name="strDownCode"></param>
        /// <param name="strAutoAcceptFlag"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="SqlCon"></param>
        /// <param name="trans"></param>
        /// <returns>처리결과,수리요청번호</returns>
        [AutoComplete(false)]
        public string mfSaveEquipRepairRequest_ReqNone(
                                                string strRepairReqCode,
                                                string strPlantCode, string strEquipCode,
                                                string strRepairDate, string strRepairTime,
                                                string strRepairUserID, string strFaultDate,
                                                string strFaultTime, string strProductCode,
                                                string strRepairReason, string strUrgentFlag, string strDownCode,
                                                string strAutoAcceptFlag,
                                                string strUserIP, string strUserID, SqlConnection SqlCon,SqlTransaction trans
                                            )
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strDownCode", ParameterDirection.Input, SqlDbType.VarChar, strDownCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairDate", ParameterDirection.Input, SqlDbType.VarChar, strRepairDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairTime", ParameterDirection.Input, SqlDbType.VarChar, strRepairTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairUserID", ParameterDirection.Input, SqlDbType.VarChar, strRepairUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strFaultDate", ParameterDirection.Input, SqlDbType.VarChar, strFaultDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strFaultTime", ParameterDirection.Input, SqlDbType.VarChar, strFaultTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strUrgentFlag", ParameterDirection.Input, SqlDbType.VarChar, strUrgentFlag, 1);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReason", ParameterDirection.Input, SqlDbType.NVarChar, strRepairReason, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParameter, "@o_strRepairReqCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(SqlCon, trans, "up_Update_EQURepairReq_Request_Down", dtParameter);
                
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                if (ErrRtn.ErrNum != 0)
                {
                    //trans.Rollback();
                    return strErrRtn;
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                //trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);

            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비수리결과등록
        /// </summary>
        /// <param name="strRepairReqCode"></param>
        /// <param name="strRepairEndDate"></param>
        /// <param name="strRepairEndTime"></param>
        /// <param name="strRepairUserID"></param>
        /// <param name="strRepairResultCode"></param>
        /// <param name="strDownCode"></param>
        /// <param name="strDownCodeName"></param>
        /// <param name="strCCSReqFlag"></param>
        /// <param name="strRepairDesc"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipRepairResult_ReqNone(
                                                string strRepairReqCode,
                                                string strRepairEndDate, string strRepairEndTime, string strRepairUserID,
                                                string strRepairResultCode, string strDownCode, string strDownCodeName, string strCCSReqFlag,
                                                string strRepairDesc,string strImportantFlag ,string strUserIP, string strUserID,SqlConnection sqlcon,SqlTransaction trans
                                             )
        {
            SQLS sql = new SQLS();


            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairStartDate", ParameterDirection.Input, SqlDbType.VarChar, strRepairEndDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairStartTime", ParameterDirection.Input, SqlDbType.VarChar, strRepairEndTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairEndDate", ParameterDirection.Input, SqlDbType.VarChar, strRepairEndDate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairEndTime", ParameterDirection.Input, SqlDbType.VarChar, strRepairEndTime, 8);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairUserID", ParameterDirection.Input, SqlDbType.VarChar, strRepairUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairResultCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairResultCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strDownCode", ParameterDirection.Input, SqlDbType.VarChar, strDownCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strDownCodeName", ParameterDirection.Input, SqlDbType.NVarChar, strDownCodeName, 250);
                sql.mfAddParamDataRow(dtParameter, "@i_strCCSReqFlag", ParameterDirection.Input, SqlDbType.VarChar, strCCSReqFlag, 1);
                sql.mfAddParamDataRow(dtParameter, "@i_strRepairDesc", ParameterDirection.Input, SqlDbType.NVarChar, strRepairDesc, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strImportantFlag", ParameterDirection.Input, SqlDbType.VarChar, strImportantFlag, 1);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQURepairReq_Result", dtParameter);
                //strErrRtn = mfExecTransStoredProc(m_DBConn, trans, "up_Update_EQURepairReq_Request", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                //trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수리접수등록
        /// </summary>
        /// <param name="strRepairReqCode"></param>
        /// <param name="dtRepair"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipRepairReceipt_ReqNone(string strRepairReqCode, DataTable dtRepair, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtRepair.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParameter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                    sql.mfAddParamDataRow(dtParameter, "@i_strReceiptUser", ParameterDirection.Input, SqlDbType.VarChar, dtRepair.Rows[i]["ReceiptUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParameter, "@i_strHeadFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepair.Rows[i]["HeadFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParameter, "@i_strHeadDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtRepair.Rows[i]["HeadDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParameter, "@i_strStageDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtRepair.Rows[i]["StageDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParameter, "@i_strReceiptDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtRepair.Rows[i]["ReceiptDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQURepairReq_Receipt", dtParameter);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 값 처리
                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;
                    }
                }

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                //trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비상태등록 (의뢰취소)
        /// </summary>
        /// <param name="dtRepair"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteEquipRepair_Receipt(DataTable dtRepair, DataTable dtMESInfo, string strUserIP, string strUserID, string strFormName)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {

                for (int i = 0; i < dtRepair.Rows.Count; i++)
                {

                    if (!dtRepair.Rows[i]["RepairReqCode"].ToString().Equals(string.Empty))
                    {
                        sql.mfConnect();

                        //Transaction시작
                        //SqlTransaction trans = m_DBConn.BeginTransaction();
                        SqlTransaction trans = sql.SqlCon.BeginTransaction();

                        string strRepairReqCode = "";

                        #region 수리의뢰 취소하기
                        //파라미터 저장
                        DataTable dtParameter = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepair.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParameter, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepair.Rows[i]["RepairReqCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_EQURepairReq_Cencel", dtParameter);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리결과 값 처리
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                        else
                        {
                            trans.Commit();
                        }

                        //수리요청번호 저장
                        strRepairReqCode = dtRepair.Rows[i]["RepairReqCode"].ToString();

                        #endregion
                    }
                    else
                    {
                        ErrRtn.ErrNum = 0;

                    }

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        #region MES


                        //시스템연결정보 BL 호출
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();

                        //시스템연결정보 조회매서드실행
                        //DataTable dtAcce = clsAcce.mfReadSystemAccessInfoDetail(dtRepair.Rows[i]["PlantCode"].ToString(), "S04");//Live
                        DataTable dtAcce = clsAcce.mfReadSystemAccessInfoDetail(dtRepair.Rows[i]["PlantCode"].ToString(), "S07");   //Test

                        //MES로연 결
                        QRPMES.IF.Tibrv clsTiv = new QRPMES.IF.Tibrv(dtAcce);

                        DataTable dt = new DataTable();

                        //설비 상태 변경 요청 매서드 실행
                        DataTable dtEquipState = clsTiv.EQP_CHANGESTATE_REQ(strFormName, dtRepair.Rows[i]["PlantCode"].ToString(),
                                                                            dtRepair.Rows[i]["RepairReqID"].ToString(),
                                                                            dtRepair.Rows[i]["AutoAccept"].ToString() == "T" ? "Y" : "N",
                                                                            "Y", dtMESInfo, dt, strUserID, strUserIP);

                        if (dtEquipState.Rows.Count > 0)
                        {
                            //성공이나 실패시 MES코드와 메세지를 EnCoding 한다.
                            ErrRtn.InterfaceResultCode = dtEquipState.Rows[0][0].ToString();
                            ErrRtn.InterfaceResultMessage = dtEquipState.Rows[0][1].ToString();

                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                        }

                        #endregion
                    }

                }

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                //trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 설비상태관리 (치공구교체POP업창)
        /// </summary>
        /// <param name="strRepairReqCode">수리요청코드</param>
        /// <param name="dtDurableMatH">치공구교체헤더</param>
        /// <param name="dtDurableMatD">치공구교체상세</param>
        /// <param name="dtUseSPH">SP교체헤더</param>
        /// <param name="dtUseSPD">SP교체상세</param>
        /// <param name="dtRequest">수리요청정보</param>
        /// <param name="dtAccept">수리접수정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과리턴</returns>
        [AutoComplete(false)]
        public string mfSaveRepairReq_ChangePOP(string strRepairReqCode,DataTable dtDurableMatH,DataTable dtDurableMatD,DataTable dtUseSPH,DataTable dtUseSPD,DataTable dtRequest,DataTable dtAccept,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                #region 수리요청등록

                //수리요청번호가 없는 경우 요청번호를 만든다.(즉. 수리요청정보를 DB에 저장한다.)
                if (strRepairReqCode.Equals(string.Empty))
                {

                    if (dtRequest.Rows.Count > 0)
                    {
                        //수리요청등록 매서드 실행
                        strErrRtn = mfSaveEquipRepairRequest_ReqNone("", dtRequest.Rows[0]["PlantCode"].ToString(), dtRequest.Rows[0]["EquipCode"].ToString(),
                                                        dtRequest.Rows[0]["RepairReqDate"].ToString(), dtRequest.Rows[0]["RepairReqTime"].ToString(),
                                                        dtRequest.Rows[0]["RepairReqID"].ToString(), dtRequest.Rows[0]["BreakDate"].ToString(),
                                                        dtRequest.Rows[0]["BreakTime"].ToString(), dtRequest.Rows[0]["ProductCode"].ToString(),
                                                        dtRequest.Rows[0]["RepairReqReson"].ToString(), dtRequest.Rows[0]["Urgent"].ToString(),
                                                        dtRequest.Rows[0]["DownCode"].ToString(), dtRequest.Rows[0]["AutoAccept"].ToString(),
                                                        strUserIP, strUserID, sql.SqlCon, trans);

                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                        else
                        {
                            //처리성공시 수리요청번호를 받는다.
                            strRepairReqCode = ErrRtn.mfGetReturnValue(0);

                            //수리요청성공시 수리의뢰접수도 저장한다. (요청번호가 없는것은 DB에 접수까지의 정보가 없다는것
                            strErrRtn = mfSaveEquipRepairReceipt_ReqNone(strRepairReqCode, dtAccept, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }

                        }

                    }
                    //수리요청번호가 없는데 정보도 없을 경우 리턴
                    else
                    {
                        ErrRtn.ErrNum = 1;
                        return ErrRtn.mfEncodingErrMessage(ErrRtn);
                    }
                }

                #endregion

                #region 치공구 교체등록

                //치공구 교체정보가 있는 경우
                if (dtDurableMatH.Rows.Count > 0)
                {
                    //수리요청코드 저장
                    dtDurableMatH.Rows[0]["RepairReqCode"] = strRepairReqCode;

                    //치공구 교체 헤더정보 BL
                    QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();

                    //치공구 교체헤더정보 저장 매서드 실행
                    strErrRtn = clsDurableMatH.mfSaveDurableMatRepairH_EquipState(dtDurableMatH, strUserIP, strUserID, sql.SqlCon, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 롤백
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    //처리성공시 상세정보를 저장한다.
                    else
                    {
                        //수리출고구분코드를 리턴받아 저장한다.
                        string strRepairGICode = ErrRtn.mfGetReturnValue(0);

                        //치공구교체 상세정보 BL
                        QRPEQU.BL.EQUMGM.DurableMatRepairD clsDurableMatD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();

                        //치공구 교체상세정보저장 매서드 실행
                        strErrRtn = clsDurableMatD.mfSaveDurableMatRepairD(strRepairGICode, dtDurableMatD, strUserIP, strUserID, sql.SqlCon, trans);

                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리실패시 롤백
                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }


                    }
                }

                #endregion 

                #region SP 교체등록

                //SP교체 정보가 있는 경우
                if (dtUseSPH.Rows.Count > 0)
                {
                    //수리SP정보 BL
                    RepairUseSP clsRepairUseSP = new RepairUseSP();
                    dtUseSPH.Rows[0]["RepairReqCode"] = strRepairReqCode;
                    //사용 SP 정보 저장 매서드 실행
                    strErrRtn = clsRepairUseSP.mfSaveEquipRepairUseSP_POP(dtUseSPH, dtUseSPD, strUserIP, strUserID, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 실채시
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                #endregion

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                    ErrRtn.mfAddReturnValue(strRepairReqCode);
                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                }

                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion



        /// <summary>
        /// OI 설비 상태 변경 처리 완료시 MES I/F 전송받은 결과 DB에 저장
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strWorkUserID">작업자ID</param>
        /// <param name="dtStateList">설비 상태 변경 List</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipRepair_MESIF_CHANGESTATE_RPT(string strPlantCode, string strWorkUserID, DataTable dtStateList, string strUserID, string strUserIP)
        {
            QRPDB.SQLS sql = new QRPDB.SQLS();
            QRPDB.TransErrRtn ErrRtn = new QRPDB.TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                System.Data.SqlClient.SqlTransaction trans = sql.SqlCon.BeginTransaction();
                DataTable dtParam = new DataTable();

                for (int i = 0; i < dtStateList.Rows.Count; i++)
                {
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["EquipCode"] == null ? string.Empty : dtStateList.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPreProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["OldProductSpecID"] == null ? string.Empty : dtStateList.Rows[i]["OldProductSpecID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["ProductSpecID"] == null ? string.Empty : dtStateList.Rows[i]["OldProductSpecID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMACHINERECIPENAME", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["MachineRecipe"] == null ? string.Empty : dtStateList.Rows[i]["MachineRecipe"].ToString(), 60);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDownCode", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["RequestDownCode"] == null ? string.Empty : dtStateList.Rows[i]["RequestDownCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["ReasonCode"] == null ? string.Empty : dtStateList.Rows[i]["ReasonCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strComment", ParameterDirection.Input, SqlDbType.NVarChar, dtStateList.Rows[i]["Comment"] == null ? string.Empty : dtStateList.Rows[i]["Comment"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqTime", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["RequestTime"] == null ? string.Empty : dtStateList.Rows[i]["RequestTime"].ToString(), 14);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqID", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["RequestUser"] == null ? string.Empty : dtStateList.Rows[i]["RequestUser"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptTime", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["AcceptTime"] == null ? string.Empty : dtStateList.Rows[i]["AcceptTime"].ToString(), 14);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptID", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["AcceptUser"] == null ? string.Empty : dtStateList.Rows[i]["AcceptUser"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairEndTime", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["CompleteTime"] == null ? string.Empty : dtStateList.Rows[i]["CompleteTime"].ToString(), 14);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairID", ParameterDirection.Input, SqlDbType.VarChar, dtStateList.Rows[i]["CompleteUser"] == null ? string.Empty : dtStateList.Rows[i]["CompleteUser"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqComment", ParameterDirection.Input, SqlDbType.NVarChar, dtStateList.Rows[i]["ReqComment"] == null ? string.Empty : dtStateList.Rows[i]["ReqComment"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcceptComment", ParameterDirection.Input, SqlDbType.NVarChar, dtStateList.Rows[i]["AcceptComment"] == null ? string.Empty : dtStateList.Rows[i]["AcceptComment"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepair_MESIF", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairReq")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class RepairFaultType : ServicedComponent
    {
        #region Select Method
        #endregion

        #region Insert(Update) Method
        #endregion

        #region Delete(Update) Method
        #endregion
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairReq")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class RepairP : ServicedComponent
    {
        #region Select Method
        #endregion

        #region Insert(Update) Method
        #endregion

        #region Delete(Update) Method
        #endregion
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairReq")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class RepairUseSP : ServicedComponent
    {
        #region DataTable
        public DataTable mfDataSetInfoH()
        {
            DataTable dtUseSPH = new DataTable();

            try
            {
                dtUseSPH.Columns.Add("PlantCode", typeof(string));
                dtUseSPH.Columns.Add("RepairReqCode", typeof(string));
                dtUseSPH.Columns.Add("UseSPChgDate", typeof(string));
                dtUseSPH.Columns.Add("UseSPChgID", typeof(string));
                dtUseSPH.Columns.Add("UseSPEtcDesc", typeof(string));

                return dtUseSPH;
            }
            catch (Exception ex)
            {
                return dtUseSPH;
                throw (ex);
            }
            finally
            {
                dtUseSPH.Dispose();
            }
        }

        public DataTable mfDataSetInfoD()
        {
            DataTable dtUseSPD = new DataTable();

            try
            {
                dtUseSPD.Columns.Add("PlantCode", typeof(string));
                dtUseSPD.Columns.Add("EquipCode", typeof(string));
                dtUseSPD.Columns.Add("RepairReqCode", typeof(string));
                dtUseSPD.Columns.Add("Seq", typeof(int));
                dtUseSPD.Columns.Add("CurSparePartCode", typeof(string));
                dtUseSPD.Columns.Add("CurInputQty", typeof(int));
                dtUseSPD.Columns.Add("ChgSPInventoryCode", typeof(string));
                dtUseSPD.Columns.Add("ChgSparePartCode", typeof(string));
                dtUseSPD.Columns.Add("ChgInputQty", typeof(int));
                dtUseSPD.Columns.Add("UnitCode", typeof(string));
                dtUseSPD.Columns.Add("ChgDesc", typeof(string));
                dtUseSPD.Columns.Add("CancelFlag", typeof(string));

                return dtUseSPD;
            }
            catch (Exception ex)
            {
                return dtUseSPD;
                throw (ex);
            }
            finally
            {
                dtUseSPD.Dispose();
            }
        }
        #endregion

        #region Select Method
        /// <summary>
        /// 수리완료등록 리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromDate">From날짜</param>
        /// <param name="strToDate">To날짜</param>
        /// <param name="strArea">AreaCode</param>
        /// <param name="strStation">StationCode</param>
        /// <param name="strEquipProc">설비공정구분코드</param>
        /// <param name="strEquipGroupCode">설비(점검)그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>수리결과가 등록된 수리요청 리스트</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairEnd(string strPlantCode, string strFromDate, string strToDate, string strArea, string strStation, string strEquipProc
                                                , string strEquipGroupCode, string strEquipLocCode, string strEquipTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strArea", ParameterDirection.Input, SqlDbType.VarChar, strArea, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strStation", ParameterDirection.Input, SqlDbType.VarChar, strStation, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipProc", ParameterDirection.Input, SqlDbType.VarChar, strEquipProc, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipTypeCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_End", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 수리완료등록헤더리스트조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>수리완료등록헤더리스트</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairEndH(string strPlantCode,
                                                string strFromDate,string strToDate,
                                                string strStationCode,string strEquipLocCode,string strProcessGroupCode,string strEquipLargeTypeCode,string strEquipGroupCode,
                                                string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipEndH = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar,strFromDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar,strToDate,10);
                sql.mfAddParamDataRow(dtParam,"@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar,strStationCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipLocCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strProcessGroupCode,40);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipLargeTypeCode,40);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipGroupCode,10);

                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //수리완료등록정보 조회프로시저 실행
                dtEquipEndH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairReq_EndH", dtParam);

                //수리완료등록정보
                return dtEquipEndH;
            }
            catch (Exception ex)
            {
                return dtEquipEndH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipEndH.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 수리완료등록 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strRepairReqCode">수리요청번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>수리결과가 등록된 수리요청 리스트 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairEndUseSPDetail(string strPlantCode, string strRepairReqCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipRepairEndUseSP_Detail", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_Receipt", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 스페어파트 사용 이력
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strRepairReqCode">수리요청번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>스페어파트 사용 이력</returns>
        [AutoComplete]
        public DataTable mfReadEquipRepairEndUseSP(string strPlantCode, string strRepairReqCode, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_DBConn.ConnectionString.ToString());
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 10);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQUEquipRepairEndUseSP", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_DBConn, "up_Select_EQURepairReq_Receipt", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        #endregion

        #region Insert(Update) Method
        [AutoComplete(false)]
        public string mfSaveEquipRepairUseSP(DataTable dtUseSPH, DataTable dtUseSPD, string strUserIP, string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                //--디비오픈--//
                sql.mfConnect();
                SqlTransaction trans;

                //--Transaction 시작--//
                trans = sql.SqlCon.BeginTransaction();

                #region Header
                DataTable dtParam = sql.mfSetParamDataTable();

                //--파라미터 값저장--//
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPH.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPH.Rows[0]["RepairReqCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUseSPChgDate", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPH.Rows[0]["UseSPChgDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUseSPChgID", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPH.Rows[0]["UseSPChgID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUseSPEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtUseSPH.Rows[0]["UseSPEtcDesc"].ToString(), 100);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //--프로시저 실행--//
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairReq_UseSPH", dtParam);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                #endregion

                //--처리결과 실패시--//
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                //--처리결과 성공시--//
                else
                {
                    #region Detail
                    for (int i = 0; i < dtUseSPD.Rows.Count; i++)
                    {
                        DataTable dtParamd = sql.mfSetParamDataTable();

                        //--파라미터 값저장--//
                        sql.mfAddParamDataRow(dtParamd, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParamd, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["EquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["RepairReqCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtUseSPD.Rows[i]["Seq"].ToString());
                        sql.mfAddParamDataRow(dtParamd, "@i_strCurSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["CurSparePartCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_intCurInputQty", ParameterDirection.Input, SqlDbType.Int, dtUseSPD.Rows[i]["CurInputQty"].ToString());
                        sql.mfAddParamDataRow(dtParamd, "@i_strChgSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["ChgSPInventoryCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strChgSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["ChgSparePartCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_intChgInputQty", ParameterDirection.Input, SqlDbType.Int, dtUseSPD.Rows[i]["ChgInputQty"].ToString());
                        sql.mfAddParamDataRow(dtParamd, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["UnitCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strChgDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtUseSPD.Rows[i]["ChgDesc"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParamd, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["CancelFlag"].ToString(), 1);

                        sql.mfAddParamDataRow(dtParamd, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParamd, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParamd, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //--프로시저 실행--//
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairUseSP", dtParamd);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                    #endregion
                }
                
                if(ErrRtn.ErrNum.Equals(0))
                    trans.Commit();


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        [AutoComplete(false)]
        public string mfSaveEquipRepairUseSP_POP(DataTable dtUseSPH, DataTable dtUseSPD, string strUserIP, string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strRepairReqCode = dtUseSPH.Rows[0]["RepairReqCode"].ToString();

                #region Header
                DataTable dtParam = sql.mfSetParamDataTable();

                //--파라미터 값저장--//
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPH.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPH.Rows[0]["RepairReqCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUseSPChgDate", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPH.Rows[0]["UseSPChgDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUseSPChgID", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPH.Rows[0]["UseSPChgID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUseSPEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtUseSPH.Rows[0]["UseSPEtcDesc"].ToString(), 100);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //--프로시저 실행--//
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQURepairReq_UseSPH", dtParam);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                #endregion

                //--처리결과 실패시--//
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }
                //--처리결과 성공시--//
                else
                {
                    #region Detail
                    for (int i = 0; i < dtUseSPD.Rows.Count; i++)
                    {
                        DataTable dtParamd = sql.mfSetParamDataTable();

                        //--파라미터 값저장--//
                        sql.mfAddParamDataRow(dtParamd, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParamd, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["EquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtUseSPD.Rows[i]["Seq"].ToString());
                        sql.mfAddParamDataRow(dtParamd, "@i_strCurSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["CurSparePartCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_intCurInputQty", ParameterDirection.Input, SqlDbType.Int, dtUseSPD.Rows[i]["CurInputQty"].ToString());
                        sql.mfAddParamDataRow(dtParamd, "@i_strChgSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["ChgSPInventoryCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strChgSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["ChgSparePartCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_intChgInputQty", ParameterDirection.Input, SqlDbType.Int, dtUseSPD.Rows[i]["ChgInputQty"].ToString());
                        sql.mfAddParamDataRow(dtParamd, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["UnitCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strChgDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtUseSPD.Rows[i]["ChgDesc"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParamd, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.VarChar, dtUseSPD.Rows[i]["CancelFlag"].ToString(), 1);

                        sql.mfAddParamDataRow(dtParamd, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParamd, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParamd, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //--프로시저 실행--//
                        strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQURepairUseSP", dtParamd);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            return strErrRtn;
                        }
                    }
                    #endregion
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }
        #endregion

        #region Delete(Update) Method
        #endregion
    }

    #region 설비품종교체

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairDeviceChg")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class RepairDeviceChg : ServicedComponent
    {

        /// <summary>
        /// 설비수리품종교체컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfDataSet()
        {
            DataTable dtRepairDevice = new DataTable();

            try
            {
                dtRepairDevice.Columns.Add("PlantCode", typeof(string));
                dtRepairDevice.Columns.Add("RepairReqCode", typeof(string));
                dtRepairDevice.Columns.Add("Seq", typeof(string));
                dtRepairDevice.Columns.Add("WorkerID", typeof(string));
                dtRepairDevice.Columns.Add("WorkDate", typeof(string));
                dtRepairDevice.Columns.Add("WorkTime", typeof(string));
                dtRepairDevice.Columns.Add("DownCode", typeof(string));
                dtRepairDevice.Columns.Add("DownCodeName", typeof(string));
                dtRepairDevice.Columns.Add("PreProductCode", typeof(string));
                dtRepairDevice.Columns.Add("ProductCode", typeof(string));
                dtRepairDevice.Columns.Add("MACHINERECIPENAME", typeof(string));
                dtRepairDevice.Columns.Add("VersionNum", typeof(string));
                dtRepairDevice.Columns.Add("EMCEquipCode", typeof(string));
                dtRepairDevice.Columns.Add("MoldSerial", typeof(string));
                dtRepairDevice.Columns.Add("Comment", typeof(string));


                return dtRepairDevice;
            }
            catch (Exception ex)
            {
                return dtRepairDevice;
                throw (ex);
            }
            finally
            {
                dtRepairDevice.Dispose();
            }
        }


        /// <summary>
        /// 설비수리품종교체 저장
        /// </summary>
        /// <param name="dtRepairDeviceChg">품종교체정보</param>
        /// <param name="dtDurableMatH">치공구교체정보헤더</param>
        /// <param name="dtDurableMatD">치공구교체정보 상세</param>
        /// <param name="dtDurableLot">신규Lot정보</param>
        /// <param name="dtRequest">수리요청정보</param>
        /// <param name="dtAccept">수리접수정보</param>
        /// <param name="dtMES">MES로 설비상태정보</param>
        /// <param name="dtMESDurableMat">MES로 보낼 치공구 교체상세정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strFormName">Form명</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveRepairDeviceChg(DataTable dtRepairDeviceChg,DataTable dtDurableMatH,DataTable dtDurableMatD, DataTable dtDurableLot,DataTable dtRequest,DataTable dtAccept,DataTable dtMES ,DataTable dtMESDurableMat,string strUserIP,string strUserID,string strFormName)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            string strRepairGICode = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrChk = "";
            string strPlantCode = "";
            string strRepairReqCode = "";
            try
            {
                //디비연결
                sql.mfConnect();

                //트랜젝션 시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtRepairDeviceChg.Rows.Count; i++)
                {
                    //수리요청코드 저장
                    strRepairReqCode = dtRepairDeviceChg.Rows[i]["RepairReqCode"].ToString();
                    strPlantCode = dtRepairDeviceChg.Rows[i]["PlantCode"].ToString();

                    //수리요청코드가 없는 경우 수리요청,접수 정보를 등록한다.
                    if (strRepairReqCode.Equals(string.Empty))
                    {
                        #region 수리요청, 접수 등록

                        if (dtRequest.Rows.Count > 0 && dtAccept.Rows.Count > 0)
                        {
                            RepairReq clsReq = new RepairReq();
                            //수리요청매서드 실행
                            strErrRtn = clsReq.mfSaveEquipRepairRequest_ReqNone("", dtRequest.Rows[i]["PlantCode"].ToString(), dtRequest.Rows[i]["EquipCode"].ToString(),
                                                                    dtRequest.Rows[i]["BreakDate"].ToString(), dtRequest.Rows[i]["BreakTime"].ToString(), dtRequest.Rows[i]["RepairReqID"].ToString(),
                                                                    dtRequest.Rows[i]["BreakDate"].ToString(), dtRequest.Rows[i]["BreakTime"].ToString(), dtRequest.Rows[i]["ProductCode"].ToString(),
                                                                    dtRequest.Rows[i]["RepairReqReson"].ToString(), dtRequest.Rows[i]["Urgent"].ToString(), dtRequest.Rows[i]["DownCode"].ToString(),
                                                                    dtRequest.Rows[i]["AutoAccept"].ToString(),
                                                                    strUserIP, strUserID, sql.SqlCon, trans);

                            //Decoding
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            //처리결과 실패시 롤백 후 리턴시킨다.
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }

                            //수리요청코드리턴받아 저장한다.
                            strRepairReqCode = ErrRtn.mfGetReturnValue(0);

                            //수리접수 등록 매서드 실행
                            strErrRtn = clsReq.mfSaveEquipRepairReceipt_ReqNone(strRepairReqCode, dtAccept, strUserIP, strUserID, sql.SqlCon, trans);

                            //Decoding
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }

                            clsReq.Dispose();
                        }
                        else
                        {
                            ErrRtn.ErrNum = -1;
                            ErrRtn.ErrMessage = "수리요청나 수리접수 정보가 없습니다.";
                            return strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn) ;
                        }

                        #endregion

                    }

                    #region 품종교체정보
                    //파라미터 정보 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairDeviceChg.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar,strRepairReqCode , 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtRepairDeviceChg.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkerID", ParameterDirection.Input, SqlDbType.VarChar, dtRepairDeviceChg.Rows[i]["WorkerID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairDeviceChg.Rows[i]["WorkDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkTime", ParameterDirection.Input, SqlDbType.VarChar, dtRepairDeviceChg.Rows[i]["WorkTime"].ToString(), 8);
                    sql.mfAddParamDataRow(dtParam, "@i_strDownCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairDeviceChg.Rows[i]["DownCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDownCodeName", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairDeviceChg.Rows[i]["DownCodeName"].ToString(), 250);
                    sql.mfAddParamDataRow(dtParam, "@i_strPreProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairDeviceChg.Rows[i]["PreProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairDeviceChg.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMACHINERECIPENAME", ParameterDirection.Input, SqlDbType.VarChar, dtRepairDeviceChg.Rows[i]["MACHINERECIPENAME"].ToString(), 60);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairDeviceChg.Rows[i]["VersionNum"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEMCEquipCode", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairDeviceChg.Rows[i]["EMCEquipCode"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoldSerial", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairDeviceChg.Rows[i]["MoldSerial"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strComment", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairDeviceChg.Rows[i]["Comment"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //설비수리 품종교체 등록 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairDeviceChg", dtParam);

                    #endregion

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리실패시 롤백후 for문을 빠져나간다.
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        #region 치공구 교체정보
                        //치공구 교체가 있는 경우 
                        if (dtDurableMatH.Rows.Count > 0)
                        {
                            //수리요청코드 저장
                            dtDurableMatH.Rows[0]["RepairReqCode"] = strRepairReqCode;

                            QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                            strErrRtn = clsDurableMatH.mfSaveDurableMatRepairH_EquipState(dtDurableMatH, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }

                            //치공구 교체번호를 받는다.
                            strRepairGICode = ErrRtn.mfGetReturnValue(0);

                            #region Detail저장

                            for (int j = 0; j < dtDurableMatD.Rows.Count; j++)
                            {
                                //--파라미터값저장--//
                                DataTable dtParamt = sql.mfSetParamDataTable();
                                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["PlantCode"].ToString(), 10);
                                sql.mfAddParamDataRow(dtParamt, "@i_strRepairGICode", ParameterDirection.Input, SqlDbType.VarChar, strRepairGICode, 20);
                                sql.mfAddParamDataRow(dtParamt, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableMatD.Rows[j]["Seq"].ToString());
                                sql.mfAddParamDataRow(dtParamt, "@i_strRepairGIGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["RepairGIGubunCode"].ToString(), 3);
                                sql.mfAddParamDataRow(dtParamt, "@i_strCurDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["CurDurableMatCode"].ToString(), 20);
                                sql.mfAddParamDataRow(dtParamt, "@i_strCurLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["CurLotNo"].ToString(), 40);
                                sql.mfAddParamDataRow(dtParamt, "@i_intCurQty", ParameterDirection.Input, SqlDbType.Int, dtDurableMatD.Rows[j]["CurQty"].ToString());
                                sql.mfAddParamDataRow(dtParamt, "@i_strChgDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["ChgDurableInventoryCode"].ToString(), 10);
                                sql.mfAddParamDataRow(dtParamt, "@i_strChgDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["ChgDurableMatCode"].ToString(), 20);
                                sql.mfAddParamDataRow(dtParamt, "@i_strChgLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["ChgLotNo"].ToString(), 40);
                                sql.mfAddParamDataRow(dtParamt, "@i_intChgQty", ParameterDirection.Input, SqlDbType.Int, dtDurableMatD.Rows[j]["ChgQty"].ToString());
                                sql.mfAddParamDataRow(dtParamt, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["UnitCode"].ToString(), 10);
                                sql.mfAddParamDataRow(dtParamt, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMatD.Rows[j]["EtcDesc"].ToString(), 100);
                                sql.mfAddParamDataRow(dtParamt, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatD.Rows[j]["CancelFlag"].ToString(), 1);
                                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                                sql.mfAddParamDataRow(dtParamt, "@o_strRepairGICode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                                sql.mfAddParamDataRow(dtParamt, "@o_strSeq", ParameterDirection.Output, SqlDbType.VarChar, 100);
                                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                                //--프로시저실행--//
                                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQUDurableMatRepairD", dtParamt);

                                //--처리결과값 구별--//
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                //--처리결과 값에 따른처리--//
                                //--실패시 For문이 멈추고 리턴--//
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    return strErrRtn;
                                }

                                dtDurableMatD.Rows[j]["Seq"] = ErrRtn.mfGetReturnValue(1);
                                dtDurableMatD.Rows[j]["CancelFlag"] = "T";
                            }

                            #endregion

                        }

                        #endregion

                        #region 신규Lot정보
                        //신규Lot등록정보가 있는 경우
                        if (dtDurableLot.Rows.Count > 0)
                        {
                            RepairLotHistory clsRepairLotHistory = new RepairLotHistory();

                            //신규Lot정보 등록 매서드 실행
                            strErrRtn = clsRepairLotHistory.mfSaveRepairLotHistory(strPlantCode,strRepairReqCode,dtDurableLot, strUserIP, strUserID, sql.SqlCon, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);    //Decoding

                            //처리 실패시 롤백
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }
                        #endregion
                    }
                    //처리성공시 트랜젝션 커밋 후 MES등록을 한다.
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();

                        #region MES

                        if (!strRepairReqCode.Equals(string.Empty))
                        {
                            strErrChk = "M";

                            //시스템연결정보 BL 호출
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();

                            //시스템연결정보 조회매서드실행
                            //DataTable dtAcce = clsAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S04"); //Live
                            DataTable dtAcce = clsAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S07");   //Test
                            
                            //MES로연결
                            QRPMES.IF.Tibrv clsTiv = new QRPMES.IF.Tibrv(dtAcce);

                            DataTable dt = new DataTable();

                            //설비 상태 변경 요청 매서드 실행
                            DataTable dtEquipState = clsTiv.EQP_CHANGESTATE_REQ(strFormName, strPlantCode, dtRepairDeviceChg.Rows[i]["WorkerID"].ToString(), 
                                                                                dtRequest.Rows[i]["AutoAccept"].ToString() == "T" ? "Y" : "N", "", 
                                                                                dtMES, dtMESDurableMat, 
                                                                                strUserID, strUserIP);

                            RepairReq clsReq = new RepairReq();

                            if (dtEquipState.Rows.Count > 0)
                            {
                                
                                //처리결과가 성공일 경우 테이블의 MESFlag 업데이트
                                if (dtEquipState.Rows[0]["returncode"].ToString().Equals("0"))
                                {
                                    strErrChk = "O";
                                    clsReq.mfSaveEquipRepair_MESIFFlag(strPlantCode, strRepairReqCode, "RER", strUserIP, strUserID);
                                }
                                //처리실패시 등록한정보 삭제하거나 널값처리
                                else
                                {
                                    strErrChk = "X";
                                    clsReq.mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "CHG", strUserIP, strUserID);
                                    strErrChk = "XX";
                                    clsReq.mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "RER", strUserIP, strUserID);
                                    strErrChk = "XXX";
                                    //치공구교체 취소하기
                                    if (dtDurableMatH.Rows.Count > 0)
                                    {
                                        QRPEQU.BL.EQUMGM.DurableMatRepairD clsRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();

                                        trans = sql.SqlCon.BeginTransaction();
                                        strErrRtn = clsRepairD.mfSaveDurableMatRepairD(strRepairGICode, dtDurableMatD, strUserIP, strUserID, sql.SqlCon, trans);

                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                        if (!ErrRtn.ErrNum.Equals(0))
                                        {
                                            trans.Rollback();
                                            return strErrRtn;
                                        }
                                        else
                                        {
                                            trans.Commit();
                                        }
                                    }
                                }

                                //성공이나 실패시 MES코드와 메세지를 EnCoding 한다.
                                ErrRtn.InterfaceResultCode = dtEquipState.Rows[0][0].ToString();
                                ErrRtn.InterfaceResultMessage = dtEquipState.Rows[0][1].ToString();

                                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                            }
                            //처리실패시 등록한정보 삭제하거나 널값처리
                            else
                            {
                                strErrChk = "X";
                                clsReq.mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "CHG", strUserIP, strUserID);
                                strErrChk = "XX";
                                clsReq.mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "RER", strUserIP, strUserID);
                                strErrChk = "XXX";
                                //치공구교체 취소하기
                                if (dtDurableMatH.Rows.Count > 0)
                                {
                                    QRPEQU.BL.EQUMGM.DurableMatRepairD clsRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();

                                    trans = sql.SqlCon.BeginTransaction();
                                    strErrRtn = clsRepairD.mfSaveDurableMatRepairD(strRepairGICode, dtDurableMatD, strUserIP, strUserID, sql.SqlCon, trans);

                                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                    if (!ErrRtn.ErrNum.Equals(0))
                                    {
                                        trans.Rollback();
                                        return strErrRtn;
                                    }
                                    else
                                    {
                                        trans.Commit();
                                    }
                                }
                            }

                            strErrChk = "";
                        }

                        #endregion
                    }

                }

                //처리결과리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                #region ErrorRollback

                // M : MES처리실패, O : Flag업데이트 실패, X : RepairDeviceChg테이블삭제실패 , XX : 결과등록삭제 실패 , 치공구상세 취소 실패
                if (!strErrChk.Equals(string.Empty))
                {
                    RepairReq clsReq = new RepairReq();

                    if (strErrChk.Equals("O"))
                    {
                        clsReq.mfSaveEquipRepair_MESIFFlag(strPlantCode, strRepairReqCode, "RER", strUserIP, strUserID);
                    }
                    if (strErrChk.Equals("X") || strErrChk.Equals("M"))
                    {
                        clsReq.mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "CHG", strUserIP, strUserID);
                    }
                    if (strErrChk.Equals("XX") || strErrChk.Equals("M"))
                    {
                        clsReq.mfSaveEquipRepair_InitReqInfo(strPlantCode, strRepairReqCode, "RER", strUserIP, strUserID);
                    }
                    if (strErrChk.Equals("XXX") || strErrChk.Equals("M"))
                    {
                        if (dtDurableMatH.Rows.Count > 0)
                        {
                            QRPEQU.BL.EQUMGM.DurableMatRepairD clsRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();

                            clsRepairD.mfSaveDurableMatRepairD_POP(strRepairGICode, dtDurableMatD, strUserIP, strUserID);
                        }
                    }
                    
                }
                
                #endregion

                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



    }

    #endregion

    #region LotNo신규등록내역

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairLotHistory")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class RepairLotHistory : ServicedComponent
    {
        /// <summary>
        /// 컬럼정보 조회
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtDataColumns = new DataTable();
            try
            {
                //dtDataColumns.Columns.Add("PlantCode", typeof(string));
                //dtDataColumns.Columns.Add("RepairReqCode", typeof(string));
                dtDataColumns.Columns.Add("LotNo", typeof(string));
                dtDataColumns.Columns.Add("EquipCode", typeof(string));
                dtDataColumns.Columns.Add("Package", typeof(string));
                dtDataColumns.Columns.Add("ModelName", typeof(string));
                dtDataColumns.Columns.Add("EtcDesc", typeof(string));
                dtDataColumns.Columns.Add("Qty", typeof(string));
                dtDataColumns.Columns.Add("WriteID", typeof(string));

                return dtDataColumns;
            }
            catch (Exception ex)
            {
                return dtDataColumns;
                throw (ex);
            }
            finally
            {
                dtDataColumns.Dispose();
            }
        }


        /// <summary>
        /// 신규 Lot정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장/param>
        /// <param name="strLang">사용언어</param>
        /// <returns>신규Lot정보</returns>
        [AutoComplete]
        public DataTable mfReadRepairLotHistory(string strPlantCode ,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtLotInfo = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //신규 Lot정보 조회 프로시저 실행
                dtLotInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_EQURepairLotHistory", dtParam);


                //Lot정보
                return dtLotInfo;

            }
            catch (Exception ex)
            {
                return dtLotInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtLotInfo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 신규 Lot정보 투입유무 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strRepairReqCode">수리요청코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadRepairLotHistory_WriteFlag(string strPlantCode,string strRepairReqCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtFlag = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //신규 Lot정보 투입유무 조회 프로시저 실행
                dtFlag = sql.mfExecReadStoredProc(sql.SqlCon,"up_Select_EQURepairLotHistory_WriteFlag",dtParam);

                return dtFlag;
            }
            catch (Exception ex)
            {
                return dtFlag;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtFlag.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 신규LotNo정보등록 매서드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strRepairReqCode">수리요청번호</param>
        /// <param name="dtNewLotInfo">신규LotNo정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveRepairLotHistory(string strPlantCode,string strRepairReqCode,DataTable dtNewLotInfo,string strUserIP,string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i < dtNewLotInfo.Rows.Count; i++)
                {
                    //파라미터정보저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairReqCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtNewLotInfo.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtNewLotInfo.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtNewLotInfo.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strModelName", ParameterDirection.Input, SqlDbType.VarChar, dtNewLotInfo.Rows[i]["ModelName"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtNewLotInfo.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_intQty", ParameterDirection.Input, SqlDbType.VarChar, dtNewLotInfo.Rows[i]["Qty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtNewLotInfo.Rows[i]["WriteID"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_EQURepairLotHistory", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { sql.Dispose(); }
        }


        /// <summary>
        /// 신규 Lot정보 투입시 WriteFlag T 처리
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strRepairReqCode">수리요청코드</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveReapirLotHistory_WriteFlag(DataTable dtRepairLotHistory,string strUserIP,string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i < dtRepairLotHistory.Rows.Count; i++)
                {
                    //파라미터정보 저장
                    DataTable dtParam = sql.mfSetParamDataTable();


                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairLotHistory.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRepairReqCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairLotHistory.Rows[i]["RepairReqCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);


                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //신규 Lot정보 투입시 WriteFlag T 처리 프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_EQURepairLotHistory_WriteFlag", dtParam);

                    //처리 실패시 실패메시지등 리턴
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { sql.Dispose(); }
        }
    }

    #endregion
}

