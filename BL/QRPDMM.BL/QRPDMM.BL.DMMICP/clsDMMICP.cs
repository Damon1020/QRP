﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


//추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;
using QRPDB;


[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPDMM.BL.DMMICP
{
    /// <summary>
    /// Durable 재고 관련 저장 클래스

    /// </summary>

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SAVEDurableStock")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SAVEDurableStock : ServicedComponent
    {
        /// <summary>
        /// 1. 기초재고등록저장

        /// </summary>
        /// <param name="dtDurableLot">Durable 현 마스터 테이블</param>
        /// <param name="dtDurableStock">Durable 현 재고 테이블</param>
        /// <param name="dtDurableStockMoveHist">Durable 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveBasicStock
            (
                DataTable dtDurableLot
                , DataTable dtDurableStock
                , DataTable dtDurableStockMoveHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();


                //---------------- 1. DurableLot  테이블 (MASDurableLot) -------------------//
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();

                if (dtDurableLot.Rows.Count > 0)
                {
                    strErrRtn = clsDurableLot.mfSaveMASDurableLot(dtDurableLot, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. Durable 현 재고 테이블 (DMMDurableStock) -------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new DurableStock();

                if (dtDurableStock.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 3. Durable 재고이력 테이블 (DMMDurableStockMoveHist) -------------------//
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new DurableStockMoveHist();

                if (dtDurableStockMoveHist.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 2. 입고정보 저장

        /// </summary>
        /// <param name="dtDurableGR">Durable 입고확인 테이블</param>
        /// <param name="dtDurableStock">Durable 현 재고 테이블</param>
        /// <param name="dtDurableStockMoveHist">Durable 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveDurableGR
            (
               
                DataTable dtDurableGR,
                DataTable dtDurableGRD
                , DataTable dtDurableStock
                , DataTable dtDurableStockMoveHist
                , DataTable dtDurableLot
                , string strAdmit
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strGRCode = "";
            string strReturnGr = "";
            string strPlantCode = "";
            string strGRSeq = "";
            try
            {
                //디비시작
                sql.mfConnect();

                if (dtDurableGR.Rows.Count > 0)
                {
                    SqlTransaction trans;
                    //Transaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    //---------------- 1. 입고확인 테이블 (DMMDurableGR) -------------------//
                    QRPDMM.BL.DMMICP.DurableGR clsDurableGR = new DurableGR();

                    //공장,입고순번저장
                    strPlantCode = dtDurableGR.Rows[0]["PlantCode"].ToString();
                    strGRSeq = dtDurableGR.Rows[0]["GRSeq"].ToString();

                    strErrRtn = clsDurableGR.mfSaveDurableGR(dtDurableGR, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    strReturnGr = strErrRtn;
                    strGRCode = ErrRtn.mfGetReturnValue(0);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }


                    for (int i = 0; i < dtDurableGRD.Rows.Count; i++)
                    {
                        dtDurableGRD.Rows[i]["GRCode"] = strGRCode;
                    }

                    //---------------- 2. 입고확인 아이템 테이블 (DMMDurableGRD) -------------------//
                    
                    strErrRtn = clsDurableGR.mfDeleteDurableGRD(strPlantCode, strGRCode, strGRSeq, sql.SqlCon, trans); //입고확인 아이템테이블을 삭제한다.
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                    if (dtDurableGRD.Rows.Count > 0)
                    {
                        strErrRtn = clsDurableGR.mfSaveDurableGRD(dtDurableGRD, strUserIP, strUserID, sql, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }

                    //중간저장이 아닌 승인된상태의 저장경우 재고,재고이력,LotNo에 저장한다.
                    if (!strAdmit.Equals(string.Empty))
                    {

                        //---------------- 3. Durable 현 재고 테이블 (DMMDurableStock) -------------------//
                        QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new DurableStock();

                        if (dtDurableStock.Rows.Count > 0)
                        {
                            strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }


                        //---------------- 4. Durable 재고이력 테이블 (DMMDurableStockMoveHist) -------------------//

                        for (int i = 0; i < dtDurableStockMoveHist.Rows.Count; i++)
                        {
                            dtDurableStockMoveHist.Rows[i]["DocCode"] = strGRCode;
                        }
                        QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new DurableStockMoveHist();

                        if (dtDurableStockMoveHist.Rows.Count > 0)
                        {
                            strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist, strUserIP, strUserID, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }

                        //---------------- 5. DurableLot  테이블 (MASDurableLot) -------------------//
                        QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();

                        if (dtDurableLot.Rows.Count > 0)
                        {
                            strErrRtn = clsDurableLot.mfSaveMASDurableLot(dtDurableLot, strUserIP, strUserID, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // ------------- 6. DMMDurableGR GRConfirmFlag 'T'로 업데이트 --------------//
                        strErrRtn = clsDurableGR.mfSaveDurableGR_ConfirmFlag(strPlantCode, strGRCode, strGRSeq, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }


                    
                    //strErrRtn = strReturnGr;

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                        ErrRtn.mfAddReturnValue(strGRCode); //파일전송을 위한 입고번호 ReturnValue추가
                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    }
                }
                else
                {
                    ErrRtn.ErrMessage = "치공구 입고정보가 없습니다.";
                    ErrRtn.ErrNum = -1;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 3. 자재출고 저장

        /// </summary>
        /// <param name="dtSPDurableTransferH">Durable 자재출고 헤더 테이블</param>
        /// <param name="dtSPDurableTransferD">Durable 자재출고 아이템 테이블</param>
        /// <param name="dtDurableStock">Durable 현 재고 테이블</param>
        /// <param name="dtDurableStockMoveHist">Durable 재고 이동 이력 테이블</param>
        /// <param name="dtDurableChgStandby">Durable 현 대기 재고 테이블</param>
        /// <param name="strStockFlag">재고 적용여부 플래그</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveTransfer(DataTable dtDurableTransferH, DataTable dtDurableTransferD, DataTable dtDurableChgStandby,
                                        DataTable dtDurableStock, DataTable dtDurableStockMoveHist, string strStockFlag, 
                                        string strUserIP, string strUserID)
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                #region Header
                //---------------- 1. 자재출고 헤더 테이블 (DMMDurableTransferH) -------------------//
                QRPDMM.BL.DMMICP.DurableTransferH clsDurableTransferH = new DurableTransferH();

                if (dtDurableTransferH.Rows.Count > 0)
                {
                    strErrRtn = clsDurableTransferH.mfSaveDurableTransferH(dtDurableTransferH, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    strTransferCode = ErrRtn.mfGetReturnValue(0);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                #endregion

                for (int i = 0; i < dtDurableTransferD.Rows.Count; i++)
                {
                    dtDurableTransferD.Rows[i]["TransferCode"] = strTransferCode;
                }

                #region Detail
                //---------------- 2. 자재출고 아이템 테이블 (DMMDurableTransferD) -------------------//
                QRPDMM.BL.DMMICP.DurableTransferD clsDurableTransferD = new DurableTransferD();
                //--- 저장 ---//
                if (dtDurableTransferD.Rows.Count > 0)
                {
                    strErrRtn = clsDurableTransferD.mfSaveDurableTransferD(dtDurableTransferD, strTransferCode, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                #endregion

                //------------------- 재고 적용여부가 F 일 경우는 자재출고 헤더/아이템 테이블에만 저장 한다 -----------------///
                if (strStockFlag == "F")
                {
                    if (ErrRtn.ErrNum == 0)
                    {
                        trans.Commit();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                #region StandBy
                //---------------- 3. 대기 재고 테이블 (DMMDurableChgStandby) -------------------//
                QRPDMM.BL.DMMICP.DurableChgStandby clsDurableChgStandby = new DurableChgStandby();

                if (dtDurableChgStandby.Rows.Count > 0)
                {
                    strErrRtn = clsDurableChgStandby.mfSaveDurableChgStandby(dtDurableChgStandby, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                #endregion

                #region Stock
                //---------------- 4. Durable 현 재고 테이블 (DMMDurableStock) -------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new DurableStock();


                if (dtDurableStock.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                #endregion

                #region History
                //---------------- 5. Durable 재고이력 테이블 (DMMDurableStockMoveHist) -------------------//
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new DurableStockMoveHist();

                if (dtDurableStockMoveHist.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDurableStockMoveHist.Rows.Count; i++)
                    {
                        dtDurableStockMoveHist.Rows[i]["DocCode"] = strTransferCode;
                    }

                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                #endregion

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 4. 자재교체 저장

        /// </summary>
        /// <param name="dtDurableChgStandbyInput">Durable 현 대기 재고 테이블 : 설비에 투입될 데이터</param>
        /// <param name="dtDurableChgStandbyOut">Durable 현 대기 재고 테이블 : 설비에서 교체될 데이터</param>
        /// <param name="dtDurableTransferD">Durable BOM 테이블에 갱신할 데이터</param> 
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveDurableChange
            (
                DataTable dtDurableTransferD
                , DataTable dtDurableChgStandbyInput
                , DataTable dtDurableChgStandbyOut
                , DataTable dtEquipDurableBOM
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                #region Detail
                //---------------- 1. 자재출고 아이템 테이블 (DMMDurableTransferD) -------------------//
                QRPDMM.BL.DMMICP.DurableTransferD clsDurableTransferD = new DurableTransferD();

                //--- 저장 ---//
                if (dtDurableTransferD.Rows.Count > 0)
                {
                    strErrRtn = clsDurableTransferD.mfSaveDurableTransferD(dtDurableTransferD, "", strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                #endregion

                #region StandBy
                //---------------- 2. 대기 재고 테이블 (DMMDurableChgStandby) :설비에 투입될 데이터-------------------//
                QRPDMM.BL.DMMICP.DurableChgStandby clsDurableChgStandby = new DurableChgStandby();

                if (dtDurableChgStandbyInput.Rows.Count > 0)
                {
                    strErrRtn = clsDurableChgStandby.mfSaveDurableChgStandby(dtDurableChgStandbyInput, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 3. 대기 재고 테이블 (DMMDurableChgStandby) :설비에서 교체될 데이터-------------------//
                if (dtDurableChgStandbyOut.Rows.Count > 0)
                {
                    strErrRtn = clsDurableChgStandby.mfSaveDurableChgStandby(dtDurableChgStandbyOut, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                #endregion

                #region BOM
                //---------------- 4. Durable BOM 테이블 (MASEquipSTBOM) -------------------//
                if (dtEquipDurableBOM.Rows.Count > 0)
                {

                    strErrRtn = clsDurableTransferD.mfSaveMASEquipDurableBOM_Chg(dtEquipDurableBOM, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                #endregion

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 5. 자재반납 저장
        /// </summary>
        /// <param name="dtDurableTransferD">자재반납정보 아이템 테이블</param> 
        /// <param name="dtDurableChgStandby">Durable 현 대기 재고 테이블 </param>
        /// <param name="dtSPDMMDurableStock">Durable 현 재고 테이블 </param>
        /// <param name="dtDurableStockMoveHist">Durable 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveDurableReturn
            (
                DataTable dtDurableTransferD
                , DataTable dtDurableChgStandby
                , DataTable dtDurableStock
                , DataTable dtDurableStockMoveHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재출고 아이템 테이블 (DMMDurableTransferD) -------------------//
                QRPDMM.BL.DMMICP.DurableTransferD clsDurableTransferD = new DurableTransferD();

                //--- 저장 ---//
                if (dtDurableTransferD.Rows.Count > 0)
                {
                    strErrRtn = clsDurableTransferD.mfSaveDurableTransferD(dtDurableTransferD, "", strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. 대기 재고 테이블 (DMMDurableChgStandby) :설비에 투입될 데이터-------------------//
                QRPDMM.BL.DMMICP.DurableChgStandby clsDurableChgStandby = new DurableChgStandby();

                if (dtDurableChgStandby.Rows.Count > 0)
                {
                    strErrRtn = clsDurableChgStandby.mfSaveDurableChgStandby(dtDurableChgStandby, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 3. Durable 현 재고 테이블 (DMMDurableStock) -------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new DurableStock();

                if (dtDurableStock.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 4. Durable 재고이력 테이블 (DMMDurableStockMoveHist) -------------------//
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new DurableStockMoveHist();

                if (dtDurableStockMoveHist.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }




                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 6. 자재이동 저장

        /// </summary>
        /// <param name="dtDurableStock_Minus">Durable 현 재고 테이블 : 현재창고 - 데이터</param>
        /// <param name="dtDurableStock_Plus">Durable 현 재고 테이블 : 이동창고 + 데이터</param>
        /// <param name="dtDurableStockMoveHist_Minus">Durable 재고 이동 이력 테이블 : 현재창고 - 데이터</param>
        /// <param name="dtDurableStockMoveHist_Plus">Durable 재고 이동 이력 테이블 : 이동창고 + 데이터</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveMove
            (
                DataTable dtDurableStock_Minus
                , DataTable dtDurableStock_Plus
                , DataTable dtDurableStockMoveHist_Minus
                , DataTable dtDurableStockMoveHist_Plus
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            { 
                //디비시작
                sql.mfConnect();
                SqlTransaction trans; 

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. Durable 현 재고 테이블 (DMMDurableStock) : 현재창고 - 데이터-------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new DurableStock();


                if (dtDurableStock_Minus.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock_Minus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. Durable 현 재고 테이블 (DMMDurableStock) : 현재창고 + 데이터-------------------//

                if (dtDurableStock_Plus.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock_Plus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 3. Durable 재고이력 테이블 (DMMDurableStockMoveHist)  : 현재창고 - 데이터-------------------//
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new DurableStockMoveHist();

                if (dtDurableStockMoveHist_Minus.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist_Minus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 4. Durable 재고이력 테이블 (DMMDurableStockMoveHist)  : 현재창고 + 데이터-------------------//

                if (dtDurableStockMoveHist_Plus.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist_Plus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        
        /// <summary>
        /// 7. 자재폐기 저장
        /// </summary>
        /// <param name="dtDurableDiscard">자재폐기 테이블</param>
        /// <param name="dtDurableStock">SparePart 현 재고 테이블</param>
        /// <param name="dtDurableStockMoveHist">SparePart 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveDiscard
            (
                DataTable dtDurableDiscard
                , DataTable dtDurableStock
                , string strUserIP
                , string strUserID
            )
        {
            #region Original BL
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strDiscardCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재출고 헤더 테이블 (DMMDurableSPTransferH) : 재고이력은 프로시져에서 처리함-------------------//
                QRPDMM.BL.DMMICP.DurableDiscard clsDurableDiscard = new DurableDiscard();

                if (dtDurableDiscard.Rows.Count > 0)
                {
                    strErrRtn = clsDurableDiscard.mfSaveDurableDiscard(dtDurableDiscard, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //strDiscardCode = ErrRtn.mfGetReturnValue(0);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. SparePart 현 재고 테이블 (EQUDurableStock) -------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new DurableStock();


                if (dtDurableStock.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }



                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
            #endregion

            #region MES IF TEST
            //try
            //{
            //    QRPMES.IF.Tibrv tib = new QRPMES.IF.Tibrv();
            //    DataTable dtlist = new DataTable();
            //    dtlist.Columns.Add("CONSUMABLEID");
            //    DataRow dr = dtlist.NewRow();
            //    dr["CONSUMABLEID"] = "123";
            //    dtlist.Rows.Add(dr);
            //    dr = dtlist.NewRow();
            //    dr["CONSUMABLEID"] = "321";
            //    dtlist.Rows.Add(dr);

            //    //DataTable dt = new DataTable();
            //    DataTable dt = tib.LOT_HOLD4QC_REQ("1234", "admin", "*", "123", dtlist, "10.60.24.163");
                

            //    string Rtn = "";
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        for (int j = 0; j < dt.Columns.Count; j++)
            //        {
            //            Rtn += dt.Rows[i][j].ToString() + "|";
            //        }
            //    }
            //    return Rtn;
            //}
            //catch(Exception ex)
            //{
            //    return ex.Message.ToString() + "\n" + ex.StackTrace.ToString();
            //}
            #endregion
        }

        [AutoComplete(false)]
        public string mfSaveMESIFErrorLog(string strFormName, string xmlSendMessage, string xmlReplyMessage, string strReturnCode, string strReturnMessage, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strFormName", ParameterDirection.Input, SqlDbType.VarChar, strFormName, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_xmlSendMessage", ParameterDirection.Input, SqlDbType.VarChar, xmlSendMessage, 8000);
                sql.mfAddParamDataRow(dtParameter, "@i_xmlReplyMessage", ParameterDirection.Input, SqlDbType.VarChar, xmlReplyMessage, 8000);

                sql.mfAddParamDataRow(dtParameter, "@i_strReturnCode", ParameterDirection.Input, SqlDbType.VarChar, strReturnCode, 50);
                sql.mfAddParamDataRow(dtParameter, "@i_strReturnMessage", ParameterDirection.Input, SqlDbType.VarChar, strReturnMessage, 50);

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSMESIFErrorLog", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                //if (ErrRtn.ErrNum != 0)
                //{
                //    trans.Rollback();
                //    return strErrRtn;
                //}
                trans.Commit();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                trans.Commit();

                //trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 7-1. 자재폐기 삭제

        /// </summary>
        /// <param name="dtDurableDiscard">자재폐기 테이블</param>
        /// <param name="dtDurableStock">SparePart 현 재고 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfDeleteDiscard
            (
                DataTable dtDurableDiscard
                , DataTable dtDurableStock
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strDiscardCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재출고 헤더 테이블 (DMMDurableSPTransferH) : 재고이력은 프로시져에서 처리함-------------------//
                QRPDMM.BL.DMMICP.DurableDiscard clsDurableDiscard = new DurableDiscard();

                if (dtDurableDiscard.Rows.Count > 0)
                {
                    strErrRtn = clsDurableDiscard.mfDeleteDurableDiscard(dtDurableDiscard, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //strDiscardCode = ErrRtn.mfGetReturnValue(0);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. SparePart 현 재고 테이블 (EQUDurableStock) -------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new DurableStock();


                if (dtDurableStock.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }



                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;


            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 8. 자재실사 저장

        /// </summary>
        /// <param name="dtDurableStockTakeH">Durable 자재실사 헤더 테이블</param>
        /// <param name="dtDurableStockTakeD">Durable 자재실사 아이템 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveStockTake
            (
                DataTable dtDurableStockTakeH
                , DataTable dtDurableStockTakeD
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strStockTakeCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재실사 헤더 테이블 (DMMDurableStockTakeH) -------------------//
                QRPDMM.BL.DMMICP.DurableStockTakeH clsDurableStockTakeH = new DurableStockTakeH();

                if (dtDurableStockTakeH.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockTakeH.mfSaveDurableStockTakeH(dtDurableStockTakeH, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    strStockTakeCode = ErrRtn.mfGetReturnValue(0);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                for (int i = 0; i < dtDurableStockTakeD.Rows.Count; i++)
                {
                    dtDurableStockTakeD.Rows[i]["StockTakeCode"] = strStockTakeCode;
                }

                //---------------- 2. 자재실사 아이템 테이블 (DMMDurableStockTakeD) -------------------//
                QRPDMM.BL.DMMICP.DurableStockTakeD clsDurableStockTakeD = new DurableStockTakeD();

                //--- 삭제 ---//
                strErrRtn = clsDurableStockTakeD.mfDeleteDurableStockTakeD(dtDurableStockTakeD, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                //--- 저장 ---//
                if (dtDurableStockTakeD.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockTakeD.mfSaveDurableStockTakeD(dtDurableStockTakeD, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 9. 자재실사 확정 저장

        /// </summary>
        /// <param name="dtDurableStockTakeH">Durable 자재출고 헤더 테이블</param>
        /// <param name="dtDurableStockTakeD">Durable 자재출고 아이템 테이블</param>
        /// <param name="dtDurableStock">Durable 현 재고 테이블</param>
        /// <param name="dtDurableStockMoveHist">Durable 재고 이동 이력 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveStockTake_Confirm
            (
                DataTable dtDurableStockTakeH
                , DataTable dtDurableStockTakeD
                , DataTable dtDurableStock
                , DataTable dtDurableStockMoveHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strTransferCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 자재실사 헤더 테이블 (DMMDurableStockTakeH) -------------------//
                QRPDMM.BL.DMMICP.DurableStockTakeH clsDurableStockTakeH = new DurableStockTakeH();

                if (dtDurableStockTakeH.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockTakeH.mfSaveDurableStockTakeH(dtDurableStockTakeH, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. 자재실사 아이템 테이블 (DMMDurableStockTakeD) -------------------//
                QRPDMM.BL.DMMICP.DurableStockTakeD clsDurableStockTakeD = new DurableStockTakeD();

                //--- 삭제 ---//
                strErrRtn = clsDurableStockTakeD.mfDeleteDurableStockTakeD(dtDurableStockTakeD, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                //--- 저장 ---//
                if (dtDurableStockTakeD.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockTakeD.mfSaveDurableStockTakeD(dtDurableStockTakeD, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                
                //---------------- 3. Durable 현 재고 테이블 (DMMDurableStock) -------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new DurableStock();
                
                if (dtDurableStock.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }
                
                //---------------- 4. Durable 재고이력 테이블 (DMMDurableStockMoveHist) -------------------//
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new DurableStockMoveHist();

                if (dtDurableStockMoveHist.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }



    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableStock")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableStock : ServicedComponent
    {
        #region STS

        /// <summary>
        /// 치공구재고현황
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">창고코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>치공구재고현황</returns>
        [AutoComplete]
        public DataTable mfReadDurableStockState(string strPlantCode, string strDurableInventoryCode, string strEquipCode, string strDurableMatCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableMatState = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableInventory", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //치공구재고현황 조회 프로시저 실행
                dtDurableMatState = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStockState", dtParam);

                //치공구재고현황 리턴
                return dtDurableMatState;

            }
            catch (Exception ex)
            {
                return dtDurableMatState;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtDurableStock = new DataTable();

            try
            {
                dtDurableStock.Columns.Add("PlantCode", typeof(String));
                dtDurableStock.Columns.Add("DurableInventoryCode", typeof(String));
                dtDurableStock.Columns.Add("DurableMatCode", typeof(String));
                dtDurableStock.Columns.Add("Qty", typeof(String));
                dtDurableStock.Columns.Add("UnitCode", typeof(String));

                dtDurableStock.Columns.Add("LotNo", typeof(string));

                dtDurableStock.Columns.Add("Package", typeof(string));
                dtDurableStock.Columns.Add("EMC", typeof(string));

                dtDurableStock.Columns.Add("ChgInventoryCode", typeof(String));
                dtDurableStock.Columns.Add("ChgQty", typeof(String));

                dtDurableStock.Columns.Add("UserIP", typeof(String));
                dtDurableStock.Columns.Add("UserID", typeof(String));

                return dtDurableStock;
            }
            catch (Exception ex)
            {
                return dtDurableStock;
                throw (ex);
            }
            finally
            {
                dtDurableStock.Dispose();
            }
        }


        /// <summary>
        /// Durable 현재재고 테이블 저장

        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableStock(DataTable dtDurableStock, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableStock.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStock.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStock.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStock.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStock.Rows[i]["LotNo"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStock.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStock.Rows[i]["EMC"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParamter, "@i_intQty", ParameterDirection.Input, SqlDbType.Int, dtDurableStock.Rows[i]["Qty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStock.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableStock", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 현재고가 없는 Durable 리스트조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadMASDurable_InputDurableStock(String strPlantCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurable_InputDurableStock", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 현재고가 없는 Durable 리스트조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDurableStock_Detail(String strPlantCode, String strDurableInventoryCode, String strDurableMatCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 현재고 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableInventoryCode">금형치공구창고</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>상세정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableStock_DetailLot(string strPlantCode, string strDurableInventoryCode, string strDurableMatCode, string strLotNo, string strLang)
        {
            DataTable dtDurableMatDetail = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableInventoryCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableMatCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar,strLotNo,40);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                dtDurableMatDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_Lot", dtParam);


                return dtDurableMatDetail;
            }
            catch (Exception ex)
            {
                return dtDurableMatDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatDetail.Dispose();
            }
        }

        /// <summary>
        /// 그리드 상에서 공장, 창고코드 선택시 해당되는 SP정보 가져옴 
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDurableStock_SPCombo(String strPlantCode, String strDurableInventoryCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_SPCombo", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 그리드 상에서 공장, 창고코드 선택시 해당되는 SP정보 가져옴 
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDurableStock_SPCombo_Lot(String strPlantCode, String strDurableInventoryCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_SPCombo_Lot", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 그리드 상에서 공장, 창고코드 선택시 해당되는 SP정보 가져옴 (LotNo정보만 가져옴)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDurableStock_SPCombo_OnlyLot(String strPlantCode, String strDurableInventoryCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_SPCombo_OnlyLot", dtParmter);
                //정보리턴 
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 그리드 상에서 공장, 창고코드 선택시 해당되는 SP정보 가져옴 (SerialFlag = 'F'인것만 가져옴 : Lot정보가 없는것만 가져옴)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDurableStock_SPCombo_OnlyDurable(String strPlantCode, String strDurableInventoryCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_SPCombo_OnlyDurable", dtParmter);
                //정보리턴 
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 공장, 치공구코드정보로 재고에 있는Lot목록 가져오기

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableStock_ComboLot(String strPlantCode, String strDurableMatCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_ComboLot", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }



        /// <summary>
        /// 치공구 재고정보 콤보
        /// </summary>
        /// <param name="strPlantCode">공장정보</param>
        /// <param name="strDurableInventoryCode">창고정보</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strModelName">모델명</param>
        /// <param name="strSerial">LotNo여부 T : 있음,F : 없음, 이외</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>치공구재고정보</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableStock_Combo(string strPlantCode,string strDurableInventoryCode, string strPackage, string strModelName,string strSerial, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtStock = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableInventoryCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar,strPackage,40);
                sql.mfAddParamDataRow(dtParam,"@i_strModelName", ParameterDirection.Input, SqlDbType.VarChar,strModelName,40);
                sql.mfAddParamDataRow(dtParam,"@i_strSerial", ParameterDirection.Input, SqlDbType.VarChar,strSerial,1);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                //치공구재고정보프로시저 실행
                dtStock = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_Combo", dtParam);

                //치공구재고정보
                return dtStock;

            }
            catch (Exception ex)
            {
                return dtStock;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtStock.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 치공구 재고정보 콤보
        /// </summary>
        /// <param name="strPlantCode">공장정보</param>
        /// <param name="strDurableInventoryCode">창고정보</param>
        /// <param name="strSerial">LotNo여부 T : 있음,F : 없음, 이외</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>치공구재고정보</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableStock_Combo_SerialChk(string strPlantCode, string strDurableInventoryCode, string strSerial, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtStock = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSerial", ParameterDirection.Input, SqlDbType.VarChar, strSerial, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //치공구재고정보프로시저 실행
                dtStock = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStock_Combo_SerialChk", dtParam);

                //치공구재고정보
                return dtStock;

            }
            catch (Exception ex)
            {
                return dtStock;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtStock.Dispose();
                sql.Dispose();
            }
        }

        #endregion

        #region PSTS

        /// <summary>
        /// 치공구재고현황
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">창고코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>치공구재고현황</returns>
        [AutoComplete]
        public DataTable mfReadDurableStockState_PSTS_frmDMM0023_S(string strPlantCode, string strDurableInventoryCode, string strEquipCode, string strDurableMatCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableMatState = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableInventory", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //치공구재고현황 조회 프로시저 실행
                dtDurableMatState = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStockState_PSTS_frmDMM0023_S", dtParam);

                //치공구재고현황 리턴
                return dtDurableMatState;

            }
            catch (Exception ex)
            {
                return dtDurableMatState;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableStockMoveHist")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableStockMoveHist : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtDurableStockMoveHist = new DataTable();

            try
            {
                dtDurableStockMoveHist.Columns.Add("MoveGubunCode", typeof(String));
                dtDurableStockMoveHist.Columns.Add("DocCode", typeof(String));
                dtDurableStockMoveHist.Columns.Add("MoveDate", typeof(String));
                dtDurableStockMoveHist.Columns.Add("MoveChargeID", typeof(String));
                dtDurableStockMoveHist.Columns.Add("PlantCode", typeof(String));
                dtDurableStockMoveHist.Columns.Add("ProductCode", typeof(String));
                dtDurableStockMoveHist.Columns.Add("DurableInventoryCode", typeof(String));
                dtDurableStockMoveHist.Columns.Add("LotNo", typeof(String));

                dtDurableStockMoveHist.Columns.Add("Package", typeof(String));
                dtDurableStockMoveHist.Columns.Add("EMC", typeof(String));

                dtDurableStockMoveHist.Columns.Add("EquipCode", typeof(String));
                dtDurableStockMoveHist.Columns.Add("DurableMatCode", typeof(String));
                dtDurableStockMoveHist.Columns.Add("MoveQty", typeof(String));
                dtDurableStockMoveHist.Columns.Add("UnitCode", typeof(String));

                dtDurableStockMoveHist.Columns.Add("UserIP", typeof(String));
                dtDurableStockMoveHist.Columns.Add("UserID", typeof(String));

                return dtDurableStockMoveHist;
            }
            catch (Exception ex)
            {
                return dtDurableStockMoveHist;
                throw (ex);
            }
            finally
            {
                dtDurableStockMoveHist.Dispose();
            }
        }


        /// <summary>
        /// Durable 재고이동 이력 테이블 저장

        /// </summary>
        /// <param name="dtDurableStockMoveHistk">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableStockMoveHist(DataTable dtDurableStockMoveHist, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableStockMoveHist.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strMoveGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["MoveGubunCode"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["DocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strMoveDate", ParameterDirection.Input, SqlDbType.Char, dtDurableStockMoveHist.Rows[i]["MoveDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strMoveChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["MoveChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["ProductCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["LotNo"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["EMC"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParamter, "@i_intMoveQty", ParameterDirection.Input, SqlDbType.Int, dtDurableStockMoveHist.Rows[i]["MoveQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockMoveHist.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableStockMoveHist", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 치공구 재고이력 화면

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLang">사용언어</param>
        [AutoComplete]
        public DataTable mfReadDMMDurableStockMoveHist_Display(String strPlantCode, String strDurableMatCode, String strLotNo, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStockMoveHist_Display", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableGR")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableGR : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtDurableStock = new DataTable();

            try
            {
                dtDurableStock.Columns.Add("PlantCode", typeof(String));
                dtDurableStock.Columns.Add("GRCode", typeof(String));
                dtDurableStock.Columns.Add("GRSeq", typeof(String));
                dtDurableStock.Columns.Add("GRDate", typeof(String));
                dtDurableStock.Columns.Add("DurableMatCode", typeof(String));
                dtDurableStock.Columns.Add("GRQty", typeof(String));
                dtDurableStock.Columns.Add("UnitCode", typeof(String));
                dtDurableStock.Columns.Add("GRConfirmFlag", typeof(String));
                dtDurableStock.Columns.Add("GRConfirmDate", typeof(String));
                dtDurableStock.Columns.Add("GRConfirmID", typeof(String));
                dtDurableStock.Columns.Add("GRDurableInventoryCode", typeof(String));
                dtDurableStock.Columns.Add("PONumber", typeof(String));

                dtDurableStock.Columns.Add("UserIP", typeof(String));
                dtDurableStock.Columns.Add("UserID", typeof(String));

                return dtDurableStock;
            }
            catch (Exception ex)
            {
                return dtDurableStock;
                throw (ex);
            }
            finally
            {
                dtDurableStock.Dispose();
            }
        }

        public DataTable mfSetDataInfo_D()
        {
            DataTable dtDurableStock = new DataTable();

            try
            {
                dtDurableStock.Columns.Add("PlantCode", typeof(String));
                dtDurableStock.Columns.Add("GRCode", typeof(String));
                dtDurableStock.Columns.Add("GRSeq", typeof(String));
                dtDurableStock.Columns.Add("LotNo", typeof(String));
                dtDurableStock.Columns.Add("LotSeq", typeof(String));
                dtDurableStock.Columns.Add("RevNo", typeof(String));
                dtDurableStock.Columns.Add("InspectResult", typeof(String));
                dtDurableStock.Columns.Add("SizeResult", typeof(String));
                dtDurableStock.Columns.Add("FileName", typeof(String));
                return dtDurableStock;
            }
            catch (Exception ex)
            {
                return dtDurableStock;
                throw (ex);
            }
            finally
            {
                dtDurableStock.Dispose();
            }
        }


        /// <summary>
        /// 치공구 입고확인 테이블 저장
        /// </summary>
        /// <param name="dtDurableGR">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableGR(DataTable dtDurableGR, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableGR.Rows.Count; i++)
                {
                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["GRCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intGRSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableGR.Rows[i]["GRSeq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["GRDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intGRQty", ParameterDirection.Input, SqlDbType.Int, dtDurableGR.Rows[i]["GRQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["GRConfirmFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["GRConfirmDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRConfirmID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["GRConfirmID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["GRDurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPONumber", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGR.Rows[i]["PONumber"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                    sql.mfAddParamDataRow(dtParamter, "@o_strGRCode", ParameterDirection.Output, SqlDbType.VarChar, 50);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableGR", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0) 
                    {
                        break;
                    }
                }

               
                //결과 값 리턴
                return strErrRtn;
            }
                 

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 치공구입고정보 입고확인Flag 업데이트
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strGRCode">입고번호</param>
        /// <param name="strGRSeq">입고순번</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveDurableGR_ConfirmFlag(string strPlantCode, string strGRCode, string strGRSeq, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strGRCode", ParameterDirection.Input, SqlDbType.VarChar, strGRCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intGRSeq", ParameterDirection.Input, SqlDbType.Int, strGRSeq);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_DMMDurableGR_GRConfirmFlag", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {

                sql.Dispose();
            }
        }

        /// <summary>
        /// 현재고가 없는 SparePart 리스트조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableGR(String strPlantCode, String strFromGRDate, String strToGRDate, String strGRConfirmFlag, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromGRDate", ParameterDirection.Input, SqlDbType.VarChar, strFromGRDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToGRDate", ParameterDirection.Input, SqlDbType.VarChar, strToGRDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strGRConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, strGRConfirmFlag, 1);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableGR", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 현재고가 없는 SparePart 리스트조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableGR_Detail(String strPlantCode, String strGRCode, String strGRSeq, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strGRCode", ParameterDirection.Input, SqlDbType.VarChar, strGRCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_intGRSeq", ParameterDirection.Input, SqlDbType.Int, strGRSeq);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableGR_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        #region DMMDurableGRD


        /// <summary>
        /// 치공구입고상세 LotNo정보
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strLotNo">LotNo</param>
        /// <returns>치공구입고상세LotNo</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableGRD_LotNo(string strPlantCode, string strGRCode,string strGRSeq ,string strLotNo)
        {
            DataTable dtGRD = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strGRCode", ParameterDirection.Input, SqlDbType.VarChar, strGRCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intGRSeq", ParameterDirection.Input, SqlDbType.Int, strGRSeq);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);

                //치공구 입고상세 LotNo확인
                dtGRD = sql.mfExecReadStoredProc(sql.SqlCon,"up_Select_DMMDurableGRD_LotNo",dtParam);

                //해당검색조건의 LotNo정보
                return dtGRD;
            }
            catch (Exception ex)
            {
                return dtGRD;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtGRD.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 현재고가 없는 SparePart 리스트조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableGRD(String strPlantCode, String strGRCode, String strGRSeq, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strGRCode", ParameterDirection.Input, SqlDbType.VarChar, strGRCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_intGRSeq", ParameterDirection.Input, SqlDbType.Int, strGRSeq);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableGRD", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 치공구 입고확인 테이블 저장
        /// </summary>
        /// <param name="dtDurableGR">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableGRD(DataTable dtDurableGRD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableGRD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGRD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strGRCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGRD.Rows[i]["GRCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intGRSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableGRD.Rows[i]["GRSeq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@I_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGRD.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRevNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGRD.Rows[i]["RevNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strInspectResult", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGRD.Rows[i]["InspectResult"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSizeResult", ParameterDirection.Input, SqlDbType.VarChar, dtDurableGRD.Rows[i]["SizeResult"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableGRD.Rows[i]["FileName"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableGRD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }


                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }



        /// <summary>
        /// 치공구입고아이템정보 삭제
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strGRCode">입고번호</param>
        /// <param name="strGRSeq">입고순번</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableGRD(string strPlantCode, string strGRCode, string strGRSeq,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
           
            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strGRCode", ParameterDirection.Input, SqlDbType.VarChar,strGRCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_intGRSeq", ParameterDirection.Input, SqlDbType.Int,strGRSeq);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);


                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_DMMDurableGRD", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
               
                sql.Dispose();
            }
        }


        #endregion

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableTransferH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableTransferH : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtDurableTransferH = new DataTable();

            try
            {
                dtDurableTransferH.Columns.Add("PlantCode", typeof(String));
                dtDurableTransferH.Columns.Add("TransferCode", typeof(String));
                dtDurableTransferH.Columns.Add("TransferDate", typeof(String));
                dtDurableTransferH.Columns.Add("TransferChargeID", typeof(String));
                dtDurableTransferH.Columns.Add("EtcDesc", typeof(String));
                dtDurableTransferH.Columns.Add("StockFlag", typeof(String));

                dtDurableTransferH.Columns.Add("UserIP", typeof(String));
                dtDurableTransferH.Columns.Add("UserID", typeof(String));

                return dtDurableTransferH;
            }
            catch (Exception ex)
            {
                return dtDurableTransferH;
                throw (ex);
            }
            finally
            {
                dtDurableTransferH.Dispose();
            }
        }

        /// <summary>
        /// 자재출고 헤더정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableTransferH(String strPlantCode, String strFromTransferDate, String strToTransferDate, String strStockFlag, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromTransferDate", ParameterDirection.Input, SqlDbType.VarChar, strFromTransferDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToTransferDate", ParameterDirection.Input, SqlDbType.VarChar, strToTransferDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockFlag", ParameterDirection.Input, SqlDbType.VarChar, strStockFlag, 1);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableTransferH", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 자재출고 헤더정보 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strTransferCode">출고번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>자재출고 헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableTransferH_Detail(String strPlantCode, String strTransferCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, strTransferCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableTransferH_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// SparePart 자재출고 테이블 저장(헤더)
        /// </summary>
        /// <param name="dtDurableTransferH">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableTransferH(DataTable dtDurableTransferH, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableTransferH.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferH.Rows[i]["TransferCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferH.Rows[i]["TransferDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferH.Rows[i]["TransferChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferH.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferH.Rows[i]["StockFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strTransferCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableTransferH", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


    }



    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableTransferD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableTransferD : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정 
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtDurableTransferD = new DataTable();

            try
            {
                dtDurableTransferD.Columns.Add("PlantCode", typeof(String));
                dtDurableTransferD.Columns.Add("TransferCode", typeof(String));
                dtDurableTransferD.Columns.Add("TransferSeq", typeof(String));
                dtDurableTransferD.Columns.Add("TransferDurableInventoryCode", typeof(String));
                dtDurableTransferD.Columns.Add("ProductCode", typeof(String));
                dtDurableTransferD.Columns.Add("TransferDurableMatCode", typeof(String));
                dtDurableTransferD.Columns.Add("TransferLotNo", typeof(String));
                dtDurableTransferD.Columns.Add("TransferQty", typeof(String));
                dtDurableTransferD.Columns.Add("ChgEquipCode", typeof(String));
                dtDurableTransferD.Columns.Add("ChgDurableMatCode", typeof(String));
                dtDurableTransferD.Columns.Add("ChgLotNo", typeof(String));
                dtDurableTransferD.Columns.Add("InputQty", typeof(String));
                dtDurableTransferD.Columns.Add("UnitCode", typeof(String));
                dtDurableTransferD.Columns.Add("TransferEtcDesc", typeof(String));
                dtDurableTransferD.Columns.Add("ChgFlag", typeof(String));
                dtDurableTransferD.Columns.Add("ChgDate", typeof(String));
                dtDurableTransferD.Columns.Add("ChgChargeID", typeof(String));
                dtDurableTransferD.Columns.Add("ChgEtcDesc", typeof(String));
                dtDurableTransferD.Columns.Add("ReturnFlag", typeof(String));
                dtDurableTransferD.Columns.Add("ReturnDate", typeof(String));
                dtDurableTransferD.Columns.Add("ReturnChargeID", typeof(String));
                dtDurableTransferD.Columns.Add("ReturnDurableInventoryCode", typeof(String));
                dtDurableTransferD.Columns.Add("ReturnEtcDesc", typeof(String));
                dtDurableTransferD.Columns.Add("DeleteFlag", typeof(string));

                dtDurableTransferD.Columns.Add("UserIP", typeof(String));
                dtDurableTransferD.Columns.Add("UserID", typeof(String));

                return dtDurableTransferD;
            }
            catch (Exception ex)
            {
                return dtDurableTransferD;
                throw (ex);
            }
            finally
            {
                dtDurableTransferD.Dispose();
            }
        }

        /// <summary>
        /// 자재출고 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strTransferCode">출고번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableTransferD(String strPlantCode, String strTransferCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, strTransferCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableTransferD", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// SparePart 자재출고 테이블 저장(아이템)
        /// </summary>
        /// <param name="dtDurableTransferD">저장할데이터항목</param>
        /// <param name="strTransferCode">자재출고번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableTransferD(DataTable dtDurableTransferD, string strTransferCode, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableTransferD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ProductCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["TransferCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intTransferSeq", ParameterDirection.Input, SqlDbType.Int, dtDurableTransferD.Rows[i]["TransferSeq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["TransferDurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["TransferDurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["TransferLotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_intTransferQty", ParameterDirection.Input, SqlDbType.Int, dtDurableTransferD.Rows[i]["TransferQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgEquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgDurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgLotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_intInputQty", ParameterDirection.Input, SqlDbType.Int, dtDurableTransferD.Rows[i]["InputQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["TransferEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ReturnFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ReturnDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ReturnChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ReturnDurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strReturnEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ReturnEtcDesc"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParamter, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["DeleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableTransferD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// SparePartBOM 테이블 저장(MASEquipSPBOM)
        /// </summary>
        /// <param name="dtDurableTransferD">저장할데이터항목</param>
        /// <param name="strTransferCode">자재출고번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveMASEquipDurableBOM_Chg(DataTable dtDurableTransferD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableTransferD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["TransferDurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["TransferLotNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intTransferQty", ParameterDirection.Input, SqlDbType.Int, dtDurableTransferD.Rows[i]["TransferQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgEquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgDurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["ChgLotNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intChgQty", ParameterDirection.Input, SqlDbType.Int, dtDurableTransferD.Rows[i]["InputQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, "", 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipDurableBOM_Chg", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn; 
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// SparePart 자재출고 테이블 삭제(아이템)
        /// </summary>
        /// <param name="dtDurableTransferD">저장할데이터항목</param>
        /// <param name="strTransferCode">자재출고번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteDurableTransferD(DataTable dtDurableTransferD, string strTransferCode, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableTransferD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableTransferD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strTransferCode", ParameterDirection.Input, SqlDbType.VarChar, strTransferCode, 20);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_DMMDurableTransferD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 자재교체 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableTransferD_Chg(String strPlantCode, String strEquipCode, string strChgFlag, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strChgFlag", ParameterDirection.Input, SqlDbType.VarChar, strChgFlag, 1);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableTransferD_Chg", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 자재반납 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableTransferD_Return(String strPlantCode, String strFromChgDate, string strToChgDate, string strReturnFlag, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromChgDate", ParameterDirection.Input, SqlDbType.VarChar, strFromChgDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToChgDate", ParameterDirection.Input, SqlDbType.VarChar, strToChgDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strReturnFlag", ParameterDirection.Input, SqlDbType.VarChar, strReturnFlag, 1);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableTransferD_Return", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableChgStandby")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableChgStandby : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtDurableChgStandby = new DataTable();

            try
            {
                dtDurableChgStandby.Columns.Add("PlantCode", typeof(String));
                dtDurableChgStandby.Columns.Add("DurableMatCode", typeof(String));
                dtDurableChgStandby.Columns.Add("LotNo", typeof(String));
                dtDurableChgStandby.Columns.Add("ChgQty", typeof(String));
                dtDurableChgStandby.Columns.Add("UnitCode", typeof(String));

                dtDurableChgStandby.Columns.Add("UserIP", typeof(String));
                dtDurableChgStandby.Columns.Add("UserID", typeof(String));

                return dtDurableChgStandby;
            }
            catch (Exception ex)
            {
                return dtDurableChgStandby;
                throw (ex);
            }
            finally
            {
                dtDurableChgStandby.Dispose();
            }
        }


        /// <summary>
        /// SparePart 대기 재고 테이블 저장

        /// </summary>
        /// <param name="dtDurableChgStandby">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableChgStandby(DataTable dtDurableChgStandby, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableChgStandby.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableChgStandby.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableChgStandby.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableChgStandby.Rows[i]["LotNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intChgQty", ParameterDirection.Input, SqlDbType.Int, dtDurableChgStandby.Rows[i]["ChgQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableChgStandby.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableChgStandby", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableDiscard")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableDiscard : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtDurableDiscard = new DataTable();

            try
            {
                dtDurableDiscard.Columns.Add("PlantCode", typeof(String));
                dtDurableDiscard.Columns.Add("DiscardCode", typeof(String));
                dtDurableDiscard.Columns.Add("DiscardDate", typeof(String));
                dtDurableDiscard.Columns.Add("DiscardChargeID", typeof(String));
                dtDurableDiscard.Columns.Add("DiscardReason", typeof(String));
                dtDurableDiscard.Columns.Add("DurableInventoryCode", typeof(String));
                dtDurableDiscard.Columns.Add("DurableMatCode", typeof(String));
                dtDurableDiscard.Columns.Add("LotNo", typeof(String));
                dtDurableDiscard.Columns.Add("DiscardQty", typeof(String));
                dtDurableDiscard.Columns.Add("UnitCode", typeof(String));

                dtDurableDiscard.Columns.Add("UserIP", typeof(String));
                dtDurableDiscard.Columns.Add("UserID", typeof(String));

                return dtDurableDiscard;
            }
            catch (Exception ex)
            {
                return dtDurableDiscard;
                throw (ex);
            }
            finally
            {
                dtDurableDiscard.Dispose();
            }
        }


        /// <summary>
        /// 치공구 자재폐기 테이블 저장

        /// </summary>
        /// <param name="dtDurableDiscard">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableDiscard(DataTable dtDurableDiscard, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableDiscard.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DiscardCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DiscardDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DiscardChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardReason", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DiscardReason"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_intDiscardQty", ParameterDirection.Input, SqlDbType.Int, dtDurableDiscard.Rows[i]["DiscardQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strDiscardCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableDiscard", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 치공구 자재폐기 테이블 삭제

        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteDurableDiscard(DataTable dtDurableDiscard, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableDiscard.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DiscardCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DiscardDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DiscardChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardReason", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DiscardReason"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["LotNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intDiscardQty", ParameterDirection.Input, SqlDbType.Int, dtDurableDiscard.Rows[i]["DiscardQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableDiscard.Rows[i]["UnitCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_DMMDurableDiscard", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 자재폐기  조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableDiscard(String strPlantCode, String strFromDiscardDate, string strToDiscardDate, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDiscardDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, strToDiscardDate, 10);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableDiscard", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

    }
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableStockTakeH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableStockTakeH : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("StockTakeCode", typeof(String));
                dtTable.Columns.Add("DurableInventoryCode", typeof(String));
                dtTable.Columns.Add("StockTakeYear", typeof(String));
                dtTable.Columns.Add("StockTakeMonth", typeof(String));

                dtTable.Columns.Add("StockTakeDate", typeof(String));
                dtTable.Columns.Add("StockTakeChargeID", typeof(String));
                dtTable.Columns.Add("StockTakeEtcDesc", typeof(String));
                dtTable.Columns.Add("StockTakeConfirmDate", typeof(String));
                dtTable.Columns.Add("StockTakeConfirmID", typeof(String));
                dtTable.Columns.Add("ConfirmEtcDesc", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }



        /// <summary>
        /// SparePart 자재실사 테이블 저장(헤더)
        /// </summary>
        /// <param name="dtDurableStockTakeH">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableStockTakeH(DataTable dtDurableStockTakeH, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableStockTakeH.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["StockTakeCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeYear", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["StockTakeYear"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeMonth", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["StockTakeMonth"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["StockTakeDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["StockTakeChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["StockTakeEtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["StockTakeConfirmDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeConfirmID", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["StockTakeConfirmID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strConfirmEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeH.Rows[i]["ConfirmEtcDesc"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strStockTakeCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableStockTakeH", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재실사 헤더정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">창고코드</param>
        /// <param name="strStockTakeYear">실사년도</param>
        /// <param name="strStockTakeMonth">실사월</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>자재실사 헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableStockTakeH(String strPlantCode, String strDurableInventoryCode, String strStockTakeYear, String strStockTakeMonth, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeYear", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeYear, 4);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeMonth", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeMonth, 2);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStockTakeH", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableStockTakeD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableStockTakeD : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("StockTakeCode", typeof(String));
                dtTable.Columns.Add("DurableInventoryCode", typeof(String));
                dtTable.Columns.Add("DurableMatCode", typeof(String));
                dtTable.Columns.Add("LotNo", typeof(String));
                dtTable.Columns.Add("CurStockQty", typeof(String));
                dtTable.Columns.Add("StockTakeQty", typeof(String));
                dtTable.Columns.Add("StockConfirmQty", typeof(String));
                dtTable.Columns.Add("UnitCode", typeof(String));
                dtTable.Columns.Add("EtcDesc", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }



        /// <summary>
        /// SparePart 자재실사 테이블 저장(아이템)
        /// </summary>
        /// <param name="dtDurableStockTakeD">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableStockTakeD(DataTable dtDurableStockTakeD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableStockTakeD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["StockTakeCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["LotNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intCurStockQty", ParameterDirection.Input, SqlDbType.Int, dtDurableStockTakeD.Rows[i]["CurStockQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_intStockTakeQty", ParameterDirection.Input, SqlDbType.Int, dtDurableStockTakeD.Rows[i]["StockTakeQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_intStockConfirmQty", ParameterDirection.Input, SqlDbType.Int, dtDurableStockTakeD.Rows[i]["StockConfirmQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["EtcDesc"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMDurableStockTakeD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// SparePart 자재실사 테이블 삭제(아이템)
        /// </summary>
        /// <param name="dtDurableStockTakeD">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteDurableStockTakeD(DataTable dtDurableStockTakeD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableStockTakeD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStockTakeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableStockTakeD.Rows[i]["StockTakeCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_DMMDurableStockTakeD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재실사 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">창고코드</param>
        /// <param name="strStockTakeYear">실사년도</param>
        /// <param name="strStockTakeMonth">실사월</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableStockTakeD(String strPlantCode, String strDurableInventoryCode, String strStockTakeYear, String strStockTakeMonth, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            { 
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeYear", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeYear, 4);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeMonth", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeMonth, 2);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStockTakeD", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 자재실사확정 아이템정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStockTakeCode">실사번호</param>

        /// <param name="strLang">사용언어</param>
        /// <returns>자재실사확정 아이템정보</returns>
        [AutoComplete]
        public DataTable mfReadDMMDurableStockTakeD_Confirm(String strPlantCode, String strStockTakeCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strStockTakeCode", ParameterDirection.Input, SqlDbType.VarChar, strStockTakeCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMDurableStockTakeD_Confirm", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }
    }
}
