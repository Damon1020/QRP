﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;
using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPDMM.BL.DMMMGM
{

    /// <summary>
    /// Durable 재고 관련 저장 클래스

    /// </summary>

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SAVEMGM")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SAVEMGM : ServicedComponent
    {

        /// <summary>
        /// 1. 정비점검관리 저장

        /// </summary>
        /// <param name="dtDurableLot">치공구 마스터 Lot 테이블</param>
        /// <param name="dtInspectHist">정기점검관리 테이블</param>
        /// <param name="dtRepairH">점검일지등록 헤더 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveInspectHist
            (
                DataTable dtDurableLot
                , DataTable dtInspectHist
                , DataTable dtRepairH
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strUsageDocCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //////////---------------- 1. Durable  테이블 (MASDurableMat) -------------------//
                ////////QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();

                ////////if (dtDurableMat.Rows.Count > 0)
                ////////{
                ////////    strErrRtn = clsDurableMat.mfSaveDurableMat(dtDurableMat, strUserIP, strUserID, sql, trans);
                ////////    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////////    if (ErrRtn.ErrNum != 0)
                ////////    {
                ////////        trans.Rollback();
                ////////        return strErrRtn;
                ////////    }
                ////////}

                //---------------- 1. Durable Lot 테이블 (MASDurableLot) -------------------//
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();

                if (dtDurableLot.Rows.Count > 0)
                {
                    strErrRtn = clsDurableLot.mfSaveMASDurableLot(dtDurableLot, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. Durable 정기점검 테이블 (DMMInspectHist) -------------------//
                QRPDMM.BL.DMMMGM.InspectHist clsInspectHist = new InspectHist();

                if (dtInspectHist.Rows.Count > 0)
                {
                    strErrRtn = clsInspectHist.mfSaveInspectHist(dtInspectHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    strUsageDocCode = ErrRtn.mfGetReturnValue(0);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                for (int i = 0; i < dtRepairH.Rows.Count; i++)
                {
                    dtRepairH.Rows[i]["UsageDocCode"] = strUsageDocCode;
                }

                //---------------- 3. 점검일지 등록 헤더테이블 (DMMRepairH) -------------------//
                // 정기점검관리 화면에서 Shot판정을 "정비"로 선택 했을시에만 저장함 //
                if (dtInspectHist.Rows[0]["UsageJudgeCode"].ToString() == "R")
                {
                    QRPDMM.BL.DMMMGM.RepairH clsRepairH = new RepairH();

                    if (dtRepairH.Rows.Count > 0)
                    {
                        strErrRtn = clsRepairH.mfSaveRepairH(dtRepairH, strUserIP, strUserID, sql, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        string strRepairCode = ErrRtn.mfGetReturnValue(0);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 2. 점검일지등록 저장

        /// </summary>
        /// <param name="dtRepairH">점검일지등록 헤더 테이블</param>
        /// <param name="dtRepairD">점검일지등록 아이템 테이블</param>
        /// <param name="dtDurableMatRepairH">점검출고등록 헤더 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveRepair
            (
                DataTable dtRepairH
                , DataTable dtRepairD
                , DataTable dtDurableMatRepairH
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strUsageDocCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 점검일지 등록 헤더테이블 (DMMRepairH) -------------------//
                QRPDMM.BL.DMMMGM.RepairH clsRepairH = new RepairH();

                if (dtRepairH.Rows.Count > 0)
                {
                    strErrRtn = clsRepairH.mfSaveRepairH(dtRepairH, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 2. 점검일지 등록 아이템테이블 (DMMRepairH) -------------------//
                QRPDMM.BL.DMMMGM.RepairD clsRepairD = new RepairD();

                // 2-1. 점검일지 등록 아이템 테이블 삭제
                strErrRtn = clsRepairD.mfDeleteRepairD(dtRepairD, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 2-2. 점검일지 등록 아이템 테이블 저장
                if (dtRepairD.Rows.Count > 0)
                {
                    strErrRtn = clsRepairD.mfSaveRepairD(dtRepairD, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 3. 점검출고 등록 헤더테이블 (EQUDurableMatRepairGIH) -------------------//
                if (dtRepairH.Rows[0]["ChangeFlag"].ToString() == "T")
                {
                    QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();

                    if (dtDurableMatRepairH.Rows.Count > 0)
                    {
                        strErrRtn = clsDurableMatRepairH.mfSaveDurableMatRepairH_2(dtDurableMatRepairH, strUserIP, strUserID, sql, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 3. 정비점검결과등록 저장

        /// </summary>
        /// <param name="dtDMMRepairUseSP">정비점검결과 테이블</param>
        /// <param name="dtSPStock_Minus">SP 현재고 테이블: - 처리 정보</param>
        /// <param name="dtSPStockMoveHist_Minus">SP 재고이력테이블 : - 처리 정보</param>
        /// <param name="dtEquipSPBOM">SP BOM 테이블 : 설비 교체정보</param>
        /// <param name="dtSPStock_Plus">SP 현재고 테이블: + 처리 정보</param>
        /// <param name="dtSPStockMoveHist_Plus">SP 재고이력테이블 : + 처리 정보</param>
 
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveRepairUseSP
            (
                DataTable dtDMMRepairUseSP
                , DataTable dtSPStock_Minus
                , DataTable dtSPStockMoveHist_Minus
                , DataTable dtEquipSPBOM
                , DataTable dtSPStock_Plus
                , DataTable dtSPStockMoveHist_Plus
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strUsageDocCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 정비점검결과 테이블  테이블 (DMMRepairUseSP) -------------------//
                QRPDMM.BL.DMMMGM.DMMRepairUseSP clsDMMRepairUseSP = new QRPDMM.BL.DMMMGM.DMMRepairUseSP();
                if (dtDMMRepairUseSP.Rows.Count > 0)
                {
                    strErrRtn = clsDMMRepairUseSP.mfSaveDMMRepairUseSP(dtDMMRepairUseSP, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }    
                }

                 
                //---------------- 2. SparePart 현 재고 테이블 (EQUSPStock) : 현재창고 - 데이터-------------------//

                QRPEQU.BL.EQUSPA.SPStock clsSPStock = new QRPEQU.BL.EQUSPA.SPStock();
                if (dtSPStock_Minus.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock_Minus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 3. SparePart 재고이력 테이블 (EQUSPStockMoveHist)  : 현재창고 - 데이터-------------------//
                QRPEQU.BL.EQUSPA.SPStockMoveHist clsSPStockMoveHist = new QRPEQU.BL.EQUSPA.SPStockMoveHist();

                if (dtSPStockMoveHist_Minus.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist_Minus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 4. SparePart BOM 테이블 (MASEquipSTBOM) -------------------//
                QRPEQU.BL.EQUSPA.SPTransferD clsSPTransferD = new QRPEQU.BL.EQUSPA.SPTransferD();
                if (dtEquipSPBOM.Rows.Count > 0)
                {

                    strErrRtn = clsSPTransferD.mfSaveMASEquipSPBOM_Chg(dtEquipSPBOM, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 5. SparePart 현 재고 테이블 (EQUSPStock) : 현재창고 + 데이터-------------------//

                if (dtSPStock_Plus.Rows.Count > 0)
                {
                    strErrRtn = clsSPStock.mfSaveSPStock(dtSPStock_Plus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 6. SparePart 재고이력 테이블 (EQUSPStockMoveHist)  : 현재창고 + 데이터-------------------//

                if (dtSPStockMoveHist_Plus.Rows.Count > 0)
                {
                    strErrRtn = clsSPStockMoveHist.mfSaveSPStockMoveHist(dtSPStockMoveHist_Plus, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                { 
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex); 
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 4. Shot수등록 화면에서의 저장

        /// </summary>
        /// <param name="dtDurableLot">치공구 마스터 Lot 테이블</param>
        /// <param name="dtInspectHist">정기점검관리 테이블</param>
        /// <param name="dtRepairH">점검일지등록 헤더 테이블</param>
        /// <param name="dtDurableMatRepairH">점검출고등록 헤더 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveShotInput
            (
                DataTable dtDurableLot
                , DataTable dtInspectHist
                , DataTable dtRepairH
                , DataTable dtDurableMatRepairH
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            TransErrRtn ErrRtn_2 = new TransErrRtn();

            string strErrRtn = "";
            string strErrRtn2 = "";
            string strUsageDocCode = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();



                //---------------- 1. Durable Lot 테이블 (MASDurableLot) -------------------//
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();

                if (dtDurableLot.Rows.Count > 0)
                {
                    strErrRtn = clsDurableLot.mfSaveMASDurableLot(dtDurableLot, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. Durable 정기점검 테이블 (DMMInspectHist) -------------------//
                QRPDMM.BL.DMMMGM.InspectHist clsInspectHist = new InspectHist();

                if (dtInspectHist.Rows.Count > 0)
                {
                    strErrRtn = clsInspectHist.mfSaveInspectHist(dtInspectHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    strUsageDocCode = ErrRtn.mfGetReturnValue(0);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                for (int i = 0; i < dtRepairH.Rows.Count; i++)
                {
                    dtRepairH.Rows[i]["UsageDocCode"] = strUsageDocCode;
                }

                //---------------- 3. 점검일지 등록 헤더테이블 (DMMRepairH) -------------------//
                // 정기점검관리 화면에서 Shot판정을 "정비"로 선택 했을시에만 저장함 //
                if (dtInspectHist.Rows[0]["UsageJudgeCode"].ToString() == "R")
                {
                    QRPDMM.BL.DMMMGM.RepairH clsRepairH = new RepairH();

                    if (dtRepairH.Rows.Count > 0)
                    {
                        strErrRtn = clsRepairH.mfSaveRepairH(dtRepairH, strUserIP, strUserID, sql, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        string strRepairCode = ErrRtn.mfGetReturnValue(0);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        for (int i = 0; i < dtDurableMatRepairH.Rows.Count; i++)
                        {
                            dtDurableMatRepairH.Rows[i]["RepairReqCode"] = strRepairCode;
                        }


                        //---------------- 4. 점검출고 등록 헤더테이블 (EQUDurableMatRepairGIH) -------------------//
                        if (dtRepairH.Rows[0]["ChangeFlag"].ToString() == "T")
                        {
                            QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();

                            if (dtDurableMatRepairH.Rows.Count > 0)
                            {
                                strErrRtn = clsDurableMatRepairH.mfSaveDurableMatRepairH_2(dtDurableMatRepairH, strUserIP, strUserID, sql, trans);
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    return strErrRtn;
                                }
                            }

                        }

                        
                    }
                }
                

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();

                    ////if (dtRepairH.Rows[0]["ChangeFlag"].ToString() == "T")
                    ////    return strErrRtn2;  //교체여부가 T이면 strErrRtn2을 반환
                    ////else
                    ////    return strErrRtn;   //교체여부가 F이면 strErrRtn을 반환


                    return strErrRtn;

                }

                trans.Commit();
                ////if (dtRepairH.Rows[0]["ChangeFlag"].ToString() == "T")
                ////    return strErrRtn2;  //교체여부가 T이면 strErrRtn2을 반환
                ////else
                ////    return strErrRtn;   //교체여부가 F이면 strErrRtn을 반환


                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InspectHist")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class InspectHist : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("UsageDocCode", typeof(String));
                dtTable.Columns.Add("DurableMatCode", typeof(String));
                dtTable.Columns.Add("LotNo", typeof(String));
                dtTable.Columns.Add("EquipCode", typeof(String));
                dtTable.Columns.Add("UsageJudgeCode", typeof(String));
                dtTable.Columns.Add("UsageJudgeDate", typeof(String));
                dtTable.Columns.Add("JudgeChargeID", typeof(String));
                dtTable.Columns.Add("CurUsage", typeof(String));
                dtTable.Columns.Add("CumUsage", typeof(String));
                dtTable.Columns.Add("EtcDesc", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo_Spec()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("UsageDocCode", typeof(String));
                dtTable.Columns.Add("DurableMatCode", typeof(String));
                dtTable.Columns.Add("LotNo", typeof(String));
                dtTable.Columns.Add("SpecCode", typeof(String));

                dtTable.Columns.Add("EMC", typeof(String));
                dtTable.Columns.Add("Package", typeof(String));

                dtTable.Columns.Add("EquipCode", typeof(String));
                dtTable.Columns.Add("UsageJudgeCode", typeof(String));
                dtTable.Columns.Add("UsageJudgeDate", typeof(String));
                dtTable.Columns.Add("JudgeChargeID", typeof(String));
                dtTable.Columns.Add("CurUsage", typeof(String));
                dtTable.Columns.Add("CumUsage", typeof(String));
                dtTable.Columns.Add("EtcDesc", typeof(String));
                dtTable.Columns.Add("FilePath", typeof(String));
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// Durable 현재재고 테이블 저장
        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveInspectHist(DataTable dtInspectHist, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtInspectHist.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageDocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageJudgeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageJudgeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageJudgeDate", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageJudgeDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strJudgeChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["JudgeChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_numCurUsage", ParameterDirection.Input, SqlDbType.Decimal, dtInspectHist.Rows[i]["CurUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numCumUsage", ParameterDirection.Input, SqlDbType.Decimal, dtInspectHist.Rows[i]["CumUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectHist.Rows[i]["EtcDesc"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strUsageDocCode", ParameterDirection.Output, SqlDbType.VarChar, 20);

                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMInspectHist", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 수명관리.

        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveInspectHist_Spec(DataTable dtInspectHist, 
                                            DataTable dtRepairH, 
                                            DataTable dtDurableMatH,
                                            DataTable dtDurableMatD,
                                            string strUserIP, 
                                            string strUserID)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                string strUsageDocCode = string.Empty;

                for (int i = 0; i < dtInspectHist.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageDocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["SpecCode"].ToString(), 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["EMC"].ToString(), 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageJudgeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageJudgeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageJudgeDate", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageJudgeDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strJudgeChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["JudgeChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_numCurUsage", ParameterDirection.Input, SqlDbType.Decimal, dtInspectHist.Rows[i]["CurUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numCumUsage", ParameterDirection.Input, SqlDbType.Decimal, dtInspectHist.Rows[i]["CumUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectHist.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectHist.Rows[i]["FilePath"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strUsageDocCode", ParameterDirection.Output, SqlDbType.VarChar, 20);

                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMInspectHist_Spec", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        strUsageDocCode = ErrRtn.mfGetReturnValue(0);

                        //자재 출고 정보 저장
                        if (dtRepairH.Rows.Count > 0)
                        {
                            RepairH clsRepairH = new RepairH();
                            strErrRtn = clsRepairH.mfSaveRepairH_Spec(dtRepairH, strUsageDocCode, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //치공구교체정보저장
                        if (dtDurableMatH.Rows.Count > 0)
                        {
                            QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurable = new QRPEQU.BL.EQUMGM.DurableMatRepairH();

                            strErrRtn = clsDurable.mfSaveDurableMatRepairH_Spec(dtDurableMatH, dtDurableMatD, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                }

                if (ErrRtn.ErrNum == 0)
                {
                    trans.Commit();
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strUsageDocCode);
                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                }

                //결과 값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수명관리.

        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveInspectHist_Spec(DataTable dtInspectHist,
                                            DataTable dtRepairH,
                                            DataTable dtDurableMatH,
                                            DataTable dtDurableMatD,
                                            DataTable dtFileInfo,
                                            string strUserIP,
                                            string strUserID)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                string strUsageDocCode = string.Empty;

                for (int i = 0; i < dtInspectHist.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageDocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["SpecCode"].ToString(), 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["EMC"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["Package"].ToString(), 30);
                    
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageJudgeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageJudgeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageJudgeDate", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["UsageJudgeDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strJudgeChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtInspectHist.Rows[i]["JudgeChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_numCurUsage", ParameterDirection.Input, SqlDbType.Decimal, dtInspectHist.Rows[i]["CurUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numCumUsage", ParameterDirection.Input, SqlDbType.Decimal, dtInspectHist.Rows[i]["CumUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectHist.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectHist.Rows[i]["FilePath"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@o_strUsageDocCode", ParameterDirection.Output, SqlDbType.VarChar, 20);

                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMInspectHist_Spec", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        strUsageDocCode = ErrRtn.mfGetReturnValue(0);

                        //자재 출고 정보 저장
                        if (dtRepairH.Rows.Count > 0)
                        {
                            RepairH clsRepairH = new RepairH();
                            strErrRtn = clsRepairH.mfSaveRepairH_Spec(dtRepairH, strUsageDocCode, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //치공구교체정보저장
                        if (dtDurableMatH.Rows.Count > 0)
                        {
                            QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurable = new QRPEQU.BL.EQUMGM.DurableMatRepairH();

                            strErrRtn = clsDurable.mfSaveDurableMatRepairH_Spec(dtDurableMatH, dtDurableMatD, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // 수명관리 파일정보
                        if (dtFileInfo.Rows.Count > 0)
                        {
                            strErrRtn = mfSaveInspectFile(dtFileInfo, strUsageDocCode, strUserIP, strUserID, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                }

                if (ErrRtn.ErrNum == 0)
                {
                    trans.Commit();
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strUsageDocCode);
                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                }

                //결과 값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 정기점검관리 대상 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatInfo(String strPlantCode, String strDurableMatCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMat_DurableBOM", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 정기점검관리 상세정보 조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">치공구창고</param>
        ///  <param name="strDurableMatCode">치공구</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadInspectHist_Detail(String strPlantCode, String strDurableInventoryCode, String strDurableMatCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMInspectHist_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 정기점검관리 정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strSpec">Spec</param>
        /// <returns>정기점검관리조회</returns>
        [AutoComplete]
        public DataTable mfReadInspectHist(string strPlantCode, string strDurableMatCode, string strLotNo, string strSpec)
        {
            SQLS sql = new SQLS();
            DataTable dtHist = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableMatCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar,strLotNo,40);
                sql.mfAddParamDataRow(dtParam,"@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar,strSpec,30);

                dtHist = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMInspectHist", dtParam);

                return dtHist;

            }
            catch (Exception ex)
            {
                return dtHist;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtHist.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 수명관리 이력조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>정비이력정보</returns>
        [AutoComplete]
        public DataTable mfReadInspectHist_RepairList(string strPlantCode,string strFromDate,string strToDate,string strDurableMatCode,string strLotNo,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRepair = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRepair = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMInspectHist_RepairList", dtParam);


                //정비이력정보
                return dtRepair;
            }
            catch (Exception ex)
            {
                return dtRepair;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRepair.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 수명관리 이력조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>정비이력정보</returns>
        [AutoComplete]
        public DataTable mfReadInspectHist_List(string strPlantCode, string strFromDate, string strToDate, string strDuarableType, string strLotNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRepair = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatType", ParameterDirection.Input, SqlDbType.VarChar, strDuarableType, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRepair = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMInspectHist_List", dtParam);


                //정비이력정보
                return dtRepair;
            }
            catch (Exception ex)
            {
                return dtRepair;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRepair.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 수명관리 LotNo가 장착된 설비 조회(MES)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        [AutoComplete]
        public DataTable mfReadInspect_Equip(string strPlantCode, string strLotNo)
        {
            SQLS sql = new SQLS();
            DataTable dtRepair = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                

                dtRepair = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMInspect_MESDB", dtParam);

                return dtRepair;
            }
            catch (Exception ex)
            {
                return dtRepair;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRepair.Dispose();
                sql.Dispose();
            }
        }



        #region InspectHistFile


        public DataTable mfDataSetInfo()
        {

            DataTable dtRtn = new DataTable();

            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("UsageDocCode", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(string));
                dtRtn.Columns.Add("FileName", typeof(string));
                dtRtn.Columns.Add("FilePath", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }

        }


        /// <summary>
        /// 수명관리파일저장
        /// </summary>
        /// <param name="dtFileInfo">파일정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveInspectFile(DataTable dtFileInfo, 
                                        string strUsageDocCode, 
                                        string strUserIP, string strUserID, 
                                        SqlConnection Sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                string strErrRtn = string.Empty;


                for (int i = 0; i < dtFileInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUsageDocCode", ParameterDirection.Input, SqlDbType.VarChar, strUsageDocCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtFileInfo.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtFileInfo.Rows[i]["FileName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFileInfo.Rows[i]["FilePath"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_DMMInspectHistFile", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        break;

                }


                return strErrRtn;
            }
            catch (Exception ex)
            {

                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 수명관리파일삭제
        /// </summary>
        /// <param name="dtFileInfo">파일정보</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteInspectFile(DataTable dtDelFileInfo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                string strErrRtn = string.Empty;
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDelFileInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelFileInfo.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUsageDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelFileInfo.Rows[i]["UsageDocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDelFileInfo.Rows[i]["Seq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_DMMInspectHistFile", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }


                if (ErrRtn.ErrNum == 0)
                    trans.Commit();


                return strErrRtn;
            }
            catch (Exception ex)
            {

                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        #endregion
    }



    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class RepairH : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("RepairCode", typeof(String));
                dtTable.Columns.Add("DurableMatCode", typeof(String));
                dtTable.Columns.Add("LotNo", typeof(String));
                dtTable.Columns.Add("EquipCode", typeof(String));
                dtTable.Columns.Add("RepairGubunCode", typeof(String));
                dtTable.Columns.Add("UsageDocCode", typeof(String));
                dtTable.Columns.Add("StandbyFlag", typeof(String));

                dtTable.Columns.Add("StandbyDate", typeof(String));
                dtTable.Columns.Add("RepairFlag", typeof(String));
                dtTable.Columns.Add("RepairDate", typeof(String));
                dtTable.Columns.Add("FinishFlag", typeof(String));
                dtTable.Columns.Add("FinishDate", typeof(String));
                dtTable.Columns.Add("RepairResultCode", typeof(String));
                dtTable.Columns.Add("ChangeFlag", typeof(String));
                dtTable.Columns.Add("EtcDesc", typeof(String));
                dtTable.Columns.Add("UsageClearFlag", typeof(String));
                dtTable.Columns.Add("UsageClearDate", typeof(String));
                dtTable.Columns.Add("ClearCurUsage", typeof(String));
                dtTable.Columns.Add("ClearCumUsage", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo_Spec()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("RepairCode", typeof(String));
                dtTable.Columns.Add("DurableMatCode", typeof(String));
                dtTable.Columns.Add("LotNo", typeof(String));
                dtTable.Columns.Add("SpecCode", typeof(String));

                dtTable.Columns.Add("EMC", typeof(String));
                dtTable.Columns.Add("Package", typeof(String));

                dtTable.Columns.Add("EquipCode", typeof(String));
                dtTable.Columns.Add("RepairGubunCode", typeof(String));
                dtTable.Columns.Add("UsageDocCode", typeof(String));
                dtTable.Columns.Add("StandbyFlag", typeof(String));

                dtTable.Columns.Add("StandbyDate", typeof(String));
                dtTable.Columns.Add("RepairFlag", typeof(String));
                dtTable.Columns.Add("RepairDate", typeof(String));
                dtTable.Columns.Add("FinishFlag", typeof(String));
                dtTable.Columns.Add("FinishDate", typeof(String));
                dtTable.Columns.Add("RepairResultCode", typeof(String));
                dtTable.Columns.Add("ChangeFlag", typeof(String));
                dtTable.Columns.Add("EtcDesc", typeof(String));
                dtTable.Columns.Add("UsageClearFlag", typeof(String));
                dtTable.Columns.Add("UsageClearDate", typeof(String));
                dtTable.Columns.Add("ClearCurUsage", typeof(String));
                dtTable.Columns.Add("ClearCumUsage", typeof(String));
                dtTable.Columns.Add("FilePath", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// Durable 현재재고 테이블 저장

        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveRepairH(DataTable dtRepairH, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtRepairH.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairGubunCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["UsageDocCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStandbyFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["StandbyFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStandbyDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["StandbyDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strFinishFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["FinishFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strFinishDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["FinishDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairResultCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairResultCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChangeFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["ChangeFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairH.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageClearFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["UsageClearFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageClearDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["UsageClearDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_numClearCurUsage", ParameterDirection.Input, SqlDbType.Decimal, dtRepairH.Rows[i]["ClearCurUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numClearCumUsage", ParameterDirection.Input, SqlDbType.Decimal, dtRepairH.Rows[i]["ClearCumUsage"].ToString());

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@o_strRepairCode", ParameterDirection.Output, SqlDbType.VarChar, 20);

                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMRepairH", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveRepairH_Spec(DataTable dtRepairH, string strUsageDocCode,string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            SQLS sql = new SQLS();
            try
            {
                for (int i = 0; i < dtRepairH.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["SpecCode"].ToString(), 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["EMC"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["Package"].ToString(), 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairGubunCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageDocCode", ParameterDirection.Input, SqlDbType.VarChar, strUsageDocCode, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStandbyFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["StandbyFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStandbyDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["StandbyDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strFinishFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["FinishFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strFinishDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["FinishDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairResultCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["RepairResultCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChangeFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["ChangeFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairH.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageClearFlag", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["UsageClearFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageClearDate", ParameterDirection.Input, SqlDbType.VarChar, dtRepairH.Rows[i]["UsageClearDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_numClearCurUsage", ParameterDirection.Input, SqlDbType.Decimal, dtRepairH.Rows[i]["ClearCurUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numClearCumUsage", ParameterDirection.Input, SqlDbType.Decimal, dtRepairH.Rows[i]["ClearCumUsage"].ToString());

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@o_strRepairCode", ParameterDirection.Output, SqlDbType.VarChar, 20);

                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_DMMRepairH_Spec", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 점검 일지등록 헤더정보 리스트 조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromUsageJudgeDate">Shot판정일 from</param>
        /// <param name="strToUsageJudgeDate">Shot 판정일 to</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strEquipName">설비명</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strDurableMatName">치공구명</param>
        /// <param name="strUserID">정비사코드</param>
        /// <param name="strUserName">정비사명</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>테이블</returns>
        [AutoComplete]
        public DataTable mfReadRepairH
            (String strPlantCode
            , String strFromUsageJudgeDate
            , String strToUsageJudgeDate
            , String strEquipCode
            , String strEquipName
            , String strDurableMatCode
            , String strDurableMatName
            , String strUserID
            , String strUserName
            , String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromUsageJudgeDate", ParameterDirection.Input, SqlDbType.VarChar, strFromUsageJudgeDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToUsageJudgeDate", ParameterDirection.Input, SqlDbType.VarChar, strToUsageJudgeDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipName", ParameterDirection.Input, SqlDbType.NVarChar, strEquipName, 100);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableMatName, 100);
                sql.mfAddParamDataRow(dtParmter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strUserName", ParameterDirection.Input, SqlDbType.NVarChar, strUserName, 100);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMRepairH", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 점검 일지등록 헤더정보 상세정보 조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strRepairCode">정비코드</param>

        /// <param name="strLang">사용언어</param>
        /// <returns>테이블</returns>
        [AutoComplete]
        public DataTable mfReadRepairH_Detail
            (String strPlantCode
            , String strRepairCode
            , String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMRepairH_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 점검출고등록정보 리스트 조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromFinishDate">점검완료일 from</param>
        /// <param name="strToFinishDate">점검완료일 to</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strEquipName">설비명</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strDurableMatName">치공구명</param>
        /// <param name="strUserID">정비사코드</param>
        /// <param name="strUserName">정비사명</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>테이블</returns>
        [AutoComplete]
        public DataTable mfReadRepairH_EQUDurableMatRepairGIH
            (String strPlantCode
            , String strFromFinishDate
            , String strToFinishDate
            , String strEquipCode
            , String strEquipName
            , String strDurableMatCode
            , String strDurableMatName
            , String strUserID
            , String strUserName
            , String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strFromFinishDate", ParameterDirection.Input, SqlDbType.VarChar, strFromFinishDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strToFinishDate", ParameterDirection.Input, SqlDbType.VarChar, strToFinishDate, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipName", ParameterDirection.Input, SqlDbType.NVarChar, strEquipName, 100);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableMatName, 100);
                sql.mfAddParamDataRow(dtParmter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strUserName", ParameterDirection.Input, SqlDbType.NVarChar, strUserName, 100);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMRepairH_EQUDurableMatRepairGIH", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

    }








    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("RepairD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class RepairD : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("RepairCode", typeof(String));
                dtTable.Columns.Add("Seq", typeof(String));
                dtTable.Columns.Add("RepairPlace", typeof(String));
                dtTable.Columns.Add("RepairPart", typeof(String));
                dtTable.Columns.Add("RepairName", typeof(String));
                dtTable.Columns.Add("DurablePMGubunCode", typeof(String));

                dtTable.Columns.Add("DurablePMTypeCode", typeof(String));
                dtTable.Columns.Add("RepairChargeID", typeof(String));
                dtTable.Columns.Add("RepairTime", typeof(String));
                dtTable.Columns.Add("EtcDesc", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 치공구 점검일지등록 아이템 저장

        /// </summary>
        /// <param name="dtRepairD">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveRepairD(DataTable dtRepairD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtRepairD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["RepairCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParamter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtRepairD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairPlace", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairD.Rows[i]["RepairPlace"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairPart", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairD.Rows[i]["RepairPart"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairName", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairD.Rows[i]["RepairName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurablePMGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["DurablePMGubunCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurablePMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["DurablePMTypeCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["RepairChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_numRepairTime", ParameterDirection.Input, SqlDbType.Decimal, dtRepairD.Rows[i]["RepairTime"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtRepairD.Rows[i]["EtcDesc"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMRepairD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 치공구 점검일지등록 테이블 삭제(아이템)
        /// </summary>
        /// <param name="dtDurableStockTakeD">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteRepairD(DataTable dtRepairD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtRepairD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["RepairCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_DMMRepairD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 점검 일지등록 아이템정보 리스트 조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">치공구창고</param>
        ///  <param name="strDurableMatCode">치공구</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadRepairD
            (String strPlantCode
            , String strRepairCode
            , String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairCode, 20);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMRepairD", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DMMRepairUseSP")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DMMRepairUseSP : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("RepairCode", typeof(String));
                dtTable.Columns.Add("Seq", typeof(String));
                dtTable.Columns.Add("CurSparePartCode", typeof(String));
                dtTable.Columns.Add("CurInputQty", typeof(String));
                dtTable.Columns.Add("ChgSPInventoryCode", typeof(String));
                dtTable.Columns.Add("ChgSparePartCode", typeof(String));

                dtTable.Columns.Add("ChgInputQty", typeof(String));
                dtTable.Columns.Add("ChgDesc", typeof(String));
                dtTable.Columns.Add("CancelFlag", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// 치공구 점검일지등록 아이템 저장

        /// </summary>
        /// <param name="dtRepairD">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDMMRepairUseSP(DataTable dtDMMRepairUseSP, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDMMRepairUseSP.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDMMRepairUseSP.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, dtDMMRepairUseSP.Rows[i]["RepairCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDMMRepairUseSP.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strCurSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtDMMRepairUseSP.Rows[i]["CurSparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intCurInputQty", ParameterDirection.Input, SqlDbType.Int, dtDMMRepairUseSP.Rows[i]["CurInputQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDMMRepairUseSP.Rows[i]["ChgSPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtDMMRepairUseSP.Rows[i]["ChgSparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intChgInputQty", ParameterDirection.Input, SqlDbType.Int, dtDMMRepairUseSP.Rows[i]["ChgInputQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strChgDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDMMRepairUseSP.Rows[i]["ChgDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDMMRepairUseSP.Rows[i]["CancelFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_DMMRepairUseSP", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 치공구 점검일지등록 테이블 삭제(아이템)
        /// </summary>
        /// <param name="dtDurableStockTakeD">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteDMMRepairUseSP(DataTable dtRepairD, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtRepairD.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, dtRepairD.Rows[i]["RepairCode"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_DMMRepairUseSP", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 점검 일지등록 아이템정보 리스트 조회

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableInventoryCode">치공구창고</param>
        ///  <param name="strDurableMatCode">치공구</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadDMMRepairUseSP
            (String strPlantCode
            , String strRepairCode
            , String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strRepairCode", ParameterDirection.Input, SqlDbType.VarChar, strRepairCode, 20);

                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DMMRepairUseSP", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }


    }
}
