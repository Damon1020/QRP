﻿/*----------------------------------------------------------------------*/
// 시스템명      : 품질관리                                            
// 모듈(분류)명  : 품질검사 관리                                      
// 프로그램ID    : clsINSPRC.cs                                          
// 프로그램명    : 품질검사관리                                      
// 작성자        : 서정현                                                
// 작성일자      : 2011-09-21                                            
// 수정이력      : 수정일자 : 수정한 내용 수정 (수정자 성명)                     
//                 추가일자 : 추가한 내용 추가 (추가작성자 성명)                      
/*----------------------------------------------------------------------*/
using System;
using System.Linq;
using System.Text;

using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPINS.BL.INSPRC
{
    public class MESCodeReturn
    {
        private string m_strMesCode;

        public string MesCode
        {
            get { return m_strMesCode; }
            set { m_strMesCode = value; }
        }

        public MESCodeReturn()
        {
            MesCode = "S07";
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcQCN")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcQCN : ServicedComponent  //실행용
    //public class ProcQCN   //디버깅용
    {
        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자

        /// </summary>
        public ProcQCN()
        {
        }
        /// <summary>
        /// 디버깅용 생성자

        /// </summary>
        /// <param name="strDBConn"></param>
        public ProcQCN(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        #region QCN발행
        // QCN 테이블의 앞부분 주요부분 필드와 똑같이 선언
        public DataTable mfSetQCNDataInfo()
        {
            DataTable dtInsProcQCN = new DataTable();
            try
            {
                dtInsProcQCN.Columns.Add("PlantCode", typeof(String)); // 공장코드 PK
                dtInsProcQCN.Columns.Add("QCNNo", typeof(String));  // QCN번호 PK
                dtInsProcQCN.Columns.Add("P_QCNITR", typeof(String));  // 구분 (QCN,ITR)
                dtInsProcQCN.Columns.Add("ProductCode", typeof(String)); // 제품코드  
                dtInsProcQCN.Columns.Add("LotNo", typeof(String)); // Lot No
                dtInsProcQCN.Columns.Add("AriseProcessCode", typeof(String)); // 발생공정코드
                dtInsProcQCN.Columns.Add("AriseEquipCode", typeof(String)); // 발생설비코드
                dtInsProcQCN.Columns.Add("AriseDate", typeof(String));      // 발생일자
                dtInsProcQCN.Columns.Add("AriseTime", typeof(String));      // 발생일시
                dtInsProcQCN.Columns.Add("ExpectProcessCode", typeof(String)); // 예상공정코드
                dtInsProcQCN.Columns.Add("ExpectEquipCode", typeof(String)); // 예상설비코드
                dtInsProcQCN.Columns.Add("MultiEquipCode", typeof(String));  // 멀티설비코드
                dtInsProcQCN.Columns.Add("MESWorkUserID", typeof(string));      // MES작업자ID
                dtInsProcQCN.Columns.Add("Qty", typeof(decimal));  // 수량
                dtInsProcQCN.Columns.Add("InspectQty", typeof(decimal)); // 검사수량
                dtInsProcQCN.Columns.Add("FaultQty", typeof(decimal)); // 불량수량
                dtInsProcQCN.Columns.Add("InspectFaultTypeCode", typeof(String));
                dtInsProcQCN.Columns.Add("FaultFilePath", typeof(string));      // 불량 Image 파일
                dtInsProcQCN.Columns.Add("WorkStepMaterialFlag", typeof(String));
                dtInsProcQCN.Columns.Add("WorkStepEquipFlag", typeof(String));
                dtInsProcQCN.Columns.Add("ActionFlag1", typeof(String)); // 작업방침여부
                dtInsProcQCN.Columns.Add("ActionFlag2", typeof(String)); // 부적합처리여부
                dtInsProcQCN.Columns.Add("ActionFlag3", typeof(String)); // 원부자재여부
                dtInsProcQCN.Columns.Add("ActionFlag4", typeof(String)); // 교육실시여부
                dtInsProcQCN.Columns.Add("ActionFlag5", typeof(String)); // 장비상태여부
                dtInsProcQCN.Columns.Add("ActionFlag6", typeof(String)); // 표준발행여부
                dtInsProcQCN.Columns.Add("ActionFlag7", typeof(String)); // 기술부서여부
                dtInsProcQCN.Columns.Add("ActionFlag8", typeof(String)); // 작업중지여부
                dtInsProcQCN.Columns.Add("ActionFlag9", typeof(String)); // 전수선별여부
                dtInsProcQCN.Columns.Add("WorkUserID", typeof(String));  // 작업자 ID
                dtInsProcQCN.Columns.Add("InspectUserID", typeof(String)); // 검사자 ID
                dtInsProcQCN.Columns.Add("EtcDesc", typeof(String)); // 비고 
                dtInsProcQCN.Columns.Add("PublicshFlag", typeof(string));       //발행완료
                dtInsProcQCN.Columns.Add("MESHoldTFlag", typeof(string));       //MESHoldTFlag
                dtInsProcQCN.Columns.Add("PublicshComplete", typeof(string));       //저장완료 AND 발행완료상태
                dtInsProcQCN.Columns.Add("HoldCode", typeof(string));       // HoldCode
                dtInsProcQCN.Columns.Add("QCConfirmComplete", typeof(string));  //ConfirmCompleteFlag

                dtInsProcQCN.Columns.Add("ReqNo", typeof(string));
                dtInsProcQCN.Columns.Add("ReqSeq", typeof(string));
                dtInsProcQCN.Columns.Add("LotProcessState", typeof(string));
                dtInsProcQCN.Columns.Add("LotHoldState", typeof(string));

                dtInsProcQCN.Columns.Add("ReTestFaultQty1", typeof(Int32));
                dtInsProcQCN.Columns.Add("ReTestFaultQty2", typeof(Int32));
                dtInsProcQCN.Columns.Add("ReTestFaultQty3", typeof(Int32));
                dtInsProcQCN.Columns.Add("ReTestInspectQty1", typeof(Int32));
                dtInsProcQCN.Columns.Add("ReTestInspectQty2", typeof(Int32));
                dtInsProcQCN.Columns.Add("ReTestInspectQty3", typeof(Int32));
                dtInsProcQCN.Columns.Add("TestSYSTEM", typeof(string));
                dtInsProcQCN.Columns.Add("TestHANDLER", typeof(string));
                dtInsProcQCN.Columns.Add("TestPROGRAM", typeof(string));

                return dtInsProcQCN;
            }
            catch (Exception ex)
            {
                return dtInsProcQCN;
                throw (ex);
            }
            finally
            {
                dtInsProcQCN.Dispose();
            }
        }


        /// <summary>
        /// QCN 발행 검색조건에 따른 검색메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strAriseDateFrom">발행일From</param>
        /// <param name="strAriseDateTo">발행일To</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInsProcQCN(string strPlantCode, string strProcessCode, string strQCNNo, string strLotNo, string strProductCode
                                        , string strAriseDateFrom, string strAriseDateTo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS(); //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString()); // 디버깅 모드
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateTo", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCN", dtParam);    //실행용
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSProcQCN", dtParam);  //디버깅용

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 발행 헤더정보 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">언어설정</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNDetail(String strPlantCode, String strQCNNo, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString()); // 디버깅 모드
            try
            {
                //디비호출
                sql.mfConnect();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 datatable에 넣기
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 호출
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNDetail", dtParameter); //실행용
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSProcQCNDetail", dtParameter); //디버깅용
                //리턴정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 발행 저장용 Method
        /// </summary>
        /// <param name="dtInsProcQCN">저장할데이터가 들어있는 테이블 </param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>저장</returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN(DataTable dtQCN, string strUserID, string strUserIP, DataTable dtAffectLot, DataTable dtEquipList, DataTable dtQCNUser, string strFormName)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString()); // 디버깅 모드
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();    //실행용
                SqlTransaction trans;

                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();//실행용
                //trans = m_SqlConnDebug.BeginTransaction();//디버깅용

                for (int i = 0; i < dtQCN.Rows.Count; i++)
                {
                    string strQCNNo = "";
                    string strPlantCode = dtQCN.Rows[i]["PlantCode"].ToString();

                    //발행완료가 되었던 정보가 아닐 경우
                    if (!dtQCN.Rows[i]["PublicshComplete"].ToString().Equals("T"))
                    {

                        DataTable dtParam = sql.mfSetParamDataTable();
                        //파라미터 저장
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["QCNNo"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ProductCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["LotNo"].ToString(), 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["AriseProcessCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strAriseEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["AriseEquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["AriseDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["AriseTime"].ToString(), 8);
                        sql.mfAddParamDataRow(dtParam, "@i_strExpectProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ExpectProcessCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strExpectEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ExpectEquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strMultiEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["MultiEquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strMESWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["MESWorkUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_dblQty", ParameterDirection.Input, SqlDbType.Decimal, dtQCN.Rows[i]["Qty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtQCN.Rows[i]["InspectQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtQCN.Rows[i]["FaultQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strFaultFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["FaultFilePath"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strWorkStepMaterialFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["WorkStepMaterialFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strWorkStepEquipFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["WorkStepEquipFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag1", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag1"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag2", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag2"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag3", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag3"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag4", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag4"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag5", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag5"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag6", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag6"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag7", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag7"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag8", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag8"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag9", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag9"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["WorkUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["InspectUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["EtcDesc"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strPublishFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["PublicshFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmFlag", ParameterDirection.Input, SqlDbType.Char, dtQCN.Rows[i]["QCConfirmComplete"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ReqNo"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ReqSeq"].ToString(), 4);
                        sql.mfAddParamDataRow(dtParam, "@i_strLotProcessState", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["LotProcessState"].ToString(), 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strLotHoldState", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["LotHoldState"].ToString(), 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //프로시저호출
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_Publication", dtParam);  //실행용
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_INSProcQCN_Publication", dtParam);       // 디버깅 모드

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        else
                        {
                            // OUTPUT값 저장
                            strQCNNo = ErrRtn.mfGetReturnValue(0);

                            // Affect Lot 정보 저장
                            if (dtAffectLot.Rows.Count > 0)
                            {
                                // Affect Lot 정보 삭제                            
                                ProcQCNAffectLot clsAffectLot = new ProcQCNAffectLot();
                                strErrRtn = clsAffectLot.mfDeleteINSProcQCNAffectLot(strPlantCode, strQCNNo, sql.SqlCon, trans);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }

                                strErrRtn = clsAffectLot.mfSaveINSProcQCNAffectLot(dtAffectLot, strUserID, strUserIP, sql.SqlCon, trans, strQCNNo);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            // 예상설비
                            if (dtEquipList.Rows.Count > 0)
                            {
                                // DB의 ExpectEquip 정보 삭제
                                ProcQCNExpectEquip clsExpectEquip = new ProcQCNExpectEquip();
                                strErrRtn = clsExpectEquip.mfDeleteINSProcQCNExpectEquip(strPlantCode, strQCNNo, sql.SqlCon, trans);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }

                                // 저장
                                strErrRtn = clsExpectEquip.mfSaveINSProcQCNExpectEquip(dtEquipList, strUserID, strUserIP, sql.SqlCon, trans, strQCNNo);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            // 수신자
                            if (dtQCNUser.Rows.Count > 0)
                            {
                                // DB의 수신자 정보 삭제
                                ProcQCNUser clsQCNUser = new ProcQCNUser();
                                strErrRtn = clsQCNUser.mfDeleteINSProcQCNUser(strPlantCode, strQCNNo, sql.SqlCon, trans);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }

                                // 저장메소드 호출
                                strErrRtn = clsQCNUser.mfSaveINSProcQCNUser(dtQCNUser, strUserID, strUserIP, sql.SqlCon, trans, strQCNNo);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                            if (ErrRtn.ErrNum.Equals(0))
                                trans.Commit();
                        }
                    }
                    else
                    {
                        // 발행완료 된경우 관리번호와 ErrNum 0으로 설정하여 Return
                        ErrRtn.ErrNum = 0;
                        strQCNNo = dtQCN.Rows[i]["QCNNo"].ToString();
                        ErrRtn.mfAddReturnValue(strQCNNo);
                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    }

                    #region MES I/F 부분 분리(기존소스 주석처리)
                    //// 모든처리가 성공일시 MES 처리를 한다.
                    //if (ErrRtn.ErrNum == 0)
                    //{
                    //    //발행완료가 체크이고, QCN HoldMESTFlag 가 F 인 것만 MES로보낸다.
                    //    if ((dtQCN.Rows[i]["PublicshFlag"].ToString().Equals("True") || dtQCN.Rows[i]["PublicshFlag"].ToString().Equals("T")) && dtQCN.Rows[0]["MESHoldTFlag"].ToString().Equals("F"))
                    //    {
                    //        //MES서버 경로 가져오기
                    //        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    //        DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtQCN.Rows[i]["PlantCode"].ToString(), "S04");

                    //        //MES 해당LotNo Hold요청 매서드 실행
                    //        QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);  //실행용
                    //        //QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv("1", dtSysAcce);// 디버깅용

                    //        string strComent = dtQCN.Rows[i]["EtcDesc"].ToString();         //코멘트
                    //        string strHoldCode = dtQCN.Rows[i]["HoldCode"].ToString();      //HoldCode 아직 정해진게 없음
                    //        string strPMUserID = dtQCN.Rows[i]["InspectUserID"].ToString(); //조치사항안 검사자

                    //        //컬럼명 변경
                    //        dtAffectLot.Columns["LotNo"].ColumnName = "LOTID";

                    //        // 헤더 Lot이 Hold 상태가 아닐경우 Hold 코드를 보낸다
                    //        if (!dtQCN.Rows[i]["LotHoldState"].ToString().Equals("NotOnHold"))
                    //            //&& !dtQCN.Rows[i]["ReqNo"].ToString().Equals(string.Empty)
                    //            //&& !dtQCN.Rows[i]["ReqSeq"].ToString().Equals(string.Empty))
                    //        {
                    //            //헤더 LotNo를 저장한다.
                    //            DataRow drLot;
                    //            drLot = dtAffectLot.NewRow();
                    //            drLot["LOTID"] = dtQCN.Rows[i]["LotNo"].ToString();
                    //            dtAffectLot.Rows.Add(drLot);
                    //        }

                    //        //해당 LotHold요청 하여 결과를 받아온다
                    //        DataTable dtMES = clsTibrv.LOT_HOLD4QC_REQ(strFormName, strPMUserID, strComent, strHoldCode, dtAffectLot, strUserIP);

                    //        //요청하여 정보가 있는 경우
                    //        if (dtMES.Rows.Count > 0)
                    //        {
                    //            //요청한결과가 성공인 경우 테이블MESHoldTFlag를 업데이트 시킨다.
                    //            if (dtMES.Rows[0][0].ToString() == "0")
                    //            {
                    //                //디비연결
                    //                sql.mfConnect(); //실행용

                    //                //트랜젝션시작
                    //                SqlTransaction transt;
                    //                transt = sql.SqlCon.BeginTransaction(); //실행용
                    //                //transt = m_SqlConnDebug.BeginTransaction();// 디버깅용


                    //                //파라미터 정보저장
                    //                DataTable dtParamt = sql.mfSetParamDataTable();

                    //                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    //                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    //                sql.mfAddParamDataRow(dtParamt, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                    //                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    //                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    //                sql.mfAddParamDataRow(dtParamt, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    //                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //                // QCN MES LotNo HoldTFlag 업데이트 프로시저 실행
                    //                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSProcQCNMESHoldTFlag", dtParamt);    //실해용
                    //                //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, transt, "up_Update_INSProcQCNMESHoldTFlag", dtParamt);    //디버깅용

                    //                // Decoding
                    //                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //                if (ErrRtn.ErrNum != 0)
                    //                    transt.Rollback();
                    //                else
                    //                    transt.Commit();

                    //            }

                    //            //MES에서 넘어온 결과코드와, 메세지를 EnCoding하여 strErrRtn 저장
                    //            ErrRtn.InterfaceResultCode = dtMES.Rows[0][0].ToString();
                    //            ErrRtn.InterfaceResultMessage = dtMES.Rows[0][1].ToString();

                    //            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    //        }
                    //    }
                    //}
                    #endregion
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// QCN 발행 저장용 Method
        /// </summary>
        /// <param name="dtInsProcQCN">저장할데이터가 들어있는 테이블 </param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>저장</returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_S(DataTable dtQCN, string strUserID, string strUserIP, 
                                            DataTable dtAffectLot, DataTable dtEquipList,
                                            DataTable dtQCNUser,DataTable dtFaulty, string strFormName)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString()); // 디버깅 모드
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();    //실행용
                SqlTransaction trans;

                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();//실행용
                //trans = m_SqlConnDebug.BeginTransaction();//디버깅용

                for (int i = 0; i < dtQCN.Rows.Count; i++)
                {
                    string strQCNNo = "";
                    string strPlantCode = dtQCN.Rows[i]["PlantCode"].ToString();

                    //발행완료가 되었던 정보가 아닐 경우
                    if (!dtQCN.Rows[i]["PublicshComplete"].ToString().Equals("T"))
                    {

                        DataTable dtParam = sql.mfSetParamDataTable();
                        //파라미터 저장
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["QCNNo"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strP_QCNITR", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["P_QCNITR"].ToString(), 3);
                        sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ProductCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["LotNo"].ToString(), 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["AriseProcessCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strAriseEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["AriseEquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["AriseDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["AriseTime"].ToString(), 8);
                        sql.mfAddParamDataRow(dtParam, "@i_strExpectProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ExpectProcessCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strExpectEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ExpectEquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strMultiEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["MultiEquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strMESWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["MESWorkUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_dblQty", ParameterDirection.Input, SqlDbType.Decimal, dtQCN.Rows[i]["Qty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtQCN.Rows[i]["InspectQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtQCN.Rows[i]["FaultQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strFaultFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["FaultFilePath"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strWorkStepMaterialFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["WorkStepMaterialFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strWorkStepEquipFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["WorkStepEquipFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag1", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag1"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag2", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag2"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag3", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag3"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag4", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag4"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag5", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag5"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag6", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag6"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag7", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag7"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag8", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag8"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strActionFlag9", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ActionFlag9"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["WorkUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["InspectUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["EtcDesc"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strPublishFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["PublicshFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmFlag", ParameterDirection.Input, SqlDbType.Char, dtQCN.Rows[i]["QCConfirmComplete"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ReqNo"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["ReqSeq"].ToString(), 4);
                        sql.mfAddParamDataRow(dtParam, "@i_strLotProcessState", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["LotProcessState"].ToString(), 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strLotHoldState", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["LotHoldState"].ToString(), 50);
                        sql.mfAddParamDataRow(dtParam, "@i_intReTestFaultQty1", ParameterDirection.Input, SqlDbType.Int, dtQCN.Rows[i]["ReTestFaultQty1"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_intReTestFaultQty2", ParameterDirection.Input, SqlDbType.Int, dtQCN.Rows[i]["ReTestFaultQty2"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_intReTestFaultQty3", ParameterDirection.Input, SqlDbType.Int, dtQCN.Rows[i]["ReTestFaultQty3"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_intReTestInspectQty1", ParameterDirection.Input, SqlDbType.Int, dtQCN.Rows[i]["ReTestInspectQty1"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_intReTestInspectQty2", ParameterDirection.Input, SqlDbType.Int, dtQCN.Rows[i]["ReTestInspectQty2"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_intReTestInspectQty3", ParameterDirection.Input, SqlDbType.Int, dtQCN.Rows[i]["ReTestInspectQty3"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strTestSYSTEM", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["TestSYSTEM"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strTestHANDLER", ParameterDirection.Input, SqlDbType.VarChar, dtQCN.Rows[i]["TestHANDLER"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strTestPROGRAM", ParameterDirection.Input, SqlDbType.NVarChar, dtQCN.Rows[i]["TestPROGRAM"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //프로시저호출
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_Publication_S", dtParam);  //실행용
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_INSProcQCN_Publication", dtParam);       // 디버깅 모드

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        else
                        {
                            // OUTPUT값 저장
                            strQCNNo = ErrRtn.mfGetReturnValue(0);

                            // Affect Lot 정보 저장
                            if (dtAffectLot.Rows.Count > 0)
                            {
                                // Affect Lot 정보 삭제                            
                                ProcQCNAffectLot clsAffectLot = new ProcQCNAffectLot();
                                strErrRtn = clsAffectLot.mfDeleteINSProcQCNAffectLot(strPlantCode, strQCNNo, sql.SqlCon, trans);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }

                                strErrRtn = clsAffectLot.mfSaveINSProcQCNAffectLot(dtAffectLot, strUserID, strUserIP, sql.SqlCon, trans, strQCNNo);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                            
                            //불량유형정보 삭제
                           ProcQCNFaultType clsFaulty = new ProcQCNFaultType();
                           strErrRtn = clsFaulty.mfDeleteProcQCNFaultType(strPlantCode, strQCNNo, sql.SqlCon, trans);
                           
                            //결과검사
                           ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            //처리실패시 RollBack 후 저장 처리 정지
                           if (ErrRtn.ErrNum != 0)
                           {
                               trans.Rollback();
                               break;
                           }
                            //불량유형정보 저장
                            if (dtFaulty.Rows.Count > 0)
                            {
                                strErrRtn = clsFaulty.mfSaveProcQCNFaultType(dtFaulty, strQCNNo, strUserID, strUserIP, sql.SqlCon, trans);
                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            // 예상설비
                            if (dtEquipList.Rows.Count > 0)
                            {
                                // DB의 ExpectEquip 정보 삭제
                                ProcQCNExpectEquip clsExpectEquip = new ProcQCNExpectEquip();
                                strErrRtn = clsExpectEquip.mfDeleteINSProcQCNExpectEquip(strPlantCode, strQCNNo, sql.SqlCon, trans);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }

                                // 저장
                                strErrRtn = clsExpectEquip.mfSaveINSProcQCNExpectEquip(dtEquipList, strUserID, strUserIP, sql.SqlCon, trans, strQCNNo);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            // 수신자
                            if (dtQCNUser.Rows.Count > 0)
                            {
                                // DB의 수신자 정보 삭제
                                ProcQCNUser clsQCNUser = new ProcQCNUser();
                                strErrRtn = clsQCNUser.mfDeleteINSProcQCNUser(strPlantCode, strQCNNo, sql.SqlCon, trans);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }

                                // 저장메소드 호출
                                strErrRtn = clsQCNUser.mfSaveINSProcQCNUser(dtQCNUser, strUserID, strUserIP, sql.SqlCon, trans, strQCNNo);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            //모든 처리가 완료되었을 경우 
                            if (ErrRtn.ErrNum == 0)
                            {
                                //QCN발행번호가 공백인경우 추가하여 UI로 보냄
                                if (ErrRtn.mfGetReturnValue(0) == null || ErrRtn.mfGetReturnValue(0).Equals(string.Empty))
                                {
                                    ErrRtn.mfAddReturnValue(strQCNNo);
                                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                }

                                //Commit
                                trans.Commit();

                            }
                        }
                    }
                    else
                    {
                        // 발행완료 된경우 관리번호와 ErrNum 0으로 설정하여 Return
                        ErrRtn.ErrNum = 0;
                        strQCNNo = dtQCN.Rows[i]["QCNNo"].ToString();
                        ErrRtn.mfAddReturnValue(strQCNNo);
                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    }

                    #region MES I/F 부분 분리(기존소스 주석처리)
                    //// 모든처리가 성공일시 MES 처리를 한다.
                    //if (ErrRtn.ErrNum == 0)
                    //{
                    //    //발행완료가 체크이고, QCN HoldMESTFlag 가 F 인 것만 MES로보낸다.
                    //    if ((dtQCN.Rows[i]["PublicshFlag"].ToString().Equals("True") || dtQCN.Rows[i]["PublicshFlag"].ToString().Equals("T")) && dtQCN.Rows[0]["MESHoldTFlag"].ToString().Equals("F"))
                    //    {
                    //        //MES서버 경로 가져오기
                    //        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    //        DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtQCN.Rows[i]["PlantCode"].ToString(), "S04");

                    //        //MES 해당LotNo Hold요청 매서드 실행
                    //        QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);  //실행용
                    //        //QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv("1", dtSysAcce);// 디버깅용

                    //        string strComent = dtQCN.Rows[i]["EtcDesc"].ToString();         //코멘트
                    //        string strHoldCode = dtQCN.Rows[i]["HoldCode"].ToString();      //HoldCode 아직 정해진게 없음
                    //        string strPMUserID = dtQCN.Rows[i]["InspectUserID"].ToString(); //조치사항안 검사자

                    //        //컬럼명 변경
                    //        dtAffectLot.Columns["LotNo"].ColumnName = "LOTID";

                    //        // 헤더 Lot이 Hold 상태가 아닐경우 Hold 코드를 보낸다
                    //        if (!dtQCN.Rows[i]["LotHoldState"].ToString().Equals("NotOnHold"))
                    //            //&& !dtQCN.Rows[i]["ReqNo"].ToString().Equals(string.Empty)
                    //            //&& !dtQCN.Rows[i]["ReqSeq"].ToString().Equals(string.Empty))
                    //        {
                    //            //헤더 LotNo를 저장한다.
                    //            DataRow drLot;
                    //            drLot = dtAffectLot.NewRow();
                    //            drLot["LOTID"] = dtQCN.Rows[i]["LotNo"].ToString();
                    //            dtAffectLot.Rows.Add(drLot);
                    //        }

                    //        //해당 LotHold요청 하여 결과를 받아온다
                    //        DataTable dtMES = clsTibrv.LOT_HOLD4QC_REQ(strFormName, strPMUserID, strComent, strHoldCode, dtAffectLot, strUserIP);

                    //        //요청하여 정보가 있는 경우
                    //        if (dtMES.Rows.Count > 0)
                    //        {
                    //            //요청한결과가 성공인 경우 테이블MESHoldTFlag를 업데이트 시킨다.
                    //            if (dtMES.Rows[0][0].ToString() == "0")
                    //            {
                    //                //디비연결
                    //                sql.mfConnect(); //실행용

                    //                //트랜젝션시작
                    //                SqlTransaction transt;
                    //                transt = sql.SqlCon.BeginTransaction(); //실행용
                    //                //transt = m_SqlConnDebug.BeginTransaction();// 디버깅용


                    //                //파라미터 정보저장
                    //                DataTable dtParamt = sql.mfSetParamDataTable();

                    //                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    //                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    //                sql.mfAddParamDataRow(dtParamt, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                    //                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    //                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    //                sql.mfAddParamDataRow(dtParamt, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    //                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //                // QCN MES LotNo HoldTFlag 업데이트 프로시저 실행
                    //                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSProcQCNMESHoldTFlag", dtParamt);    //실해용
                    //                //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, transt, "up_Update_INSProcQCNMESHoldTFlag", dtParamt);    //디버깅용

                    //                // Decoding
                    //                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //                if (ErrRtn.ErrNum != 0)
                    //                    transt.Rollback();
                    //                else
                    //                    transt.Commit();

                    //            }

                    //            //MES에서 넘어온 결과코드와, 메세지를 EnCoding하여 strErrRtn 저장
                    //            ErrRtn.InterfaceResultCode = dtMES.Rows[0][0].ToString();
                    //            ErrRtn.InterfaceResultMessage = dtMES.Rows[0][1].ToString();

                    //            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    //        }
                    //    }
                    //}
                    #endregion
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// QCN List 저장 Method
        /// </summary>
        /// <param name="dtQCNList">QCNList 정보 저장된 DataTable</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_List_PSTS(DataTable dtQCNList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = "";

                for (int i = 0; i < dtQCNList.Rows.Count; i++)
                {
                    //QC최종 Confirm체크 가 되었던 정보가 아닌경우
                    if (!dtQCNList.Rows[i]["QCConfirmComplete"].ToString().Equals("T"))
                    {
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["QCNNo"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["EtcDesc"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCauseDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["FirCauseDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCauseUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirCauseUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCauseDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirCauseDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strFIRCauseCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirCauseCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCorrectDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["FirCorrectDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCorrectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirCorrectUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCorrectDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirCorrectDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCorrectCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirCorrectCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirMeasureDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["FirMeasureDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirMeasureUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirMeasureUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirMeasureDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirMeasureDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirMeasureCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirMeasureCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MManFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["Fir4MManFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MEquipFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["Fir4MEquipFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MMethodFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["Fir4MMethodFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MEnviroFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["Fir4MEnviroFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MMaterialFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["Fir4MMaterialFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["FirResultFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecActionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["SecActionDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecActionUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["SecActionUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecActionDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["SecActionDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecActionCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["SecActionCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["SecResultFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_dblThiFCostQty", ParameterDirection.Input, SqlDbType.Decimal, dtQCNList.Rows[i]["ThiFCostQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblThiFCostTime", ParameterDirection.Input, SqlDbType.Decimal, dtQCNList.Rows[i]["ThiFCostTime"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblThiFCostPerson", ParameterDirection.Input, SqlDbType.Decimal, dtQCNList.Rows[i]["ThiFCostPerson"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblThiFCostScrap", ParameterDirection.Input, SqlDbType.Decimal, dtQCNList.Rows[i]["ThiFCostScrap"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strThiFCostCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["ThiFCostCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strThiResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["ThiResultFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strImputationDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["ImputationDeptCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["QCConfirmUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["QCConfirmDate"].ToString(), 30);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["QCConfirmFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmResultFlag", ParameterDirection.Input, SqlDbType.Char, dtQCNList.Rows[i]["QCConfirmResultFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strTestSYSTEM", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["TestSYSTEM"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strTestHANDLER", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[i]["TestHANDLER"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strTestPROGRAM", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["TestPROGRAM"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirReturnDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["FirReturnDesc"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecReturnDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["SecReturnDesc"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strThiReturnDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["ThiReturnDesc"].ToString(), 1000);

                        sql.mfAddParamDataRow(dtParam, "@i_strFile", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[i]["File"].ToString(), 100);

                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_List_PSTS", dtParam);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        else
                        {
                            trans.Commit();
                        }
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// QCN MESHold 전송 메소드
        /// </summary>
        /// <param name="strFormName">폼명</param>
        /// <param name="strInspectUserID">검사자아이디</param>
        /// <param name="strComment">comment</param>
        /// <param name="strHoldCode">Hold코드</param>
        /// <param name="dtLotList">Lot리스트 데이터 테이블</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">관리번호</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_MESHold(string strFormName, string strInspectUserID, string strComment
                                                , string strHoldCode, DataTable dtLotList
                                                , string strPlantCode, string strQCNNo, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버 경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S04");
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S07");
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                //MES 해당LotNo Hold요청 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);  //실행용

                //해당 LotHold요청 하여 결과를 받아온다
                DataTable dtMES = clsTibrv.LOT_HOLD4QC_REQ(strFormName
                                                        , strInspectUserID
                                                        , strComment
                                                        , strHoldCode
                                                        , dtLotList
                                                        , strUserIP);

                //요청하여 정보가 있는 경우
                if (dtMES.Rows.Count > 0)
                {
                    //요청한결과가 성공인 경우 테이블MESHoldTFlag를 업데이트 시킨다.
                    if (dtMES.Rows[0]["returncode"].ToString().Equals("0"))
                    {
                        //디비연결
                        sql.mfConnect(); //실행용

                        //트랜젝션시작
                        SqlTransaction transt;
                        transt = sql.SqlCon.BeginTransaction(); //실행용

                        //파라미터 정보저장
                        DataTable dtParamt = sql.mfSetParamDataTable();

                        sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParamt, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                        sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParamt, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                        sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        // QCN MES LotNo HoldTFlag 업데이트 프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSProcQCNMESHoldTFlag", dtParamt);    //실해용

                        // Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                            transt.Rollback();
                        else
                            transt.Commit();

                    }

                    //MES에서 넘어온 결과코드와, 메세지를 EnCoding하여 strErrRtn 저장
                    ErrRtn.InterfaceResultCode = dtMES.Rows[0]["returncode"].ToString();
                    ErrRtn.InterfaceResultMessage = dtMES.Rows[0]["returnmessage"].ToString();

                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);

                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// QCN 발행 헤더정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcQCN(string strPlantCode, string strQCNNo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";

                // 디비 연결
                sql.mfConnect();
                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 장비재검 삭제
                ProcQCNEquReInspect clsQCNEqu = new ProcQCNEquReInspect();
                strErrRtn = clsQCNEqu.mfDeleteINSProcQCNEquReInspect(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 자재재검 삭제
                ProcQCNMatReInspect clsQCNMat = new ProcQCNMatReInspect();
                strErrRtn = clsQCNMat.mfDeleteINSProcQCNMatReInspect(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 예상설비 테이블 삭제
                ProcQCNExpectEquip clsExpectEquip = new ProcQCNExpectEquip();
                strErrRtn = clsExpectEquip.mfDeleteINSProcQCNExpectEquip(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 사용자 정보 삭제
                ProcQCNUser clsQCNUser = new ProcQCNUser();
                strErrRtn = clsQCNUser.mfDeleteINSProcQCNUser(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 상세정보 삭제
                ProcQCNAffectLot clsAffectLot = new ProcQCNAffectLot();
                strErrRtn = clsAffectLot.mfDeleteINSProcQCNAffectLot(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    // 헤더정보 삭제
                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcQCN", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// QCN 발행 헤더정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcQCN_PSTS(string strPlantCode, string strQCNNo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";

                // 디비 연결
                sql.mfConnect();
                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 장비재검 삭제
                ProcQCNEquReInspect clsQCNEqu = new ProcQCNEquReInspect();
                strErrRtn = clsQCNEqu.mfDeleteINSProcQCNEquReInspect(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 자재재검 삭제
                ProcQCNMatReInspect clsQCNMat = new ProcQCNMatReInspect();
                strErrRtn = clsQCNMat.mfDeleteINSProcQCNMatReInspect(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 예상설비 테이블 삭제
                ProcQCNExpectEquip clsExpectEquip = new ProcQCNExpectEquip();
                strErrRtn = clsExpectEquip.mfDeleteINSProcQCNExpectEquip(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 사용자 정보 삭제
                ProcQCNUser clsQCNUser = new ProcQCNUser();
                strErrRtn = clsQCNUser.mfDeleteINSProcQCNUser(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                //불량유형정보삭제 2012-09-03
                ProcQCNFaultType clsFaulty = new ProcQCNFaultType();
                strErrRtn = clsFaulty.mfDeleteProcQCNFaultType(strPlantCode, strQCNNo, sql.SqlCon, trans);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 상세정보 삭제
                ProcQCNAffectLot clsAffectLot = new ProcQCNAffectLot();
                strErrRtn = clsAffectLot.mfDeleteINSProcQCNAffectLot(strPlantCode, strQCNNo, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    // 헤더정보 삭제
                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcQCN", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// QCN 발행 검색조건에 따른 검색메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAriseProcessCode">공정코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <returns>사용언어</returns>
        [AutoComplete]
        public DataTable mfReadInsProcQCN_Check(string strPlantCode, string strAriseProcessCode, string strLotNo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS(); //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString()); // 디버깅 모드
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCN_Check", dtParam);    //실행용
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSProcQCN_Check", dtParam);  //디버깅용

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        #endregion

        #region QCNList
        /// <summary>
        /// QCN List 저장용 데이터 테이블 컬럼 설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_List()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("QCNNo", typeof(string));
                dtRtn.Columns.Add("P_QCNITR", typeof(string));  // 구분 (QCN,ITR)
                dtRtn.Columns.Add("FirCauseDesc", typeof(string));
                dtRtn.Columns.Add("FirCauseUserID", typeof(string));
                dtRtn.Columns.Add("FirCauseDate", typeof(string));
                dtRtn.Columns.Add("FirCauseCompleteFlag", typeof(string));
                dtRtn.Columns.Add("FirCorrectDesc", typeof(string));
                dtRtn.Columns.Add("FirCorrectUserID", typeof(string));
                dtRtn.Columns.Add("FirCorrectDate", typeof(string));
                dtRtn.Columns.Add("FirCorrectCompleteFlag", typeof(string));
                dtRtn.Columns.Add("FirMeasureDesc", typeof(string));
                dtRtn.Columns.Add("FirMeasureUserID", typeof(string));
                dtRtn.Columns.Add("FirMeasureDate", typeof(string));
                dtRtn.Columns.Add("FirMeasureCompleteFlag", typeof(string));
                dtRtn.Columns.Add("Fir4MManFlag", typeof(string));
                dtRtn.Columns.Add("Fir4MEquipFlag", typeof(string));
                dtRtn.Columns.Add("Fir4MMethodFlag", typeof(string));
                dtRtn.Columns.Add("Fir4MEnviroFlag", typeof(string));
                dtRtn.Columns.Add("Fir4MMaterialFlag", typeof(string));
                dtRtn.Columns.Add("FirResultFlag", typeof(string));
                dtRtn.Columns.Add("SecActionDesc", typeof(string));
                dtRtn.Columns.Add("SecActionUserID", typeof(string));
                dtRtn.Columns.Add("SecActionDate", typeof(string));
                dtRtn.Columns.Add("SecActionCompleteFlag", typeof(string));
                dtRtn.Columns.Add("SecResultFlag", typeof(string));
                dtRtn.Columns.Add("ThiFCostQty", typeof(decimal));
                dtRtn.Columns.Add("ThiFCostTime", typeof(decimal));
                dtRtn.Columns.Add("ThiFCostPerson", typeof(decimal));
                dtRtn.Columns.Add("ThiFCostScrap", typeof(decimal));
                dtRtn.Columns.Add("ThiFCostCompleteFlag", typeof(string));
                dtRtn.Columns.Add("ThiResultFlag", typeof(string));
                dtRtn.Columns.Add("ImputationDeptCode", typeof(string));
                dtRtn.Columns.Add("QCConfirmUserID", typeof(string));
                dtRtn.Columns.Add("QCConfirmFlag", typeof(string));
                dtRtn.Columns.Add("QCConfirmDate", typeof(string));
                dtRtn.Columns.Add("MESReleaseTFlag", typeof(string));
                dtRtn.Columns.Add("QCConfirmComplete", typeof(string));
                dtRtn.Columns.Add("QCConfirmResultFlag", typeof(string));
                dtRtn.Columns.Add("EquReInspectCompleteFlag", typeof(string));
                dtRtn.Columns.Add("MaterialReInspectCompleteFlag", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                dtRtn.Columns.Add("TestSYSTEM", typeof(string));
                dtRtn.Columns.Add("TestHANDLER", typeof(string));
                dtRtn.Columns.Add("TestPROGRAM", typeof(string));

                dtRtn.Columns.Add("FirReturnDesc", typeof(string));
                dtRtn.Columns.Add("SecReturnDesc", typeof(string));
                dtRtn.Columns.Add("ThiReturnDesc", typeof(string));

                dtRtn.Columns.Add("File", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }



        /// <summary>
        /// QCN List 검색조건에 따른 검색메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strAriseDateFrom">발행일From</param>
        /// <param name="strAriseDateTo">발행일To</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInsProcQCN_List(string strPlantCode, string strProcessCode, string strQCNNo, string strLotNo, string strProductCode
                                        , string strQCConfirmFlag, string strAriseDateFrom, string strAriseDateTo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, strQCConfirmFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateTo", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNList", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// QCN List 검색조건에 따른 검색메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strAriseDateFrom">발행일From</param>
        /// <param name="strAriseDateTo">발행일To</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInsProcQCN_List_S(string strPlantCode, string strProcessCode, string strQCNNo, string strP_QCNITR,string strLotNo, string strProductCode
                                        , string strQCConfirmFlag, string strAriseDateFrom, string strAriseDateTo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strP_QNCITR", ParameterDirection.Input, SqlDbType.VarChar, strP_QCNITR, 3);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, strQCConfirmFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateTo", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNList_S", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN List 헤더정보 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">언어설정</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNDetail_List(String strPlantCode, String strQCNNo, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비호출
                sql.mfConnect();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 datatable에 넣기
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 호출
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNDetail_List", dtParameter);

                //리턴정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN List 헤더정보 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">언어설정</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNDetail_List_PSTS(String strPlantCode, String strQCNNo, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비호출
                sql.mfConnect();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 datatable에 넣기
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저 호출
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNDetail_List_PSTS", dtParameter);

                //리턴정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCNList 헤더 하단 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN 번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNDetail_ListBottom(string strPlantCode, string strQCNNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCN_ListBottom", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCNList 헤더 하단 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN 번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNDetail_ListBottom_PSTS(string strPlantCode, string strQCNNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCN_ListBottom_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 자재점검 ResultFlag 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQCNNo"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNMatReInspectCheck(String strPlantCode, String strQCNNo)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNMatReInspectCheck", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 설비점검 ResultFlag 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQCNNo"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNEquReInspectCheck(String strPlantCode, String strQCNNo)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNEquReInspectCheck", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// QCN List 저장 Method
        /// </summary>
        /// <param name="dtQCNList">QCNList 정보 저장된 DataTable</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_List(DataTable dtQCNList, string strUserID, string strUserIP)     //,string strFormName,DataTable dtAffectLot)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                string strErrRtn = "";

                for (int i = 0; i < dtQCNList.Rows.Count; i++)
                {
                    //QC최종 Confirm체크 가 되었던 정보가 아닌경우
                    if (!dtQCNList.Rows[i]["QCConfirmComplete"].ToString().Equals("T"))
                    {
                        trans = sql.SqlCon.BeginTransaction();

                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["QCNNo"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[0]["EtcDesc"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCauseDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[0]["FirCauseDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCauseUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirCauseUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCauseDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirCauseDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strFIRCauseCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirCauseCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCorrectDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[0]["FirCorrectDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCorrectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirCorrectUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCorrectDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirCorrectDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirCorrectCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirCorrectCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirMeasureDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[0]["FirMeasureDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirMeasureUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirMeasureUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirMeasureDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirMeasureDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirMeasureCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirMeasureCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MManFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["Fir4MManFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MEquipFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["Fir4MEquipFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MMethodFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["Fir4MMethodFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MEnviroFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["Fir4MEnviroFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFir4MMaterialFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["Fir4MMaterialFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strFirResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["FirResultFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecActionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNList.Rows[0]["SecActionDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecActionUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["SecActionUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecActionDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["SecActionDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecActionCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["SecActionCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strSecResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["SecResultFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_dblThiFCostQty", ParameterDirection.Input, SqlDbType.Decimal, dtQCNList.Rows[0]["ThiFCostQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblThiFCostTime", ParameterDirection.Input, SqlDbType.Decimal, dtQCNList.Rows[0]["ThiFCostTime"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblThiFCostPerson", ParameterDirection.Input, SqlDbType.Decimal, dtQCNList.Rows[0]["ThiFCostPerson"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_dblThiFCostScrap", ParameterDirection.Input, SqlDbType.Decimal, dtQCNList.Rows[0]["ThiFCostScrap"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strThiFCostCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["ThiFCostCompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strThiResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["ThiResultFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strImputationDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["ImputationDeptCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["QCConfirmUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["QCConfirmDate"].ToString(), 30);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, dtQCNList.Rows[0]["QCConfirmFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strQCConfirmResultFlag", ParameterDirection.Input, SqlDbType.Char, dtQCNList.Rows[0]["QCConfirmResultFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_List", dtParam);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        else
                        {
                            trans.Commit();
                        }
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 자재 재검 팝업 저장시 CompleteFlag 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_ListMatComplete(DataTable dt, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                string strErrRtn = "";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMatCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MaterialReInspectCompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_MaterialReInspectComplete", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        /// <summary>
        /// 설비 재검 저장시 CompleteFlag저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_ListEquComplete(DataTable dt, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                string strErrRtn = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["EquReInspectCompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_EquReInspectComplete", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES TrackOUT용 DataTable 컬럼 설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_MESTrackOUT()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("FormName", typeof(string));
                dtRtn.Columns.Add("FACTORYID", typeof(string));
                dtRtn.Columns.Add("USERID", typeof(string));
                dtRtn.Columns.Add("LOTID", typeof(string));
                dtRtn.Columns.Add("EQPID", typeof(string));
                dtRtn.Columns.Add("RECIPEID", typeof(string));
                dtRtn.Columns.Add("AREAID", typeof(string));
                dtRtn.Columns.Add("SPLITFLAG", typeof(string));
                dtRtn.Columns.Add("SCRAPFLAG", typeof(string));
                dtRtn.Columns.Add("REPAIRFLAG", typeof(string));
                dtRtn.Columns.Add("RWFLAG", typeof(string));
                dtRtn.Columns.Add("CONSUMEFLAG", typeof(string));
                dtRtn.Columns.Add("COMMENT", typeof(string));
                dtRtn.Columns.Add("QCFLAG", typeof(string));
                dtRtn.Columns.Add("SAMPLECOUNT", typeof(string));
                dtRtn.Columns.Add("QUALEFLAG", typeof(string));
                dtRtn.Columns.Add("REASONQTY", typeof(string));
                dtRtn.Columns.Add("REASONCODE", typeof(string));
                dtRtn.Columns.Add("CAUSEEQPID", typeof(string));
                dtRtn.Columns.Add("QCNNo", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 테이블 헤더 Lot Release 성공시 Flag 업데이트 메소드
        /// ReleaseFlag는 자재재검에서 보내고 성공시 헤더에 ReleaseFlag "T"로 업데이트
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">관리번호</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_MESReleaseTFlag(string strPlantCode, string strQCNNo, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect(); //실행용

                //트랜젝션시작
                SqlTransaction transt;
                transt = sql.SqlCon.BeginTransaction(); //실행용
                //transt = m_SqlConnDebug.BeginTransaction();// 디버깅용


                //파라미터 정보저장
                DataTable dtParamt = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamt, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // QCN MES LotNo MESReleaseTFlag 업데이트 프로시저 실행
                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSProcQCNMESReleaseTFlag", dtParamt);    //실행용

                // Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    transt.Rollback();
                else
                    transt.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정검사에서 QCN 이동한 경우 TrackIn 공정이면 TrackOut 처리하는 메소드
        /// </summary>
        /// <param name="dtStdInfo">기본정보 저장된 DataTable</param>
        /// <param name="dtScrapList">Scrap DataTable</param>
        /// <param name="dtFaultInfo">불량정보 DataTable</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_MESTrackOUT(DataTable dtStdInfo, DataTable dtScrapList, DataTable dtFaultInfo, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S04");
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S07");
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKOUT4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString(),
                                                                    dtStdInfo.Rows[0]["FACTORYID"].ToString(),
                                                                    dtStdInfo.Rows[0]["USERID"].ToString(),
                                                                    dtStdInfo.Rows[0]["LOTID"].ToString(),
                                                                    dtStdInfo.Rows[0]["EQPID"].ToString(),
                                                                    dtStdInfo.Rows[0]["RECIPEID"].ToString(),
                                                                    dtStdInfo.Rows[0]["AREAID"].ToString(),
                                                                    dtStdInfo.Rows[0]["SPLITFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["SCRAPFLAG"].ToString(),
                                                                    dtScrapList,
                                                                    dtStdInfo.Rows[0]["REPAIRFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["RWFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["CONSUMEFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["COMMENT"].ToString(),
                                                                    dtStdInfo.Rows[0]["QCFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["SAMPLECOUNT"].ToString(),
                                                                    dtStdInfo.Rows[0]["QUALEFLAG"].ToString(),
                                                                    dtFaultInfo,
                                                                    strUserIP);

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 디비연결
                    sql.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["FACTORYID"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_MESTrackOut", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES I/F TrackOut 처리 메소드
        /// </summary>
        /// <param name="dtStdInfo">I/F 와 성공시 DB업데이트에 필요한 정보가 담긴 데이터 테이블</param>
        /// <param name="dtScrapList">ScrapList 데이터 테이블</param>
        /// <param name="dtInspectItemList">검사항목데이터테이블</param>
        /// <param name="dtFaultInfo">불량정보데이터테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_MESTrackOUT(DataTable dtStdInfo, DataTable dtScrapList, DataTable dtInspectItemList, DataTable dtFaultInfo, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S04");      //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S07");        //Test Server
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKOUT4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString(),
                                                                    dtStdInfo.Rows[0]["FACTORYID"].ToString(),
                                                                    dtStdInfo.Rows[0]["USERID"].ToString(),
                                                                    dtStdInfo.Rows[0]["LOTID"].ToString(),
                                                                    dtStdInfo.Rows[0]["EQPID"].ToString(),
                                                                    dtStdInfo.Rows[0]["RECIPEID"].ToString(),
                                                                    dtStdInfo.Rows[0]["AREAID"].ToString(),
                                                                    dtStdInfo.Rows[0]["SPLITFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["SCRAPFLAG"].ToString(),
                                                                    dtScrapList,
                                                                    dtStdInfo.Rows[0]["REPAIRFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["RWFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["CONSUMEFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["COMMENT"].ToString(),
                                                                    dtStdInfo.Rows[0]["QCFLAG"].ToString(),
                                                                    dtInspectItemList,
                                                                    dtFaultInfo,
                                                                    strUserIP);

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 디비연결
                    sql.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["FACTORYID"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_MESTrackOut", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES I/F TrackOut 처리 메소드
        /// </summary>
        /// <param name="dtStdInfo">I/F 와 성공시 DB업데이트에 필요한 정보가 담긴 데이터 테이블</param>
        /// <param name="dtScrapList">ScrapList 데이터 테이블</param>
        /// <param name="dtInspectItemList">검사항목데이터테이블</param>
        /// <param name="dtFaultInfo">불량정보데이터테이블</param>
        /// <param name="strTKOutFlag">Track Out 여부</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_MESTrackOUT(DataTable dtStdInfo, DataTable dtScrapList, DataTable dtInspectItemList, DataTable dtFaultInfo, string strTKOutFlag,string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S04");      //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S07");        //Test Server
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKOUT4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString(),
                                                                    dtStdInfo.Rows[0]["FACTORYID"].ToString(),
                                                                    dtStdInfo.Rows[0]["USERID"].ToString(),
                                                                    dtStdInfo.Rows[0]["LOTID"].ToString(),
                                                                    dtStdInfo.Rows[0]["EQPID"].ToString(),
                                                                    dtStdInfo.Rows[0]["RECIPEID"].ToString(),
                                                                    dtStdInfo.Rows[0]["AREAID"].ToString(),
                                                                    dtStdInfo.Rows[0]["SPLITFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["SCRAPFLAG"].ToString(),
                                                                    dtScrapList,
                                                                    dtStdInfo.Rows[0]["REPAIRFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["RWFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["CONSUMEFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["COMMENT"].ToString(),
                                                                    dtStdInfo.Rows[0]["QCFLAG"].ToString(),
                                                                    dtInspectItemList,
                                                                    dtFaultInfo,
                                                                    strTKOutFlag,
                                                                    strUserIP);

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 디비연결
                    sql.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["FACTORYID"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCN_MESTrackOut", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// QCNList QCNCalcelFlag Update메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN발행번호</param>
        /// <param name="strQCNCancelFlag">QCN취소여부</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_List_QCNCancelFlag(string strPlantCode, string strQCNNo, string strQCNCancelFlag, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNCancelFlag", ParameterDirection.Input, SqlDbType.Char, strQCNCancelFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCNList_QCNCancelFlag", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        #endregion
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcQCNAffectLot")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcQCNAffectLot : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("QCNNo", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("LotType", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("ProductCode", typeof(string));
                dtRtn.Columns.Add("ProcessCode", typeof(string));
                dtRtn.Columns.Add("EquipCode", typeof(string));
                dtRtn.Columns.Add("Qty", typeof(decimal));
                dtRtn.Columns.Add("WorkDateTime", typeof(DateTime));
                dtRtn.Columns.Add("WorkUserID", typeof(string));
                dtRtn.Columns.Add("MESHoldTFlag", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 발행 AffectLot 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">언어설정</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNAffectLot(String strPlantCode, String strQCNNo, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비호출
                sql.mfConnect();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 datatable에 넣기
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);

                //프로시저 호출
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNAffectLot", dtParameter);

                //리턴정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// QCN 발행 AffectLot 정보 저장 메소드
        /// </summary>
        /// <param name="dtAffect">AffectLot 정보 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">Sqlconnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCNAffectLot(DataTable dtAffect, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strQCNNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtAffect.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtAffect.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtAffect.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLotType", ParameterDirection.Input, SqlDbType.VarChar, dtAffect.Rows[i]["LotType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtAffect.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtAffect.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtAffect.Rows[i]["ProcessCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtAffect.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@I_dblQty", ParameterDirection.Input, SqlDbType.Decimal, dtAffect.Rows[i]["Qty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_datWorkDateTime", ParameterDirection.Input, SqlDbType.DateTime, dtAffect.Rows[i]["WorkDateTime"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtAffect.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcQCNAffectLot", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// QCNAffectLot 정보 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcQCNAffectLot(string strPlantCode, string strQCNNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSProcQCNAffectLot", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// QCN MESHold 전송 메소드
        /// </summary>
        /// <param name="strFormName">폼명</param>
        /// <param name="strInspectUserID">검사자아이디</param>
        /// <param name="strComment">comment</param>
        /// <param name="strHoldCode">Hold코드</param>
        /// <param name="dtLotList">Lot리스트 데이터 테이블</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">관리번호</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCN_MESHold_AffectLot(string strFormName, string strInspectUserID, string strComment
                                                , string strHoldCode, DataTable dtLotList
                                                , string strPlantCode, string strQCNNo, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버 경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S04");
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S07");
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                //MES 해당LotNo Hold요청 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);  //실행용

                //해당 LotHold요청 하여 결과를 받아온다
                DataTable dtMES = clsTibrv.LOT_HOLD4QC_REQ(strFormName
                                                        , strInspectUserID
                                                        , strComment
                                                        , strHoldCode
                                                        , dtLotList
                                                        , strUserIP);

                //요청하여 정보가 있는 경우
                if (dtMES.Rows.Count > 0)
                {
                    //요청한결과가 성공인 경우 테이블MESHoldTFlag를 업데이트 시킨다.
                    if (dtMES.Rows[0]["returncode"].ToString().Equals("0"))
                    {
                        //디비연결
                        sql.mfConnect(); //실행용

                        //트랜젝션시작
                        SqlTransaction transt;
                        transt = sql.SqlCon.BeginTransaction(); //실행용

                        //파라미터 정보저장
                        DataTable dtParamt = sql.mfSetParamDataTable();

                        sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParamt, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                        sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParamt, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                        sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        // QCN MES LotNo HoldTFlag 업데이트 프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSProcQCNMESHoldTFlag_AffectLot", dtParamt);    //실해용

                        // Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                            transt.Rollback();
                        else
                            transt.Commit();

                    }

                    //MES에서 넘어온 결과코드와, 메세지를 EnCoding하여 strErrRtn 저장
                    ErrRtn.InterfaceResultCode = dtMES.Rows[0]["returncode"].ToString();
                    ErrRtn.InterfaceResultMessage = dtMES.Rows[0]["returnmessage"].ToString();

                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);

                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcQCNEquReInspect")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcQCNEquReInspect : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("QCNNo", typeof(string));
                dtRtn.Columns.Add("InspectSeq", typeof(Int32));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("InspectResult", typeof(string));
                dtRtn.Columns.Add("FaultDesc", typeof(string));
                dtRtn.Columns.Add("ResultFlag", typeof(string));
                dtRtn.Columns.Add("InspectUserID", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("EquipCode", typeof(string));
                dtRtn.Columns.Add("OperCondition", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="intInspectSeq">검사번호</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNEquReInspect(string strPlantCode, string strQCNNo, int intInspectSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intInspectSeq", ParameterDirection.Input, SqlDbType.Int, intInspectSeq.ToString());

                // 프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNEquReInspect", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>처리결과</returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNEquReInspect_Check(string strPlantCode, string strQCNNo)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);

                // 프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNEquReInspect_Check", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 장비재검 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장데이터</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCNEquReInspect(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // 디비연결
                sql.mfConnect();

                string strErrRtn = "";
                SqlTransaction trans;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intInspectSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["InspectSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResult", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["InspectResult"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["FaultDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["EquipCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strOperCondition", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["OperCondition"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 저장프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCNEquReInspect", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 장비재검 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcQCNEquReInspect(string strPlantCode, string strQCNNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSProcQCNEquReInspect", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcQCNMatReInspect")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcQCNMatReInspect : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("QCNNo", typeof(string));
                dtRtn.Columns.Add("InspectSeq", typeof(Int32));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("InspectResult", typeof(string));
                dtRtn.Columns.Add("InspectQty", typeof(decimal));
                dtRtn.Columns.Add("FaultTypeCode", typeof(string));
                dtRtn.Columns.Add("FaultQty", typeof(decimal));
                dtRtn.Columns.Add("FaultDesc", typeof(string));
                dtRtn.Columns.Add("ResultFlag", typeof(string));
                dtRtn.Columns.Add("InspectUserID", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("MESReleaseTFlag", typeof(string));
                dtRtn.Columns.Add("MESHoldTFlag", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="intInspectSeq">검사번호</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNMatReInspect(string strPlantCode, string strQCNNo, int intInspectSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intInspectSeq", ParameterDirection.Input, SqlDbType.Int, intInspectSeq.ToString());

                // 프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNMatReInspect", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 자재재검여부 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>처리결과</returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNMatReInspect_Check(string strPlantCode, string strQCNNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNMatReInspect_Check", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 자재재검 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장데이터</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCNMatReInspect(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // 디비연결
                sql.mfConnect();

                string strErrRtn = "";
                SqlTransaction trans;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intInspectSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["InspectSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResult", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["InspectResult"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["InspectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["FaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["FaultDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMESHoldFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["MESHoldTFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 저장프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCNMatReInspect", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 장비재검 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN번호</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcQCNMatReInspect(string strPlantCode, string strQCNNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSProcQCNMatReInspect", dtParam);
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 자재재검 MESRelease I/F 메소드
        /// </summary>
        /// <param name="strFormName">폼명</param>
        /// <param name="strInspectUserID">검사자ID</param>
        /// <param name="strComment">Comment</param>
        /// <param name="dtLotList">LotList DataTable</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">관리번호</param>
        /// <param name="intInspectSeq">검사차수</param>
        /// <param name="intSeq">검사순번</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCNMatReInspect_MESRelease(string strFormName, string strInspectUserID, string strComment, DataTable dtLotList
                                                            , string strPlantCode, string strQCNNo, int intInspectSeq, int intSeq, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S04");
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S07");
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                //QCNList MES전송 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtMES = clsTibrv.LOT_RELEASE4QC_REQ(strFormName
                                                            , strInspectUserID.ToUpper()
                                                            , strComment
                                                            , dtLotList
                                                            , strUserIP);

                // MES I/F 성공시
                if (dtMES.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    sql.mfConnect();
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intInspectSeq", ParameterDirection.Input, SqlDbType.Int, intInspectSeq.ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, intSeq.ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCNMatReInspect_MESRelease", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                        trans.Commit();
                    else
                        trans.Rollback();
                }
                ErrRtn.InterfaceResultCode = dtMES.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtMES.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcQCNMatReInspectValue")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcQCNMatReInspectValue : ServicedComponent
    {
        /// <summary>
        /// QCN 자재재검 검사값 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN발행번호</param>
        /// <param name="intInspectSeq">n차순번</param>
        /// <param name="intSeq">검사순번</param>
        /// <param name="intValueSeq">검사값 순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadProcQCNMatReInspectValue(string strPlantCode, string strQCNNo, int intInspectSeq, int intSeq, int intValueSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtInspectValue = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intInspectSeq", ParameterDirection.Input, SqlDbType.Int, intInspectSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, intSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intValueSeq", ParameterDirection.Input, SqlDbType.Int, intValueSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtInspectValue = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNMatReInspectValue", dtParam);

                return dtInspectValue;
            }
            catch (Exception ex)
            {
                return dtInspectValue;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// QCN 자재재검 검사값 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN 발행번호</param>
        /// <param name="intInspectSeq">n차순번</param>
        /// <param name="intSeq">검사순번</param>
        /// <param name="intValueSeq">검사값순번 : -999 일시 해당 관리번호 전체 검사값 정보 삭제</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">Transaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteProcQCNMatReInspectValue(string strPlantCode, string strQCNNo, int intInspectSeq, int intSeq, int intValueSeq
                                                    , SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intInspectSeq", ParameterDirection.Input, SqlDbType.Int, intInspectSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, intSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intValueSeq", ParameterDirection.Input, SqlDbType.Int, intValueSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSProcQCNMatReInspectValue", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// up_Update_INSProcQCNMatReInspectValue
        /// </summary>
        /// <param name="dtValue">검사값 정보 데이터 테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveProcQCNMatReInspectValue(DataTable dtValue, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 삭제
                if (dtValue.Rows.Count > 0)
                {
                    strErrRtn = mfDeleteProcQCNMatReInspectValue(dtValue.Rows[0]["PlantCode"].ToString()
                                                                , dtValue.Rows[0]["QCNNo"].ToString()
                                                                , Convert.ToInt32(dtValue.Rows[0]["InspectSeq"])
                                                                , Convert.ToInt32(dtValue.Rows[0]["Seq"])
                                                                , Convert.ToInt32(dtValue.Compute("MAX(ValueSeq)", string.Empty))
                                                                , sql.SqlCon
                                                                , trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        return strErrRtn;
                }

                for (int i = 0; i < dtValue.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtValue.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtValue.Rows[i]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intInspectSeq", ParameterDirection.Input, SqlDbType.Int, dtValue.Rows[i]["InspectSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtValue.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intValueSeq", ParameterDirection.Input, SqlDbType.Int, dtValue.Rows[i]["ValueSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtValue.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcQCNMatReInspectValue", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcQCNExpectEquip")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcQCNExpectEquip : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("QCNNo", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("CheckFlag", typeof(string));
                dtRtn.Columns.Add("ExpectEquipCode", typeof(string));
                dtRtn.Columns.Add("WorkUserID", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                throw (ex);
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 예상설비 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNExpectEquip(string strPlantCode, string strQCNNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNExpectEquip", dtParam);
            }
            catch (Exception ex)
            {
                throw (ex);
                return dtRtn;
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 예상설비 저장 메소드
        /// </summary>
        /// <param name="dtEquipList">예상설비 정보 데이터테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCNExpectEquip(DataTable dtEquipList, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strQCNNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;
                DataTable dtParam;

                for (int i = 0; i < dtEquipList.Rows.Count; i++)
                {
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckFlag", ParameterDirection.Input, SqlDbType.Char, dtEquipList.Rows[i]["CheckFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strExpectEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["ExpectEquipCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipList.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcQCNExpectEquip", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// QCN 예상설비 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">QCN 관리번호</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcQCNExpectEquip(string strPlantCode, string strQCNNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                return strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSProcQCNExpectEquip", dtParam);
            }
            catch (Exception ex)
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcQCNUser")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcQCNUser : ServicedComponent  //실행용
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("QCNNo", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("UserID", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN발행 수신인 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcQCNUser(string strPlantCode, string strQCNNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_StrQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNUser", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 발행 수신인 저장 메소드
        /// </summary>
        /// <param name="dtQCNUser">수신인 정보 DataTable</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SQLConnention 변수</param>
        /// <param name="trans">Transaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcQCNUser(DataTable dtQCNUser, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strQCNNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                DataTable dtParam = new DataTable();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtQCNUser.Rows.Count; i++)
                {
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtQCNUser.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);//dtQCNUser.Rows[i]["QCNNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtQCNUser.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, dtQCNUser.Rows[i]["UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtQCNUser.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strLogUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLogUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strQCNNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcQCNUser", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// QCN 발행 수신인 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQCNNo">관리번호</param>
        /// <param name="sqlCon">SQLConnention 변수</param>
        /// <param name="trans">Transaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcQCNUser(string strPlantCode, string strQCNNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSProcQCNUser", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPCN")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SPCN : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(int));
                dtRtn.Columns.Add("ReqItemSeq", typeof(int));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("WriteTime", typeof(string));
                dtRtn.Columns.Add("OCP", typeof(string));
                dtRtn.Columns.Add("UpperRun", typeof(string));
                dtRtn.Columns.Add("LowerRun", typeof(string));
                dtRtn.Columns.Add("UpperTrend", typeof(string));
                dtRtn.Columns.Add("LowerTrend", typeof(string));
                dtRtn.Columns.Add("Cycle", typeof(string));
                dtRtn.Columns.Add("ProblemDesc", typeof(string));
                dtRtn.Columns.Add("ProblemUserID", typeof(string));
                dtRtn.Columns.Add("ProblemDate", typeof(string));
                dtRtn.Columns.Add("CauseDesc", typeof(string));
                dtRtn.Columns.Add("CauseUserID", typeof(string));
                dtRtn.Columns.Add("CauseDate", typeof(string));
                dtRtn.Columns.Add("PreventionDesc", typeof(string));
                dtRtn.Columns.Add("PreventionUserID", typeof(string));
                dtRtn.Columns.Add("PreventionDate", typeof(string));
                dtRtn.Columns.Add("ResultDesc", typeof(string));
                dtRtn.Columns.Add("ResultUserID", typeof(string));
                dtRtn.Columns.Add("ResultDate", typeof(string));
                dtRtn.Columns.Add("CompleteFlag", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [SPCN LIST] List 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strProductCode"> 제품코드 </param>
        /// <param name="strLotNo"> LotNo </param>        
        /// <param name="strProcessCode"> 공정 </param>
        /// <param name="strWriteFromDate"> 등록일 From </param>
        /// <param name="strWriteToDate"> 등록일 To </param>        
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSPCNList(String strPlantCode, String strCompleteFlag, String strProcessCode,
                                        String strWriteFromDate, String strWriteToDate, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, strCompleteFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteFromDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteToDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcSPCNList", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [SPCN LIST] 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="intReqLotSeq"> 의뢰Lot순번 </param>
        /// <param name="intReqItemSeq"> 검사항목순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSPCNDetail(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq, int intReqItemSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcSPCNDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [SPCN LIST] 저장 Method
        /// </summary>
        /// <param name="dtSPCN"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>        
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveSPCNSave(DataTable dtSPCN, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                String strErrRtn = "";
                // 디비연결
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtSPCN.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSPCN.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqitemSeq", ParameterDirection.Input, SqlDbType.Int, dtSPCN.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteTime", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["WriteTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strOCP", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["OCP"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUpperRun", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["UpperRun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLowerRun", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["LowerRun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUpperTrend", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["UpperTrend"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLowerTrend", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["LowerTrend"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCycle", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["Cycle"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strProblemDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSPCN.Rows[i]["ProblemDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strProblemUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ProblemUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProblemDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ProblemDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSPCN.Rows[i]["CauseDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["CauseUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["CauseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPreventionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSPCN.Rows[i]["PreventionDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strPreventionUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["PreventionUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPreventionDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["PreventionDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSPCN.Rows[i]["ResultDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ResultUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ResultDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcSPCN", dtParam);

                    // 실행결과
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // 실패이면 Rollback
                        trans.Rollback();
                        break;
                    }
                    trans.Commit();

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// [SPCN LIST] 측정값 Data 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="intReqLotSeq"> 의뢰Lot순번 </param>
        /// <param name="intReqItemSeq"> 검사항목순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSPCNDetailValue(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq, int intReqItemSeq)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcSPCNDetail_Value", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// SPCN 작성완료후 MES SPCN_ALARM 메소드
        /// </summary>
        /// <param name="strFormName">프로그램명</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot번호</param>
        /// <param name="intReqItemSeq">항목번호</param>
        /// <returns></returns>
        public string mfSaveSPCNMESIF(string strFormName, string strUserID, string strEquipCode, string strUserIP,
                                        string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, int intReqItemSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S04");
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S07");
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                clsTibrv.SPCN_ALARM(strFormName, strUserID, "*", "*", strEquipCode, "Y", strUserIP);

                sql.mfConnect();
                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcSPCN_MESSPCN", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                else
                {
                    trans.Rollback();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Monitoring")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Monitoring : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("EquipCode", typeof(string));
                dtRtn.Columns.Add("ProcessCode", typeof(string));
                dtRtn.Columns.Add("ProductCode", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 작성완료시 CycleMonitoring Update메소드
        /// </summary>
        /// <param name="dtSave">저장정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCycleMonitoring(DataTable dtSave, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcCycleMonitoring", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// [ 순회검사 모니터링] List 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strEquipLocCode"> 위치코드 </param>  
        /// <param name="strStartFromDate"> 모니터링 시작일 </param>
        /// <param name="strStartToDate"> 모니터링 시작일 </param>  
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCycleMonitoring(String strPlantCode, String strEquipLocCode, String strStartFromDate, String strStartToDate, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStartFromDate", ParameterDirection.Input, SqlDbType.VarChar, strStartFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStartToDate", ParameterDirection.Input, SqlDbType.VarChar, strStartToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCycleMonitoring", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [ 순회검사 모니터링] List 검색 Method (Area - 위치 - 설비그룹 - 설비별 모니터링 의 계층형태로 구성)
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strEquipLocCode"> 위치코드 </param>  
        /// <param name="strStartFromDate"> 모니터링 시작일 </param>
        /// <param name="strStartToDate"> 모니터링 시작일 </param>  
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCycleMonitoringHierarchy(String strPlantCode, String strEquipLocCode, String strStartFromDate, String strStartToDate, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStartFromDate", ParameterDirection.Input, SqlDbType.VarChar, strStartFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStartToDate", ParameterDirection.Input, SqlDbType.VarChar, strStartToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCycleMonitoring_Hierarchy", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectReqH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectReqH : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqDeptCode", typeof(string));
                dtRtn.Columns.Add("ReqUserID", typeof(string));
                dtRtn.Columns.Add("ReqDate", typeof(string));
                dtRtn.Columns.Add("InspectReqTypeCode", typeof(string));
                dtRtn.Columns.Add("EmergencyTypeCode", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 검색조건에 따른 조회 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strProductCode"></param>
        /// <param name="strProcInspectType"></param>
        /// <param name="strLotNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqH(string strPlantCode, string strProductCode, string strProcInspectType, string strLotNo
                                                , string strFromInspectDate, string strToInspectDate
                                                , string strCustomerCOde, string strPackage, string strDetailProcType, string strProcessCode
                                                , string strCustomerProductSpec, string strInspectUserID
                                                , string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProcInspectType", ParameterDirection.Input, SqlDbType.VarChar, strProcInspectType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strFromInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strFromInspectDate, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strToInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strToInspectDate, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCOde, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDetailProcType", ParameterDirection.Input, SqlDbType.VarChar, strDetailProcType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerProductSpec", ParameterDirection.Input, SqlDbType.VarChar, strCustomerProductSpec, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, strInspectUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqH", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 검색조건에 따른 조회 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strProductCode"></param>
        /// <param name="strProcInspectType"></param>
        /// <param name="strLotNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqH_PSTS(string strPlantCode, string strProductCode, string strProcInspectType, string strLotNo
                                                , string strFromInspectDate, string strToInspectDate
                                                , string strCustomerCOde, string strPackage, string strDetailProcType, string strProcessCode
                                                , string strCustomerProductSpec, string strInspectUserID, string strEquipCode
                                                , string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProcInspectType", ParameterDirection.Input, SqlDbType.VarChar, strProcInspectType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strFromInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strFromInspectDate, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strToInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strToInspectDate, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCOde, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDetailProcType", ParameterDirection.Input, SqlDbType.VarChar, strDetailProcType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerProductSpec", ParameterDirection.Input, SqlDbType.VarChar, strCustomerProductSpec, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, strInspectUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqH_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
        
        /// <summary>
        /// MES I/F 이후 TrackInFlag 가져오기
        /// </summary>
        /// <param name="PlantCode"></param>
        /// <param name="strProductCode"></param>
        /// <param name="strProcessCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReq_InitCheck(string strPlantCode, string strProcessCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReq_InitCheck", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        /// <param name="dtSave">헤더정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="dtLot">Lot정보 데이터 테이블</param>
        /// <param name="dtItem">아이템정보 데이터 테이블</param>
        /// <param name="dsResult">검사결과 데이터 테이블</param>
        /// <param name="dtFault">불량유형 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqH(DataTable dtSave, string strUserID, string strUserIP, DataTable dtLot, DataTable dtItem, DataSet dsResult, DataTable dtFault, DataTable dtCycle)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectReqTypeCode"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEmergencyTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["EmergencyTypeCode"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqH", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();

                        // 관리번호 변수에 저장
                        string strReqNo = ErrRtn.mfGetReturnValue(0);
                        string strReqSeq = ErrRtn.mfGetReturnValue(1);

                        if (dtLot.Rows.Count > 0)
                        {
                            // Lot정보 저장
                            ProcInspectReqLot clsLot = new ProcInspectReqLot();
                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot(dtLot, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                            //결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                //trans.Rollback();
                                break;
                            }
                        }

                        // Item 정보포함 하위 정보 삭제
                        ProcInspectReqItem clsItem = new ProcInspectReqItem();
                        strErrRtn = clsItem.mfDeleteINSProcInspectItem(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq, sql.SqlCon, trans);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                            //trans.Rollback();
                            break;
                        }

                        if (dtItem.Rows.Count > 0)
                        {
                            // Item 정보 저장
                            strErrRtn = clsItem.mfSaveINSProcInspectItem(dtItem, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                            //결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                //trans.Rollback();
                                break;
                            }
                        }

                        // 검사결과 저장
                        if (dsResult.Tables.Count > 0)
                        {
                            // 계수
                            if (dsResult.Tables["Count"].Rows.Count > 0)
                            {
                                ProcInspectResultCount clsCount = new ProcInspectResultCount();
                                strErrRtn = clsCount.mfSaveINSProcResultCount(dsResult.Tables["Count"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }
                            // 계량
                            if (dsResult.Tables["Measure"].Rows.Count > 0)
                            {
                                ProcInspectResultMeasure clsMeasure = new ProcInspectResultMeasure();
                                strErrRtn = clsMeasure.mfSaveINSProcResultMeasure(dsResult.Tables["Measure"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }
                            // OkNg
                            if (dsResult.Tables["OkNg"].Rows.Count > 0)
                            {
                                ProcInspectResultOkNg clsOkNg = new ProcInspectResultOkNg();
                                strErrRtn = clsOkNg.mfSaveINSProcResultOkNg(dsResult.Tables["OkNg"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }

                            // 설명
                            if (dsResult.Tables["Desc"].Rows.Count > 0)
                            {
                                ProcInspectResultDesc clsDesc = new ProcInspectResultDesc();
                                strErrRtn = clsDesc.mfSaveINSProcResultDesc(dsResult.Tables["Desc"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }

                            // 선택
                            if (dsResult.Tables["Select"].Rows.Count > 0)
                            {
                                ProcInspectResultSelect clsSelect = new ProcInspectResultSelect();
                                strErrRtn = clsSelect.mfSaveINSProcResultSelect(dsResult.Tables["Select"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }
                        }
                        // 불량정보 저장
                        if (dtFault.Rows.Count > 0)
                        {
                            ProcInspectResultFault clsFault = new ProcInspectResultFault();
                            strErrRtn = clsFault.mfSaveINSProcInspectResultFault(dtFault, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                            //결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                //trans.Rollback();
                                break;
                            }
                        }

                        // 작성완료시 순회검사모니터링 테이블에 저장
                        if (dtLot.Rows[0]["CompleteFlag"].ToString().Equals("T"))
                        {
                            if (dtCycle.Rows.Count > 0)
                            {
                                Monitoring clsMot = new Monitoring();
                                strErrRtn = clsMot.mfSaveCycleMonitoring(dtCycle, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    //trans.Rollback();
                                    break;
                                }
                            }
                        }

                        //trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        /// <param name="dtSave">헤더정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="dtLot">Lot정보 데이터 테이블</param>
        /// <param name="dtItem">아이템정보 데이터 테이블</param>
        /// <param name="dsResult">검사결과 데이터 테이블</param>
        /// <param name="dtFault">불량유형 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqH_S(DataTable dtSave, string strUserID, string strUserIP, DataTable dtLot, DataTable dtItem, DataSet dsResult, DataTable dtFault, DataTable dtCycle)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectReqTypeCode"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEmergencyTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["EmergencyTypeCode"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqH", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();

                        // 관리번호 변수에 저장
                        string strReqNo = ErrRtn.mfGetReturnValue(0);
                        string strReqSeq = ErrRtn.mfGetReturnValue(1);

                        if (dtLot.Rows.Count > 0)
                        {
                            // Lot정보 저장
                            ProcInspectReqLot clsLot = new ProcInspectReqLot();
                            strErrRtn = clsLot.mfSaveINSProcInspectReqLot_PSTS(dtLot, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                            //결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                //trans.Rollback();
                                break;
                            }
                        }

                        // Item 정보포함 하위 정보 삭제
                        ProcInspectReqItem clsItem = new ProcInspectReqItem();
                        strErrRtn = clsItem.mfDeleteINSProcInspectItem(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq, sql.SqlCon, trans);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                            //trans.Rollback();
                            break;
                        }

                        if (dtItem.Rows.Count > 0)
                        {
                            // Item 정보 저장
                            strErrRtn = clsItem.mfSaveINSProcInspectItem(dtItem, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                            //결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                //trans.Rollback();
                                break;
                            }
                        }

                        // 검사결과 저장
                        if (dsResult.Tables.Count > 0)
                        {
                            // 계수
                            if (dsResult.Tables["Count"].Rows.Count > 0)
                            {
                                ProcInspectResultCount clsCount = new ProcInspectResultCount();
                                strErrRtn = clsCount.mfSaveINSProcResultCount(dsResult.Tables["Count"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }
                            // 계량
                            if (dsResult.Tables["Measure"].Rows.Count > 0)
                            {
                                ProcInspectResultMeasure clsMeasure = new ProcInspectResultMeasure();
                                strErrRtn = clsMeasure.mfSaveINSProcResultMeasure(dsResult.Tables["Measure"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }
                            // OkNg
                            if (dsResult.Tables["OkNg"].Rows.Count > 0)
                            {
                                ProcInspectResultOkNg clsOkNg = new ProcInspectResultOkNg();
                                strErrRtn = clsOkNg.mfSaveINSProcResultOkNg(dsResult.Tables["OkNg"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }

                            // 설명
                            if (dsResult.Tables["Desc"].Rows.Count > 0)
                            {
                                ProcInspectResultDesc clsDesc = new ProcInspectResultDesc();
                                strErrRtn = clsDesc.mfSaveINSProcResultDesc(dsResult.Tables["Desc"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }

                            // 선택
                            if (dsResult.Tables["Select"].Rows.Count > 0)
                            {
                                ProcInspectResultSelect clsSelect = new ProcInspectResultSelect();
                                strErrRtn = clsSelect.mfSaveINSProcResultSelect(dsResult.Tables["Select"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                    //trans.Rollback();
                                    break;
                                }
                            }
                        }
                        // 불량정보 저장
                        if (dtFault.Rows.Count > 0)
                        {
                            ProcInspectResultFault clsFault = new ProcInspectResultFault();
                            strErrRtn = clsFault.mfSaveINSProcInspectResultFault_S(dtFault, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                            //결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                string strDErrRtn = mfDeleteINSProcInspectReqALL(dtSave.Rows[i]["PlantCode"].ToString(), strReqNo, strReqSeq);
                                //trans.Rollback();
                                break;
                            }
                        }

                        // 작성완료시 순회검사모니터링 테이블에 저장
                        if (dtLot.Rows[0]["CompleteFlag"].ToString().Equals("T"))
                        {
                            if (dtCycle.Rows.Count > 0)
                            {
                                Monitoring clsMot = new Monitoring();
                                strErrRtn = clsMot.mfSaveCycleMonitoring(dtCycle, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                                //결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    //trans.Rollback();
                                    break;
                                }
                            }
                        }

                        //trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 공정검사등록 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcInspectReqALL(string strPlantCode, string strReqNo, string strReqSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                // 디비연결
                sql.mfConnect();

                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcINspectReqALL", dtParam);

                //결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectReqLot")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectReqLot : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ProductCode", typeof(string));
                dtRtn.Columns.Add("EquipCode", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("ProcInspectType", typeof(string));
                dtRtn.Columns.Add("ProcInspectPattern", typeof(string));
                //dtRtn.Columns.Add("LotReqSeq", typeof(string));
                //dtRtn.Columns.Add("LotMonthCount", typeof(decimal));
                //dtRtn.Columns.Add("LotYearSize", typeof(decimal));
                //dtRtn.Columns.Add("SampleSize", typeof(decimal));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("LotSize", typeof(decimal));
                dtRtn.Columns.Add("InspectUserID", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("InspectTime", typeof(string));
                dtRtn.Columns.Add("PassFailFlag", typeof(string));
                dtRtn.Columns.Add("CompleteFlag", typeof(string));
                dtRtn.Columns.Add("StackSeq", typeof(string));
                dtRtn.Columns.Add("LotState", typeof(string));
                dtRtn.Columns.Add("HoldState", typeof(string));
                dtRtn.Columns.Add("WorkUserID", typeof(string));
                dtRtn.Columns.Add("WorkProcessCode", typeof(string));
                dtRtn.Columns.Add("NowProcessCode", typeof(string));
                dtRtn.Columns.Add("LossCheckFlag", typeof(string));
                dtRtn.Columns.Add("LossQty", typeof(Int32));
                //dtRtn.Columns.Add("OUTSOURCINGVENDOR", typeof(string));
                dtRtn.Columns.Add("PCB_LAYOUT", typeof(string));
                dtRtn.Columns.Add("WaferYN", typeof(string));
                dtRtn.Columns.Add("HANDLER", typeof(string));
                dtRtn.Columns.Add("PROGRAM", typeof(string));
                dtRtn.Columns.Add("MESTrackInTFlag", typeof(string));
                dtRtn.Columns.Add("MESTrackInTDate", typeof(string));

                dtRtn.Columns.Add("FileName", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MES 인터페이스 성공시 저장에 필요한 데이터 테이블 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_MESTrackIn()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("FormName", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MES TrackOUT용 DataTable 컬럼 설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_MESTrackOUT()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("FormName", typeof(string));
                dtRtn.Columns.Add("FACTORYID", typeof(string));
                dtRtn.Columns.Add("USERID", typeof(string));
                dtRtn.Columns.Add("LOTID", typeof(string));
                dtRtn.Columns.Add("EQPID", typeof(string));
                dtRtn.Columns.Add("RECIPEID", typeof(string));
                dtRtn.Columns.Add("AREAID", typeof(string));
                dtRtn.Columns.Add("SPLITFLAG", typeof(string));
                dtRtn.Columns.Add("SCRAPFLAG", typeof(string));
                dtRtn.Columns.Add("REPAIRFLAG", typeof(string));
                dtRtn.Columns.Add("RWFLAG", typeof(string));
                dtRtn.Columns.Add("CONSUMEFLAG", typeof(string));
                dtRtn.Columns.Add("COMMENT", typeof(string));
                dtRtn.Columns.Add("QCFLAG", typeof(string));
                dtRtn.Columns.Add("SAMPLECOUNT", typeof(string));
                dtRtn.Columns.Add("QUALEFLAG", typeof(string));
                dtRtn.Columns.Add("REASONQTY", typeof(string));
                dtRtn.Columns.Add("REASONCODE", typeof(string));
                dtRtn.Columns.Add("CAUSEEQPID", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사등록 Lot정보 조회 Method
        /// </summary>
        /// <param name="strPlangCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqLot(string strPlangCode, string strReqNo, string strReqSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlangCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqLot", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사등록 Lot 정보 저장
        /// </summary>
        /// <param name="dtLot">Lot 정보 저장된 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot(DataTable dtLot, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans
                                                , string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["EquipCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcInspectType", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ProcInspectType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcInspectPattern", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ProcInspectPattern"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_dblLotSize", ParameterDirection.Input, SqlDbType.Decimal, dtLot.Rows[i]["LotSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTime", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPassFailFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PassFailFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["StackSeq"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotState", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["LotState"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strHoldState", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["HoldState"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["WorkProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strNowPRocessCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["NowProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLossCheckFlag", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["LossCheckFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intLossQty", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["LossQty"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_strOUTSOURCINGVENDOR", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["OUTSOURCINGVENDOR"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectReqLot", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// MES I/F TrackIn 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFormName">화면ID</param>
        /// <param name="dtLotList">TrackIN 시 필요정보 담긴 데이터 테이블</param>
        /// <param name="strUserID">검사자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcINspectReqLot_MESTrackIn(string strPlantCode, string strFormName, DataTable dtLotList, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKIN4QC_REQ(strFormName, strPlantCode, strUserID, dtLotList, "Y", strUserIP);

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                string strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// MES I/F 성공시 TrackInTFlag 업데이트 메소드
        /// </summary>
        /// <param name="dtStdInfo">Update에 필요한 PK저장된 데이터테이블</param>
        /// <param name="dtLotList">MES I/F시 전달할 LotList</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcINspectReqLot_MESTrackIn(DataTable dtStdInfo, DataTable dtLotList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04");  // LiveServer
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");    // TestServer
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKIN4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString(), dtStdInfo.Rows[0]["PlantCode"].ToString(), strUserID, dtLotList, "Y", strUserIP);

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 디비연결
                    sql.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtStdInfo.Rows[0]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqLot_MESTrackIn", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES I/F TrackOut 처리 메소드
        /// </summary>
        /// <param name="dtStdInfo">I/F 와 성공시 DB업데이트에 필요한 정보가 담긴 데이터 테이블</param>
        /// <param name="dtScrapList">ScrapList 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_MESTrackOUT(DataTable dtStdInfo, DataTable dtScrapList, DataTable dtFaultInfo, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S04");      //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S07");        //Test Server
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKOUT4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString(),
                                                                    dtStdInfo.Rows[0]["FACTORYID"].ToString(),
                                                                    dtStdInfo.Rows[0]["USERID"].ToString(),
                                                                    dtStdInfo.Rows[0]["LOTID"].ToString(),
                                                                    dtStdInfo.Rows[0]["EQPID"].ToString(),
                                                                    dtStdInfo.Rows[0]["RECIPEID"].ToString(),
                                                                    dtStdInfo.Rows[0]["AREAID"].ToString(),
                                                                    dtStdInfo.Rows[0]["SPLITFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["SCRAPFLAG"].ToString(),
                                                                    dtScrapList,
                                                                    dtStdInfo.Rows[0]["REPAIRFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["RWFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["CONSUMEFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["COMMENT"].ToString(),
                                                                    dtStdInfo.Rows[0]["QCFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["SAMPLECOUNT"].ToString(),
                                                                    dtStdInfo.Rows[0]["QUALEFLAG"].ToString(),
                                                                    dtFaultInfo,
                                                                    strUserIP);

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 디비연결
                    sql.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["FACTORYID"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtStdInfo.Rows[0]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqLot_MESTrackOut", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES I/F TrackOut 처리 메소드
        /// </summary>
        /// <param name="dtStdInfo">I/F 와 성공시 DB업데이트에 필요한 정보가 담긴 데이터 테이블</param>
        /// <param name="dtScrapList">ScrapList 데이터 테이블</param>
        /// <param name="dtInspectItemList">검사항목데이터테이블</param>
        /// <param name="dtFaultInfo">불량정보데이터테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_MESTrackOUT(DataTable dtStdInfo, DataTable dtScrapList, DataTable dtInspectItemList, DataTable dtFaultInfo, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S04");      //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S07");        //Test Server
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKOUT4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString(),
                                                                    dtStdInfo.Rows[0]["FACTORYID"].ToString(),
                                                                    dtStdInfo.Rows[0]["USERID"].ToString(),
                                                                    dtStdInfo.Rows[0]["LOTID"].ToString(),
                                                                    dtStdInfo.Rows[0]["EQPID"].ToString(),
                                                                    dtStdInfo.Rows[0]["RECIPEID"].ToString(),
                                                                    dtStdInfo.Rows[0]["AREAID"].ToString(),
                                                                    dtStdInfo.Rows[0]["SPLITFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["SCRAPFLAG"].ToString(),
                                                                    dtScrapList,
                                                                    dtStdInfo.Rows[0]["REPAIRFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["RWFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["CONSUMEFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["COMMENT"].ToString(),
                                                                    dtStdInfo.Rows[0]["QCFLAG"].ToString(),
                                                                    dtInspectItemList,
                                                                    dtFaultInfo,
                                                                    strUserIP);

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 디비연결
                    sql.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["FACTORYID"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtStdInfo.Rows[0]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqLot_MESTrackOut", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES I/F TrackOut 처리 메소드
        /// </summary>
        /// <param name="dtStdInfo">I/F 와 성공시 DB업데이트에 필요한 정보가 담긴 데이터 테이블</param>
        /// <param name="dtScrapList">ScrapList 데이터 테이블</param>
        /// <param name="dtInspectItemList">검사항목데이터테이블</param>
        /// <param name="dtFaultInfo">불량정보데이터테이블</param>
        /// <param name="strTKOutFlag">TrackOut여부</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_MESTrackOUT(DataTable dtStdInfo, DataTable dtScrapList, DataTable dtInspectItemList, DataTable dtFaultInfo, string strTKOutFlag,string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S04");      //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), "S07");        //Test Server
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["FACTORYID"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKOUT4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString(),
                                                                    dtStdInfo.Rows[0]["FACTORYID"].ToString(),
                                                                    dtStdInfo.Rows[0]["USERID"].ToString(),
                                                                    dtStdInfo.Rows[0]["LOTID"].ToString(),
                                                                    dtStdInfo.Rows[0]["EQPID"].ToString(),
                                                                    dtStdInfo.Rows[0]["RECIPEID"].ToString(),
                                                                    dtStdInfo.Rows[0]["AREAID"].ToString(),
                                                                    dtStdInfo.Rows[0]["SPLITFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["SCRAPFLAG"].ToString(),
                                                                    dtScrapList,
                                                                    dtStdInfo.Rows[0]["REPAIRFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["RWFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["CONSUMEFLAG"].ToString(),
                                                                    dtStdInfo.Rows[0]["COMMENT"].ToString(),
                                                                    dtStdInfo.Rows[0]["QCFLAG"].ToString(),
                                                                    dtInspectItemList,
                                                                    dtFaultInfo,
                                                                    strTKOutFlag,
                                                                    strUserIP);

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 디비연결
                    sql.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["FACTORYID"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtStdInfo.Rows[0]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqLot_MESTrackOut", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// MES I/F Hold 처리 메소드
        /// </summary>
        /// <param name="strFormName">폼이름</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strComment">비고</param>
        /// <param name="strHoldCode">Hold코드</param>
        /// <param name="dtLotList">Lot 리스트</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="dtStdInfo">MES 성공시 DB저장에 필요한 정보가 담긴 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_MESHold(string strFormName, string strUserID, string strComment, string strHoldCode
                                                        , DataTable dtLotList, string strUserIP, DataTable dtStdInfo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04");      //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");        //Test Server
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_HOLD4QC_REQ(strFormName, strUserID, strComment, strHoldCode, dtLotList, strUserIP);

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    // 디비연결
                    sql.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtStdInfo.Rows[0]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqLot_MESHold", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }
                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES SPCN ALARM 처리 메소드
        /// </summary>
        /// <param name="strFormName">화면명</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="dtStdInfo">MES 성공시 DB저장에 필요한 정보가 담긴 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_MESSPCN(string strFormName, string strUserID, string strEquipCode, string strUserIP, DataTable dtStdInfo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04");      //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");        //Test Server
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                clsTibrv.SPCN_ALARM(strFormName, strUserID, "*", "*", strEquipCode, "N", strUserIP);

                sql.mfConnect();
                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqNo"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtStdInfo.Rows[0]["ReqLotSeq"].ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqLot_MESSPCN", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                else
                {
                    trans.Rollback();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 MES Release 메소드
        /// </summary>
        /// <param name="dtStdInfo">Mes Flag저장에 필요한 기본정보가 담긴 데이터 테이블</param>
        /// <param name="dtLotList">LotNo/HoldCode 정보가 저장된 데이터 테이블</param>
        /// <param name="strInspectUserID">검사자ID</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_MESRelease(DataTable dtStdInfo, DataTable dtLotList, string strInspectUserID, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04");      //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");        //Test Server
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), mcr.MesCode);

                //QCNList MES전송 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtMES = clsTibrv.LOT_RELEASE4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString()
                                                            , strInspectUserID
                                                            , ""
                                                            , dtLotList
                                                            , strUserIP);

                // MES I/F 성공시
                if (dtMES.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    sql.mfConnect();
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtStdInfo.Rows[0]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqLot_MESRelease", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                        trans.Commit();
                    else
                        trans.Rollback();
                }
                ErrRtn.InterfaceResultCode = dtMES.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtMES.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정검사등록 Lot 정보 저장
        /// </summary>
        /// <param name="dtLot">Lot 정보 저장된 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_PSTS(DataTable dtLot, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans
                                                , string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcInspectType", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ProcInspectType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcInspectPattern", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ProcInspectPattern"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_dblLotSize", ParameterDirection.Input, SqlDbType.Decimal, dtLot.Rows[i]["LotSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTime", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPassFailFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PassFailFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["StackSeq"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotState", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["LotState"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strHoldState", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["HoldState"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["WorkProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strNowPRocessCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["NowProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLossCheckFlag", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["LossCheckFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intLossQty", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["LossQty"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_strOUTSOURCINGVENDOR", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["OUTSOURCINGVENDOR"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strPCB_LAYOUT", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["PCB_LAYOUT"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strWaferYN", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["WaferYN"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strHANDLER", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["HANDLER"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPROGRAM", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["PROGRAM"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strMESTrackInTFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["MESTrackInTFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMESTrackInTDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["MESTrackInTDate"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["FileName"].ToString(), 200);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectReqLot_PSTS", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// WAFER 수입검사여부 MES 전송결과 저장
        /// </summary>
        /// <param name="strFormName">화면ID</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="strInspectUserID">검사자ID</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strBGFlag">B/G여부</param>
        /// <param name="strComment">비고</param>
        /// <param name="strUserID">로그인사용자ID</param>
        /// <param name="strUserIP">로그인사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_LOT_WAFERBG4QC_REQ(string strFormName, string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq
                                                            , string strInspectUserID, string strLotNo, string strBGFlag, string strComment
                                                            , string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                // 수입검사 Wafer여부 MES전송 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtMES = clsTibrv.LOT_WAFERBG4QC_REQ(strFormName, strPlantCode, strInspectUserID, strLotNo, strBGFlag, strComment, strUserID, strUserIP);

                // MES I/F 성공시
                if (dtMES.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    sql.mfConnect();

                    SqlTransaction trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectReqLot_LOT_WAFERBG4QC_REQ", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                        trans.Commit();
                    else
                        trans.Rollback();
                }
                ErrRtn.InterfaceResultCode = dtMES.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtMES.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// SBL_PASS 여부 확인 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReq_Check_SBLPASS(string strPlantCode, string strProductCode, string strLotNo)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReq_Check_SBLPASS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// LotNo에 대한 양산품, 개발품 구분 2012.10.16 추가
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>        
        /// <param name="strLotNo">LotNo</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReq_Check_MP(string strPlantCode, string strLotNo)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);                
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReq_Check_MP", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정검사등록 Lot정보 동일한 LotNo+작업공정+설비 검색
        /// </summary>
        /// <param name="strPlangCode">공장코드</param>
        /// <param name="strWorkProcessCode">작업공정</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqLot_Check(string strPlangCode, string strWorkProcessCode, string strEquipCode, string strLotNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlangCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkProcess", ParameterDirection.Input, SqlDbType.VarChar, strWorkProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqLot_Check", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectReqItem")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectReqItem : ServicedComponent
    {

        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("ProcessCode", typeof(string));
                //dtRtn.Columns.Add("ProcessSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectGroupCode", typeof(string));
                dtRtn.Columns.Add("InspectTypeCode", typeof(string));
                dtRtn.Columns.Add("InspectItemCode", typeof(string));
                dtRtn.Columns.Add("InspectIngFlag", typeof(string));
                dtRtn.Columns.Add("StackSeq", typeof(string));
                dtRtn.Columns.Add("Generation", typeof(string));
                //dtRtn.Columns.Add("ProcessInspectFlag", typeof(string));
                //dtRtn.Columns.Add("ProductItemFlag", typeof(string));
                //dtRtn.Columns.Add("QualityItemFlag", typeof(string));
                dtRtn.Columns.Add("InspectCondition", typeof(string));
                dtRtn.Columns.Add("Method", typeof(string));
                //dtRtn.Columns.Add("SpecDesc", typeof(string));
                //dtRtn.Columns.Add("MeasureToolCode", typeof(string));
                dtRtn.Columns.Add("UpperSpec", typeof(decimal));
                dtRtn.Columns.Add("LowerSpec", typeof(decimal));
                dtRtn.Columns.Add("SpecRange", typeof(string));
                dtRtn.Columns.Add("FaultQty", typeof(decimal));
                //dtRtn.Columns.Add("SampleSize", typeof(decimal));
                dtRtn.Columns.Add("ProcessSampleSize", typeof(decimal));
                //dtRtn.Columns.Add("ProductItemSampleSize", typeof(decimal));
                //dtRtn.Columns.Add("QualityItemSampleSize", typeof(decimal));
                dtRtn.Columns.Add("UnitCode", typeof(string));
                //dtRtn.Columns.Add("InspectPeriod", typeof(string));
                //dtRtn.Columns.Add("PeriodUnitCode", typeof(string));
                //dtRtn.Columns.Add("CompareFlag", typeof(string));
                dtRtn.Columns.Add("DataType", typeof(string));
                //dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(string));
                dtRtn.Columns.Add("Mean", typeof(decimal));
                dtRtn.Columns.Add("StdDev", typeof(decimal));
                dtRtn.Columns.Add("MaxValue", typeof(decimal));
                dtRtn.Columns.Add("MinValue", typeof(decimal));
                dtRtn.Columns.Add("DataRange", typeof(decimal));
                dtRtn.Columns.Add("Cp", typeof(decimal));
                dtRtn.Columns.Add("Cpk", typeof(decimal));
                dtRtn.Columns.Add("OCPCount", typeof(Int32));
                dtRtn.Columns.Add("UpperRunCount", typeof(Int32));
                dtRtn.Columns.Add("LowerRunCount", typeof(Int32));
                dtRtn.Columns.Add("UpperTrendCount", typeof(Int32));
                dtRtn.Columns.Add("LowerTrendCount", typeof(Int32));
                //dtRtn.Columns.Add("CycleCount", typeof(Int32));
                dtRtn.Columns.Add("LCL", typeof(decimal));
                dtRtn.Columns.Add("CL", typeof(decimal));
                dtRtn.Columns.Add("UCL", typeof(decimal));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// LotNo 입력시 공정검사규격서에서 Item 정보가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        //public DataTable mfReadINSProcInspectItem_Init(string strPlantCode, string strProcessCode, string strProductCode, string strLang)
        public DataTable mfReadINSProcInspectItem_Init(string strPlantCode, string strProcessCode, string strProductCode, string strStackSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                //sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                //sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReq_Init", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 데이터 유형 선택일시 콤보박스 설정하기 위한 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectItem_DataTypeSelect(string strPlantCode, string strInspectItemCode, string strProductCode, string strProcessCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParma = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParma, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParma, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParma, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParma, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParma, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqItem_DataTypeSelect", dtParma);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사등록 Item 정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqItem(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqItem", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사등록 Sampling 그리드 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqItem_Sampling(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqItem_Sampling", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 불량유형 그리드 조회용 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqItem_Fault(string strPlantCode, string strReqNo, string strReqSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqItem_Fault", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 검사항목별 평균값, 범위값 조회 검사일기준(From~To)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strPackage">패키지</param>
        /// <param name="strStackSeq">차수</param>
        /// <param name="strGeneration">세대</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strInspectDate">검사일(From)</param>
        /// <param name="strInspectTime">검사일(To)</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqItem_Mean_Range_FromToDate(string strPlantCode, string strCustomerCode, string strPackage, string strStackSeq
                                                                , string strGeneration, string strProcessCode, string strEquipCode, string strFromDate, string strToDate)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 20);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqItem_Mean_Range_FromToDate", dtParam);

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 검사항목별 평균값, 범위값 조회 검사일기준(From~To)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strPackage">패키지</param>
        /// <param name="strStackSeq">차수</param>
        /// <param name="strGeneration">세대</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strInspectDate">검사일(From)</param>
        /// <param name="strInspectTime">검사일(To)</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectReqItem_Mean_Range_Top30(string strPlantCode, string strCustomerCode, string strPackage, string strStackSeq
                                                                , string strGeneration, string strProcessCode, string strEquipCode
                                                                , string strInspectDate, string strInspectTime, string strInspectItemCode)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, strInspectDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectTime", ParameterDirection.Input, SqlDbType.VarChar, strInspectTime, 8);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectReqItem_Mean_Range_Top30", dtParam);

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 Item 정보 저장 메소드
        /// </summary>
        /// <param name="dtItem">Item 정보 저장된 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectItem(DataTable dtItem, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtItem.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["ProcessCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_intProcessSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ProcessSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectItemCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectIngFlag", ParameterDirection.Input, SqlDbType.Char, dtItem.Rows[i]["InspectIngFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["StackSeq"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["Generation"].ToString(), 40);
                    //sql.mfAddParamDataRow(dtParam, "@i_strProcessInspectFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["ProcessInspectFlag"].ToString(), 1);
                    //sql.mfAddParamDataRow(dtParam, "@i_strProductItemFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["ProductItemFlag"].ToString(), 1);
                    //sql.mfAddParamDataRow(dtParam, "@i_strQualityItemFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["QualityItemFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectCondition", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["InspectCondition"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMethod", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["Method"].ToString(), 50);
                    //sql.mfAddParamDataRow(dtParam, "@i_strSpecDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["SpecDesc"].ToString(), 50);
                    //sql.mfAddParamDataRow(dtParam, "@I_strMesureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["MeasureToolCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_dblUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["UpperSpec"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["LowerSpec"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["SpecRange"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["FaultQty"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_dblSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["SampleSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblProcessSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["ProcessSampleSize"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_dblProductItemSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["ProductItemSampleSize"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_dblQualityItemSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["QualityItemSampleSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["UnitCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strInspectPeriod", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectPeriod"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strPeriodUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["PeriodUnitCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strCompareFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["CompareFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["DataType"].ToString(), 2);
                    //sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["EtcDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_dblMean", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Mean"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblStdDev", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["StdDev"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMaxValue", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["MaxValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMinValue", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["MinValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblDataRange", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["DataRange"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCp", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Cp"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCpk", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Cpk"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intOCPCount", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["OCPCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intUpperRunCount", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["UpperRunCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intLowerRunCount", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["LowerRunCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intUpperTrendCount", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["UpperTrendCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intLowerTrendCount", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["LowerTrendCount"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_intCycleCount", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["CycleCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblLCL", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["LCL"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCL", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["CL"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblUCL", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["UCL"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectReqItem", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 공정검사 Item 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcInspectItem(string strPlantCode, string strReqNo, string strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();

                trans = sqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSProcINspectReqItem", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                else
                {
                    trans.Rollback();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 공정검사 조회 RowData수정용 저장 메소드(임시)
        /// </summary>
        /// <param name="dtItem">Item데이터 테이블</param>
        /// <param name="dsResult">결과값 DataSet</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectItem(DataTable dtItem, DataSet dsResult, string strUserID, string strUserIP, string strReqNo, string strReqSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                trans.Commit();

                strErrRtn = mfSaveINSProcInspectItem(dtItem, strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                //결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }

                // 검사결과 저장
                if (dsResult.Tables.Count > 0)
                {
                    // 계수
                    if (dsResult.Tables["Count"].Rows.Count > 0)
                    {
                        ProcInspectResultCount clsCount = new ProcInspectResultCount();
                        strErrRtn = clsCount.mfSaveINSProcResultCount(dsResult.Tables["Count"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            return strErrRtn;
                        }
                    }
                    // 계량
                    if (dsResult.Tables["Measure"].Rows.Count > 0)
                    {
                        ProcInspectResultMeasure clsMeasure = new ProcInspectResultMeasure();
                        strErrRtn = clsMeasure.mfSaveINSProcResultMeasure(dsResult.Tables["Measure"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            return strErrRtn;
                        }
                    }
                    // OkNg
                    if (dsResult.Tables["OkNg"].Rows.Count > 0)
                    {
                        ProcInspectResultOkNg clsOkNg = new ProcInspectResultOkNg();
                        strErrRtn = clsOkNg.mfSaveINSProcResultOkNg(dsResult.Tables["OkNg"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            return strErrRtn;
                        }
                    }

                    // 설명
                    if (dsResult.Tables["Desc"].Rows.Count > 0)
                    {
                        ProcInspectResultDesc clsDesc = new ProcInspectResultDesc();
                        strErrRtn = clsDesc.mfSaveINSProcResultDesc(dsResult.Tables["Desc"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            return strErrRtn;
                        }
                    }

                    // 선택
                    if (dsResult.Tables["Select"].Rows.Count > 0)
                    {
                        ProcInspectResultSelect clsSelect = new ProcInspectResultSelect();
                        strErrRtn = clsSelect.mfSaveINSProcResultSelect(dsResult.Tables["Select"], strUserID, strUserIP, sql.SqlCon, trans, strReqNo, strReqSeq);

                        //결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            return strErrRtn;
                        }
                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectResultCount")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectResultCount : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable("Count");
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(decimal));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveINSProcResultCount(DataTable dtCount, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtCount.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCount.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtCount.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtCount.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtCount.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtCount.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectResultCount", dtParam);

                    //결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectResultDesc")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectResultDesc : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable("Desc");
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveINSProcResultDesc(DataTable dtDesc, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtDesc.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDesc.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtDesc.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDesc.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtDesc.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.NVarChar, dtDesc.Rows[i]["InspectValue"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectResultDesc", dtParam);

                    //결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectResultMeasure")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectResultMeasure : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable("Measure");
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(decimal));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveINSProcResultMeasure(DataTable dtMeasure, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtMeasure.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMeasure.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtMeasure.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtMeasure.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtMeasure.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtMeasure.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectResultMeasure", dtParam);

                    //결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 계량형 데이터 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="intReqItemSeq">Item순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectResultMeasure(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, int intReqItemSeq, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                // SET Parameter Data
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectResultMeasure", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectResultOkNg")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectResultOkNg : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable("OkNg");
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveINSProcResultOkNg(DataTable dtOkNg, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtOkNg.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtOkNg.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtOkNg.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtOkNg.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtOkNg.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtOkNg.Rows[i]["InspectValue"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectResultOkNg", dtParam);

                    //결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectResultSelect")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectResultSelect : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable("Select");
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveINSProcResultSelect(DataTable dtSelect, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtSelect.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSelect.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSelect.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSelect.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSelect.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtSelect.Rows[i]["InspectValue"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectResultSelect", dtParam);

                    //결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcInspectResultFault")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcInspectResultFault : ServicedComponent
    {
        /// <summary>
        /// 데이터테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqInspectSeq", typeof(Int32));
                dtRtn.Columns.Add("FaultTypeCode", typeof(string));
                dtRtn.Columns.Add("QCNFlag", typeof(char));
                dtRtn.Columns.Add("ITRFlag", typeof(char));
                dtRtn.Columns.Add("InspectQty", typeof(decimal));
                dtRtn.Columns.Add("FaultQty", typeof(decimal));
                dtRtn.Columns.Add("UnitCode", typeof(string));
                dtRtn.Columns.Add("FilePath", typeof(string));
                dtRtn.Columns.Add("ExpectProcessCode", typeof(string));
                dtRtn.Columns.Add("CauseEquipCode", typeof(string));
                dtRtn.Columns.Add("WorkUserID", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 불량유형 검사결과 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="intReqItemSeq">Item순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectResultFault(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, int intReqItemSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectResultFault", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 헤더부터 저장하는경우 저장 메소드
        /// </summary>
        /// <param name="dtFault">불량정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectResultFault(DataTable dtFault, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                trans = sqlCon.BeginTransaction();

                // 기존정보 삭제
                strErrRtn = mfDeleteINSProcInspectResultFault(dtFault.Rows[0]["PlantCode"].ToString(), strReqNo, strReqSeq
                                                            , Convert.ToInt32(dtFault.Rows[0]["ReqLotSeq"])
                                                            , Convert.ToInt32(dtFault.Rows[0]["ReqItemSeq"]), sqlCon, trans);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqInspectSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqInspectSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["FaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["QCNFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["InspectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strExpectProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ExpectProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["CauseEquipCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIp", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectResultFault", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


               /// <summary>
        /// 불량정보만 저장하는경우 저장 메소드
        /// </summary>
        /// <param name="dtFault">불량정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectResultFault(DataTable dtFault, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                string strErrRtn = string.Empty;
                trans = sql.SqlCon.BeginTransaction();

                // 기존정보 삭제
                strErrRtn = mfDeleteINSProcInspectResultFault(dtFault.Rows[0]["PlantCode"].ToString(), dtFault.Rows[0]["ReqNo"].ToString(), dtFault.Rows[0]["ReqSeq"].ToString()
                                                            , Convert.ToInt32(dtFault.Rows[0]["ReqLotSeq"])
                                                            , Convert.ToInt32(dtFault.Rows[0]["ReqItemSeq"]), sql.SqlCon, trans);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqInspectSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqInspectSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["FaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["QCNFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["InspectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strExpectProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ExpectProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["CauseEquipCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIp", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectResultFault", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 헤더부터 저장하는경우 저장 메소드  (PSTS 전용)
        /// </summary>
        /// <param name="dtFault">불량정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectResultFault_S(DataTable dtFault, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                trans = sqlCon.BeginTransaction();

                // 기존정보 삭제
                strErrRtn = mfDeleteINSProcInspectResultFault(dtFault.Rows[0]["PlantCode"].ToString(), strReqNo, strReqSeq
                                                            , Convert.ToInt32(dtFault.Rows[0]["ReqLotSeq"])
                                                            , Convert.ToInt32(dtFault.Rows[0]["ReqItemSeq"]), sqlCon, trans);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqInspectSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqInspectSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["FaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["QCNFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strITRFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["ITRFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["InspectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strExpectProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ExpectProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["CauseEquipCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIp", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSProcInspectResultFault_S", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 불량정보만 저장하는경우 저장 메소드  (PSTS 전용)
        /// </summary>
        /// <param name="dtFault">불량정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectResultFault_S(DataTable dtFault, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                string strErrRtn = string.Empty;
                trans = sql.SqlCon.BeginTransaction();

                // 기존정보 삭제
                strErrRtn = mfDeleteINSProcInspectResultFault(dtFault.Rows[0]["PlantCode"].ToString(), dtFault.Rows[0]["ReqNo"].ToString(), dtFault.Rows[0]["ReqSeq"].ToString()
                                                            , Convert.ToInt32(dtFault.Rows[0]["ReqLotSeq"])
                                                            , Convert.ToInt32(dtFault.Rows[0]["ReqItemSeq"]), sql.SqlCon, trans);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqInspectSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqInspectSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["FaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["QCNFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strITRFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["ITRFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["InspectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strExpectProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ExpectProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["CauseEquipCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIp", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcInspectResultFault_S", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 불량유형 검사데이터 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="intReqItemSeq">Item순번</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcInspectResultFault(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, int intReqItemSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSProcInspectResultFault", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량유형 검사데이터 삭제 메소드(단독삭제)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="intReqItemSeq">Item순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSProcInspectResultFault(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, int intReqItemSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcInspectResultFault", dtParam);

                // 검사결과
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES I/F TrackOut 시 검사항목에 해당하는 불량유형 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot 순번</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectResultFault_MESTrackOut(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, string strInspectItemCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectResultFault_MESTrackOut", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 작성완료후 QCN 자동이동 여부 판단하는 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectResultFault_MoveQCN(string strPlantCode, string strReqNo, string strReqSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectResultFault_MoveQCN", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사에서 QCN 자동이동후 화면에 보여질 정보 조회 메소드(STS)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="intReqItemSeq">항목순번</param>
        /// <param name="intReqInspectSeq">검사결과순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectResultFault_InitQCN(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, int intReqItemSeq, int intReqInspectSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqInspectSeq", ParameterDirection.Input, SqlDbType.Int, intReqInspectSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectResultFault_InitQCN", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사에서 QCN 자동이동후 화면에 보여질 정보 조회 메소드(PSTS)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="intReqItemSeq">항목순번</param>
        /// <param name="intReqInspectSeq">검사결과순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectResultFault_InitQCN_PSTS(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, int intReqItemSeq, int intReqInspectSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqInspectSeq", ParameterDirection.Input, SqlDbType.Int, intReqInspectSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectResultFault_InitQCN_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 불량일시 불량인 검사항목에 대한 불량정보가 저장되었는지 확인하는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcInspectResultFault_InspectItem(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcInspectResultFault_InspectItem", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcMESInterface")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcMESInterface : ServicedComponent
    {
        /// <summary>
        /// LotInfo 정보 I/F 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <returns></returns>
        public DataTable mfRead_LOT_INFO_REQ(string strPlantCode, string strLotNo)
        {
            DataTable dtLotInfo = new DataTable();
            try
            {
                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                // I/F 메소드 호출
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                dtLotInfo = clsTibrv.LOT_INFO_REQ(strLotNo);

                return dtLotInfo;
            }
            catch (Exception ex)
            {
                dtLotInfo.Columns.Add("returncode", typeof(string));
                dtLotInfo.Columns.Add("returnmessage", typeof(string));
                DataRow _dr = dtLotInfo.NewRow();
                _dr["returncode"] = "-999";
                _dr["returnmessage"] = "Exception Error<br/>" + ex.Message;
                dtLotInfo.Rows.Add(_dr);
                return dtLotInfo;
            }
            finally
            {
                dtLotInfo.Dispose();
            }
        }

        /// <summary>
        /// 예상공정 선택시 설비정보 I/F
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <returns></returns>
        public DataTable mfRead_LOT_PROCESSED_EQP_REQ(string strPlantCode, string strLotNo, string strProcessCode)
        {
            DataTable dtEquipInfo = new DataTable();
            try
            {
                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                // I/F 메소드 호출
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                dtEquipInfo = clsTibrv.LOT_PROCESSED_EQP_REQ(strLotNo, strProcessCode);

                return dtEquipInfo;
            }
            catch (Exception ex)
            {
                dtEquipInfo.Columns.Add("returncode", typeof(string));
                dtEquipInfo.Columns.Add("returnmessage", typeof(string));
                DataRow _dr = dtEquipInfo.NewRow();
                _dr["returncode"] = "-999";
                _dr["returnmessage"] = "Exception Error<br/>" + ex.Message;
                dtEquipInfo.Rows.Add(_dr);
                return dtEquipInfo;
            }
            finally
            {
                dtEquipInfo.Dispose();
            }
        }

        /// <summary>
        /// AffectLot I/F
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <returns></returns>
        public DataTable mfRead_LOT_AFFECT_REQ(string strPlantCode, string strLotNo, string strProcessCode, string strEquipCode)
        {
            DataTable dtAffectLot = new DataTable();
            try
            {
                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                // I/F 메소드 호출
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                dtAffectLot = clsTibrv.LOT_AFFECT_REQ(strLotNo, strProcessCode,strEquipCode); //LOT_PROCESSED_EQP_REQ,LOT_AFFECT_REQ

                return dtAffectLot;
            }
            catch (Exception ex)
            {
                dtAffectLot.Columns.Add("returncode", typeof(string));
                dtAffectLot.Columns.Add("returnmessage", typeof(string));
                DataRow _dr = dtAffectLot.NewRow();
                _dr["returncode"] = "-999";
                _dr["returnmessage"] = "Exception Error<br/>" + ex.Message;
                dtAffectLot.Rows.Add(_dr);
                return dtAffectLot;
            }
            finally
            {
                dtAffectLot.Dispose();
            }
        }

        /// <summary>
        /// MES I/F TrackIn 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFormName">화면ID</param>
        /// <param name="dtLotList">TrackIN 시 필요정보 담긴 데이터 테이블</param>
        /// <param name="strUserID">검사자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string LOT_TKIN4QC_REQ(string strPlantCode, string strFormName, DataTable dtLotList, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                MESCodeReturn mcr = new MESCodeReturn();
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_TKIN4QC_REQ(strFormName, strPlantCode, strUserID, dtLotList, "Y", strUserIP);

                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                string strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcQCNFaultType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcQCNFaultType : ServicedComponent
    {
        /// <summary>
        /// 불량유형정보 
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetData()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));     //공장코드
                dtRtn.Columns.Add("QCNNo", typeof(string));         //QCN발행정보
                dtRtn.Columns.Add("Seq", typeof(string));           //순번
                dtRtn.Columns.Add("InspectFaultTypeCode", typeof(string));//불량유형
                dtRtn.Columns.Add("EtcDesc", typeof(string));       //비고

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// QCN 불량유형 정보 조회 
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strQCNNo">발행번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadProcQCNFaultType(string strPlantCode, string strQCNNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                //DB연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); //공장
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);         //QCN발행번호
                
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);            //사용언어
                //QCN불량유형정보 조회 SP 실행
                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcQCNFaultType", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// QCN불량유형정보 저장
        /// </summary>
        /// <param name="dtFault">불량정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveProcQCNFaultType(DataTable dtFault, string strQCNNo, string strUserID, string strUserIP, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    //if (i == 0)
                    //{
                    //    // 삭제전 기존데이터 삭제
                    //    strErrRtn = mfDeleteProcQCNFaultType(dtFault.Rows[i]["PlantCode"].ToString(), dtFault.Rows[i]["QCNNo"].ToString(), sqlcon, trans);
                    //    // 처리결과검사
                    //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //    //처리결과실패시 Rollback처리
                    //    if (ErrRtn.ErrNum != 0)
                    //        break;
                    //        //trans.Rollback();
                    //        //return strErrRtn;
                    //}


                    DataTable dtParam = sql.mfSetParamDataTable();
                    
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100); //ReturnValue
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10); //공장
                    //sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["QCNNo"].ToString(), 20);        //발행번호
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);        //발행번호
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["Seq"].ToString());                    //순번
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["InspectFaultTypeCode"].ToString(),10);//불량유형정보
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["EtcDesc"].ToString(),1000);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20); //사용자ID
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);//사용자IP

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000); //ErrorMessage

                    //QCN불량정보 저장 SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_INSProcQCNFaultType", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    
                }

                //처리검사가 모두 정상처리시 Commit
                //if(ErrRtn.ErrNum == 0)
                //    trans.Commit();
                
                //처리결과 Return
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// QCN불량유형정보 삭제
        /// </summary>
        ///<param name="strPlantCode">공장</param>
        ///<param name="strQCNNo">QCN발행번호</param>
        ///<param name="sqlcon">SqlConnection</param>
        ///<param name="trans">SqlTransaction</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteProcQCNFaultType(string strPlantCode, string strQCNNo, SqlConnection sqlcon,SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQCNNo", ParameterDirection.Input, SqlDbType.VarChar, strQCNNo, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcQCNFaultType", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


    }
}
