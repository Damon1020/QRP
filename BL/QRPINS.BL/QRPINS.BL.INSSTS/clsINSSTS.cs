﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질검사 관리                                         */
/* 모듈(분류)명 : 공정검사 관리                                         */
/* 프로그램ID   : clsINSSTS.cs                                          */
/* 프로그램명   : 공정 Low Yield 조회                                   */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2011-09-06                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                                                                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;


[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
              Authentication = AuthenticationOption.None,
             ImpersonationLevel = ImpersonationLevelOption.Impersonate)]



namespace QRPINS.BL.INSSTS
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("INSProcLowYield")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    //public class INSProcLowYield      //디버깅용
    public class INSProcLowYield : ServicedComponent    //실행용
    {

        private SqlConnection m_SqlConnDebug;

        public INSProcLowYield()
        {
        }

        public INSProcLowYield(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 공장 LowYield 데이터테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtINSProcLowYield = new DataTable();
            try
            {
                dtINSProcLowYield.Columns.Add("PlantCode", typeof(String));
                dtINSProcLowYield.Columns.Add("AriseProcessCode", typeof(String));
                dtINSProcLowYield.Columns.Add("AriseDate", typeof(String));
                dtINSProcLowYield.Columns.Add("AriseTime", typeof(String));
                //dtINSProcLowYield.Columns.Add("ProductCode", typeof(String));
                //dtINSProcLowYield.Columns.Add("LossName", typeof(String));
                //dtINSProcLowYield.Columns.Add("LossQty", typeof(Decimal));
                //dtINSProcLowYield.Columns.Add("LotQty", typeof(Decimal));
                //dtINSProcLowYield.Columns.Add("ProcessYield", typeof(Decimal));
                dtINSProcLowYield.Columns.Add("TechnAriseCause", typeof(String));
                dtINSProcLowYield.Columns.Add("TechnMeasureDesc", typeof(String));
                dtINSProcLowYield.Columns.Add("TechnRegistUserID", typeof(string));
                dtINSProcLowYield.Columns.Add("AriseCause", typeof(String));
                dtINSProcLowYield.Columns.Add("MeasureDesc", typeof(String));
                dtINSProcLowYield.Columns.Add("FilePath", typeof(String));
                dtINSProcLowYield.Columns.Add("RegistUserID", typeof(string));
                dtINSProcLowYield.Columns.Add("ReturnFlag", typeof(String));
                dtINSProcLowYield.Columns.Add("ReturnComment", typeof(String));
                dtINSProcLowYield.Columns.Add("ConfirmFlag", typeof(String));
                dtINSProcLowYield.Columns.Add("ConfirmDesc", typeof(String));
                dtINSProcLowYield.Columns.Add("LotReleaseFlag", typeof(String));
                dtINSProcLowYield.Columns.Add("ReleaseDesc", typeof(String));
                dtINSProcLowYield.Columns.Add("ScrapFlag", typeof(String));
                //dtINSProcLowYield.Columns.Add("WriteUserID", typeof(String));
                dtINSProcLowYield.Columns.Add("WriteUserID", typeof(String));
                //dtINSProcLowYield.Columns.Add("BinName", typeof(String));
                //dtINSProcLowYield.Columns.Add("LotNo", typeof(String));
                //dtINSProcLowYield.Columns.Add("EquipCode", typeof(String));
                //dtINSProcLowYield.Columns.Add("MESTFlag", typeof(String));
                //dtINSProcLowYield.Columns.Add("ReleaseComplete", typeof(String));
                dtINSProcLowYield.Columns.Add("ManFlag", typeof(string));
                dtINSProcLowYield.Columns.Add("MethodFlag", typeof(string));
                dtINSProcLowYield.Columns.Add("MaterialFlag", typeof(string));
                dtINSProcLowYield.Columns.Add("MachineFlag", typeof(string));
                dtINSProcLowYield.Columns.Add("EnvironmentFlag", typeof(string));

                dtINSProcLowYield.Columns.Add("ImputeProcess", typeof(string));
                dtINSProcLowYield.Columns.Add("ImputeEquipCode", typeof(string));
                dtINSProcLowYield.Columns.Add("ImputeUserID", typeof(string));

                return dtINSProcLowYield;
            }
            catch (System.Exception ex)
            {
                return dtINSProcLowYield;
                throw (ex);
            }
            finally
            {
                dtINSProcLowYield.Dispose();
            }
        }


        /// <summary>
        /// 공장 Low Yield 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strAriseDateFrom"></param>
        /// <param name="strAriseDateto"></param>
        /// <param name="strProductCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcLowYield(String strPlantCode, String strAriseDateFrom, String strAriseDateTo
                                                , String strProductCode, String strLotReleaseFlag
                                                , string strPackage, string strCustomerCode, string strLotNO, String strLang)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();    //실행용

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateTo", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotReleaseFlag", ParameterDirection.Input, SqlDbType.VarChar, strLotReleaseFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNO", ParameterDirection.Input, SqlDbType.VarChar, strLotNO, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcLowYield", dtParam);    //실행용
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSProcLowYield", dtParam);    //디버깅용
                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 공장 Low Yield 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAriseProcessCode">발생공정코드</param>
        /// <param name="strAriseDate">발생일</param>
        /// <param name="strAriseTime">발생시간</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcLowYield_Detail(string strPlantCode, string strAriseProcessCode, string strAriseDate, string strAriseTime, string strLang)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();    //실행용

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcLowYield_Detail", dtParam);//실행용
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSProcLowYieldForGrid", dtParam); //디버깅용

                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 공장 Low Yield 저장(업데이트)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strFormName">폼이름</param>
        /// <param name="dtLoss">dtLoss(불량코드SCRAPCODE, 불량수량SCRAPQTY)</param>
        /// <returns></returns>
        [AutoComplete(false)]
        //public string mfSaveINSProcLowYield(DataTable dt, string strUserID, string strUserIP, string strFormName, DataTable dtLoss, DataTable dtMES)
        public string mfSaveINSProcLowYield(DataTable dt, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();    //실행용

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strTechnAriseCause", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["TechnAriseCause"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strTechnMeasureDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["TechnMeasureDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strTechnRegistUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["TechnRegistUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseCause", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["AriseCause"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MeasureDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strRegistUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["RegistUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReturnFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ReturnFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReturnComment", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ReturnComment"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strConfirmFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ConfirmFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strConfirmDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ConfirmDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotReleaseFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LotReleaseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReleaseDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ReleaseDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strScrapFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ScrapFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strManFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["ManFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMethodFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["MethodFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["MaterialFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMachineFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["MachineFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEnvironmentFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["EnvironmentFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strImputeProcess", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ImputeProcess"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strImputeEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ImputeEquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strImputeUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ImputeUserID"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcLowYield", dtParam); //실행용
                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_INSProcLowYield", dtParam); //디버깅용

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    #region 기존서수 주석처리
                    /*
                    if (ErrRtn.ErrNum == 0)
                    {
                        if (dt.Rows[i]["LotReleaseFlag"].ToString().Equals("True") && dt.Rows[i]["MESTFlag"].ToString().Equals("F"))
                        {
                            //MES 서버 경로 가져오기
                            QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();

                            DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dt.Rows[i]["PlantCode"].ToString(), "S04");


                            //MES 공정 Low Yield List 전송 매서드 실행
                            QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);  //실행용
                            //QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv("1",dtSysAcce);  //디버깅용

                            DataTable dtProceeLowYield = clsTibrv.LOW_YIELD_RESULT_REQ(strFormName, dtMES.Rows[0]["LotNo"].ToString(), dtMES.Rows[0]["Process"].ToString()
                                                                                        , dtMES.Rows[0]["WorkUserID"].ToString(), dtMES.Rows[0]["EquipCode"].ToString()
                                                                                        , dtMES.Rows[0]["ScrapFlag"].ToString(), dtLoss, strUserIP);

                            if (dtProceeLowYield.Rows.Count > 0)
                            {
                                // MES전송이 성공하여 성공MES코드를 받아오면 테이블의 MESTFlag를 업데이트시킨다.
                                if (dtProceeLowYield.Rows[0][0].ToString().Equals("0"))
                                {
                                    //디비연결
                                    sql.mfConnect();    //실행용

                                    //트랜젝션 시작
                                    SqlTransaction transt = sql.SqlCon.BeginTransaction();  //실행용
                                    //SqlTransaction transt = m_SqlConnDebug.BeginTransaction(); //디버깅용

                                    //파라미터 정보 저장
                                    DataTable dtPtarmt = sql.mfSetParamDataTable();

                                    sql.mfAddParamDataRow(dtPtarmt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                                    sql.mfAddParamDataRow(dtPtarmt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);           //공장
                                    sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseProcessCode"].ToString(), 10); //발생공정
                                    sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseDate"].ToString(), 10);          //발생일
                                    sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseTime"].ToString(), 10);        //발생시간
                                    sql.mfAddParamDataRow(dtPtarmt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                                    sql.mfAddParamDataRow(dtPtarmt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                                    sql.mfAddParamDataRow(dtPtarmt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                                    //공정 LowYield List MESTFlag 업데이트 프로시저 실행
                                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSProcLowYieldMESTFlag", dtPtarmt);   //실행용
                                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, transt, "up_Update_INSProcLowYieldMESTFlag", dtPtarmt);   //디버깅용

                                    // Decoding //
                                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                    // 업데이트 성공시 커밋 실패시 롤백처리를 한다.
                                    if (ErrRtn.ErrNum != 0)
                                        transt.Rollback();
                                    else
                                        transt.Commit();


                                }

                                //성공이나 실패시 MES코드와 메세지를 EnCoding 한다.
                                ErrRtn.InterfaceResultCode = dtProceeLowYield.Rows[0][0].ToString();
                                ErrRtn.InterfaceResultMessage = dtProceeLowYield.Rows[0][1].ToString();

                                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                            }
                        }

                    }
                     */
                    #endregion
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        [AutoComplete(false)]
        public string mfSaveINSProcLowYield_MES_Result(DataTable dtMES, DataTable dtScrap, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;

                //MES 서버 경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtMES.Rows[0]["PlantCode"].ToString(), "S04");    //MES_LiveServer
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtMES.Rows[0]["PlantCode"].ToString(), "S07");    //MES_TestServer

                //MES 공정 Low Yield List 전송 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);  //실행용

                DataTable dtProceeLowYield = clsTibrv.LOW_YIELD_RESULT_REQ(dtMES.Rows[0]["FormName"].ToString()
                                                                        , dtMES.Rows[0]["LotNo"].ToString()
                                                                        , dtMES.Rows[0]["AriseProcessCode"].ToString()
                                                                        , dtMES.Rows[0]["WorkUserID"].ToString()
                                                                        , dtMES.Rows[0]["EquipCode"].ToString()
                                                                        , dtMES.Rows[0]["ScrapFlag"].ToString()
                                                                        , dtScrap
                                                                        , strUserIP);

                if (dtProceeLowYield.Rows.Count > 0)
                {
                    // MES전송이 성공하여 성공MES코드를 받아오면 테이블의 MESTFlag를 업데이트시킨다.
                    if (dtProceeLowYield.Rows[0][0].ToString().Equals("0"))
                    {
                        //디비연결
                        sql.mfConnect();    //실행용

                        //트랜젝션 시작
                        SqlTransaction transt = sql.SqlCon.BeginTransaction();  //실행용

                        //파라미터 정보 저장
                        DataTable dtPtarmt = sql.mfSetParamDataTable();

                        sql.mfAddParamDataRow(dtPtarmt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                        sql.mfAddParamDataRow(dtPtarmt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMES.Rows[0]["PlantCode"].ToString(), 10);           //공장
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtMES.Rows[0]["AriseProcessCode"].ToString(), 10); //발생공정
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtMES.Rows[0]["AriseDate"].ToString(), 10);          //발생일
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtMES.Rows[0]["AriseTime"].ToString(), 10);        //발생시간
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtPtarmt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //공정 LowYield List MESTFlag 업데이트 프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSProcLowYieldMESTFlag", dtPtarmt);   //실행용

                        // Decoding //
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 업데이트 성공시 커밋 실패시 롤백처리를 한다.
                        if (ErrRtn.ErrNum != 0)
                            transt.Rollback();
                        else
                            transt.Commit();
                    }

                    //성공이나 실패시 MES코드와 메세지를 EnCoding 한다.
                    ErrRtn.InterfaceResultCode = dtProceeLowYield.Rows[0][0].ToString();
                    ErrRtn.InterfaceResultMessage = dtProceeLowYield.Rows[0][1].ToString();
                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 저수율 MES Release 메소드
        /// </summary>
        /// <param name="dtStdInfo">Mes Flag저장에 필요한 기본정보가 담긴 데이터 테이블</param>
        /// <param name="dtLotList">LotNo/HoldCode 정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcInspectReqLot_MESRelease(DataTable dtStdInfo, DataTable dtLotList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04");    //MES_LiveServer
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");    //MES_TestServer

                //QCNList MES전송 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtMES = clsTibrv.LOT_RELEASE4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString()
                                                            , dtStdInfo.Rows[0]["WorkUserID"].ToString()
                                                            , dtStdInfo.Rows[0]["Comment"].ToString()
                                                            , dtLotList
                                                            , strUserIP);

                // MES I/F 성공시
                if (dtMES.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    sql.mfConnect();
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcLowYield_MESReleaseTFlag", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                        trans.Commit();
                    else
                        trans.Rollback();
                }

                ErrRtn.InterfaceResultCode = dtMES.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtMES.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 저장 테스트용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcLowYieldBtn(DataTable dt, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLossName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["LossName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_decLossQty", ParameterDirection.Input, SqlDbType.Decimal, dt.Rows[i]["LossQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decLotQty", ParameterDirection.Input, SqlDbType.Decimal, dt.Rows[i]["LotQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decProcessYield", ParameterDirection.Input, SqlDbType.Decimal, dt.Rows[i]["ProcessYield"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcLowYieldBtn", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();

                }
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 파일 업로드용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strAriseProcessCode"></param>
        /// <param name="strAriseDate"></param>
        /// <param name="strAriseTime"></param>
        /// <param name="strFileName"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcLowYieldFile(string strPlantCode, string strAriseProcessCode, string strAriseDate, string strAriseTime, string strFileName
                                                    , string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //db open
                sql.mfConnect();
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, strFileName, 1000);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시져 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcLowYiledFile", dtParam);

                //결과확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                    trans.Commit();

                return strErrRtn;

            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #region MES Message I/F
        /// <summary>
        /// MES Message I/F 를 통해 넘어온 저수율 발생정보 저장
        /// </summary>
        /// <param name="strPLANTNAME">PlantCode</param>
        /// <param name="strLOTID">LotNo</param>
        /// <param name="strOPERID">AriseProcessCode</param>
        /// <param name="strEQPID">AriseEquipCode</param>
        /// <param name="strPRODUCTSPECNAME">ProductCode</param>
        /// <param name="strPRODUCTQUANTITY">LotQty</param>
        /// <param name="strYIELD">ProcessYield</param>
        /// <param name="dtScrap">INSProcLowYieldD</param>
        /// <param name="strUserIP"></param>
        /// <param name="strUSERID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcLowYield_IF(string strPLANTNAME, string strISSUEDATE, string strLOTID, string strOPERID, string strEQPID,
                                               string strPRODUCTSPECNAME, string strPRODUCTQUANTITY, string strBINNAME, string strYIELD,
                                               DataTable dtScrap, string strUserIP, string strUSERID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string str = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터 저장

                // 1.저수율을 구하기 위해 Item정보의 SCRAPQTY 합계를 구한다.
                decimal dblScrapQty = 0.0m;
                for (int i = 0; i < dtScrap.Rows.Count; i++)
                {
                    dblScrapQty += Convert.ToDecimal(dtScrap.Rows[i]["SCRAPQTY"]);
                }

                //
                // 2. 저수율(strYIELD)  : 100 - (불량수량합계/검사수량*100)
                decimal dblYield = 0.0m;
                if (!strPRODUCTQUANTITY.Equals("0"))
                {
                    dblYield = 100 - (dblScrapQty / Convert.ToDecimal(strPRODUCTQUANTITY) * 100);

                    // 3. 소수점 4번째자리에서 반올림
                    dblYield = Math.Round(dblYield, 3, MidpointRounding.AwayFromZero);
                }
                   

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPLANTNAME", ParameterDirection.Input, SqlDbType.VarChar, strPLANTNAME, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strISSUEDATE", ParameterDirection.Input, SqlDbType.VarChar, strISSUEDATE, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLOTID", ParameterDirection.Input, SqlDbType.VarChar, strLOTID, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strOPERID", ParameterDirection.Input, SqlDbType.VarChar, strOPERID, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEQPID", ParameterDirection.Input, SqlDbType.VarChar, strEQPID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPRODUCTSPECNAME", ParameterDirection.Input, SqlDbType.VarChar, strPRODUCTSPECNAME, 20);
                sql.mfAddParamDataRow(dtParam, "@i_numPRODUCTQUANTITY", ParameterDirection.Input, SqlDbType.Decimal, strPRODUCTQUANTITY);
                sql.mfAddParamDataRow(dtParam, "@i_strBINNAME", ParameterDirection.Input, SqlDbType.VarChar, strBINNAME, 50);
                //sql.mfAddParamDataRow(dtParam, "@i_numYIELD", ParameterDirection.Input, SqlDbType.Decimal, strYIELD);
                sql.mfAddParamDataRow(dtParam, "@i_numYIELD", ParameterDirection.Input, SqlDbType.Decimal, dblYield.ToString());

                sql.mfAddParamDataRow(dtParam, "@i_strUSERID", ParameterDirection.Input, SqlDbType.VarChar, strUSERID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                sql.mfAddParamDataRow(dtParam, "@AriseDate", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                sql.mfAddParamDataRow(dtParam, "@AriseTime", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcLowYield_IF", dtParam);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                string strAriseDate = ErrRtn.mfGetReturnValue(0);
                string strAriseTime = ErrRtn.mfGetReturnValue(1);

                if (ErrRtn.ErrNum != 0 || strAriseDate.Equals(string.Empty) || strAriseTime.Equals(string.Empty))
                {
                    trans.Rollback();
                }
                else
                {
                    for (int i = 0; i < dtScrap.Rows.Count; i++)
                    {
                        DataTable dtParamd = sql.mfSetParamDataTable();

                        sql.mfAddParamDataRow(dtParamd, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                        sql.mfAddParamDataRow(dtParamd, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPLANTNAME, 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strOPERID, 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                        sql.mfAddParamDataRow(dtParamd, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);

                        sql.mfAddParamDataRow(dtParamd, "@i_strLossName", ParameterDirection.Input, SqlDbType.VarChar, dtScrap.Rows[i]["SCRAPCODE"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParamd, "@i_numLossQty", ParameterDirection.Input, SqlDbType.Decimal, dtScrap.Rows[i]["SCRAPQTY"].ToString());

                        sql.mfAddParamDataRow(dtParamd, "@i_strUSERID", ParameterDirection.Input, SqlDbType.VarChar, strUSERID, 20);
                        sql.mfAddParamDataRow(dtParamd, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                        sql.mfAddParamDataRow(dtParamd, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcLowYieldD_IF", dtParamd);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }

                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return str;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        #endregion
    }

    /// <summary>
    /// 공정 Low Yield 아이템 
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("INSProcLowYieldD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    //public class INSProcLowYieldD    //디버기용
    public class INSProcLowYieldD : ServicedComponent //실해용
    {
        private SqlConnection m_SqlConnDebug;

        public INSProcLowYieldD()
        {
        }

        public INSProcLowYieldD(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// Low Yield 상세저장
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtINSLowYieldD = new DataTable();
            try
            {
                dtINSLowYieldD.Columns.Add("PlantCode", typeof(String));
                dtINSLowYieldD.Columns.Add("AriseProcessCode", typeof(String));
                dtINSLowYieldD.Columns.Add("AriseDate", typeof(String));
                dtINSLowYieldD.Columns.Add("AriseTime", typeof(String));
                dtINSLowYieldD.Columns.Add("LossName", typeof(String));
                dtINSLowYieldD.Columns.Add("LossQty", typeof(Decimal));

                return dtINSLowYieldD;
            }
            catch (Exception ex)
            {
                return dtINSLowYieldD;
                throw (ex);
            }
            finally
            {
                dtINSLowYieldD.Dispose();
            }
        }

        /// <summary>
        /// Low Yield 상세 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strAriseProcessCode"></param>
        /// <param name="strAriseDate"></param>
        /// <param name="strAriseTime"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcLowYieldD(String strPlantCode, String strAriseProcessCode, String strAriseDate, String strAriseTime, String strLang)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtProceLowYieldD = new DataTable();
            try
            {
                sql.mfConnect();    //실행용

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtProceLowYieldD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcLowYieldD", dtParam); //실행용
                //dtProceLowYieldD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSProcLowYieldD", dtParam); //디버깅용

                return dtProceLowYieldD;

            }
            catch (Exception ex)
            {
                return dtProceLowYieldD;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtProceLowYieldD.Dispose();
            }
        }

        /// <summary>
        ///Low Yield 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSLowYieldD(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcLowYieldD", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Low  Yield 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelINSLowYieldD(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcLowYieldD", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


    }






    /// <summary>
    /// 원자재 특채정보
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialSpecial")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class INSMaterialSpecial : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public INSMaterialSpecial()
        {
        }

        public INSMaterialSpecial(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 원재자 특채정보 DataTable
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtMaterialSpecial = new DataTable();
            try
            {
                dtMaterialSpecial.Columns.Add("PlantCode", typeof(String));
                dtMaterialSpecial.Columns.Add("StdNumber", typeof(String));
                dtMaterialSpecial.Columns.Add("MaterialCode", typeof(String));
                dtMaterialSpecial.Columns.Add("MaterialSpec", typeof(String));
                dtMaterialSpecial.Columns.Add("VendorCode", typeof(String));
                dtMaterialSpecial.Columns.Add("VendorName", typeof(String));
                dtMaterialSpecial.Columns.Add("LotNo", typeof(String));
                dtMaterialSpecial.Columns.Add("GRNo", typeof(String));
                dtMaterialSpecial.Columns.Add("GRDate", typeof(String));
                dtMaterialSpecial.Columns.Add("WriteUserID", typeof(String));
                dtMaterialSpecial.Columns.Add("WriteDate", typeof(String));
                dtMaterialSpecial.Columns.Add("Qty", typeof(Decimal));
                dtMaterialSpecial.Columns.Add("EtcDesc", typeof(String));
                dtMaterialSpecial.Columns.Add("ReqObjectDesc", typeof(String));
                dtMaterialSpecial.Columns.Add("Result", typeof(String));
                dtMaterialSpecial.Columns.Add("InspectResult", typeof(String));
                dtMaterialSpecial.Columns.Add("MeetingDate1", typeof(String));
                dtMaterialSpecial.Columns.Add("MeetingPlace1", typeof(String));
                dtMaterialSpecial.Columns.Add("FileName1", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseUserID1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductUserID1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopUserID1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipUserID1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityUserID1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcUserID1", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseUserName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductUserName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopUserName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipUserName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityUserName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcUserName1", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseDeptName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductDeptName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopDeptName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipDeptName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityDeptName1", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcDeptName1", typeof(String));

                dtMaterialSpecial.Columns.Add("MRBDesc", typeof(String));
                dtMaterialSpecial.Columns.Add("MRBUserID", typeof(String));
                dtMaterialSpecial.Columns.Add("MRBUserName", typeof(String));
                dtMaterialSpecial.Columns.Add("MRBDeliveryDate", typeof(String));
                dtMaterialSpecial.Columns.Add("MRBEtcDesc", typeof(String));
                dtMaterialSpecial.Columns.Add("MeetingDate2", typeof(String));
                dtMaterialSpecial.Columns.Add("MeetingPlace2", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseUserID2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductUserID2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopUserID2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipUserID2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityUserID2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcUserID2", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseUserName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductUserName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopUserName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipUserName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityUserName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcUserName2", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseDeptName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductDeptName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopDeptName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipDeptName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityDeptName2", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcDeptName2", typeof(String));

                dtMaterialSpecial.Columns.Add("ResultDesc", typeof(String));
                dtMaterialSpecial.Columns.Add("ResultFileName", typeof(String));
                dtMaterialSpecial.Columns.Add("ResultUserID", typeof(String));
                dtMaterialSpecial.Columns.Add("ResultUserName", typeof(String));
                dtMaterialSpecial.Columns.Add("ResultDeliveryDate", typeof(String));
                dtMaterialSpecial.Columns.Add("ResultEtcDesc", typeof(String));

                dtMaterialSpecial.Columns.Add("AgreePurchaseUserID", typeof(String));
                dtMaterialSpecial.Columns.Add("AgreeProductUserID", typeof(String));
                dtMaterialSpecial.Columns.Add("AgreeDevelopUserID", typeof(String));
                dtMaterialSpecial.Columns.Add("AgreeEquipUserID", typeof(String));
                dtMaterialSpecial.Columns.Add("AgreeQualityUserID", typeof(String));

                dtMaterialSpecial.Columns.Add("AgreePurchaseUserName", typeof(String));
                dtMaterialSpecial.Columns.Add("AgreeProductUserName", typeof(String));
                dtMaterialSpecial.Columns.Add("AgreeDevelopUserName", typeof(String));
                dtMaterialSpecial.Columns.Add("AgreeEquipUserName", typeof(String));
                dtMaterialSpecial.Columns.Add("AgreeQualityUserName", typeof(String));

                dtMaterialSpecial.Columns.Add("AgreeDesc", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseUserID3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductUserID3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopUserID3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipUserID3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityUserID3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcUserID3", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseUserName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductUserName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopUserName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipUserName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityUserName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcUserName3", typeof(String));

                dtMaterialSpecial.Columns.Add("DistPurchaseDeptName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistProductDeptName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistDevelopDeptName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEquipDeptName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistQualityDeptName3", typeof(String));
                dtMaterialSpecial.Columns.Add("DistEtcDeptName3", typeof(String));

                dtMaterialSpecial.Columns.Add("CompleteFlag", typeof(String));
                dtMaterialSpecial.Columns.Add("MESTFlag", typeof(String));
                dtMaterialSpecial.Columns.Add("CompleteSate", typeof(String));
                dtMaterialSpecial.Columns.Add("GWTFlag", typeof(String));
                dtMaterialSpecial.Columns.Add("GWResultState", typeof(String));

                return dtMaterialSpecial;
            }
            catch (System.Exception ex)
            {
                return dtMaterialSpecial;
                throw (ex);
            }
            finally
            {
                dtMaterialSpecial.Dispose();
            }
        }


        /// <summary>
        /// 원자재 특채정보 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMaterialSpecial(String strPlantCode, String strVendorCode, String strMaterialCode, String strLotNo, String strWriteDateFrom
                                                , String strWriteDateTo, String strLang)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString);    //디버깅용
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect(); //실행용

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strWriteDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteDateTo", ParameterDirection.Input, SqlDbType.VarChar, strWriteDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialSpecial", dtParam);//실행용
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSMaterialSpecial", dtParam);//디버깅용

                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 원자재 특채 정보 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMaterialSpecial(DataTable dt, string strUserID, string strUserIP, string strFormName, DataTable dtWMS, DataTable dtShip)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString);    //디버깅용
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                sql.mfConnect();    //실행용
                SqlTransaction trans;

                string strRtnStdNumber = "";
                string strStdNum = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    #region Header
                    //작성이 완료된 상태면 저장을 안한다.(이미 DB안에 정보가 있기때문)
                    if (!dt.Rows[i]["CompleteSate"].ToString().Equals("T"))
                    {

                        trans = sql.SqlCon.BeginTransaction();  //실행용
                        //trans = m_SqlConnDebug.BeginTransaction();          //디버깅용
                        DataTable dtParam = sql.mfSetParamDataTable();
                        //파라미터 저장
                        #region Header Parameter
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["GRNo"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MaterialCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VendorCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["LotNo"].ToString(), 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_decQty", ParameterDirection.Input, SqlDbType.Decimal, dt.Rows[i]["Qty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqObjectDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ReqObjectDesc"].ToString(), 500);
                        sql.mfAddParamDataRow(dtParam, "@i_strResult", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Result"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectResult", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectResult"].ToString(), 2);
                        sql.mfAddParamDataRow(dtParam, "@i_strMeetingDate1", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MeetingDate1"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strMeetingPlace1", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MeetingPlace1"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strFileName1", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FileName1"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistPurchaseUserID1", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistPurchaseUserID1"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistProductUserID1", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistProductUserID1"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistDevelopUserID1", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistDevelopUserID1"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistEquipUserID1", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistEquipUserID1"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistQualityUserID1", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistQualityUserID1"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistEtcUserID1", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistEtcUserID1"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strMRBDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MRBDesc"].ToString(), 500);
                        sql.mfAddParamDataRow(dtParam, "@i_strMRBUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MRBUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strMRBDeliveryDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MRBDeliveryDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strMRBEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MRBEtcDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strMeetingDate2", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MeetingDate2"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strMeetingPlace2", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MeetingPlace2"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistPurchaseUserID2", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistPurchaseUserID2"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistProductUserID2", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistProductUserID2"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistDevelopUserID2", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistDevelopUserID2"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistEquipUserID2", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistEquipUserID2"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistQualityUserID2", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistQualityUserID2"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistEtcUserID2", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistEtcUserID2"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strResultDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ResultDesc"].ToString(), 500);
                        sql.mfAddParamDataRow(dtParam, "@i_strResultFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ResultFileName"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtParam, "@i_strResultUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ResultUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strResultDeliveryDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ResultDeliveryDate"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strResultEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ResultEtcDesc"].ToString(), 200);
                        sql.mfAddParamDataRow(dtParam, "@i_strAgreePurchaseUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreePurchaseUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strAgreeProductUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreeProductUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strAgreeDevelopUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreeDevelopUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strAgreeEquipUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreeEquipUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strAgreeQualityUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreeQualityUserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strAgreeDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["AgreeDesc"].ToString(), 500);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistPurchaseUserID3", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistPurchaseUserID3"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistProductUserID3", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistProductUserID3"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistDevelopUserID3", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistDevelopUserID3"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistEquipUserID3", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistEquipUserID3"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistQualityUserID3", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistQualityUserID3"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strDistEtcUserID3", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistEtcUserID3"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CompleteFlag"].ToString(), 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialSpecial", dtParam); //실행용
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_INSMaterialSpecial", dtParam); //디버깅용

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        #endregion
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        else
                        {
                            //관리번호 저장
                            strStdNum = strErrRtn;
                            strRtnStdNumber = ErrRtn.mfGetReturnValue(0);
                            TransErrRtn ErrRtnShip = new TransErrRtn();
                            string strShipRtn = string.Empty;
                            if (dtShip.Rows.Count > 0)
                            {
                                INSSTS.INSMaterialSpecialShip dtShipData = new INSMaterialSpecialShip();
                                strShipRtn = dtShipData.mfSaveMaterialSpecialShip(dtShip, strUserID, strUserIP, sql, trans, strRtnStdNumber);
                                ErrRtnShip = ErrRtnShip.mfDecodingErrMessage(strShipRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            trans.Commit();
                        }
                    }
                    else
                    {
                        ErrRtn.ErrNum = 0;
                        strRtnStdNumber = dt.Rows[i]["StdNumber"].ToString();
                    }
                    #endregion

                    #region WMS
                    // WMS 처리
                    if (dtWMS.Rows.Count > 0 && ErrRtn.ErrNum.Equals(0) && dt.Rows[0]["GWTFlag"].ToString().Equals("T"))
                    {
                        strErrRtn = mfSaveWMSMATInspectReq(dtWMS, strUserID, strUserIP);

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrMessage.Equals("00") || !ErrRtn.ErrNum.Equals(0))
                        {
                            break;
                        }
                        else
                        {
                            sql.mfConnect();
                            trans = sql.SqlCon.BeginTransaction();
                            DataTable dtParam = sql.mfSetParamDataTable();
                            sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                            sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strRtnStdNumber, 20);
                            sql.mfAddParamDataRow(dtParam, "@i_strValidationDate", ParameterDirection.Input, SqlDbType.VarChar, dtWMS.Rows[0]["ValidationDate"].ToString(), 10);
                            sql.mfAddParamDataRow(dtParam, "@i_strValidationDateFlag", ParameterDirection.Input, SqlDbType.Char, dtWMS.Rows[0]["ValidationDateFlag"].ToString(), 1);
                            sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                            sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                            strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialSpecial_WMS", dtParam);  // 실행용
                            //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_INSMaterialSpecial_WMS", dtParam);  // 디버깅용
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                            else
                            {
                                trans.Commit();
                            }
                        }
                    }
                    #endregion

                    #region MES 주석처리
                    //--QRP 서버에 등록이 되었을 경우 MES로 보낸다--//
                //    if (ErrRtn.ErrNum == 0)
                //    {
                //        // MES 전송 데이터 테이블 설정
                //        DataTable dtMES = dtWMS.DefaultView.ToTable(true, "LotNo", "InspectResultFlag");

                //        // 작성완료가 체크되어 있고 수입검사결정이 선택되어 있을경우 MES로 보냄 //
                //        if (Convert.ToBoolean(dt.Rows[i]["CompleteFlag"]) == true
                //            && dt.Rows[i]["InspectResult"].ToString() != ""
                //            && dt.Rows[i]["MESTFlag"].ToString() == "F"
                //            && dt.Rows[0]["GWTFlag"].ToString().Equals("T")
                //            && dtMES.Rows.Count > 0)
                //        {
                //            //MES컬럼설정
                //            //DataTable dtMES = new DataTable();
                //            //dtMES.Columns.Add("CONSUMABLEID", typeof(string));  //LOTNO
                //            //dtMES.Columns.Add("HOLDCODE", typeof(string));      //HoldCode  아직정해진코드가 없음
                //            //dtMES.Columns.Add("ACTIONTYPE", typeof(string));    //조치결과

                //            ////MES정보저장
                //            //DataRow drMES;

                //            //drMES = dtMES.NewRow();
                //            //drMES["CONSUMABLEID"] = dt.Rows[i]["LotNo"].ToString();
                //            //drMES["HOLDCODE"] = strHoldCode;
                //            //drMES["ACTIONTYPE"] = dt.Rows[i]["InspectResult"].ToString();
                //            //dtMES.Rows.Add(drMES);

                //            // MES 전송 데이터 테이블 설정
                //            dtMES.Columns["LotNo"].ColumnName = "CONSUMABLEID";
                //            dtMES.Columns["InspectResultFlag"].ColumnName = "ACTIONTYPE";
                //            // HoldCode 컬럼 추가
                //            dtMES.Columns.Add("HOLDCODE", typeof(string));

                //            // 컬럼순서변경
                //            dtMES.Columns["HOLDCODE"].SetOrdinal(1);
                //            dtMES.Columns["ACTIONTYPE"].SetOrdinal(2);

                //            // HoldCode 설정
                //            QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                //            DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(dt.Rows[i]["PlantCode"].ToString(), strFormName);
                //            string strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                //            for (int j = 0; j < dtMES.Rows.Count; j++)
                //            {
                //                dtMES.Rows[j]["HOLDCODE"] = strHoldCode;
                //            }

                //            // 시스템연결정보에서 MES연결정보를 가져온다 
                //            QRPSYS.BL.SYSPGM.SystemAccessInfo SysAcc = new QRPSYS.BL.SYSPGM.SystemAccessInfo();

                //            DataTable dtSysAcc = SysAcc.mfReadSystemAccessInfoDetail(dt.Rows[i]["PlantCode"].ToString(), "S04");

                //            // MES연결경로를 통하여 원자재 특채정보 MES 매서드를 호출한다 
                //            QRPMES.IF.Tibrv tib = new QRPMES.IF.Tibrv(dtSysAcc); //실행용
                //            //QRPMES.IF.Tibrv tib = new QRPMES.IF.Tibrv("1", dtSysAcc); //디버깅용

                //            DataTable dtResult = tib.CON_RELEASE4QC_REQ(strFormName, strUserID, dt.Rows[i]["EtcDesc"].ToString(), dtMES, strUserIP);


                //            if (dtResult.Rows.Count > 0)
                //            {
                //                // MES처리결과가 성공일 경우 
                //                if (dtResult.Rows[0][0].ToString() == "0")
                //                {
                //                    // 디비연결
                //                    sql.mfConnect();

                //                    //MES로 성공적으로 전송했으면 MESTFlag 처리
                //                    SqlTransaction transt;
                //                    transt = sql.SqlCon.BeginTransaction(); //실행용
                //                    //transt = m_SqlConnDebug.BeginTransaction(); //디버깅용

                //                    // 파라미터 저장
                //                    DataTable dtParamt = sql.mfSetParamDataTable();
                //                    sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                //                    sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                //                    sql.mfAddParamDataRow(dtParamt, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strRtnStdNumber, 20);
                //                    sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                //                    sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                //                    sql.mfAddParamDataRow(dtParamt, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                //                    sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //                    //원자재특채정보 : MES전송결과 프로시저 실행
                //                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSMaterialSpecialMESFlag", dtParamt); // 실행용 
                //                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, transt, "up_Update_INSMaterialSpecialMESFlag", dtParamt);  //디버깅용

                //                    //-- 처리결과에 따라 롤백, 커밋처리를 한다.--//
                //                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //                    if (ErrRtn.ErrNum != 0)
                //                    {
                //                        transt.Rollback();
                //                    }
                //                    else
                //                    {
                //                        transt.Commit();
                //                    }


                //                }
                //                //성공이나 실패면 MES오류 메세지를 처리한다.

                //                ErrRtn.InterfaceResultCode = dtResult.Rows[0][0].ToString();
                //                ErrRtn.InterfaceResultMessage = dtResult.Rows[0][1].ToString();
                //                ErrRtn.mfAddReturnValue(strRtnStdNumber);
                //                //MES
                //                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);

                //            }
                //            else
                //            {
                //                ErrRtn.mfAddReturnValue(strRtnStdNumber);
                //                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                //            }

                //        }
                //    }
                    //    //strErrRtn = strStdNum;
                    #endregion
                }

                //처리결과 리턴
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public DataTable mfSaveMaterialSpecialGRWApproval(DataTable dt, DataTable dtShip, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        {
            QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
            DataTable dtRtn = new DataTable();

            try
            {
                string strPlantCode = dt.Rows[0]["PlantCode"].ToString();
                string strStdNumber = dt.Rows[0]["StdNumber"].ToString();
                string strLegacyKey = strPlantCode + "||" + strStdNumber;

                string strFilePath = "\\INSMaterialSpecialFile\\";

                DataTable dtFileInfo = grw.mfSetFileInfoDataTable();

                if (!dt.Rows[0]["FileName1"].ToString().Equals(string.Empty))
                {
                    DataRow dr = dtFileInfo.NewRow();
                    dr["NM_FILE"] = strPlantCode + "-" + strStdNumber + "-Fir-" + dt.Rows[0]["FileName1"].ToString();
                    dr["PlantCode"] = dt.Rows[0]["PlantCode"].ToString();
                    dr["NM_LOCATION"] = strFilePath;

                    dtFileInfo.Rows.Add(dr);
                }
                if (!dt.Rows[0]["ResultFileName"].ToString().Equals(string.Empty))
                {
                    DataRow dr = dtFileInfo.NewRow();
                    dr["NM_FILE"] = strPlantCode + "-" + strStdNumber + "-Sec-" + dt.Rows[0]["ResultFileName"].ToString();
                    dr["PlantCode"] = dt.Rows[0]["PlantCode"].ToString();
                    dr["NM_LOCATION"] = strFilePath;

                    dtFileInfo.Rows.Add(dr);
                }

                dtRtn = grw.Material(strLegacyKey, dtSendLine, dtCcLine, dt, dtShip, dtFormInfo, dtFileInfo, strUserIP, strUserID);

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw(ex);
            }

            return dtRtn;
        }



        /// <summary>
        /// LotNo검색시 MaterialGR 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLotNo"></param>
        /// <param name="strMaterialCode"></param>
        /// <param name="strGRNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMaterialGRForMS(String strPlantCode, String strLotNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialGRForMS", dtParam);
                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 원자재 특채정보 삭제용
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMatrialSpecial(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSMaterialSpecial", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 그리드 클릭시 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMaterialSpecialGrid(String strPlantCode, String strStdNumber, String strLang)
        {
            SQLS sql = new SQLS(); //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();    //실행용

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialSpecialForGrid", dtParam); //실행용
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSMaterialSpecialForGrid", dtParam);  //디버깅용

                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// WMS 전송용 DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetWMSDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("GRNo", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(char));
                dtRtn.Columns.Add("ValidationDate", typeof(string));
                dtRtn.Columns.Add("IFFlag", typeof(string));
                dtRtn.Columns.Add("ValidationDateFlag", typeof(char));
                dtRtn.Columns.Add("AdmitDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 원자재특채결과정보 WMS에 전송
        /// </summary>
        /// <param name="dtSave">저장정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveWMSMATInspectReq(DataTable dtSave, string strUserID, string strUserIP)
        {
            #region 변경전
            ////TransErrRtn ErrRtn = new TransErrRtn();
            ////try
            ////{
            ////    string strErrRtn = string.Empty;
            ////    //WMS Oracle DB 연결정보 읽기
            ////    QRPSYS.BL.SYSPGM.SystemAccessInfo clsPgm = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
            ////    DataTable dtPgm = clsPgm.mfReadSystemAccessInfoDetail(dtSave.Rows[0]["PlantCode"].ToString(), "S05");
            ////    QRPDB.ORADB oraDB = new QRPDB.ORADB();
            ////    oraDB.mfSetORADBConnectString(dtPgm.Rows[0]["SystemAddressPath"].ToString(), dtPgm.Rows[0]["AccessID"].ToString(), dtPgm.Rows[0]["AccessPassword"].ToString());

            ////    //WMS Oracle DB연결하기
            ////    oraDB.mfConnect();

            ////    //Transaction 시작
            ////    OracleTransaction trans;
            ////    trans = oraDB.SqlCon.BeginTransaction();

            ////    for (int i = 0; i < dtSave.Rows.Count; i++)
            ////    {
            ////        //WMS 처리 SP 호출
            ////        DataTable dtParam = oraDB.mfSetParamDataTable();
            ////        oraDB.mfAddParamDataRow(dtParam, "V_PLANTCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["PlantCode"].ToString(), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_CONSTRACTCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["GRNo"].ToString(), 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_LOTNO", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["LotNo"].ToString(), 50);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_RESULTFLAG", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERIP", ParameterDirection.Input, OracleDbType.Varchar2, strUserIP, 15);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERID", ParameterDirection.Input, OracleDbType.Varchar2, strUserID, 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_VALIDATIONDATE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["ValidationDate"].ToString(), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_IFFLAG", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["IFFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "RTN", ParameterDirection.Output, OracleDbType.Varchar2, 2);

            ////        strErrRtn = oraDB.mfExecTransStoredProc(oraDB.SqlCon, trans, "UP_UPDATE_INSINSPECTRESULT", dtParam);
            ////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            ////        //WMS 처리결과에 따른 Transaction 처리
            ////        if (ErrRtn.ErrNum != 0)
            ////            trans.Rollback();
            ////    }
            ////    trans.Commit();

            ////    oraDB.Dispose();
            ////    return strErrRtn;
            ////}
            ////catch (Exception ex)
            ////{
            ////    return ex.Message;
            ////}
            ////finally
            ////{
            ////}
            #endregion
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                //SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strValidationDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ValidationDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strIFFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["IFFlag"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AdmitDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_INSMaterialSpecial_WMS_Send", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrMessage.Equals("00") || !ErrRtn.ErrNum.Equals(0))
                    {
                        //trans.Rollback();
                        break;
                    }
                }

                //if (ErrRtn.ErrNum.Equals(0))
                //    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("INSMaterialSpecialShip")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class INSMaterialSpecialShip : ServicedComponent
    {
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtMaterialSpecialShip = new DataTable();
            try
            {
                dtMaterialSpecialShip.Columns.Add("PlantCode", typeof(String));
                dtMaterialSpecialShip.Columns.Add("StdNumber", typeof(String));
                dtMaterialSpecialShip.Columns.Add("LotSeq", typeof(int));
                dtMaterialSpecialShip.Columns.Add("LotCheckFlag", typeof(String));
                dtMaterialSpecialShip.Columns.Add("LotNo", typeof(String));
                dtMaterialSpecialShip.Columns.Add("GRNo", typeof(String));
                dtMaterialSpecialShip.Columns.Add("EtcDesc", typeof(String));
                dtMaterialSpecialShip.Columns.Add("GRDate", typeof(String));
                dtMaterialSpecialShip.Columns.Add("MaterialCode", typeof(String));
                dtMaterialSpecialShip.Columns.Add("MaterialName", typeof(String));
                dtMaterialSpecialShip.Columns.Add("GRQty", typeof(String));
                dtMaterialSpecialShip.Columns.Add("VendorCode", typeof(String));
                dtMaterialSpecialShip.Columns.Add("VendorName", typeof(String));

                return dtMaterialSpecialShip;
            }
            catch (Exception ex)
            {
                return dtMaterialSpecialShip;
                throw (ex);
            }
            finally
            {
                dtMaterialSpecialShip.Dispose();
            }
        }

        /// <summary>
        /// Ship정보검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLotNo"></param>
        /// <param name="strMaterialCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadShipInfo(String strPlantCode, String strLotNo, String strMaterialCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialSpecialShip", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
        /// <summary>
        /// 검색시 저장된 ShipInfo 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadShipInfo_Grid(String strPlantCode, String strStdNumber, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialSpecialShipForGrid", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// MaterialSpecialSpecialShip 저장
        /// </summary>
        /// <param name="dtShip"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strStdNumber"></param>
        /// <returns></returns>
        [AutoComplete(false)]

        public string mfSaveMaterialSpecialShip(DataTable dtShip, String strUserID, String strUserIP, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dtShip.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtShip.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intLotSeq", ParameterDirection.Input, SqlDbType.Int, dtShip.Rows[i]["LotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLotCheckFlag", ParameterDirection.Input, SqlDbType.Char, dtShip.Rows[i]["LotCheckFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtShip.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtShip.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtShip.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialSpecialShip", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
               
            }
        }

    }




    /// <summary>
    /// OCAP List
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("OCAP")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class OCAP : ServicedComponent
    {
        /// <summary>
        /// [OCAP List] List 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strProcessCode"> 공정코드 </param>
        /// <param name="strEquipCode"> 설비코드 </param>
        /// <param name="strLotNo"> LotNo </param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcOCAP(String strPlantCode, String strProcessCode, String strEquipCode, String strLotNo, string strFromReasonDate, string strToReasonDate, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strFromReasonDate", ParameterDirection.Input, SqlDbType.VarChar, strFromReasonDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToReasonDate", ParameterDirection.Input, SqlDbType.VarChar, strToReasonDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcOCAP", dtParam);
                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveOCAP_MESIF(DataTable dtVar, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["FACTORYID"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReasonEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["EQPID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["LOTID"].ToString(), 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["PACKAGE"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strOcapType", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["OCAPTYPE"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReasonProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["REASONOPERID"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strActionProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["ACTIONOPERID"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strActionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtVar.Rows[0]["ACTIONITEM"].ToString(), 500);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["CUSTOMERID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, dtVar.Rows[0]["USERID"].ToString(), 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSOCAP_MESIF", dtParam);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정검사 MES I/F후 OCAP에 등록된 정보있으면 정보 반환하는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strPackage">Package코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcOCAP_ForINSProcReq(string strPlantCode, string strLotNo, string strProcessCode, string strPackage)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcOCAP_ForINSProcReq", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    #region AbnormalYield

    /// <summary>
    /// AbnormalYield
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AbnormalYield")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AbnormalYield : ServicedComponent
    {
        /// <summary>
        /// 데이터 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataSet()
        {
            DataTable dtColumns = new DataTable();

            try
            {
                dtColumns.Columns.Add("PlantCode", typeof(string));
                dtColumns.Columns.Add("ManageNo", typeof(string));
                dtColumns.Columns.Add("LotNo", typeof(string));
                dtColumns.Columns.Add("LotQty", typeof(string));
                dtColumns.Columns.Add("LotProcessState", typeof(string));
                dtColumns.Columns.Add("ProcessHoldState", typeof(string));
                dtColumns.Columns.Add("Customer", typeof(string));
                dtColumns.Columns.Add("ProductCode", typeof(string));
                dtColumns.Columns.Add("ProductName", typeof(string));
                dtColumns.Columns.Add("AriseProcessCode", typeof(string));
                dtColumns.Columns.Add("AriseProcessName", typeof(string));
                dtColumns.Columns.Add("WorkUserID", typeof(string));
                dtColumns.Columns.Add("EquipCode", typeof(string));
                dtColumns.Columns.Add("Package", typeof(string));
                dtColumns.Columns.Add("AriseDate", typeof(string));
                dtColumns.Columns.Add("AriseTime", typeof(string));
                dtColumns.Columns.Add("AcceptFlag", typeof(string));
                dtColumns.Columns.Add("AcceptDate", typeof(string));
                dtColumns.Columns.Add("AcceptTime", typeof(string));

                dtColumns.Columns.Add("EtcDesc", typeof(string));
                dtColumns.Columns.Add("AriseCause", typeof(string));
                dtColumns.Columns.Add("MeasureDesc", typeof(string));
                dtColumns.Columns.Add("RegistUserID", typeof(string));
                dtColumns.Columns.Add("S0DeptCode", typeof(string));
                dtColumns.Columns.Add("ManFlag", typeof(string));
                dtColumns.Columns.Add("MethodFlag", typeof(string));
                dtColumns.Columns.Add("MaterialFlag", typeof(string));
                dtColumns.Columns.Add("MachineFlag", typeof(string));
                dtColumns.Columns.Add("EnvironmentFlag", typeof(string));
                
                dtColumns.Columns.Add("S1UserID", typeof(string));
                dtColumns.Columns.Add("S1Comment", typeof(string));
                dtColumns.Columns.Add("S1DeptCode", typeof(string));
                dtColumns.Columns.Add("S1CompleteFlag", typeof(string));
                dtColumns.Columns.Add("S2Date", typeof(string));
                dtColumns.Columns.Add("S2UserID", typeof(string));
                dtColumns.Columns.Add("S2Comment", typeof(string));
                dtColumns.Columns.Add("S2DeptCode", typeof(string));
                dtColumns.Columns.Add("S2CompleteFlag", typeof(string));
                dtColumns.Columns.Add("S3Date", typeof(string));
                dtColumns.Columns.Add("S3UserID", typeof(string));
                dtColumns.Columns.Add("S3Comment", typeof(string));
                dtColumns.Columns.Add("S3DeptCode", typeof(string));
                dtColumns.Columns.Add("S3CompleteFlag", typeof(string));
                dtColumns.Columns.Add("S4Date", typeof(string));
                dtColumns.Columns.Add("S4UserID", typeof(string));
                dtColumns.Columns.Add("S4Comment", typeof(string));
                dtColumns.Columns.Add("S4CompleteFlag", typeof(string));
                dtColumns.Columns.Add("S5Date", typeof(string));
                dtColumns.Columns.Add("S5UserID", typeof(string));
                dtColumns.Columns.Add("S5Comment", typeof(string));
                dtColumns.Columns.Add("S5CompleteFlag", typeof(string));
                dtColumns.Columns.Add("ResultFlag", typeof(string));
                dtColumns.Columns.Add("ReturnFlag", typeof(string));
                dtColumns.Columns.Add("ReturnStep", typeof(string));
                dtColumns.Columns.Add("AbnormalFlag", typeof(string));


                return dtColumns;

            }
            catch (Exception ex)
            {
                return dtColumns;
                throw (ex);
            }
            finally
            {
                dtColumns.Dispose();
            }
        }


        /// <summary>
        /// 저수율 헤더리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAriseDateFrom">검색시작일</param>
        /// <param name="strAriseDateTo">검색종료일</param>
        /// <param name="strCustomerCode">고객</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strLotReleaseFlag">LotRelease여부</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcAbnormalYield(string strPlantCode, string strAriseDateFrom, string strAriseDateTo,
                                                            string strProductCode, string strLotReleaseFlag, string strPackage,
                                                            string strCustomerCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateTo", ParameterDirection.Input, SqlDbType.VarChar, strAriseDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLotReleaseFlag", ParameterDirection.Input, SqlDbType.VarChar, strLotReleaseFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcAbnormalYield", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }



        /// <summary>
        /// 공장 Low Yield 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAriseProcessCode">발생공정코드</param>
        /// <param name="strAriseDate">발생일</param>
        /// <param name="strAriseTime">발생시간</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSProcAbnormalYield_Detail(string strPlantCode, string strAriseProcessCode, string strAriseDate, string strAriseTime, string strLang)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();    //실행용

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcAbnormalYield_Detail", dtParam);//실행용

                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }



        /// <summary>
        /// 저수율 정보 저장
        /// </summary>
        /// <param name="dtYield">저수율저장정보</param>
        /// <param name="dtYieldFile">파일정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcAbnormalYield(DataTable dtYield, DataTable dtYieldFile, String strUserID, String strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            String strErrRtn = "";
            try
            {
                // DBOpen
                sql.mfConnect();
                
                // Transsaction
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // Loop
                for (int i = 0; i < dtYield.Rows.Count; i++)
                {
                    #region SP Paramter
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["PlantCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["ManageNo"].ToString(), 50);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseProcessCode"].ToString(), 10);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseDate"].ToString(),10);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseTime"].ToString(), 10);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseCause", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["AriseCause"].ToString(), 200);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["MeasureDesc"].ToString(), 200);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strRegistUserID", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["RegistUserID"].ToString(), 20);
                     
                    sql.mfAddParamDataRow(dtParam, "@i_strManFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["ManFlag"].ToString(), 1);
                     
                    sql.mfAddParamDataRow(dtParam, "@i_strMethodFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["MethodFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["MaterialFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strMachineFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["MachineFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strEnvironmentFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["EnvironmentFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strS1UserID", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S1UserID"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strS1Comment", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["S1Comment"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParam, "@i_strS1DeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S1DeptCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["S1CompleteFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strS2Date", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S2Date"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strS2UserID", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S2UserID"].ToString(), 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS2Comment", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["S2Comment"].ToString(), 1000);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS2DeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S2DeptCode"].ToString(), 10);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS2CompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["S2CompleteFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS3Date", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S3Date"].ToString(), 10);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS3UserID", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S3UserID"].ToString(), 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS3Comment", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["S3Comment"].ToString(), 1000);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS3DeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S3DeptCode"].ToString(), 10);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS3CompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["S3CompleteFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS4Date", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S4Date"].ToString(), 10);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS4UserID", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S4UserID"].ToString(), 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS4Comment", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["S4Comment"].ToString(), 1000);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS4CompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["S4CompleteFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS5Date", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S5Date"].ToString(), 10);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS5UserID", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S5UserID"].ToString(), 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS5Comment", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["S5Comment"].ToString(), 1000);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strS5CompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["S5CompleteFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["ResultFlag"].ToString(), 2);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strReturnFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["ReturnFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strReturnStep", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["ReturnStep"].ToString(), 5);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["AbnormalFlag"].ToString(), 1);


                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    #endregion

                    // SP
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcAbnormalYield", dtParam);
                    // 결과처리
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // Fail -> Rollback
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else // Complete
                    {
                        if (dtYieldFile.Rows.Count > 0)
                        {
                            // Save File
                            AbnormalYieldFile clsFile = new AbnormalYieldFile();
                            // 중복행 제거
                            DataTable dtStep = dtYieldFile.DefaultView.ToTable(true, "Step");
                            for (int f = 0; f < dtStep.Rows.Count; f++)
                            {
                                // 삭제
                                strErrRtn = clsFile.mfDeleteINSAbnormalYieldFile(dtYield.Rows[i]["PlantCode"].ToString(), dtYield.Rows[i]["ManageNo"].ToString(), dtYield.Rows[i]["AriseProcessCode"].ToString(),
                                                                        dtYield.Rows[i]["AriseDate"].ToString(), dtYield.Rows[i]["AriseTime"].ToString(),
                                                                        dtStep.Rows[f]["Step"].ToString(), "0", sql.SqlCon, trans);
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                // Fail -> Rollback
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                         
                            // 저장
                            strErrRtn = clsFile.mfSaveINSAbnormalYieldFile(dtYieldFile, strUserID, strUserIP, sql.SqlCon, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            // Fail -> Rollback
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }

                        }
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                // DB Colse
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 获取ManageNo
        /// </summary>
        /// <param name="txtLotno"></param>
        /// <param name="txtProductCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfManageNo()
        {
            SQLS sql = new SQLS();  //실행용
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();    //실행용

                //DataTable dtParam = sql.mfSetParamDataTable();
                //sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcAbnormalYieldManageNo");//실행용

                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
        /// <summary>
        /// 保存HEAD部分数据
        /// </summary>
        /// <param name="dtYield"></param>
        /// <param name="dtYieldFile"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcAbnormalYieldHead(DataTable dtYield, String strUserID, String strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            String strErrRtn = "";
            try
            {
                // DBOpen
                sql.mfConnect();

                // Transsaction
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // Loop
                for (int i = 0; i < dtYield.Rows.Count; i++)
                {
                    #region SP Paramter
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["ManageNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotQty", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["LotQty"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotProcessState", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["LotProcessState"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessHoldState", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["ProcessHoldState"].ToString(), 50);                    
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomer", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["Customer"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkUserID", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["WorkUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcceptDate", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AcceptDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcceptTime", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AcceptTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcceptFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["AcceptFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["EtcDesc"].ToString(), 500);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    #endregion

                    // SP
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcAbnormalYieldHead", dtParam);
                    // 결과처리
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // Fail -> Rollback
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                // DB Colse
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES 저수율 정보 전송
        /// </summary>
        /// <param name="dtMES"></param>
        /// <param name="dtScrap"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcAbnormalYield_MES_Result(DataTable dtMES, DataTable dtScrap, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;

                //MES 서버 경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtMES.Rows[0]["PlantCode"].ToString(), "S04");    //MES_LiveServer
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtMES.Rows[0]["PlantCode"].ToString(), "S07");    //MES_TestServer

                //MES 공정 Low Yield List 전송 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);  //실행용

                DataTable dtProceeLowYield = clsTibrv.LOW_YIELD_RESULT_REQ(dtMES.Rows[0]["FormName"].ToString()
                                                                        , dtMES.Rows[0]["LotNo"].ToString()
                                                                        , dtMES.Rows[0]["AriseProcessCode"].ToString()
                                                                        , dtMES.Rows[0]["WorkUserID"].ToString()
                                                                        , dtMES.Rows[0]["EquipCode"].ToString()
                                                                        , dtMES.Rows[0]["ScrapFlag"].ToString()
                                                                        , dtScrap
                                                                        , strUserIP);

                if (dtProceeLowYield.Rows.Count > 0)
                {
                    // MES전송이 성공하여 성공MES코드를 받아오면 테이블의 MESTFlag를 업데이트시킨다.
                    if (dtProceeLowYield.Rows[0][0].ToString().Equals("0"))
                    {
                        //디비연결
                        sql.mfConnect();    //실행용

                        //트랜젝션 시작
                        SqlTransaction transt = sql.SqlCon.BeginTransaction();  //실행용

                        //파라미터 정보 저장
                        DataTable dtPtarmt = sql.mfSetParamDataTable();

                        sql.mfAddParamDataRow(dtPtarmt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                        sql.mfAddParamDataRow(dtPtarmt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMES.Rows[0]["PlantCode"].ToString(), 10);           //공장
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtMES.Rows[0]["AriseProcessCode"].ToString(), 10); //발생공정
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtMES.Rows[0]["AriseDate"].ToString(), 10);          //발생일
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtMES.Rows[0]["AriseTime"].ToString(), 10);        //발생시간
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtPtarmt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtPtarmt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //공정 LowYield List MESTFlag 업데이트 프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSProcAbnormalYieldMESTFlag", dtPtarmt);   //실행용

                        // Decoding //
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 업데이트 성공시 커밋 실패시 롤백처리를 한다.
                        if (ErrRtn.ErrNum != 0)
                            transt.Rollback();
                        else
                            transt.Commit();
                    }

                    //성공이나 실패시 MES코드와 메세지를 EnCoding 한다.
                    ErrRtn.InterfaceResultCode = dtProceeLowYield.Rows[0][0].ToString();
                    ErrRtn.InterfaceResultMessage = dtProceeLowYield.Rows[0][1].ToString();
                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 저수율 MES Release 메소드
        /// </summary>
        /// <param name="dtStdInfo">Mes Flag저장에 필요한 기본정보가 담긴 데이터 테이블</param>
        /// <param name="dtLotList">LotNo/HoldCode 정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcAbnormalYield_MES_MESRelease(DataTable dtStdInfo, DataTable dtLotList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04");    //MES_LiveServer
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");    //MES_TestServer

                //QCNList MES전송 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtMES = clsTibrv.LOT_RELEASE4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString()
                                                            , dtStdInfo.Rows[0]["WorkUserID"].ToString()
                                                            , dtStdInfo.Rows[0]["Comment"].ToString()
                                                            , dtLotList
                                                            , strUserIP);

                // MES I/F 성공시
                if (dtMES.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    sql.mfConnect();
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcAbnormalYield_MESReleaseTFlag", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum == 0)
                        trans.Commit();
                    else
                        trans.Rollback();
                }

                ErrRtn.InterfaceResultCode = dtMES.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtMES.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 저수율 Abnormal 변경
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAriseProcessCode">발생공정</param>
        /// <param name="strAriseDate">발생일</param>
        /// <param name="strAriseTime">발생시간</param>
        /// <param name="strAbnormalFlag"></param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns> 、、DataTable dtYield
        [AutoComplete(false)]
        public string mfSaveINSProcAbnormalYield_Abnormal(DataTable dtYield, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                // Loop
                for (int i = 0; i < dtYield.Rows.Count; i++)
                {
                    #region SP Paramter
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["ManageNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["AbnormalFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseCause", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["AriseCause"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtYield.Rows[i]["MeasureDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strRegistUserID", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["RegistUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strManFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["ManFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMethodFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["MethodFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["MaterialFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMachineFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["MachineFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEnvironmentFlag", ParameterDirection.Input, SqlDbType.Char, dtYield.Rows[i]["EnvironmentFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strS0DeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtYield.Rows[i]["S0DeptCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    #endregion

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcAbnormalYield_Abnormal", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum == 0)
                        trans.Commit();
                    else
                        trans.Rollback();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 저수율 Abnormal 반려
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAriseProcessCode">발생공정</param>
        /// <param name="strAriseDate">발생일</param>
        /// <param name="strAriseTime">발생시간</param>
        /// <param name="strReturnFlag">반려여부</param>
        /// <param name="strReturnStep">반려Step</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcAbnormalYield_Return(string strPlantCode, string strAriseProcessCode, string strAriseDate, string strAriseTime,
                                                            string strReturnFlag, string strReturnStep, string strResultFlag,
                                                            string strS5Date, string strS5UserID, string strS5Comment, 
                                                            string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strReturnFlag", ParameterDirection.Input, SqlDbType.Char, strReturnFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strReturnStep", ParameterDirection.Input, SqlDbType.VarChar, strReturnStep, 5);

                sql.mfAddParamDataRow(dtParam, "@i_strS5Date", ParameterDirection.Input, SqlDbType.VarChar, strS5Date, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strS5UserID", ParameterDirection.Input, SqlDbType.VarChar, strS5UserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strS5Comment", ParameterDirection.Input, SqlDbType.NVarChar, strS5Comment, 1000);
                sql.mfAddParamDataRow(dtParam, "@i_strResultFlag", ParameterDirection.Input, SqlDbType.VarChar, strResultFlag, 2);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcAbnormalYield_Return", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        public DataTable RtnMESDataColumns()
        {
            DataTable dtRtn = new DataTable();

            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManageNo", typeof(string));
                dtRtn.Columns.Add("FORMNAME", typeof(string));
                dtRtn.Columns.Add("LOTID", typeof(string));
                dtRtn.Columns.Add("AriseProcessCode", typeof(string));
                dtRtn.Columns.Add("RELEASEFLAG", typeof(string));
                dtRtn.Columns.Add("USERID", typeof(string));
                dtRtn.Columns.Add("USERIP", typeof(string));
                dtRtn.Columns.Add("COMMENT", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// TEST Release 처리여부 메소드
        /// </summary>
        /// <param name="dtStdInfo">Mes Flag저장에 필요한 기본정보가 담긴 데이터 테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSProcAbnormalYield_LOT_TESTHOLD4QC_REQ(DataTable dtStdInfo, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                // MES 서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04");    //MES_LiveServer
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");    //MES_TestServer

                // MES전송 매서드 실행
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtMES = clsTibrv.LOT_TESTHOLD4QC_REQ(dtStdInfo.Rows[0]["FORMNAME"].ToString(), dtStdInfo.Rows[0]["USERID"].ToString(),
                                                                dtStdInfo.Rows[0]["LOTID"].ToString(),dtStdInfo.Rows[0]["AriseProcessCode"].ToString(),
                                                                dtStdInfo.Rows[0]["RELEASEFLAG"].ToString(),dtStdInfo.Rows[0]["COMMENT"].ToString(),
                                                                dtStdInfo.Rows[0]["USERIP"].ToString());
                                                            
                // MES I/F 성공시
                if (dtMES.Rows[0]["returncode"].ToString().Trim().Equals("0"))
                {
                    sql.mfConnect();

                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ManageNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReleaseFlag", ParameterDirection.Input, SqlDbType.Char, dtStdInfo.Rows[0]["RELEASEFLAG"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcAbnormalYield_MESTFlag", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum == 0)
                        trans.Commit();
                    else
                        trans.Rollback();
                }

                ErrRtn.InterfaceResultCode = dtMES.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtMES.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


    }

        /// <summary>
    /// AbnormalYieldD
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AbnormalYieldD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AbnormalYieldD : ServicedComponent
    {

        /// <summary>
        /// Low Yield 상세 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strAriseProcessCode"></param>
        /// <param name="strAriseDate"></param>
        /// <param name="strAriseTime"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAbnormalYieldD(String strPlantCode, String strAriseProcessCode, String strAriseDate, String strAriseTime, String strLang)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtProceLowYieldD = new DataTable();
            try
            {
                sql.mfConnect();    //실행용

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtProceLowYieldD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcAbnormalYieldD", dtParam); //실행용

                return dtProceLowYieldD;

            }
            catch (Exception ex)
            {
                return dtProceLowYieldD;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtProceLowYieldD.Dispose();
            }
        }

    }

    /// <summary>
    /// AbnormalYieldFile
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AbnormalYieldFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AbnormalYieldFile : ServicedComponent
    {

        /// <summary>
        /// Set DataColumns 
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataSet()
        {
            DataTable dtColumns = new DataTable();
            try
            {
                dtColumns.Columns.Add("PlantCode", typeof(string));
                dtColumns.Columns.Add("ManageNo", typeof(string));
                dtColumns.Columns.Add("AriseProcessCode", typeof(string));
                dtColumns.Columns.Add("AriseDate", typeof(string));
                dtColumns.Columns.Add("AriseTime", typeof(string));
                dtColumns.Columns.Add("Step", typeof(string));
                dtColumns.Columns.Add("Seq", typeof(int));
                dtColumns.Columns.Add("FileName", typeof(string));
                dtColumns.Columns.Add("FilePath", typeof(string));

                return dtColumns;

            }
            catch (Exception ex)
            {
                return dtColumns;
                throw (ex);
            }
            finally
            {
                dtColumns.Dispose();
            }
        }

        /// <summary>
        /// Low Yield 파일정보 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strAriseProcessCode"></param>
        /// <param name="strAriseDate"></param>
        /// <param name="strAriseTime"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAbnormalYieldFile(String strPlantCode, String strManageNo ,String strAriseProcessCode, String strAriseDate, String strAriseTime, String strLang)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtProceLowYieldD = new DataTable();
            try
            {
                sql.mfConnect();    //실행용

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);

                dtProceLowYieldD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcAbnormalYieldFile", dtParam); //실행용

                return dtProceLowYieldD;

            }
            catch (Exception ex)
            {
                return dtProceLowYieldD;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtProceLowYieldD.Dispose();
            }
        }


        /// <summary>
        /// 저수율 파일정보 저장
        /// </summary>
        /// <param name="dtFileInfo">파일정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSAbnormalYieldFile(DataTable dtFileInfo, String strUserID, String strUserIP, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = string.Empty;
            SQLS sql = new SQLS();
            try
            {

                for (int i = 0; i < dtFileInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["ManageNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStep", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["Step"].ToString(),5);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtFileInfo.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtFileInfo.Rows[i]["FileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFileInfo.Rows[i]["FilePath"].ToString(), 2000);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_INSProcAbnormalYieldFile", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 저수율 파일정보 삭제
        /// </summary>
        /// <param name="dtFileInfo">파일정보</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSAbnormalYieldFile(DataTable dtFileInfo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = string.Empty;
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtFileInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["ManageNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["AriseTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStep", ParameterDirection.Input, SqlDbType.VarChar, dtFileInfo.Rows[i]["Step"].ToString(),5);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtFileInfo.Rows[i]["Seq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcAbnormalYieldFile", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 저수율 파일정보 삭제
        /// </summary>
        /// <param name="dtFileInfo">파일정보</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSAbnormalYieldFile(string strPlantCode, string strManageNo, string strAriseProcessCode, string strAriseDate, string strAriseTime, 
                                                    string strStep, string strSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = string.Empty;
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStep", ParameterDirection.Input, SqlDbType.VarChar, strStep,5);
                sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, strSeq);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSProcAbnormalYieldFile", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else   
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 저수율 파일정보 삭제
        /// </summary>
        /// <param name="dtFileInfo">파일정보</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSAbnormalYieldFile(string strPlantCode, string strManageNo, string strAriseProcessCode, string strAriseDate, string strAriseTime,
                                                    string strStep, string strSeq, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = string.Empty;
            SQLS sql = new SQLS();
            try
            {

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, strAriseDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseTime", ParameterDirection.Input, SqlDbType.VarChar, strAriseTime, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStep", ParameterDirection.Input, SqlDbType.VarChar, strStep, 5);
                sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, strSeq);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_INSProcAbnormalYieldFile", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                
            }
            finally
            {
                sql.Dispose();
            }
        }
    }


    #endregion


}
