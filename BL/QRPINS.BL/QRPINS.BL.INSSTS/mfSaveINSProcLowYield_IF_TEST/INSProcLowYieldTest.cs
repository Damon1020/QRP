﻿using QRPINS.BL.INSSTS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;

namespace mfSaveINSProcLowYield_IF_TEST
{
    
    
    /// <summary>
    ///这是 INSProcLowYieldTest 的测试类，旨在
    ///包含所有 INSProcLowYieldTest 单元测试
    ///</summary>
    [TestClass()]
    public class INSProcLowYieldTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试属性
        // 
        //编写测试时，还可使用以下属性:
        //
        //使用 ClassInitialize 在运行类中的第一个测试前先运行代码
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //使用 ClassCleanup 在运行完类中的所有测试后再运行代码
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //使用 TestInitialize 在运行每个测试前先运行代码
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //使用 TestCleanup 在运行完每个测试后运行代码
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///mfSaveINSProcLowYield_IF 的测试
        ///</summary>
        [TestMethod()]
        public void mfSaveINSProcLowYield_IFTest()
        {
            DataTable dtScrap = new DataTable();
            dtScrap.Columns.Add("SCRAPCODE", typeof(string));
            dtScrap.Columns.Add("SCRAPQTY", typeof(string));
            DataRow drScrap = dtScrap.NewRow();
            drScrap["SCRAPCODE"] = "F043";
            drScrap["SCRAPQTY"] = "2166";
            dtScrap.Rows.Add(drScrap);

            INSProcLowYield target = new INSProcLowYield(); // TODO: 初始化为适当的值
            string strPLANTNAME = "2210"; // TODO: 初始化为适当的值
            string strISSUEDATE = "2016-02-24 23:08:19"; // TODO: 初始化为适当的值
            string strLOTID = "AG9XR6.10K"; // TODO: 初始化为适当的值
            string strOPERID = "A8100"; // TODO: 初始化为适当的值
            string strEQPID = "PV0001"; // TODO: 初始化为适当的值
            string strPRODUCTSPECNAME = "05033-1931-1"; // TODO: 初始化为适当的值
            string strPRODUCTQUANTITY = "22328.0"; // TODO: 初始化为适当的值
            string strBINNAME = ""; // TODO: 初始化为适当的值
            string strYIELD = "90.299"; // TODO: 初始化为适当的值
            //dtScrap = null; // TODO: 初始化为适当的值
            string strUserIP = "10.61.62.110"; // TODO: 初始化为适当的值
            string strUSERID = "PS003611"; // TODO: 初始化为适当的值
            string expected = string.Empty; // TODO: 初始化为适当的值
            string actual;
            actual = target.mfSaveINSProcLowYield_IF(strPLANTNAME, strISSUEDATE, strLOTID, strOPERID, strEQPID, strPRODUCTSPECNAME, strPRODUCTQUANTITY, strBINNAME, strYIELD, dtScrap, strUserIP, strUSERID);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("验证此测试方法的正确性。");
        }
    }
}
