﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질검사관리                                          */
/* 프로그램ID   : clsINSIMP.cs                                          */
/* 프로그램명   : 수입검사관리                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-16                                            */
/* 수정이력     : 2011-09-16 : AQL 적용/비적용 부분 수정 (이종호)       */
/*                2011-08-23 : 수입검사클래스 추가 (이종호)             */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Using 추가
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using System.Diagnostics;

using QRPDB;
using Oracle.DataAccess.Client;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]
namespace QRPINS.BL.INSIMP
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialGR")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialGR : ServicedComponent
    {
        /// <summary>
        /// 저장용 DataTable 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtGR = new DataTable();
            try
            {
                dtGR.Columns.Add("PlantCode", typeof(String));
                dtGR.Columns.Add("GRNo", typeof(String));               //주석삭제:mfSaveINSMaterialGR_IMSI 사용
                dtGR.Columns.Add("LotNo", typeof(String));              //주석삭제:mfSaveINSMaterialGR_IMSI 사용
                //dtGR.Columns.Add("GRType", typeof(String));
                dtGR.Columns.Add("VendorCode", typeof(String));
                dtGR.Columns.Add("MaterialCode", typeof(String));
                dtGR.Columns.Add("GRDate", typeof(String));
                //dtGR.Columns.Add("GRQty", typeof(Double));
                //dtGR.Columns.Add("UnitCode", typeof(String));
                //dtGR.Columns.Add("MaterialSpec1", typeof(String));
                dtGR.Columns.Add("MoldSeq", typeof(String));            //주석삭제:mfSaveINSMaterialGR_IMSI 사용    
                //dtGR.Columns.Add("ApplyDevice", typeof(String));
                dtGR.Columns.Add("SpecNo", typeof(String));             //주석삭제:mfSaveINSMaterialGR_IMSI 사용
                //dtGR.Columns.Add("FloorPlanNo", typeof(String));
                //dtGR.Columns.Add("ManufactureDate", typeof(String));
                //dtGR.Columns.Add("ExpirationDate", typeof(String));
                //dtGR.Columns.Add("EtcDesc", typeof(String));
                dtGR.Columns.Add("MaterialReqFlag", typeof(String));
                //dtGR.Columns.Add("InspectFlag", typeof(String));
                dtGR.Columns.Add("GRFlag", typeof(String));             //추가:mfSaveINSMaterialGR_IMSI 사용
                dtGR.Columns.Add("MoLotNo", typeof(string));

                return dtGR;
            }
            catch (Exception ex)
            {
                return dtGR;
                throw (ex);
            }
            finally
            {
                dtGR.Dispose();
            }
        }

        /// <summary>
        /// 조회용 DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetSearchDataInfo()
        {
            DataTable dtGR = new DataTable();
            try
            {
                dtGR.Columns.Add("PlantCode", typeof(String));
                dtGR.Columns.Add("GRNo", typeof(String));
                dtGR.Columns.Add("LotNo", typeof(String));
                dtGR.Columns.Add("VendorCode", typeof(String));
                dtGR.Columns.Add("MaterialCode", typeof(String));
                dtGR.Columns.Add("GRFromDate", typeof(String));
                dtGR.Columns.Add("GRToDate", typeof(String));
                dtGR.Columns.Add("ConsumableTypeCode", typeof(string));
                dtGR.Columns.Add("GRFlag", typeof(char));

                return dtGR;
            }
            catch (Exception ex)
            {
                return dtGR;
                throw (ex);
            }
            finally
            {
                dtGR.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 검색(그리드용) Method
        /// </summary>
        /// <param name="dtGR"> 검색조건이 저장되어있는 DataTable </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialGR(DataTable dtGR, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtnGR = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                for (int i = 0; i < dtGR.Rows.Count; i++)
                {
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtGR.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRFromDate", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["GRFromDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRToDate", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["GRToDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    // SP 실행
                    dtRtnGR = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialGR", dtParam);
                }
                return dtRtnGR;
            }
            catch (Exception ex)
            {
                return dtRtnGR;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtnGR.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 상세검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <param name="strGRDate"> 입고일 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialGRDetail(String strPlantCode, String strVendorCode, String strMaterialCode, String strGRDate, String strSpecNo, string strMoldSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtnGR = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, strGRDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, strSpecNo, 200);
                sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, strMoldSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtnGR = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialGRDetail", dtParam);

                return dtRtnGR;
            }
            catch (Exception ex)
            {
                return dtRtnGR;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtnGR.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 상세조회(그리드) Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <param name="strGRDate"> 입고일 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialGRDetail_Grid(String strPlantCode, String strVendorCode, String strMaterialCode, String strGRDate, String strSpecNo, string strMoldSeq, string strGRNo)
        {
            SQLS sql = new SQLS();
            DataTable dtRtnGR = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, strGRDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, strSpecNo, 200);
                sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, strMoldSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, strGRNo, 20);

                dtRtnGR = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialGRDetailGrid", dtParam);

                return dtRtnGR;
            }
            catch (Exception ex)
            {
                return dtRtnGR;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtnGR.Dispose();
            }
        }

        /// <summary>
        /// 자재입고조회(양산품) 검색 Method
        /// </summary>
        /// <param name="dtGR"> 검색조건이 저장되어있는 DataTable </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialGRMassProduct(DataTable dtGR, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtnGR = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                for (int i = 0; i < dtGR.Rows.Count; i++)
                {
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtGR.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRFromDate", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["GRFromDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRToDate", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["GRToDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["ConsumableTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.Char, dtGR.Rows[i]["GRFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    // SP 실행
                    dtRtnGR = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialGRMassProduct", dtParam);
                }
                return dtRtnGR;
            }
            catch (Exception ex)
            {
                return dtRtnGR;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtnGR.Dispose();
            }
        }

        /// <summary>
        /// 자재입고조회 화면에서 수입검사화면으로 이동할때 화면에 나와야 할 기본정보를 가져오기 위한 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <param name="strGRDate"> 입고일 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqInit(string strPlantCode, string strVendorCode, string strMaterialCode, string strGRDate, string strGRNo, string strSpecNo
                                                , string strMoldSeq, string strLang, string strMoveFormName)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, strGRDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, strGRNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, strSpecNo, 200);
                sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, strMoldSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                sql.mfAddParamDataRow(dtParam, "@i_strMoveFormName", ParameterDirection.Input, SqlDbType.NVarChar, strMoveFormName, 30);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqInit", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// SpecNo, 금형차수 저장 Method --> 웹수발주 개발전까지만 임시로 진행함.
        /// </summary>
        [AutoComplete(false)]
        public String mfSaveINSMaterialGR_IMSI(DataTable dtSave, String strUserID, String strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                string strOutput = string.Empty;

                //DB 연결
                sql.mfConnect();

                // 트랜잭션 변수 선언
                SqlTransaction trans;

                // 트랜잭션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["MoLotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["SpecNo"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MoldSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["GRFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strGRType", ParameterDirection.Output, SqlDbType.VarChar, 1);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialGR_IMSI", dtParam);

                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    // AML 등급판정 메소드 호출
                    strErrRtn = mfSaveINSMaterialGR_AML(dtSave, strUserID, strUserIP, sql.SqlCon, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                        trans.Commit();
                    else
                        trans.Rollback();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        /// <summary>
        /// SpecNo, 금형차수 저장 Method --> 웹수발주 개발전까지만 임시로 진행함.
        /// </summary>
        [AutoComplete(false)]
        public String mfSaveINSMaterialGR_AML(DataTable dtSave, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";
                string strOutput = string.Empty;

                //////DB 연결
                ////sql.mfConnect();

                ////// 트랜잭션 변수 선언
                ////SqlTransaction trans;

                ////// 트랜잭션 시작
                ////trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["MoLotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["SpecNo"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MoldSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["GRFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strGRType", ParameterDirection.Output, SqlDbType.VarChar, 1);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //////// SP 실행
                    //////strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialGR_AML", dtParam);
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialGR_AML", dtParam);

                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        //trans.Rollback();
                        break;
                    }
                    else
                    {
                        if (ErrRtn.mfGetReturnValue(0).Equals("N"))
                        {
                            strOutput = ErrRtn.mfGetReturnValue(0);
                        }
                    }
                }
                ////if (ErrRtn.ErrNum.Equals(0))
                ////    trans.Commit();

                ErrRtn.InterfaceResultMessage = strOutput;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }

        }


        /// <summary>
        /// 자재입고조회(양산품) 검색 Method
        /// </summary>
        /// <param name="dtGR"> 검색조건이 저장되어있는 DataTable </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMaterialGRMassProduct(DataTable dtGR)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = "";

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[0]["GRNo"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtGR.Rows[0]["LotNo"].ToString(), 50);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[0]["VendorCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[0]["MaterialCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strGRFromDate", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[0]["GRFromDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strGRToDate", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[0]["GRToDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[0]["ConsumableTypeCode"].ToString(), 40);
                sql.mfAddParamDataRow(dtParam, "@i_strGRFlag", ParameterDirection.Input, SqlDbType.Char, dtGR.Rows[0]["GRFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSMaterialGRMassProduct", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        [AutoComplete]
        public DataTable mfReadINSmaterialGR_Check(string strPlantCode, string strGRNo, string strMoLotNo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, strGRNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strMoLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strMoLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialGR_Check", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /////// <summary>
        /////// 저장 Method
        /////// </summary>
        /////// <param name="dtGR"> 저장에 필요한 데이터가 담겨있는 DataTable </param>
        /////// <param name="UserID"> 사용자IP </param>
        /////// <param name="UserIP"> 사용자ID </param>
        /////// <param name="sqlCon"> SqlConnention 변수 </param>
        /////// <param name="trans"> Transaction 변수 </param>
        /////// <returns></returns>
        ////[AutoComplete(false)]
        ////public String mfSaveINSMaterialGR(DataTable dtGR, String UserID, String UserIP, SqlConnection sqlCon, SqlTransaction trans)
        ////{
        ////    TransErrRtn ErrRtn = new TransErrRtn();
        ////    try
        ////    {
        ////        SQLS sql = new SQLS();
        ////        String strErrRtn = "";

        ////        for (int i = 0; i < dtGR.Rows.Count; i++)
        ////        {
        ////            // Parameter DataTable 설정
        ////            DataTable dtParam = sql.mfSetParamDataTable();
        ////            sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
        ////            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["PlantCode"].ToString(), 10);
        ////            sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["VendorCode"].ToString(), 10);
        ////            sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["MaterialCode"].ToString(), 20);
        ////            sql.mfAddParamDataRow(dtParam, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["GRDate"].ToString(), 10);
        ////            sql.mfAddParamDataRow(dtParam, "@i_MaterialReqFlag", ParameterDirection.Input, SqlDbType.VarChar, dtGR.Rows[i]["MaterialReqFlag"].ToString(), 1);
        ////            sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        ////            // SP 실행
        ////            strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialGR", dtParam);

        ////            // 결과검사
        ////            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////            if (ErrRtn.ErrNum != 0)
        ////            {
        ////                break;
        ////            }
        ////        }
        ////        return strErrRtn;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        ErrRtn.SystemInnerException = ex.InnerException.ToString();
        ////        ErrRtn.SystemMessage = ex.Message;
        ////        ErrRtn.SystemStackTrace = ex.StackTrace;
        ////        return ErrRtn.mfEncodingErrMessage(ErrRtn);
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialConfirmH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialConfirmH : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtConfirmHeader = new DataTable();
            try
            {
                dtConfirmHeader.Columns.Add("PlantCode", typeof(String));
                dtConfirmHeader.Columns.Add("StdNumber", typeof(String));
                dtConfirmHeader.Columns.Add("ReqID", typeof(String));
                dtConfirmHeader.Columns.Add("ReqDate", typeof(String));
                dtConfirmHeader.Columns.Add("ReqPurpose", typeof(String));
                dtConfirmHeader.Columns.Add("EtcDesc", typeof(String));
                dtConfirmHeader.Columns.Add("MSDSFileName", typeof(String));
                dtConfirmHeader.Columns.Add("ICPFileName", typeof(String));
                dtConfirmHeader.Columns.Add("ShipmentQty", typeof(Double));
                dtConfirmHeader.Columns.Add("ApplyDevice", typeof(string));
                dtConfirmHeader.Columns.Add("ReturnReason", typeof(String));
                dtConfirmHeader.Columns.Add("ReturnUserID", typeof(string));
                dtConfirmHeader.Columns.Add("ReturnDate", typeof(string));
                dtConfirmHeader.Columns.Add("S1IngFlag", typeof(String));
                dtConfirmHeader.Columns.Add("S2IngFlag", typeof(String));
                dtConfirmHeader.Columns.Add("S3IngFlag", typeof(String));
                dtConfirmHeader.Columns.Add("S4IngFlag", typeof(String));
                dtConfirmHeader.Columns.Add("ReceiptID", typeof(String));
                dtConfirmHeader.Columns.Add("ReceiptDate", typeof(String));
                dtConfirmHeader.Columns.Add("S1UserID", typeof(String));
                dtConfirmHeader.Columns.Add("S1WriteDate", typeof(String));
                dtConfirmHeader.Columns.Add("S1FileName", typeof(String));
                dtConfirmHeader.Columns.Add("S1EtcDesc", typeof(String));
                dtConfirmHeader.Columns.Add("S1Result", typeof(String));
                dtConfirmHeader.Columns.Add("S1CompleteFlag", typeof(string));
                dtConfirmHeader.Columns.Add("S2UserID", typeof(String));
                dtConfirmHeader.Columns.Add("S2WriteDate", typeof(String));
                dtConfirmHeader.Columns.Add("S2FileName", typeof(String));
                dtConfirmHeader.Columns.Add("S2EtcDesc", typeof(String));
                dtConfirmHeader.Columns.Add("S2Result", typeof(String));
                dtConfirmHeader.Columns.Add("S2CompleteFlag", typeof(string));
                dtConfirmHeader.Columns.Add("S3UserID", typeof(String));
                dtConfirmHeader.Columns.Add("S3WriteDate", typeof(String));
                dtConfirmHeader.Columns.Add("S3FileName", typeof(String));
                dtConfirmHeader.Columns.Add("S3EtcDesc", typeof(String));
                dtConfirmHeader.Columns.Add("S3Result", typeof(String));
                dtConfirmHeader.Columns.Add("S3MoveCode", typeof(string));
                dtConfirmHeader.Columns.Add("S3CompleteFlag", typeof(string));
                dtConfirmHeader.Columns.Add("S4UserID", typeof(String));
                dtConfirmHeader.Columns.Add("S4WriteDate", typeof(String));
                dtConfirmHeader.Columns.Add("S4FileName", typeof(String));
                dtConfirmHeader.Columns.Add("S4EtcDesc", typeof(String));
                dtConfirmHeader.Columns.Add("S4Result", typeof(String));
                dtConfirmHeader.Columns.Add("S4MoveCode", typeof(string));
                dtConfirmHeader.Columns.Add("S4CompleteFlag", typeof(string));
                dtConfirmHeader.Columns.Add("PassFailFlag", typeof(String));
                dtConfirmHeader.Columns.Add("CompleteFlag", typeof(String));
                dtConfirmHeader.Columns.Add("WriteID", typeof(String));
                dtConfirmHeader.Columns.Add("WriteDate", typeof(String));
                dtConfirmHeader.Columns.Add("ProgStatus", typeof(String));

                return dtConfirmHeader;
            }
            catch (Exception ex)
            {
                return dtConfirmHeader;
                throw (ex);
            }
            finally
            {
                dtConfirmHeader.Dispose();
            }
        }

        /// <summary>
        /// 반려용 DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetReturnDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("StdNumber", typeof(String));
                dt.Columns.Add("ReturnReason", typeof(String));

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// 조회용 DataTable 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetSearchDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("StdNumber", typeof(String));
                dt.Columns.Add("MaterialCode", typeof(String));
                dt.Columns.Add("MaterialName", typeof(string));
                dt.Columns.Add("LotNo", typeof(String));
                dt.Columns.Add("VendorCode", typeof(String));
                dt.Columns.Add("VendorName", typeof(string));
                dt.Columns.Add("ReqFromDate", typeof(String));
                dt.Columns.Add("ReqToDate", typeof(String));
                dt.Columns.Add("PassFailFlag", typeof(String));
                dt.Columns.Add("ProgStatus", typeof(String));
                dt.Columns.Add("ConsumableType", typeof(string));
                dt.Columns.Add("ReqUserID", typeof(string));
                dt.Columns.Add("CompleteFlag", typeof(string));

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 인증의뢰현황(검색조건에 따른 그리드 조회용)
        /// </summary>
        /// <param name="dtSearch"> 검색조건이 저장된 DataTable </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialConfirmH(DataTable dtSearch, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtnHeader = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialName", ParameterDirection.Input, SqlDbType.NVarChar, dtSearch.Rows[i]["MaterialName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSearch.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorName", ParameterDirection.Input, SqlDbType.NVarChar, dtSearch.Rows[i]["VendorName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqFromDate", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ReqFromDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqToDate", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ReqToDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strConsumableType", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ConsumableType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ReqUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    dtRtnHeader = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialConfirmH", dtParam);
                }
                return dtRtnHeader;
            }
            catch (Exception ex)
            {
                return dtRtnHeader;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtnHeader.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 인증의뢰현황 그리드 더블클릭시 헤더정보 상세조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialConfirmHDetail(String strPlantCode, String strStdNumber, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtHDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtHDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialConfirmHDetail", dtParam);

                return dtHDetail;
            }
            catch (Exception ex)
            {
                return dtHDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtHDetail.Dispose();
            }
        }

        /// <summary>
        /// 신규자재인증등록화면 검색조건에 따른 조회 Method(그리드)
        /// </summary>
        /// <param name="dtSearch"> 검색조건이 저장된 DataTable </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialConfirmHRegist(DataTable dtSearch, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialName", ParameterDirection.Input, SqlDbType.NVarChar, dtSearch.Rows[i]["MaterialName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPassFailFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["PassFailFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strProgStatus", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ProgStatus"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorName", ParameterDirection.Input, SqlDbType.NVarChar, dtSearch.Rows[i]["VendorName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqFromDate", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ReqFromDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqToDate", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ReqToDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strConsumableType", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ConsumableType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ReqUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialConfirmHRegist", dtParam);
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 인증등록 화면 Step 진행여부 하단 Control Data 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialConfirmHRegistDetail(String strPlantCode, String strStdNumber)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_StrPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialConfirmHRegistDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 저장 Method(신규자재인증의뢰)
        /// </summary>
        /// <param name="dtHeader"> 헤더저장에 필요한 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="dtDetail"> 상세정보 저장에 필요한 DataTable </param>
        /// <param name="dtGR"> 수입자재입고정보 테이블 저장에 필요한 DataTable <param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveINSMaterialConfirmH(DataTable dtHeader, String strUserID, String strUserIP, DataTable dtDetail, DataTable dtFile)
        {
            // 인스턴스 객체 생성
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction변수 선언
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqPurpose", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ReqPurpose"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strMSDSFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["MSDSFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strICPFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ICPFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_dblShipmentQty", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["ShipmentQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strApplyDevice", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ApplyDevice"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialConfirmH", dtParam);

                    // 결과체크
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        String strRtnStdNumber = ErrRtn.mfGetReturnValue(0);
                        if (strRtnStdNumber != dtHeader.Rows[i]["StdNumber"].ToString())
                        {
                            String strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                            if (dtDetail.Rows.Count > 0)
                            {
                                // 상세정보 저장
                                MaterialConfirmD clsDetail = new MaterialConfirmD();
                                // Method 호출
                                strErrRtn = clsDetail.mfSaveINSMaterialConfirmD(strPlantCode, strRtnStdNumber, dtDetail, strUserID, strUserIP, sql.SqlCon, trans);

                                // 결과 확인
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            if (dtFile.Rows.Count > 0)
                            {
                                // 첨부파일정보 저장
                                MaterialConfirmFile clsFile = new MaterialConfirmFile();
                                strErrRtn = clsFile.mfSaveMaterialConfirmFIle(dtFile, strRtnStdNumber, strUserID, strUserIP, sql.SqlCon, trans);

                                // 결과 확인
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                        }

                        if(ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 저장 Method(신규자재인증의뢰현황)
        /// </summary>
        /// <param name="dtHeader"> 저장정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveINSMaterialConfirmH(DataTable dtHeader, String strUserID, String strUserIP, DataTable dtFile)
        {
            // 인스턴스 객체 생성
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction변수 선언
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqPurpose", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ReqPurpose"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strMSDSFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["MSDSFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strICPFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ICPFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_dblShipmentQty", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["ShipmentQty"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strApplyDevice", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ApplyDevice"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialConfirmH", dtParam);

                    // 결과체크
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        String strRtnStdNumber = ErrRtn.mfGetReturnValue(0);

                        // 첨부파일정보 저장
                        MaterialConfirmFile clsFile = new MaterialConfirmFile();
                        strErrRtn = clsFile.mfSaveMaterialConfirmFIle(dtFile, strRtnStdNumber, strUserID, strUserIP, sql.SqlCon, trans);

                        // 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 신규자재인증등록 저장 Method
        /// </summary>
        /// <param name="dtHeader"> 헤더정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserID"> 사용자IP </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveINSMaterialConfirmHRegist(DataTable dtHeader, String strUserID, String strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReturnReason", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ReturnReason"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strS1IngFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S1IngFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strS2IngFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S2IngFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strS3IngFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S3IngFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strS4IngFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S4IngFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReceiptID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReceiptDate"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strS1UserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S1UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strS1WriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S1WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strS1FileName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["S1FileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strS1EtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["S1EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strS1Result", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S1Result"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strS1CompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["S1CompleteFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strS2UserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S2UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strS2WriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S2WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strS2FileName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["S2FileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strS2EtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["S2EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strS2Result", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S2Result"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strS2CompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["S2CompleteFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strS3UserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S3UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strS3WriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S3WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strS3FileName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["S3FileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strS3EtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["S3EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strS3Result", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S3Result"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strS3MoveCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S3MoveCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strS3CompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["S3CompleteFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strS4UserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S4UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strS4WriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S4WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strS4FileName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["S4FileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strS4EtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["S4EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strS4Result", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S4Result"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strS4MoveCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["S4MoveCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strS4CompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["S4CompleteFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strPassFailFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PassFailFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProgStatus", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ProgStatus"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialConfirmHRegist", dtParam);

                    // 에러 체크
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        ///// <summary>
        ///// 신규자재인증등록 반려시 저장 Method
        ///// </summary>
        ///// <param name="dtReturn"> 저장정보가 담긴 DataTable </param>
        ///// <param name="strUserID"> 사용자ID </param>
        ///// <param name="strUserIP"> 사용자IP </param>
        ///// <returns></returns>
        //[AutoComplete(false)]
        //public String mfSaveINSMaterialConfirmHReturn(DataTable dtReturn, String strUserID, String strUserIP)
        //{
        //    SQLS sql = new SQLS();
        //    TransErrRtn ErrRtn = new TransErrRtn();
        //    String strErrRtn = "";
        //    try
        //    {
        //        sql.mfConnect();
        //        // Transaction 시작
        //        SqlTransaction trans = sql.SqlCon.BeginTransaction();

        //        for (int i = 0; i < dtReturn.Rows.Count; i++)
        //        {
        //            DataTable dtParam = sql.mfSetParamDataTable();
        //            sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
        //            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtReturn.Rows[i]["PlantCode"].ToString(), 10);
        //            sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtReturn.Rows[i]["StdNumber"].ToString(), 20);
        //            sql.mfAddParamDataRow(dtParam, "@i_strReturnReason", ParameterDirection.Input, SqlDbType.NVarChar, dtReturn.Rows[i]["ReturnReason"].ToString(), 500);
        //            sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
        //            sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
        //            sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        //            strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialConfirmHReturn", dtParam);

        //            // 결과확인
        //            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        //            if (ErrRtn.ErrNum != 0)
        //            {
        //                trans.Rollback();
        //                break;
        //            }
        //        }
        //        if (ErrRtn.ErrNum.Equals(0))
        //        {
        //            trans.Commit();
        //        }
        //        return strErrRtn;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrRtn.SystemInnerException = ex.InnerException.ToString();
        //        ErrRtn.SystemMessage = ex.Message;
        //        ErrRtn.SystemStackTrace = ex.StackTrace;
        //        return ErrRtn.mfEncodingErrMessage(ErrRtn);
        //    }
        //    finally
        //    {
        //        sql.mfDisConnect();
        //        sql.Dispose();
        //    }
        //}

        /////// <summary>
        /////// 신규자재 인증등록 헤더 삭제
        /////// </summary>
        /////// <param name="strPlantCode">공장코드</param>
        /////// <param name="strStdNumber">관리번호</param>
        /////// <returns></returns>
        ////public string mfDeleteINSMaterialConfirmH(string strPlantCode, string strStdNumber)
        ////{
        ////    SQLS sql = new SQLS();
        ////    TransErrRtn ErrRtn = new TransErrRtn();
        ////    try
        ////    {
        ////        string strErrRtn = "";
        ////        // DB 연결
        ////        sql.mfConnect();
        ////        // 트랜잭션 시작
        ////        SqlTransaction trans = sql.SqlCon.BeginTransaction();

        ////        // 상세정보부터 삭제
        ////        MaterialConfirmD clsDetail = new MaterialConfirmD();
        ////        strErrRtn = clsDetail.mfDeleteINSMaterialConfirmD(strPlantCode, strStdNumber, sql.SqlCon, trans);

        ////        // ErrorCheck
        ////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////        if (ErrRtn.ErrNum != 0)
        ////        {
        ////            trans.Rollback();
        ////            return strErrRtn;
        ////        }
        ////        else
        ////        {
        ////            // 성공시 헤더 삭제
        ////            DataTable dtParam = sql.mfSetParamDataTable();
        ////            sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
        ////            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        ////            sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
        ////            sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        ////            // SP 실행
        ////            strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSMaterialConfirmH", dtParam);

        ////            // ErrorCheck
        ////            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        ////            if (ErrRtn.ErrNum != 0)
        ////            {
        ////                trans.Rollback();
        ////                return strErrRtn;
        ////            }
        ////            else
        ////            {
        ////                trans.Commit();
        ////            }
        ////        }
        ////        return strErrRtn;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        ErrRtn.SystemInnerException = ex.InnerException.ToString();
        ////        ErrRtn.SystemMessage = ex.Message;
        ////        ErrRtn.SystemStackTrace = ex.StackTrace;
        ////        return ErrRtn.mfEncodingErrMessage(ErrRtn);
        ////    }
        ////    finally
        ////    {
        ////        sql.mfDisConnect();
        ////    }
        ////}
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialConfirmD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialConfirmD : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtDetail = new DataTable();
            try
            {
                dtDetail.Columns.Add("GRNo", typeof(String));
                dtDetail.Columns.Add("LotNo", typeof(String));
                dtDetail.Columns.Add("GRQty", typeof(Double));

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 인증의뢰현황 상세 그리드 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialConfirmD(String strPlantCode, String strStdNumber)
        {
            SQLS sql = new SQLS();
            DataTable dtDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);

                dtDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialConfirmD", dtParam);

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 신규자재인증의뢰 상세 저장 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <param name="dtDetail"> LotNo, 관리번호, 수량등이 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveINSMaterialConfirmD(String strPlantCode, String strStdNumber, DataTable dtDetail, String strUserID, String strUserIP
                                                , SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_dblGRQty", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["GRQty"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Input, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialConfirmD", dtParam);

                    // 결과 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /////// <summary>
        /////// 신규자재인증등록 상세 삭제
        /////// </summary>
        /////// <param name="strPlantCode">공장코드</param>
        /////// <param name="strStdNumber">관리번호</param>
        /////// <param name="sqlCon">SqlConnection</param>
        /////// <param name="trans">트랜잭션변수</param>
        /////// <returns></returns>
        ////public string mfDeleteINSMaterialConfirmD(string strPlantCode, string strStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        ////{
        ////    TransErrRtn ErrRtn = new TransErrRtn();
        ////    try
        ////    {
        ////        SQLS sql = new SQLS();

        ////        // 파라미터 데이터테이블 설정
        ////        DataTable dtParam = sql.mfSetParamDataTable();
        ////        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
        ////        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        ////        sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
        ////        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        ////        // SP 실행
        ////        string strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMaterialConfirmD", dtParam);

        ////        return strErrRtn;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        ErrRtn.SystemInnerException = ex.InnerException.ToString();
        ////        ErrRtn.SystemMessage = ex.Message;
        ////        ErrRtn.SystemStackTrace = ex.StackTrace;
        ////        return ErrRtn.mfEncodingErrMessage(ErrRtn);
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialConfirmReturn")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialConfirmReturn : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("StdNumber", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("ReturnReason", typeof(string));
                dtRtn.Columns.Add("ReturnDate", typeof(string));
                dtRtn.Columns.Add("ReturnUserID", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규자재 반려 테이블 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMaterialConfirmReturn(string strPlantCode, string strStdNumber)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialConfirmReturn", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 신규자재 반려테이블 저장 메소드
        /// </summary>
        /// <param name="dtSave">정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMaterialConfirmReturn(DataTable dtSave, string strUserID, string strUserIP)//, SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtparam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtparam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtparam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtparam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtparam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtparam, "@i_strReturnReason", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ReturnReason"].ToString(), 500);
                    sql.mfAddParamDataRow(dtparam, "@i_strReturnDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReturnDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtparam, "@i_strReturnUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReturnUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtparam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtparam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtparam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtparam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialConfirmReturn", dtparam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 반려 테이블 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        /// <param name="intSeq">순번(현재사용안함:0으로 넘기면됨)</param>
        /// <param name="sqlCon">SqlConnection</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteMaterialConfirmReturn(string strPlantCode, string strStdNumber, Int32 intSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, intSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMaterialConfirmReturn", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialConfirmFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialConfirmFile : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("StdNumber", typeof(string));
                dtRtn.Columns.Add("UniqueKey", typeof(string));
                dtRtn.Columns.Add("FileTitle", typeof(string));
                dtRtn.Columns.Add("FilePath", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("CompleteDate", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 신규자재인증 첨부파일 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMaterialConfirmFile(string strPlantCode, string strStdNumber, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialConfirmFile", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 신규자재인증 첨부파일 저장 메소드
        /// </summary>
        /// <param name="dtFile">File 정보 저장된 데이터 테이블</param>
        /// <param name="strStdNumber">관리번호</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SqlTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMaterialConfirmFIle(DataTable dtFile, string strStdNumber, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtFile.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFile.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUniqueKey", ParameterDirection.Input, SqlDbType.VarChar, dtFile.Rows[i]["UniqueKey"].ToString(), 21);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtFile.Rows[i]["FileTitle"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFile.Rows[i]["FilePath"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFile.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtFile.Rows[i]["CompleteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialConfirmFile", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 신규자재 첨부파일 삭제 메소드
        /// </summary>
        /// <param name="dtFile">삭제정보가 담긴 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteMaterialConfirmFile(DataTable dtFile)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;
                for (int i = 0; i < dtFile.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFile.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtFile.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUniqueKey", ParameterDirection.Input, SqlDbType.VarChar, dtFile.Rows[i]["UniqueKey"].ToString(), 21);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSMaterialConfirmFile", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectReqH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectReqH : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        /// <summary>
        /// 생성자
        /// </summary>
        public MatInspectReqH()
        {
        }
        /// <summary>
        /// 디버깅용 생성자
        /// </summary>
        /// <param name="strDBConn"></param>
        public MatInspectReqH(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReceiptDate", typeof(string));
                dtRtn.Columns.Add("ReceiptTime", typeof(string));
                dtRtn.Columns.Add("MaterialGrade", typeof(string));
                dtRtn.Columns.Add("InspectCount", typeof(int));
                dtRtn.Columns.Add("PassFailFlag", typeof(string));
                dtRtn.Columns.Add("InspectResultType", typeof(string));
                dtRtn.Columns.Add("FaultDesc", typeof(string));
                dtRtn.Columns.Add("CompleteFlag", typeof(string));
                dtRtn.Columns.Add("ICPCheckFlag", typeof(string));
                dtRtn.Columns.Add("TotalQty", typeof(double));
                dtRtn.Columns.Add("SamplingLotCount", typeof(int));
                dtRtn.Columns.Add("InspectUserID", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("InspectTime", typeof(string));
                dtRtn.Columns.Add("ReviewUserID", typeof(string));
                dtRtn.Columns.Add("ReviewDate", typeof(string));
                dtRtn.Columns.Add("AdmitUserID", typeof(string));
                dtRtn.Columns.Add("AdmitDate", typeof(string));
                dtRtn.Columns.Add("EtcDescQA", typeof(string));
                dtRtn.Columns.Add("WMSTransFlag", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// WMS 전송용 DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetWMSDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("GRNo", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(char));
                dtRtn.Columns.Add("InspectDesc", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ValidationDate", typeof(string));
                dtRtn.Columns.Add("IFFlag", typeof(char));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사결과정보 WMS에 전송
        /// </summary>
        /// <param name="dtSave">저장정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveWMSMATInspectReq(DataTable dtSave, string strUserID, string strUserIP)
        {
            #region 변경전
            ////QRPDB.ORADB oraDB = new QRPDB.ORADB();
            ////TransErrRtn ErrRtn = new TransErrRtn();
            ////try
            ////{
            ////    string strErrRtn = string.Empty;
            ////    string strWMSErrRtn = string.Empty;
            ////    //WMS Oracle DB 연결정보 읽기
            ////    QRPSYS.BL.SYSPGM.SystemAccessInfo clsPgm = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
            ////    DataTable dtPgm = clsPgm.mfReadSystemAccessInfoDetail(dtSave.Rows[0]["PlantCode"].ToString(), "S05");
            ////    oraDB.mfSetORADBConnectString(dtPgm.Rows[0]["SystemAddressPath"].ToString(), dtPgm.Rows[0]["AccessID"].ToString(), dtPgm.Rows[0]["AccessPassword"].ToString());
            ////    //oraDB.mfSetORADBConnectString("STS_MES_TEST", "wmsuser", "wms119");

            ////    //WMS Oracle DB연결하기
            ////    oraDB.mfConnect();

            ////    //Transaction 시작
            ////    OracleTransaction trans;
            ////    trans = oraDB.SqlCon.BeginTransaction();

            ////    for (int i = 0; i < dtSave.Rows.Count; i++)
            ////    {
            ////        //WMS 처리 SP 호출
            ////        DataTable dtParam = oraDB.mfSetParamDataTable();
            ////        oraDB.mfAddParamDataRow(dtParam, "V_PLANTCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["PlantCode"].ToString(), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_CONSTRACTCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["GRNo"].ToString(), 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_LOTNO", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["LotNo"].ToString(), 50);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_RESULTFLAG", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERIP", ParameterDirection.Input, OracleDbType.Varchar2, strUserIP, 15);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERID", ParameterDirection.Input, OracleDbType.Varchar2, strUserID, 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_VALIDATIONDATE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["ValidationDate"].ToString(), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_IFFLAG", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["IFFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "RTN", ParameterDirection.Output, OracleDbType.Varchar2, 2);

            ////        strErrRtn = oraDB.mfExecTransStoredProc(oraDB.SqlCon, trans, "UP_UPDATE_INSINSPECTRESULT", dtParam);
            ////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            ////        //WMS 처리결과에 따른 Transaction 처리
            ////        strWMSErrRtn = ErrRtn.mfGetReturnValue(0);
            ////        if (!strWMSErrRtn.Equals("00"))
            ////        {
            ////            trans.Rollback();
            ////            break;
            ////        }
            ////    }
            ////    if (strWMSErrRtn.Equals("00"))
            ////        trans.Commit();

            ////    clsPgm.Dispose();
            ////    oraDB.Dispose();

            ////    return strErrRtn;
            ////}
            ////catch (Exception ex)
            ////{
            ////    return ex.Message;
            ////}
            ////finally
            ////{
            ////    oraDB.mfDisConnect();
            ////}
            #endregion
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                //SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strValidationDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ValidationDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strIFFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["IFFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_INSMatInspectReqH_WMS_Send", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrMessage.Equals("00") || !ErrRtn.ErrNum.Equals(0))
                    {
                        //trans.Rollback();
                        break;
                    }
                }

                //if (ErrRtn.ErrNum.Equals(0))
                //    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// WMS 전송성공후 WMSTFlag Update 메소드
        /// </summary>
        /// <param name="dtWMS">WMS 저장정보가 담긴 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMatInspectReqH_WMSTFlag(DataTable dtWMS, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtWMS.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtWMS.Rows[0]["ReqNo"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtWMS.Rows[0]["ReqSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMatInspectReqH_WMSFlag", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수입검사등록/조회 화면 검색조건에 따른 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="strGRFromDate"> 검사일From </param>
        /// <param name="strGRToDate"> 검사일To </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqH(String strPlantCode, String strMaterialCode, String strVendorCode
                                                , string strConsumableTypeCode, string strLotNo, string strInspectResultFlag
                                                , String strFromGRDate, String strToGRDate, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            try
            {
                // DB 연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strConsumableTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, strInspectResultFlag, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strFromGRdate", ParameterDirection.Input, SqlDbType.VarChar, strFromGRDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToGRDate", ParameterDirection.Input, SqlDbType.VarChar, strToGRDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqH", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사등록/조회 그리드 더블클릭시 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInsMatInspectReqHDetail(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqHDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 헤더정보 저장 Method(검사Lot적용 버튼 클릭시 저장)
        /// </summary>
        /// <param name="dtHeader"> 헤더정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="dtLot"> Lot 정보가 담긴 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveINSMatInspectReqH(DataTable dtHeader, String strUserID, String strUserIP, DataTable dtLot)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 변수 선언
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReceiptDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptTime", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReceiptTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialGrade", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["MaterialGrade"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intInspectCount", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["InspectCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strPassFailFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PassFailFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultType", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectResultType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["FaultDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strICPCheckFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ICPCheckFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblTotalQty", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["TotalQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSamplingLotCount", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["SamplingLotCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTime", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReviewUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReviewUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReviewDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReviewDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AdmitUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDescQA", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDescQA"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strWMSTransFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WMSTransFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMatInspectReqH", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        // 실패일경우 Rollback
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        String strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                        // OUTPUT 값 저장
                        String strReqNo = ErrRtn.mfGetReturnValue(0);
                        String strReqSeq = ErrRtn.mfGetReturnValue(1);

                        // 검사결과 삭제
                        MatInspectResultMeasure clsMeasure = new MatInspectResultMeasure();
                        strErrRtn = clsMeasure.mfDeleteMatInspectResultMeasure(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // 계수형
                        MatInspectResultCount clsCount = new MatInspectResultCount();
                        strErrRtn = clsCount.mfDeleteMatInspectResultCount(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // OK/NG
                        MatInspectResultOkNg clsOkNg = new MatInspectResultOkNg();
                        strErrRtn = clsOkNg.mfDeleteMatInspectResultOkNg(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // 설명
                        MatInspectResultDesc clsDesc = new MatInspectResultDesc();
                        strErrRtn = clsDesc.mfDeleteMatInspectResultDesc(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // 선택
                        MatInspectResultSelect clsSelect = new MatInspectResultSelect();
                        strErrRtn = clsSelect.mfDeleteMatInspectResultSelect(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // 기존 Item 정보 삭제
                        MatInspectReqItem clsItem = new MatInspectReqItem();
                        strErrRtn = clsItem.mfDeleteINSMatInspectReqItem(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        // 기존 COCFile 정보 삭제
                        MatInspectReqLotCOCFile clsCOC = new MatInspectReqLotCOCFile();
                        strErrRtn = clsCOC.mfDeleteINSMatInspectReqLotCOCFile_All(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        // 기존 Lot 정보 삭제
                        MatInspectReqLot clsLot = new MatInspectReqLot();
                        strErrRtn = clsLot.mfDeleteINSMatInspectReqLot(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        // Lot정보 저장
                        strErrRtn = clsLot.mfSaveINSMatInspectReqLot(dtLot, strReqNo, strReqSeq, strUserID, strUserIP, sql.SqlCon, trans);

                        // 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        // COCFile 정보 저장
                        strErrRtn = clsCOC.mfSaveINSMatInspectReqLotCOCFile_FromWMS(strPlantCode, strReqNo, strReqSeq, strUserID, strUserIP, sql.SqlCon, trans);
                        // 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }

                    if (ErrRtn.ErrNum.Equals(0))
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수입검사등록 저장 Method
        /// </summary>
        /// <param name="dtHeader"> 헤더정보 DataTable </param>
        /// <param name="dtLot"> Lot정보 DataTable </param>
        /// <param name="dtItem"> Item 정보 DataTable </param>
        /// <param name="dtMeasure"> DataType가 계량인 검사결과 DataTable </param>
        /// <param name="dtCount"> DataType가 계수인 검사결과 DataTable </param>
        /// <param name="dtOkNg"> DataType가 OK/NG인 검사결과 DataTable </param>
        /// <param name="dtDesc"> DataType가 설명인 검사결과 DataTable </param>
        /// <param name="dtSelect"> DataType가 선택인 검사결과 DataTable </param>
        /// <param name="dtAML"> 수입검사 작성완료시 AML정보 저장하기 위한 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveINSMatInspectReqH(DataTable dtHeader, DataTable dtLot, DataTable dtItem
                                            , DataTable dtMeasure, DataTable dtCount, DataTable dtOkNg, DataTable dtDesc, DataTable dtSelect, DataTable dtAML
                                            , String strUserID, String strUserIP, String strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                String strErrRtn = "";
                // 디비 연결
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();
                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReceiptDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptTime", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReceiptTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialGrade", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["MaterialGrade"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intInspectCount", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["InspectCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strPassFailFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PassFailFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultType", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectResultType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["FaultDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strICPCheckFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ICPCheckFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblTotalQty", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["TotalQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSamplingLotCount", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["SamplingLotCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTime", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReviewUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReviewUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReviewDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReviewDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AdmitUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDescQA", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDescQA"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strWMSTransFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WMSTransFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMatInspectReqH", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        // 실패일경우 Rollback
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        String strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                        // OUTPUT 값 저장
                        String strReqNo = ErrRtn.mfGetReturnValue(0);
                        String strReqSeq = ErrRtn.mfGetReturnValue(1);

                        // 검사결과 삭제
                        MatInspectResultMeasure clsMeasure = new MatInspectResultMeasure();
                        strErrRtn = clsMeasure.mfDeleteMatInspectResultMeasure(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // 계수형
                        MatInspectResultCount clsCount = new MatInspectResultCount();
                        strErrRtn = clsCount.mfDeleteMatInspectResultCount(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // OK/NG
                        MatInspectResultOkNg clsOkNg = new MatInspectResultOkNg();
                        strErrRtn = clsOkNg.mfDeleteMatInspectResultOkNg(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // 설명
                        MatInspectResultDesc clsDesc = new MatInspectResultDesc();
                        strErrRtn = clsDesc.mfDeleteMatInspectResultDesc(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        // 선택
                        MatInspectResultSelect clsSelect = new MatInspectResultSelect();
                        strErrRtn = clsSelect.mfDeleteMatInspectResultSelect(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        ////// 기존 Item 정보 삭제
                        ////MatInspectReqItem clsItem = new MatInspectReqItem();
                        ////strErrRtn = clsItem.mfDeleteINSMatInspectReqItem(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        ////// 결과 확인
                        ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        ////if (ErrRtn.ErrNum != 0)
                        ////{
                        ////    trans.Rollback();
                        ////    break;
                        ////}

                        ////// 기존 Lot 정보 삭제
                        ////MatInspectReqLot clsLot = new MatInspectReqLot();
                        ////strErrRtn = clsLot.mfDeleteINSMatInspectReqLot(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                        ////// 결과 확인
                        ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        ////if (ErrRtn.ErrNum != 0)
                        ////{
                        ////    trans.Rollback();
                        ////    break;
                        ////}

                        // Lot정보 저장
                        if (dtLot.Rows.Count > 0)
                        {
                            MatInspectReqLot clsLot = new MatInspectReqLot();
                            strErrRtn = clsLot.mfSaveINSMatInspectReqLot(dtLot, strReqNo, strReqSeq, strUserID, strUserIP, sql.SqlCon, trans);

                            // 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }

                            // Item 저장
                            MatInspectReqItem clsItem = new MatInspectReqItem();
                            strErrRtn = clsItem.mfSaveInsMatInspectReqItem(dtItem, strUserID, strUserIP, sql.SqlCon, trans);

                            // 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }

                            // 데이터 유형에 따른 검사결과 저장 Method 호출
                            // 계량형
                            if (dtMeasure.Rows.Count > 0)
                            {
                                //QRPINS.BL.INSIMP.MatInspectResultMeasure clsMeasure = new QRPINS.BL.INSIMP.MatInspectResultMeasure();
                                strErrRtn = clsMeasure.mfSaveMatInspectResultMeasure(dtMeasure, strUserIP, strUserID, sql.SqlCon, trans);

                                // 결과 검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                            // 계수형
                            if (dtCount.Rows.Count > 0)
                            {
                                //QRPINS.BL.INSIMP.MatInspectResultCount clsCount = new QRPINS.BL.INSIMP.MatInspectResultCount();
                                strErrRtn = clsCount.mfSaveMatInspectResultCount(dtCount, strUserIP, strUserID, sql.SqlCon, trans);

                                // 결과 검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                            // OkNg
                            if (dtOkNg.Rows.Count > 0)
                            {
                                //QRPINS.BL.INSIMP.MatInspectResultOkNg clsOkNg = new QRPINS.BL.INSIMP.MatInspectResultOkNg();
                                strErrRtn = clsOkNg.mfSaveMatInspectResultOkNg(dtOkNg, strUserIP, strUserID, sql.SqlCon, trans);

                                // 결과 검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                            // 설명
                            if (dtDesc.Rows.Count > 0)
                            {
                                //QRPINS.BL.INSIMP.MatInspectResultDesc clsDesc = new QRPINS.BL.INSIMP.MatInspectResultDesc();
                                strErrRtn = clsDesc.mfSaveMatInspectResultDesc(dtDesc, strUserIP, strUserID, sql.SqlCon, trans);

                                // 결과 검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                            // 선택
                            if (dtSelect.Rows.Count > 0)
                            {
                                //QRPINS.BL.INSIMP.MatInspectResultSelect clsSelect = new QRPINS.BL.INSIMP.MatInspectResultSelect();
                                strErrRtn = clsSelect.mfSaveMatInspectResultSelect(dtSelect, strUserIP, strUserID, sql.SqlCon, trans);

                                // 결과 검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }

                            // AML
                            if (dtAML.Rows.Count > 0)
                            {
                                QRPINS.BL.INSIMP.AML clsAML = new QRPINS.BL.INSIMP.AML();
                                strErrRtn = clsAML.mfSaveINSAMLRegist(dtAML, strUserID, strUserIP, sql.SqlCon, trans);

                                // 결과 검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                        }
                    }
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        /// <summary>
        /// 수입검사 헤더 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰번호 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMatInspectReqH(String strPlantCode, String strReqNo, String strReqSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                String strErrRtn = "";
                // DB 연결
                sql.mfConnect();

                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                ////// 수입검사결과 테이블부터 먼저 삭제
                ////// 계량형
                ////MatInspectResultMeasure clsMeasure = new MatInspectResultMeasure();
                ////strErrRtn = clsMeasure.mfDeleteMatInspectResultMeasure(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                ////// 결과검사
                ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    trans.Rollback();
                ////    return strErrRtn;
                ////}

                ////// 계수형
                ////MatInspectResultCount clsCount = new MatInspectResultCount();
                ////strErrRtn = clsCount.mfDeleteMatInspectResultCount(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                ////// 결과검사
                ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    trans.Rollback();
                ////    return strErrRtn;
                ////}

                ////// OK/NG
                ////MatInspectResultOkNg clsOkNg = new MatInspectResultOkNg();
                ////strErrRtn = clsOkNg.mfDeleteMatInspectResultOkNg(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                ////// 결과검사
                ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    trans.Rollback();
                ////    return strErrRtn;
                ////}

                ////// 설명
                ////MatInspectResultDesc clsDesc = new MatInspectResultDesc();
                ////strErrRtn = clsDesc.mfDeleteMatInspectResultDesc(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                ////// 결과검사
                ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    trans.Rollback();
                ////    return strErrRtn;
                ////}

                ////// 선택
                ////MatInspectResultSelect clsSelect = new MatInspectResultSelect();
                ////strErrRtn = clsSelect.mfDeleteMatInspectResultSelect(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                ////// 결과검사
                ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    trans.Rollback();
                ////    return strErrRtn;
                ////}

                ////// Item 정보 삭제
                ////MatInspectReqItem clsItem = new MatInspectReqItem();
                ////strErrRtn = clsItem.mfDeleteINSMatInspectReqItem(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                ////// 결과검사
                ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    trans.Rollback();
                ////    return strErrRtn;
                ////}

                ////// Lot 정보 삭제
                ////MatInspectReqLot clsLot = new MatInspectReqLot();
                ////strErrRtn = clsLot.mfDeleteINSMatInspectReqLot(strPlantCode, strReqNo, strReqSeq, sql.SqlCon, trans);
                ////// 결과검사
                ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    trans.Rollback();
                ////    return strErrRtn;
                ////}

                // 헤더정보 삭제
                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Input, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSMatInspectReqH", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        #region 수입검사성적서 Report

        /// <summary>
        /// 수입검사 성적서 Header
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInsMatInspectReq_Report_Header(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReq_Report_Header", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 수입검사 성적서 불량 정보조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInsMatInspectReq_Report_Abnormal(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReq_Report_Abnormal", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 성적서 검사결과 정보조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInsMatInspectReq_Report_Result(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReq_Report_Result", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 수입검사 성적서 검사Data 정보조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInsMatInspectReq_Report_Data(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReq_Report_DATA", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        #endregion


    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectReqLot")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectReqLot : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(int));
                dtRtn.Columns.Add("GRNo", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("COCFilePath", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("MaterialCode", typeof(string));
                dtRtn.Columns.Add("LotSize", typeof(double));
                dtRtn.Columns.Add("InspectResultFlag", typeof(string));
                dtRtn.Columns.Add("SamplingLotFlag", typeof(string));
                dtRtn.Columns.Add("AcceptCount", typeof(Int32));
                dtRtn.Columns.Add("RejectCount", typeof(Int32));
                dtRtn.Columns.Add("AQLSampleSize", typeof(double));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Abnormal Result 결과 저장용 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_Abnormal()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("AbnormalResultFlag", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사/조회 그리드 더블클릭시 Lot정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqLot(String strPlantCode, String strReqNo, String strReqSeq)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqLot", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사등록 Lot 정보 저장 Method
        /// </summary>
        /// <param name="dtLot"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="Sqlcon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveINSMatInspectReqLot(DataTable dtLot, String strReqNo, String strReqSeq, String strUserID, String strUserIP, SqlConnection Sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCOCFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["COCFilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_dblLotSize", ParameterDirection.Input, SqlDbType.Decimal, dtLot.Rows[i]["LotSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strSamplingLotFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["SamplingLotFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intAcceptCount", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["AcceptCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intRejectCount", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["RejectCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblAQLSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtLot.Rows[i]["AQLSampleSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_INSMatInspectReqLot", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰번호순번 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteINSMatInspectReqLot(String strPlantCode, String strReqNo, String strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectReqLot", dtParam);

                // 결과 검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 원자재이상발생 WMS 전송후 AbnormalResult Flag Update 메소드
        /// </summary>
        /// <param name="dtSave">저장할 정보가 들어있는 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMatInspectReqLot_Abnormal_Result(DataTable dtSave, string strAbnormalResultID, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                DataTable dtParam = new DataTable();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AbnormalResultFlag"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalResultID", ParameterDirection.Input, SqlDbType.VarChar, strAbnormalResultID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMatInspectReqLot_AbnormalResult", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 원자재이상발생 Lot_Hold 관리 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장정보 데이터 테이블</param>
        /// <param name="strAbnormalReleaseID">ReleaseUserID</param>
        /// <param name="strUserID">로그인사용자아이디</param>
        /// <param name="strUserIP">로그인사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMatInspectReqLot_Abnormal_Release(DataTable dtSave, string strAbnormalReleaseID, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                DataTable dtParam = new DataTable();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AbnormalResultFlag"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalReleaseID", ParameterDirection.Input, SqlDbType.VarChar, strAbnormalReleaseID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMatInspectReqLot_AbnormalRelease", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 원자재이상발생 Lot_Hold관리 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqLot_AbnormalHold(string strPlantCode, string strMaterialCode, string strLotNo
                                                                , string strFromGRDate, string strToGRDate, string strHistoryCheck, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strFromGRDate", ParameterDirection.Input, SqlDbType.VarChar, strFromGRDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToGRDate", ParameterDirection.Input, SqlDbType.VarChar, strToGRDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strHistoryCheck", ParameterDirection.Input, SqlDbType.VarChar, strHistoryCheck, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqLot_AbnormalHold", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 공정이상LotNo  MES/IF Release 
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="dtMESReleaseInfo"></param>
        /// <param name="strUserID">작성자</param>
        /// <param name="strComment">Comment</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strFormName">폼명</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable mfSaveINSMatInspectReqLot_MESRELEASE(string strPlantCode,
                                                            DataTable dtMESReleaseInfo,
                                                            string strUserID,
                                                            string strComment,
                                                            string strUserIP,
                                                            string strFormName)
        {

            SQLS sql = new SQLS();
            DataTable dtMES = new DataTable();
            try
            {

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S04"); //Live
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S07");    //Test

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                //MESLot정보 전송
                dtMES = clsTibrv.CON_RELEASE4QC_REQ(strFormName             //폼명
                                                    , strUserID             //작성자
                                                    , strComment            //Comment
                                                    , dtMESReleaseInfo      //LotList
                                                    , strUserIP);
                clsTibrv.Dispose();

                return dtMES;
            }
            catch (Exception ex)
            {
                return dtMES;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtMES.Dispose();
                sql.Dispose();
            }

        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectReqLotCOCFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectReqLotCOCFile : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(int));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("COCFileName", typeof(string));
                dtRtn.Columns.Add("COCFileValue", typeof(byte[]));
                dtRtn.Columns.Add("DeleteFlag", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 COCFile 조회(리스트)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqLotCOCFile(string strPlantCode, string strReqNo, string strReqSeq, Int32 intReqLotSeq)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqLotCOCFile", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 COCFile 데이터 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호/param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="intSeq">순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqLotCOCFile_Value(string strPlantCode, string strReqNo, string strReqSeq, Int32 intReqLotSeq, Int32 intSeq)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, intSeq.ToString());
                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqLotCOCFile_Value", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 COCFile 저장
        /// </summary>
        /// <param name="dtCOCFileList">COCFileList</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMatInspectReqLotCOCFile(DataTable dtSaveCOCFileList, DataTable dtDeleteCOCFileList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                // 삭제할 정보가 있는경우 삭제
                if (dtDeleteCOCFileList.Rows.Count > 0)
                    strErrRtn = mfDeleteINSMatInspectReqLotCOCFile(dtDeleteCOCFileList, sql.SqlCon, trans);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (!ErrRtn.ErrNum.Equals(0))
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                for (int i = 0; i < dtSaveCOCFileList.Rows.Count; i++)
                {
                    DataTable dtParam = mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveCOCFileList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSaveCOCFileList.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSaveCOCFileList.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSaveCOCFileList.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSaveCOCFileList.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCOCFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveCOCFileList.Rows[i]["COCFileName"].ToString(), 200);
                    mfAddParamDataRow(dtParam, "@i_vbrCOCFileValue", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtSaveCOCFileList.Rows[i]["COCFileValue"]);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMatInspectReqLotCOCFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 COCFile삭제
        /// </summary>
        /// <param name="dtCOCFileList">삭제List</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SqlTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMatInspectReqLotCOCFile(DataTable dtCOCFileList, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;
                for (int i = 0; i < dtCOCFileList.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtCOCFileList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtCOCFileList.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtCOCFileList.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtCOCFileList.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCOCFileList.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectReqLotCOCFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// COCFile 전체 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMatInspectReqLotCOCFile_All(string strPlantCode, string strReqNo, string strReqSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSMatInspectReqLotCOCFile_All", dtParam);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (!ErrRtn.ErrNum.Equals(0))
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// COCFile 전체 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMatInspectReqLotCOCFile_All(string strPlantCode, string strReqNo, string strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectReqLotCOCFile_All", dtParam);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사Lot적용시 WMS에서 COCFile정보 저장하는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="sqlCon">SqlConnention 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMatInspectReqLotCOCFile_FromWMS(string strPlantCode, string strReqNo, string strReqSeq, string strUserID, string strUserIP
                                                        , SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMatInspectReqLotCOCFile_FromWMS", dtParam);
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="strValue">Paramter 인자값</param>
        private void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, byte[] Value)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = Value;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 데이터테이블 설정
        /// </summary>
        /// <returns>Parameter테이블</returns>
        private DataTable mfSetParamDataTable()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable();

                DataColumn dc = new DataColumn("ParamName", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("ParamDirect", typeof(ParameterDirection));
                dt.Columns.Add(dc);

                dc = new DataColumn("DBType", typeof(SqlDbType));
                dt.Columns.Add(dc);

                dc = new DataColumn("Value", typeof(object));
                dt.Columns.Add(dc);

                dc = new DataColumn("Length", typeof(int));
                dt.Columns.Add(dc);

                return dt;
            }
            catch (Exception ex)
            {
                //throw ex;
                return dt;
            }
        }

        /// <summary>
        /// 저장프로시저 트랙잭션(DML) 함수 실행 : SP함수명 및 인자(데이터테이블)가 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns></returns>
        private string mfExecTransStoredProc(SqlConnection Conn, SqlTransaction Trans, string strSPName, DataTable dtSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_Cmd = new SqlCommand();
                    m_Cmd.Connection = Conn;
                    m_Cmd.Transaction = Trans;
                    m_Cmd.CommandType = CommandType.StoredProcedure;
                    m_Cmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        param.SqlValue = dr["Value"];

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                            {
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                            }
                        }

                        m_Cmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_Cmd.ExecuteScalar());

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_Cmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_Cmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_Cmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectReqItem")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectReqItem : ServicedComponent
    {
        /// <summary>
        /// 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                //dtRtn.Columns.Add("InspectGroupCode", typeof(String));
                //dtRtn.Columns.Add("InspectTypeCode", typeof(String));
                //dtRtn.Columns.Add("InspectItemCode", typeof(String));
                //dtRtn.Columns.Add("InspectCondition", typeof(String));
                //dtRtn.Columns.Add("Method", typeof(String));
                //dtRtn.Columns.Add("SpecDesc", typeof(String));
                //dtRtn.Columns.Add("MeasureToolCode", typeof(String));
                //dtRtn.Columns.Add("UpperSpec", typeof(Double));
                //dtRtn.Columns.Add("LowerSpec", typeof(Double));
                //dtRtn.Columns.Add("SpecRange", typeof(String));
                dtRtn.Columns.Add("SampleSize", typeof(Decimal));
                dtRtn.Columns.Add("FaultQty", typeof(Int32));
                //dtRtn.Columns.Add("UnitCode", typeof(String));
                //dtRtn.Columns.Add("CompareFlag", typeof(String));
                //dtRtn.Columns.Add("DataType", typeof(String));
                //dtRtn.Columns.Add("EtcDesc", typeof(String));
                dtRtn.Columns.Add("InspectResultFlag", typeof(String));
                dtRtn.Columns.Add("Mean", typeof(Decimal));
                dtRtn.Columns.Add("StdDev", typeof(Decimal));
                dtRtn.Columns.Add("MaxValue", typeof(Decimal));
                dtRtn.Columns.Add("MinValue", typeof(Decimal));
                dtRtn.Columns.Add("DataRange", typeof(Decimal));
                dtRtn.Columns.Add("Cp", typeof(Decimal));
                dtRtn.Columns.Add("Cpk", typeof(Decimal));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }
        /// <summary>
        /// 수입검사등록/조회 그리드 더블클릭시 Item 정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqItem(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqItem", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사등록/조회 그리드 더블클릭시 Sampling 그리드 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqItemSamlpling(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                /////////////////////////////////////////////////////////////////////////////////   // 2012.07.25 - Lee
                ////PSTS, LEAD_FRAME 자재인경우 SampleSize 고정처리에 의한 호출 프로시져 변경////  --> Strat
                /////////////////////////////////////////////////////////////////////////////////
                
                //dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqItemSampling", dtParam);      /STS
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqItemSampling", dtParam);        // PSTS

                /////////////////////////////////////////////////////////////////////////////////   // 2012.07.25 - Lee
                ////PSTS, LEAD_FRAME 자재인경우 SampleSize 고정처리에 의한 호출 프로시져 변경////  --> End
                /////////////////////////////////////////////////////////////////////////////////

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 아이템 저장 Method
        /// </summary>
        /// <param name="dtItem"> 아이템 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveInsMatInspectReqItem(DataTable dtItem, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                for (int i = 0; i < dtItem.Rows.Count; i++)
                {
                    // 파라미터 데이터 테이블 정의
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_dblSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["SampleSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultQty", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMean", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Mean"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblStdDev", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["StdDev"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMaxValue", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["MaxValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMinValue", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["MinValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblDataRange", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["DataRange"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCp", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Cp"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCpk", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Cpk"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMatInspectReqItem", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Item 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰번호순번 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteINSMatInspectReqItem(String strPlantCode, String strReqNo, String strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                // Parameter Datatable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectReqItem", dtParam);

                // 결과 검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Sampling 그리드에서 DataType이 선택일때 콤보박스 유저공통테이블에서 Key, Value 가져오는 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq"> 의뢰번호순번 </param>
        /// <param name="intReqLotSeq">의뢰Lot순번</param>
        /// <param name="intReqItemSeq"> 검사항목순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMatInspectReqItem_DataTypeSelect(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq, int intReqItemSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMatInspectReqItem_DataTypeSelect", dtParam);

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectResultMeasure")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectResultMeasure : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(Double));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 계량형 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMatInspectResultMeasure(DataTable dtSave, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMatInspectResultMeasure", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                sql.Dispose();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 계량형 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰번호순번 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMatInspectResultMeasure(String strPlantCode, String strReqNo, String strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectResultMeasure", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectResultCount")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectResultCount : ServicedComponent
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(Double));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 계수형 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMatInspectResultCount(DataTable dtSave, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMatInspectResultCount", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 계수형 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰번호순번 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMatInspectResultCount(String strPlantCode, String strReqNo, String strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectResultCount", dtParam);
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectResultOkNg")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectResultOkNg : ServicedComponent
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(String));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// OkNg 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMatInspectResultOkNg(DataTable dtSave, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectValue"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMatInspectResultOkNg", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// OkNg 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰번호순번 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMatInspectResultOkNg(String strPlantCode, String strReqNo, String strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectResultOkNg", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectResultDesc")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectResultDesc : ServicedComponent
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(String));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 설명 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMatInspectResultDesc(DataTable dtSave, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["InspectValue"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMatInspectResultDesc", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설명 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰번호순번 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMatInspectResultDesc(String strPlantCode, String strReqNo, String strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectResultDesc", dtParam);
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MatInspectResultSelect")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MatInspectResultSelect : ServicedComponent
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(String));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 선택 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMatInspectResultSelect(DataTable dtSave, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectValue"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMatInspectResultSelect", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 선택 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰번호순번 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMatInspectResultSelect(String strPlantCode, String strReqNo, String strReqSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = "";

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMatInspectResultSelect", dtParam);
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialAbnormalH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialAbnormalH : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("StdNumber", typeof(string));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("AbnormalType", typeof(string));
                dtRtn.Columns.Add("StatusFlag", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("AriseProcessCode", typeof(string));
                dtRtn.Columns.Add("AriseDate", typeof(string));

                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("VendorName", typeof(string));
                dtRtn.Columns.Add("MaterialCode", typeof(string));
                dtRtn.Columns.Add("MaterialName", typeof(string));

                dtRtn.Columns.Add("CompleteFlag", typeof(string));
                dtRtn.Columns.Add("Complete", typeof(string));
                dtRtn.Columns.Add("MESTFlag", typeof(string));
                dtRtn.Columns.Add("FInalCompleteFlag", typeof(char));
                dtRtn.Columns.Add("ShortActionDesc", typeof(string));
                dtRtn.Columns.Add("LongActionDesc", typeof(string));
                dtRtn.Columns.Add("MaterialTreate", typeof(string));
                dtRtn.Columns.Add("LongFile", typeof(string));
                dtRtn.Columns.Add("ShortFile", typeof(string));
                dtRtn.Columns.Add("LongFileData", typeof(byte[]));
                dtRtn.Columns.Add("ShortFileData", typeof(byte[]));
                dtRtn.Columns.Add("InspectDesc", typeof(string));
                dtRtn.Columns.Add("ShortUserName", typeof(string));
                dtRtn.Columns.Add("ShortEmail", typeof(string));
                dtRtn.Columns.Add("ShortConfirmFlag", typeof(string));
                dtRtn.Columns.Add("ShortConfirmDesc", typeof(string));
                dtRtn.Columns.Add("ShortConfirmUserID", typeof(string));
                dtRtn.Columns.Add("ShortConfirmDate", typeof(string));
                dtRtn.Columns.Add("LongUserName", typeof(string));
                dtRtn.Columns.Add("LongEmail", typeof(string));
                dtRtn.Columns.Add("LongConfirmFlag", typeof(string));
                dtRtn.Columns.Add("LongConfirmDesc", typeof(string));
                dtRtn.Columns.Add("LongConfirmUserID", typeof(string));
                dtRtn.Columns.Add("LongConfirmDate", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 조회용 DataTable Column 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetSearchDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("StdNumber", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("MaterialCode", typeof(string));
                dtRtn.Columns.Add("AbnormalType", typeof(string));
                dtRtn.Columns.Add("FromWriteDate", typeof(string));
                dtRtn.Columns.Add("ToWriteDate", typeof(string));
                dtRtn.Columns.Add("MaterialGroupCode", typeof(string));
                dtRtn.Columns.Add("ConsumableTypeCode", typeof(string));
                dtRtn.Columns.Add("FinalCompleteFlag", typeof(string));
                dtRtn.Columns.Add("Lang", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 검색조건에 따른 원자재 이상발생 관리 조회 Method
        /// </summary>
        /// <param name="dtSearch"> 검색조건이 저장된 DataTable </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReaeINSMaterialAbnormalH(DataTable dtSearch)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["VendorCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalType", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["AbnormalType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFromWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["FromWriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strToWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ToWriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@I_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ConsumableTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strFinalCompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtSearch.Rows[i]["FinalCompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["Lang"].ToString(), 3);

                    dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalH", dtParam);
                }

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 그리드 더블클릭시 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialAbnormalHDetail(string strPlantCode, string strStdNumber, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalHDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 원자재 이상발생 관리에서 Lot 번호 입력시 수입검사 Lot 테이블에서 Ship에 해당하는 정보 가져오는 SP
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strLotNo"> Lot 번호 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialAbnormalH_InitProcessH(string strPlantCode, string strLotNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalD_InitProcessH", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 원자재 이상발생관리 헤더 저장
        /// </summary>
        /// <param name="dtHeader"> 헤더정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="dtDetail"> 상세정보 DataTable </param>
        /// <param name="dtFault"> 불량정보 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalH_Click(DataTable dtHeader, string strUserID, string strUserIP, DataTable dtDetail, DataTable dtFault)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                String strErrRtn = "";
                SqlTransaction trans;
                string strStdNumber = "";
                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {

                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();
                    // 파라미터 데이타테이블 설정
                    DataTable dtParam = mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalType", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AbnormalType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStatusFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StatusFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AriseDate"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["VendorName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["MaterialName"].ToString(), 300);

                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, "F", 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFinalCompleteFlag", ParameterDirection.Input, SqlDbType.Char, "F", 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortActionDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortActionDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongActionDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongActionDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialTreate", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["MaterialTreate"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongFile", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["LongFile"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortFile", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ShortFile"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectDesc"].ToString(), 1000);
                    mfAddParamDataRow(dtParam, "@i_vbLongFileData", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtHeader.Rows[i]["LongFileData"]);
                    mfAddParamDataRow(dtParam, "@i_vbShortFileData", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtHeader.Rows[i]["ShortFileData"]);

                    sql.mfAddParamDataRow(dtParam, "@i_strShortUserName", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortUserName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortEmail", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortEmail"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortConfirmFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["ShortConfirmFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortConfirmDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortConfirmDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortConfirmUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortConfirmUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortConfirmDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongUserName", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongUserName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongEmail", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongEmail"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongConfirmFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["LongConfirmFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongConfirmDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongConfirmDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongConfirmUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongConfirmUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongConfirmDate"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialAbnormalH", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // OUTPUT 값 저장
                        strStdNumber = ErrRtn.mfGetReturnValue(0);

                        // 성공시 Detail, Fault Data 저장
                        
                        // DFault 삭제
                        if (dtFault.Rows.Count > 0)
                        {
                            MaterialAbnormalDFault clsFault = new MaterialAbnormalDFault();
                            // 삭제 Method 실행
                            if(strStdNumber.Substring(0, 4).Equals("AE02"))
                                strErrRtn = clsFault.mfDeleteINSMaterialAbnormalDFault_Mate(dtFault, strStdNumber, sql.SqlCon, trans);
                            else
                                strErrRtn = clsFault.mfDeleteINSMaterialAbnormalDFault_Proce(dtFault, strStdNumber, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        
                        // Detail 저장
                        if (dtDetail.Rows.Count > 0)
                        {
                            MaterialAbnormalD clsDetail = new MaterialAbnormalD();
                            // 저장 Method 실행
                            if (strStdNumber.Substring(0, 4).Equals("AE02"))
                                strErrRtn = clsDetail.mfSaveINSMaterialAbnormalD_Mat(dtDetail, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            else
                                strErrRtn = clsDetail.mfSaveINSMaterialAbnormalD_Proc(dtDetail, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // DFault 저장
                        if (dtFault.Rows.Count > 0)
                        {
                            MaterialAbnormalDFault clsFault = new MaterialAbnormalDFault();
                            // 저장 Method 실행
                            if (strStdNumber.Substring(0, 4).Equals("AE02"))
                                strErrRtn = clsFault.mfSaveINSMaterialAbnormalDFault_Mat(dtFault, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            else
                                strErrRtn = clsFault.mfSaveINSMaterialAbnormalDFault_Proc(dtFault, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 원자재 이상발생관리 헤더 저장
        /// </summary>
        /// <param name="dtHeader"> 헤더정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="dtDetail"> 상세정보 DataTable </param>
        /// <param name="dtFault"> 불량정보 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalH_ToolSave(DataTable dtHeader, string strUserID, string strUserIP, DataTable dtDetail, DataTable dtFault)//, DataTable dtWMS,,string strFormName,DataTable dtProcInspectLotNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                String strErrRtn = "";
                SqlTransaction trans;
                string strStdNumber = "";
                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    ////작성완료 여부가 진행상태인 경우
                    //if (!dtHeader.Rows[0]["Complete"].ToString().Equals("T"))
                    //{
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();
                    // 파라미터 데이타테이블 설정
                    DataTable dtParam = mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalType", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AbnormalType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStatusFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StatusFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AriseProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AriseDate"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["VendorName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["MaterialName"].ToString(), 300);

                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CompleteFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strFinalCompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["FinalCompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortActionDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortActionDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongActionDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongActionDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialTreate", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["MaterialTreate"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongFile", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["LongFile"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortFile", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ShortFile"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectDesc"].ToString(), 1000);
                    mfAddParamDataRow(dtParam, "@i_vbLongFileData", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtHeader.Rows[i]["LongFileData"]);
                    mfAddParamDataRow(dtParam, "@i_vbShortFileData", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtHeader.Rows[i]["ShortFileData"]);

                    sql.mfAddParamDataRow(dtParam, "@i_strShortUserName", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortUserName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortEmail", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortEmail"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortConfirmFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["ShortConfirmFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortConfirmDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortConfirmDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortConfirmUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortConfirmUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strShortConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ShortConfirmDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongUserName", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongUserName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongEmail", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongEmail"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongConfirmFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["LongConfirmFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongConfirmDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongConfirmDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongConfirmUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongConfirmUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLongConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["LongConfirmDate"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialAbnormalH", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // OUTPUT 값 저장
                        strStdNumber = ErrRtn.mfGetReturnValue(0);

                        // 성공시 Detail, Fault Data 저장

                        MaterialAbnormalDFault clsFault = new MaterialAbnormalDFault();
                        // DFault 삭제
                        if (dtFault.Rows.Count > 0)
                        {
                            // 삭제 Method 실행
                            if (strStdNumber.Substring(0, 4).Equals("AE02"))
                                strErrRtn = clsFault.mfDeleteINSMaterialAbnormalDFault_Mate(dtFault, strStdNumber, sql.SqlCon, trans);
                            else
                                strErrRtn = clsFault.mfDeleteINSMaterialAbnormalDFault_Proce(dtFault, strStdNumber, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // Detail 저장
                        if (dtDetail.Rows.Count > 0)
                        {
                            MaterialAbnormalD clsDetail = new MaterialAbnormalD();
                            // 저장 Method 실행
                            // 공정이상인지 수입이상인지 구분
                            if (strStdNumber.Substring(0, 4).Equals("AE02"))
                                strErrRtn = clsDetail.mfSaveINSMaterialAbnormalD_Mat(dtDetail, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            else if (strStdNumber.Substring(0, 4).Equals("AE03"))
                                strErrRtn = clsDetail.mfSaveINSMaterialAbnormalD_Proc(dtDetail, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);

                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // DFault 저장
                        if (dtFault.Rows.Count > 0)
                        {
                            ////MaterialAbnormalDFault clsFault = new MaterialAbnormalDFault();
                            // 저장 Method 실행
                            if (strStdNumber.Substring(0, 4).Equals("AE02"))
                                strErrRtn = clsFault.mfSaveINSMaterialAbnormalDFault_Mat(dtFault, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            else
                                strErrRtn = clsFault.mfSaveINSMaterialAbnormalDFault_Proc(dtFault, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        trans.Commit();
                    }
                    //}
                    ////작성완료가 완료상태인경우 
                    //else
                    //{
                    //    ErrRtn.ErrNum = 0;
                    //    strStdNumber = dtHeader.Rows[i]["StdNumber"].ToString();
                    //}
                    #region 기존 WMS 소스 주석처리
                    ////if (dtWMS.Rows.Count > 0 && ErrRtn.ErrNum.Equals(0))
                    ////{
                    ////    strErrRtn = mfSaveWMSMATAbnormal(dtWMS, strUserID, strUserIP);

                    ////    // 결과 검사
                    ////    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    ////    if (!ErrRtn.ErrMessage.Equals("00") || !ErrRtn.ErrNum.Equals(0))
                    ////    {
                    ////        break;
                    ////    }
                    ////    else
                    ////    {
                    ////        sql.mfConnect();
                    ////        trans = sql.SqlCon.BeginTransaction();
                    ////        // WMSTransFlag Update
                    ////        DataTable dtParam = sql.mfSetParamDataTable();
                    ////        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    ////        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    ////        sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    ////        sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    ////        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    ////        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialAbnormalH_WMS", dtParam);

                    ////        // 결과 검사
                    ////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    ////        if (ErrRtn.ErrNum != 0)
                    ////        {
                    ////            trans.Rollback();
                    ////            break;
                    ////        }
                    ////        else
                    ////        {
                    ////            trans.Commit();
                    ////        }
                    ////    }
                    ////}
                    #endregion
                    #region 기존 MES 소스 주석처리
                    ////if (ErrRtn.ErrNum.Equals(0))
                    ////{
                    ////    //공정이상발생정보 AND MESTFlag가 F AND 작성완료체크가 체크된경우 MES로 정보 전송
                    ////    if (strStdNumber.Substring(0, 4).Equals("AE03") && dtHeader.Rows[0]["MESTFlag"].ToString().Equals("F") && dtHeader.Rows[0]["CompleteFlag"].ToString().Equals("True"))
                    ////    {
                    ////        //이상발생구분이 공정이상 이거나 공정이상발생정보 그리드에 정보가 있을 경우
                    ////        if (dtHeader.Rows[0]["AbnormalType"].ToString().Equals("2") || dtProcInspectLotNo.Rows.Count > 0)
                    ////        {
                    ////            //공장,등록자 저장
                    ////            string strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString(); //공장
                    ////            string strWriteID = dtHeader.Rows[i]["WriteUserID"].ToString(); //등록자
                    ////            //string strHoldCode = "HQ02"; //HoldCode 아직미정
                    ////            //MES 로 보내줄 공정이상발생 Ship Lot (dtProcInspectLotNo)

                    ////            // HoldCode 설정
                    ////            QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                    ////            DataTable dtReason = clsReason.mfReadMASReasonCode_MESHoldCode(strPlantCode, strFormName);
                    ////            string strHoldCode = dtReason.Rows[0]["REASONCODE"].ToString();

                    ////            //MES서버경로 가져오기
                    ////            QRPSYS.BL.SYSPGM.SystemAccessInfo clsAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    ////            DataTable dtAccess = clsAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S04");

                    ////            //MES 원자재이상발생 요청(원자재 Hold)매서드 실행
                    ////            QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtAccess);
                    ////            DataTable dtMES = clsTibrv.CON_HOLD4QC_REQ(strFormName, strWriteID, "", strHoldCode, dtProcInspectLotNo, strUserIP);

                    ////            //MES 결과정보가 있는 경우
                    ////            if (dtMES.Rows.Count > 0)
                    ////            {
                    ////                //처리결과가 성공인 경우
                    ////                if (dtMES.Rows[0][0].ToString().Equals("0"))
                    ////                {
                    ////                    //디비연결
                    ////                    sql.mfConnect();

                    ////                    //트랜젝션 시작
                    ////                    SqlTransaction transt = sql.SqlCon.BeginTransaction();

                    ////                    //파라미터 저장
                    ////                    DataTable dtParamt = sql.mfSetParamDataTable();

                    ////                    sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    ////                    sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);    //공장
                    ////                    sql.mfAddParamDataRow(dtParamt, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);  //관리번호
                    ////                    sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    ////                    sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strWriteID, 20);

                    ////                    sql.mfAddParamDataRow(dtParamt, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    ////                    sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    ////                    //원자재이상발생관리 MES여부확인 프로시저 실행
                    ////                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_INSMaterialAbnormalHMESTFlag", dtParamt);

                    ////                    //DeCoding
                    ////                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    ////                    //처리결과에 따라서 롤백 혹인 커밋처리를한다.
                    ////                    if (ErrRtn.ErrNum != 0)
                    ////                        transt.Rollback();
                    ////                    else
                    ////                        transt.Commit();
                    ////                }

                    ////                //성공이거나 실패의 메세지를 박아서 EnCoding 한다.
                    ////                ErrRtn.InterfaceResultCode = dtMES.Rows[0][0].ToString();
                    ////                ErrRtn.InterfaceResultMessage = dtMES.Rows[0][1].ToString();
                    ////                ErrRtn.mfAddReturnValue(strStdNumber);
                    ////                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    ////            }
                    ////            else
                    ////            {
                    ////                ErrRtn.mfAddReturnValue(strStdNumber);
                    ////                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    ////            }
                    ////        }
                    ////    }
                    ////}
                    #endregion
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 원자재 이상발생 관리 헤더 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMaterialAbnormalH(string strPlantCode, string strStdNumber)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                // DB 연결
                sql.mfConnect();

                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 불량정보부터 삭제
                MaterialAbnormalDFault clsFault = new MaterialAbnormalDFault();
                strErrRtn = clsFault.mfDeleteINSMaterialAbnormalDFaultALL(strPlantCode, strStdNumber, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 상세정보 삭제
                MaterialAbnormalD clsDetail = new MaterialAbnormalD();
                strErrRtn = clsDetail.mfDeleteINSMaterialAbnormalD(strPlantCode, strStdNumber, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 헤더정보 삭제
                // 파라미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 저장프로시져 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSMaterialAbnormalH", dtParam);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    // 성공이면 Commit
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 원자재 이상발생관리 헤더 삭제
        /// </summary>
        /// <param name="dtHeader"> 헤더정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="dtDetail"> 상세정보 DataTable </param>
        /// <param name="dtFault"> 불량정보 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalH_Del(DataTable dtHeader, string strUserID, string strUserIP, DataTable dtFault)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                String strErrRtn = "";
                SqlTransaction trans;
                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {

                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    string strStdNumber = dtHeader.Rows[i]["StdNumber"].ToString();
                    if (i.Equals(0))
                    {

                        // DFault 저장
                        if (dtFault.Rows.Count > 0)
                        {
                            // 불량정보부터 삭제
                            MaterialAbnormalDFault clsFault = new MaterialAbnormalDFault();
                            // 저장 Method 실행
                            if (strStdNumber.Substring(0, 4).Equals("AE02"))
                                strErrRtn = clsFault.mfSaveINSMaterialAbnormalDFault_Del(dtFault, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            else if (strStdNumber.Substring(0, 4).Equals("AE03"))
                                strErrRtn = clsFault.mfSaveINSMaterialAbnormalDFault_Del(dtFault, strStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            else
                                return strErrRtn;

                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                    }

                    // 파라미터 데이타테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdNumber"].ToString(), 20);


                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialAbnormalH_Del", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="strValue">Paramter 인자값</param>
        private void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, byte[] Value)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = Value;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 데이터테이블 설정
        /// </summary>
        /// <returns>Parameter테이블</returns>
        private DataTable mfSetParamDataTable()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable();

                DataColumn dc = new DataColumn("ParamName", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("ParamDirect", typeof(ParameterDirection));
                dt.Columns.Add(dc);

                dc = new DataColumn("DBType", typeof(SqlDbType));
                dt.Columns.Add(dc);

                dc = new DataColumn("Value", typeof(object));
                dt.Columns.Add(dc);

                dc = new DataColumn("Length", typeof(int));
                dt.Columns.Add(dc);

                return dt;
            }
            catch (Exception ex)
            {
                //throw ex;
                return dt;
            }
        }

        /// <summary>
        /// 저장프로시저 트랙잭션(DML) 함수 실행 : SP함수명 및 인자(데이터테이블)가 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns></returns>
        private string mfExecTransStoredProc(SqlConnection Conn, SqlTransaction Trans, string strSPName, DataTable dtSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_Cmd = new SqlCommand();
                    m_Cmd.Connection = Conn;
                    m_Cmd.Transaction = Trans;
                    m_Cmd.CommandType = CommandType.StoredProcedure;
                    m_Cmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        param.SqlValue = dr["Value"];

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                            {
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                            }
                        }

                        m_Cmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_Cmd.ExecuteScalar());

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_Cmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_Cmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_Cmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    Debug.Print("DataBase 연결되지 않았습니다.");
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }


        #region MES
        /// <summary>
        /// MES용 데이터 테이블 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataTable_MES()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("FormName", typeof(string));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("Comment", typeof(string));
                dtRtn.Columns.Add("HoldCode", typeof(string));
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("StdNumber", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MES Hold 요청하고 성공시 MES Flag "T"로 Update하는 메소드
        /// </summary>
        /// <param name="dtStdInfo">기본정보(공장, 관리번호, 폼명, 작성자ID, Comment, HoldCode) 저장된 데이터 테이블</param>
        /// <param name="dtLotList">LotList DataTable</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfINSMaterialAbnormal_MES(DataTable dtStdInfo, DataTable dtLotList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04"); //Live
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");    //Test

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtMesHold = clsTibrv.CON_HOLD4QC_REQ(dtStdInfo.Rows[0]["FormName"].ToString()             //폼명
                                                                , dtStdInfo.Rows[0]["WriteUserID"].ToString()       //작성자
                                                                , dtStdInfo.Rows[0]["Comment"].ToString()           //Comment
                                                                , dtStdInfo.Rows[0]["HoldCode"].ToString()          //HoldCode
                                                                , dtLotList                                         //LotList
                                                                , strUserIP);                                       //사용자IP
                clsTibrv.Dispose();

                // MES I/F 성공시
                if (dtMesHold.Rows.Count > 0)
                {
                    if (dtMesHold.Rows[0]["returncode"].ToString().Equals("0"))
                    {
                        //디비연결
                        sql.mfConnect();

                        //트랜젝션 시작
                        SqlTransaction trans = sql.SqlCon.BeginTransaction();

                        //파라미터 저장
                        DataTable dtParam = sql.mfSetParamDataTable();

                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["StdNumber"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        // MES Flag T로 Update
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialAbnormalHMESTFlag", dtParam);

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        // 결과에 따라 Commit OR RollBack
                        if (ErrRtn.ErrNum != 0)
                            trans.Rollback();
                        else
                            trans.Commit();
                    }

                    // MES InterFace 결과를 EnCoding 한다.
                    ErrRtn.InterfaceResultCode = dtMesHold.Rows[0][0].ToString();
                    ErrRtn.InterfaceResultMessage = dtMesHold.Rows[0][1].ToString();
                    ErrRtn.mfAddReturnValue(dtStdInfo.Rows[0]["StdNumber"].ToString());
                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                else
                {
                    ErrRtn.mfAddReturnValue(dtStdInfo.Rows[0]["StdNumber"].ToString());
                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        #endregion

        #region WMS
        /// <summary>
        /// WMS 전송용 DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetWMSDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("GRNo", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(char));
                dtRtn.Columns.Add("ValidationDate", typeof(string));
                dtRtn.Columns.Add("IFFlag", typeof(string));
                dtRtn.Columns.Add("AdmitDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 작성완료후 저장시 WMS 검사결과 전송
        /// </summary>
        /// <param name="dtSave">WMS 전송정보 저장된 데이터 테이블/param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveWMSMATAbnormal(DataTable dtSave, string strUserID, string strUserIP)
        {
            #region 변경전
            ////TransErrRtn ErrRtn = new TransErrRtn();
            ////try
            ////{
            ////    string strErrRtn = string.Empty;
            ////    //WMS Oracle DB 연결정보 읽기
            ////    QRPSYS.BL.SYSPGM.SystemAccessInfo clsPgm = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
            ////    DataTable dtPgm = clsPgm.mfReadSystemAccessInfoDetail(dtSave.Rows[0]["PlantCode"].ToString(), "S05");
            ////    QRPDB.ORADB oraDB = new QRPDB.ORADB();
            ////    oraDB.mfSetORADBConnectString(dtPgm.Rows[0]["SystemAddressPath"].ToString(), dtPgm.Rows[0]["AccessID"].ToString(), dtPgm.Rows[0]["AccessPassword"].ToString());

            ////    //WMS Oracle DB연결하기
            ////    oraDB.mfConnect();

            ////    //Transaction 시작
            ////    OracleTransaction trans;
            ////    trans = oraDB.SqlCon.BeginTransaction();

            ////    for (int i = 0; i < dtSave.Rows.Count; i++)
            ////    {
            ////        // WMS 처리 SP 호출
            ////        DataTable dtParam = oraDB.mfSetParamDataTable();

            ////        oraDB.mfAddParamDataRow(dtParam, "V_PLANTCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["PlantCode"].ToString(), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_CONSTRACTCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["GRNo"].ToString(), 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_LOTNO", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["LotNo"].ToString(), 50);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_RESULTFLAG", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERIP", ParameterDirection.Input, OracleDbType.Varchar2, strUserIP, 15);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERID", ParameterDirection.Input, OracleDbType.Varchar2, strUserID, 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_VALIDATIONDATE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["ValidationDate"].ToString(), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_IFFLAG", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["IFFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "RTN", ParameterDirection.Output, OracleDbType.Int32);

            ////        strErrRtn = oraDB.mfExecTransStoredProc(oraDB.SqlCon, trans, "UP_UPDATE_INSINSPECTRESULT", dtParam);
            ////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            ////        //WMS 처리결과에 따른 Transaction 처리
            ////        if (ErrRtn.ErrNum != 0)
            ////            trans.Rollback();
            ////    }
            ////    trans.Commit();

            ////    clsPgm.Dispose();
            ////    oraDB.Dispose();

            ////    return strErrRtn;
            ////}
            ////catch (Exception ex)
            ////{
            ////    return ex.Message;
            ////}
            ////finally
            ////{
            ////}
            #endregion
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                //SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["GRNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strValidationDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ValidationDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strIFFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["IFFlag"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AdmitDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_INSMaterialAbnormalH_WMS_Send", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrMessage.Equals("00") || !ErrRtn.ErrNum.Equals(0))
                    {
                        //trans.Rollback();
                        break;
                    }
                }

                //if(ErrRtn.ErrNum.Equals(0))
                //    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// WMS Flag 저장 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormal_WMS_TFlag(string strPlantCode, string strStdNumber)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;

                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // WMSTransFlag Update
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialAbnormalH_WMS", dtParam);

                // 결과 검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        #endregion
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialAbnormalD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialAbnormalD : ServicedComponent
    {
        /// <summary>
        /// DataTable Column 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("StdNumber", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(int));
                dtRtn.Columns.Add("ReqItemSeq", typeof(int));
                dtRtn.Columns.Add("ReqFileName", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        #region 수입이상
        /// <summary>
        /// 원자재이상발생 상세(수입이상) 리스트 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">관리번호</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰번호순번</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialAbnormalD_Mat(string strPlantCode, string strStdNumber, string strReqNo, string strReqSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalD_Mat", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 원자재 이상발생(수입이상) 상세저장
        /// </summary>
        /// <param name="dtDetail"> 상세정보 DataTable </param>
        /// <param name="strStdNumber">관리번호</param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalD_Mat(DataTable dtDetail, string strStdNumber, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ReqFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialAbnormalD_Mat", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보가 저장된 상태에서 상세정보 저장 Method
        /// </summary>
        /// <param name="dtDetail"> 상세정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalD_Mat(DataTable dtDetail, string strUserID, string strUserIP, DataTable dtFault)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();
                string strErrRtn = "";
                SqlTransaction trans;
                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();
                    // 파리미터 DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ReqFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialAbnormalD_Mat", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // 성공시 불량정보 저장
                        // 성공시 Detail, Fault Data 저장
                        // DFault 삭제
                        if (dtFault.Rows.Count > 0)
                        {
                            MaterialAbnormalDFault clsFault = new MaterialAbnormalDFault();

                            // OUTPUT으로 받은 관리번호 저장
                            string strRtnStdNumber = ErrRtn.mfGetReturnValue(0);
                            /*
                            // 삭제 Method 실행
                            strErrRtn = clsFault.mfDeleteINSMaterialAbnormalDFault_Mat(dtFault, strRtnStdNumber, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                            */
                            // 저장 Method 호출
                            strErrRtn = clsFault.mfSaveINSMaterialAbnormalDFault_Proc(dtFault, strRtnStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        #endregion

        #region 공정이상
        /// <summary>
        /// 원자재 이상발생 관리에서 Lot 번호 입력시 수입검사 Lot 테이블에서 Ship에 해당하는 정보 가져오는 SP
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strLotNo"> Lot 번호 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialAbnormalD_InitProcessList(string strPlantCode, string strLotNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalD_InitProcessList", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 원자재 이상발생 관리 상세조회(공정이상) Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialAbnormalD_Proc(string strPlantCode, string strStdNumber, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalD_Proc", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 원자재 이상발생관리 상세저장 
        /// </summary>
        /// <param name="dtDetail"> 상세정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalD_Proc(DataTable dtDetail, string strStdNumber, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ReqFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialAbnormalD_Proc", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보가 저장된 상태에서 상세정보 저장 Method
        /// </summary>
        /// <param name="dtDetail"> 상세정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalD_Proc(DataTable dtDetail, string strUserID, string strUserIP, DataTable dtFault)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();
                string strErrRtn = "";
                SqlTransaction trans;
                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();
                    // 파리미터 DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ReqFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSMaterialAbnormalD_Proc", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // 성공시 불량정보 저장
                        // 성공시 Detail, Fault Data 저장
                        // DFault 삭제
                        if (dtFault.Rows.Count > 0)
                        {
                            if (dtDetail.Rows[i]["PlantCode"].ToString() == dtFault.Rows[0]["PlantCode"].ToString() &&
                                dtDetail.Rows[i]["StdNumber"].ToString() == dtFault.Rows[0]["StdNumber"].ToString() &&
                                dtDetail.Rows[i]["ReqNo"].ToString() == dtFault.Rows[0]["ReqNo"].ToString() &&
                                dtDetail.Rows[i]["ReqSeq"].ToString() == dtFault.Rows[0]["ReqSeq"].ToString() &&
                                dtDetail.Rows[i]["ReqLotSeq"].ToString() == dtFault.Rows[0]["ReqLotSeq"].ToString() &&
                                dtDetail.Rows[i]["ReqItemSeq"].ToString() == dtFault.Rows[0]["ReqItemSeq"].ToString())
                            {
                                MaterialAbnormalDFault clsFault = new MaterialAbnormalDFault();

                                // OUTPUT으로 받은 관리번호 저장
                                string strRtnStdNumber = ErrRtn.mfGetReturnValue(0);

                                // 삭제 Method 실행
                                /*strErrRtn = clsFault.mfDeleteINSMaterialAbnormalDFault_Proc(dtFault, strRtnStdNumber, sql.SqlCon, trans);
                                // 결과 검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                                 * */
                                // 저장 Method 호출
                                strErrRtn = clsFault.mfSaveINSMaterialAbnormalDFault_Proc(dtFault, strRtnStdNumber, strUserID, strUserIP, sql.SqlCon, trans);
                                // 결과 검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                        }
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        #endregion


        /// <summary>
        /// 원자재 이상발생 상세 삭제시 DeleteFlag 변경 저장
        /// </summary>
        /// <param name="dtDetail"> 상세정보 DataTable </param>
        /// <param name="strStdNumber">관리번호</param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalD_Del(DataTable dtDetail, string strStdNumber, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ReqFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialAbnormalD_Del", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 원자재 이상발생관리 상세정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMaterialAbnormalD(string strPlantCode, string strStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                // 파리미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 저장프로시져 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMaterialAbnormalD", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 수입검사화면에서 완료이고, 불합격인 정보일때 등록된 원자재이상발생정보로 이동하기 위한 관리번호 조회하는 메소드 
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">수입검사관리번호</param>
        /// <param name="strReqSeq">수입검사관리번호순번</param>
        /// <returns></returns>
        public DataTable mfReadINSMaterialAbnormalD_FindStdNum(string strPlantCode, string strReqNo, string strReqSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalD_FindStdNum", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialAbnormalDFault")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialAbnormalDFault : ServicedComponent
    {
        /// <summary>
        /// DataTable Column 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("StdNumber", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(int));
                dtRtn.Columns.Add("ReqItemSeq", typeof(int));
                dtRtn.Columns.Add("FaultSeq", typeof(int));
                dtRtn.Columns.Add("InspectQty", typeof(double));
                dtRtn.Columns.Add("FaultQty", typeof(double));
                dtRtn.Columns.Add("FaultTypeCode", typeof(string));
                dtRtn.Columns.Add("FaultTypeName", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("FilePath", typeof(string));
                dtRtn.Columns.Add("FileVarValue", typeof(byte[]));
                dtRtn.Columns.Add("DeleteFlag", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 원자재 이상발생 관리 불량유형 정보 조회 Method
        /// </summary>
        /// <param name="dtSearch"> 검색조건이 저장된 DataTable </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialAbnormalDFault(DataTable dtSearch)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[0]["StdNumber"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[0]["ReqNo"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[0]["ReqSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSearch.Rows[0]["ReqLotSeq"].ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSearch.Rows[0]["ReqItemSeq"].ToString());

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalDFault", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 불량정보 저장 Method
        /// </summary>
        /// <param name="dtFaulg"> 불량정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalDFault_Mat(DataTable dtFault, string strStdNumber, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["FaultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["InspectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["FaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FaultTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FilePath"].ToString(), 1000);
                    mfAddParamDataRow(dtParam, "@i_vbFileData", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtFault.Rows[i]["FileVarValue"]);
                    sql.mfAddParamDataRow(dtParam, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["DeleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialAbnormalDFault", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 불량정보 삭제 Method
        /// </summary>
        /// <param name="dtFaulg"> 불량정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalDFault_Del(DataTable dtFault, string strStdNumber, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["FaultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["InspectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["FaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FaultTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FilePath"].ToString(), 1000);
                    mfAddParamDataRow(dtParam, "@i_vbFileData", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtFault.Rows[i]["FileVarValue"]);
                    sql.mfAddParamDataRow(dtParam, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["DeleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialAbnormalDFault_Del", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량정보 삭제 Method
        /// </summary>
        /// <param name="dtFault"> 불량정보 DataTable </param>
        /// <param name="sqlCon">SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMaterialAbnormalDFault_Mat(DataTable dtFault, string strStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";
                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_StrPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["FaultSeq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMaterialAbnormalDFault_Mat", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;
                    }
                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량정보 삭제 Method
        /// </summary>
        /// <param name="dtFault"> 불량정보 DataTable </param>
        /// <param name="sqlCon">SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMaterialAbnormalDFault_Mate(DataTable dtFault, string strStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";
                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    if (!dtFault.Rows[i]["DeleteFlag"].ToString().Equals("T")) //삭제여부가 T 가아니면 패스
                        continue;

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_StrPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["FaultSeq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMaterialAbnormalDFault_Mate", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;
                    }
                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량정보 저장 Method
        /// </summary>
        /// <param name="dtFaulg"> 불량정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSMaterialAbnormalDFault_Proc(DataTable dtFault, string strStdNumber, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["FaultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["InspectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.Decimal, dtFault.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["FaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FaultTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtFault.Rows[i]["FilePath"].ToString(), 1000);
                    mfAddParamDataRow(dtParam, "@i_vbFileData", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtFault.Rows[i]["FileVarValue"]);
                    sql.mfAddParamDataRow(dtParam, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.Char, dtFault.Rows[i]["DeleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = mfExecTransStoredProc(sqlCon, trans, "up_Update_INSMaterialAbnormalDFault", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량정보 삭제 Method
        /// </summary>
        /// <param name="dtFault"> 불량정보 DataTable </param>
        /// <param name="sqlCon">SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMaterialAbnormalDFault_Proc(DataTable dtFault, string strStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_StrPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["FaultSeq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMaterialAbnormalDFault_Proc", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;
                    }
                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량정보 삭제 Method
        /// </summary>
        /// <param name="dtFault"> 불량정보 DataTable </param>
        /// <param name="sqlCon">SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMaterialAbnormalDFault_Proce(DataTable dtFault, string strStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtFault.Rows.Count; i++)
                {
                    if (!dtFault.Rows[i]["DeleteFlag"].ToString().Equals("T")) // 삭제여부가 T가아니면 패스
                        continue;

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_StrPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFault.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFault.Rows[i]["FaultSeq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMaterialAbnormalDFault_Proce", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;
                    }
                    
                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 불량정보 전체 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 관리번호 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSMaterialAbnormalDFaultALL(string strPlantCode, string strStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                // 파라미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSMaterialAbnormalDFaultALL", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 원자재이상발생(수입이상) 불량정보 그리드 LotNo 콤보박스 설정 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="intReqItemSeq">Item순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialAbnormalDFault_Mat_LotNo_Combo(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, int intReqItemSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalDFault_Mat_LotNoCombo", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// WMS 전송용 LotNo와 전송플래그 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strStdNumber">원자재이상관리번호</param>
        /// <param name="strReqNo">수입검사의뢰번호</param>
        /// <param name="strReqSeq">수입검사의뢰번호순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSMaterialAbnormalDFault_Mat_WMSData(string strPlantCode, string strStdNumber, string strReqNo, string strReqSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSMaterialAbnormalDFault_WMSData", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }




        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="strValue">Paramter 인자값</param>
        private void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, byte[] Value)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = Value;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 데이터테이블 설정
        /// </summary>
        /// <returns>Parameter테이블</returns>
        private DataTable mfSetParamDataTable()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable();

                DataColumn dc = new DataColumn("ParamName", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("ParamDirect", typeof(ParameterDirection));
                dt.Columns.Add(dc);

                dc = new DataColumn("DBType", typeof(SqlDbType));
                dt.Columns.Add(dc);

                dc = new DataColumn("Value", typeof(object));
                dt.Columns.Add(dc);

                dc = new DataColumn("Length", typeof(int));
                dt.Columns.Add(dc);

                return dt;
            }
            catch (Exception ex)
            {
                //throw ex;
                return dt;
            }
        }

        /// <summary>
        /// 저장프로시저 트랙잭션(DML) 함수 실행 : SP함수명 및 인자(데이터테이블)가 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns></returns>
        private string mfExecTransStoredProc(SqlConnection Conn, SqlTransaction Trans, string strSPName, DataTable dtSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_Cmd = new SqlCommand();
                    m_Cmd.Connection = Conn;
                    m_Cmd.Transaction = Trans;
                    m_Cmd.CommandType = CommandType.StoredProcedure;
                    m_Cmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        param.SqlValue = dr["Value"];

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                            {
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                            }
                        }

                        m_Cmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_Cmd.ExecuteScalar());

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_Cmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_Cmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_Cmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    Debug.Print("DataBase 연결되지 않았습니다.");
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AVL")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AVL : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("ResultFile", typeof(string));
                dtRtn.Columns.Add("ImproveFile", typeof(string));
                dtRtn.Columns.Add("AuditGrade", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 검색조건에 따른 AVL정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strAuditGrade">AuditGrade</param>
        /// <param name="strFromWriteDate">작성일(From)</param>
        /// <param name="strToWriteDate">작성일(To)</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAVL(string strPlantCode, string strVendorCode, string strAuditGrade, string strFromWriteDate, string strToWriteDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터테이블설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAuditGrade", ParameterDirection.Input, SqlDbType.VarChar, strAuditGrade, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strFromWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strFromWriteDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strToWriteDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 저장프로시저실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAVL", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AVL리스트 더블클릭시 상세데이터 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAVLDetail(string strPlantCode, string strVendorCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터테이블설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 저장프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAVLDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AVL정보 헤더 저장 Method
        /// </summary>
        /// <param name="dtHeader"> 헤더정보 데이터테이블 </param>
        /// <param name="strUserID"> 사용자아이디 </param>
        /// <param name="strUserIP"> 사용자아이피 </param>
        /// <param name="dtFile"> 첨부파일 데이터테이블 </param>
        /// <param name="dtConfirm"> 인증서정보 데이터테이블 </param>
        /// <param name="dtHistory"> 이력정보 데이터테이블 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSAVL(DataTable dtHeader, string strUserID, string strUserIP, DataTable dtFile, DataTable dtConfirm)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";

                //DB 연결
                sql.mfConnect();

                // 트랜잭션 변수 선언
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAuditGrade", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AuditGrade"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSAVL", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // 삭제를 위한 변수 설정
                        string strPlantCode = dtHeader.Rows[0]["PlantCode"].ToString();
                        string strVendorCode = dtHeader.Rows[0]["VendorCode"].ToString();

                        // 성공이면 첨부파일, 인증서정보, 이력정보 삭제후 저장

                        if (dtFile.Rows.Count > 0)
                        {
                            // 첨부파일 삭제/저장
                            AVLFile clsFile = new AVLFile();

                            strErrRtn = clsFile.mfDeleteINSAVLFile(strPlantCode, strVendorCode, sql.SqlCon, trans);
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }

                            strErrRtn = clsFile.mfSaveINSAVLFile(dtFile, strUserID, strUserIP, sql.SqlCon, trans);
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (dtConfirm.Rows.Count > 0)
                        {
                            // 인증서정보 삭제/저장
                            AVLConfirm clsComfirm = new AVLConfirm();

                            strErrRtn = clsComfirm.mfDeleteINSAVLConfirm(strPlantCode, strVendorCode, sql.SqlCon, trans);
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }

                            strErrRtn = clsComfirm.mfSaveINSAVLConfirm(dtConfirm, strUserID, strUserIP, sql.SqlCon, trans);
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // 이력정보 저장
                        AVLHistory clsHistory = new AVLHistory();
                        strErrRtn = clsHistory.mfSaveINSAVLHistory(dtHeader, strUserID, strUserIP, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// AVL 정보 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSAVL(string strPlantCode, string strVendorCode)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                // DB 연결
                sql.mfConnect();
                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // File, 인증서, 이력정보부터 삭제
                AVLConfirm clsConfirm = new AVLConfirm();
                strErrRtn = clsConfirm.mfDeleteINSAVLConfirm(strPlantCode, strVendorCode, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                AVLFile clsFile = new AVLFile();
                strErrRtn = clsFile.mfDeleteINSAVLFile(strPlantCode, strVendorCode, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                AVLHistory clsHistory = new AVLHistory();
                strErrRtn = clsHistory.mfDeleteINSAVLHistory(strPlantCode, strVendorCode, sql.SqlCon, trans);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 헤더정보 삭제
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSAVL", dtParam);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AVLConfirm")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AVLConfirm : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("ConfirmName", typeof(string));
                dtRtn.Columns.Add("ConfirmDate", typeof(string));
                dtRtn.Columns.Add("FileTitle", typeof(string));
                dtRtn.Columns.Add("FilePassName", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AVL 인증서정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAVLConfirm(string strPlantCode, string strVendorCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터테이블설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);

                // 저장프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAVLConfirm", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AVL 인증서정보 저장Method
        /// </summary>
        /// <param name="dtConfirm"> 인증서정보 데이터테이블 </param>
        /// <param name="strUserID"> 사용자아이디 </param>
        /// <param name="strUserIP"> 사용자아이피 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSAVLConfirm(DataTable dtConfirm, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtConfirm.Rows.Count; i++)
                {
                    // 파라미터 데이터테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtConfirm.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtConfirm.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtConfirm.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strConfirmName", ParameterDirection.Input, SqlDbType.NVarChar, dtConfirm.Rows[i]["ConfirmName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtConfirm.Rows[i]["ConfirmDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtConfirm.Rows[i]["FileTitle"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePassName", ParameterDirection.Input, SqlDbType.NVarChar, dtConfirm.Rows[i]["FilePassName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtConfirm.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSAVLConfirm", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// AVL 인증서 정보 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="sqlCon"> SqlConnentionc 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSAVLConfirm(string strPlantCode, string strVendorCode, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                // 파라미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSAVLConfirm", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AVLFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AVLFile : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("FileTitle", typeof(string));
                dtRtn.Columns.Add("FilePassName", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AVL 첨부파일정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAVLFile(string strPlantCode, string strVendorCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터테이블설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);

                // 저장프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAVLFile", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AVL 첨부파일 저장 Method
        /// </summary>
        /// <param name="dtFile"> 첨부파일정보 데이터테이블 </param>
        /// <param name="strUserID"> 사용자아이디 </param>
        /// <param name="strUserIP"> 사용자아이피 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSAVLFile(DataTable dtFile, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtFile.Rows.Count; i++)
                {
                    // 파라미터 데이터테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFile.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtFile.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtFile.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFileTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtFile.Rows[i]["FileTitle"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePassName", ParameterDirection.Input, SqlDbType.NVarChar, dtFile.Rows[i]["FilePassName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtFile.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSAVLFile", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// AVL 첨부FIle 정보 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="sqlCon"> SqlConnentionc 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSAVLFile(string strPlantCode, string strVendorCode, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                // 파라미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSAVLFile", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AVLHistory")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AVLHistory : ServicedComponent
    {
        /// <summary>
        /// AVL 이력정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAVLHistory(string strPlantCode, string strVendorCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터테이블설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 저장프로시저 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAVLHistory", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AVL 이력정보 저장 Method
        /// </summary>
        /// <param name="dtHistory"> 이력정보 데이터테이블 </param>
        /// <param name="strUserID"> 사용자아이디 </param>
        /// <param name="strUserIP"> 사용자아이피 </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSAVLHistory(DataTable dtHistory, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtHistory.Rows.Count; i++)
                {
                    // 파라미터 데이터테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHistory.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtHistory.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHistory.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHistory.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAuditGrade", ParameterDirection.Input, SqlDbType.VarChar, dtHistory.Rows[i]["AuditGrade"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHistory.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultReport", ParameterDirection.Input, SqlDbType.NVarChar, dtHistory.Rows[i]["ResultFile"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strImproveReport", ParameterDirection.Input, SqlDbType.NVarChar, dtHistory.Rows[i]["ImproveFile"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSAVLHistory", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// AVL 이력관리정보 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="sqlCon"> SqlConnentionc 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSAVLHistory(string strPlantCode, string strVendorCode, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                // 파라미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_INSAVLHistory", dtParam);

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AML")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AML : ServicedComponent
    {
        /// <summary>
        /// DataTable Column 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("MaterialCode", typeof(string));
                dtRtn.Columns.Add("MoldSeq", typeof(string));
                dtRtn.Columns.Add("Spec", typeof(string));
                dtRtn.Columns.Add("SpecNo", typeof(string));
                dtRtn.Columns.Add("CustomerCode", typeof(string));
                dtRtn.Columns.Add("CustomerAdmitDate", typeof(string));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("MaterialGrade", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("InspectLevel", typeof(string));
                dtRtn.Columns.Add("InspectRate", typeof(string));
                dtRtn.Columns.Add("AQLFlag", typeof(char));
                dtRtn.Columns.Add("HistoryInfo", typeof(char));
                dtRtn.Columns.Add("ShipCount", typeof(Int32));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수입검사등록에서 저장후 검사결과 AML에 저장하는 DataTable Column 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_InspectReq()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("MaterialCode", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("SpecNo", typeof(string));
                dtRtn.Columns.Add("MoldSeq", typeof(string));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("InspectResult", typeof(string));
                dtRtn.Columns.Add("MaterialGrade", typeof(string));
                dtRtn.Columns.Add("AcceptCount", typeof(Int32));
                dtRtn.Columns.Add("RejectCount", typeof(Int32));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 검색조건에 따른 조회 Method
        /// </summary>
        /// <param name="strPlnatCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialGrade">자재등급</param>
        /// <param name="strFromWriteDate">등록일(From)</param>
        /// <param name="strToWriteDate">등록일(To)</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAML(string strPlantCode, string strVendorCode, string strMaterialGrade, string strFromWriteDate, string strToWriteDate,
                                      string strMaterialCode, string strConsumableTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // 파리미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialGrade", ParameterDirection.Input, SqlDbType.VarChar, strMaterialGrade, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strFromWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strFromWriteDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToWriteDate", ParameterDirection.Input, SqlDbType.VarChar, strToWriteDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strConsumableTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAML", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// INSAML 헤더 상세조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadINSAML_Detail(string strPlantCode, string strVendorCode, string strMaterialCode, string strMoldSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // 파리미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, strMoldSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAMLDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AML 헤더정보 저장Method
        /// </summary>
        /// <param name="dtSave">헤더정보저장된 DataTable</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSAML(DataTable dtSave, DataTable dtDelHistory, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MoldSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["Spec"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["SpecNo"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["CustomerCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["CustomerAdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialGrade", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialGrade"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectLevel", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectLevel"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectRate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectRate"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strAQLFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["AQLFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strHistoryInfo", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["HistoryInfo"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intShipCount", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ShipCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSAML", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        if (dtDelHistory.Rows.Count > 0)
                        {
                            AMLHistory DelHistory = new AMLHistory();
                            strErrRtn = DelHistory.mfDeleteINSAMLHistory(dtDelHistory, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수입검사완료시 AML 헤더/이력정보 저장 Method
        /// </summary>
        /// <param name="dtSave">AML 저장정보가 담긴 DataTable</param>
        /// <param name="strUserID">사용자 IP</param>
        /// <param name="strUserIP">사용자 ID</param>
        /// <param name="sqlCon">Sqlconnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveINSAMLRegist(DataTable dtSave, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                SQLS sql = new SQLS();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["SpecNo"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MoldSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["WriteDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResult", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectResult"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialGrade", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialGrade"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intAcceptCount", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["AcceptCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intRejectCount", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["RejectCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSAMLRegist", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// AML 정보 삭제 메소드
        /// </summary>
        /// <param name="dtDelete">키가 저장된 데이터 테이블</param>
        /// <returns></returns>
        public string mfDeleteINSAML(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();

                // 트랜잭션시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam;
                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["MoldSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSAML", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("AMLHistory")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class AMLHistory : ServicedComponent
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("MaterialCode", typeof(string));
                dtRtn.Columns.Add("MoldSeq", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(int));
                dtRtn.Columns.Add("SpecNo", typeof(string));
                dtRtn.Columns.Add("MaterialGrade", typeof(string));
                dtRtn.Columns.Add("GrDate", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("InspectResult", typeof(string));
                dtRtn.Columns.Add("GrNo", typeof(string));
                dtRtn.Columns.Add("ShipmentCount", typeof(int));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// AML 이력정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadAMLHistory(string strPlantCode, string strMaterialCode, string strVendorCode, string strMoldSeq, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, strMoldSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAMLHistory", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AQL 적용을 위한 정보 조회하는 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <param name="strSpecNo">SpecNo</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadAMLForAQL(string strPlantCode, string strVendorCode, string strMaterialCode, string strSpecNo, string strMoldSeq)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, strSpecNo, 200);
                sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, strMoldSeq, 4);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAMLForAQL", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// AMLHistory 삭제 메소드
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteINSAMLHistory(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMoldSeq", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MoldSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_INSAMLHistory", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }
}
