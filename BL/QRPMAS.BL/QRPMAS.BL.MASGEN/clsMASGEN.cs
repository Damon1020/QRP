﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 공통기준정보                                          */
/* 프로그램ID   : clsMASGEN.cs                                          */
/* 프로그램명   : 고객정보                                              */
/* 작성자       : 정결                                                  */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-07-25 : 고객정보 추가 (이종호)                   */
/*                2011-07-25 : 거래처정보 추가 (이종호)                 */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
              Authentication = AuthenticationOption.None,
             ImpersonationLevel = ImpersonationLevelOption.Impersonate)]


namespace QRPMAS.BL.MASGEN
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Unit")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class Unit : ServicedComponent 
    {
        /// <summary>
        ///  컬럼 설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtUnit = new DataTable();
            try
            {
                dtUnit.Columns.Add("UnitCode", typeof(String));
                dtUnit.Columns.Add("UnitName", typeof(String));
                dtUnit.Columns.Add("UseFlag", typeof(Char));

                return dtUnit;
            }
            catch (Exception ex)
            {
                return dtUnit;
                throw (ex);
            }
            finally
            {
                dtUnit.Dispose();
            }
        }

        /// <summary>
        /// 단위정보 검색용 Method
        /// </summary>
        /// <param name="strUnitCode">단위 코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadUnit(String strUnitName, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtUnit = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strUnitName", ParameterDirection.Input, SqlDbType.VarChar, strUnitName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtUnit = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASUnit", dtParam);

                return dtUnit;
            }
            catch (Exception ex)
            {
                return dtUnit;
                throw(ex);
            }
            finally 
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtUnit.Dispose();
            }
        }



        /// <summary>
        /// 단위정보 콤보박스용 Method
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASUnitCombo()
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                // DB 접속
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASUnitCombo");

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        [AutoComplete(false)]
        public String mfSaveMASUnit(DataTable dtUnit, String strUserIP, String strUserID)
        {
            // 변수 선언
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                
                for (int i = 0; i < dtUnit.Rows.Count; i++)
                {
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtUnit.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitName", ParameterDirection.Input, SqlDbType.NVarChar, dtUnit.Rows[i]["UnitName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.VarChar, dtUnit.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASUnit", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public String mfDeleteMASUnit(DataTable dtUnit)
        {
            // 변수 선언
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();

                for (int i = 0; i < dtUnit.Rows.Count; i++)
                {
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtUnit.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASUnit", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Customer")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Customer : ServicedComponent
    {
        /// <summary>
        /// DataTable Setting
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("CustomerCode", typeof(String));
                dt.Columns.Add("Seq", typeof(Int32));
                dt.Columns.Add("DeptName", typeof(String));
                dt.Columns.Add("PersonName", typeof(String));
                dt.Columns.Add("Position", typeof(String));
                dt.Columns.Add("Tel", typeof(String));
                dt.Columns.Add("Hp", typeof(String));
                dt.Columns.Add("EMail", typeof(String));
                dt.Columns.Add("EtcDesc", typeof(String));
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// 고객정보 조회
        /// </summary>
        /// <param name="strCustomerName"> 고객명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCustomer()
        {
            // 인스턴스 객체 생성
            SQLS sql = new SQLS();
            // 반환용 DataTable 생성
            DataTable dtCustomer = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // SP 실행
                dtCustomer = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCustomer");

                return dtCustomer;
            }
            catch (Exception ex)
            {
                return dtCustomer;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCustomer.Dispose();
            }
        }

        /// <summary>
        /// 고객정보 Popup 조회
        /// </summary>
        /// <param name="strCustomerName"> 고객명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCustomerPopup(string strLang)
        {
            // 인스턴스 객체 생성
            SQLS sql = new SQLS();
            // 반환용 DataTable 생성
            DataTable dtCustomer = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                // SP 실행
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dtCustomer = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCustomerPopup", dtParam);

                return dtCustomer;
            }
            catch (Exception ex)
            {
                return dtCustomer;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCustomer.Dispose();
            }
        }

        /// <summary>
        /// 고객정보 상세조회
        /// </summary>
        /// <param name="strCustomerCode"> 고객코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCustomerDetail(String strCustomerCode, String strLang)
        {
            // 인스턴스 객체 생성
            SQLS sql = new SQLS();
            // 반환용 DataTable 생성
            DataTable dtCustomer = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter DataTable에 매개변수 설정
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtCustomer = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCustomerDetail", dtParam);

                return dtCustomer;
            }
            catch (Exception ex)
            {
                return dtCustomer;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCustomer.Dispose();
            }
        }

        /// <summary>
        /// 고객담당자 정보 조회
        /// </summary>
        /// <param name="strCustomerCode"> 고객코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCustomerP(String strCustomerCode)
        {
            SQLS sql = new SQLS();
            DataTable dtCustomerP = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                // Paremeter 저장
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);

                // SP 실행
                 dtCustomerP= sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCustomerP", dtParam);

                 return dtCustomerP;
            }
            catch (Exception ex)
            {
                return dtCustomerP;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCustomerP.Dispose();
            }
        }


        /// <summary>
        /// 고객사 콤보조회
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns>고객사정보</returns>
        [AutoComplete]
        public DataTable mfReadCustomer_Combo(string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCustomerP = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                // Paremeter 저장
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtCustomerP = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCustomer_Combo", dtParam);

                return dtCustomerP;
            }
            catch (Exception ex)
            {
                return dtCustomerP;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCustomerP.Dispose();
            }
        }

        /// <summary>
        /// 고객담당자 저장 Method
        /// </summary>
        /// <param name="dtCustomerP"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> IP </param>
        /// <param name="strUserID"> ID </param>
        /// <param name="sqlCon"> SqlConnection </param>
        /// <param name="trans"> SqlTransaction </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCustomerP(DataTable dtCustomerP, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            // 변수 선언
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();

                for (int i = 0; i < dtCustomerP.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtCustomerP.Rows[i]["CustomerCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtCustomerP.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strDeptName", ParameterDirection.Input, SqlDbType.NVarChar, dtCustomerP.Rows[i]["DeptName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPersonName", ParameterDirection.Input, SqlDbType.NVarChar, dtCustomerP.Rows[i]["PersonName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPosition", ParameterDirection.Input, SqlDbType.NVarChar, dtCustomerP.Rows[i]["Position"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strTel", ParameterDirection.Input, SqlDbType.VarChar, dtCustomerP.Rows[i]["Tel"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHp", ParameterDirection.Input, SqlDbType.VarChar, dtCustomerP.Rows[i]["Hp"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEMail", ParameterDirection.Input, SqlDbType.VarChar, dtCustomerP.Rows[i]["EMail"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtCustomerP.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_MASCustomerP", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 고객담당자 삭제 Method
        /// </summary>
        /// <param name="dtDelCusP"> 삭제할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> IP </param>
        /// <param name="strUserID"> ID </param>
        /// <param name="dtSaveCusP"> 저장할 정보가 담긴 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteCustomerP(String strCustomerCode, String strUserIP, String strUserID, DataTable dtSaveCusP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                SqlTransaction trans;

                // 트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();
                
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASCustomerP", dtParam);
                // 결과값 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 저장할 데이터가 있고 성공시 저장Method 실행
                if (dtSaveCusP.Rows.Count > 0)
                {
                    strErrRtn = mfSaveCustomerP(dtSaveCusP, strUserIP, strUserID, sql.SqlCon, trans);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                // 저장할 데이터가 없는경우 Commit
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Vendor")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Vendor : ServicedComponent
    {
        /// <summary>
        /// DataTable Setting
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("VendorCode", typeof(String));
                dt.Columns.Add("Seq", typeof(Int32));
                dt.Columns.Add("DeptName", typeof(String));
                dt.Columns.Add("PersonName", typeof(String));
                dt.Columns.Add("Position", typeof(String));
                dt.Columns.Add("Tel", typeof(String));
                dt.Columns.Add("Hp", typeof(String));
                dt.Columns.Add("EMail", typeof(String));
                dt.Columns.Add("EtcDesc", typeof(String));
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }
        /// <summary>
        /// 거래처정보 조회
        /// </summary>
        /// <param name="strVendorName"> 거래처명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadVendor(string strVendorCode ,string strVendorName,string strLang)
        {
            // 인스턴스 객체 생성
            SQLS sql = new SQLS();
            // 반환용 DataTable 생성
            DataTable dtVendor = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParame = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParame, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 20);
                sql.mfAddParamDataRow(dtParame, "@i_strVendorName", ParameterDirection.Input, SqlDbType.NVarChar, strVendorName, 50);
                sql.mfAddParamDataRow(dtParame, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtVendor = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASVendor", dtParame);

                return dtVendor;
            }
            catch (Exception ex)
            {
                return dtVendor;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVendor.Dispose();
            }
        }

        /// <summary>
        /// 거래처정보 Popup 조회
        /// </summary>
        /// <param name="strVendorName"> 거래처명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadVendorPopup(string strLang)
        {
            // 인스턴스 객체 생성
            SQLS sql = new SQLS();
            // 반환용 DataTable 생성
            DataTable dtVendor = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                // SP 실행
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dtVendor = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASVendorPopup", dtParam);

                return dtVendor;
            }
            catch (Exception ex)
            {
                return dtVendor;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVendor.Dispose();
            }
        }

        /// <summary>
        /// VendorCode 콤보
        /// </summary>
        /// <param name="strVendorCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadVendorPCombo(String strVendorCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtVPCombo = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dtVPCombo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASVendorPCombo", dtParam);
                return dtVPCombo;
            }
            catch (Exception ex)
            {
                return dtVPCombo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVPCombo.Dispose();
            }
        }



        /// <summary>
        /// 거래처정보 상세조회
        /// </summary>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadVendorDetail(String strVendorCode, String strLang)
        {
            // 인스턴스 객체 생성
            SQLS sql = new SQLS();
            // 반환용 DataTable 생성
            DataTable dtVendor = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter DataTable에 매개변수 설정
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtVendor = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASVendorDetail", dtParam);

                return dtVendor;
            }
            catch (Exception ex)
            {
                return dtVendor;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVendor.Dispose();
            }
        }

        /// <summary>
        /// 거래처담당자 정보 조회
        /// </summary>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadVendorP(String strVendorCode)
        {
            SQLS sql = new SQLS();
            DataTable dtVendorP = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                // Paremeter 저장
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);

                // SP 실행
                dtVendorP = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASVendorP", dtParam);

                return dtVendorP;
            }
            catch (Exception ex)
            {
                return dtVendorP;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVendorP.Dispose();
            }
        }

        /// <summary>
        /// 표준문서등록 그리드 삽입용
        /// </summary>
        /// <param name="strVendorCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadVendorPForGrid(String strVendorCode, int intSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtVendorPForGrid = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                // Paremeter 저장
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.VarChar, intSeq.ToString());

                // SP 실행
                dtVendorPForGrid = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASVendorPForGrid", dtParam);

                return dtVendorPForGrid;
            }
            catch (Exception ex)
            {
                return dtVendorPForGrid; 
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVendorPForGrid.Dispose();
            }
        }

        /// <summary>
        /// 거래처담당자 저장 Method
        /// </summary>
        /// <param name="dtVendorP"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> IP </param>
        /// <param name="strUserID"> ID </param>
        /// <param name="sqlCon"> SqlConnection </param>
        /// <param name="trans"> SqlTransaction </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveVendorP(DataTable dtVendorP, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            // 변수 선언
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();

                for (int i = 0; i < dtVendorP.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtVendorP.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtVendorP.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strDeptName", ParameterDirection.Input, SqlDbType.NVarChar, dtVendorP.Rows[i]["DeptName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPersonName", ParameterDirection.Input, SqlDbType.NVarChar, dtVendorP.Rows[i]["PersonName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPosition", ParameterDirection.Input, SqlDbType.NVarChar, dtVendorP.Rows[i]["Position"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strTel", ParameterDirection.Input, SqlDbType.VarChar, dtVendorP.Rows[i]["Tel"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHp", ParameterDirection.Input, SqlDbType.VarChar, dtVendorP.Rows[i]["Hp"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEMail", ParameterDirection.Input, SqlDbType.VarChar, dtVendorP.Rows[i]["EMail"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtVendorP.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_MASVendorP", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 거래처담당자 삭제 Method
        /// </summary>
        /// <param name="dtDelCusP"> 삭제할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> IP </param>
        /// <param name="strUserID"> ID </param>
        /// <param name="dtSaveCusP"> 저장할 정보가 담긴 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASVendorP(String strVendorCode, String strUserIP, String strUserID, DataTable dtSaveCusP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASVendorP", dtParam);
                // 결과값 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                
                // 저장할 데이터가 있고 성공시 저장Method 실행
                if (dtSaveCusP.Rows.Count > 0)
                {
                    strErrRtn = mfSaveVendorP(dtSaveCusP, strUserIP, strUserID, sql.SqlCon, trans);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                // 저장할 데이터가 없는경우 Commit
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }
}
