﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels; 
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;
using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPMAS.BL.MASDMM
{

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableMatType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableMatType : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPStock = new DataTable();

            try
            {
                dtSPStock.Columns.Add("PlantCode", typeof(String));
                dtSPStock.Columns.Add("DurableMatTypeCode", typeof(String));
                dtSPStock.Columns.Add("DurableMatTypeName", typeof(String));
                dtSPStock.Columns.Add("DurableMatTypeNameCh", typeof(String));
                dtSPStock.Columns.Add("DurableMatTypeNameEn", typeof(String));
                dtSPStock.Columns.Add("UseFlag", typeof(String));


                return dtSPStock;
            }
            catch (Exception ex)
            {
                return dtSPStock;
                throw (ex);
            }
            finally
            {
                dtSPStock.Dispose();
            }
        }

        /// <summary>
        /// 치공구유형조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>치공구유형조회</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatType(String strPlantCode, String strLang)
        {
            DataTable dtDurableMatType = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMatType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatType", dtParam);

                //정보리턴
                return dtDurableMatType;
            }
            catch (Exception ex)
            {
                return dtDurableMatType;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatType.Dispose();
            }
        }


        /// <summary>
        /// Area정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>Area정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatType_combo(String strPlantCode, String strLang)
        {
            DataTable dtDurableMatType = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMatType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatType_combo", dtParam);

                //정보리턴
                return dtDurableMatType;
            }
            catch (Exception ex)
            {
                return dtDurableMatType;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatType.Dispose();
            }
        }
    }



    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableMat")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableMat : ServicedComponent

    {

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDataColumnsinfo()
        {
            DataTable dtDurableMat = new DataTable();

            try
            {
                dtDurableMat.Columns.Add("PlantCode", typeof(String));
                dtDurableMat.Columns.Add("DurableMatCode", typeof(String));
                dtDurableMat.Columns.Add("DurableMatName", typeof(String));
                dtDurableMat.Columns.Add("DurableMatNameCh", typeof(String));
                dtDurableMat.Columns.Add("DurableMatNameEn", typeof(String));
                dtDurableMat.Columns.Add("DurableMatTypeCode", typeof(String));
                dtDurableMat.Columns.Add("UsageLimitLower", typeof(String));
                dtDurableMat.Columns.Add("UsageLimit", typeof(String));
                dtDurableMat.Columns.Add("UsageLimitUpper", typeof(String));
                dtDurableMat.Columns.Add("UsageLimitUnitName", typeof(String));
                dtDurableMat.Columns.Add("UseFlag", typeof(String));

                dtDurableMat.Columns.Add("UserIP", typeof(String));
                dtDurableMat.Columns.Add("UserID", typeof(String));

                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                dtDurableMat.Dispose();
            }
        }



        /// <summary>
        /// 금형치공구 POPUP
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatPOPUP(string strPlantCode, string strLang)
        {
            DataTable dtDurableMatPOP = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtDurableMatPOP = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatPopup", dtParam);

                return dtDurableMatPOP;
            }
            catch (Exception ex)
            {
                return dtDurableMatPOP;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatPOP.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구 POPUP
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatPOPUP_Package(string strPlantCode,string strPackage, string strLang)
        {
            DataTable dtDurableMatPOP = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtDurableMatPOP = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatPopup_Package", dtParam);

                return dtDurableMatPOP;
            }
            catch (Exception ex)
            {
                return dtDurableMatPOP;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatPOP.Dispose();
            }
        }

        /// <summary>		Durablemat
        /// 금형치공구코드 명 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구명</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatName(string strPlantCode, string strDurableMatCode, string strLang)
        {

            DataTable dtDurableMatName = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtDurableMatName = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatName", dtParam);
                //정보리턴
                return dtDurableMatName;
            }
            catch (Exception ex)
            {
                return dtDurableMatName;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatName.Dispose();
            }

        }


        /// <summary>
        ///금형치공구코드 명(Pakcage + LotNo) 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구명</returns>
        [AutoComplete]
        public DataTable mfReadDurableMat_Name(string strPlantCode, string strLotNo, string strPackage ,string strLang)
        {

            DataTable dtDurableMatName = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저실행
                dtDurableMatName = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMat_Name", dtParam);
                //정보리턴
                return dtDurableMatName;
            }
            catch (Exception ex)
            {
                return dtDurableMatName;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatName.Dispose();
            }

        }

        /// <summary>		
        /// 금형치공구 명 콤보 (중복제거)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구명</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatNameCombo(string strPlantCode, string strDurableMatCode, string strLang)
        {

            DataTable dtDurableMatName = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtDurableMatName = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatName_Combo", dtParam);
                //정보리턴
                return dtDurableMatName;
            }
            catch (Exception ex)
            {
                return dtDurableMatName;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMatName.Dispose();
            }

        }

        /// <summary>
        /// 금형치공구 상세정보  조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatDetail(String strPlantCode, String strDurableMatCode, String strLang)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatDetail", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }



        #region 금형치공구정보(기준정보)

        #region STS

        /// <summary>
        /// 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfoH()
        {
            DataTable dtDurableMat = new DataTable();

            try
            {
                dtDurableMat.Columns.Add("PlantCode", typeof(string));
                dtDurableMat.Columns.Add("DurableMatCode", typeof(string));
                dtDurableMat.Columns.Add("DurableMatName", typeof(string));
                dtDurableMat.Columns.Add("DurableMatNameCh", typeof(string));
                dtDurableMat.Columns.Add("DurableMatNameEn", typeof(string));
                dtDurableMat.Columns.Add("Spec", typeof(string));
                dtDurableMat.Columns.Add("UsageLimitLower", typeof(string));
                dtDurableMat.Columns.Add("UsageLimit", typeof(string));
                dtDurableMat.Columns.Add("UsageLimitUpper", typeof(string));
                dtDurableMat.Columns.Add("SerialFlag", typeof(string));
                dtDurableMat.Columns.Add("UseFlag", typeof(string));

                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                dtDurableMat.Dispose();
            }
        }

        /// <summary>
        /// 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfoDel()
        {
            DataTable dtDurableMat = new DataTable();

            try
            {
                dtDurableMat.Columns.Add("PlantCode", typeof(string));
                dtDurableMat.Columns.Add("DurableMatCode", typeof(string));

                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                dtDurableMat.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장정보</param>
        /// <param name="strDurableMatTypeCode">금형치공구유형</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMat_Package(string strPlantCode,string strDurableMatTypeCode, string strPackage,string strDurableMatName,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableMatPack = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableMatTypeCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableMatTypeCode,5);
                sql.mfAddParamDataRow(dtParam,"@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar,strPackage,40);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableMatName, 50);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                //금형치공구조회 (공장,치공구유형, Package) 조건
                dtDurableMatPack = sql.mfExecReadStoredProc(sql.SqlCon,"up_Select_MASDurableMat_Package",dtParam);


                //금형치공구정보 리턴
                return dtDurableMatPack;

            }
            catch (Exception ex)
            {
                return dtDurableMatPack;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDurableMatPack.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatTypeCode">금형치공구유형코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>정보 리턴</returns>
        [AutoComplete]
        public DataTable mfReadDurableMat(String strPlantCode, String strDurableMatTypeCode, String strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatTypeCode, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMat", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구정보 저장
        /// </summary>
        ///<param name="dtDurableMat"></param>
        ///<param name="dtDurableMatBOM_Model"></param>
        ///<param name="dtDurableMatBOM_Package"></param>
        ///<param name="strDurableMatCode"></param>
        ///<param name="strPlantCode"></param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMat(DataTable dtDurableMat,DataTable dtDurableMatBOM_Model,DataTable dtDurableMatBOM_Package,
                                        DataTable dtDurableLot, DataTable dtDurableStock, DataTable dtDurableStockMoveHist,
                                        DataTable dtDurableRepairH,DataTable dtDurableRepairD,DataTable dtRepairLotHistory,
                                            string strPlantCode,string strDurableMatCode, string strSendMDM,string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();
                //트랜젝션시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();


                for (int i = 0; i < dtDurableMat.Rows.Count; i++)
                {
                    //파라미터 정보 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["PlantCode"].ToString(), 10);                   //공장
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["DurableMatCode"].ToString(), 20);         //금형치공구코드
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatName"].ToString(), 50);        //금형치공구명
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameCh"].ToString(), 50);    //금형치공구명 중문
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameEn"].ToString(), 50);    //금형치공구명 영문
                    sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["Spec"].ToString(), 100);                           //규격

                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimitLower", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitLower"].ToString());           //사용한계하한허용치
                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimit", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimit"].ToString());                     //사용한계치
                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimitUpper", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitUpper"].ToString());           //사용한계상한허용치

                    sql.mfAddParamDataRow(dtParam, "@i_strSerialFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableMat.Rows[i]["SerialFlag"].ToString(), 1);                     //Serial여부
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableMat.Rows[i]["UseFlag"].ToString(), 1);                           //사용여부

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //금형치공구정보저장프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurableMatH", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 트랜 롤백 후 
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                }

                #region BOM_Model 처리
                //금형치공구 정보를 저장 한 후 BOM정보를 처리한다.
                DurableMatBOM_Model clsBOM_Model = new DurableMatBOM_Model();

                //금형치공구BOM_Model 삭제 매서드 실행
                if (!strPlantCode.Equals(string.Empty))
                {
                    strErrRtn = clsBOM_Model.mfDeleteDurableMatBOM_Model(strPlantCode, strDurableMatCode, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 리턴
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //금형치공구 BOM_Model 저장 정보가 있을 경우
                if (dtDurableMatBOM_Model.Rows.Count > 0)
                {

                    //금형치공구 BOM_Model 정보저장매서드 실행
                    strErrRtn = clsBOM_Model.mfSaveDurableMatBOM_Model(dtDurableMatBOM_Model, strUserIP, strUserID, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과가 실패시 롤백처리
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                }
                #endregion

                #region Package

                DurableMatBOM_Package clsBOM_Package = new DurableMatBOM_Package();

                if (!strPlantCode.Equals(string.Empty))
                {
                    //Package 삭제 매서드 실행
                    strErrRtn = clsBOM_Package.mfDeleteDurableMatBOM_Package(strPlantCode, strDurableMatCode, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //삭제처리실패시 롤백
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //금형치공구 BOM 저장 매서드 실행
                if (dtDurableMatBOM_Package.Rows.Count > 0)
                {
                    //금형치공구BOM정보 저장
                    strErrRtn = clsBOM_Package.mfSaveDurableMatBOM_Package(dtDurableMatBOM_Package, strUserIP, strUserID, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리 실패시 롤백 후 리턴
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                }

                #endregion

                #region Lot Stock Hist
                //---------------- 1. DurableLot  테이블 (MASDurableLot) -------------------//
                

                if (dtDurableLot.Rows.Count > 0)
                {
                    QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();

                    strErrRtn = clsDurableLot.mfSaveMASDurableLot(dtDurableLot, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. Durable 현 재고 테이블 (DMMDurableStock) -------------------//
                

                if (dtDurableStock.Rows.Count > 0)
                {
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();

                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 3. Durable 재고이력 테이블 (DMMDurableStockMoveHist) -------------------//
                

                if (dtDurableStockMoveHist.Rows.Count > 0)
                {
                    QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();

                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                #endregion

                #region 신규Lot정보의 설비투입 및 Flag

                if (dtDurableRepairH.Rows.Count > 0)
                {
                    QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();

                    //신규Lot정보의 설비투입매서드 실행
                    strErrRtn = clsDurableMatRepairH.mfSaveDurableMatRepairH_RepairLot(dtDurableRepairH, dtDurableRepairD, dtRepairLotHistory, strUserIP, strUserID, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 정보를 롤백시키고 리턴
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                #endregion

                if (ErrRtn.ErrNum.Equals(0)) 
                {
                    trans.Commit();

                    if (strSendMDM.Equals("T"))
                    {
                        mfSaveDurableMat_MDM(dtDurableMat, strUserIP, strUserID, sql.SqlCon);
                        //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        //if (!ErrRtn.ErrNum.Equals(0))
                        //    return strErrRtn;
                    }
                }
                
                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비연결종료
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        /// <summary>
        ///금형치공구정보 삭제
        /// </summary>
        /// <param name="dtDurableMat">공장코드,금형치공구코드</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableMat(DataTable dtDurableMat,string strUserIP,string strUserID)//string strPlantCode, string strDurableMatCode)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                

                for (int i = 0; i < dtDurableMat.Rows.Count; i++)
                {
                   //트랜젝션 시작
                    SqlTransaction trans;
                    trans = sql.SqlCon.BeginTransaction();

                    //파라미터정보 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //금형치공구 삭제 프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDurableMat", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();

                        if (ErrRtn.ErrMessage.Equals("00"))
                        {
                            strErrRtn = mfUpdateDurableMat_UseFlag(dtDurableMat.Rows[i]["PlantCode"].ToString(), dtDurableMat.Rows[i]["DurableMatCode"].ToString(), strUserIP, strUserID, sql.SqlCon);
                        }
                        else
                        {
                            return strErrRtn;
                        }
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                
                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구삭제시 FK에러로 튕길경우 사용여부F로 업데이트
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfUpdateDurableMat_UseFlag(string strPlantCode, string strDurableMatCode,string strUserIP,string strUserID,SqlConnection sqlCon)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,30);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableMatCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //삭제시 FK에러로 되었을 경우 사용여부를 업데이트 시킨다.
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_MASDurableMat_UseFlag", dtParam);

                if (!ErrRtn.ErrNum.Equals(0))
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                //처리결과정보
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                
            }
        }

        /// <summary>
        /// 금형치공구정보 MDM 전송
        /// </summary>
        /// <param name="dtDurableMat">금형치공구정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMat_MDM(DataTable dtDurableMat, string strUserIP, string strUserID,SqlConnection sqlcon)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {

                for (int i = 0; i < dtDurableMat.Rows.Count; i++)
                {
                    //파라미터 정보 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["PlantCode"].ToString(), 10);                   //공장
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["DurableMatCode"].ToString(), 20);         //금형치공구코드
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatName"].ToString(), 50);        //금형치공구명
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameCh"].ToString(), 50);    //금형치공구명 중문
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameEn"].ToString(), 50);    //금형치공구명 영문
                    sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["Spec"].ToString(), 100);                           //규격

                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimitLower", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitLower"].ToString());           //사용한계하한허용치
                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimit", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimit"].ToString());                     //사용한계치
                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimitUpper", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitUpper"].ToString());           //사용한계상한허용치

                    sql.mfAddParamDataRow(dtParam, "@i_strSerialFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableMat.Rows[i]["SerialFlag"].ToString(), 1);                     //Serial여부
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableMat.Rows[i]["UseFlag"].ToString(), 1);                           //사용여부

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //금형치공구정보저장프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_MASDurableMat_MDM", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        return strErrRtn;
                    }

                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }

        }


        /// <summary>
        /// 금형치공구정보 MDM 전송
        /// </summary>
        /// <param name="dtDurableMat">금형치공구정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMat_MDM(DataTable dtDurableMat, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                for (int i = 0; i < dtDurableMat.Rows.Count; i++)
                {
                    //파라미터 정보 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["PlantCode"].ToString(), 10);                   //공장
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["DurableMatCode"].ToString(), 20);         //금형치공구코드
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatName"].ToString(), 50);        //금형치공구명
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameCh"].ToString(), 50);    //금형치공구명 중문
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameEn"].ToString(), 50);    //금형치공구명 영문
                    sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["Spec"].ToString(), 100);                           //규격

                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimitLower", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitLower"].ToString());           //사용한계하한허용치
                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimit", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimit"].ToString());                     //사용한계치
                    sql.mfAddParamDataRow(dtParam, "@i_intUsageLimitUpper", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitUpper"].ToString());           //사용한계상한허용치

                    sql.mfAddParamDataRow(dtParam, "@i_strSerialFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableMat.Rows[i]["SerialFlag"].ToString(), 1);                     //Serial여부
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableMat.Rows[i]["UseFlag"].ToString(), 1);                           //사용여부

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //금형치공구정보저장프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_MASDurableMat_MDM", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        return strErrRtn;
                    }

                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        #endregion

        #region PSTS

        /// <summary>
        /// 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfoMat_PSTS()
        {
            DataTable dtDurableMat = new DataTable();

            try
            {
                dtDurableMat.Columns.Add("PlantCode", typeof(string));
                dtDurableMat.Columns.Add("DurableMatCode", typeof(string));
                dtDurableMat.Columns.Add("DurableMatName", typeof(string));
                dtDurableMat.Columns.Add("DurableMatNameCh", typeof(string));
                dtDurableMat.Columns.Add("DurableMatNameEn", typeof(string));
                dtDurableMat.Columns.Add("Spec", typeof(string));
                dtDurableMat.Columns.Add("UsageLimitLower", typeof(string));
                dtDurableMat.Columns.Add("UsageLimit", typeof(string));
                dtDurableMat.Columns.Add("UsageLimitUpper", typeof(string));
                dtDurableMat.Columns.Add("SerialFlag", typeof(string));
                dtDurableMat.Columns.Add("UseFlag", typeof(string));
                dtDurableMat.Columns.Add("ManageLifeFlag", typeof(string));
                dtDurableMat.Columns.Add("ChipQty", typeof(Int32));
                dtDurableMat.Columns.Add("SafeQty", typeof(Int32));

                dtDurableMat.TableName = "DurableMat";

                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                dtDurableMat.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장정보</param>
        /// <param name="strDurableMatTypeCode">금형치공구유형</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMat_PSTS(string strPlantCode, string strPackage, string strDurableMatName, string strInventory,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableMatPack = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableMatName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strInventory", ParameterDirection.Input, SqlDbType.VarChar, strInventory, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //금형치공구조회 (공장,치공구유형, Package) 조건
                dtDurableMatPack = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMat_PSTS", dtParam);


                //금형치공구정보 리턴
                return dtDurableMatPack;

            }
            catch (Exception ex)
            {
                return dtDurableMatPack;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDurableMatPack.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장정보</param>
        /// <param name="strDurableMatTypeCode">금형치공구유형</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMat_PSTS(string strPlantCode, string strDurableMatTypeCode, string strInventory, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableMatPack = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatTypeCode, 5);
                
                sql.mfAddParamDataRow(dtParam, "@i_strInventory", ParameterDirection.Input, SqlDbType.VarChar, strInventory, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //금형치공구조회 (공장,치공구유형, 치공구창고) 조건
                dtDurableMatPack = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMat_Type", dtParam);


                //금형치공구정보 리턴
                return dtDurableMatPack;

            }
            catch (Exception ex)
            {
                return dtDurableMatPack;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDurableMatPack.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구정보 저장
        /// </summary>
        ///<param name="dtDurableMat"></param>
        ///<param name="dtDurableMatBOM_Model"></param>
        ///<param name="dtDurableMatBOM_Package"></param>
        ///<param name="strDurableMatCode"></param>
        ///<param name="strPlantCode"></param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMat_PSTS(DataTable dtDurableMat, DataTable dtDurableMatBOM_Model, DataTable dtDurableMatBOM_Package,
                                        DataTable dtDurableLot, DataTable dtDurableStock, DataTable dtDurableStockMoveHist,
                                        DataTable dtDurableRepairH, DataTable dtDurableRepairD, DataTable dtRepairLotHistory,// DataTable dtDurableMatBOM_Equip,
                                            string strPlantCode, string strDurableMatCode, string strSendMDM, string strUserIP, string strUserID)
        {

            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();
                //트랜젝션시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                    for (int i = 0; i < dtDurableMat.Rows.Count; i++)
                    {

                        //파라미터 정보 저장
                        DataTable dtParam = sql.mfSetParamDataTable();

                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["PlantCode"].ToString(), 10);                   //공장
                        sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["DurableMatCode"].ToString(), 20);         //금형치공구코드
                        sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatName"].ToString(), 50);        //금형치공구명
                        sql.mfAddParamDataRow(dtParam, "@i_strDurableMatNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameCh"].ToString(), 50);    //금형치공구명 중문
                        sql.mfAddParamDataRow(dtParam, "@i_strDurableMatNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameEn"].ToString(), 50);    //금형치공구명 영문
                        sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["Spec"].ToString(), 100);                           //규격
                        sql.mfAddParamDataRow(dtParam, "@i_intUsageLimitLower", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitLower"].ToString());           //사용한계하한허용치
                        sql.mfAddParamDataRow(dtParam, "@i_intUsageLimit", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimit"].ToString());                     //사용한계치
                        sql.mfAddParamDataRow(dtParam, "@i_intUsageLimitUpper", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitUpper"].ToString());           //사용한계상한허용치
                        sql.mfAddParamDataRow(dtParam, "@i_strSerialFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableMat.Rows[i]["SerialFlag"].ToString(), 1);                     //Serial여부
                        sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableMat.Rows[i]["UseFlag"].ToString(), 1);                           //사용여부
                        sql.mfAddParamDataRow(dtParam, "@i_strManageLifeFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["ManageLifeFlag"].ToString(), 1);          //수명관리사용여부
                        sql.mfAddParamDataRow(dtParam, "@i_intSafeQty", ParameterDirection.Input, SqlDbType.Int, dtDurableMat.Rows[i]["SafeQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_intChipQty", ParameterDirection.Input, SqlDbType.Int, dtDurableMat.Rows[i]["ChipQty"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //금형치공구정보저장프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurableMatH_PSTS", dtParam);

                        //Decoding
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //처리실패시 트랜 롤백 후 
                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                    }

                #region BOM_Model 처리 2012-10-11 주석 
                ////금형치공구 정보를 저장 한 후 BOM정보를 처리한다.
                //DurableMatBOM_Model clsBOM_Model = new DurableMatBOM_Model();

                ////금형치공구BOM_Model 삭제 매서드 실행
                //if (!strPlantCode.Equals(string.Empty))
                //{
                //    strErrRtn = clsBOM_Model.mfDeleteDurableMatBOM_Model(strPlantCode, strDurableMatCode, sql.SqlCon, trans);

                //    //Decoding
                //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //    //처리실패시 리턴
                //    if (!ErrRtn.ErrNum.Equals(0))
                //    {
                //        trans.Rollback();
                //        return strErrRtn;
                //    }
                //}

                ////금형치공구 BOM_Model 저장 정보가 있을 경우
                //if (dtDurableMatBOM_Model.Rows.Count > 0)
                //{

                //    //금형치공구 BOM_Model 정보저장매서드 실행
                //    strErrRtn = clsBOM_Model.mfSaveDurableMatBOM_Model(dtDurableMatBOM_Model, strUserIP, strUserID, sql.SqlCon, trans);

                //    //Decoding
                //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //    //처리결과가 실패시 롤백처리
                //    if (!ErrRtn.ErrNum.Equals(0))
                //    {
                //        trans.Rollback();
                //        return strErrRtn;
                //    }

                //}

                #endregion

                #region BOM_Equip 처리 2013-01-10 주석 (Lot별로 관리함.)
                ////금형치공구 정보를 저장 한 후 BOM정보를 처리한다.
                //DurableMatBOM_Equip clsBOM_Equip = new DurableMatBOM_Equip();

                ////금형치공구BOM_Model 삭제 매서드 실행
                //if (!strPlantCode.Equals(string.Empty))
                //{
                //    strErrRtn = clsBOM_Equip.mfDeleteDurableMatBOM_Equip(strPlantCode, strDurableMatCode, sql.SqlCon, trans);

                //    //Decoding
                //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //    //처리실패시 리턴
                //    if (!ErrRtn.ErrNum.Equals(0))
                //    {
                //        trans.Rollback();
                //        return strErrRtn;
                //    }
                //}

                ////금형치공구 BOM_Model 저장 정보가 있을 경우
                //if (dtDurableMatBOM_Equip.Rows.Count > 0)
                //{

                //    //금형치공구 BOM_Model 정보저장매서드 실행
                //    strErrRtn = clsBOM_Equip.mfSaveDurableMatBOM_Equip(dtDurableMatBOM_Equip, strUserIP, strUserID, sql.SqlCon, trans);

                //    //Decoding
                //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //    //처리결과가 실패시 롤백처리
                //    if (!ErrRtn.ErrNum.Equals(0))
                //    {
                //        trans.Rollback();
                //        return strErrRtn;
                //    }

                //}

                #endregion

                #region Package

                DurableMatBOM_Package clsBOM_Package = new DurableMatBOM_Package();

                if (!strPlantCode.Equals(string.Empty))
                {
                    //Package 삭제 매서드 실행
                    strErrRtn = clsBOM_Package.mfDeleteDurableMatBOM_Package(strPlantCode, strDurableMatCode, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //삭제처리실패시 롤백
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //금형치공구 BOM 저장 매서드 실행
                if (dtDurableMatBOM_Package.Rows.Count > 0)
                {
                    //금형치공구BOM정보 저장
                    strErrRtn = clsBOM_Package.mfSaveDurableMatBOM_Package(dtDurableMatBOM_Package, strUserIP, strUserID, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리 실패시 롤백 후 리턴
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }

                }

                #endregion

                #region Lot Stock Hist
                //---------------- 1. DurableLot  테이블 (MASDurableLot) -------------------//


                if (dtDurableLot.Rows.Count > 0)
                {
                    QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();

                    strErrRtn = clsDurableLot.mfSaveMASDurableLot(dtDurableLot, strUserIP, strUserID, sql, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. Durable 현 재고 테이블 (DMMDurableStock) -------------------//


                if (dtDurableStock.Rows.Count > 0)
                {
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();

                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 3. Durable 재고이력 테이블 (DMMDurableStockMoveHist) -------------------//


                if (dtDurableStockMoveHist.Rows.Count > 0)
                {
                    QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();

                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockMoveHist, strUserIP, strUserID, sql, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                #endregion

                #region 신규Lot정보의 설비투입 및 Flag

                if (dtDurableRepairH.Rows.Count > 0)
                {
                    QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();

                    //신규Lot정보의 설비투입매서드 실행
                    strErrRtn = clsDurableMatRepairH.mfSaveDurableMatRepairH_RepairLot(dtDurableRepairH, dtDurableRepairD, dtRepairLotHistory, strUserIP, strUserID, sql.SqlCon, trans);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 정보를 롤백시키고 리턴
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                #endregion

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();

                    if (strSendMDM.Equals("T"))
                    {
                        mfSaveDurableMat_MDM(dtDurableMat, strUserIP, strUserID, sql.SqlCon);
                    }
                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비연결종료
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        #endregion

        #endregion


        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();


            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("DurableMatCode", typeof(String));
                dtTable.Columns.Add("DurableMatName", typeof(String));
                dtTable.Columns.Add("DurableMatNameCh", typeof(String));
                dtTable.Columns.Add("DurableMatNameEn", typeof(String));
                dtTable.Columns.Add("DurableMatTypeCode", typeof(String));
                dtTable.Columns.Add("Spec", typeof(String));
                dtTable.Columns.Add("UsageLimitLower", typeof(String));
                dtTable.Columns.Add("UsageLimit", typeof(String));
                dtTable.Columns.Add("UsageLimitUpper", typeof(String));
                dtTable.Columns.Add("UsageLimitUnitName", typeof(String));
                dtTable.Columns.Add("UseFlag", typeof(String));
                dtTable.Columns.Add("ChangeUsage", typeof(String));
                dtTable.Columns.Add("CurUsage", typeof(String));
                dtTable.Columns.Add("CumUsage", typeof(String));
                dtTable.Columns.Add("LastInspectDate", typeof(String));
                dtTable.Columns.Add("LimitArrivalDate", typeof(String));



                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }


        /// <summary>
        /// Durable 현재재고 테이블 저장

        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDurableMat(DataTable dtDurableMat, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDurableMat.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["DurableMatCode"].ToString(), 20);


                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["DurableMatNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["DurableMatTypeCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParamter, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["Spec"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_numUsageLimitLower", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitLower"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numUsageLimit", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimit"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numUsageLimitUpper", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["UsageLimitUpper"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUsageLimitUnitName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableMat.Rows[i]["UsageLimitUnitName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_numChangeUsage", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["ChangeUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numCurUsage", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["CurUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numCumUsage", ParameterDirection.Input, SqlDbType.Decimal, dtDurableMat.Rows[i]["CumUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strLastInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["LastInspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLimitArrivalDate", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMat.Rows[i]["LimitArrivalDate"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurableMat", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        [AutoComplete]
        public DataTable mfReadDurableMatLot(string strPlantCode, string strDurableMatCode, string strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }

        /// <summary>
        /// 치공구 상세 Lot정보
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDurableMatCode"></param>
        /// <param name="strInventoryCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDurableMatLot_S(string strPlantCode, string strDurableMatCode, string strInventoryCode,string strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strInventoryCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_PSTS", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }

        /// <summary>
        /// Type Combo
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDurableMat_TypeCombo(string strPlantCode, string strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMat_TypeCombo", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }


    }

    #region 금형치공구BOM_Model

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableMatBOM_Model")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DurableMatBOM_Model : ServicedComponent
    {
        /// <summary>
        /// 컬럼정보
        /// </summary>
        /// <returns>컬럼정보 리턴</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtDurableMatBOM = new DataTable();
            try
            {
                dtDurableMatBOM.Columns.Add("PlantCode", typeof(string));
                dtDurableMatBOM.Columns.Add("DurableMatCode", typeof(string));
                dtDurableMatBOM.Columns.Add("ModelName", typeof(string));

                // 컬러정보 리턴
                return dtDurableMatBOM;

            }
            catch (Exception ex)
            {
                return dtDurableMatBOM;
                throw (ex);
            }
            finally
            {
                dtDurableMatBOM.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구BOM Model정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구BOM Model정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatBOM_Model(string strPlantCode, string strDurableMatCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtBOMModel = new DataTable();

            try
            {
                //DB연결
                sql.mfConnect();

                //파라미터 정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableMatCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //금형치공구BOM Model 조회 프로시저 실행
                dtBOMModel = sql.mfExecReadStoredProc(sql.SqlCon,"up_Select_MASDurableMatBOM_Model",dtParam);


                //금형치공구BOM Model정보
                return dtBOMModel;
            }
            catch (Exception ex)
            {
                return dtBOMModel;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtBOMModel.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구BOM_Model 정보 저장
        /// </summary>
        /// <param name="dtDurableMatBOM_Model">금형치공구BOM_Model 정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatBOM_Model(DataTable dtDurableMatBOM_Model, string strUserIP, string strUserID, SqlConnection Sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                for (int i = 0; i < dtDurableMatBOM_Model.Rows.Count; i++)
                {
                    //파라미터저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    
                    sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtDurableMatBOM_Model.Rows[i]["PlantCode"].ToString(),10);
                    sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar,dtDurableMatBOM_Model.Rows[i]["DurableMatCode"].ToString(),20);
                    sql.mfAddParamDataRow(dtParam,"@i_strModelName", ParameterDirection.Input, SqlDbType.NVarChar,dtDurableMatBOM_Model.Rows[i]["ModelName"].ToString(),50);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //금형치공구BOM_Model 저장프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_MASDurableMatBOM_Model", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        break;
                    }

                }

                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
            
        }



        /// <summary>
        /// 금형치공구BOM_Model삭제
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableMatBOM_Model(string strPlantCode, string strDurableMatCode,SqlConnection Sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //금형치공구BOM_Model삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Delete_MASDurableMatBOM_Model", dtParam);

                ////Decoding
                //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //if (!ErrRtn.ErrNum.Equals(0))
                //    return strErrRtn;

                //처리결과 리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);

            }
            finally
            {
                sql.Dispose();
            }
        }

    }

    #endregion

    #region 금형치공구BOM_Package

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableMatBOM_Package")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DurableMatBOM_Package : ServicedComponent
    {

        /// <summary>
        /// 데이터 컬럼셋
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataSet()
        {
            DataTable dtBOM_Package = new DataTable();

            try
            {
                dtBOM_Package.Columns.Add("PlantCode", typeof(string));
                dtBOM_Package.Columns.Add("DurableMatCode", typeof(string));
                dtBOM_Package.Columns.Add("Package", typeof(string));
               
                return dtBOM_Package;

            }
            catch (Exception ex)
            {
                return dtBOM_Package;
                throw (ex);
            }
            finally
            {
                dtBOM_Package.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구BOM_Package조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구BOM_Package정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatBOM_Package(string strPlantCode, string strDurableMatCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtBOM_Package = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode ,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //금형/치공구BOM_Package조회 프로시저 실행
                dtBOM_Package = sql.mfExecReadStoredProc(sql.SqlCon,"up_Select_MASDurableMatBOM_Package",dtParam);

                //금형치공구BOM_Package정보리턴
                return dtBOM_Package;

            }
            catch (Exception ex)
            {
                return dtBOM_Package;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtBOM_Package.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 BOM_Package저장
        /// </summary>
        /// <param name="dtDurableMatBOM_Package">금형치공구 BOM_Package 정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatBOM_Package(DataTable dtDurableMatBOM_Package, string strUserIP, string strUserID, SqlConnection Sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {

                for (int i = 0; i < dtDurableMatBOM_Package.Rows.Count; i++)
                {
                    //파라미터정보저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    
                    sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtDurableMatBOM_Package.Rows[i]["PlantCode"].ToString(),10);
                    sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar,dtDurableMatBOM_Package.Rows[i]["DurableMatCode"].ToString(),20);
                    sql.mfAddParamDataRow(dtParam,"@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar,dtDurableMatBOM_Package.Rows[i]["Package"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //금형/치공구BOM_Model 저장프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_MASDurableMatBOM_Package", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리실패시 for문종료
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        break;
                    }

                }

                //처리결과리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }



        /// <summary>
        /// 금형/치공구BOM_Model 삭제
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableMatBOM_Package(string strPlantCode, string strDurableMatCode,SqlConnection Sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //파라미터 정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableMatCode,20);
                
                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //금형/치공구BOM_Model 삭제프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Delete_MASDurableMatBOM_Package", dtParam);

                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }
    }


    #endregion

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableInventory")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    //DurableInventory 클래스
    public class DurableInventory : ServicedComponent
    {
        public DataTable mfReadDurableInventory(String strPlantCode, String strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableInventory", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPStock = new DataTable();


            try
            {
                dtSPStock.Columns.Add("PlantCode", typeof(String));
                dtSPStock.Columns.Add("DurableInventoryCode", typeof(String));
                dtSPStock.Columns.Add("DurableInventoryName", typeof(String));
                dtSPStock.Columns.Add("DurableInventoryNameCh", typeof(String));
                dtSPStock.Columns.Add("DurableInventoryNameEn", typeof(String));
                dtSPStock.Columns.Add("UseFlag", typeof(String));

                return dtSPStock;
            }
            catch (Exception ex)
            {
                return dtSPStock;
                throw (ex);
            }
            finally
            {
                dtSPStock.Dispose();
            }
        }

        /// <summary>
        /// 입력내용을 DB에 저장
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMASDurableInventory(DataTable dtGroup, String strUserIP, String strUserID)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;
                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtGroup.Rows.Count; i++)
                {

                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryName", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurableInventoryName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryNameCh", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurableInventoryNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryNameEn", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurableInventoryNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtGroup.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurableInventory", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 체크한 내용을 DB에서 삭제
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASDurableInventory(DataTable dtGroup, String strUserIP, String strUserID)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;
                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtGroup.Rows.Count; i++)
                {

                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDurableInventory", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if(ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구창고정보 콤보박스
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>창고정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableInventoryCombo(string strPlantCode, string strLang)
        {
            DataTable dtDurableInven = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터값저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtDurableInven = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableInventoryCombo", dtParam);
                //정보리턴
                return dtDurableInven;
            }
            catch (System.Exception ex)
            {
                return dtDurableInven;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableInven.Dispose();
            }

        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableSection")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    //DurableInventory 클래스
    public class DurableSection : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPStock = new DataTable();


            try
            {
                dtSPStock.Columns.Add("PlantCode", typeof(String));
                dtSPStock.Columns.Add("DurableInventoryCode", typeof(String));
                dtSPStock.Columns.Add("SectionNum", typeof(String));
                dtSPStock.Columns.Add("SectionName", typeof(String));
                dtSPStock.Columns.Add("SectionNameCh", typeof(String));
                dtSPStock.Columns.Add("SectionNameEn", typeof(String));
                dtSPStock.Columns.Add("ColumnNum", typeof(String));
                dtSPStock.Columns.Add("RowNum", typeof(String));
                dtSPStock.Columns.Add("EtcDesc", typeof(String));
                dtSPStock.Columns.Add("UseFlag", typeof(String));



                return dtSPStock;
            }
            catch (Exception ex)
            {
                return dtSPStock;
                throw (ex);
            }
            finally
            {
                dtSPStock.Dispose();
            }
        }

        public DataTable mfReadDurableSection(String strPlantCode, String strDurableInventoryCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableInventoryCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableSection", dtParam);

                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 섹션정보 삭제 후 저장
        /// </summary>
        /// <param name="dtDurableSection">SparePart 현 재고 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfDelete_Save_MASDurableSection
            (
                DataTable dtDurableSection
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //---------------- 1. 치공구 창고 섹션 테이블 삭제 (MASDurableSection) -------------------//

                if (dtDurableSection.Rows.Count > 0)
                {
                    strErrRtn = mfDeleteMASDurableSection(dtDurableSection, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. 치공구 창고 섹션 테이블 저장 (MASDurableSection) -------------------//
                if (dtDurableSection.Rows.Count > 0)
                {
                    strErrRtn = mfSaveMASDurableSection(dtDurableSection, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                trans.Commit();
                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 입력내용을 DB에 저장
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMASDurableSection(DataTable dtDurableSection, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {
            // 변수들...

            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dtDurableSection.Rows.Count; i++)
                {
                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableSection.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableSection.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSectionNum", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableSection.Rows[i]["SectionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSectionName", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableSection.Rows[i]["SectionName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSectionNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableSection.Rows[i]["SectionNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSectionNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableSection.Rows[i]["SectionNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strColumnNum", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableSection.Rows[i]["ColumnNum"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRowNum", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableSection.Rows[i]["RowNum"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDurableSection.Rows[i]["EtcDesc"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtDurableSection.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurableSection", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 체크한 내용을 DB에서 삭제 mfDeleteMASDurableSection
        /// </summary>
        /// <param name="dt"> 삭제할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASDurableSection(DataTable dtDurableSection, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {
            // 변수들...
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {

                for (int i = 0; i < dtDurableSection.Rows.Count; i++)
                {

                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableSection.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableSection.Rows[i]["DurableInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDurableSection", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurablePMGubun")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurablePMGubun : ServicedComponent
    {

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPStock = new DataTable();


            try
            {
                dtSPStock.Columns.Add("PlantCode", typeof(String));
                dtSPStock.Columns.Add("DurablePMGubunCode", typeof(String));
                dtSPStock.Columns.Add("DurablePMGubunName", typeof(String));
                dtSPStock.Columns.Add("DurablePMGubunNameCh", typeof(String));
                dtSPStock.Columns.Add("DurablePMGubunNameEn", typeof(String));
                dtSPStock.Columns.Add("UseFlag", typeof(String));

                return dtSPStock;
            }
            catch (Exception ex)
            {
                return dtSPStock;
                throw (ex);
            }
            finally
            {
                dtSPStock.Dispose();
            }
        }

        public DataTable mfReadDurablePMGubun(String strPlantCode, String strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurablePMGubun", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }

        public DataTable mfReadDurablePMGubun_Combo(String strPlantCode, String strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurablePMGubun_Combo", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }


        /// <summary>
        /// 입력내용을 DB에 저장
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMASDurablePMGubun(DataTable dtGroup, String strUserIP, String strUserID)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;
                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtGroup.Rows.Count; i++)
                {

                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMGubunCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMGubunName", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMGubunName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMGubunNameCh", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMGubunNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMGubunNameEn", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMGubunNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtGroup.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurablePMGubun", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 체크한 내용을 DB에서 삭제
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASDurablePMGubun(DataTable dtGroup, String strUserIP, String strUserID)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;
                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtGroup.Rows.Count; i++)
                {

                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMGubunCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDurablePMGubun", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurablePMType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurablePMType : ServicedComponent
    {

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPStock = new DataTable();


            try
            {
                dtSPStock.Columns.Add("PlantCode", typeof(String));
                dtSPStock.Columns.Add("DurablePMGubunCode", typeof(String));
                dtSPStock.Columns.Add("DurablePMTypeCode", typeof(String));
                dtSPStock.Columns.Add("DurablePMTypeName", typeof(String));
                dtSPStock.Columns.Add("DurablePMTypeNameCh", typeof(String));
                dtSPStock.Columns.Add("DurablePMTypeNameEn", typeof(String));
                dtSPStock.Columns.Add("UseFlag", typeof(String));

                return dtSPStock;
            }
            catch (Exception ex)
            {
                return dtSPStock;
                throw (ex);
            }
            finally
            {
                dtSPStock.Dispose();
            }
        }

        public DataTable mfReadDurablePMType(String strPlantCode, String strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurablePMType", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            } 
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }

        public DataTable mfReadDurablePMType_Combo(String strPlantCode, string strDurablePMGubunCode, String strLang)
        {
            DataTable dtDurableMat = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurablePMGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strDurablePMGubunCode, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurablePMType_Combo", dtParam);

                //정보리턴
                return dtDurableMat;
            }
            catch (Exception ex)
            {
                return dtDurableMat;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableMat.Dispose();
            }
        }

        /// <summary>
        /// 입력내용을 DB에 저장
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMASDurablePMType(DataTable dtGroup, String strUserIP, String strUserID)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;
                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtGroup.Rows.Count; i++)
                {

                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMGubunCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMTypeName", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMTypeNameCh", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMTypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMTypeNameEn", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMTypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtGroup.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurablePMType", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 체크한 내용을 DB에서 삭제
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASDurablePMType(DataTable dtGroup, String strUserIP, String strUserID)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;
                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtGroup.Rows.Count; i++)
                {

                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMGubunCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurablePMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["DurablePMTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDurablePMType", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableLot")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableLot : ServicedComponent
    {
        #region STS

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("DurableMatCode", typeof(String));
                dtTable.Columns.Add("LotNo", typeof(String));

                dtTable.Columns.Add("Package", typeof(String));
                dtTable.Columns.Add("EMC", typeof(String));

                dtTable.Columns.Add("GRDate", typeof(String));
                dtTable.Columns.Add("DiscardFlag", typeof(String));
                dtTable.Columns.Add("DiscardDate", typeof(String));
                dtTable.Columns.Add("DiscardChargeID", typeof(String));
                dtTable.Columns.Add("DiscardReason", typeof(String));
                dtTable.Columns.Add("ChangeUsage", typeof(String));
                dtTable.Columns.Add("CurUsage", typeof(String));
                dtTable.Columns.Add("CumUsage", typeof(String));
                dtTable.Columns.Add("LastInspectDate", typeof(String));
                dtTable.Columns.Add("LimitArrivalDate", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDelDatainfo()
        {
            DataTable dtTable = new DataTable();

            try
            {
                dtTable.Columns.Add("PlantCode", typeof(String));
                dtTable.Columns.Add("DurableMatCode", typeof(String));
                dtTable.Columns.Add("LotNo", typeof(String));
                dtTable.Columns.Add("Package", typeof(String));
                dtTable.Columns.Add("EMC", typeof(String));
                dtTable.Columns.Add("MoveGubunCode", typeof(String));

                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// Package에 해당하는 LotNo 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPakcage">Pakcage</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadMASDurableLot_Package(string strPlantCode, string strPakcage, string strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPakcage, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_Package", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// Durable 현재재고 테이블 저장
        /// </summary>
        /// <param name="dtDurableStock">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveMASDurableLot(DataTable dtMASDurableLot, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtMASDurableLot.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["LotNo"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["EMC"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParamter, "@i_strGRDate", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["GRDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardFlag", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["DiscardFlag"].ToString(), 1 );
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["DiscardDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardChargeID", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["DiscardChargeID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDiscardReason", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["DiscardReason"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_numChangeUsage", ParameterDirection.Input, SqlDbType.Decimal, dtMASDurableLot.Rows[i]["ChangeUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numCurUsage", ParameterDirection.Input, SqlDbType.Decimal, dtMASDurableLot.Rows[i]["CurUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_numCumUsage", ParameterDirection.Input, SqlDbType.Decimal, dtMASDurableLot.Rows[i]["CumUsage"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strLastInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["LastInspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLimitArrivalDate", ParameterDirection.Input, SqlDbType.VarChar, dtMASDurableLot.Rows[i]["LimitArrivalDate"].ToString(), 10);
                   
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurableLot", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 정기점검관리 대상 정보 조회(LotNo추가)

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadMASDurableMatLot_Detail(String strPlantCode, String strDurableMatCode, String strLotNo, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatLot_Detail", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            { 
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구 상세정보  조회 (Lot정보 상세조회)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구</returns>
        [AutoComplete]
        public DataTable mfReadDurableLot_Detail(String strPlantCode, String strDurableMatCode, String strLotNo, String strLang)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS(); 
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_Detail", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }

        /// <summary>
        /// 정기점검관리 대상 정보 조회(LotNo추가)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadMASDurableLot_Combo(String strPlantCode, String strDurableMatCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장

                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_Combo", dtParmter);
                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadTOOL_INFO_REQ(string strPlantCode, string strDurableLotNo)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strDurableLotNo, 40);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMat_MESIF", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }

        
        [AutoComplete]
        public DataTable mfReadMASDurableLot_WithPackage(string strPlantCode, string strDurableMatName, string strPackage, string strLang)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatName, 50);
                sql.mfAddParamDataRow(dtParmter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_WithPackage", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 Lot정보 삭제
        /// </summary>
        /// <param name="dtDurableLot">삭제할 정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteDurableLot(DataTable dtDurableLot,string strUserIP,string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDurableLot.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableLot.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableLot.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableLot.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtDurableLot.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, dtDurableLot.Rows[i]["EMC"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strMoveGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableLot.Rows[i]["MoveGubunCode"].ToString(), 3);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDurableLot", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();



                //결과 값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region PSTS

        [AutoComplete]
        public DataTable mfReadMASDurableLot_PSTS_frmMASZ0032_S(string strPlantCode, string strDurableMatName, string strPackage, string strLang)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableMatName, 50);
                sql.mfAddParamDataRow(dtParmter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_PSTS_frmMASZ0032_S", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadMASDurableLot_PSTS_frmMASZ0032_S(string strPlantCode, string strDurableTypeCode, string strLang)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableTypeCode, 5);
                
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_frmMASZ0032_S_Type", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }

        /// <summary>
        /// 마지막순번의 LotNo 검색
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASDurableLot_LastLot(string strPlantCode, string strDurableMatCode, string strLotNo, string strLang)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_LastLot", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }



        #endregion


    }

    ////[EventTrackingEnabled(true)]
    ////[JustInTimeActivation(true)]
    ////[ConstructionEnabled(true, Default = "None")]
    ////[Transaction(TransactionOption.Supported)]
    ////[ObjectPooling(true)]
    ////[Serializable]
    ////[System.EnterpriseServices.Description("PMResultType")]
    ////[ClassInterface(ClassInterfaceType.None)]
    ////[ComponentAccessControl(true), SecurityRole("QRPService", true)]

    ////public class PMResultType : ServicedComponent
    ////{

    ////    ///<summary>
    ////    ///컬럼 설정
    ////    ///</summary>
    ////    ///<return></return>
    ////    public DataTable mfSetDatainfo()
    ////    {
    ////        DataTable dtTable = new DataTable();


    ////        try
    ////        {
    ////            dtTable.Columns.Add("PlantCode", typeof(String));
    ////            dtTable.Columns.Add("PMResultCode", typeof(String));
    ////            dtTable.Columns.Add("PMResultName", typeof(String));
    ////            dtTable.Columns.Add("PMResultNameCh", typeof(String));
    ////            dtTable.Columns.Add("PMResultNameEn", typeof(String));
    ////            dtTable.Columns.Add("ChangeFlag", typeof(String));
    ////            dtTable.Columns.Add("RepairFlag", typeof(String));
    ////            dtTable.Columns.Add("UseFlag", typeof(String));
    ////            return dtTable;
    ////        }
    ////        catch (Exception ex)
    ////        {
    ////            return dtTable;
    ////            throw (ex);
    ////        }
    ////        finally
    ////        {
    ////        }
    ////    }

    ////    public DataTable mfReadPMResultType_Combo(String strPlantCode, String strLang)
    ////    {
    ////        DataTable dtDurableMat = new DataTable();
    ////        SQLS sql = new SQLS();

    ////        try
    ////        {
    ////            //디비연결
    ////            sql.mfConnect();

    ////            DataTable dtParam = sql.mfSetParamDataTable();

    ////            //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
    ////            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
    ////            sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

    ////            //프로시져 호출
    ////            dtDurableMat = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPMResultType_Combo", dtParam);

    ////            //정보리턴
    ////            return dtDurableMat;
    ////        }
    ////        catch (Exception ex)
    ////        {
    ////            return dtDurableMat;
    ////            throw (ex);
    ////        }
    ////        finally
    ////        {
    ////            //디비종료
    ////            sql.mfDisConnect();
    ////        }
    ////    }

    ////    public DataTable mfReadPMResultType_Detail(String strPlantCode, String strPMResultCode, String strLang)
    ////    {
    ////        DataTable dtTable = new DataTable();
    ////        SQLS sql = new SQLS();

    ////        try
    ////        {
    ////            //디비연결
    ////            sql.mfConnect();

    ////            DataTable dtParam = sql.mfSetParamDataTable();

    ////            //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
    ////            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
    ////            sql.mfAddParamDataRow(dtParam, "@i_strPMResultCode", ParameterDirection.Input, SqlDbType.VarChar, strPMResultCode, 5);
    ////            sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

    ////            //프로시져 호출
    ////            dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPMResultType_Detail", dtParam);

    ////            //정보리턴
    ////            return dtTable;
    ////        }
    ////        catch (Exception ex)
    ////        {
    ////            return dtTable;
    ////            throw (ex);
    ////        }
    ////        finally
    ////        {
    ////            //디비종료
    ////            sql.mfDisConnect();
    ////        }
    ////    }

    ////}


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableSpec")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DurableSpec : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("SpecCode", typeof(string));
                dtRtn.Columns.Add("SpecName", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수명관리 Spec 기준정보 조회 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASDurableSpec(string strPlantCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableSpec", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 수명관리 Spec 기준정보 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASDurableSpec(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["SpecCode"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecName", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["SpecName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDurableSpec", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수명관리 Spec 기준정보 삭제 메소드
        /// </summary>
        /// <param name="dtDelete">삭제정보 저장된 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteMASDurableSpec(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["SpecCode"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDurableSpec", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("LotNoSpecInfo")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class LotNoSpecInfo : ServicedComponent
    {
        public DataTable mfSetDataInfo_frmMASZ0032()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("DurableMatCode", typeof(string));
                dtRtn.Columns.Add("DurableMatName", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));

                dtRtn.Columns.Add("Package", typeof(string));
                dtRtn.Columns.Add("EMC", typeof(string));

                dtRtn.Columns.Add("SpecCode", typeof(string));
                dtRtn.Columns.Add("UsageLimitLower", typeof(decimal));
                dtRtn.Columns.Add("UsageLimit", typeof(decimal));
                dtRtn.Columns.Add("UsageLimitUpper", typeof(decimal));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구 수명관리 LotNo정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableName">금형치공구명</param>
        /// <param name="strPackage">패키지</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>LotNo</returns>
        [AutoComplete]
        public DataTable mfReadLotNoSpecInfo_LotNo(string strPlantCode, string strDurableName, string strPackage, string strEquipBOMChk, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtLotNo = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipBOMChk", ParameterDirection.Input, SqlDbType.Char, strEquipBOMChk, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtLotNo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_LotNo", dtParam);

                return dtLotNo;
            }
            catch (Exception ex)
            {
                return dtLotNo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtLotNo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 수명관리 LotNo정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableName">금형치공구명</param>
        /// <param name="strPackage">패키지</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>LotNo</returns>
        [AutoComplete]
        public DataTable mfReadLotNoSpecInfo_LotNo_PSTS(string strPlantCode, string strDurableName, string strPackage, string strEquipBOMChk, string strInventoryCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtLotNo = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strInventoryCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipBOMChk", ParameterDirection.Input, SqlDbType.Char, strEquipBOMChk, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtLotNo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_LotNo_PSTS", dtParam);

                return dtLotNo;
            }
            catch (Exception ex)
            {
                return dtLotNo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtLotNo.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 수명관리 LotNo정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableName">금형치공구명</param>
        /// <param name="strPackage">패키지</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>LotNo</returns>
        [AutoComplete]
        public DataTable mfReadLotNoSpecInfo_LotNo_PSTS(string strPlantCode, string strDurableMatTypeCode,
                                                        string strInventoryCode, string strCurUsageGubun, string struSearchUse ,string strLang, string strUserID)
        {
            SQLS sql = new SQLS();
            DataTable dtLotNo = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatTypeCode, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strInventoryCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCurUsageGubun", ParameterDirection.Input, SqlDbType.VarChar, strCurUsageGubun, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strstruSearchUse", ParameterDirection.Input, SqlDbType.VarChar, struSearchUse, 5);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtLotNo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_LotNo_Type", dtParam);

                return dtLotNo;
            }
            catch (Exception ex)
            {
                return dtLotNo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtLotNo.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구 수명관리 Limit체크
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableName">금형치공구명</param>
        /// <param name="strPackage">패키지</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>LimitLotNo정보</returns>
        [AutoComplete]
        public DataTable mfReadLotNoSpecInfo_LimitChk(string strPlantCode, string strDurableName, string strPackage, string strEquipBOMChk, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtLimit = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, strDurableName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipBOMChk", ParameterDirection.Input, SqlDbType.Char, strEquipBOMChk, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtLimit = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_LimitChk", dtParam);

                return dtLimit;
            }
            catch (Exception ex)
            {
                return dtLimit;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtLimit.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구 수명관리 Limit체크
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>LimitLotNo정보</returns>
        [AutoComplete]
        public DataTable mfReadLotNoSpecInfo_LimitChk(string strPlantCode, string strDurableMatTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtLimit = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatTypeCode, 5);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtLimit = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_LimitChk_Type", dtParam);

                return dtLimit;
            }
            catch (Exception ex)
            {
                return dtLimit;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtLimit.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구 수명관리 그리드더블클릭시 상세정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <returns>상세정보</returns>
        [AutoComplete]
        public DataTable mfReadLotNoSpecInfo_Detail(string strPlantCode, string strDurableMatCode, string strLotNo,string strPackage,string strEMC)
        {
            DataTable dtSpec = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, strEMC, 40);

                dtSpec = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_Detail", dtParam);

                return dtSpec;
            }
            catch (Exception ex)
            {
                return dtSpec;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtSpec.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구별 Spec 저장
        /// </summary>
        /// <param name="dtLotNoSpecInfoList">저장할 데이터테이블 정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveLotNoSpecInfo_frmMASZ0032(DataTable dtLotNoSpecInfoList_Save, DataTable dtLotNoSpecInfoList_Delete, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                if (dtLotNoSpecInfoList_Delete.Rows.Count > 0)
                {
                    strErrRtn = mfDeleteLotNoSpecInfo_frmMASZ0032(dtLotNoSpecInfoList_Delete, sql.SqlCon, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                ////strErrRtn = mfDeleteLotNoSpecInfo_frmMASZ0032_All(dtLotNoSpecInfoList_Save, sql.SqlCon, trans);
                ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    trans.Rollback();
                ////    return strErrRtn;
                ////}

                for (int i = 0; i < dtLotNoSpecInfoList_Save.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, dtLotNoSpecInfoList_Save.Rows[i]["DurableMatName"].ToString(), 50);

                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["SpecCode"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_dblUsageLimitLower", ParameterDirection.Input, SqlDbType.Decimal, dtLotNoSpecInfoList_Save.Rows[i]["UsageLimitLower"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblUsageLimit", ParameterDirection.Input, SqlDbType.Decimal, dtLotNoSpecInfoList_Save.Rows[i]["UsageLimit"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblUsageLimitUpper", ParameterDirection.Input, SqlDbType.Decimal, dtLotNoSpecInfoList_Save.Rows[i]["UsageLimitUpper"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASLotNoSpecInfo_frmMASZ0032", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구별 Spec 저장
        /// </summary>
        /// <param name="dtLotNoSpecInfoList">저장할 데이터테이블 정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveLotNoSpecInfo_frmMASZ0032(DataTable dtLotNoSpecInfoList_Save,
                                                      DataTable dtLotNoSpecInfoList_Delete,
                                                      DataTable dtLotNoSpecInfoList_Equip,
                                                      string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                if (dtLotNoSpecInfoList_Delete.Rows.Count > 0)
                {
                    strErrRtn = mfDeleteLotNoSpecInfo_frmMASZ0032(dtLotNoSpecInfoList_Delete, sql.SqlCon, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                for (int i = 0; i < dtLotNoSpecInfoList_Save.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatName", ParameterDirection.Input, SqlDbType.NVarChar, dtLotNoSpecInfoList_Save.Rows[i]["DurableMatName"].ToString(), 50);

                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["LotNo"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEMC", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["EMC"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParam, "@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList_Save.Rows[i]["SpecCode"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_dblUsageLimitLower", ParameterDirection.Input, SqlDbType.Decimal, dtLotNoSpecInfoList_Save.Rows[i]["UsageLimitLower"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblUsageLimit", ParameterDirection.Input, SqlDbType.Decimal, dtLotNoSpecInfoList_Save.Rows[i]["UsageLimit"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblUsageLimitUpper", ParameterDirection.Input, SqlDbType.Decimal, dtLotNoSpecInfoList_Save.Rows[i]["UsageLimitUpper"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASLotNoSpecInfo_frmMASZ0032", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        #region BOM_Equip 처리 2013-01-10 (Lot별로 관리함.)
                        //금형치공구 정보를 저장 한 후 BOM정보를 처리한다.
                        DurableMatBOM_Equip clsBOM_Equip = new DurableMatBOM_Equip();

                        //금형치공구BOM_Model 삭제 매서드 실행
                        if (!dtLotNoSpecInfoList_Save.Rows[i]["PlantCode"].ToString().Equals(string.Empty))
                        {
                            strErrRtn = clsBOM_Equip.mfDeleteDurableMatBOM_Equip(dtLotNoSpecInfoList_Save.Rows[i]["PlantCode"].ToString(), 
                                                                                    dtLotNoSpecInfoList_Save.Rows[i]["DurableMatCode"].ToString(), 
                                                                                    dtLotNoSpecInfoList_Save.Rows[i]["LotNo"].ToString(),
                                                                                    sql.SqlCon, trans);

                            //Decoding
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            //처리실패시 리턴
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }

                        //금형치공구 BOM_Model 저장 정보가 있을 경우
                        if (dtLotNoSpecInfoList_Equip.Rows.Count > 0)
                        {

                            //금형치공구 BOM_Model 정보저장매서드 실행
                            strErrRtn = clsBOM_Equip.mfSaveDurableMatBOM_Equip(dtLotNoSpecInfoList_Equip, strUserIP, strUserID, sql.SqlCon, trans);

                            //Decoding
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            //처리결과가 실패시 롤백처리
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }

                        }

                        #endregion
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구별 Spec 삭제
        /// </summary>
        /// <param name="dtLotNoSpecInfoList">삭제할 데이터테이블 정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteLotNoSpecInfo_frmMASZ0032(DataTable dtLotNoSpecInfoList, SqlConnection Sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {

                //sql.mfConnect();
                //SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtLotNoSpecInfoList.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList.Rows[i]["SpecCode"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Delete_MASLotNoSpecInfo_frmMASZ0032", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        //trans.Rollback();
                        break;
                    }
                }

                //if (ErrRtn.ErrNum.Equals(0))
                //trans.Commit();

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구별 Spec 삭제
        /// </summary>
        /// <param name="dtLotNoSpecInfoList">삭제할 데이터테이블 정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteLotNoSpecInfo_frmMASZ0032_All(DataTable dtLotNoSpecInfoList, SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {

                //sql.mfConnect();
                //SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                //for (int i = 0; i < dtLotNoSpecInfoList.Rows.Count; i++)
                //{
                if (dtLotNoSpecInfoList.Rows.Count > 0)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList.Rows[0]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtLotNoSpecInfoList.Rows[0]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASLotNoSpecInfo_All_frmMASZ0032", dtParam);
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_MASLotNoSpecInfo_All_frmMASZ0032", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        //trans.Rollback();
                    }
                }

                //if (ErrRtn.ErrNum.Equals(0))
                //    trans.Commit();

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                ////sql.mfDisConnect();
                ////sql.Dispose();
            }
        }

        /// <summary>
        /// MES 入库时确认DIE NO信息
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <param name="strProductQty"></param>
        /// <param name="dtSerialNo"></param>
        /// <returns></returns>
        public DataTable mfReadMOLD_LIFETYPE_REQ_RECEVE(string strPlantCode, string strEquipCode, string strProductQty, DataTable dtSerialNo)
        {
            // Return Datable 설정
            DataTable dtRtn = new DataTable();

            // Columns 설정
            dtRtn.Columns.Add("ReturnCode", typeof(string));
            dtRtn.Columns.Add("ReturnMessage", typeof(string));

            // Columns 기본값 설정
            DataRow drRow = dtRtn.NewRow();

            drRow["ReturnCode"] = string.Empty;
            drRow["ReturnMessage"] = string.Empty;

            dtRtn.Rows.Add(drRow);

            SQLS sql = new SQLS();

            try
            {


                // sql 연결
                sql.mfConnect();

                // 보내온 SerialNo 수 대로 루프를 돌며 처리한다.
                for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 공장

                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSerialNo.Rows[i]["SerialNo"].ToString(), 50); // SerialNo

                    sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSerialNo.Rows[i]["SpecNo"].ToString(), 50); // SpecNo

                    // SerialNo 수명처리 정보 조회 매서드 실행
                    DataTable dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableLot_RECEVE", dtParam);

                    if (dtResult.Rows.Count > 0)
                    {

                        // 정보가 있는경우 ReturnCode와 메세지 저장
                        dtRtn.Rows[0]["ReturnCode"] = dtResult.Rows[0]["ReturnCode"];
                        dtRtn.Rows[0]["ReturnMessage"] = dtResult.Rows[0]["ErrorMessage"];

                        // 처리된 정보 결과가 실패인경우 처리를 멈추고 ReturnData를 보내줌.
                        if (dtResult.Rows[0]["ReturnCode"].ToString().Equals("N"))
                            break;
                    }
                    else
                    {
                        // 정보가 없거나 조회실패했을 경우 정보없는 코드 + 메세지 저장 후 작업을 멈춤.
                        // QRP - 금형/치공구관리 - 금형치공구기준정보에서 없는 정보는 처리 할 수 없습니다.(SerialNo)
                        dtRtn.Rows[0]["ReturnCode"] = "N";
                        dtRtn.Rows[0]["ReturnMessage"] = " QRP - 金型/治工具管理-金型治工具基准情报里没有的信息是无法处理.(" + dtSerialNo.Rows[i]["SerialNo"].ToString().Trim() + ")";
                        break;
                    }
                }

                // Return 데이터 정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                // Return Error Info
                dtRtn.Rows[0]["ReturnCode"] = "N";
                dtRtn.Rows[0]["ReturnMessage"] = ex.Message;

                return dtRtn;
                throw (ex);

            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        /// <summary>
        /// MES装载时确认DIE NO和设备信息
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <param name="strProductQty"></param>
        /// <param name="dtSerialNo"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMOLD_LIFETYPE_REQ_KIT(string strPlantCode, string strEquipCode, string strProductQty, DataTable dtSerialNo)
        {
            // Return Datable 설정
            DataTable dtRtn = new DataTable();

            // Columns 설정
            dtRtn.Columns.Add("ReturnCode", typeof(string));
            dtRtn.Columns.Add("ReturnMessage", typeof(string));

            // Columns 기본값 설정
            DataRow drRow = dtRtn.NewRow();

            drRow["ReturnCode"] = string.Empty;
            drRow["ReturnMessage"] = string.Empty;

            dtRtn.Rows.Add(drRow);

            SQLS sql = new SQLS();

            try
            {


                // sql 연결
                sql.mfConnect();

                // 보내온 SerialNo 수 대로 루프를 돌며 처리한다.
                for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 공장

                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSerialNo.Rows[i]["SerialNo"].ToString(), 50); // SerialNo

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.NVarChar, strEquipCode, 50); // EquipCode

                    // SerialNo 수명처리 정보 조회 매서드 실행
                    DataTable dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatBOM_Equip_KIT", dtParam);

                    if (dtResult.Rows.Count > 0)
                    {

                        // 정보가 있는경우 ReturnCode와 메세지 저장
                        dtRtn.Rows[0]["ReturnCode"] = dtResult.Rows[0]["ReturnCode"];
                        dtRtn.Rows[0]["ReturnMessage"] = dtResult.Rows[0]["ErrorMessage"];

                        // 처리된 정보 결과가 실패인경우 처리를 멈추고 ReturnData를 보내줌.
                        if (dtResult.Rows[0]["ReturnCode"].ToString().Equals("N"))
                            break;
                    }
                    else
                    {
                        // 정보가 없거나 조회실패했을 경우 정보없는 코드 + 메세지 저장 후 작업을 멈춤.
                        // QRP - 금형/치공구관리 - 금형치공구기준정보에서 없는 정보는 처리 할 수 없습니다.(SerialNo)
                        dtRtn.Rows[0]["ReturnCode"] = "N";
                        dtRtn.Rows[0]["ReturnMessage"] = " QRP - 金型/治工具管理-金型治工具基准情报里没有的信息是无法处理.(" + dtSerialNo.Rows[i]["SerialNo"].ToString().Trim() + ")";
                        break;
                    }
                }

                // Return 데이터 정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                // Return Error Info
                dtRtn.Rows[0]["ReturnCode"] = "N";
                dtRtn.Rows[0]["ReturnMessage"] = ex.Message;

                return dtRtn;
                throw (ex);

            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        /// <summary>
        /// CONSUMABLE_JIG_REQ
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="dtSerialNo">치공구Lot List</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCONSUMABLE_JIG_REQ(string strPlantCode, string strSerialNo)
        {
            // Return Datable 설정
            DataTable dtRtn = new DataTable();

            // Columns 설정
            dtRtn.Columns.Add("ReturnCode", typeof(string));
            dtRtn.Columns.Add("ReturnMessage", typeof(string));

            // Columns 기본값 설정
            DataRow drRow = dtRtn.NewRow();

            drRow["ReturnCode"] = string.Empty;
            drRow["ReturnMessage"] = string.Empty;

            dtRtn.Rows.Add(drRow);

            SQLS sql = new SQLS();

            try
            {


                // sql 연결
                sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 공장

                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.NVarChar, strSerialNo, 50); // SerialNo

                    // SerialNo 수명처리 정보 조회 매서드 실행
                    DataTable dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_MesJIGChk", dtParam);

                    if (dtResult.Rows.Count > 0)
                    {

                        // 정보가 있는경우 ReturnCode와 메세지 저장
                        dtRtn.Rows[0]["ReturnCode"] = dtResult.Rows[0]["ReturnCode"];
                        dtRtn.Rows[0]["ReturnMessage"] = dtResult.Rows[0]["ErrorMessage"];

                    }
                    else
                    {
                        // 정보가 없거나 조회실패했을 경우 정보없는 코드 + 메세지 저장 후 작업을 멈춤.
                        // QRP - 금형/치공구관리 - 금형치공구기준정보에서 없는 정보는 처리 할 수 없습니다.(SerialNo)
                        dtRtn.Rows[0]["ReturnCode"] = "99";
                        dtRtn.Rows[0]["ReturnMessage"] = " (dtResult.Rows.Count = 0)QRP - 金型/治工具管理-金型治工具基准情报里没有的信息是无法处理.(" + strSerialNo + ")";
                    }

                // Return 데이터 정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                // Return Error Info
                dtRtn.Rows[0]["ReturnCode"] = "99";
                dtRtn.Rows[0]["ReturnMessage"] = ex.Message;

                return dtRtn;
                throw (ex);

            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// LOT_SOCKET_REQ
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strProductQty">제품수량</param>
        /// <param name="dtSerialNo">치공구Lot List</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadLOT_SOCKET_REQ_Tkin(string strPlantCode , string strProductQty, DataTable dtSerialNo)
        {
            // Return Datable 설정
            DataTable dtRtn = new DataTable();

            // Columns 설정
            dtRtn.Columns.Add("ReturnCode", typeof(string));
            dtRtn.Columns.Add("ReturnMessage", typeof(string));

            // Columns 기본값 설정
            DataRow drRow = dtRtn.NewRow();

            drRow["ReturnCode"] = string.Empty;
            drRow["ReturnMessage"] = string.Empty;

            dtRtn.Rows.Add(drRow);

            SQLS sql = new SQLS();

            try
            {


                // sql 연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 공장

                sql.mfAddParamDataRow(dtParam, "@i_dbProductQty", ParameterDirection.Input, SqlDbType.Decimal, strProductQty);  // 제품생산 수량

                // 보내온 SerialNo 수 대로 루프를 돌며 처리한다.
                string strSerialNoList = "";
                for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                {
                    if (dtSerialNo.Rows[i]["SerialType"].ToString() == "S-TKIT")
                    {
                        strSerialNoList = strSerialNoList + dtSerialNo.Rows[i]["SerialNo"].ToString() + ",";
                    }

                    strSerialNoList = strSerialNoList.Substring(0, strSerialNoList.Length - 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNoList", ParameterDirection.Input, SqlDbType.NVarChar, strSerialNoList, 1000); // SerialNo

                    // SerialNo 수명처리 정보 조회 매서드 실행
                    DataTable dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_MESSocketTKINChk", dtParam);

                    System.IO.StreamWriter log = new System.IO.StreamWriter("I:/" + "BL_mfReadLOT_SOCKET_REQ_Tkin.txt", true);

                    log.WriteLine("1--");

                    if (dtResult.Rows.Count > 0)
                    {

                        // 정보가 있는경우 ReturnCode와 메세지 저장
                        dtRtn.Rows[0]["ReturnCode"] = dtResult.Rows[0]["ReturnCode"];
                        dtRtn.Rows[0]["ReturnMessage"] = dtResult.Rows[0]["ErrorMessage"];

                        log.WriteLine(dtResult.Rows[0]["ReturnCode"]);
                        log.WriteLine(dtResult.Rows[0]["ErrorMessage"]);

                        // 처리된 정보 결과가 실패인경우 처리를 멈추고 ReturnData를 보내줌.
                        if (dtResult.Rows[0]["ReturnCode"].ToString().Equals("99")
                            || dtResult.Rows[0]["ReturnCode"].ToString().Equals("F")
                            || dtResult.Rows[0]["ReturnCode"].ToString().Equals("N"))
                            break;
                    }
                    else
                    {
                        // 정보가 없거나 조회실패했을 경우 정보없는 코드 + 메세지 저장 후 작업을 멈춤.
                        // QRP - 금형/치공구관리 - 금형치공구기준정보에서 없는 정보는 처리 할 수 없습니다.(SerialNo)
                        dtRtn.Rows[0]["ReturnCode"] = "99";
                        dtRtn.Rows[0]["ReturnMessage"] = " QRP - 金型/治工具管理-金型治工具基准情报里没有的信息是无法处理.(" + dtSerialNo.Rows[i]["SerialNo"].ToString().Trim() + ")";
                        break;
                    }
                }

                // Return 데이터 정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                // Return Error Info
                dtRtn.Rows[0]["ReturnCode"] = "99";
                dtRtn.Rows[0]["ReturnMessage"] = ex.Message;

                return dtRtn;
                throw (ex);

            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// OI Track In 시 LotNo(Serial No), 생산수량을 받아 LotNo 수명체크
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strProductQty">제품수량</param>
        /// <param name="dtSerialNo">치공구Lot List</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMOLD_LIFETYPE_REQ_Tkin(string strPlantCode, string strEquipCode, string strProductQty, DataTable dtSerialNo)
        {
            // Return Datable 설정
            DataTable dtRtn = new DataTable();

            // Columns 설정
            dtRtn.Columns.Add("ReturnCode", typeof(string));
            dtRtn.Columns.Add("ReturnMessage", typeof(string));

            // Columns 기본값 설정
            DataRow drRow = dtRtn.NewRow();

            drRow["ReturnCode"] = string.Empty;
            drRow["ReturnMessage"] = string.Empty;

            dtRtn.Rows.Add(drRow);

            SQLS sql = new SQLS();

            try
            {
                 

                // sql 연결
                sql.mfConnect();

                // 보내온 SerialNo 수 대로 루프를 돌며 처리한다.
                for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 공장

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode,20);  // 설비코드

                    sql.mfAddParamDataRow(dtParam, "@i_dbProductQty", ParameterDirection.Input, SqlDbType.Decimal, strProductQty);  // 제품생산 수량
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSerialNo.Rows[i]["SerialNo"].ToString(), 50); // SerialNo
                    
                    // SerialNo 수명처리 정보 조회 매서드 실행
                    DataTable dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_MESTKINChk", dtParam);

                    if (dtResult.Rows.Count > 0)
                    {
                        
                        // 정보가 있는경우 ReturnCode와 메세지 저장
                        dtRtn.Rows[0]["ReturnCode"] = dtResult.Rows[0]["ReturnCode"];
                        dtRtn.Rows[0]["ReturnMessage"] = dtResult.Rows[0]["ErrorMessage"];

                        // 처리된 정보 결과가 실패인경우 처리를 멈추고 ReturnData를 보내줌.
                        if (dtResult.Rows[0]["ReturnCode"].ToString().Equals("99") 
                            || dtResult.Rows[0]["ReturnCode"].ToString().Equals("F")
                            || dtResult.Rows[0]["ReturnCode"].ToString().Equals("N"))
                            break;
                    }
                    else
                    {
                        // 정보가 없거나 조회실패했을 경우 정보없는 코드 + 메세지 저장 후 작업을 멈춤.
                        // QRP - 금형/치공구관리 - 금형치공구기준정보에서 없는 정보는 처리 할 수 없습니다.(SerialNo)
                        dtRtn.Rows[0]["ReturnCode"] = "99";
                        dtRtn.Rows[0]["ReturnMessage"] = " QRP - 金型/治工具管理-金型治工具基准情报里没有的信息是无法处理.(" + dtSerialNo.Rows[i]["SerialNo"].ToString().Trim() + ")";
                        break;
                    }
                }

                // Return 데이터 정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                // Return Error Info
                dtRtn.Rows[0]["ReturnCode"] = "99";
                dtRtn.Rows[0]["ReturnMessage"] = ex.Message;

                return dtRtn;
                throw (ex);

            }
            finally
            { 
                dtRtn.Dispose();
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadMOLD_LIFETYPE_REQ_Tkin_EMC(string strPlantCode, string strEquipCode, string strProductQty, DataTable dtSerialNo, string strProductCode, string strOLD_EMC, string strNEW_EMC, string strOLD_PKG, string strNEW_PKG)
        {
            // Return Datable 설정
            DataTable dtRtn = new DataTable();

            // Columns 설정
            dtRtn.Columns.Add("ReturnCode", typeof(string));
            dtRtn.Columns.Add("ReturnMessage", typeof(string));

            // Columns 기본값 설정
            DataRow drRow = dtRtn.NewRow();

            drRow["ReturnCode"] = string.Empty;
            drRow["ReturnMessage"] = string.Empty;

            dtRtn.Rows.Add(drRow);

            SQLS sql = new SQLS();

            int SerialNoSum = dtSerialNo.Rows.Count;
            if (SerialNoSum > 0)
            {
                decimal dProductQty = Convert.ToDecimal(strProductQty) / SerialNoSum;
                strProductQty = Convert.ToString(dProductQty);
            }

            try
            {


                // sql 연결
                sql.mfConnect();

                // 보내온 SerialNo 수 대로 루프를 돌며 처리한다.
                for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 공장

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);  // 설비코드

                    sql.mfAddParamDataRow(dtParam, "@i_dbProductQty", ParameterDirection.Input, SqlDbType.Decimal, strProductQty);  // 제품생산 수량

                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSerialNo.Rows[i]["SerialNo"].ToString(), 50); // SerialNo

                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);

                    sql.mfAddParamDataRow(dtParam, "@i_strOLD_EMC", ParameterDirection.Input, SqlDbType.VarChar, strOLD_EMC, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strNEW_EMC", ParameterDirection.Input, SqlDbType.VarChar, strNEW_EMC, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strOLD_PKG", ParameterDirection.Input, SqlDbType.VarChar, strOLD_PKG, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strNEW_PKG", ParameterDirection.Input, SqlDbType.VarChar, strNEW_PKG, 30);

                    // SerialNo 수명처리 정보 조회 매서드 실행
                    DataTable dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLotNoSpecInfo_MESTKINChk_EMC", dtParam);

                    if (dtResult.Rows.Count > 0)
                    {

                        // 정보가 있는경우 ReturnCode와 메세지 저장
                        dtRtn.Rows[0]["ReturnCode"] = dtResult.Rows[0]["ReturnCode"];
                        dtRtn.Rows[0]["ReturnMessage"] = dtResult.Rows[0]["ErrorMessage"];

                        // 처리된 정보 결과가 실패인경우 처리를 멈추고 ReturnData를 보내줌.
                        if (dtResult.Rows[0]["ReturnCode"].ToString().Equals("99")
                            || dtResult.Rows[0]["ReturnCode"].ToString().Equals("F")
                            || dtResult.Rows[0]["ReturnCode"].ToString().Equals("N"))
                            break;
                    }
                    else
                    {
                        // 정보가 없거나 조회실패했을 경우 정보없는 코드 + 메세지 저장 후 작업을 멈춤.
                        // QRP - 금형/치공구관리 - 금형치공구기준정보에서 없는 정보는 처리 할 수 없습니다.(SerialNo)
                        dtRtn.Rows[0]["ReturnCode"] = "99";
                        dtRtn.Rows[0]["ReturnMessage"] = " QRP - 金型/治工具管理-金型治工具基准情报里没有的信息是无法处理.(" + dtSerialNo.Rows[i]["SerialNo"].ToString().Trim() + ")";
                        break;
                    }
                }

                // Return 데이터 정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                // Return Error Info
                dtRtn.Rows[0]["ReturnCode"] = "99";
                dtRtn.Rows[0]["ReturnMessage"] = ex.Message;

                return dtRtn;
                throw (ex);

            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
                sql.Dispose();
            }

        }


        /// <summary>
        /// OI Track Out 시 LotNo(Serial No), 생산수량을 받아 LotNo Shot수 증가
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strLotNo">작업생산LotNo</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strProductQty">생산수량</param>
        /// <param name="dtSerialNo">금형SerialNo</param>
        /// <param name="strEventName">EVENTNAME</param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMOLD_LIFETYPE_REQ_TkOut(string strPlantCode, string strEquipCode, string strLotNo,
                                                        string strProductCode,string strProductQty, 
                                                        string strEventName, DataTable dtSerialNo,
                                                        string strUserIP,string strUserID)
        {
            QRPDB.SQLS sql = new QRPDB.SQLS();
            QRPDB.TransErrRtn ErrRtn = new QRPDB.TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                // sql 연결
                sql.mfConnect();

                // Sqltransaction 설정
                System.Data.SqlClient.SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 보내온 SerialNo 수 대로 루프를 돌며 처리한다.
                for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 공장
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20); // 작업중인설비
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50); // 작업중인 LotNo
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20); // 제품코드
                    sql.mfAddParamDataRow(dtParam, "@i_dbProductQty", ParameterDirection.Input, SqlDbType.Decimal, strProductQty);  // 제품생산 수량
                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSerialNo.Rows[i]["SerialNo"].ToString(), 50); // SerialNo
                    sql.mfAddParamDataRow(dtParam, "@i_strEventName", ParameterDirection.Input, SqlDbType.VarChar, strEventName, 10);  // EventName

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20); // 사용자 ID
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15); // 사용자 IP

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.NVarChar, 4000);    // Return ErrorMessage 

                    // SerialNo 수명처리 정보 저장 매서드 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASLotNoSpecInfo_MESTKOUT", dtParam);

                    // 처리결과 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리결과 실패인경우 롤백처리 후 처리 종료
                    if (ErrRtn.ErrNum != 0)
                    {
                        dtParam.Dispose();
                        trans.Rollback();
                        break;
                    }
                    dtParam.Dispose();
                }

                // 모든 처리가 완료 되었을 경우 Commit 처리
                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                // Return 데이터 정보
                return strErrRtn;

            }
            catch (Exception ex)
            {
                // Return Error Info
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);

            }
            finally
            {
                // Sql 연결 종료
                sql.mfDisConnect();
                // Resource 해제
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveMOLD_LIFETYPE_REQ_TkOut_EMC(string strPlantCode, string strEquipCode, string strLotNo,
                                                        string strProductCode, string strProductQty,
                                                        string strEventName, DataTable dtSerialNo, string strNEW_EMC,string strNEW_PKG,
                                                        string strUserIP, string strUserID)
        {
            QRPDB.SQLS sql = new QRPDB.SQLS();
            QRPDB.TransErrRtn ErrRtn = new QRPDB.TransErrRtn();
            try
            {
                int SerialNoSum = dtSerialNo.Rows.Count;
                decimal dProductQty = Convert.ToDecimal(strProductQty) / SerialNoSum;
                strProductQty = Convert.ToString(dProductQty);

                string strErrRtn = string.Empty;

                // sql 연결
                sql.mfConnect();

                // Sqltransaction 설정
                System.Data.SqlClient.SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 보내온 SerialNo 수 대로 루프를 돌며 처리한다.
                for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 공장
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20); // 작업중인설비
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50); // 작업중인 LotNo
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20); // 제품코드
                    sql.mfAddParamDataRow(dtParam, "@i_dbProductQty", ParameterDirection.Input, SqlDbType.Decimal, strProductQty);  // 제품생산 수량
                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSerialNo.Rows[i]["SerialNo"].ToString(), 50); // SerialNo
                    sql.mfAddParamDataRow(dtParam, "@i_strEventName", ParameterDirection.Input, SqlDbType.VarChar, strEventName, 10);  // EventName

                    sql.mfAddParamDataRow(dtParam, "@i_strNEW_EMC", ParameterDirection.Input, SqlDbType.VarChar, strNEW_EMC, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strNEW_PKG", ParameterDirection.Input, SqlDbType.VarChar, strNEW_PKG, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20); // 사용자 ID
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15); // 사용자 IP

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.NVarChar, 4000);    // Return ErrorMessage 

                    // SerialNo 수명처리 정보 저장 매서드 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASLotNoSpecInfo_MESTKOUT_EMC", dtParam);

                    // 처리결과 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리결과 실패인경우 롤백처리 후 처리 종료
                    if (ErrRtn.ErrNum != 0)
                    {
                        dtParam.Dispose();
                        trans.Rollback();
                        break;
                    }
                    dtParam.Dispose();
                }

                // 모든 처리가 완료 되었을 경우 Commit 처리
                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                // Return 데이터 정보
                return strErrRtn;

            }
            catch (Exception ex)
            {
                // Return Error Info
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);

            }
            finally
            {
                // Sql 연결 종료
                sql.mfDisConnect();
                // Resource 해제
                sql.Dispose();
            }

        }

    }




    #region 금형치공구BOM_Equip

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DurableMatBOM_Equip")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DurableMatBOM_Equip : ServicedComponent
    {
        /// <summary>
        /// 컬럼정보
        /// </summary>
        /// <returns>컬럼정보 리턴</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtDurableMatBOM = new DataTable();
            try
            {
                dtDurableMatBOM.Columns.Add("PlantCode", typeof(string));
                dtDurableMatBOM.Columns.Add("DurableMatCode", typeof(string));
                dtDurableMatBOM.Columns.Add("EquipCode", typeof(string));
                dtDurableMatBOM.Columns.Add("LotNo", typeof(string));

                // 컬러정보 리턴
                return dtDurableMatBOM;

            }
            catch (Exception ex)
            {
                return dtDurableMatBOM;
                throw (ex);
            }
            finally
            {
                dtDurableMatBOM.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구BOM Equip정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">금형치공구코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>금형치공구BOM Equip정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableMatBOM_Equip(string strPlantCode, string strDurableMatCode, string strLotNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtBOMModel = new DataTable();

            try
            {
                //DB연결
                sql.mfConnect();

                //파라미터 정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //금형치공구BOM Equip 조회 프로시저 실행
                dtBOMModel = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDurableMatBOM_Equip", dtParam);


                //금형치공구BOM Equip정보
                return dtBOMModel;
            }
            catch (Exception ex)
            {
                return dtBOMModel;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtBOMModel.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 금형치공구BOM_Equip 정보 저장
        /// </summary>
        /// <param name="dtDurableMatBOM_Model">금형치공구BOM_Equip 정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveDurableMatBOM_Equip(DataTable dtDurableMatBOM_Equip, string strUserIP, string strUserID, SqlConnection Sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                for (int i = 0; i < dtDurableMatBOM_Equip.Rows.Count; i++)
                {
                    //파라미터저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatBOM_Equip.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatBOM_Equip.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatBOM_Equip.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDurableMatBOM_Equip.Rows[i]["LotNo"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //금형치공구BOM_Model 저장프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_MASDurableMatBOM_Equip", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        break;
                    }

                }

                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }

        }

        /// <summary>
        /// 금형치공구BOM_Equip삭제
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="Sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteDurableMatBOM_Equip(string strPlantCode, 
                                                string strDurableMatCode, string strLotNo,
                                                SqlConnection Sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //금형치공구BOM_Model삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Delete_MASDurableMatBOM_Equip", dtParam);

                ////Decoding
                //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //if (!ErrRtn.ErrNum.Equals(0))
                //    return strErrRtn;

                //처리결과 리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);

            }
            finally
            {
                sql.Dispose();
            }
        }

    }

    #endregion


}
