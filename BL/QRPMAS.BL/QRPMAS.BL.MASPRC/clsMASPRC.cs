﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 마스터 관리 기준정보                                  */
/* 프로그램ID   : clsMASPRC.cs                                          */
/* 프로그램명   : 공정관리기준정보                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-21                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// 추가
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]
namespace QRPMAS.BL.MASPRC
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Plant")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Plant : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        /// <summary>
        /// 생성자

        /// </summary>
        public Plant()
        {
        }

        /// <summary>
        /// Debug모드용 생성자

        /// </summary>
        /// <param name="strDBConn"></param>
        public Plant(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 공장정보 조회용 Method
        /// </summary>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASPlant(String strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPlant", dtParam);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPlant", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 공장 콤보박스용 Method
        /// </summary>
        /// <param name="strLang"> Language </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadPlantForCombo(String strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPlantCombo", dtParam);
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASPlantCombo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Process")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Process : ServicedComponent
    {
        /// <summary>
        /// 공정 조회용 Method
        /// </summary>
        /// <param name="strSearchPlant"> PlantCode </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASProcess(String strPlantCode, String strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcess", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 공정 콤보박스용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strLang"> Language </param>
        /// <returns> DB에서 넘어온 정보 DataTable </returns>
        [AutoComplete]
        public DataTable mfReadProcessForCombo(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcessCombo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 공정그룹(DETAILPROCESSOPERATIONTYPE) 조회용 Method
        /// </summary>
        /// <param name="strSearchPlant"> PlantCode </param>
        /// <param name="strLang"> Language </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASProcessGroup(String strPlantCode, String strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcess_Group", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 공장/공정그룹에 따른 공정콤보박스 설정 메소드

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessGroupCode">공정그룹</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProcessCombo_ProcessGroup(string strPlantCode, string strProcessGroupCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_StrProcessGroupCode", ParameterDirection.Input, SqlDbType.NVarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcessCombo_WithProcessGroup", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 예상 공정 콤보박스용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strAriseProcess"> 발생공정코드 </param>
        /// <param name="strLang"> Language </param>
        /// <returns> DB에서 넘어온 정보 DataTable </returns>
        [AutoComplete]
        public DataTable mfReadExpectProcessForCombo(String strPlantCode, String strAriseProcessCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strAriseProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASExpectProcessCombo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 공정명 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strProcessCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadProcessName(String strPlantCode, String strProcessCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcessName", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 데이터 테이블 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ProcessCode", typeof(string));
                dtRtn.Columns.Add("TrackInFlag", typeof(char));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정정보 TrackInFlag 저장 메소드

        /// </summary>
        /// <param name="dtSave">저장정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strPserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveProcess_TrackIn(DataTable dtSave, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strTrackInFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["TrackInFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASProcess_TrackIn", dtParam);

                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadProcessDetailProcessOperationType(string strPlantCode)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcess_DetailProcessOperationType", dtParam);
                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadProcessWithDetailProcessOperationType(string strPlantCode, string DetailProcessOperationType, string strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.VarChar, DetailProcessOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcess_WithDetailProcessOperationType", dtParam);
                return dt;
            }
            catch (System.Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 해당 공정에 속하는 설비가 맞는지 Check하는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProcEquipGroup_Check(string strPlantCode, string strProcessCode, string strEquipCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcEquipGroup_Check", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공정정보 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProcess_Detail(string strPlantCode, string strProcessCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcess_Detail", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Line")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Line : ServicedComponent
    {
        /// <summary>
        /// 라인정보 조회용 Method
        /// </summary>
        /// <param name="strSearchPlant"> PlantCode </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASLine(String strPlantCode, String strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLine", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]       //,20, 1048576)]  //enable, minPoolSize, maxPoolSize
    [Serializable]
    [System.EnterpriseServices.Description("LineProc")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class LineProc : ServicedComponent
    {
        /// <summary>
        /// 라인 공정순서정보 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> PlantCode </param>
        /// <param name="strLineCode"> LineCode </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASLineProc(String strPlantCode, String strLineCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLineCode", ParameterDirection.Input, SqlDbType.VarChar, strLineCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASLineProc", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ReasonCode")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ReasonCode : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReasonCodeType", typeof(string));
                dtRtn.Columns.Add("ReasonCode", typeof(string));
                dtRtn.Columns.Add("MaterialAbnormaProclFlag", typeof(char));
                dtRtn.Columns.Add("MaterialSpecialFlag", typeof(char));
                dtRtn.Columns.Add("INSProcFlag", typeof(char));
                dtRtn.Columns.Add("QCNFlag", typeof(char));
                dtRtn.Columns.Add("CCSFlag", typeof(char));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 설비정지유형 팝업창
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strSuperCode">슈퍼코드</param>
        /// <returns>설비정지유형 팝업창정보</returns>
        [AutoComplete]
        public DataTable mfReadResonCode_EquipStopType(string strPlantCode, string strEquipCode, string strSuperCode)
        {
            DataTable dtStopType = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strSuperResonCode", ParameterDirection.Input, SqlDbType.VarChar,strSuperCode,40);

                //설비정지유형팝업창 조회 프로시저 실행
                dtStopType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASReasonCode_EquipStopType", dtParam);

                //설비정지유형팝업창정보
                return dtStopType;
            }
            catch (Exception ex)
            {
                return dtStopType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtStopType.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// DownCode 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>DownCode</returns>
        [AutoComplete]
        public DataTable mfReadReasonCode_DownCode(string strPlantCode, string strLang)
        {
            DataTable dtReasonCode = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtReasonCode = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASReasonCode_DownCode", dtParam);

                //DownCode 정보
                return dtReasonCode;
            }
            catch (Exception ex)
            {
                return dtReasonCode;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtReasonCode.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// DownCode 상세정보
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strSuperReason">대분류</param>
        /// <returns>DownCode 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadReasonCode_DownCodeInfo(string strPlantCode, string strEquipCode, string strSuperReason)
        
        {
            DataTable dtReason = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strSuperReason", ParameterDirection.Input, SqlDbType.VarChar, strSuperReason,3);

                dtReason = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASReasonCode_DownCodeInfo", dtParam);

                return dtReason;
            }
            catch (Exception ex)
            {
                return dtReason;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtReason.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 사유코드 조회용 Method
        /// </summary>
        /// <param name="strSearchPlant"> PlantCode </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASReasonCode(String strPlantCode, String strHoldCode, String strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strHoldCode", ParameterDirection.Input, SqlDbType.VarChar, strHoldCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASReasonCode", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        ////ReasonTypeCode 콤보박스 조회용 BL Method
        [AutoComplete]
        public DataTable mfReadMASReasonTypeCode_Combo(String strPlantCode)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASReasonCodeType_Combo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// ReasonCode 저장 메소드 : 원자재이상발생(공정이상) 적용, 원자재특채, 공정검사, QCN, CCS HoldCode 
        /// </summary>
        /// <param name="dtSave">저장정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strPserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASReasonCode(DataTable dtSave, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReasonCodeType", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReasonCodeType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strReasonCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReasonCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialAbnormaProclFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialAbnormaProclFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialSpecialFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialSpecialFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strINSProcFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["INSProcFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["QCNFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["CCSFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASReasonCode", dtParam);

                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if(ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES I/F시 화면별 홀드코드 가져오는 메소드

        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFormName">화면명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASReasonCode_MESHoldCode(string strPlantCode, string strFormName)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFormName", ParameterDirection.Input, SqlDbType.VarChar, strFormName, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASReasonCode_MESHoldCode", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }
}
