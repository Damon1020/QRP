﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : clsMASEQU.cs                                          */
/* 프로그램명   : 설비관리기준정보                                      */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPMAS.BL.MASEQU
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Area")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Area : ServicedComponent
    {
        /// <summary>
        /// Area정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>Area정보</returns>
        [AutoComplete]
        public DataTable mfReadArea(String strPlantCode, String strLang)
        {
            DataTable dtArea = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtArea = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASArea", dtParam);

                //정보리턴
                return dtArea;
            }
            catch (Exception ex)
            {
                return dtArea;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtArea.Dispose();
            }
        }
        /// <summary>
        /// Area콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>Area정보</returns>
        [AutoComplete]
        public DataTable mfReadAreaCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtArea = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtArea = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASAreaCombo", dtParam);
                //정보리턴
                return dtArea;
            }
            catch (Exception ex)
            {
                return dtArea;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtArea.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Station")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Station : ServicedComponent
    {
        /// <summary>
        /// Station정보조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>Station정보</returns>
        [AutoComplete]
        public DataTable mfReadStation(String strPlantCode, String strLang)
        {
            DataTable dtStation = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtPramter = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtPramter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtPramter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtStation = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASStation", dtPramter);

                //정보리턴
                return dtStation;
            }
            catch (Exception ex)
            {
                return dtStation;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtStation.Dispose();
            }
        }
        /// <summary>
        /// Station 정보 콤보박스
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>Station정보</returns>
        [AutoComplete]
        public DataTable mfReadStationCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtStation = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저호출
                dtStation = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASStationCombo", dtParamter);
                //정보리턴
                return dtStation;
            }
            catch (Exception ex)
            {
                return dtStation;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtStation.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipLocation")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class EquipLocation : ServicedComponent
    {
        /// <summary>
        /// 위치정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>위치정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipLocation(String strPlantCode, String strLang)
        {
            DataTable dtLocation = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //프로시저 파라미터의 값을 datatabel에 넣기
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtLocation = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipLocation", dtParamter);

                //정보리턴
                return dtLocation;
            }
            catch (Exception ex)
            {
                return dtLocation;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtLocation.Dispose();
            }
        }

        /// <summary>
        /// 위치콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>위치정보</returns>
        [AutoComplete]
        public DataTable mfReadLocationCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtLocation = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtLocation = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipLocCombo", dtParam);
                //정보리턴
                return dtLocation;
            }
            catch (Exception ex)
            {
                return dtLocation;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtLocation.Dispose();
            }
        }

       /// <summary>
       /// 위치정보 콤보
       /// </summary>
       /// <param name="strPlantCode">공장</param>
       /// <param name="strStationCode">Station코드</param>
       /// <param name="strLang">사용언어</param>
       /// <returns>위치정보</returns>
        [AutoComplete]
        public DataTable mfReadLocation_Combo(string strPlantCode,string strStationCode ,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtLocation = new DataTable();

            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtLocation = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipLoc_Combo", dtParam);
                //정보리턴
                return dtLocation;
            }
            catch (Exception ex)
            {
                return dtLocation;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtLocation.Dispose();
            }
        }
        
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipProcGubun")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class EquipProcGubun : ServicedComponent
    {
        /// <summary>
        /// 설비공정구분정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비공정구분정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipProcGubun(String strPlantCode, String strLang)
        {
            DataTable dtProcGubun = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비호출
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //디비파라미터에 넣을 값을 테이블에 지정
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtProcGubun = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipProcGubun", dtParamter);

                //정보리턴
                return dtProcGubun;
            }
            catch (Exception ex)
            {
                return dtProcGubun;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtProcGubun.Dispose();
            }
        }

        /// <summary>
        /// 설비공정구분콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비공정구분정보</returns>
        [AutoComplete]
        public DataTable mfReadProGubunCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtProcGubun = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtProcGubun = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPorcGubunCombo", dtParam);
                //정보리턴
                return dtProcGubun;
            }
            catch (Exception ex)
            {
                return dtProcGubun;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtProcGubun.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class EquipType : ServicedComponent
    {
        /// <summary>
        /// 설비유형정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비유형정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipType(String strPlantCode, String strLang)
        {
            DataTable dtEquipType = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비호출
                sql.mfConnect();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 datatable에 넣기
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquipType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipType", dtParameter);

                //리턴정보
                return dtEquipType;
            }
            catch (Exception ex)
            {
                return dtEquipType;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipType.Dispose();
            }
        }

        /// <summary>
        /// 설비유형 콤보 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipTypeCombo(String strPlantCode, String strLang)
        {
            DataTable dtEquipType = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtEquipType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipTypeCombo", dtParam);

                return dtEquipType;
            }
            catch (Exception ex)
            {
                return dtEquipType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipType.Dispose();
            }
        }
    }

    #region 설비그룹

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipGroup")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipGroup : ServicedComponent
    {
        /// <summary>
        /// 점검그룹 설비리스트컬럼
        /// </summary>
        /// <returns>컬럼설정한 빈테이블</returns>
        [AutoComplete]
        public DataTable mfSetDateInfo()
        {
            DataTable dtColumns = new DataTable();
            try
            {

                dtColumns.Columns.Add("PlantCode", typeof(String));
                dtColumns.Columns.Add("EquipGroupCode", typeof(String));
                dtColumns.Columns.Add("EquipGroupName", typeof(String));
                dtColumns.Columns.Add("EquipGroupNameCh", typeof(String));
                dtColumns.Columns.Add("EquipGroupNameEn", typeof(String));
                dtColumns.Columns.Add("EquipImageFile", typeof(String));
                dtColumns.Columns.Add("UseFlag", typeof(String));

                //정보리턴
                return dtColumns;

            }
            catch (Exception ex)
            {
                return dtColumns;
                throw (ex);
            }
            finally
            {
                dtColumns.Dispose();
            }
        }

        /// <summary>
        /// 설비점검그룹콤보박스
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검그룹콤보박스 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipGroupCombo(string strPlantCode, string strLang)
        {
            DataTable dtEquipGroup = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtEquipGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroupCombo", dtParamter);

                //정보리턴
                return dtEquipGroup;
            }
            catch (Exception ex)
            {
                return dtEquipGroup;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipGroup.Dispose();
            }
        }

        /// <summary>
        /// 설비점검그룹콤보박스
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검그룹콤보박스 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipGroupCombo_Info(string strPlantCode, string strLang)
        {
            DataTable dtEquipGroup = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtEquipGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroupCombo_Info", dtParamter);

                //정보리턴
                return dtEquipGroup;
            }
            catch (Exception ex)
            {
                return dtEquipGroup;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipGroup.Dispose();
            }
        }

        /// <summary>
        /// 설비유형코드에 해당하는 설비그룹코드 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="sttEquipTypeCode">설비유형코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비유형별 설비그룹콤보박스 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipTypeGroupCombo(string strPlantCode, string strEquipTypeCode, string strLang)
        {
            DataTable dtEquipGroup = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 설정
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipTypeCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 실행
                dtEquipGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipTypeGroupCombo", dtParamter);

                //정보리턴
                return dtEquipGroup;
            }
            catch(Exception ex)
            {
                return dtEquipGroup;
                throw (ex);
            }
            finally 
            { 
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipGroup.Dispose();
            }
        }

        /// <summary>
        /// 공장, 공정코드로 설비 콤보박스용 데이터 조회하는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable mfReadEquipGroupCombo_Process(string strPlantCode, string strProcessCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtparam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtparam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtparam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtparam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProcEquipGroup_Process", dtparam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
                sql.Dispose();
            }
        }



        #region Select

        /// <summary>
        /// 설비점검그룹조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비그룹정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipGroup(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipGroup = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저호출
                dtEquipGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroup", dtParam);
                //정보리턴
                return dtEquipGroup;
            }
            catch (Exception ex)
            {
                return dtEquipGroup;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipGroup.Dispose();
            }
        }

        /// <summary>
        /// 점검그룹 설비리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검그룹 설비리스트 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipGroupList(string strPlantCode, string strEquipGroupCode, string strLang)
        {
            DataTable dtGroupList = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtGroupList = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroupList", dtParamter);
                //정보리턴
                return dtGroupList;
            }
            catch (Exception ex)
            {
                return dtGroupList;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtGroupList.Dispose();
            }
        }

        /// <summary>
        /// 점검그룹 설비리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검그룹 설비리스트 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipGroupList_PSTS(string strPlantCode, string strEquipGroupCode, string strLang)
        {
            DataTable dtGroupList = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtGroupList = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroupList_PSTS", dtParamter);
                //정보리턴
                return dtGroupList;
            }
            catch (Exception ex)
            {
                return dtGroupList;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtGroupList.Dispose();
            }
        }

        /// <summary>
        /// 설비정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipGroupList_PM(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, string strEquipGroupCode,
                                           string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroup_PM", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 설비그룹 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipGroup_Info(string strPlantCode, string strEquipGroupCode, string strLang)
        {
            DataTable dtGroupList = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtGroupList = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroup_Info", dtParamter);
                //정보리턴
                return dtGroupList;
            }
            catch (Exception ex)
            {
                return dtGroupList;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtGroupList.Dispose();
            }
        }

        /// <summary>
        /// 점검그룹 설비리스트 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strEquipProcGubunCode">공정그룹코드</param>
        /// <param name="strStationCode">StationCode</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검그룹설비리스트정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipGroupList(string strPlantCode, string strEquipGroupCode,string strStationCode,string strEquipLocCode,string strEquipTypeCode, string strLang)
        {
            DataTable dtGroupList = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipTypeCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtGroupList = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroup_Detail", dtParamter);
                //정보리턴
                return dtGroupList;
            }
            catch (Exception ex)
            {
                return dtGroupList;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtGroupList.Dispose();
            }
        }

        #endregion

        #region Save,Delete

        /// <summary>
        /// 점검그룹 저장
        /// </summary>
        /// <param name="dtGroupList">저장값이 담아있는DateTabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipGroup(DataTable dtGroupList, string strUserID, string strUserIP, DataTable dtSaveEquip, DataTable dtDeleteEquip, DataTable dtSaveGroupP, DataTable dtDeleteGroupP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtGroupList.Rows.Count; i++)
                {
                    //Transaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtPameter = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtPameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtPameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroupList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroupList.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupName", ParameterDirection.Input, SqlDbType.NVarChar, dtGroupList.Rows[i]["EquipGroupName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtGroupList.Rows[i]["EquipGroupNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtGroupList.Rows[i]["EquipGroupNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipImageFile", ParameterDirection.Input, SqlDbType.NVarChar, dtGroupList.Rows[i]["EquipImageFile"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPameter, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtGroupList.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtPameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtPameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtPameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipGroup", dtPameter);
                    //결과값확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {

                        //성공시 검사유형 처리
                        //설비리스트 삭제
                        if (dtDeleteEquip.Rows.Count > 0)
                        {
                            MASEQU.Equip clsEqu = new Equip();
                            strErrRtn = clsEqu.mfDeleteEquipList(dtDeleteEquip, strUserID, strUserIP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //정비사리스트삭제
                        if (dtDeleteGroupP.Rows.Count > 0)
                        {
                            MASEQU.EquipGroupP clsGroupP = new EquipGroupP();
                            strErrRtn = clsGroupP.mfDeleteEquipGroupP(dtDeleteGroupP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //설비리스트 저장
                        if (dtSaveEquip.Rows.Count > 0)
                        {
                            MASEQU.Equip clsEqu = new Equip();
                            strErrRtn = clsEqu.mfSaveEquipList(dtSaveEquip, strUserID, strUserIP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //정비사리스트 저장
                        if (dtSaveGroupP.Rows.Count > 0)
                        {
                            MASEQU.EquipGroupP clsGroupP = new EquipGroupP();
                            strErrRtn = clsGroupP.mfSaveEquipGroupP(dtSaveGroupP, strUserID, strUserIP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        trans.Commit();
                    }
                }
                //결과값리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 점검그룹 저장
        /// </summary>
        /// <param name="dtGroupList">저장값이 담아있는DateTabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipGroup_PSTS(DataTable dtGroupList, string strUserID, string strUserIP, DataTable dtSaveEquip, DataTable dtDeleteEquip, DataTable dtSaveGroupP, DataTable dtDeleteGroupP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtGroupList.Rows.Count; i++)
                {
                    //Transaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtPameter = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtPameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtPameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroupList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroupList.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupName", ParameterDirection.Input, SqlDbType.NVarChar, dtGroupList.Rows[i]["EquipGroupName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtGroupList.Rows[i]["EquipGroupNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtGroupList.Rows[i]["EquipGroupNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipImageFile", ParameterDirection.Input, SqlDbType.NVarChar, dtGroupList.Rows[i]["EquipImageFile"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPameter, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtGroupList.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtPameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtPameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtPameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipGroup", dtPameter);
                    //결과값확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {

                        //성공시 검사유형 처리
                        //설비리스트 삭제
                        if (dtDeleteEquip.Rows.Count > 0)
                        {
                            MASEQU.Equip clsEqu = new Equip();
                            strErrRtn = clsEqu.mfDeleteEquipList(dtDeleteEquip, strUserID, strUserIP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //정비사리스트삭제
                        if (dtDeleteGroupP.Rows.Count > 0)
                        {
                            MASEQU.EquipGroupP clsGroupP = new EquipGroupP();
                            strErrRtn = clsGroupP.mfDeleteEquipGroupP(dtDeleteGroupP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //설비리스트 저장
                        if (dtSaveEquip.Rows.Count > 0)
                        {
                            MASEQU.Equip clsEqu = new Equip();
                            strErrRtn = clsEqu.mfSaveEquipList_PSTS(dtSaveEquip, strUserID, strUserIP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //정비사리스트 저장
                        if (dtSaveGroupP.Rows.Count > 0)
                        {
                            MASEQU.EquipGroupP clsGroupP = new EquipGroupP();
                            strErrRtn = clsGroupP.mfSaveEquipGroupP(dtSaveGroupP, strUserID, strUserIP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        trans.Commit();
                    }
                }
                //결과값리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 점검그룹 이미지명 저장
        /// </summary>
        /// <param name="dtGroupList">저장값이 담아있는DateTabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipGroupImageFile(string strPlantCode, string strEquipGroupCode, string strEquipImageFile, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                DataTable dtPameter = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtPameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtPameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtPameter, "@i_strEquipImageFile", ParameterDirection.Input, SqlDbType.NVarChar, strEquipImageFile, 100);
                sql.mfAddParamDataRow(dtPameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtPameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtPameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipGroupImageFile", dtPameter);
                //결과값확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                //결과값리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 점검그룹 삭제
        /// </summary>
        /// <param name="dtEquip">정보가들어있는 Datatabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipGroup(DataTable dtEquipGroup, string strUserID, string strUserIP, DataTable dtEquip, DataTable dtGroupP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtEquipGroup.Rows.Count; i++)
                {
                    //Transaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipGroup.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipGroup.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipGroup", dtParam);
                    //결과값확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        if (dtEquip.Rows.Count > 0)
                        {
                            MASEQU.Equip clsEqu = new Equip();
                            strErrRtn = clsEqu.mfDeleteEquipList(dtEquip, strUserID, strUserIP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }

                        }

                        if (dtGroupP.Rows.Count > 0)
                        {
                            MASEQU.EquipGroupP clsGroupP = new EquipGroupP();
                            strErrRtn = clsGroupP.mfDeleteEquipGroupP(dtGroupP, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        trans.Commit();
                    }
                }
                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion
    }

    #endregion

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipGroupP")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipGroupP : ServicedComponent
    {
        /// <summary>
        /// 필요한 컬럼 추가
        /// </summary>
        /// <returns>컬럼만있는 Datatabel</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtColumns = new DataTable();
            try
            {

                dtColumns.Columns.Add("PlantCode", typeof(String));
                dtColumns.Columns.Add("EquipGroupCode", typeof(String));
                dtColumns.Columns.Add("UserID", typeof(String));

                return dtColumns;
            }
            catch (Exception ex)
            {
                return dtColumns;
                throw (ex);
            }
            finally
            {
                dtColumns.Dispose();
            }
        }

        /// <summary>
        /// 점검그룹 정비사리스트 조회
        /// </summary>
        /// <param name="strPlnatcode">공장코드</param>
        /// <param name="strEquipGroupCode">그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>정비사리스트</returns>
        [AutoComplete]
        public DataTable mfReadEquipGroupP(string strPlnatcode, string strEquipGroupCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipGroupP = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlnatcode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저호출
                dtEquipGroupP = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroupP", dtParam);
                //정보리턴
                return dtEquipGroupP;
            }
            catch (Exception ex)
            {
                return dtEquipGroupP;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipGroupP.Dispose();
            }
        }

        /// <summary>
        /// 점검그룹 정비사리스트 저장
        /// </summary>
        /// <param name="dtEquipGroupP">저장할데이터</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipGroupP(DataTable dtEquipGroupP, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                //sql.mfConnect();
                //SqlTransaction trans;

                for (int i = 0; i < dtEquipGroupP.Rows.Count; i++)
                {
                    //Transaction 시작
                    //trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParm = sql.mfSetParamDataTable();
                    //파라미터 저장
                    sql.mfAddParamDataRow(dtParm, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParm, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipGroupP.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParm, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipGroupP.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParm, "@i_strUser", ParameterDirection.Input, SqlDbType.VarChar, dtEquipGroupP.Rows[i]["UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParm, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParm, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParm, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipGroupP", dtParm);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    //    trans.Rollback();
                    //else
                    //    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 점검그룹 정비사리스트 삭제
        /// </summary>
        /// <param name="dtEquipGroupP">정보가들어있는데이터</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipGroupP(DataTable dtEquipGroupP, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dtEquipGroupP.Rows.Count; i++)
                {
                    //Transaction시작
                    //trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipGroupP.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipGroupP.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipGroupP.Rows[i]["UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipGroupP", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    //    trans.Rollback();
                    //else
                    //    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Equip")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Equip : ServicedComponent
    {
        /// <summary>
        /// 설비정보 저장삭제를위한 컬럼추가
        /// </summary>
        /// <returns>컬럼을 설정한 Datatabel</returns>
        public DataTable mfEquipDataSet()
        {
            DataTable dtEquip = new DataTable();
            try
            {
                dtEquip.Columns.Add("EquipGroupCode", typeof(string));
                dtEquip.Columns.Add("PlantCode", typeof(string));
                dtEquip.Columns.Add("EquipCode", typeof(string));
                //dtEquip.Columns.Add("EquipName", typeof(string));
                //dtEquip.Columns.Add("EquipNameCh", typeof(string));
                //dtEquip.Columns.Add("EquipNameEn", typeof(string));
                //dtEquip.Columns.Add("AreaCode", typeof(string));
                //dtEquip.Columns.Add("StationCode", typeof(string));
                //dtEquip.Columns.Add("EquipLocCode", typeof(string));
                //dtEquip.Columns.Add("EquipProcGubunCode", typeof(string));
                //dtEquip.Columns.Add("EquipTypeCode", typeof(string));
                //dtEquip.Columns.Add("SuperEquipCode", typeof(string));
                //dtEquip.Columns.Add("ModelName", typeof(string));
                //dtEquip.Columns.Add("SerialNo", typeof(string));
                //dtEquip.Columns.Add("EquipGroupCode", typeof(string));
                //dtEquip.Columns.Add("GRDate", typeof(string));
                //dtEquip.Columns.Add("MakeYear", typeof(string));
                //dtEquip.Columns.Add("EquipLevelCode", typeof(string));
                //dtEquip.Columns.Add("VendorCode", typeof(string));
                dtEquip.Columns.Add("PMWeekDay", typeof(string));
                dtEquip.Columns.Add("PMMonthWeek", typeof(int));
                dtEquip.Columns.Add("PMMonthDay", typeof(string));
                //------------- 2012-08-24 -------------------------//
                dtEquip.Columns.Add("PMQuarterDay", typeof(string));  
                dtEquip.Columns.Add("PMQuarterWeek", typeof(string)); 
                dtEquip.Columns.Add("PMQuarterMonth", typeof(string));
                dtEquip.Columns.Add("PMHalfDay", typeof(string));     
                dtEquip.Columns.Add("PMHalfWeek", typeof(string));    
                dtEquip.Columns.Add("PMHalfMonth", typeof(string));   
                //--------------------------------------------------//
                dtEquip.Columns.Add("PMYearMonth", typeof(int));
                dtEquip.Columns.Add("PMYearWeek", typeof(int));
                dtEquip.Columns.Add("PMYearDay", typeof(string));
                
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                dtEquip.Dispose();
            }
        }



        /// <summary>
        /// 설비리스트 ->예방점검정보(일괄)
        /// </summary>
        /// <param name="strPlanYear">계획년도</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <param name="strStationCode">Station코드</param>
        /// <param name="strProcGubunCode">공정구분코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비리스트 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipListBatch(string strPlanYear, string strPlantCode, string strAreaCode, string strStationCode, string strProcGubunCode, string strLang)
        {
            DataTable dtEquipList = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlanYear", ParameterDirection.Input, SqlDbType.Char, strPlanYear, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저 실행
                dtEquipList = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipListBatch", dtParam);

                //정보리턴
                return dtEquipList;
            }
            catch (Exception ex)
            {
                return dtEquipList;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipList.Dispose();
            }
        }

        /// <summary>
        /// 설비명조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비명</returns>
        [AutoComplete]
        public DataTable mfReadEquipName(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtEquipName = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtEquipName = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipName", dtParam);

                return dtEquipName;
            }
            catch (Exception ex)
            {
                return dtEquipName;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipName.Dispose();
            }
        }

        /// <summary>
        /// 설비명조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비명</returns>
        [AutoComplete]
        public DataTable mfReadEquipProcessGroup(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtEquipName = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtEquipName = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipProcessGroup", dtParam);

                return dtEquipName;
            }
            catch (Exception ex)
            {
                return dtEquipName;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipName.Dispose();
            }
        }

        /// <summary>
        /// 설비명조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">Station코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_Combo(string strPlantCode, string strStationCode, string strLang)
        {
            DataTable dtEquipName = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저실행
                dtEquipName = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_Combo", dtParam);

                return dtEquipName;
            }
            catch (Exception ex)
            {
                return dtEquipName;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipName.Dispose();
            }
        }

        #region ComboBox

        /// <summary>
        ///  ProcessGroupCode ComboBox  (설비대분류 - PM제외여부, 사용여부등등 해제)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <returns>ProcessGroupCode정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_ProcCombo(string strPlantCode, string strStationCode, string strEquipLocCode)
        {
            SQLS sql = new SQLS();
            DataTable dtProcess = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);

                dtProcess = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_ProcGroup", dtParam);

                return dtProcess;

            }
            catch (Exception ex)
            {
                return dtProcess;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtProcess.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// ProcessGroupCode ComboBox
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <returns>ProcessGroupCode정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_ProcessCombo(string strPlantCode, string strStationCode, string strEquipLocCode)
        {
            SQLS sql = new SQLS();
            DataTable dtProcess = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);

                dtProcess = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_ProcessGroup", dtParam);

                return dtProcess;

            }
            catch (Exception ex)
            {
                return dtProcess;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtProcess.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검그룹 ->설비정보 그리드리스트
        /// </summary>
        /// <param name="dtEquip">디비로보내줄 정보</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipListCombo(string PlantCode, string AreaCode, string StationCode, string EquipLocCode, string EquipProcGubunCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipList = new DataTable();
            try
            {
                //디비시작
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, PlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, AreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, StationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, EquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, EquipProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquipList = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroupEquipInfo", dtParam);

                //정보리턴
                return dtEquipList;

            }
            catch (Exception ex)
            {
                return dtEquipList;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipList.Dispose();
            }
        }

        /// <summary>
        /// 공장코드 기준 설비 목록 조회 (콤보박스용)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipForPlantCombo(string strPlantCode, string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipForPlantCombo", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 설비모델콤보박스조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>모델정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipModelCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtModelName = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //설비모델정보(콤보)조회 프로시저 실행
                dtModelName = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipModelCombo", dtParam);

                //모델 정보
                return dtModelName;
            }
            catch (Exception ex)
            {
                return dtModelName;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtModelName.Dispose();
            }
        }

        /// <summary>
        /// 설비모델조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>모델정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipModel_Station(string strPlantCode, string strStationCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtModelName = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //설비모델정보(콤보)조회 프로시저 실행
                dtModelName = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipModel_Station", dtParam);

                //모델 정보
                return dtModelName;
            }
            catch (Exception ex)
            {
                return dtModelName;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtModelName.Dispose();
            }
        }


        /// <summary>
        /// EquipLargeTypeCode콤보박스 (설비 중분류)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">공정분류</param>
        /// <returns>설비유형정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_EquipLargeType(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipLargeType = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar,strStationCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar,strEquipLocCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar,strProcessGroup,40);

                //EquipLargeTypeCode콤보박스 (설비 중분류) 프로시저실행
                dtEquipLargeType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_EquipLargeTypeCombo", dtParam);

                //설비중분류
                return dtEquipLargeType;
            }
            catch (Exception ex)
            {
                return dtEquipLargeType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipLargeType.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// EquipLargeTypeCode콤보박스 (설비 중분류 - PM제외여부, 사용여부등등 해제)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">공정분류</param>
        /// <returns>설비유형정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_LargeType(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipLargeType = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);

                //EquipLargeTypeCode콤보박스 (설비 중분류) 프로시저실행
                dtEquipLargeType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_LargeTypeCombo", dtParam);

                //설비중분류
                return dtEquipLargeType;
            }
            catch (Exception ex)
            {
                return dtEquipLargeType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipLargeType.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// EquipGroup콤보박스 (설비 소분류)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">공정분류</param>
        /// <param name="strEquipLargeTypeCode">설비유형</param>
        /// <returns>설비그룹정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_EquipGroupCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipGroup = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //EquipGroup콤보박스 (설비 소분류) 조회 프로시저 실행
                dtEquipGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_EquipGroupCombo", dtParam);

                //EquipGroup콤보정보
                return dtEquipGroup;
            }
            catch (Exception ex)
            {
                return dtEquipGroup;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipGroup.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// EquipGroup콤보박스 (설비 소분류- PM제외여부, 사용여부등등 해제)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">공정분류</param>
        /// <param name="strEquipLargeTypeCode">설비유형</param>
        /// <returns>설비그룹정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_GroupCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipGroup = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //EquipGroup콤보박스 (설비 소분류) 조회 프로시저 실행
                dtEquipGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_GroupCombo", dtParam);

                //EquipGroup콤보정보
                return dtEquipGroup;
            }
            catch (Exception ex)
            {
                return dtEquipGroup;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipGroup.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 위치별 설비유형정보
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비유형정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_EquipTypeCombo(string strPlantCode, string strEquipLocCode ,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipType = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //Station별 설비유형정보조회 프로시저 실행
                dtEquipType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_EquipTypeCode", dtParam);

                //설비유형정보
                return dtEquipType;
            }
            catch (Exception ex)
            {
                return dtEquipType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipType.Dispose();
                sql.Dispose();
            }
        }
        #endregion


        /// <summary>
        /// 설비정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAreaCode">Area</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipInfo(string strPlantCode, string strAreaCode, string strStationCode, string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 설비정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquip_Info(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, string strEquipGroupCode,
                                           string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_EquipInfo", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 설비정보Popup 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAreaCode">Area</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPopup(string strPlantCode, string strAreaCode, string strStationCode, string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPopup", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 설비정보Popup 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAreaCode">Area</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPopup_EquipProcGubun(string strPlantCode, string strEquipProcGubunCode, string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPopup_EquipProcGubun", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 설비정보Popup 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strAreaCode">Area</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPopup_2(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, string strEquipGroupCode,
                                           string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPopup_2", dtParamter);

                //설비정보 리턴
                return dtEquip;
            } 
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// DiscardFlag 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        public DataTable mfReadEquipDiscard(String strPlantCode, String strEquipCode, String strLang)
        {
            DataTable dtEquip = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPopupDiscard", dtParam);

                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보 설비리스트저장
        /// </summary>
        /// <param name="dtEquipList">저장할 정보</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <returns>처리결과 값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipList(DataTable dtEquipList, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dtEquipList.Rows.Count; i++)
                {
                    //Transaction시작
                    //trans = sql.SqlCon.BeginTransaction();

                    DataTable dtPameter = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtPameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPameter, "@i_strPMWeekDay", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PMWeekDay"].ToString(), 3);
                    sql.mfAddParamDataRow(dtPameter, "@i_intPMMonthWeek", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMMonthWeek"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_strPMMonthDay", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PMMonthDay"].ToString(), 3);
                    sql.mfAddParamDataRow(dtPameter, "@i_intPMYearMonth", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMYearMonth"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_intPMYearWeek", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMYearWeek"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_strPMYearDay", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PMYearDay"].ToString(), 3);

                    sql.mfAddParamDataRow(dtPameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtPameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtPameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipList", dtPameter);
                    //결과값확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                        //trans.Rollback();
                    }
                    //else
                    //{
                    //    trans.Commit();
                    //}
                }
                //결과값리턴
                return strErrRtn;
            }   
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
            }
        }


        /// <summary>
        /// 설비점검정보 설비리스트저장
        /// </summary>
        /// <param name="dtEquipList">저장할 정보</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <returns>처리결과 값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipList_PSTS(DataTable dtEquipList, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dtEquipList.Rows.Count; i++)
                {
                    //Transaction시작
                    //trans = sql.SqlCon.BeginTransaction();

                    DataTable dtPameter = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtPameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPameter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPameter, "@i_strPMWeekDay", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PMWeekDay"].ToString(), 3);
                    sql.mfAddParamDataRow(dtPameter, "@i_intPMMonthWeek", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMMonthWeek"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_strPMMonthDay", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PMMonthDay"].ToString(), 3);

                    sql.mfAddParamDataRow(dtPameter, "@i_intPMQuarterMonth", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMQuarterMonth"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_intPMQuarterWeek", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMQuarterWeek"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_strPMQuarterDay", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PMQuarterDay"].ToString(), 3);

                    sql.mfAddParamDataRow(dtPameter, "@i_intPMHalfMonth", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMHalfMonth"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_intPMHalfWeek", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMHalfWeek"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_strPMHalfDay", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PMHalfDay"].ToString(), 3);

                    sql.mfAddParamDataRow(dtPameter, "@i_intPMYearMonth", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMYearMonth"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_intPMYearWeek", ParameterDirection.Input, SqlDbType.Int, dtEquipList.Rows[i]["PMYearWeek"].ToString());
                    sql.mfAddParamDataRow(dtPameter, "@i_strPMYearDay", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PMYearDay"].ToString(), 3);

                    sql.mfAddParamDataRow(dtPameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtPameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtPameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipList_PSTS", dtPameter);
                    //결과값확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                        //trans.Rollback();
                    }
                    //else
                    //{
                    //    trans.Commit();
                    //}
                }
                //결과값리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
            }
        }


        /// <summary>
        /// 설비구성품정보 저장(금형치공구, SparePart) : 테이블 일괄 삭제 후 저장
        /// </summary>
        /// <param name="dtEquipDurableBOM">금형 치공구 데이터테이블</param>
        /// <param name="dtEquipDurableBOM">SparePart 데이터테이블</param>
        /// <param name="dtDurableStock">금형치공구 재고 테이블</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveEquipBOM
            (
                DataTable dtEquipDurableBOM
                , DataTable dtEquipSPBOM
                , DataTable dtDurableStock
                , string strPlantCode
                , string strEquipCode
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();


                //---------------- 1. 금형/치공구 BOM 정보 삭제 후 저장 -------------------//
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new EquipDurableBOM();

                // 삭제
                strErrRtn = clsEquipDurableBOM.mfDeleteEquipDurableBOM(strPlantCode, strEquipCode, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 저장
                if (dtEquipDurableBOM.Rows.Count > 0)
                {

                    strErrRtn = clsEquipDurableBOM.mfSaveEquipDurableBOM(dtEquipDurableBOM, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. SparePart BOM 정보 삭제 후 저장 -------------------//
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new EquipSPBOM();

                // 삭제
                strErrRtn = clsEquipSPBOM.mfDeleteEquipSPBOM(strPlantCode, strEquipCode, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 저장
                if (dtEquipSPBOM.Rows.Count > 0)
                {
                    strErrRtn = clsEquipSPBOM.mfSaveEquipSPBOM(dtEquipSPBOM, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 3. Durable 현 재고 테이블 (DMMDurableStock) -------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();

                if (dtDurableStock.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }


                trans.Commit();
                return strErrRtn;

            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비구성품정보 저장(금형치공구, SparePart) : 테이블 일괄 삭제 후 저장
        /// </summary>
        /// <param name="dtEquipDurableBOM">금형 치공구 데이터테이블</param>
        /// <param name="dtEquipDurableBOM">SparePart 데이터테이블</param>
        /// <param name="dtDurableStock">금형치공구 재고 테이블</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns> 
        [AutoComplete(false)]
        public String mfSaveEquipBOM
            (
                DataTable dtEquipDurableBOM
                , DataTable dtEquipSPBOM
                , DataTable dtDurableStock
                , string strPlantCode
                , string strEquipCode
                , DataTable dtDurableStockHist
                , string strUserIP
                , string strUserID
            )
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();


                //---------------- 1. 금형/치공구 BOM 정보 삭제 후 저장 -------------------//
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new EquipDurableBOM();

                // 삭제
                strErrRtn = clsEquipDurableBOM.mfDeleteEquipDurableBOM(strPlantCode, strEquipCode, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 저장
                if (dtEquipDurableBOM.Rows.Count > 0)
                {

                    strErrRtn = clsEquipDurableBOM.mfSaveEquipDurableBOM(dtEquipDurableBOM, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }


                //---------------- 2. SparePart BOM 정보 삭제 후 저장 -------------------//
                QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new EquipSPBOM();

                // 삭제
                strErrRtn = clsEquipSPBOM.mfDeleteEquipSPBOM(strPlantCode, strEquipCode, strUserIP, strUserID, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 저장
                if (dtEquipSPBOM.Rows.Count > 0)
                {
                    strErrRtn = clsEquipSPBOM.mfSaveEquipSPBOM(dtEquipSPBOM, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //---------------- 3. Durable 현 재고 테이블 (DMMDurableStock) -------------------//
                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();

                if (dtDurableStock.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStock.mfSaveDurableStock(dtDurableStock, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                //-------------- 4. Durable 재고이력 테이블(DMMDurableStockMoveHist) ------------//
                QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();

                if (dtDurableStockHist.Rows.Count > 0)
                {
                    strErrRtn = clsDurableStockMoveHist.mfSaveDurableStockMoveHist(dtDurableStockHist, strUserIP, strUserID, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }


                trans.Commit();
                return strErrRtn;

            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비점검정보 설비리스트삭제
        /// </summary>
        /// <param name="dtEquipList">삭제할 정보</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <returns>처리결과 값</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipList(DataTable dtEquipList, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dtEquipList.Rows.Count; i++)
                {
                    //Transaction시작
                    //trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipList.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipList", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    //    trans.Rollback();
                    //else
                    //    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 설비정보 Detail
        /// </summary>
        /// <param name="strPlantCode">공장번호</param>
        /// <param name="strEqiupCode">설비번호</param>
        /// <param name="strLang">언어</param>
        /// <returns>DataTable</returns>
        [AutoComplete]
        public DataTable mfReadEquipInfoDetail(string strPlantCode, string strEqiupCode, string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEqiupCode, 20);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_Detail", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 설비관리대장조회 헤더
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAreaCode">AreaCode</param>
        /// <param name="strStationCode">StationCode</param>
        /// <param name="strEquipProcGubunCode">공정구분코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strEquipTypeCode">설비유형</param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipMerge(string strPlantCode, string strAreaCode, string strStationCode, string strEquipProcGubunCode, string strEquipGroupCode
                                            , string strEquipLocCode, string strEquipTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipMerge = new DataTable();

            try
            {
                sql.mfConnect();

                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipProcGubunCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //설비관리대장조회 헤더프로시저 실행
                dtEquipMerge = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipMerge", dtParam);

                //설비정보 리턴
                return dtEquipMerge;
            }
            catch (Exception ex)
            {
                return dtEquipMerge;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipMerge.Dispose();
            }
        }

        /// <summary>
        /// 설비관리대장조회 상세
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>상세조회정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipMergeInfo(string strPlantCode, string strEquipCode, string strFromDate, string strToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipMergeInfo = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtEquipMergeInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipMergeInfo", dtParam);

                //상세조회정보 리턴
                return dtEquipMergeInfo;
            }
            catch (Exception ex)
            {
                return dtEquipMergeInfo;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipMergeInfo.Dispose();
            }
        }

        /// <summary>
        /// 설비 코드로 설비유형에 해당하는 설비코드들 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASEquip_EquipType(string strPlantCode, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquip = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_EquipType", dtParam);

                //상세조회정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

        /// <summary>
        /// 공장, Area, 설비코드로 공정그룹 콤보박스용 데이터 조회 하는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strAreaCode">Area코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipArea_WithDetailProcessOperationType(string strPlantCode, string strEquipCode, string strAreaCode)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_AreaWithDetailProcessOperationType", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비 검,교정 이력조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>이력정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_VerityMoveHist(string strPlantCode, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMoveHist = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);   //공장
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);   // 설비
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);              // 사용언어

                //설비이력 검,교정 완료정보 조회프로시저실행
                dtMoveHist = sql.mfExecReadStoredProc(sql.SqlCon,"up_Select_MASEquip_VerityMoveHist",dtParam);

                return dtMoveHist;
            }
            catch (Exception ex)
            {
                return dtMoveHist;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtMoveHist.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 폐기 승인 된 설비 MDM 에 송신
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strDOCNO">공장||설비로된 코드 (그룹웨어경우)</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveEquipDiscard_MDM(string strPlantCode,string strEquipCode,string strDOCNO)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strDOCNO", ParameterDirection.Input, SqlDbType.VarChar, strDOCNO, 100);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //폐기 승인 된 설비 MDM 에 송신 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_EQUEquipDiscard_MDM", dtParam);

                //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// MDM 송신이 성공시 MASEquipMDMIFFlag Update
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipCode">설비</param>
        /// <param name="strDOCNO">공장||설비로된코드(그룹웨어경우)</param>
        /// <param name="sqlCon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveEquipMDMIFDicardFlag(string strPlantCode, string strEquipCode, string strDOCNO)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                sql.mfConnect();

                //트랜젝션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strDOCNO", ParameterDirection.Input, SqlDbType.VarChar, strDOCNO, 100);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //MDM 송신이 성공시 MASEquipMDMIFFlag업데이트 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquip_MDMFlag", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (!ErrRtn.ErrNum.Equals(0))
                    trans.Rollback();
                else
                    trans.Commit();


                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비콤보박스 메소드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">그룹코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquip_Combo(string strPlantCode, string strAreaCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, string strEquipGroupCode,
                                           string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_EquipCombo1", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }


        /// <summary>
        /// 설비정보Popup 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadEquip_POPUP(string strPlantCode, string strStationCode,
                                            string strEquipLocCode, string strProcessGroup,
                                            string strEquipLargeTypeCode, string strEquipGroupCode,
                                            string strLang)
        {
            DataTable dtEquip = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 호출
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquip_POPUP", dtParamter);

                //설비정보 리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquip.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipFile : ServicedComponent
    {
        /// <summary>
        /// 설비첨부화일 컬럼셋
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetEquipFile()
        {
            DataTable dtEquipFile = new DataTable();
            try
            {
                dtEquipFile.Columns.Add("PlantCode", typeof(string));
                dtEquipFile.Columns.Add("EquipCode", typeof(string));
                dtEquipFile.Columns.Add("Seq", typeof(Int32));
                dtEquipFile.Columns.Add("FileName", typeof(string));
                dtEquipFile.Columns.Add("FileDesc", typeof(string));

                return dtEquipFile;
            }
            catch (Exception ex)
            {
                return dtEquipFile;
            }
            finally
            {
                dtEquipFile.Dispose();
            }
        }

        /// <summary>
        /// 설비첨부화일 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipFile(string strPlantCode, string strEquipCode)
        {
            SQLS sql = new SQLS();
            DataTable dtFile = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                //sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipFile", dtParamter);

                return dtFile;
            }
            catch (Exception ex)
            {
                return dtFile;
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtFile.Dispose();
            }
        }

        /// <summary>
        /// 설비 첨부화일 삭제
        /// </summary>
        /// <param name="dtEquipFile"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteEquipFIle(string strPlantCode, string strEquipCode, SQLS sql, SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                DataTable dtPareamter = sql.mfSetParamDataTable();
                //파라미터값저장
                sql.mfAddParamDataRow(dtPareamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtPareamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtPareamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtPareamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipFile", dtPareamter);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }

        }

        /// <summary>
        /// 설비 첨부화일 저장
        /// </summary>
        /// <param name="dtEquipFile"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipFIle(string strPlantCode, string strEquipCode, DataTable dtEquipFile, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //삭제먼저
                strErrRtn = mfDeleteEquipFIle(strPlantCode, strEquipCode, sql, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //에러가 없으면 갱신
                if (ErrRtn.ErrNum == 0)
                {
                    for (int i = 0; i < dtEquipFile.Rows.Count; i++)
                    {
                        DataTable dtPareamter = sql.mfSetParamDataTable();
                        //파라미터값저장
                        sql.mfAddParamDataRow(dtPareamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtPareamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipFile.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtPareamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipFile.Rows[i]["EquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtPareamter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipFile.Rows[i]["Seq"].ToString());
                        sql.mfAddParamDataRow(dtPareamter, "@i_strFileName", ParameterDirection.Input, SqlDbType.VarChar, dtEquipFile.Rows[i]["FileName"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtPareamter, "@i_strFileDesc", ParameterDirection.Input, SqlDbType.VarChar, dtEquipFile.Rows[i]["FileDesc"].ToString(), 1000);
                        sql.mfAddParamDataRow(dtPareamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtPareamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtPareamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                        //프로시저실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipFile", dtPareamter);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                            break;
                    }
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                else
                {
                    trans.Rollback();
                }
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }

        }


        /// <summary>
        /// 설비 첨부화일 저장
        /// </summary>
        /// <param name="dtEquipFile"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipFIle(string strPlantCode, string strEquipCode, DataTable dtEquipFile, DataTable dtEquipPMCancel ,string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                if (!strPlantCode.Equals(string.Empty))
                {
                    //삭제먼저
                    strErrRtn = mfDeleteEquipFIle(strPlantCode, strEquipCode, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    //에러가 없으면 갱신
                    if (ErrRtn.ErrNum == 0)
                    {
                        for (int i = 0; i < dtEquipFile.Rows.Count; i++)
                        {
                            DataTable dtPareamter = sql.mfSetParamDataTable();
                            //파라미터값저장
                            sql.mfAddParamDataRow(dtPareamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                            sql.mfAddParamDataRow(dtPareamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipFile.Rows[i]["PlantCode"].ToString(), 10);
                            sql.mfAddParamDataRow(dtPareamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipFile.Rows[i]["EquipCode"].ToString(), 20);
                            sql.mfAddParamDataRow(dtPareamter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipFile.Rows[i]["Seq"].ToString());
                            sql.mfAddParamDataRow(dtPareamter, "@i_strFileName", ParameterDirection.Input, SqlDbType.VarChar, dtEquipFile.Rows[i]["FileName"].ToString(), 1000);
                            sql.mfAddParamDataRow(dtPareamter, "@i_strFileDesc", ParameterDirection.Input, SqlDbType.VarChar, dtEquipFile.Rows[i]["FileDesc"].ToString(), 1000);
                            sql.mfAddParamDataRow(dtPareamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                            sql.mfAddParamDataRow(dtPareamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                            sql.mfAddParamDataRow(dtPareamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                            //프로시저실행
                            strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipFile", dtPareamter);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        if (ErrRtn.ErrNum != 0)
                            trans.Rollback();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }
                // 설비PM제외정보저장 매서드 실행

                if (dtEquipPMCancel.Rows.Count > 0)
                {
                    strErrRtn = mfSaveEquipPMCancel(dtEquipPMCancel, strUserIP, strUserID, sql.SqlCon, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 롤백처리
                    if (!ErrRtn.ErrNum.Equals(0))
                        trans.Rollback();
                }
  

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }

        }


        /// <summary>
        /// PM제외여부Flag 설정
        /// </summary>
        /// <param name="dtEquipPMCancel">PM제외여부정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveEquipPMCancel(DataTable dtEquipPMCancel, string strUserIP, string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                for (int i = 0; i < dtEquipPMCancel.Rows.Count; i++)
                {
                    //프로시저의 파라미터정보저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMCancel.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMCancel.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMCancelFlag", ParameterDirection.Input, SqlDbType.Char, dtEquipPMCancel.Rows[i]["PMCancelFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 설비PM제외여부 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_MASEquip_PMCancel", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 처리 실패시 루프를 멈춘다.
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }
                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }

    #region 설비점검정보

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipPMH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipPMH : ServicedComponent
    {
        /// <summary>
        /// 설비점검정보헤더 컬럼셋
        /// </summary>
        /// <returns>데이터가없는 컬럼</returns>
        public DataTable mfSetEquipPMHData()
        {
            DataTable dtEquipPMH = new DataTable();
            try
            {
                dtEquipPMH.Columns.Add("StdNumber", typeof(string));
                dtEquipPMH.Columns.Add("VersionNum", typeof(string));
                dtEquipPMH.Columns.Add("PlantCode", typeof(string));
                dtEquipPMH.Columns.Add("PlantName", typeof(string));
                dtEquipPMH.Columns.Add("EquipGroupCode", typeof(string));
                dtEquipPMH.Columns.Add("EquipGroupName", typeof(string));
                dtEquipPMH.Columns.Add("StationCode", typeof(string));
                dtEquipPMH.Columns.Add("StationName", typeof(string));
                dtEquipPMH.Columns.Add("EquipLocCode", typeof(string));
                dtEquipPMH.Columns.Add("EquipLocName", typeof(string));
                dtEquipPMH.Columns.Add("ProcessGroup", typeof(string));
                dtEquipPMH.Columns.Add("EquipLargeTypeCode", typeof(string));
                dtEquipPMH.Columns.Add("EquipLargeTypeName", typeof(string));
                dtEquipPMH.Columns.Add("EquipMiddleTypeName", typeof(string));
                dtEquipPMH.Columns.Add("EquipTypeCode", typeof(string));
                dtEquipPMH.Columns.Add("WriteID", typeof(string));
                dtEquipPMH.Columns.Add("WriteDate", typeof(string));
                dtEquipPMH.Columns.Add("AdmitID", typeof(string));
                dtEquipPMH.Columns.Add("AdmitSecondID", typeof(string));
                dtEquipPMH.Columns.Add("AdmitDate", typeof(string));
                dtEquipPMH.Columns.Add("EtcDesc", typeof(string));
                dtEquipPMH.Columns.Add("RejectReason", typeof(string));
                dtEquipPMH.Columns.Add("RevisionReason", typeof(string));
                dtEquipPMH.Columns.Add("AdmitStatusCode", typeof(string));
                dtEquipPMH.Columns.Add("AdmitState", typeof(string));
                dtEquipPMH.Columns.Add("AdmitSecondState", typeof(string));

                

                return dtEquipPMH;
            }
            catch (Exception ex)
            {
                return dtEquipPMH;
            }
            finally
            {
                dtEquipPMH.Dispose();
            }
        }

        #region Select

        /// <summary>
        /// 설비점검정보 승인 조회
        /// </summary>
        /// <param name="strUserID">사용자아이피</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>승인요청중인정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMAdmit(string strUserID, string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtAdmit = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtAdmit = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMAdmit", dtParamter);

                //정보리턴
                return dtAdmit;
            }
            catch (Exception ex)
            {
                //정보리턴
                return dtAdmit;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtAdmit.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보 승인 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strStationCode">StatonCode</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>승인요청중인정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMAdmit_S(string strPlantCode
                                            , string strStationCode
                                            , string strEquipLocCode
                                            , string strProcessGroupCode
                                            , string strEquipLargeTypeCode
                                            , string strEquipGroupCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtAdmit = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParamter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);

                sql.mfAddParamDataRow(dtParamter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저실행
                dtAdmit = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMAdmit_S", dtParamter);
                
                //정보리턴
                return dtAdmit;
            }
            catch (Exception ex)
            {
                //정보리턴
                return dtAdmit;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtAdmit.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보승인헤더 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="strVersionNum">개정번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검승인헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMAdmitH(string strPlantCode, string strStdNumber, string strVersionNum, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtAdmitH = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar,strStdNumber,10);
                sql.mfAddParamDataRow(dtParam,"@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int,strVersionNum);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

                //설비점검정보승인헤더 조회 프로시저 실행
                dtAdmitH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMAdmitH", dtParam);

                //설비점검정보승인헤더
                return dtAdmitH;
            }
            catch (Exception ex)
            {
                return dtAdmitH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtAdmitH.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보승인헤더 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="strVersionNum">개정번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검승인헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMAdmitH_S(string strPlantCode, string strStdNumber, string strVersionNum, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtAdmitH = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 10);
                sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, strVersionNum);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //설비점검정보승인헤더 조회 프로시저 실행
                dtAdmitH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMAdmitH_S", dtParam);
                
                //설비점검정보승인헤더
                return dtAdmitH;
            }
            catch (Exception ex)
            {
                return dtAdmitH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtAdmitH.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보승인리스트
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <param name="strStationCode">StatonCode</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검정보승인리스트</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMAdmitFN(string strPlantCode, string strStationCode, string strEquipLocCode,string strProcessGroupCode,string strEquipLargeTypeCode,string strEquipGroupCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPMAdmitFN = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //설비점검정보승인리스트 조회 프로시저실행
                dtPMAdmitFN = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMAdmitFN", dtParam);

                //설비점검정보승인리스트
                return dtPMAdmitFN;
            }
            catch (Exception ex)
            {
                return dtPMAdmitFN;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPMAdmitFN.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비점검정보헤더 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipGroupCode">설비그룹코드</param>
        /// <returns>설비점검정보헤더 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMH(string strPlantCode, string strEquipGroupCode, string strLang)
        {
            DataTable dtEquipPMH = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비 오픈
                sql.mfConnect();

                DataTable dtParameter = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저 호출
                dtEquipPMH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMH", dtParameter);
                //정보 리턴
                return dtEquipPMH;
            }
            catch (Exception ex)
            {
                return dtEquipPMH;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipPMH.Dispose();
            }
        }


        /// <summary>
        /// 설비점검정보헤더 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">설비위치코드</param>
        /// <param name="strEquipTypeCode">설비유형코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMH_H(string strPlantCode, string strEquipGroupCode, string strStationCode, string strEquipLocCode,string strEquipLargeTypeCode, string strProcessGroup ,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipPMH = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //설비점검정보헤더 조회(Station,위치,유형검색조건추가) 프로시저 실행
                dtEquipPMH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMH_H", dtParam);

                //설비점검정보등록정보 리턴
                return dtEquipPMH;
            }
            catch (Exception ex)
            {
                return dtEquipPMH;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                dtEquipPMH.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검그룹 팝업창 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipPMHPopup(string strPlantCode, string strStationCode, string strEquipLocCode
                                                            , string strProcessGroupCode, string strEquipLargeTypeCode, string strEquipGroupCode, string strLang)
        {
            DataTable dtEquipPMH = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode" , ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtEquipPMH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMHPop", dtParam);

                return dtEquipPMH;
            }
            catch (Exception ex)
            {
                return dtEquipPMH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipPMH.Dispose();
            }
        }

        /// <summary>
        /// 개정시 StdNumber에서 마지막 개정번호 검색
        /// </summary>
        /// <param name="strStdNumber">표준번호</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable dtReadEquipPMHMaxVer(string strStdNumber)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 10);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMH_MaxVer", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        #endregion

        #region Save,Delete
        /// <summary>
        /// 설비점검정보헤더 저장
        /// </summary>
        /// <param name="dtEquipPMH">저장할 설비점검정보헤더 정보</param>
        /// <param name="dtEquipPMD">저장할 설비점검정보상세 정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipPMH(DataTable dtEquipPMH, DataTable dtEquipPMD, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            string strStdNumber = "";
            string strVersionNum = "";

            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtEquipPMH.Rows.Count; i++)
                {
                    //Transaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtPareamter = sql.mfSetParamDataTable();
                    //파라미터값저장
                    sql.mfAddParamDataRow(dtPareamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtPareamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["StdNumber"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtEquipPMH.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtPareamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["AdmitID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strRejectReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["RejectReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strRevisionReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["RevisionReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["AdmitStatusCode"].ToString(), 2);

                    sql.mfAddParamDataRow(dtPareamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                    sql.mfAddParamDataRow(dtPareamter, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtPareamter, "@o_intVersionNum", ParameterDirection.Output, SqlDbType.Int);
                    sql.mfAddParamDataRow(dtPareamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipPMH", dtPareamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    // 프로시져내의 리턴값을 받아옴
                    strStdNumber = ErrRtn.mfGetReturnValue(0);
                    strVersionNum = ErrRtn.mfGetReturnValue(1);



                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        //상세정보 삭제
                        EquipPMD clsEquipPMD = new EquipPMD();
                        strErrRtn = clsEquipPMD.mfDeleteEquipPMD(strStdNumber, strVersionNum, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        //상세정보 저장
                        if (dtEquipPMD.Rows.Count > 0)
                        {


                            strErrRtn = clsEquipPMD.mfSaveEquipPMD(dtEquipPMD, strStdNumber, strVersionNum, strUserID, strUserIP, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }


                        }


                    }

                    trans.Commit();
                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비점검정보헤더 저장(Station,위치,유형추가)
        /// </summary>
        /// <param name="dtEquipPMH">저장할 설비점검정보헤더 정보</param>
        /// <param name="dtEquipPMD">저장할 설비점검정보상세 정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipPMH_H(DataTable dtEquipPMH, DataTable dtEquipPMD, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            string strStdNumber = "";
            string strVersionNum = "";

            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtEquipPMH.Rows.Count; i++)
                {
                    //Transaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtPareamter = sql.mfSetParamDataTable();
                    //파라미터값저장
                    sql.mfAddParamDataRow(dtPareamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtPareamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["StdNumber"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtEquipPMH.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtPareamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["StationCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipLocCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipLargeTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["AdmitID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strRejectReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["RejectReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strRevisionReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["RevisionReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["AdmitStatusCode"].ToString(), 2);

                    sql.mfAddParamDataRow(dtPareamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                    sql.mfAddParamDataRow(dtPareamter, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtPareamter, "@o_intVersionNum", ParameterDirection.Output, SqlDbType.Int);
                    sql.mfAddParamDataRow(dtPareamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipPMH_H", dtPareamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    // 프로시져내의 리턴값을 받아옴
                    strStdNumber = ErrRtn.mfGetReturnValue(0);
                    strVersionNum = ErrRtn.mfGetReturnValue(1);



                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        //상세정보 삭제
                        EquipPMD clsEquipPMD = new EquipPMD();
                        strErrRtn = clsEquipPMD.mfDeleteEquipPMD(strStdNumber, strVersionNum, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        //상세정보 저장
                        if (dtEquipPMD.Rows.Count > 0)
                        {


                            strErrRtn = clsEquipPMD.mfSaveEquipPMD(dtEquipPMD, strStdNumber, strVersionNum, strUserID, strUserIP, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                    trans.Commit();
                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비점검정보헤더 저장(Station,위치,유형추가)
        /// </summary>
        /// <param name="dtEquipPMH">저장할 설비점검정보헤더 정보</param>
        /// <param name="dtEquipPMD">저장할 설비점검정보상세 정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipPMH_H_S(DataTable dtEquipPMH, DataTable dtEquipPMD, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            string strStdNumber = "";
            string strVersionNum = "";

            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtEquipPMH.Rows.Count; i++)
                {
                    //Transaction시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtPareamter = sql.mfSetParamDataTable();
                    //파라미터값저장
                    sql.mfAddParamDataRow(dtPareamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtPareamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["StdNumber"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtEquipPMH.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtPareamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["StationCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipLocCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipLargeTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["AdmitID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strAdmitSecondID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["AdmitSecondID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strRejectReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["RejectReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strRevisionReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMH.Rows[i]["RevisionReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["AdmitStatusCode"].ToString(), 2);

                    sql.mfAddParamDataRow(dtPareamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtPareamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                    sql.mfAddParamDataRow(dtPareamter, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtPareamter, "@o_intVersionNum", ParameterDirection.Output, SqlDbType.Int);
                    sql.mfAddParamDataRow(dtPareamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipPMH_H_S", dtPareamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    // 프로시져내의 리턴값을 받아옴
                    strStdNumber = ErrRtn.mfGetReturnValue(0);
                    strVersionNum = ErrRtn.mfGetReturnValue(1);



                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        //상세정보 삭제
                        EquipPMD clsEquipPMD = new EquipPMD();
                        strErrRtn = clsEquipPMD.mfDeleteEquipPMD(strStdNumber, strVersionNum, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        //상세정보 저장
                        if (dtEquipPMD.Rows.Count > 0)
                        {


                            strErrRtn = clsEquipPMD.mfSaveEquipPMD(dtEquipPMD, strStdNumber, strVersionNum, strUserID, strUserIP, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                    trans.Commit();
                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보헤더 삭제
        /// </summary>
        /// <param name="dtEquipPMH">삭제할 설비점검정보헤더</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipPMH(DataTable dtEquipPMH)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strStdNum = dtEquipPMH.Rows[0]["StdNumber"].ToString();
            string strVersionNum = dtEquipPMH.Rows[0]["VersionNum"].ToString();

            try
            {
                //디비오픈
                sql.mfConnect();

                SqlTransaction trans;

                for (int i = 0; i < dtEquipPMH.Rows.Count; i++)
                {
                    //BeginTransaction시작
                    trans = sql.SqlCon.BeginTransaction();


                    EquipPMD clsEquipPMD = new EquipPMD();
                    strErrRtn = clsEquipPMD.mfDeleteEquipPMD(strStdNum, strVersionNum, sql.SqlCon, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;

                    }

                    DataTable dtParamter = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["StdNumber"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtEquipPMH.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipPMH", dtParamter);
                    //결과값처리
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }

                }
                //처리값리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보헤더 삭제 (Station,공정구분조건추가)
        /// </summary>
        /// <param name="dtEquipPMH">삭제할 설비점검정보헤더</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipPMH_H(DataTable dtEquipPMH)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strStdNum = dtEquipPMH.Rows[0]["StdNumber"].ToString();
            string strVersionNum = dtEquipPMH.Rows[0]["VersionNum"].ToString();

            try
            {
                //디비오픈
                sql.mfConnect();

                SqlTransaction trans;

                for (int i = 0; i < dtEquipPMH.Rows.Count; i++)
                {
                    //BeginTransaction시작
                    trans = sql.SqlCon.BeginTransaction();


                    EquipPMD clsEquipPMD = new EquipPMD();
                    strErrRtn = clsEquipPMD.mfDeleteEquipPMD(strStdNum, strVersionNum, sql.SqlCon, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;

                    }

                    DataTable dtParamter = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["StationCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipLocCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["EquipLargeTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMH.Rows[i]["StdNumber"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtEquipPMH.Rows[i]["VersionNum"].ToString());
                    
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    
                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipPMH_H", dtParamter);
                    //결과값처리
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }

                }
                //처리값리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보승인 승인
        /// </summary>
        /// <param name="dtEquipAdmit">승인결과정보</param>
        /// <param name="strUserIP">사용자 아이디</param>
        /// <param name="strUserID">사용자 아이피</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipPMAdmit(DataTable dtEquipAdmit, string strUserIP, string strUserID)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();
                DataTable dtParamter = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtEquipAdmit.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["AdmitID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["AdmitDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strRejectReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipAdmit.Rows[0]["RejectReason"].ToString(), 100);
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["AdmitStatusCode"].ToString(), 2);
                sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipAdmit.Rows[0]["EtcDesc"].ToString(), 100);


                sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipPMAdmit", dtParamter);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보승인 승인
        /// </summary>
        /// <param name="dtEquipAdmit">승인결과정보</param>
        /// <param name="strUserIP">사용자 아이디</param>
        /// <param name="strUserID">사용자 아이피</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipPMAdmit_S(DataTable dtEquipAdmit, string strUserIP, string strUserID)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {

                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();

                DataTable dtParamter = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtEquipAdmit.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["AdmitID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitState", ParameterDirection.Input, SqlDbType.Char, dtEquipAdmit.Rows[0]["AdmitState"].ToString(), 1);
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitSecondID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["AdmitSecondID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitSecondState", ParameterDirection.Input, SqlDbType.Char, dtEquipAdmit.Rows[0]["AdmitSecondState"].ToString(), 1);
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["AdmitDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParamter, "@i_strRejectReason", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipAdmit.Rows[0]["RejectReason"].ToString(), 100);
                sql.mfAddParamDataRow(dtParamter, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipAdmit.Rows[0]["AdmitStatusCode"].ToString(), 2);
                sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipAdmit.Rows[0]["EtcDesc"].ToString(), 100);

                sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipPMAdmit_S", dtParamter);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region 그룹웨어 전송
        /// <summary>
        /// 그룹웨어 전송
        /// </summary>
        /// <param name="dt">PMH</param>
        /// <param name="dtFromItem">변경전 PMD</param>
        /// <param name="dtToItem">변경후 PMD</param>
        /// <param name="dtSendLine">결제선</param>
        /// <param name="dtCcLine">통보선</param>
        /// <param name="dtFormInfo">문서정보</param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public DataTable mfSaveMASEquipPMGRWApproval(DataTable dt, DataTable dtFromItem, DataTable dtToItem, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        {
            QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
            DataTable dtRtn = new DataTable();

            try
            {
                string strPlantCode = dt.Rows[0]["PlantCode"].ToString();
                string strStdNumber = dt.Rows[0]["StdNumber"].ToString();
                string strVersionNum = dt.Rows[0]["VersionNum"].ToString();
                string strLegacyKey = strPlantCode + "||" + strStdNumber + "||" + strVersionNum;

                DataTable dtfile = new DataTable();
                dtfile.Columns.Add("ImageFile", typeof(string));

                DataTable dtFileInfo = grw.mfSetFileInfoDataTable();
                string strFilePach = "MASEquipPMD\\";
                foreach (DataRow dr in dtfile.Rows)
                {
                    DataRow _dr = dtFileInfo.NewRow();
                    _dr["PlantCode"] = strPlantCode;
                    _dr["NM_FILE"] = dr["ImageFile"].ToString();
                    _dr["NM_LOCATION"] = strFilePach;
                }

                string strPlantName = dt.Rows[0]["PlantName"].ToString();
                string strStationName = dt.Rows[0]["StationName"].ToString();
                string strEquipGroupName = dt.Rows[0]["EquipGroupName"].ToString();
                string strRevisionReason = dt.Rows[0]["RevisionReason"].ToString();
                string strEquipLocName = dt.Rows[0]["EquipLocName"].ToString();
                string strEquipLTypeName = dt.Rows[0]["EquipLargeTypeName"].ToString();
                string strEquipMTypeName = dt.Rows[0]["EquipMiddleTypeName"].ToString();

                dtRtn = grw.EquipPM(strLegacyKey, dtSendLine, dtCcLine, strPlantName, strStationName, strEquipLocName, strEquipLTypeName, strEquipMTypeName, strEquipGroupName
                                , strRevisionReason, dtFromItem, dtToItem, dtFormInfo, dtFileInfo, strUserIP, strUserID);

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            return dtRtn;
        }
        #endregion

    }

    #endregion

    #region 설비점검정보상세

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipPMD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipPMD : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public EquipPMD()
        {

        }

        public EquipPMD(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 설비점검정보상세 컬럼셋
        /// </summary>
        /// <returns>정보가없는 컬럼</returns>
        public DataTable mfSetEquipPMDData()
        {
            DataTable dtEquipPMD = new DataTable();
            try
            {
                dtEquipPMD.Columns.Add("StdNumber", typeof(string));
                dtEquipPMD.Columns.Add("VersionNum", typeof(string));
                dtEquipPMD.Columns.Add("Seq", typeof(string));
                dtEquipPMD.Columns.Add("PMInspectName", typeof(string));
                dtEquipPMD.Columns.Add("PMInspectRegion", typeof(string));
                dtEquipPMD.Columns.Add("PMInspectCriteria", typeof(string));
                dtEquipPMD.Columns.Add("PMPeriodCode", typeof(string));
                dtEquipPMD.Columns.Add("PMMethod", typeof(string));
                dtEquipPMD.Columns.Add("FaultFixMethod", typeof(string));
                dtEquipPMD.Columns.Add("ImageFile", typeof(string));
                dtEquipPMD.Columns.Add("UnitDesc", typeof(string));
                dtEquipPMD.Columns.Add("StandardManCount", typeof(string));
                dtEquipPMD.Columns.Add("StandardTime", typeof(string));
                dtEquipPMD.Columns.Add("LevelCode", typeof(string));
                dtEquipPMD.Columns.Add("MeasureValueFlag", typeof(string));

                return dtEquipPMD;
            }
            catch (Exception)
            {
                return dtEquipPMD;
            }
            finally
            {
                dtEquipPMD.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보상세 조회
        /// </summary>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="intVersionNum">개정번호</param>
        /// <returns>설비점검정보상세 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMD(string strStdNumber, string strVersionNum)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipPMD = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, strVersionNum);

                //프로시저 호출
                dtEquipPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipPMD", dtParameter);

                //정보리턴
                return dtEquipPMD;
            }
            catch (Exception ex)
            {
                return dtEquipPMD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipPMD.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보상세 저장
        /// </summary>
        /// <param name="dtEquipPMD">저장할정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="strVersionNum">개정번호</param>
        /// <param name="sql">Sql.sqlcon</param>
        /// <param name="trans">sql.SqlCon.BeginTransaction()</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipPMD(DataTable dtEquipPMD, string strStdNumber, string strVersionNum, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dtEquipPMD.Rows.Count; i++)
                {
                    //trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParamter = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, strVersionNum);
                    sql.mfAddParamDataRow(dtParamter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipPMD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strPMInspectName", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMD.Rows[i]["PMInspectName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPMInspectRegion", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMD.Rows[i]["PMInspectRegion"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPMInspectCriteria", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMD.Rows[i]["PMInspectCriteria"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPMPeriodCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMD.Rows[i]["PMPeriodCode"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPMMethod", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMD.Rows[i]["PMMethod"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strFaultFixMethod", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMD.Rows[i]["FaultFixMethod"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strImageFile", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMD.Rows[i]["ImageFile"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipPMD.Rows[i]["UnitDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_intStandardManCount", ParameterDirection.Input, SqlDbType.Int, dtEquipPMD.Rows[i]["StandardManCount"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_decStandardTime", ParameterDirection.Input, SqlDbType.Decimal, dtEquipPMD.Rows[i]["StandardTime"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strLevelCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMD.Rows[i]["LevelCode"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strMeasureValueFlag", ParameterDirection.Input, SqlDbType.VarChar, dtEquipPMD.Rows[i]["MeasureValueFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParamter, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 10);
                    sql.mfAddParamDataRow(dtParamter, "@o_intVersionNum", ParameterDirection.Output, SqlDbType.Int);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_MASEquipPMD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    //else
                    //{
                    //    trans.Commit();
                    //}
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보상세 삭제
        /// </summary>
        /// <param name="StdNumber">표준번호</param>
        /// <param name="VersionNum">개정번호</param>
        /// <param name="sql">sql.con</param>
        /// <param name="trans">BeginTrans</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipPMD(string StdNumber, string VersionNum, SqlConnection sqlCon, SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                //sql.mfConnect();
                //SqlTransaction trans;

                //trans = sql.SqlCon.BeginTransaction();
                DataTable dtParmter = sql.mfSetParamDataTable();

                //파라미터값저장  
                sql.mfAddParamDataRow(dtParmter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParmter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, StdNumber, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, VersionNum);
                //sql.mfAddParamDataRow(dtParmter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipPMD.Rows[i]["Seq"].ToString());

                sql.mfAddParamDataRow(dtParmter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_MASEquipPMD", dtParmter);
                //결과값 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;

                }
                //else
                //{
                //    trans.Commit();
                //}


                //결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 설비점검정보상세 조회
        /// </summary>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="intVersionNum">개정번호</param>
        /// <returns>설비점검정보상세 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMDForEquipGroup(string strPlantCode, string strEquipGroupCode, SqlConnection sqlConn)
        {
            SQLS sql = new SQLS(sqlConn.ConnectionString);    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtEquipPMD = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    //디비오픈
                    //sql.mfConnect();  //실행용
                    sqlConn.Open();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);

                //프로시저 호출
                dtEquipPMD = sql.mfExecReadStoredProc(sqlConn, "up_Select_MASEquipPMD_EquipGroup", dtParameter); //실행용
                //dtEquipPMD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipPMD_EquipGroup", dtParameter);     //디버깅용


                //정보리턴
                return dtEquipPMD;
            }
            catch (Exception ex)
            {
                return dtEquipPMD;
                throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
                dtEquipPMD.Dispose();
            }
        }

        /// <summary>
        /// 설비점검정보상세 조회 : 작업자기준
        /// </summary>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="intVersionNum">개정번호</param>
        /// <returns>설비점검정보상세 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMDForEquipWorker(string strPlantCode, string strUserID, SqlConnection sqlConn)
        {
            SQLS sql = new SQLS(sqlConn.ConnectionString);    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtEquipPMD = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    //디비오픈
                    //sql.mfConnect();  //실행용
                    sqlConn.Open();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                //프로시저 호출
                dtEquipPMD = sql.mfExecReadStoredProc(sqlConn, "up_Select_MASEquipPMD_EquipWorker", dtParameter); //실행용
                //dtEquipPMD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipPMD_EquipWorker", dtParameter);     //디버깅용


                //정보리턴
                return dtEquipPMD;
            }
            catch (Exception ex)
            {
                return dtEquipPMD;
                throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비점검정보상세 조회 : 일괄(Batch)기준
        /// </summary>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="intVersionNum">개정번호</param>
        /// <returns>설비점검정보상세 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipPMDForBatch(string strPlantCode, string strAreaCode, string strStationCode, string strProcGubunCode, SqlConnection sqlConn)
        {
            SQLS sql = new SQLS(sqlConn.ConnectionString);    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtEquipPMD = new DataTable();
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    //디비오픈
                    //sql.mfConnect();  //실행용
                    sqlConn.Open();

                DataTable dtParameter = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strEquipProcGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strProcGubunCode, 10);

                //프로시저 호출
                dtEquipPMD = sql.mfExecReadStoredProc(sqlConn, "up_Select_MASEquipPMD_Batch", dtParameter); //실행용
                //dtEquipPMD = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipPMD_Batch", dtParameter);     //디버깅용


                //정보리턴
                return dtEquipPMD;
            }
            catch (Exception ex)
            {
                return dtEquipPMD;
                throw (ex);
            }
            finally
            {
                //디비종료
                //sql.mfDisConnect();
                sql.Dispose();
                dtEquipPMD.Dispose();
            }
        }

    }

    #endregion

    #region 설비점검결과 등록
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("PMResultType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class PMResultType : ServicedComponent
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtMASPMResult = new DataTable();

            try
            {
                dtMASPMResult.Columns.Add("PlantCode", typeof(String));
                dtMASPMResult.Columns.Add("PMResultCode", typeof(String));
                dtMASPMResult.Columns.Add("PMResultName", typeof(String));
                dtMASPMResult.Columns.Add("PMResultNameCh", typeof(String));
                dtMASPMResult.Columns.Add("PMResultNameEn", typeof(String));
                dtMASPMResult.Columns.Add("PMUseFlag", typeof(String));
                dtMASPMResult.Columns.Add("ChangeFlag", typeof(String));
                dtMASPMResult.Columns.Add("UseFlag", typeof(Char));

                return dtMASPMResult;
            }
            catch (Exception ex)
            {
                return dtMASPMResult;
                throw (ex);
            }
            finally
            {
                dtMASPMResult.Dispose();
            }
        }

        /// <summary>
        /// 점검결과 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과콤보</returns>
        [AutoComplete]
        public DataTable mfReadPMResultTypeCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtResultCombo = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 실행
                dtResultCombo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPMResultTypeCombo", dtParam);

                //정보리턴
                return dtResultCombo;
            }
            catch (Exception ex)
            {
                return dtResultCombo;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtResultCombo.Dispose();
            }
        }

        /// <summary>
        /// 점검결과 정보등록
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>점검결과 정보등록</returns>

        [AutoComplete]
        public DataTable mfReadMASPMResultType(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMASPMResult = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저호출
                dtMASPMResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPMResultType", dtParam);
                //정보리턴
                return dtMASPMResult;
            }
            catch (Exception ex)
            {
                return dtMASPMResult;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMASPMResult.Dispose();
            }
        }

        public DataTable mfReadPMResultType_Detail(String strPlantCode, String strPMResultCode, String strLang)
        {
            DataTable dtTable = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //DB 프로시저의 파라미터에 보내는 값을 Datatable에 삽입
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPMResultCode", ParameterDirection.Input, SqlDbType.VarChar, strPMResultCode, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시져 호출
                dtTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPMResultType_Detail", dtParam);

                //정보리턴
                return dtTable;
            }
            catch (Exception ex)
            {
                return dtTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtTable.Dispose();
            }
        }

        /// <summary>
        /// 점검결과 정보저장/// </summary>
        /// <param name="dtMASPMResult">저장할데이터</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>저장</returns>
        [AutoComplete(false)]
        public string mfSavePMResultType(DataTable dtMASPMResult, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtMASPMResult.Rows.Count; i++)
                {


                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASPMResult.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASPMResult.Rows[i]["PMResultCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultName", ParameterDirection.Input, SqlDbType.NVarChar, dtMASPMResult.Rows[i]["PMResultName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtMASPMResult.Rows[i]["PMResultNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtMASPMResult.Rows[i]["PMResultNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMUseFlag", ParameterDirection.Input, SqlDbType.Char, dtMASPMResult.Rows[i]["PMUseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChangeFlag", ParameterDirection.Input, SqlDbType.Char, dtMASPMResult.Rows[i]["ChangeFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtMASPMResult.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASPMResultType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 점검결과 정보 삭제
        /// </summary>
        /// <param name="dtMASPMResult">정보가들어있는데이터</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>삭제</returns>
        [AutoComplete(false)]
        public string mfDeletePMResultType(DataTable dtPMResult)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtPMResult.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMResult.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPMResultCode", ParameterDirection.Input, SqlDbType.VarChar, dtPMResult.Rows[i]["PMResultCode"].ToString(), 5);
                    // PK(PlantCode + PMResultCode) 삭제 조건  
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASPMResultType", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        public DataTable mfReadPMResultType_MESIF(string strPlantCode)
        {
            SQLS sql = new SQLS();
            DataTable dtResultCombo = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);

                //프로시저 실행
                dtResultCombo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPMResultType_MESIF", dtParam);

                //정보리턴
                return dtResultCombo;
            }
            catch (Exception ex)
            {
                return dtResultCombo;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtResultCombo.Dispose();
            }
        }
    }
    #endregion


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPInventory")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPInventory : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDataInfo()
        {
            DataTable dtSPInventory = new DataTable();

            try
            {
                dtSPInventory.Columns.Add("PlantCode", typeof(String));
                dtSPInventory.Columns.Add("SPInventoryCode", typeof(String));
                dtSPInventory.Columns.Add("SPInventoryName", typeof(String));
                dtSPInventory.Columns.Add("SPInventoryNameCh", typeof(String));
                dtSPInventory.Columns.Add("SPInventoryNameEn", typeof(String));
                dtSPInventory.Columns.Add("UseFlag", typeof(String));

                return dtSPInventory;
            }
            catch (Exception ex)
            {
                return dtSPInventory;
                throw (ex);
            }
            finally
            {
                dtSPInventory.Dispose();
            }
        }

        /// <summary>
        /// SP창고정보 콤보  	SPinven
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SP창고정보</returns>
        [AutoComplete]
        public DataTable mfReadMASSPInventoryCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtInven = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtInven = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSPInventoryCombo", dtParam);

                return dtInven;
            }
            catch (Exception ex)
            {
                return dtInven;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtInven.Dispose();
            }
        }

        ///<summary>
        ///조회
        ///</summary>
        ///<param name="strPlantCode">공장코드</param>
        ///<return>dtSPInventory</return>

        [AutoComplete]
        public DataTable mfReadSPInventory(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSPInventory = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 5);

                dtSPInventory = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSPInventory", dtParam);

                return dtSPInventory;
            }
            catch (Exception ex)
            {
                return dtSPInventory;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtSPInventory.Dispose();
            }
        }

        /// <summary>
        /// 저장 Method
        /// </summary>
        /// <param name="dtParam"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>

        [AutoComplete(false)]
        public String mfSaveSPInventory(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    //컬럼
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSPInventoryName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["SPInventoryName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSPInventoryNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["SPInventoryNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSPInventoryNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["SPInventoryNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    //컬럼 끝
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASSPInventory", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        /// <summary>
        /// 삭제 Method
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteSPInventory(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSPInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPInventoryCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASSPInventory", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                if (!ErrRtn.ErrNum.Equals(0))
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPSection")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SPSection : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPSection = new DataTable();

            try
            {
                dtSPSection.Columns.Add("PlantCode", typeof(String));
                dtSPSection.Columns.Add("SPSectionCode", typeof(String));
                dtSPSection.Columns.Add("SPSectionName", typeof(String));
                dtSPSection.Columns.Add("SPSectionNameCh", typeof(String));
                dtSPSection.Columns.Add("SPSectionNameEn", typeof(String));
                dtSPSection.Columns.Add("UseFlag", typeof(String));

                return dtSPSection;
            }
            catch (Exception ex)
            {
                return dtSPSection;
                throw (ex);
            }
            finally
            {
                dtSPSection.Dispose();
            }
        }
        ///<summary>
        ///조회
        ///</summary>
        ///<param name="strPlantCode">공장코드</param>
        ///<return>dtSPSection</return>

        [AutoComplete]
        public DataTable mfReadSPSection(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSPSection = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 5);

                dtSPSection = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSPSection", dtParam);

                return dtSPSection;
            }
            catch (Exception ex)
            {
                return dtSPSection;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtSPSection.Dispose();
            }
        }

        /// <summary>
        /// 저장 Method
        /// </summary>
        /// <param name="dtParam"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>

        [AutoComplete(false)]
        public String mfSaveSPSection(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    //컬럼
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_SPSectionCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPSectionCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_SPSectionName", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPSectionName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_SPSectionNameCh", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPSectionNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_SPSectionNameEn", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPSectionNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    //컬럼 끝
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SPSection", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();

            }
        }
        /// <summary>
        /// 삭제 Method
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        /// 

        [AutoComplete(false)]
        public String mfDeleteSPSection(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_SPSectionCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPSectionCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_SPSectionName", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPSectionName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_SPSectionNameCh", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPSectionNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_SPSectionNameEn", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["SPSectionNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SPSection", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SparePart")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class SparePart : ServicedComponent
    {
        #region STS

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtSPSparePart = new DataTable();

            try
            {
                dtSPSparePart.Columns.Add("PlantCode", typeof(String));
                dtSPSparePart.Columns.Add("SparePartCode", typeof(String));
                dtSPSparePart.Columns.Add("SparePartName", typeof(String));
                dtSPSparePart.Columns.Add("SparePartNameCh", typeof(String));
                dtSPSparePart.Columns.Add("SparePartNameEn", typeof(String));
                dtSPSparePart.Columns.Add("Spec", typeof(String));
                dtSPSparePart.Columns.Add("Maker", typeof(String));
                dtSPSparePart.Columns.Add("UnitCode", typeof(String));
                dtSPSparePart.Columns.Add("UseFlag", typeof(String));

                return dtSPSparePart;
            }
            catch (Exception ex)
            {
                return dtSPSparePart;
                throw (ex);
            }
            finally
            {
                dtSPSparePart.Dispose();
            }
        }

        /// <summary>
        /// SparePart 팝업창
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart정보</returns>
        public DataTable mfReadMASSparePartPOP(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSparePart = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtSparePart = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePartPopup", dtParam);

                return dtSparePart;
            }
            catch (Exception ex)
            {
                return dtSparePart;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtSparePart.Dispose();
            }
        }


        /// <summary>			
        /// SparePart명 		
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlantCode">SparePart코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart명</returns>
        [AutoComplete]
        public DataTable mfReadMASSparePartName(string strPlantCode, string strSparePartCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSparePart = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, strSparePartCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtSparePart = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePartName", dtParam);

                return dtSparePart;
            }
            catch (Exception ex)
            {
                return dtSparePart;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtSparePart.Dispose();
            }
        }

        ///<summary>
        ///조회
        ///</summary>
        ///<param name="strPlantCode">공장코드</param>
        ///<return>dtSPSection</return>

        [AutoComplete]
        public DataTable mfReadSparePart(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSparePart = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 5);

                dtSparePart = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePart", dtParam);

                return dtSparePart;
            }
            catch (Exception ex)
            {
                return dtSparePart;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtSparePart.Dispose();
            }
        }


        /// <summary>
        /// SparePart 상세정보  조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strSparePartCode">SparePart코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart</returns>
        [AutoComplete]
        public DataTable mfReadSparePartDetail(String strPlantCode, String strSparePartCode, String strLang)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, strSparePartCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePartDetail", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }

        /// <summary>
        /// SparePart저장
        /// </summary>
        /// <param name="dtSaveSP"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveSparePart(DataTable dtSaveSP, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSaveSP.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveSP.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveSP.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strSparePartName", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["SparePartName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSparePartNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["SparePartNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSparePartNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["SparePartNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["Spec"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaker", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["Maker"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveSP.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtSaveSP.Rows[i]["UseFlag"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASSparePart", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfDeleteMASSparePart(DataTable dtDelSP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strResult = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDelSP.Rows.Count; i++)
                {

                    DataTable dtPram = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtPram, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtPram, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelSP.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPram, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelSP.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPram, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASSparePart", dtPram);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                return strResult;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region PSTS

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataSet mfSetDatainfo_PSTS()
        {
            DataSet dsSparePart = new DataSet();
            try
            {
                DataTable dtSPSparePart = new DataTable();
                dtSPSparePart.Columns.Add("Check", typeof(Boolean));
                dtSPSparePart.Columns.Add("PlantCode", typeof(String));
                dtSPSparePart.Columns.Add("SparePartCode", typeof(String));
                dtSPSparePart.Columns.Add("SparePartName", typeof(String));
                dtSPSparePart.Columns.Add("SparePartNameCh", typeof(String));
                dtSPSparePart.Columns.Add("SparePartNameEn", typeof(String));
                dtSPSparePart.Columns.Add("Spec", typeof(String));
                dtSPSparePart.Columns.Add("Maker", typeof(String));
                dtSPSparePart.Columns.Add("UnitCode", typeof(String));
                dtSPSparePart.Columns.Add("SafeQty", typeof(Int32));
                dtSPSparePart.Columns.Add("UseFlag", typeof(String));

                dtSPSparePart.TableName = "SparePart";
                dsSparePart.Tables.Add(dtSPSparePart);
                return dsSparePart;
            }
            catch (Exception ex)
            {
                return dsSparePart;
                throw (ex);
            }
            finally
            {
                dsSparePart.Dispose();
            }
        }

        /// <summary>
        /// SparePart 팝업창
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart정보</returns>
        public DataTable mfReadMASSparePartPOP_PSTS(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSparePart = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtSparePart = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePartPopup", dtParam);

                return dtSparePart;
            }
            catch (Exception ex)
            {
                return dtSparePart;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtSparePart.Dispose();
            }
        }


        /// <summary>			
        /// SparePart명 		
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPlantCode">SparePart코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart명</returns>
        [AutoComplete]
        public string mfReadMASSparePartName_PSTS(string strPlantCode, string strSparePartCode, string strLang)
        {
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, strSparePartCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                DataTable dtSparePart = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePartName", dtParam);
                dtSparePart.TableName = "SparePart";
                DataSet dsSparePart = new DataSet();
                dsSparePart.Tables.Add(dtSparePart);

                return CompressDataSet(dsSparePart);
            }
            catch (Exception ex)
            {
                return string.Empty;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        ///<summary>
        ///조회
        ///</summary>
        ///<param name="strPlantCode">공장코드</param>
        ///<return>dtSPSection</return>
        [AutoComplete]
        public string mfReadSparePart_PSTS(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 5);

                DataTable dtSparePart = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePart_PSTS", dtParam);
                dtSparePart.TableName = "SparePart";
                DataSet dsSP = new DataSet();
                dsSP.Tables.Add(dtSparePart);

                return CompressDataSet(dsSP);
            }
            catch (Exception ex)
            {
                return string.Empty;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// SparePart 상세정보  조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strSparePartCode">SparePart코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart</returns>
        [AutoComplete]
        public DataTable mfReadSparePartDetail_PSTS(String strPlantCode, String strSparePartCode, String strLang)
        {
            DataTable dtReadTable = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, strSparePartCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadTable = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSparePartDetail", dtParmter);
                //정보리턴
                return dtReadTable;
            }
            catch (Exception ex)
            {
                return dtReadTable;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadTable.Dispose();
            }
        }

        /// <summary>
        /// SparePart저장
        /// </summary>
        /// <param name="dtSaveSP"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveSparePart_PSTS(string strDS, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                DataSet dsSaveSP = DecompressDataSet(strDS);
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                DataTable dtSaveSP = dsSaveSP.Tables["SparePart"];

                for (int i = 0; i < dtSaveSP.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveSP.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveSP.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strSparePartName", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["SparePartName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSparePartNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["SparePartNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSparePartNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["SparePartNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["Spec"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaker", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveSP.Rows[i]["Maker"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveSP.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSafeQty", ParameterDirection.Input, SqlDbType.Int, dtSaveSP.Rows[i]["SafeQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtSaveSP.Rows[i]["UseFlag"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASSparePart_PSTS", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfDeleteMASSparePart_PSTS(string strDSSP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";

                DataSet dsSparePart = DecompressDataSet(strDSSP);

                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                DataTable dtDelSP = dsSparePart.Tables["SparePart"];

                for (int i = 0; i < dtDelSP.Rows.Count; i++)
                {

                    DataTable dtPram = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtPram, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtPram, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelSP.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPram, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelSP.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtPram, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASSparePart", dtPram);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        private string CompressDataSet(DataSet dataSet)
        {
            if (dataSet == null)
                throw new ArgumentNullException("dataSet");

            dataSet.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            byte[] result = null;

            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, dataSet);

                using (System.IO.MemoryStream resultStream = new System.IO.MemoryStream())
                {
                    using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(resultStream, System.IO.Compression.CompressionMode.Compress))
                    {
                        result = memoryStream.ToArray();
                        deflateStream.Write(result, 0, result.Length);
                    }

                    result = resultStream.ToArray();
                }
            }

            return Convert.ToBase64String(result, 0, result.Length);
        }

        private string CompressDataTable(DataTable dtTable)
        {
            if (dtTable == null)
                throw new ArgumentNullException("dtTable");

            dtTable.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            byte[] result = null;

            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, dtTable);

                using (System.IO.MemoryStream resultStream = new System.IO.MemoryStream())
                {
                    using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(resultStream, System.IO.Compression.CompressionMode.Compress))
                    {
                        result = memoryStream.ToArray();
                        deflateStream.Write(result, 0, result.Length);
                    }

                    result = resultStream.ToArray();
                }
            }

            return Convert.ToBase64String(result, 0, result.Length);
        }

        private DataSet DecompressDataSet(string base64String)
        {
            if (String.IsNullOrEmpty(base64String))
                throw new ArgumentNullException("base64String");

            byte[] deflatedData = Convert.FromBase64String(base64String);

            using (System.IO.MemoryStream deflatedStream = new System.IO.MemoryStream(deflatedData, false))
            {
                using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(deflatedStream, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (System.IO.MemoryStream uncompressedStream = new System.IO.MemoryStream())
                    {
                        int read = 0;
                        byte[] readBuffer = new byte[64000];

                        while ((read = deflateStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
                            uncompressedStream.Write(readBuffer, 0, read);

                        uncompressedStream.Seek(0L, System.IO.SeekOrigin.Begin);
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        return binaryFormatter.Deserialize(uncompressedStream) as DataSet;
                    }
                }
            }
        }

        #endregion
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DeviceChgPoint")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DeviceChgPoint : ServicedComponent
    {
        /// <summary>
        /// 저장 삭제시 필요한 컬럼셋
        /// </summary>
        public DataTable mfDataSet()
        {
            DataTable dtDevice = new DataTable();
            try
            {

                dtDevice.Columns.Add("PlantCode", typeof(string));
                dtDevice.Columns.Add("DeviceChgPointCode", typeof(string));
                dtDevice.Columns.Add("DeviceChgPointName", typeof(string));
                dtDevice.Columns.Add("DeviceChgPointNameCh", typeof(string));
                dtDevice.Columns.Add("DeviceChgPointNameEn", typeof(string));
                dtDevice.Columns.Add("CCSFlag", typeof(bool));
                dtDevice.Columns.Add("UseFlag", typeof(string));

                return dtDevice;
            }
            catch (Exception ex)
            {
                return dtDevice;
                throw (ex);
            }
            finally
            {
                dtDevice.Dispose();
            }
        }

        /// <summary>
        /// 품종교체점검Point정보 콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>품종교체점검Point정보</returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgPointCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDeviceChgPoint = new DataTable();
            try
            {
                // 디비연결
                sql.mfConnect();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                // 프로시저 실행
                dtDeviceChgPoint = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDeviceChgPointCombo", dtParam);

                return dtDeviceChgPoint;
            }
            catch (Exception ex)
            {
                return dtDeviceChgPoint;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDeviceChgPoint.Dispose();
            }
        }

        /// <summary>
        /// 품종교체점검Point정보등록 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>품종교체점검Point정보</returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgPoint(string strPlantCode, string strLang)
        {
            DataTable dtReadDevice = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저 호출
                dtReadDevice = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDeviceChgPoint", dtParmter);
                //정보리턴
                return dtReadDevice;
            }
            catch (Exception ex)
            {
                return dtReadDevice;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadDevice.Dispose();
            }
        }

        /// <summary>
        /// 품종교체점검Point정보등록
        /// </summary>
        /// <param name="dtSaveDevice">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDeviceChgPoint(DataTable dtSaveDevice, string strUserIP, string strUserID)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dtSaveDevice.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDeviceChgPointCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["DeviceChgPointCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDeviceChgPointName", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveDevice.Rows[i]["DeviceChgPointName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDeviceChgPointNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveDevice.Rows[i]["DeviceChgPointNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDeviceChgPointNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveDevice.Rows[i]["DeviceChgPointNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParamter, "@i_strCCSFlag", ParameterDirection.Input, SqlDbType.Bit, dtSaveDevice.Rows[i]["CCSFlag"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtSaveDevice.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDeviceChgPoint", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                }
                if (ErrRtn.ErrNum == 0)
                {
                    trans.Commit();
                }
                //결과 값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 품종교체점검Point정보등록 삭제
        /// </summary>
        /// <param name="dtDeleteDevice">삭제할데이터항목</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteDeviceChgPoint(DataTable dtDeleteDevice)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dtDeleteDevice.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    //파라미터저장
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDeleteDevice.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strDeviceChgPointCode", ParameterDirection.Input, SqlDbType.VarChar, dtDeleteDevice.Rows[i]["DeviceChgPointCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDeviceChgPoint", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();

                }
                if (ErrRtn.ErrNum == 0)
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipCCSType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipCCSType : ServicedComponent
    {
        /// <summary>
        /// 저장,삭제시 필요한 컬럼지정
        /// </summary>
        /// <returns>필요컬럼정보</returns>
        public DataTable mfDataSet()
        {
            DataTable dtCCStype = new DataTable();
            try
            {
                dtCCStype.Columns.Add("PlantCode", typeof(string));
                dtCCStype.Columns.Add("EquipCCSTypeCode", typeof(string));
                dtCCStype.Columns.Add("EquipCCSTypeName", typeof(string));
                dtCCStype.Columns.Add("EquipCCSTypeNameCh", typeof(string));
                dtCCStype.Columns.Add("EquipCCSTypeNameEn", typeof(string));
                dtCCStype.Columns.Add("CCSFlag", typeof(bool));
                dtCCStype.Columns.Add("UseFlag", typeof(string));

                return dtCCStype;
            }
            catch (Exception ex)
            {
                return dtCCStype;
                throw (ex);
            }
            finally
            {
                dtCCStype.Dispose();
            }
        }
        /// <summary>
        /// 설비변경점유형정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipCCSType(string strPlantCode, string strLang)
        {
            DataTable dtEquipCCSType = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저호출
                dtEquipCCSType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipCCSType", dtParam);
                //정보리턴
                return dtEquipCCSType;
            }
            catch (Exception ex)
            {
                return dtEquipCCSType;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipCCSType.Dispose();
            }
        }
        /// <summary>
        /// 설비변경점유형정보 저장
        /// </summary>
        /// <param name="dtEquipCCStype">저장할정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveEquipCCStype(DataTable dtEquipCCStype, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtEquipCCStype.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    //파라미터에 값넣기
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipCCStype.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCCSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipCCStype.Rows[i]["EquipCCSTypeCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCCSTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipCCStype.Rows[i]["EquipCCSTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCCSTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipCCStype.Rows[i]["EquipCCSTypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCCSTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtEquipCCStype.Rows[i]["EquipCCSTypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSFlag", ParameterDirection.Input, SqlDbType.Bit, dtEquipCCStype.Rows[i]["CCSFlag"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtEquipCCStype.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipCCSType", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();

                }
                if (ErrRtn.ErrNum == 0)
                {
                    trans.Commit();
                }
                //처리결과값리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 설비변경점유형정보 삭제
        /// </summary>
        /// <param name="dtEquipCCSType">삭제할정보</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeleteEquipCCSType(DataTable dtEquipCCSType)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtEquipCCSType.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    //파라미터값넣기
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipCCSType.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCCSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipCCSType.Rows[i]["EquipCCSTypeCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipCCSType", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                }
                if (ErrRtn.ErrNum == 0)
                {
                    trans.Commit();
                }
                //처리결과값리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 설비변경점유형 콤보박스
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비변경점유형정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipCCSTypeCombo(string strPlantCode, string strLang)
        {
            DataTable dtEquipCCSType = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                // 파라미터 값 저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 실행 //
                dtEquipCCSType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipCCSTypeCombo", dtParam);

                //설비변경점유형 정보
                return dtEquipCCSType;
            }
            catch (Exception ex)
            {
                return dtEquipCCSType;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipCCSType.Dispose();
            }
        }

        /// <summary>
        /// 설비변경점유형 CCS대상여부 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCCSTypeCode">설비변경점유형코드</param>
        /// <returns>CCS대상여부</returns>
        [AutoComplete]
        public DataTable mfReadEquipCCSTypeCCSFlag(string strPlantCode, string strEquipCCSTypeCode)
        {
            SQLS sql = new SQLS();
            DataTable dtCCSFlag = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                // 파라미터값 저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCCSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCCSTypeCode, 5);

                // 프로시저 실행 //
                dtCCSFlag = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipCCSTypeCCSFlag", dtParam);

                // CCS대상여부 리턴//
                return dtCCSFlag;
            }
            catch (Exception ex)
            {
                return dtCCSFlag;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtCCSFlag.Dispose();
            }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DeviceChgPMH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DeviceChgPMH : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataTable mfSetDatainfo()
        {
            DataTable dtDeviceChgPMH = new DataTable();

            try
            {
                dtDeviceChgPMH.Columns.Add("StdNumber", typeof(String));
                dtDeviceChgPMH.Columns.Add("VersionNum", typeof(String));
                dtDeviceChgPMH.Columns.Add("PlantCode", typeof(String));
                dtDeviceChgPMH.Columns.Add("PlantName", typeof(String));
                dtDeviceChgPMH.Columns.Add("EquipTypeCode", typeof(String));
                dtDeviceChgPMH.Columns.Add("WriteID", typeof(String));
                dtDeviceChgPMH.Columns.Add("WriteDate", typeof(String));
                dtDeviceChgPMH.Columns.Add("AdmitID", typeof(String));

                dtDeviceChgPMH.Columns.Add("AdmitDate", typeof(String));
                dtDeviceChgPMH.Columns.Add("EtcDesc", typeof(String));
                dtDeviceChgPMH.Columns.Add("RejectReason", typeof(String));
                dtDeviceChgPMH.Columns.Add("RevisionReason", typeof(String));
                dtDeviceChgPMH.Columns.Add("AdmitStatusCode", typeof(String));
                dtDeviceChgPMH.Columns.Add("RevisionType", typeof(String));

                dtDeviceChgPMH.Columns.Add("UserIP", typeof(String));
                dtDeviceChgPMH.Columns.Add("UserID", typeof(String));
                dtDeviceChgPMH.Columns.Add("StatioinName", typeof(String));
                dtDeviceChgPMH.Columns.Add("EquipGroupName", typeof(String));

                return dtDeviceChgPMH;
            }
            catch (Exception ex)
            {
                return dtDeviceChgPMH;
                throw (ex);
            }
            finally
            {
                dtDeviceChgPMH.Dispose();
            }
        }

        /// <summary>
        /// 품종교체정보에 등록되어있지 않는 설비점검그룹 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검그룹</returns>
        [AutoComplete]
        public DataTable mfReadEquipGroup(string strPlantCode, string strLang)
        {
            DataTable dtReadDevice = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저 호출
                dtReadDevice = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipGroup_DeviceChgPMH_Combo", dtParmter);
                //정보리턴
                return dtReadDevice;
            }
            catch (Exception ex)
            {
                return dtReadDevice;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadDevice.Dispose();
            }
        }


        /// <summary>
        /// 품종교체정보에 등록되어있는 품종교체정보 상세조회(최종버젼만 조회)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검그룹</returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgPMHCheck(string strPlantCode, string strEquipTypeCode, string strLang)
        {
            DataTable dtReadDevice = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipTypeCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                //프로시저 호출
                dtReadDevice = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_DeviceChgPMHCheck", dtParmter);
                //정보리턴
                return dtReadDevice;
            }
            catch (Exception ex)
            {
                return dtReadDevice;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadDevice.Dispose();
            }
        }



        /// <summary>
        /// 품종교체정보에 등록되어있는 헤더정보 조회 리스트
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAdmitStatusCode">승인상태코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>품종교체정보</returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgPMHList(string strPlantCode, string strAdmitStatusCode, string strLang)
        {
            DataTable dtReadDevice = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, strAdmitStatusCode, 2);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadDevice = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDeviceChgPMH", dtParmter);
                //정보리턴
                return dtReadDevice;
            }
            catch (Exception ex)
            {
                return dtReadDevice;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadDevice.Dispose();
            }
        }



        /// <summary>
        /// 품종교체정보에 등록되어있는 헤더 상세정보 조회 리스트
        /// </summary>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="strVersionNum">개정번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>품종교체정보</returns>   
        [AutoComplete]
        public DataTable mfReadDeviceChgPMHDetail(string strStdNumber, string strVersionNum, string strLang)
        {
            DataTable dtReadDevice = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈 
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, strVersionNum);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadDevice = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDeviceChgPMH_Detail", dtParmter);
                //정보리턴
                return dtReadDevice;
            }
            catch (Exception ex)
            {
                return dtReadDevice;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadDevice.Dispose();
            }
        }



        /// <summary>
        /// 품종교체점검정보등록
        /// </summary>
        /// <param name="dtSaveDevice">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDeviceChgPMH(DataTable dtSaveDevice, DataTable dtSaveDeviceD, string strUserIP, string strUserID)
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            string strStdNumber = "";
            int intVersionNum = 0;
            string strVersionNum = "";

            try
            {


                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                QRPMAS.BL.MASEQU.DeviceChgPMD clsDeviceChgPMD = new DeviceChgPMD();

                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSaveDevice.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["StdNumber"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSaveDevice.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["EquipTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["AdmitID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["AdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRejectReason", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["RejectReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRevisionReason", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["RevisionReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParamter, "@i_strAdmitStatusCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["AdmitStatusCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParamter, "@i_strRevisionType", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["RevisionType"].ToString(), 20);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 15);

                    sql.mfAddParamDataRow(dtParamter, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParamter, "@o_intVersionNum", ParameterDirection.Output, SqlDbType.Int);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDeviceChgPMH", dtParamter);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //1
                    // 프로시져내의 리턴값을 받아옴
                    strStdNumber = ErrRtn.mfGetReturnValue(0);
                    //intVersionNum = Convert.ToInt32(ErrRtn.mfGetReturnValue(1));
                    strVersionNum = ErrRtn.mfGetReturnValue(1);

                    //2
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    //승인 및 반려는 헤더만 갱신한다.
                    if (dtSaveDevice.Rows[i]["AdmitStatusCode"].ToString() != "FN" && dtSaveDevice.Rows[i]["AdmitStatusCode"].ToString() != "RE")
                    {
                        // 아이템 테이블 삭제
                        strErrRtn = clsDeviceChgPMD.mfDeleteDeviceChgPMD(strStdNumber, strVersionNum, strUserIP, strUserID, sql, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }


                        // 저장할 데이터가 있고 성공시 저장Method 실행
                        if (dtSaveDeviceD.Rows.Count > 0)
                        {

                            // 아이템 테이블 저장
                            strErrRtn = clsDeviceChgPMD.mfSaveDeviceChgPMD(strStdNumber, strVersionNum, dtSaveDeviceD, strUserIP, strUserID, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                    //3
                    ////if (ErrRtn.ErrNum != 0)
                    ////{
                    ////    trans.Rollback();
                    ////    return strErrRtn;
                    ////}



                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }

            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DeviceChgPMD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class DeviceChgPMD : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        ///

        public DataTable mfSetDatainfo()
        {
            DataTable dtDeviceChgPMD = new DataTable();

            try
            {
                dtDeviceChgPMD.Columns.Add("StdNumber", typeof(String));
                dtDeviceChgPMD.Columns.Add("VersionNum", typeof(String));
                dtDeviceChgPMD.Columns.Add("Seq", typeof(String));
                dtDeviceChgPMD.Columns.Add("PMInspectGubun", typeof(String));
                dtDeviceChgPMD.Columns.Add("PMInspectName", typeof(String));
                dtDeviceChgPMD.Columns.Add("PMInspectCriteria", typeof(String));
                dtDeviceChgPMD.Columns.Add("PMPeriodCode", typeof(String));
                dtDeviceChgPMD.Columns.Add("LevelCode", typeof(String));
                dtDeviceChgPMD.Columns.Add("UnitDesc", typeof(String));
                dtDeviceChgPMD.Columns.Add("UserIP", typeof(String));
                dtDeviceChgPMD.Columns.Add("UserID", typeof(String));

                return dtDeviceChgPMD;
            }
            catch (Exception ex)
            {
                return dtDeviceChgPMD;
                throw (ex);
            }
            finally
            {
                dtDeviceChgPMD.Dispose();
            }
        }



        /// <summary>
        /// 품종교체점검정보등록 아이템테이블
        /// </summary>
        /// <param name="dtSaveDevice">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveDeviceChgPMD(string strStdNumber
            , string strVersionNum
            , DataTable dtSaveDevice
            , string strUserIP
            , string strUserID
            , SQLS sql
            , SqlTransaction trans)
        {
            string strErrRtn = "";

            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {

                for (int i = 0; i < dtSaveDevice.Rows.Count; i++)
                {
                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParamter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, strVersionNum);
                    sql.mfAddParamDataRow(dtParamter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSaveDevice.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strPMInspectGubun", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveDevice.Rows[i]["PMInspectGubun"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPMInspectName", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveDevice.Rows[i]["PMInspectName"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strPMInspectCriteria", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveDevice.Rows[i]["PMInspectCriteria"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLevelCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveDevice.Rows[i]["LevelCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASDeviceChgPMD", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }

                }
                //결과 값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 품종교체점검정보등록 아이템테이블 삭제
        /// </summary>
        /// <param name="strUserIP"> IP </param>
        /// <param name="strUserID"> ID </param>
        /// <param name="dtSaveCusP"> 저장할 정보가 담긴 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteDeviceChgPMD(String strStdNumber, string strVersionNum, String strUserIP, String strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 10);
                sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, strVersionNum);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASDeviceChgPMD", dtParam);
                // 결과값 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {

            }
        }

        /// <summary>
        /// 품종교체정보 아이템테이블 조회
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비점검그룹</returns>
        [AutoComplete]
        public DataTable mfReadDeviceChgPMD(String strStdNumber, String strVersionNum, String strLang)
        {
            DataTable dtReadDeviceChgPMD = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, strVersionNum);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadDeviceChgPMD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASDeviceChgPMD", dtParmter);
                //정보리턴
                return dtReadDeviceChgPMD;
            }
            catch (Exception ex)
            {
                return dtReadDeviceChgPMD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadDeviceChgPMD.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipDurableBOM")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class EquipDurableBOM : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>

        public DataTable mfSetDatainfo()
        {
            DataTable dtEquipDurableBOM = new DataTable();

            try
            {
                dtEquipDurableBOM.Columns.Add("PlantCode", typeof(String));
                dtEquipDurableBOM.Columns.Add("EquipCode", typeof(String));
                dtEquipDurableBOM.Columns.Add("Seq", typeof(String));
                dtEquipDurableBOM.Columns.Add("DurableMatCode", typeof(String));
                dtEquipDurableBOM.Columns.Add("LotNo", typeof(String));
                dtEquipDurableBOM.Columns.Add("InputQty", typeof(String));
                dtEquipDurableBOM.Columns.Add("UnitCode", typeof(String));
                dtEquipDurableBOM.Columns.Add("EtcDesc", typeof(String));

                dtEquipDurableBOM.Columns.Add("UserIP", typeof(String));
                dtEquipDurableBOM.Columns.Add("UserID", typeof(String));

                return dtEquipDurableBOM;
            }
            catch (Exception ex)
            {
                return dtEquipDurableBOM;
                throw (ex);
            }
            finally
            {
                dtEquipDurableBOM.Dispose();
            }
        }

        /// <summary>
        /// 금형치공구 수량-StandbyQty
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>수량정보및간단한정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableBOM_TotalQty(string strPlantCode, string strEquipCode, string strDurableMatCode, string strLotNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableBOM = new DataTable();

            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtDurableBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOM_TotalQty", dtParmter);
                //정보리턴
                return dtDurableBOM;
            }
            catch (Exception ex)
            {
                return dtDurableBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtDurableBOM.Dispose();
            }

        }


        /// <summary>
        /// 설비별 설비구성품 BOM 정보 조회 : Limit_1 보다 샷수가 큰 치공구 정보만 조회함.
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비별 설비구성품 BOM 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipDurableBOM_LimitOver(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOM_LimitOver", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }


        /// <summary> 
        /// 설비구성품 BOM 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEquipDurableBOM_EquipSTBOM(String strPlantCode, String strEquipCode, String strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOM_MASEquipSPBOM", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }

        /// <summary>
        /// 설비별 설비구성품 BOM 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비별 설비구성품 BOM 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipDurableBOMCombo(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOMCombo", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }

        /// <summary>
        /// 설비별 설비구성품 BOM 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비별 설비구성품 BOM 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipDurableBOMCombo_Lot(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOMCombo_Lot", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }


        /// <summary>
        /// 설비별 설비구성품 BOM 정보 조회(Lot정보만 조회함)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비별 설비구성품 BOM 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipDurableBOMCombo_OnlyLot(string strPlantCode, string strEquipCode, string strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOMCombo_OnlyLot", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }


        /// <summary>
        /// MASEquipDurableBOM 상세정보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strDurableMatCode">금형/치공구코드</param>
        /// <param name="strLang"></param>
        /// <returns>금형/치공구 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipDurableBOM_DurableMatInputQty(string strPlantCode, string strEquipCode, string strDurableMatCode, string strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOM_DurableMatInputQty", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }


        /// <summary>
        /// MASEquipDurableBOM 상세정보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strDurableMatCode">금형/치공구코드</param>
        /// <param name="strLang"></param>
        /// <returns>금형/치공구 상세정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipDurableBOM_Detail(string strPlantCode, string strEquipCode, string strDurableMatCode, string strLotNo, string strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, strDurableMatCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOM_Detail", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }
        /// <summary>
        /// 설비구성품(금형치공구) 테이블 저장
        /// </summary>
        /// <param name="dtEquipDurableBOM">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveEquipDurableBOM(DataTable dtEquipDurableBOM, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtEquipDurableBOM.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDurableBOM.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDurableBOM.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipDurableBOM.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDurableBOM.Rows[i]["DurableMatCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDurableBOM.Rows[i]["LotNo"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParamter, "@i_intInputQty", ParameterDirection.Input, SqlDbType.Int, dtEquipDurableBOM.Rows[i]["InputQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDurableBOM.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtEquipDurableBOM.Rows[i]["EtcDesc"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipDurableBOM", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 설비구성품(금형치공구) 테이블 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strEquipCode"> 설비코드 </param
        /// <param name="strUserIP"> IP </param>
        /// <param name="strUserID"> ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteEquipDurableBOM(String strPlantCode, string strEquipCode, String strUserIP, String strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipDurableBOM", dtParam);
                // 결과값 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {

            }
        }


        /// <summary>
        /// 구성품에해당하는 설비조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDurableMatCode">치공구코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비정보</returns>
        [AutoComplete]
        public DataTable mfReadDurableBOM_Equip(string strPlantCode,string strDurableMatCode,string strLotNo,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDurableEquipBOM = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDurableMatCode", ParameterDirection.Input, SqlDbType.VarChar,strDurableMatCode,20);
                sql.mfAddParamDataRow(dtParam,"@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar,strLotNo,40);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                dtDurableEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipDurableBOM_Equip", dtParam);

                return dtDurableEquipBOM;
            }
            catch (Exception ex)
            {
                return dtDurableEquipBOM;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDurableEquipBOM.Dispose();
                sql.Dispose();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipSPBOM")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class EquipSPBOM : ServicedComponent
    {
        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>

        public DataTable mfSetDatainfo()
        {
            DataTable dtEquipSPBOM = new DataTable();

            try
            {
                dtEquipSPBOM.Columns.Add("PlantCode", typeof(String));
                dtEquipSPBOM.Columns.Add("EquipCode", typeof(String));
                dtEquipSPBOM.Columns.Add("Seq", typeof(String));
                dtEquipSPBOM.Columns.Add("SparePartCode", typeof(String));
                dtEquipSPBOM.Columns.Add("InputQty", typeof(String));
                dtEquipSPBOM.Columns.Add("UnitCode", typeof(String));
                dtEquipSPBOM.Columns.Add("EtcDesc", typeof(String));

                dtEquipSPBOM.Columns.Add("UserIP", typeof(String));
                dtEquipSPBOM.Columns.Add("UserID", typeof(String));

                return dtEquipSPBOM;
            }
            catch (Exception ex)
            {
                return dtEquipSPBOM;
                throw (ex);
            }
            finally
            {
                dtEquipSPBOM.Dispose();
            }
        }


        /// <summary>
        /// 설비구성품(SparePart) 테이블 저장
        /// </summary>
        /// <param name="dtEquipDurableBOM">저장할데이터항목</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfSaveEquipSPBOM(DataTable dtEquipSPBOM, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtEquipSPBOM.Rows.Count; i++)
                {

                    DataTable dtParamter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParamter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParamter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipSPBOM.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipSPBOM.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtEquipSPBOM.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipSPBOM.Rows[i]["SparePartCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_intInputQty", ParameterDirection.Input, SqlDbType.Int, dtEquipSPBOM.Rows[i]["InputQty"].ToString());
                    sql.mfAddParamDataRow(dtParamter, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipSPBOM.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParamter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtEquipSPBOM.Rows[i]["EtcDesc"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParamter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipSPBOM", dtParamter);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                //결과 값 리턴
                return strErrRtn;
            }


            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 설비구성품(SparePart) 테이블 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strEquipCode"> 설비코드 </param>
        /// <param name="strUserIP"> IP </param>
        /// <param name="strUserID"> ID </param>

        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteEquipSPBOM(String strPlantCode, string strEquipCode, String strUserIP, String strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipSPBOM", dtParam);
                // 결과값 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    return strErrRtn;
                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {

            }


        }



        /// <summary>
        /// 설비구성품 BOM 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEquipSTBOM(String strPlantCode, String strEquipCode, String strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipSPBOM", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }

        /// <summary>
        /// 설비구성품 BOM 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEquipSTBOMCombo(String strPlantCode, String strEquipCode, String strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipSPBOMCombo", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }

        /// <summary>
        /// 설비구성품 BOM 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>설비구성품 BOM</returns>
        [AutoComplete]
        public DataTable mfReadEquipSTBOMDetail(String strPlantCode, String strEquipCode, String strSparePartCode, String strLang)
        {
            DataTable dtReadEquipBOM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParmter = sql.mfSetParamDataTable();
                //파라미터 저장
                sql.mfAddParamDataRow(dtParmter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParmter, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strSparePartCode", ParameterDirection.Input, SqlDbType.VarChar, strSparePartCode, 20);
                sql.mfAddParamDataRow(dtParmter, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //프로시저 호출
                dtReadEquipBOM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipSPBOM_Detail", dtParmter);
                //정보리턴
                return dtReadEquipBOM;
            }
            catch (Exception ex)
            {
                return dtReadEquipBOM;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtReadEquipBOM.Dispose();
            }
        }
    }


    #region SparePart 반입 구분 정보 등록
    /* 
     * SparePart 반입 구분 정보 등록
     * 
     * 2011.08.26 서정현
     */

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipCarryInGubun")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipCarryInGubun : ServicedComponent
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtMASEquipCarryInGubun = new DataTable();

            try
            {
                dtMASEquipCarryInGubun.Columns.Add("PlantCode", typeof(String));
                dtMASEquipCarryInGubun.Columns.Add("EquipCarryInGubunCode", typeof(String));
                dtMASEquipCarryInGubun.Columns.Add("EquipCarryInGubunName", typeof(String));
                dtMASEquipCarryInGubun.Columns.Add("EquipCarryInGubunNameCh", typeof(String));
                dtMASEquipCarryInGubun.Columns.Add("EquipCarryInGubunNameEn", typeof(String));
                dtMASEquipCarryInGubun.Columns.Add("UseFlag", typeof(Char));

                return dtMASEquipCarryInGubun;
            }
            catch (Exception ex)
            {
                return dtMASEquipCarryInGubun;
                throw (ex);
            }
            finally
            {
                dtMASEquipCarryInGubun.Dispose();
            }
        }

        /// <summary>
        /// 반입구분정보 콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>반입구분정보</returns>
        [AutoComplete]
        public DataTable mfReadMASEquipCarryInGubunCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMASEquipCarryInGubun = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저호출
                dtMASEquipCarryInGubun = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipCarryInGubunCombo", dtParam);
                //정보리턴
                return dtMASEquipCarryInGubun;
            }
            catch (Exception ex)
            {
                return dtMASEquipCarryInGubun;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMASEquipCarryInGubun.Dispose();
            }
        }

        /// <summary>
        /// SparePart 반입 구분 정보등록
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart 반입 구분 정보등록</returns>

        [AutoComplete]
        public DataTable mfReadMASEquipCarryInGubun(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMASEquipCarryInGubun = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저호출
                dtMASEquipCarryInGubun = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipCarryInGubun", dtParam);
                //정보리턴
                return dtMASEquipCarryInGubun;
            }
            catch (Exception ex)
            {
                return dtMASEquipCarryInGubun;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMASEquipCarryInGubun.Dispose();
            }
        }

        /// <summary>
        /// SparePart 창고 반입 구분 정보 저장 
        /// </summary>
        /// <param name="dtMASEquipCarryInGubun">저장할데이터</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>저장</returns>
        [AutoComplete(false)]
        public string mfSaveMASEquipCarryInGubun(DataTable dtMASEquipCarryInGubun, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtMASEquipCarryInGubun.Rows.Count; i++)
                {


                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryInGubun.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMASEquipCarryInGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryInGubun.Rows[i]["EquipCarryInGubunCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strMASEquipCarryInGubunName", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryInGubun.Rows[i]["EquipCarryInGubunName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMASEquipCarryInGubunNameCh", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryInGubun.Rows[i]["EquipCarryInGubunNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMASEquipCarryInGubunNameEn", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryInGubun.Rows[i]["EquipCarryInGubunNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtMASEquipCarryInGubun.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipCarryInGubun", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// SparePart 창고 반입 구분 정보 삭제
        /// </summary>
        /// <param name="dtMASPMResult">정보가들어있는데이터</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>삭제</returns>
        [AutoComplete(false)]
        public string mfDeleteMASEquipCarryInGubun(DataTable dtMASEquipCarryInGubun)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtMASEquipCarryInGubun.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryInGubun.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCarryInGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryInGubun.Rows[i]["EquipCarryInGubunCode"].ToString(), 5);
                    // PK(PlantCode + MASEquipCarryInGubunCode) 삭제 조건  
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipCarryInGubun", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }
    #endregion


    #region SparePart 반출 구분 정보 등록 관련
    /* 
     * SparePart 반출 구분 정보 등록
     * 
     * 
     */

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipCarryOutGubun")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipCarryOutGubun : ServicedComponent
    {
        // private SqlConnection m_SqlConnDebug; 
        /// <summary>
        /// Debug모드용 생성자
        /// </summary>
        /// <param name="strDBConn"></param>
        ///

        // public EquipCarryOutGubun(string strDBConn)
        //{
        //    m_SqlConnDebug = new SqlConnection(strDBConn);
        //    m_SqlConnDebug.Open();
        //}

        public EquipCarryOutGubun()
        {

        }

        public DataTable mfSetDataInfo()
        {
            DataTable dtMASEquipCarryOutGubun = new DataTable();

            try
            {
                dtMASEquipCarryOutGubun.Columns.Add("PlantCode", typeof(String));
                dtMASEquipCarryOutGubun.Columns.Add("EquipCarryOutGubunCode", typeof(String));
                dtMASEquipCarryOutGubun.Columns.Add("EquipCarryOutGubunName", typeof(String));
                dtMASEquipCarryOutGubun.Columns.Add("EquipCarryOutGubunNameCh", typeof(String));
                dtMASEquipCarryOutGubun.Columns.Add("EquipCarryOutGubunNameEn", typeof(String));
                dtMASEquipCarryOutGubun.Columns.Add("UseFlag", typeof(Char));

                return dtMASEquipCarryOutGubun;
            }
            catch (Exception ex)
            {
                return dtMASEquipCarryOutGubun;
                throw (ex);
            }
            finally
            {
                dtMASEquipCarryOutGubun.Dispose();
            }
        }


        /// <summary>
        /// 반출구분정보 콤보
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>반출구분정보</returns>
        [AutoComplete]
        public DataTable mfReadMASEquipCarryOutGubunCombo(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();


            DataTable dtMASEquipCarryOutGubun = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저호출
                dtMASEquipCarryOutGubun = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipCarryOutGubunCombo", dtParam);

                //정보리턴
                return dtMASEquipCarryOutGubun;
            }
            catch (Exception ex)
            {
                return dtMASEquipCarryOutGubun;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMASEquipCarryOutGubun.Dispose();
            }
        }

        /// <summary>
        /// SparePart 반출 구분 정보등록 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>SparePart 반출 구분 정보등록</returns>

        [AutoComplete]
        public DataTable mfReadMASEquipCarryOutGubun(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            // SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());

            DataTable dtMASEquipCarryOutGubun = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저호출
                // dtMASEquipCarryOutGubun = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipCarryOutGubun", dtParam);
                dtMASEquipCarryOutGubun = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipCarryOutGubun", dtParam);
                //정보리턴
                return dtMASEquipCarryOutGubun;
            }
            catch (Exception ex)
            {
                return dtMASEquipCarryOutGubun;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMASEquipCarryOutGubun.Dispose();
            }
        }


        /// <summary>
        /// SparePart 창고 반입 구분 정보 저장 
        /// </summary>
        /// <param name="dtMASEquipCarryOutGubun">저장할데이터</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>저장</returns>
        [AutoComplete(false)]
        public string mfSaveMASEquipCarryOutGubun(DataTable dtMASEquipCarryOutGubun, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString);  // 디버깅용 연결 

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtMASEquipCarryOutGubun.Rows.Count; i++)
                {

                    //trans = m_SqlConnDebug.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryOutGubun.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMASEquipCarryOutGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryOutGubun.Rows[i]["EquipCarryOutGubunCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strMASEquipCarryOutGubunName", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryOutGubun.Rows[i]["EquipCarryOutGubunName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMASEquipCarryOutGubunNameCh", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryOutGubun.Rows[i]["EquipCarryOutGubunNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMASEquipCarryOutGubunNameEn", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryOutGubun.Rows[i]["EquipCarryOutGubunNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtMASEquipCarryOutGubun.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipCarryOutGubun", dtParam);           //실행용
                    // strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_MASEquipCarryOutGubun", dtParam);    //디버깅용
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// SparePart 창고 반출 구분 정보 삭제
        /// </summary>
        /// <param name="dtMASEquipCarryOutGubun">정보가들어있는데이터</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns>삭제</returns>
        [AutoComplete(false)]
        public string mfDeleteMASEquipCarryOutGubun(DataTable dtMASEquipCarryOutGubun)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtMASEquipCarryOutGubun.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryOutGubun.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCarryOutGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtMASEquipCarryOutGubun.Rows[i]["EquipCarryOutGubunCode"].ToString(), 5);
                    // PK(PlantCode + MASEquipCarryOutGubunCode) 삭제 조건  
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저호출
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipCarryOutGubun", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }
    #endregion


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("EquipWorker")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class EquipWorker : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public EquipWorker()
        {

        }

        public EquipWorker(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 정비사별 설비리스트컬럼
        /// </summary>
        /// <returns>컬럼설정한 빈테이블</returns>
        [AutoComplete]
        public DataTable mfSetDateInfo()
        {
            DataTable dtColumns = new DataTable();
            try
            {

                dtColumns.Columns.Add("PlantCode", typeof(String));
                dtColumns.Columns.Add("EquipCode", typeof(String));
                dtColumns.Columns.Add("UserID", typeof(String));
                dtColumns.Columns.Add("AdminRight", typeof(String));

                //정보리턴
                return dtColumns;

            }
            catch (Exception ex)
            {
                return dtColumns;
                throw (ex);
            }
            finally
            {
                dtColumns.Dispose();
            }
        }


        /// <summary>
        /// 설비정비사조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipWorker_All(string strPlantCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipWorker = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //설비정비사조회 프로시저 실행
                dtEquipWorker = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipWorker_All", dtParam);

                return dtEquipWorker;
            }
            catch (Exception ex)
            {
                return dtEquipWorker;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipWorker.Dispose();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 정비사별 설비정보 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipWorker(string strPlantCode, string strUserID, string strLang)
        {
            SQLS sql = new SQLS();      //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtEquipGroup = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();        //실행용

                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저호출
                dtEquipGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipWorker", dtParam);   //실행용
                //dtEquipGroup = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipWorker", dtParam);   //디버깅용
                //정보리턴
                return dtEquipGroup;
            }
            catch (Exception ex)
            {
                return dtEquipGroup;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipGroup.Dispose();
            }
        }

        /// <summary>
        /// 정비사별 설비(부)관리자 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strUserID">정비사아이디</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>해당 정비사의 설비(부)관리자 정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipWorker_Sub(string strPlantCode, string strUserID,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtEquipWorkerSub = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();


                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtEquipWorkerSub = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipWorker_Sub", dtParam);

                return dtEquipWorkerSub;
            }
            catch (Exception ex)
            {
                return dtEquipWorkerSub;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipWorkerSub.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 정비사 그리드 콤보
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadEquipWorker_Combo(string strPlantCode, string strEquipCode, string strLang)
        {
            SQLS sql = new SQLS();      //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtEquipGroup = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();        //실행용

                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저호출
                dtEquipGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipWorker_Combo", dtParam);   //실행용
                //dtEquipGroup = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASEquipWorker", dtParam);   //디버깅용
                //정보리턴
                return dtEquipGroup;
            }
            catch (Exception ex)
            {
                return dtEquipGroup;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtEquipGroup.Dispose();
            }
        }

        /// <summary>
        /// 정비사현황정보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroupCode">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="strEquipGroupCode">설비그룹</param>
        /// <param name="strTargetCode"></param>
        /// <returns>정비사현황정보</returns>
        [AutoComplete]
        public DataTable mfReadEquipWorker_CurrentStatus(
                                                            string strPlantCode,
                                                            string strStationCode,
                                                            string strEquipLocCode,
                                                            string strProcessGroupCode,
                                                            string strEquipLargeTypeCode,
                                                            string strEquipGroupCode,
                                                            string strTargetCode
                                                        )
        {
            DataTable dtEquipWorker = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStationCode", ParameterDirection.Input, SqlDbType.VarChar, strStationCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_StrEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLargeTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLargeTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strTargetCode", ParameterDirection.Input, SqlDbType.VarChar, strTargetCode, 20);

                //정비사현황 조회 프로시저 실행
                dtEquipWorker = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASEquipWorker_CurrentStatus", dtParam);


                // 정비사현황정보
                return dtEquipWorker;
            }
            catch (Exception ex)
            {
                return dtEquipWorker;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtEquipWorker.Dispose();
                sql.Dispose();
            }

        }


        /// <summary>
        /// 정비사별 설비정보 저장
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipWorkID"></param>
        /// <param name="dtEquipWorkerList"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveEquipWorker(string strPlantCode, string strEquipWorkID, DataTable dtEquipWorkerList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();      //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();  //실행용
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();    //실행용
                //trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

                DataTable dtPameter = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtPameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtPameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtPameter, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, strEquipWorkID, 20);

                sql.mfAddParamDataRow(dtPameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipWorker", dtPameter);    //실행용
                //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Delete_MASEquipWorker", dtPameter);    //디버깅용

                //결과값확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    for (int i = 0; i < dtEquipWorkerList.Rows.Count; i++)
                    {
                        DataTable dtPameters = sql.mfSetParamDataTable();
                        //파라미터값 저장
                        sql.mfAddParamDataRow(dtPameters, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                        
                        sql.mfAddParamDataRow(dtPameters, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtPameters, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtEquipWorkerList.Rows[i]["EquipCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtPameters, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, dtEquipWorkerList.Rows[i]["UserID"].ToString(), 20);
                        sql.mfAddParamDataRow(dtPameters, "@i_strAdminRight", ParameterDirection.Input, SqlDbType.VarChar, dtEquipWorkerList.Rows[i]["AdminRight"].ToString(), 1);
                        
                        sql.mfAddParamDataRow(dtPameters, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtPameters, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 10);
                        sql.mfAddParamDataRow(dtPameters, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        //프로시저 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASEquipWorker", dtPameters);    //실행용
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_MASEquipWorker", dtPameters);    //디버깅용

                        //결과값확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                //결과값리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipWorkID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteEquipWorker(string strPlantCode, string strEquipWorkID)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비오픈
                sql.mfConnect();    //실행용
                SqlTransaction trans;

                trans = sql.SqlCon.BeginTransaction();   //실행용
                //trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

                DataTable dtPameter = sql.mfSetParamDataTable();
                //파라미터값 저장
                sql.mfAddParamDataRow(dtPameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtPameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtPameter, "@i_strEquipWorkID", ParameterDirection.Input, SqlDbType.VarChar, strEquipWorkID, 20);
                sql.mfAddParamDataRow(dtPameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASEquipWorker", dtPameter);   //실행용
                //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Delete_MASEquipWorker", dtPameter);   //디버깅용

                //결과값확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                //결과값리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }
}


