﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : clsMASQUA.cs                                          */
/* 프로그램명   : 품질관리기준정보                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-07-27 : InspectFaultType클래스 추가 (이종호)     */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPMAS.BL.MASQUA
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InspectGroup")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InspectGroup : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public InspectGroup()
        {
        }

        public InspectGroup(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// DataTable Columns 정의
        /// </summary>
        /// <returns> DataTable </returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("InspectGroupCode", typeof(String));
                dt.Columns.Add("InspectGroupName", typeof(String));
                dt.Columns.Add("InspectGroupNameCh", typeof(String));
                dt.Columns.Add("InspectGroupNameEn", typeof(String));
                dt.Columns.Add("UseFlag", typeof(Char));
                dt.Columns.Add("SendMDM", typeof(String));

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// DB로부터 InspectGroup정보를 가져오는 함수
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectGroup(String strPlantCode, String strLang)
        {
            // 변수선언
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                // DB연결
                sql.mfConnect();
                // Parameter 변수로 쓰일 DataTable변수 선언 / 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                // Parameter 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                // SP실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectGroup", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// InspectGroup ComboBox 설정용 함수
        /// </summary>
        /// <param name="strPlantCode"> 공장코드</param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectGroupCombo(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            DataTable dt = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                // Parameter 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                // SP 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectGroupCombo", dtParam);
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASInspectGroupCombo", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 입력내용을 DB에 저장
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMASInspectGroup(DataTable dtGroup, String strUserIP, String strUserID, DataTable dtSaveType, DataTable dtDelType)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;

                for (int i = 0; i < dtGroup.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();
                    // Parameter용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtGroup.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupName", ParameterDirection.Input, SqlDbType.NVarChar, dtGroup.Rows[i]["InspectGroupName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtGroup.Rows[i]["InspectGroupNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtGroup.Rows[i]["InspectGroupNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtGroup.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASInspectGroup", dtParam);
                    // 결과값 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        //trans.Commit();
                        // 성공시 검사유형 처리
                        // 삭제먼저                        
                        if (dtDelType.Rows.Count > 0)
                        {
                            MASQUA.InspectType dType = new InspectType();
                            strErrRtn = dType.mfDeleteMASInspectType(dtDelType, sql.SqlCon, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // 저장
                        if (dtSaveType.Rows.Count > 0)
                        {
                            MASQUA.InspectType sType = new InspectType();
                            strErrRtn = sType.mfSaveMASInspectType(dtSaveType, strUserIP, strUserID, sql, trans); //  sql.SqlCon, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                            //else
                            //{
                            //trans.Commit();
                            //}
                        }
                        //else
                        trans.Commit();

                        if (ErrRtn.ErrNum.Equals(0)
                            && dtGroup.Rows[i]["SendMDM"].ToString().Equals("T"))
                        {
                            //MDM전송
                            string strErr = "";
                            strErr = mfSaveMASInspectGroup_MDM(dtGroup, dtSaveType, dtDelType, sql.SqlCon, strUserIP, strUserID);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErr);
                            if (!ErrRtn.ErrNum.Equals(0))
                                break;
                            else
                            {
                                trans = sql.SqlCon.BeginTransaction();

                                strErr = mfSaveMASInspectGroup_MDMFlag(dtGroup.Rows[i]["PlantCode"].ToString(), dtGroup.Rows[i]["InspectGroupCode"].ToString(), strUserIP, strUserID, sql.SqlCon, trans);

                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErr);

                                if (ErrRtn.ErrNum.Equals(0))
                                    trans.Commit();
                                else
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }
                        }
                    }
                }


                    

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 검사그룹코드 정보 MDM송신
        /// </summary>
        /// <param name="dtInspectGroup">검사그룹코드정보</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASInspectGroup_MDM(DataTable dtInspectGroup,DataTable dtInspectType,DataTable dtInspectTypeDel,SqlConnection sqlcon,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for(int i =0 ; i < dtInspectGroup.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectGroup.Rows[i]["PlantCode"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strInSpectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectGroup.Rows[i]["InspectGroupCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInSpectGroupName", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectGroup.Rows[i]["InspectGroupName"].ToString(), 250);
                    sql.mfAddParamDataRow(dtParam, "@i_strInSpectGroupNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectGroup.Rows[i]["InspectGroupNameCh"].ToString(), 250);
                    sql.mfAddParamDataRow(dtParam, "@i_strInSpectGroupNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectGroup.Rows[i]["InspectGroupNameEn"].ToString(), 250);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtInspectGroup.Rows[i]["UseFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_MASInspectGroup_MDM", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                    else
                    {

                        strErrRtn = mfSaveMASInspectType_MDMUseFlag(dtInspectTypeDel, sqlcon);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0))
                            break;

                        if (dtInspectType.Rows.Count > 0)
                        {
                            strErrRtn = mfSaveMASInspectType_MDM(dtInspectType, strUserIP, strUserID, sqlcon);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0))
                                break;
                        }
                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();

            }
        }


        /// <summary>
        /// 검사유형정보 MDM송신
        /// </summary>
        /// <param name="dtInspectType">검사유형정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASInspectType_MDM(DataTable dtInspectType,string strUserIP,string strUserID ,SqlConnection sqlcon)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";


            try
            {
                for (int i = 0; i < dtInspectType.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 8000);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectType.Rows[i]["PlantCode"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectType.Rows[i]["InspectGroupCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectType.Rows[i]["InspectTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtInspectType.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectType.Rows[i]["InspectTypeName"].ToString(), 250);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectType.Rows[i]["InspectTypeNameCh"].ToString(), 250);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectType.Rows[i]["InspectTypeNameEn"].ToString(), 250);
                    sql.mfAddParamDataRow(dtParam, "@i_strMonitoringFlag", ParameterDirection.Input, SqlDbType.Char, dtInspectType.Rows[i]["MonitoringFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtInspectType.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strBomCheckFlag", ParameterDirection.Input, SqlDbType.Char, dtInspectType.Rows[i]["BomCheckFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_MASInspectType_MDM", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }


                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 검사유형정보 MDM UseFlag F 처리
        /// </summary>
        /// <param name="dtInspectTypeDel">등록할검사그룹정보</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASInspectType_MDMUseFlag(DataTable dtInspectTypeDel,SqlConnection sqlcon)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                
                for (int i = 0; i < dtInspectTypeDel.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectTypeDel.Rows[i]["PlantCode"].ToString(),4);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectTypeDel.Rows[i]["InspectGroupCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectTypeDel.Rows[i]["InspectTypeCode"].ToString(),40);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_MASInspectType_MDMUseFlag", dtParam);

                    if (!ErrRtn.ErrNum.Equals(0))
                        break;

                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 검사그룹정보,검사유형정보가 MDM에 등록이 완료되면 MASInspectGroup의 MDMFlag를 바꿔준다.
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strInspectGroupCode">검사그룹코드</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveMASInspectGroup_MDMFlag(string strPlantCode, string strInspectGroupCode, string strUserIP, string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strInSpectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectGroupCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_MASInspectGroup_MDMFlag", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (!ErrRtn.ErrNum.Equals(0))
                    return strErrRtn;


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }



        /// <summary>
        /// DB에서 정보 삭제
        /// </summary>
        /// <param name="dtGroup"> 삭제할 검사분류정보가 담긴 DataTable </param>
        /// <param name="dtType"> 삭제할 검사유형정보가 담긴 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASInspectGroup(DataTable dtDelete,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {

                    // 검사분류 삭제
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_MASInspectGroup", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }

                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// DB에서 정보 삭제
        /// </summary>
        /// <param name="dtGroup"> 삭제할 검사분류정보가 담긴 DataTable </param>
        /// <param name="dtType"> 삭제할 검사유형정보가 담긴 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASInspectGroup(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();

                strErrRtn = mfDeleteMASInspectGroup_MDM(dtDelete, "F" ,sql.SqlCon);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                {
                    SqlTransaction trans;
                    trans = sql.SqlCon.BeginTransaction();

                    for (int i = 0; i < dtDelete.Rows.Count; i++)
                    {

                        //// 검사유형 정보부터 삭제
                        //InspectType clsInType = new InspectType();
                        //strErrRtn = clsInType.mfDeleteMASInspectTypeALL(dtDelete, sql, trans); // sql.SqlCon, trans);
                        //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        //// 검사유형삭제가 제대로 됐는지 검사
                        //if (ErrRtn.ErrNum != 0)
                        //{
                        //    trans.Rollback();
                        //    break;
                        //}

                        // 검사분류 삭제
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["InspectGroupCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASInspectGroup", dtParam);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                    }

                    if (ErrRtn.ErrNum.Equals(0))
                        trans.Commit();
                    else
                    {
                       mfDeleteMASInspectGroup_MDM(dtDelete, "T", sql.SqlCon);
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 해당검사분류정보 MDM  UseFlag 처리
        /// </summary>
        /// <param name="dtDelete"> 삭제할 검사분류정보가 담긴 DataTable </param>
        /// <param name="strUseFlag">사용여부</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteMASInspectGroup_MDM(DataTable dtDelete, string strUseFlag,SqlConnection sqlcon)
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["InspectGroupCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, strUseFlag, 1);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_MASInspectGroup_MDMUseFlag", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InspectType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InspectType : ServicedComponent
    {
        /// <summary>
        /// DataTable Columns 정의
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("InspectGroupCode", typeof(String));
                dt.Columns.Add("InspectTypeCode", typeof(String));
                dt.Columns.Add("Seq", typeof(int));
                dt.Columns.Add("InspectTypeName", typeof(String));
                dt.Columns.Add("InspectTypeNameCh", typeof(String));
                dt.Columns.Add("InspectTypeNameEn", typeof(String));
                dt.Columns.Add("MonitoringFlag", typeof(String));
                dt.Columns.Add("BomCheckFlag", typeof(String));
                dt.Columns.Add("UseFlag", typeof(Char));

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// 조회함수
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strInspectGroupCode"> 검사분류코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectType(String strPlantCode, String strInspectGroupCode)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectGroupCode, 10);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectType", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 콤보박스 설정함수
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strInspectGroupCode"> 검사분류코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectTypeForCombo(String strPlantCode, String strInspectGroupCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectTypeCombo", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 검사유형 콤보 조회화면(InspectGroupCode = 'IG02' 만 조회)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strInspectGroupCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectTypeForCombo_STA(String strPlantCode, String strInspectGroupCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectTypeCombo_STA", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 저장함수
        /// </summary>
        /// <param name="dt"> 저장정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="sqlCon"> Sql Connection </param>
        /// <param name="trans"> 트랜젝션변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMASInspectType(DataTable dt, String strUserIP, String strUserID, SQLS sql, SqlTransaction trans) //  SqlConnection sqlCon, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["InspectTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["InspectTypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["InspectTypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMonitoringFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MonitoringFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strBomCheckFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["BomCheckFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASInspectType", dtParam);
                    //strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_MASInspectType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    //    trans.Rollback();
                    //else
                    //    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }

        /// <summary>
        /// 삭제함수(저장시)
        /// </summary>
        /// <param name="dt"> 삭제할 정보가 담긴 DataTable </param>
        /// <param name="sqlCon"> SqlConnection </param>
        /// <param name="trans"> 트렌젝션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASInspectType(DataTable dt, SqlConnection sqlCon, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SQLS sql = new SQLS();
                    //trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_MASInspectType", dtParam);
                    //strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASInspectType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    //    trans.Rollback();
                    //else
                    //    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public String mfDeleteMASInspectTypeALL(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_MASInspectType", dtParam);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASInspectTypeALL", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    //    trans.Rollback();
                    //else
                    //    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }

        /// <summary>
        /// BomCheckFlag 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInspectGroupCode">검사분류코드</param>
        /// <param name="strInspectTypeCode">검사유형코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectType_BomChecFlag(string strPlantCode, string strInspectGroupCode, string strInspectTypeCode)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectTypeCode, 10);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectType_BomCheckFlag", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InspectItem")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InspectItem : ServicedComponent
    {
        /// <summary>
        /// DataTable Column 설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtInspectItem = new DataTable();
            try
            {
                dtInspectItem.Columns.Add("PlantCode", typeof(String));
                dtInspectItem.Columns.Add("InspectItemCode", typeof(String));
                dtInspectItem.Columns.Add("InspectItemName", typeof(String));
                dtInspectItem.Columns.Add("InspectItemNameCh", typeof(String));
                dtInspectItem.Columns.Add("InspectItemNameEn", typeof(String));
                dtInspectItem.Columns.Add("InspectGroupCode", typeof(String));
                dtInspectItem.Columns.Add("InspectTypeCode", typeof(String));
                dtInspectItem.Columns.Add("InspectCondition", typeof(String));
                dtInspectItem.Columns.Add("InspectMethod", typeof(String));
                dtInspectItem.Columns.Add("DataTypeCode", typeof(String));
                dtInspectItem.Columns.Add("ComGubunCode", typeof(String));
                dtInspectItem.Columns.Add("UnitCode", typeof(String));
                dtInspectItem.Columns.Add("UseDecimalPoint", typeof(int));
                dtInspectItem.Columns.Add("UseFlag", typeof(Char));
                dtInspectItem.Columns.Add("SendMDM", typeof(Char));

                dtInspectItem.Columns.Add("SPCNFlag", typeof(Char)); //SPCNFlag 2012-12-07 추가

                return dtInspectItem;
            }
            catch (Exception ex)
            {
                return dtInspectItem;
                throw (ex);
            }
            finally
            {
                dtInspectItem.Dispose();
            }
        }

        /// <summary>
        /// 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strInspectGroupCode"> 검사분류코드 </param>
        /// <param name="strInspectTypeCode"> 검사유형코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectItem(String strPlantCode, String strInspectGroupCode, String strInspectTypeCode, String strLang)
        {
            // Instance 선언
            DataTable dtInspectItem = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Parameter Instance 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 테이블에 UI로부터 넘겨받은 인자 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP실행
                dtInspectItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectItem", dtParam);

                return dtInspectItem;
            }
            catch (Exception ex)
            {
                return dtInspectItem;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtInspectItem.Dispose();
            }
        }

        /// <summary>
        /// 조회 Popup Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strInspectGroupCode"> 검사분류코드 </param>
        /// <param name="strInspectTypeCode"> 검사유형코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectItemPopup(String strPlantCode, String strInspectGroupCode, String strInspectTypeCode, String strLang)
        {
            // Instance 선언
            DataTable dtInspectItem = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Parameter Instance 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 테이블에 UI로부터 넘겨받은 인자 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP실행
                dtInspectItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectItemPopup", dtParam);

                return dtInspectItem;
            }
            catch (Exception ex)
            {
                return dtInspectItem;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtInspectItem.Dispose();
            }
        }

        /// <summary>
        /// 검사항목 조회(항목코드까지 검색조건)
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strInspectGroupCode"> 검사분류코드 </param>
        /// <param name="strInspectTypeCode"> 검사유형코드 </param>
        /// <param name="strInspectItemCode"> 검사항목코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        //public DataTable mfReadMASInspectItemDetail(String strPlantCode, String strInspectGroupCode, String strInspectTypeCode, String strInspectItemCode, String strLang)
        public DataTable mfReadMASInspectItemDetail(String strPlantCode, String strInspectItemCode, String strLang)
        {
            // Instance 선언
            DataTable dtInspectItem = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Parameter Instance 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 테이블에 UI로부터 넘겨받은 인자 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP실행
                dtInspectItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectItemDetail", dtParam);

                return dtInspectItem;
            }
            catch (Exception ex)
            {
                return dtInspectItem;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtInspectItem.Dispose();
            }
        }

        /// <summary>
        /// ComboBox용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="InspectGroupCode"> 검사분류코드 </param>
        /// <param name="InspectTypeCode"> 검사유형코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectItemCombo(String strPlantCode, String strInspectGroupCode, String strInspectTypeCode, String strLang)
        {
            // Instance 객체 생성
            DataTable dtInspectItem = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtInspectItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectItemCombo", dtParam);

                return dtInspectItem;
            }
            catch (Exception ex)
            {
                return dtInspectItem;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtInspectItem.Dispose();
            }
        }

        /// <summary>
        /// 통계값 분석 검색 콤보박스용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASInspectItemComboSTA(String strPlantCode, String strLang)
        {
            DataTable dtInspectItem = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtInspectItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectItemComboForSTA", dtParam);
                return dtInspectItem;
            }
            catch (Exception ex)
            {
                return dtInspectItem;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtInspectItem.Dispose();
            }
        }


        /// <summary>
        /// 저장용 Method
        /// </summary>
        /// <param name="dt"> 저장할 값이 들어있는 DataTable </param>
        /// <param name="strUserIP"> UserIP </param>
        /// <param name="strUserID"> UserID </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASInspectItem(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectItemCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["InspectItemName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["InspectItemNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["InspectItemNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectCondition", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["InspectCondition"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectMethod", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["InspectMethod"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strDataTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DataTypeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ComGubunCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intUseDecimalPoint", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["UseDecimalPoint"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["UseFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strSPCNFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["SPCNFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASInspectItem", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                    {
                        trans.Commit();

                        if (dt.Rows[i]["SendMDM"].ToString().Equals("T"))
                        {
                            string strErr = "";
                            strErr = mfSaveInspectItem_MDM(dt, strUserIP, strUserID, sql.SqlCon);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErr);
                            if (!ErrRtn.ErrNum.Equals(0))
                                break;
                        }
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 검사항목코드정보 MDM전송
        /// </summary>
        /// <param name="dtInspectItem">검사항목정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveInspectItem_MDM(DataTable dtInspectItem, string strUserIP, string strUserID,SqlConnection sqlcon)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                for (int i = 0; i < dtInspectItem.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam,"@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,30);

                    sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["PlantCode"].ToString(),4);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectitemCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["InspectItemCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectitemName", ParameterDirection.Input, SqlDbType.NVarChar,dtInspectItem.Rows[i]["InspectItemName"].ToString(),250);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectitemNameCh", ParameterDirection.Input, SqlDbType.NVarChar,dtInspectItem.Rows[i]["InspectItemNameCh"].ToString(),250);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectitemNameEn", ParameterDirection.Input, SqlDbType.NVarChar,dtInspectItem.Rows[i]["InspectItemNameEn"].ToString(),250);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["InspectGroupCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["InspectTypeCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectCondition", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["InspectCondition"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectMethod", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["InspectMethod"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strDataTypeCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["DataTypeCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["ComGubunCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["UnitCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strUseDeciMalPoint", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["UseDecimalPoint"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char,dtInspectItem.Rows[i]["UseFlag"].ToString(),1);

                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon,"up_Update_MASInspectItem_MDM",dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if(!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);

            }
            finally
            {
                sql.Dispose();
            }
        }



        /// <summary>
        /// 삭제용 Method
        /// </summary>
        /// <param name="dt"> 삭제할 정보가 들어있는 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASInspectItem(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();

                strErrRtn = mfSaveInspectItem_MDMUseFlag(dt, "F", sql.SqlCon);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                {

                    SqlTransaction trans;

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        trans = sql.SqlCon.BeginTransaction();
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectGroupCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectTypeCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectItemCode"].ToString(), 20);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASInspectItem", dtParam);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                            trans.Rollback();
                        else
                            trans.Commit();
                    }

                    //검사항목정보 삭제 실패시 MDM UseFlag 변경
                    if(!ErrRtn.ErrNum.Equals(0))
                        mfSaveInspectItem_MDMUseFlag(dt, "T", sql.SqlCon);
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 검사항목정보 MDMUSEFLAG 변경
        /// </summary>
        /// <param name="dtInspectItem">검사항목정보</param>
        /// <param name="strUseFlag">사용여부</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveInspectItem_MDMUseFlag(DataTable dtInspectItem, string strUseFlag, SqlConnection sqlcon)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                for (int i = 0; i < dtInspectItem.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["PlantCode"].ToString(),4);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["InspectGroupCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["InspectTypeCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar,dtInspectItem.Rows[i]["InspectItemCode"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam,"@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char,strUseFlag,1);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_MASInspectItem_MDMUseFlag", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if(!ErrRtn.ErrNum.Equals(0))
                        break;

                }


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }

        }


    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MTType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MTType : ServicedComponent
    {
        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetMASMTType()
        {
            DataTable dtMASMTType = new DataTable();

            try
            {
                dtMASMTType.Columns.Add("PlantCode", typeof(String));
                dtMASMTType.Columns.Add("MTLTypeCode", typeof(String));
                dtMASMTType.Columns.Add("MTMTypeCode", typeof(String));
                dtMASMTType.Columns.Add("MTTypeCode", typeof(String));
                dtMASMTType.Columns.Add("MTTypeName", typeof(String));
                dtMASMTType.Columns.Add("MTTypeNameCh", typeof(String));
                dtMASMTType.Columns.Add("MTTypeNameEn", typeof(String));
                dtMASMTType.Columns.Add("InspectPeriod", typeof(Int32));
                dtMASMTType.Columns.Add("InspectPeriodUnitCode", typeof(String));
                dtMASMTType.Columns.Add("UseFlag", typeof(Char));

                return dtMASMTType;
            }
            catch (Exception ex)
            {
                return dtMASMTType;
                throw (ex);
            }
            finally
            {
                dtMASMTType.Dispose();
            }
        }
        /// <summary>
        /// 콤보박스용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strLang"> Language </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASMTTypeCombo(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtResult = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMTTypeCombo", dtParam);
                return dtResult;
            }
            catch (Exception ex)
            {
                return dtResult;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtResult.Dispose();
            }
        }
        /// <summary>
        /// 중분류 이용 콤보박스용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strLang"> Language </param>
        /// /// <param name="strMTMTypeCode"> 계측기 중분류 코드 </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASMTTypeCombo(String strPlantCode, String strLang, String strMTMTypeCode)
        {
            SQLS sql = new SQLS();
            DataTable dtResult = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strMTMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTMTypeCode, 5);
                dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMTType_Group_Combo", dtParam);
                return dtResult;
            }
            catch (Exception ex)
            {
                return dtResult;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtResult.Dispose();
            }
        }
        /// <summary>
        /// 검색
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMTType(string strPlantCode)
        {
            SQLS sql = new SQLS();
            DataTable dtMTType = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable datparam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(datparam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);

                dtMTType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMTType", datparam);
                return dtMTType;
            }
            catch (Exception ex)
            {
                return dtMTType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMTType.Dispose();
            }
        }
        /// <summary>
        /// 저장
        /// </summary>
        /// <param name="dtInputMTType"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMASMTType(DataTable dtInputMTType, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtInputMTType.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInputMTType.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInputMTType.Rows[i]["MTLTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInputMTType.Rows[i]["MTMTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInputMTType.Rows[i]["MTTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dtInputMTType.Rows[i]["MTTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtInputMTType.Rows[i]["MTTypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtInputMTType.Rows[i]["MTTypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intInspectPeriod", ParameterDirection.Input, SqlDbType.Int, dtInputMTType.Rows[i]["InspectPeriod"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectPeriodUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtInputMTType.Rows[i]["InspectPeriodUnitCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtInputMTType.Rows[i]["UseFlag"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASMTType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        /// <summary>
        /// 삭제
        /// </summary>
        /// <param name="dtMTType"> 삭제할 정보가 담긴 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteMASMTType(DataTable dtMTType)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strResult = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtMTType.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtPram = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtPram, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtPram, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMTType.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPram, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtMTType.Rows[i]["MTTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtPram, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASMTType", dtPram);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strResult;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MeasureTool")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MeasureTool : ServicedComponent
    {
        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtMeasureTool = new DataTable();

            try
            {
                dtMeasureTool.Columns.Add("PlantCode", typeof(String));
                dtMeasureTool.Columns.Add("MeasureToolCode", typeof(String));
                dtMeasureTool.Columns.Add("MeasureToolName", typeof(String));
                dtMeasureTool.Columns.Add("MeasureToolNameCh", typeof(String));
                dtMeasureTool.Columns.Add("MeasureToolNameEn", typeof(String));
                dtMeasureTool.Columns.Add("MTTypeCode", typeof(String));
                dtMeasureTool.Columns.Add("WriteUserID", typeof(String));
                dtMeasureTool.Columns.Add("Spec", typeof(String));
                dtMeasureTool.Columns.Add("ModelName", typeof(String));
                dtMeasureTool.Columns.Add("MakerCompany", typeof(String));
                dtMeasureTool.Columns.Add("MakerNo", typeof(String));
                dtMeasureTool.Columns.Add("AcquireDate", typeof(String));
                dtMeasureTool.Columns.Add("AcquireAmt", typeof(Decimal));
                dtMeasureTool.Columns.Add("SerialNo", typeof(String));
                dtMeasureTool.Columns.Add("AcquireDeptCode", typeof(String));
                dtMeasureTool.Columns.Add("AcquireReason", typeof(String));
                dtMeasureTool.Columns.Add("InspectPeriod", typeof(Int32));
                dtMeasureTool.Columns.Add("InspectPeriodUnitCode", typeof(String));
                dtMeasureTool.Columns.Add("InspectNextDate", typeof(String));
                dtMeasureTool.Columns.Add("LastInspectDate", typeof(String));
                dtMeasureTool.Columns.Add("DiscardDate", typeof(String));
                dtMeasureTool.Columns.Add("DiscardFlag", typeof(Char));
                dtMeasureTool.Columns.Add("DiscardReason", typeof(String));
                dtMeasureTool.Columns.Add("MTFileName", typeof(String));
                dtMeasureTool.Columns.Add("UseFlag", typeof(Char));

                dtMeasureTool.Columns.Add("Class", typeof(String));
                dtMeasureTool.Columns.Add("Result", typeof(String));
                dtMeasureTool.Columns.Add("Customer", typeof(String));
                dtMeasureTool.Columns.Add("Allowance", typeof(String));
                dtMeasureTool.Columns.Add("Purpose", typeof(String));


                dtMeasureTool.Columns.Add("MTLTypeCode", typeof(String));
                dtMeasureTool.Columns.Add("MTMTypeCode", typeof(String));

                return dtMeasureTool;
            }
            catch (Exception ex)
            {
                return dtMeasureTool;
                throw (ex);
            }
            finally
            {
                dtMeasureTool.Dispose();
            }
        }

        /// <summary>
        /// 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMTTypeCode"> 계측기분류코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMeasureTool(String strPlantCode, String strMTTypeCode, string strMeasureToolCode, string strMeasureToolName
                                            , string strAcquireDeptCode, string strInspectNextDateFrom, string strInspectNextDateTo
                                            , string strNextDateOverCheck, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMeasureTool = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, strMeasureToolCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolName", ParameterDirection.Input, SqlDbType.NVarChar, strMeasureToolName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strAcquireDeptCode", ParameterDirection.Input, SqlDbType.VarChar, strAcquireDeptCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectNextDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectNextDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectNextDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectNextDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strNextDateOverCheck", ParameterDirection.Input, SqlDbType.VarChar, strNextDateOverCheck, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtMeasureTool = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMeasureTool", dtParam);

                return dtMeasureTool;
            }
            catch (Exception ex)
            {
                return dtMeasureTool;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMeasureTool.Dispose();
            }
        }

        /// <summary>
        /// 조회용 Popup Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMTTypeCode"> 계측기분류코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMeasureToolPopup(String strPlantCode, String strMTTypeCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMeasureTool = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtMeasureTool = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMeasureToolPopup", dtParam);

                return dtMeasureTool;
            }
            catch (Exception ex)
            {
                return dtMeasureTool;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMeasureTool.Dispose();
            }
        }

        /// <summary>
        /// 상세 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMTTypeCode"> 계측기분류코드 </param>
        /// <param name="strMeasureToolCode"> 계측기코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMeasureTool_Detail(String strPlantCode, String strMTTypeCode, String strMeasureToolCode)
        {
            SQLS sql = new SQLS();
            DataTable dtMeasureTool = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, strMeasureToolCode, 10);

                dtMeasureTool = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMeasureToolDetail", dtParam);

                return dtMeasureTool;
            }
            catch (Exception ex)
            {
                return dtMeasureTool;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMeasureTool.Dispose();
            }
        }

        /// <summary>
        /// 계측기 검교정 주기 / 단위 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMTTypeCode"> 계측기분류코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMTInspectPeriod(String strPlantCode, String strMTTypeCode)
        {
            SQLS sql = new SQLS();
            DataTable dtMTInspectPeriod = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTTypeCode, 10);


                dtMTInspectPeriod = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMTInspectPeriod", dtParam);

                return dtMTInspectPeriod;
            }
            catch (Exception ex)
            {
                return dtMTInspectPeriod;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMTInspectPeriod.Dispose();
            }
        }


        /// <summary>
        /// 계측기이력 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strMeasureToolCode">계측기코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>계측기이력정보</returns>
        [AutoComplete]
        public DataTable mfReadMASMeasureTool_MoveHist(string strPlantCode, string strMeasureToolCode, string strLang)
        {
            DataTable dtMoveHist = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, strMeasureToolCode, 20);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                dtMoveHist = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMeasureToolMoveHist", dtParam);
                return dtMoveHist;
            }
            catch (Exception ex)
            {
                return dtMoveHist;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtMoveHist.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 저장 Method
        /// </summary>
        /// <param name="dt"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자ID </param>
        /// <param name="strUserID"> 사용자IP </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMASMeasureTool(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MeasureToolCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MeasureToolName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MeasureToolNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolnameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MeasureToolNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MTTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Spec"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strModelName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ModelName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMakerCompany", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MakerCompany"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMakerNo", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MakerNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcquireDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AcquireDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_dblAcquireAmt", ParameterDirection.Input, SqlDbType.Decimal, dt.Rows[i]["AcquireAmt"].ToString(), 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strSerialNo", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["SerialNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcquireDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AcquireDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcquireReason", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["AcquireReason"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_intInspectPeriod", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["InspectPeriod"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectPeriodUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectPeriodUnitCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectNextDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectNextDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLastInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LastInspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDiscardDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DiscardDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDiscardFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["DiscardFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDiscardReason", ParameterDirection.Input, SqlDbType.Text, dt.Rows[i]["DiscardReason"].ToString(), 8000);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MTFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["UseFlag"].ToString(), 1);

                    // 2012-11-16 추가
                    sql.mfAddParamDataRow(dtParam, "@i_strClass", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["Class"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strAllowance", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Allowance"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strResult", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["Result"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strPurpose", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Purpose"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomer", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Customer"].ToString(), 50);
                    // 2012-11-16

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASMeasureTool", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 삭제 Method
        /// </summary>
        /// <param name="dt"> 삭제할 정보가 저장된 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMASMeasureTool(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MTTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MeasureToolCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASMeasureTool", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 콤보박스용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMTTypeCode"> 계측기분류코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMeasureToolCombo(String strPlantCode, String strMTTypeCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMTool = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtMTool = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMeasureToolCombo", dtParam);

                return dtMTool;
            }
            catch (Exception ex)
            {
                return dtMTool;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMTool.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InspectFaultType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InspectFaultType : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtIFType = new DataTable();
            try
            {
                dtIFType.Columns.Add("PlantCode", typeof(String));
                dtIFType.Columns.Add("InspectFaultTypeCode", typeof(String));
                dtIFType.Columns.Add("InspectFaultTypeName", typeof(String));
                dtIFType.Columns.Add("InspectFaultTypeNameCh", typeof(String));
                dtIFType.Columns.Add("InspectFaultTypeNameEn", typeof(String));
                dtIFType.Columns.Add("QCNFlag", typeof(Char));
                dtIFType.Columns.Add("ParentInspectFaultTypeCode", typeof(String));
                dtIFType.Columns.Add("InspectFaultGubun", typeof(string));
                dtIFType.Columns.Add("LocationOrMaterial", typeof(string));
                dtIFType.Columns.Add("ProcessGroup", typeof(string));
                dtIFType.Columns.Add("UseFlag", typeof(Char));
                return dtIFType;
            }
            catch (Exception ex)
            {
                return dtIFType;
                throw (ex);
            }
            finally
            {
                dtIFType.Dispose();
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectFaultType(String strPlantCode)
        {
            SQLS sql = new SQLS();
            DataTable dtIFType = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // 파라미터 DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                // 파리미터 데이터 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);

                // SP 실행
                dtIFType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectFaultType", dtParam);

                return dtIFType;
            }
            catch (Exception ex)
            {
                return dtIFType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtIFType.Dispose();
            }
        }

        /// <summary>
        /// 불량유형 콤보박스옹 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectFaultTypeCombo(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectFaultTypeCombo", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 불량유형 콤보설정 메소드(공정그룹에 따라 정렬)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectFaultTypeCombo_ProcessGroup(string strPlantCode, string strProcessCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectFaultTypeCombo_ProcessGroup", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 불량유형 콤보설정 메소드(불량구분에 따라)
        /// 
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strInspectFaultGubun"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectFaultTypeCombo_WithInspectFaultGubun(string strPlantCode, string strInspectFaultGubun, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultGubun", ParameterDirection.Input, SqlDbType.Char, strInspectFaultGubun, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectFaultType_Combo_WithFatulGubun", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 공장코드, 불량유형코드로 QCNFlag 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInspectFaultTypeCode">불량유형코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectFaultType_QCNFlag(string strPlantCode, string strInspectFaultTypeCode)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectFaultTypeCode, 10);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInspectFaultType_QCNFlag", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtInspectFaultType"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveInspectFaultType(DataTable dtInspectFaultType, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                SqlTransaction trans;

                // Parameter 변수 저장
                for (int i = 0; i < dtInspectFaultType.Rows.Count; i++)
                {
                    // Start transaction
                    trans = sql.SqlCon.BeginTransaction();

                    // Set Parameter DataTable
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectFaultType.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectFaultType.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectFaultType.Rows[i]["InspectFaultTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectFaultType.Rows[i]["InspectFaultTypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectFaultType.Rows[i]["InspectFaultTypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strQCNFlag", ParameterDirection.Input, SqlDbType.Char, dtInspectFaultType.Rows[i]["QCNFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strParentInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtInspectFaultType.Rows[i]["ParentInspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultGubun", ParameterDirection.Input, SqlDbType.Char, dtInspectFaultType.Rows[i]["InspectFaultGubun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLocationOrMaterial", ParameterDirection.Input, SqlDbType.NVarChar, dtInspectFaultType.Rows[i]["LocationOrMaterial"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.VarChar, dtInspectFaultType.Rows[i]["ProcessGroup"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtInspectFaultType.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASInspectFaultType", dtParam);

                    // 에러체크를 위한 Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 불량유형 삭제 메소드
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteInspectFaultType(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASInspectFaultType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSReqType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CCSReqType : ServicedComponent
    {
        /// <summary>
        /// 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtCCSReqType = new DataTable();

            try
            {
                dtCCSReqType.Columns.Add("PlantCode", typeof(String));
                dtCCSReqType.Columns.Add("ProcessGroup", typeof(String));
                dtCCSReqType.Columns.Add("CCSReqTypeCode", typeof(String));
                dtCCSReqType.Columns.Add("CCSReqTypeName", typeof(String));
                dtCCSReqType.Columns.Add("CCSReqTypeNameCh", typeof(String));
                dtCCSReqType.Columns.Add("CCSReqTypeNameEn", typeof(String));
                dtCCSReqType.Columns.Add("MESReqTypeCode", typeof(String));
                dtCCSReqType.Columns.Add("UseFlag", typeof(Char));

                return dtCCSReqType;
            }
            catch (Exception ex)
            {
                return dtCCSReqType;
                throw (ex);
            }
            finally
            {
                dtCCSReqType.Dispose();
            }
        }

        /// <summary>
        /// 조회용 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessGroup">공정그룹</param>        
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSReqType(String strPlantCode, String strProcessGroup)
        {
            SQLS sql = new SQLS();
            DataTable dtCCSReqType = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, strProcessGroup, 40);

                dtCCSReqType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCCSReqType", dtParam);

                return dtCCSReqType;
            }
            catch (Exception ex)
            {
                return dtCCSReqType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCCSReqType.Dispose();
            }
        }


        /// <summary>
        /// 저장 Method
        /// </summary>
        /// <param name="dtParam"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSReqType(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    //컬럼
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CCSReqTypeCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["CCSReqTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["CCSReqTypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["CCSReqTypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMESReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MESReqTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    //컬럼 끝
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASCCSReqType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        /// <summary>
        /// 콤보박스용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strProcessCode"> 공정코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASReqTypeCombo(String strPlantCode, String strProcessGroup, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMTool = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtMTool = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCSSReqTypeCombo", dtParam);

                return dtMTool;
            }
            catch (Exception ex)
            {
                return dtMTool;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMTool.Dispose();
            }
        }

        /// <summary>
        /// 콤보박스용 Method(공정코드로 공정그룹을 찾아서 Display)
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strProcessCode"> 공정코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASCCSReqTypeCombo(String strPlantCode, String strProcessCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMTool = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtMTool = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCSSReqTypeCombo2", dtParam);

                return dtMTool;
            }
            catch (Exception ex)
            {
                return dtMTool;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMTool.Dispose();
            }
        }
        /// <summary>
        /// 삭제 Method
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteCCSReqType(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            string strResult = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CCSReqTypeCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASCCSReqType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strResult;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// CCS의뢰 화면 MES I/F이후 공장,공정코드로 의뢰사유 콤보박스 설정해주는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASCCSReqType_CCSInspect(string strPlantCode, string strProcessCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCCSReqType_CCSInspect", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    #region CCS의뢰 기본항목 정보
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSReqItem")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CCSReqItem : ServicedComponent
    {
        /// <summary>
        /// CCS의뢰 기본항목 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtCCSReqItem = new DataTable();
            try
            {
                dtCCSReqItem.Columns.Add("PlantCode", typeof(String));
                dtCCSReqItem.Columns.Add("ProcessCode", typeof(String));
                dtCCSReqItem.Columns.Add("CCSReqItemCode", typeof(String));
                dtCCSReqItem.Columns.Add("CCSReqItemName", typeof(String));
                dtCCSReqItem.Columns.Add("CCSReqItemNameCh", typeof(String));
                dtCCSReqItem.Columns.Add("CCSReqItemNameEn", typeof(String));
                dtCCSReqItem.Columns.Add("UseFlag", typeof(Char));
                dtCCSReqItem.Columns.Add("Check", typeof(Boolean));

                return dtCCSReqItem;
            }
            catch (Exception ex)
            {
                return dtCCSReqItem;
                throw (ex);
            }
            finally
            {
                dtCCSReqItem.Dispose();
            }
        }
        /// <summary>
        /// 조회용 Method(CCS의뢰 기본항목 )
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>  
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSReqItem(String strPlantCode, String strProcessCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCCSReqItem = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtCCSReqItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCCSReqItem", dtParam);

                return dtCCSReqItem;
            }
            catch (Exception ex)
            {
                return dtCCSReqItem;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCCSReqItem.Dispose();
            }
        }
        /// <summary>
        /// 콤보박스용 Method(CCS의뢰 기본항목 )
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strProcessCode"> 공정코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASReqItemCombo(String strPlantCode, String strProcessCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCombo = sql.mfSetParamDataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtCombo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASCCSReqItemCombo", dtParam);

                return dtCombo;
            }
            catch (Exception ex)
            {
                return dtCombo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCombo.Dispose();
            }
        }

        /// <summary>
        /// 저장 Method(CCS의뢰 기본항목 )
        /// </summary>
        [AutoComplete(false)]
        public String mfSaveCCSReqItem(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqItemCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CCSReqItemCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqItemName", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CCSReqItemName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqItemNameCh", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CCSReqItemNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqItemNameEn", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CCSReqItemNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckFlag", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["Check"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@errorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASCCSReqItem", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 삭제 Method(CCS의뢰 기본항목 )        
        /// </summary>
        [AutoComplete(false)]
        public String mfDeleteCCSReqItem(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.Output, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "i_strCCSReqItemCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["CCSReqItemCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_MASCCSReqItem", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }
    #endregion

}