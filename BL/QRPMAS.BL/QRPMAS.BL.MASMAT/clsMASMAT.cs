﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 자재관리 기준정보                                     */
/* 프로그램ID   : clsMASMAT.cs                                          */
/* 프로그램명   : 자재관리기준정보                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-21                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;
using Oracle.DataAccess.Client;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPMAS.BL.MASMAT
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialGroup")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialGroup : ServicedComponent
    {
        /// <summary>
        /// MaterialGruop 조회용 Method
        /// </summary>
        /// <param name="strSearchMaterialGroupCode"> 자재그룹코드</param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASMaterialGroup(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterialGroup", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 자재그룹 콤보박스 설정 함수
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASMaterialGroupCombo(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();

            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterialGroupCombo", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialType : ServicedComponent
    {
        /// <summary>
        /// 자재유형 조회용 Method
        /// </summary>
        /// <param name="strMaterialTypeCode"> 자재유형코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMaterialType(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterialType", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 자재유형 콤보박스용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMaterialTypeCombo(String strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterialTypeCombo", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }

    ////[EventTrackingEnabled(true)]
    ////[JustInTimeActivation(true)]
    ////[ConstructionEnabled(true, Default = "None")]
    ////[Transaction(TransactionOption.Supported)]
    ////[ObjectPooling(true)]
    ////[Serializable]
    ////[System.EnterpriseServices.Description("PackageGroup")]
    ////[ClassInterface(ClassInterfaceType.None)]
    ////[ComponentAccessControl(true), SecurityRole("QRPService", true)]
    ////public class PackageGroup : ServicedComponent
    ////{
    ////    /// <summary>
    ////    /// 패키지그룹 콤보박스 설정 함수
    ////    /// </summary>
    ////    /// <param name="strPlantCode"> 공장코드 </param>
    ////    /// <returns> DataTable </returns>
    ////    [AutoComplete]
    ////    public DataTable mfReadMASPackageGroupCombo(string strLang)
    ////    {
    ////        SQLS sql = new SQLS();
    ////        DataTable dt = new DataTable();

    ////        try
    ////        {
    ////            sql.mfConnect();
    ////            DataTable dtParam = sql.mfSetParamDataTable();

    ////            sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
    ////            dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPackageGroupCombo", dtParam);

    ////            return dt;
    ////        }
    ////        catch (Exception ex)
    ////        {
    ////            return dt;
    ////            throw (ex);
    ////        }
    ////        finally
    ////        {
    ////            sql.mfDisConnect();
    ////        }
    ////    }
    ////}

    ////[EventTrackingEnabled(true)]
    ////[JustInTimeActivation(true)]
    ////[ConstructionEnabled(true, Default = "None")]
    ////[Transaction(TransactionOption.Supported)]
    ////[ObjectPooling(true)]
    ////[Serializable]
    ////[System.EnterpriseServices.Description("PackageType")]
    ////[ClassInterface(ClassInterfaceType.None)]
    ////[ComponentAccessControl(true), SecurityRole("QRPService", true)]
    ////public class PackageType : ServicedComponent
    ////{
    ////    /// <summary>
    ////    /// 패키지유형 콤보박스 설정 함수
    ////    /// </summary>
    ////    /// <param name="strPlantCode"> 공장코드 </param>
    ////    /// <returns> DataTable </returns>
    ////    [AutoComplete]
    ////    public DataTable mfReadMASPackageTypeCombo(string strLang)
    ////    {
    ////        SQLS sql = new SQLS();
    ////        DataTable dt = new DataTable();

    ////        try
    ////        {
    ////            sql.mfConnect();
    ////            DataTable dtParam = sql.mfSetParamDataTable();

    ////            sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
    ////            dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPackageTypeCombo", dtParam);

    ////            return dt;
    ////        }
    ////        catch (Exception ex)
    ////        {
    ////            return dt;
    ////            throw (ex);
    ////        }
    ////        finally
    ////        {
    ////            sql.mfDisConnect();
    ////        }
    ////    }
    ////}

    ////[EventTrackingEnabled(true)]
    ////[JustInTimeActivation(true)]
    ////[ConstructionEnabled(true, Default = "None")]
    ////[Transaction(TransactionOption.Supported)]
    ////[ObjectPooling(true)]
    ////[Serializable]
    ////[System.EnterpriseServices.Description("Package")]
    ////[ClassInterface(ClassInterfaceType.None)]
    ////[ComponentAccessControl(true), SecurityRole("QRPService", true)]
    ////public class Package : ServicedComponent
    ////{
    ////    /// <summary>
    ////    /// Package 조회용 Method
    ////    /// </summary>
    ////    /// <param name="strPlantCode"> 공장코드 </param>
    ////    /// <returns></returns>
    ////    [AutoComplete]
    ////    public DataTable mfReadMASPackage(string strPlantCode, string strLang)
    ////    {
    ////        SQLS sql = new SQLS();
    ////        DataTable dt = new DataTable();
    ////        try
    ////        {
    ////            sql.mfConnect();
    ////            DataTable dtParam = sql.mfSetParamDataTable();
    ////            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);                
    ////            sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
    ////            dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPackagePop", dtParam);
    ////            return dt;
    ////        }
    ////        catch (Exception ex)
    ////        {
    ////            return dt;
    ////            throw (ex);
    ////        }
    ////        finally
    ////        {
    ////            sql.mfDisConnect();
    ////        }
    ////    }
    ////}


    ////[EventTrackingEnabled(true)]
    ////[JustInTimeActivation(true)]
    ////[ConstructionEnabled(true, Default = "None")]
    ////[Transaction(TransactionOption.Supported)]
    ////[ObjectPooling(true)]
    ////[Serializable]
    ////[System.EnterpriseServices.Description("Family")]
    ////[ClassInterface(ClassInterfaceType.None)]
    ////[ComponentAccessControl(true), SecurityRole("QRPService", true)]
    ////public class Family : ServicedComponent
    ////{
    ////    /// <summary>
    ////    /// 패키지유형 콤보박스 설정 함수
    ////    /// </summary>
    ////    /// <param name="strPlantCode"> 공장코드 </param>
    ////    /// <returns> DataTable </returns>
    ////    [AutoComplete]
    ////    public DataTable mfReadMASFamilyCombo(string strLang)
    ////    {
    ////        SQLS sql = new SQLS();
    ////        DataTable dt = new DataTable();

    ////        try
    ////        {
    ////            sql.mfConnect();
    ////            DataTable dtParam = sql.mfSetParamDataTable();

    ////            sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
    ////            dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASFamilyCombo", dtParam);

    ////            return dt;
    ////        }
    ////        catch (Exception ex)
    ////        {
    ////            return dt;
    ////            throw (ex);
    ////        }
    ////        finally
    ////        {
    ////            sql.mfDisConnect();
    ////        }
    ////    }
    ////}


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("ConsumableType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ConsumableType : ServicedComponent
    {
        /// <summary>
        /// 패키지유형 콤보박스 설정 함수
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadMASConsumableTypeCombo(string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();

            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASConsumableTypeCombo", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Material")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Material : ServicedComponent
    {
        /// <summary>
        /// Material 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialGroupCode"> 자재그룹코드 </param>
        /// <param name="strMaterialTypeCode"> 자재유형코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMaterial(string strPlantCode, string strMaterialCode, string strMaterialTypeCode, string strConsumableTypeCode, string strInspectFlag, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strConsumableTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectFlag", ParameterDirection.Input, SqlDbType.Char, strInspectFlag, 1);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterial", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// Material Popup용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialGroupCode"> 자재그룹코드 </param>
        /// <param name="strMaterialTypeCode"> 자재유형코드 </param>
        /// <returns></returns>
        [AutoComplete]
        //public DataTable mfReadMASMaterialPopup(string strPlantCode, string strMaterialGroupCode, string strMaterialTypeCode, string strConsumableTypeCode, string strLang)
        public DataTable mfReadMASMaterialPopup(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strMaterialGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialGroupCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strMaterialTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialTypeCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strConsumableTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterialPopup", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 신제품Qual 콤보박스 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMaterialComboForNewQual(string strPlantCode,string strConsumableTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strConsumableTypeCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterialComboForNewQual", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 공장/자재코드로 자재정보 조회하는 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMaterialDetail(String strPlantCode, String strMaterialCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterialDetail", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 그리드 더블클릭 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strMaterialCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMaterialDetailInfo(string strPlantCode, string strMaterialCode)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterialDetailInfo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 데이터 테이블 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("MaterialCode", typeof(string));
                dtRtn.Columns.Add("MaterialName", typeof(string));
                dtRtn.Columns.Add("MaterialNameCh", typeof(string));
                dtRtn.Columns.Add("MaterialNameEn", typeof(string));
                dtRtn.Columns.Add("MaterialGroupCode", typeof(string));
                dtRtn.Columns.Add("MaterialTypeCode", typeof(string));
                dtRtn.Columns.Add("ConsumableTypeCode", typeof(string));
                dtRtn.Columns.Add("InspectFlag", typeof(string));
                dtRtn.Columns.Add("Spec", typeof(string));
                dtRtn.Columns.Add("UseFlag", typeof(string));
                dtRtn.Columns.Add("CreateType", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 자재정보 검대상여부 저장 메소드
        /// </summary>
        /// <param name="dtSave">자재정보 저장된 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASMaterial(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["InspectFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASMaterial", dtParam);

                    // 검사결과
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break; 
                    }
                }
                if(ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        /// <summary>
        /// 자재정보 저장
        /// </summary>
        /// <param name="dtSaveMat">자재정보 DataTable</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfSaveMASMaterialDetail(DataTable dtSaveMat, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSaveMat.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveMat.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveMat.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialName", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveMat.Rows[i]["MaterialName"].ToString(), 300);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveMat.Rows[i]["MaterialNameCh"].ToString(), 300);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveMat.Rows[i]["MaterialNameEn"].ToString(), 300);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveMat.Rows[i]["MaterialGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveMat.Rows[i]["MaterialTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSaveMat.Rows[i]["ConsumableTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFlag", ParameterDirection.Input, SqlDbType.Char, dtSaveMat.Rows[i]["InspectFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveMat.Rows[i]["Spec"].ToString(), 300);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtSaveMat.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCreateType", ParameterDirection.Input, SqlDbType.VarChar, dtSaveMat.Rows[i]["CreateType"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASMaterialDetail", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 자재정보 검사대상여부 WMS 전송 메소드
        /// </summary>
        /// <param name="dtSave">자재정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASmaterial_WMS_Oracle(DataTable dtSave, string strUserID, string strUserIP)
        {
            #region 변경전
            ////TransErrRtn ErrRtn = new TransErrRtn();
            ////try
            ////{
            ////    string strErrRtn = string.Empty;
            ////    //WMS Oracle DB 연결정보 읽기
            ////    QRPSYS.BL.SYSPGM.SystemAccessInfo clsPgm = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
            ////    DataTable dtPgm = clsPgm.mfReadSystemAccessInfoDetail(dtSave.Rows[0]["PlantCode"].ToString(), "S05");
            ////    QRPDB.ORADB oraDB = new QRPDB.ORADB();
            ////    oraDB.mfSetORADBConnectString(dtPgm.Rows[0]["SystemAddressPath"].ToString(), dtPgm.Rows[0]["AccessID"].ToString(), dtPgm.Rows[0]["AccessPassword"].ToString());
            ////    //oraDB.mfSetORADBConnectString("STS_MES_TEST", "wmsuser", "wms119");

            ////    //WMS Oracle DB연결하기
            ////    oraDB.mfConnect();

            ////    //Transaction 시작
            ////    OracleTransaction trans;
            ////    trans = oraDB.SqlCon.BeginTransaction();
            ////    string Result = string.Empty;
            ////    for (int i = 0; i < dtSave.Rows.Count; i++)
            ////    {
            ////        //WMS 처리 SP 호출
            ////        DataTable dtParam = oraDB.mfSetParamDataTable();
            ////        oraDB.mfAddParamDataRow(dtParam, "V_PLANT", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["PlantCode"].ToString(), 4);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_MATERIALCODE", ParameterDirection.Input, OracleDbType.Varchar2, dtSave.Rows[i]["MaterialCode"].ToString(), 20);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_INS", ParameterDirection.Input, OracleDbType.Char, dtSave.Rows[i]["InspectFlag"].ToString(), 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_TFLAG", ParameterDirection.Input, OracleDbType.Char, "T", 1);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_TDATE", ParameterDirection.Input, OracleDbType.Varchar2, DateTime.Now.ToString("yyyy-MM-dd"), 10);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERIP", ParameterDirection.Input, OracleDbType.Varchar2, strUserIP, 15);
            ////        oraDB.mfAddParamDataRow(dtParam, "V_USERID", ParameterDirection.Input, OracleDbType.Varchar2, strUserID, 15);
            ////        oraDB.mfAddParamDataRow(dtParam, "RTN", ParameterDirection.Output, OracleDbType.Int32);

            ////        //strErrRtn = oraDB.mfExecTransStoredProc(oraDB.SqlCon, trans, "up_Update_MASMaterial_INS", dtParam);
            ////        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

            ////        //WMS 처리결과에 따른 Transaction 처리
            ////        Result = ErrRtn.mfGetReturnValue(0);
            ////        if (!Result.Equals("0"))
            ////        {
            ////            trans.Rollback();
            ////            break;
            ////        }
            ////    }
            ////    if (Result.Equals("0"))
            ////        trans.Commit();

            ////    clsPgm.Dispose();
            ////    oraDB.Dispose();

            ////    return strErrRtn;
            ////}
            ////catch (Exception ex)
            ////{
            ////    return ex.Message;
            ////}
            ////finally
            ////{

            ////}           
            #endregion
            //up_Update_MASMaterial_WMS
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                //SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@I_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["InspectFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strTFlag", ParameterDirection.Input, SqlDbType.Char, "T", 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strTDate", ParameterDirection.Input, SqlDbType.VarChar, DateTime.Now.ToString("yyyy-MM-dd"), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_MASMaterial_WMS_Send", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0) || !ErrRtn.ErrMessage.Equals("00"))
                    {
                        //trans.Rollback();
                        break;
                    }
                }

                //if (ErrRtn.ErrNum.Equals(0))
                //{
                //    trans.Commit();
                //}
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }

        }   

        /// <summary>
        /// 자재정보 테이블 WMSTFlag Update 메소드
        /// </summary>
        /// <param name="dtSave">자재정보 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveMASmaterial_WMSTFlag(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_MASMaterial_WMS", dtParam);

                    // 검사결과
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 자재콤보용 메소드
        /// </summary>
        /// <param name="strPalntCode">공장코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMaterial_MaterialCombo(string strPalntCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();
                // 파라미터 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPalntCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP호출
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASMaterial_MaterialCombo", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Product")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Product : ServicedComponent
    {

        /// <summary>
        /// Product 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strCustomerSpec">고객사제품코드</param>
        /// <param name="strPackagroup_1">패키지그룹_1</param>
        /// <param name="strPackagroup_2">패키지그룹_2</param>
        /// <param name="strPackagroup_3">패키지그룹_3</param>
        /// <param name="strPackagroup_4">패키지그룹_4</param>
        /// <param name="strPackagroup_5">패키지그룹_5</param>
        /// <param name="strProductActionType">제품구분</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>제품정보</returns>
        [AutoComplete]
        //public DataTable mfReadMASProduct(string strPlantCode, string strMaterialGroupCode, string strMaterialTypeCode, string strPackGroupCode, string strPackageTypeCode, string strFamilyCode, string strLang)
        public DataTable mfReadMASProduct(string strPlantCode, string strPackage, string strCustomerCode, string strCustomerSpec,
                                          string strPackagroup_1, string strPackagroup_2, string strPackagroup_3, string strPackagroup_4, string strPackagroup_5,
                                          string strProductActionType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);   //공장
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);           //Package
                sql.mfAddParamDataRow(dtParam, "@i_strCustomer", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);     //고객사
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerSpec", ParameterDirection.Input, SqlDbType.NVarChar, strCustomerSpec, 40);//고객사제품코드
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_1", ParameterDirection.Input, SqlDbType.VarChar, strPackagroup_1, 40);//패키지그룹_1
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_2", ParameterDirection.Input, SqlDbType.VarChar, strPackagroup_2, 40);//패키지그룹_2
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_3", ParameterDirection.Input, SqlDbType.VarChar, strPackagroup_3, 40);//패키지그룹_3
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_4", ParameterDirection.Input, SqlDbType.NVarChar, strPackagroup_4, 40);//패키지그룹_4
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup_5", ParameterDirection.Input, SqlDbType.NVarChar, strPackagroup_5, 40);//패키지그룹_5
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.NVarChar, strProductActionType, 40);//패키지그룹_5

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProduct", dtParam);
                
                return dt;


                //sql.mfAddParamDataRow(dtParam, "@i_strMaterialGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialGroupCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strMaterialTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialTypeCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strPackageGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strPackGroupCode, 40);
                //sql.mfAddParamDataRow(dtParam, "@i_strPackageTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strPackageTypeCode, 40);
                //sql.mfAddParamDataRow(dtParam, "@i_strFamilyCode", ParameterDirection.Input, SqlDbType.VarChar, strFamilyCode, 40);

            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// Package 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_Package(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProduct_Package", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

       /// <summary>
       /// Model조회용(검색조건)
       /// </summary>
       /// <param name="strPlantCode">공장</param>
       /// <param name="strCustomerCode">고객사코드</param>
       /// <param name="strLang">사용언어</param>
       /// <returns>Model정보</returns>
        [AutoComplete]
        public DataTable mfReadMASProductPackage_Customer(string strPlantCode,string strCustomerCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPackage = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dtPackage = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProductPackage_Customer", dtParam);
                return dtPackage;
            }
            catch (Exception ex)
            {
                return dtPackage;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtPackage.Dispose();
            }
        }

        /// <summary>
        /// Package조회용(검색조건)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>Package정보</returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_Package_Customer(string strPlantCode, string strCustomerCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPackage = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dtPackage = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProduct_Package_Customer", dtParam);
                return dtPackage;
            }
            catch (Exception ex)
            {
                return dtPackage;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtPackage.Dispose();
            }
        }

        /// <summary>
        /// PackageGroup 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_PackageGroup(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProduct_PackageGroup", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// PackageGroupCombo 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPackageNum">패키지넘버</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>리턴</returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_PackageGroup(string strPlantCode, string strPackageNum, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPackageGroup = new DataTable();
            try
            {
                //sql연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroupNum", ParameterDirection.Input, SqlDbType.Char, strPackageNum, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //PackageGroupCombo조회 프로시저 실행
                dtPackageGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProduct_PackageGroupCombo", dtParam);

                //PackageGroup 정보
                return dtPackageGroup;

            }
            catch (Exception ex)
            {
                return dtPackageGroup;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPackageGroup.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 제품구분(ProductActionType) 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductActionType">제품구분</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>리턴</returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_ActionType(string strPlantCode, string strProductActionType, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtActionType = new DataTable();
            try
            {
                //sql연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductActionType", ParameterDirection.Input, SqlDbType.NVarChar, strProductActionType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //제품구분 조회 프로시저 실행
                dtActionType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProduct_ActionType", dtParam);

                //PackageGroup 정보
                return dtActionType;

            }
            catch (Exception ex)
            {
                return dtActionType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtActionType.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// Density 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_Density(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProduct_Density", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 패키지에 따른 제품 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPackage">패키지</param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProductWithPackageCombo(string strPlantCode, String strPackage, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);


                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProductWithPackage", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
        /// <summary>
        /// StackSeq 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_StackSeq(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProduct_StackSeq", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// Generation 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_Generation(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProduct_Generation", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// Product Popup용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialGroupCode"> 자재그룹코드 </param>
        /// <param name="strMaterialTypeCode"> 자재유형코드 </param>
        /// <returns></returns>
        [AutoComplete]
        //public DataTable mfReadMASProductPopup(string strPlantCode, string strMaterialGroupCode, string strMaterialTypeCode, string strPackGroupCode, string strPackageTypeCode, string strFamilyCode, string strLang)
        public DataTable mfReadMASProductPopup(string strPlantCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strMaterialGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialGroupCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strMaterialTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialTypeCode, 10);
                //sql.mfAddParamDataRow(dtParam, "@i_strPackageGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strPackGroupCode, 40);
                //sql.mfAddParamDataRow(dtParam, "@i_strPackageTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strPackageTypeCode, 40);
                //sql.mfAddParamDataRow(dtParam, "@i_strFamilyCode", ParameterDirection.Input, SqlDbType.VarChar, strFamilyCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProductPopup", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 제품코드로 고객사 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드(없을시 공백)</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>고객사코드 명</returns>
        [AutoComplete]
        public DataTable mfReadMaterialCustomer(string strPlantCode,string strProductCode , string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCustomer = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //제품코드로 고객사 검색하는 프로시저 실행 
                dtCustomer = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProductCustomer", dtParam);

                //고객사정보 리턴
                return dtCustomer;
            }
            catch (Exception ex)
            {
                return dtCustomer;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtCustomer.Dispose();
            }
        }

        /// <summary>
        /// 제품코드로 고객사 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드(없을시 공백)</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>고객사코드 명</returns>
        [AutoComplete]
        public DataTable mfReadProductInfo_frmINS0008C_PSTS(string strPlantCode, string strProductCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCustomer = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                // 제품코드로 제품관련 정보 조회하는 SP호출 
                dtCustomer = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProductInfo_frmINS0008C_PSTS", dtParam);

                // 제품관련정보 Return
                return dtCustomer;
            }
            catch (Exception ex)
            {
                return dtCustomer;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtCustomer.Dispose();
            }
        }

        /// <summary>
        /// 제품정보 조회(공장/제품코드로 조회)
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strProductCode"> 제품코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASMaterialDetail(String strPlantCode, String strProductCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProductDetail", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 고객제품코드로 제품정보 조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strCustomerProductSpec"> 고객제품코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMAProduct_CustomerSpec_WithPackage_CustomerCode(string strPlantCode, string strCustomerCode, string strPackage, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProduct_CustomerSpec_WIthPackage_Customer", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 공장, 고객사, Package로 고객제품코드 조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strCustomerProductSpec"> 고객제품코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASCustomerProductSpec(string strPlantCode, string strCustomerProductSpec, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantcode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerProductSpec", ParameterDirection.Input, SqlDbType.NVarChar, strCustomerProductSpec, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "Up_Select_MASProductCustomerSpec", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// PackageGroupCombo 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPackageNum">패키지넘버</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>리턴</returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_PackageGroup(string strPlantCode, string strPackageNum, string strCustomerCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtPackageGroup = new DataTable();
            try
            {
                //sql연결
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroupNum", ParameterDirection.Input, SqlDbType.Char, strPackageNum, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //PackageGroupCombo조회 프로시저 실행
                dtPackageGroup = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProduct_PackageGroupCombo1", dtParam);

                //PackageGroup 정보
                return dtPackageGroup;

            }
            catch (Exception ex)
            {
                return dtPackageGroup;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtPackageGroup.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// Package ComboBox 데이터 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strPackageGroupNum">PackageGroup번호</param>
        /// <param name="strPackageGroup">PackageGroup코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMASProduct_PackageCombo(string strPlantCode, string strCustomerCode, string strPackageGroupNum, string strPackageGroup, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroupNum", ParameterDirection.Input, SqlDbType.Char, strPackageGroupNum, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackageGroup", ParameterDirection.Input, SqlDbType.VarChar, strPackageGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASProduct_PackageCombo1", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Inventory")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Inventory : ServicedComponent
    {
        /// <summary>
        /// 창고조회
        /// </summary>
        /// <param name="strInventorycode"> 창고코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInventory(String strPlantCode, String strLang)
        {
            //DB연결
            SQLS sql = new SQLS();
            DataTable dtInven = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParm = sql.mfSetParamDataTable();
                //dtparm에 저장
                sql.mfAddParamDataRow(dtParm, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParm, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //저장프로시저 실행
                dtInven = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASInventory", dtParm);

                return dtInven;

            }
            catch (Exception ex)
            {
                return dtInven;
                throw (ex);
            }
            finally
            {
                //DB종료
                sql.mfDisConnect();
                sql.Dispose();
                dtInven.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Section")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Section : ServicedComponent
    {
        /// <summary>
        /// 창고Section검색
        /// </summary>
        /// <param name="strPlant"> 공장코드 </param>
        /// <param name="strInventoryCode"> 창고코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSection(String strPlant, String strInventoryCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtsection = new DataTable();
            try
            {
                //DB연결
                sql.mfConnect();
                DataTable dtPram = sql.mfSetParamDataTable();
                //데이터저장
                sql.mfAddParamDataRow(dtPram, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlant, 10);
                sql.mfAddParamDataRow(dtPram, "@i_strInventoryCode", ParameterDirection.Input, SqlDbType.VarChar, strInventoryCode, 10);
                sql.mfAddParamDataRow(dtPram, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //저장프로시저 실행
                dtsection = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASSection", dtPram);

                return dtsection;
            }
            catch (Exception ex)
            {
                return dtsection;
                throw (ex);
            }
            finally
            {
                //DB종료
                sql.mfDisConnect();
                sql.Dispose();
                dtsection.Dispose();
            }
        }
    }
}
