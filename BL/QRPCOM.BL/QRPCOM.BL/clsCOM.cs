﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//추가참조
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;
using System.Net.Mail;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]
namespace QRPCOM.BL
{

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("Mail")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Mail : ServicedComponent
    {
        /// <summary>
        /// SMTP로 메일 보내기
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strSMTPServer">SMTP서버 주소</param>
        /// <param name="intPort">SMTP서버 Port</param>
        /// <param name="strFromEMail">보내는 사람 이메일 주소</param>
        /// <param name="strFromUserName">보내는 사람 이름</param>
        /// <param name="strToEMail">받는 사람 이메일 주소</param>
        /// <param name="strSubject">메일 제목</param>
        /// <param name="strBody">메일 내용</param>
        /// <param name="arrAttchFileList">첨부화일</param>
        public bool mfSendSMTPMail(string strPlantCode, string strFromEMail, string strFromUserName, string strToEMail, 
                                 string strSubject, string strBody, System.Collections.ArrayList arrAttchFileList)
        {
            string strFolderPath = "";
            string strFilePath = "";
            try
            {   
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S06");

                if (dtSysAccess.Rows.Count <= 0)
                    return false;

                string strSMTPServer = dtSysAccess.Rows[0]["SystemAddressPath"].ToString();
                int intPort = Convert.ToInt32(dtSysAccess.Rows[0]["EtcAccessInfo1"].ToString());

                SmtpClient client = new SmtpClient(strSMTPServer);
                //MailAddress from = new MailAddress(strFromEMail, strFromUserName, System.Text.Encoding.UTF8);
                //MailAddress to = new MailAddress(strToEMail);

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(strFromEMail, strFromUserName, System.Text.Encoding.UTF8);
                mail.To.Add(strToEMail);
                mail.Subject = strSubject;
                mail.Body = strBody;
                //string someArrows = new string(new char[] { '\u2190', '\u2191', '\u2192', '\u2193' });
                //message.Body += Environment.NewLine;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                // Body에 HTML태그 사용
                mail.IsBodyHtml = true;

                if (arrAttchFileList.Count > 0)
                {
                    //메일 첨부화일 경로 가지고 오기
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                    if (dtFilePath.Rows.Count > 0)
                    {
                        string strServerPath = @dtFilePath.Rows[0]["ServerPath"].ToString();  //@"d:\QRP_STS_FileSvr\UploadFile\";
                        string strFolderName = dtFilePath.Rows[0]["FolderName"].ToString();  //"MailAttachment";

                        strFolderPath = strServerPath + strFolderName;
                        System.Net.Mail.Attachment attach;
                        for (int i = 0; i < arrAttchFileList.Count; i++)
                        {
                            //첨부화일 경로 및 첨부화일명
                            strFilePath = strFolderPath + "\\" + arrAttchFileList[i].ToString();
                            //첨부화일이 있는지 체크함.
                            if (System.IO.File.Exists(strFilePath))
                            {
                                attach = new System.Net.Mail.Attachment(strFilePath);
                                mail.Attachments.Add(attach);
                            }
                        }
                    }
                }
                //client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                //System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("tica100", "dance1st");
                //client.UseDefaultCredentials = false;
                //client.Credentials = SMTPUserInfo;

                //client.SendAsync(mail, userState);
                client.Send(mail);
                mail.Dispose();
                //message.Dispose();

                return true;
            }
            catch (System.Exception ex)
            {
                throw (ex);
                return false;
            }
            finally
            {
                //string strFilePath = "";
                //메일을 모두 보낸 후에 첨부화일을 삭제한다.
                if (arrAttchFileList.Count > 0 && strFolderPath != "")
                {
                    for (int i = 0; i < arrAttchFileList.Count; i++)
                    {
                        strFilePath = strFolderPath + "\\" + arrAttchFileList[i].ToString();
                        //첨부화일이 있는지 체크함.
                        if (System.IO.File.Exists(strFilePath))
                            System.IO.File.Delete(strFilePath);
                    }
                }
            }
        }

        /// <summary>
        /// 发送邮件，固定PSTSMANAGER发送
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strFromUserName"></param>
        /// <param name="strToEMail"></param>
        /// <param name="strSubject"></param>
        /// <param name="strBody"></param>
        /// <param name="arrAttchFileList"></param>
        /// <returns></returns>
        public bool mfSendSMTPMail(string strPlantCode, string strFromUserName, string strToEMail,
                         string strSubject, string strBody, System.Collections.ArrayList arrAttchFileList)
        {
            string strFolderPath = "";
            string strFilePath = "";
            try
            {
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S06");

                if (dtSysAccess.Rows.Count <= 0)
                    return false;

                string strSMTPServer = dtSysAccess.Rows[0]["SystemAddressPath"].ToString();
                int intPort = Convert.ToInt32(dtSysAccess.Rows[0]["EtcAccessInfo1"].ToString());
                string strFrom = dtSysAccess.Rows[0]["AccessID"].ToString();
                string strPwd = dtSysAccess.Rows[0]["AccessPassword"].ToString();

                SmtpClient client = new SmtpClient(strSMTPServer, intPort);

                #region 邮件服务器验证用户名和密码
                MailAddress net_From = new MailAddress(strFrom, strFromUserName, System.Text.Encoding.UTF8);
                try
                {
                    System.Net.NetworkCredential network = new System.Net.NetworkCredential(strFrom, strPwd);
                    client.Credentials = new System.Net.NetworkCredential(net_From.Address, strPwd);
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return false;
                }
                #endregion

                MailMessage mail = new MailMessage();
                mail.From = net_From;
                mail.To.Clear();
                mail.To.Add(strToEMail);
                mail.Subject = strSubject;
                mail.Body = strBody;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                // Body에 HTML태그 사용
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.Normal;

                //清空历史附件，防止附件多加载发送
                mail.Attachments.Clear();

                if (arrAttchFileList.Count > 0)
                {
                    //메일 첨부화일 경로 가지고 오기
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0022");

                    if (dtFilePath.Rows.Count > 0)
                    {
                        string strServerPath = @dtFilePath.Rows[0]["ServerPath"].ToString();  //@"d:\QRP_STS_FileSvr\UploadFile\";
                        string strFolderName = dtFilePath.Rows[0]["FolderName"].ToString();  //"MailAttachment";

                        strFolderPath = strServerPath + strFolderName;

                        System.Net.Mail.Attachment attach;
                        for (int i = 0; i < arrAttchFileList.Count; i++)
                        {
                            //첨부화일 경로 및 첨부화일명
                            strFilePath = strFolderPath + "\\" + arrAttchFileList[i].ToString();
                            //첨부화일이 있는지 체크함.
                            if (System.IO.File.Exists(strFilePath))
                            {
                                attach = new System.Net.Mail.Attachment(strFilePath);
                                mail.Attachments.Add(attach);
                            }
                        }
                    }
                }

                client.Send(mail);
                mail.Dispose();

                return true;
            }
            catch (System.Exception ex)
            {
                throw (ex);
                return false;
            }
            finally
            {
                //string strFilePath = "";
                //메일을 모두 보낸 후에 첨부화일을 삭제한다.
                if (arrAttchFileList.Count > 0 && strFolderPath != "")
                {
                    for (int i = 0; i < arrAttchFileList.Count; i++)
                    {
                        strFilePath = strFolderPath + "\\" + arrAttchFileList[i].ToString();
                        //첨부화일이 있는지 체크함.
                        if (System.IO.File.Exists(strFilePath))
                            System.IO.File.Delete(strFilePath);
                    }
                }
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("DataType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DataType : ServicedComponent
    {
        /// <summary>
        /// DataType ComboBox 설정용 Method
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDataTypeForCombo(String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Lang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);


                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_COMDataTypeCombo", dtParam);          

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("PeriodUnit")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class PeriodUnit : ServicedComponent
    {
        /// <summary>
        /// PeriodUnit ComboBox 설정용 Method
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadPeriodUnitForCombo(String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Lang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_COMPeriodUnitCombo", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }
}
