﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : ISO관리                                               */
/* 프로그램ID   : clsISOPRC.cs                                          */
/* 프로그램명   : 공정검사규격서                                        */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-09                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]
namespace QRPISO.BL.ISOPRC
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcessInspectSpecH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcessInspectSpecH : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("StdNumber", typeof(String));
                dt.Columns.Add("StdSeq", typeof(String));
                dt.Columns.Add("VersionNum", typeof(Int32));                
                dt.Columns.Add("Package", typeof(String));
                dt.Columns.Add("CustomerCode", typeof(string));
                dt.Columns.Add("WriteID", typeof(String));
                dt.Columns.Add("WriteDate", typeof(String));
                dt.Columns.Add("RevisionUserID", typeof(String));
                dt.Columns.Add("RevisionDate", typeof(String));
                dt.Columns.Add("AdmitUserID", typeof(String));
                dt.Columns.Add("AdmitDate", typeof(String));
                dt.Columns.Add("EtcDesc", typeof(String));
                dt.Columns.Add("EtcDesc1", typeof(String));
                dt.Columns.Add("EtcDesc2", typeof(String));
                dt.Columns.Add("EtcDesc3", typeof(String));
                dt.Columns.Add("EtcDesc4", typeof(String));
                dt.Columns.Add("MDM", typeof(String));
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_MachineUsingReqH()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("StdNumber", typeof(String));
                dt.Columns.Add("StdSeq", typeof(String));
                dt.Columns.Add("VersionNum", typeof(Int32));
                dt.Columns.Add("Package", typeof(String));
                dt.Columns.Add("CustomerCode", typeof(string));
                dt.Columns.Add("QTY", typeof(string));
                dt.Columns.Add("WriteID", typeof(String));
                dt.Columns.Add("WriteDate", typeof(String));
                dt.Columns.Add("EtcDesc", typeof(String));

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>        
        /// <param name="strPackage"> Package </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOProcessInspectSpecH(String strPlantCode, String strPackage, string strCustomerCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtHeader = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 4);

                dtHeader = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcessInspectSpecH", dtParam);

                return dtHeader;
            }
            catch (Exception ex)
            {
                return dtHeader;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtHeader.Dispose();
            }
        }

        /// <summary>
        /// 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>        
        /// <param name="strPackage"> Package </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMachineUsingReqH(String strPlantCode, String strPackage, string strCustomerCode,String strStdNumberS, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtHeader = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumberS", ParameterDirection.Input, SqlDbType.VarChar, strStdNumberS, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 4);

                dtHeader = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMachineUsingReqH", dtParam);

                return dtHeader;
            }
            catch (Exception ex)
            {
                return dtHeader;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtHeader.Dispose();
            }
        }

        /// <summary>
        /// 그리드 더블클릭시 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOProcessInspectSpecHDetail(String strPlantCode, String strStdNumber, String strStdSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtHDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtHDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcessInspectSpecHDetail", dtParam);

                return dtHDetail;
            }
            catch (Exception ex)
            {
                return dtHDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtHDetail.Dispose();
            }
        }

        /// <summary>
        /// 그리드 더블클릭시 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMachineUsingReqHDetail(String strPlantCode, String strStdNumber, String strStdSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtHDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtHDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMachineUsingReqHDetail", dtParam);

                return dtHDetail;
            }
            catch (Exception ex)
            {
                return dtHDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtHDetail.Dispose();
            }
        }

        /// <summary>
        /// 공정검사규격서 체크(DB에 이미 등록된 표준번호가 있는지 검사)
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>        
        /// <param name="strPackage"> Package </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOProcessInspectSpecCheck(String strPlantCode, String strPackage, string strCustomerCode)
        {
            SQLS sql = new SQLS();
            DataTable dtCheck = new DataTable();
            try
            {
                // DB연결
                sql.mfConnect();
                
                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                // SP 실행
                dtCheck = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcessInspectSpecHCheck", dtParam);

                // DB로부터 가져온 Data Return
                return dtCheck;
            }
            catch (Exception ex)
            {
                return dtCheck;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtCheck.Dispose();
            }
        }

        [AutoComplete]
        public string mfTest()
        {
            string strRtn = string.Empty;
            try
            {
                // 호스트 이름으로 IP를 구한다 
                System.Net.IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()); 
                System.Net.IPAddress[] addr = ipEntry.AddressList; 
                
                for (int i = 0; i < addr.Length; i++) 
                {
                    if (ipEntry.AddressList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        strRtn = ipEntry.AddressList[i].ToString();
                    } 
                    //Console.WriteLine("IP Address {0}: {1} ", i, addr[i].ToString()); 
                    //strRtn = strRtn + addr[i].ToString();
                }
                return strRtn;
            }
            catch(Exception ex)
            {
                return strRtn;
                throw(ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 헤더정보 저장 Method
        /// </summary>
        /// <param name="dtHeader"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="dtDetail"> 상세정보가 담긴 DataTable </param>
        /// <param name="strDelPlantCode"> 삭제할 데이터가 있는경우 삭제할 데이터의 공장코드(없을경우 공백) </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOProcessInspectSpecH(DataTable dtHeader, String strUserID, String strUserIP, DataTable dtDetail, String strDelPlantCode)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 변수
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CustomerCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc1", ParameterDirection.Input, SqlDbType.Text, dtHeader.Rows[i]["EtcDesc1"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc2", ParameterDirection.Input, SqlDbType.Text, dtHeader.Rows[i]["EtcDesc2"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc3", ParameterDirection.Input, SqlDbType.Text, dtHeader.Rows[i]["EtcDesc3"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc4", ParameterDirection.Input, SqlDbType.Text, dtHeader.Rows[i]["EtcDesc4"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISOProcessInspectSpecH", dtParam);

                    // 결과확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // Error가 발생했으면 Rollback하고 For문 빠져나간다
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // Output 결과를 받음
                        String strRtnStdNumber = ErrRtn.mfGetReturnValue(0);
                        String strRtnStdSeq = ErrRtn.mfGetReturnValue(1);

                        // 상세정보삭제
                        QRPISO.BL.ISOPRC.ProcessInspectSpecD clsDetail = new ProcessInspectSpecD();
                        String strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                        strErrRtn = clsDetail.mfDeleteISOProcessInspectSpecDAll(strPlantCode, strRtnStdNumber, strRtnStdSeq, sql.SqlCon, trans);

                        // 결과확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        // 저장할 상세정보가 있으면 저장
                        if (dtDetail.Rows.Count > 0)
                        {
                            // 상세정보 저장
                            strErrRtn = clsDetail.mfSaveISOProcessInspectSpecD(dtDetail, strUserID, strUserIP, sql.SqlCon, trans, strRtnStdNumber, strRtnStdSeq);

                            // 결과확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                            else
                            {
                                strErrRtn = clsDetail.mfSaveISOProcessInspectSpecHHistory(dtHeader, strUserID, strUserIP, sql.SqlCon, trans, strRtnStdNumber, strRtnStdSeq);
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    break;
                                }
                            }       
                        }
                        
                        trans.Commit();

                        ////if (dtHeader.Rows[i]["MDM"].ToString().Equals("T"))
                        ////{
                        ////    TransErrRtn Err = new TransErrRtn();

                        ////    string strError = mfSaveISOProcInspectSpecH_MDM(dtHeader, dtDetail, strRtnStdNumber, strRtnStdSeq, sql.SqlCon);

                        ////    Err = Err.mfDecodingErrMessage(strError);
                        ////    if (Err.ErrNum.Equals(0))
                        ////    {
                        ////        trans = sql.SqlCon.BeginTransaction();

                        ////        strError = mfSaveISOProcInspectSpecH_MDMTFlag(dtHeader.Rows[i]["PlantCode"].ToString(),
                        ////                                                        strRtnStdNumber,
                        ////                                                        strRtnStdSeq, "1", strUserIP, strUserID, sql.SqlCon, trans);

                        ////        Err = Err.mfDecodingErrMessage(strError);

                        ////        if (Err.ErrNum.Equals(0))
                        ////            trans.Commit();
                        ////        else
                        ////            trans.Rollback();

                        ////    }
                        ////}

                        //////////////////////////////////////////////////////
                        // LIVE 적용시 LIVE서버 IP 주소 반영할것!!!!!!!!!!!!!!
                        //////////////////////////////////////////////////////
                        //if (mfTest().Equals("10.60.60.91") || mfTest().Equals("10.60.60.92"))
                        if (mfTest().Equals("10.61.61.71") || mfTest().Equals("10.61.61.73"))
                        {
                            TransErrRtn Err = new TransErrRtn();

                            string strERROR = mfSaveISOProcInspectSpecH_MDM_T(strPlantCode, strRtnStdNumber, strRtnStdSeq, 1, sql.SqlCon);

                            Err = Err.mfDecodingErrMessage(strERROR);
                            if (Err.ErrNum.Equals(0))
                            {
                                trans = sql.SqlCon.BeginTransaction();

                                strERROR = mfSaveISOProcInspectSpecH_MDMTFlag(dtHeader.Rows[i]["PlantCode"].ToString(),
                                                                                strRtnStdNumber,
                                                                                strRtnStdSeq, "1", strUserIP, strUserID, sql.SqlCon, trans);

                                Err = Err.mfDecodingErrMessage(strERROR);

                                if (Err.ErrNum.Equals(0))
                                    trans.Commit();
                                else
                                    trans.Rollback();
                            }

                        }

                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 헤더정보 저장 Method
        /// </summary>
        /// <param name="dtHeader"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="dtDetail"> 상세정보가 담긴 DataTable </param>
        /// <param name="strDelPlantCode"> 삭제할 데이터가 있는경우 삭제할 데이터의 공장코드(없을경우 공백) </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOMachineUsingReqH(DataTable dtHeader, String strUserID, String strUserIP, DataTable dtDetail, DataTable dtMatDetail, DataTable dtApproveDetail, String strDelPlantCode)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 변수
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StdSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CustomerCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strQTY", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["QTY"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISOMachineUsingReqH", dtParam);

                    // 결과확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // Error가 발생했으면 Rollback하고 For문 빠져나간다
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // Output 결과를 받음
                        String strRtnStdNumber = ErrRtn.mfGetReturnValue(0);
                        String strRtnStdSeq = ErrRtn.mfGetReturnValue(1);

                        // 상세정보삭제
                        QRPISO.BL.ISOPRC.ProcessInspectSpecD clsDetail = new ProcessInspectSpecD();
                        String strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                        strErrRtn = clsDetail.mfDeleteISOMachineUsingReqDAll(strPlantCode, strRtnStdNumber, strRtnStdSeq, sql.SqlCon, trans);

                        // 결과확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        // dtMatDetail
                        strErrRtn = clsDetail.mfDeleteISOMachineUsingReqMatDAll(strPlantCode, strRtnStdNumber, strRtnStdSeq, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        // dtApproveDetail
                        strErrRtn = clsDetail.mfDeleteISOMachineUsingReqApproveDAll(strPlantCode, strRtnStdNumber, strRtnStdSeq, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }

                        // 저장할 상세정보가 있으면 저장
                        if (dtDetail.Rows.Count > 0)
                        {
                            // 상세정보 저장
                            strErrRtn = clsDetail.mfSaveISOMachineUsingReqD(dtDetail, strUserID, strUserIP, sql.SqlCon, trans, strRtnStdNumber, strRtnStdSeq);

                            // 결과확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // dtMatDetail
                        if (dtMatDetail.Rows.Count > 0)
                        {
                            // 상세정보 저장
                            strErrRtn = clsDetail.mfSaveISOMachineUsingReqMatD(dtMatDetail, strUserID, strUserIP, sql.SqlCon, trans, strRtnStdNumber, strRtnStdSeq);

                            // 결과확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // dtApproveDetail
                        if (dtApproveDetail.Rows.Count > 0)
                        {
                            // 상세정보 저장
                            strErrRtn = clsDetail.mfSaveISOMachineUsingReqApproveD(dtApproveDetail, strUserID, strUserIP, sql.SqlCon, trans, strRtnStdNumber, strRtnStdSeq);

                            // 결과확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        //SendEmail
                        if (dtApproveDetail.Rows.Count > 0)
                        {
                            // 상세정보 저장
                            strErrRtn = clsDetail.mfSaveISOMachineUsingReqApproveSendEmail( sql.SqlCon, trans, strRtnStdNumber, strRtnStdSeq);

                            // 결과확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                            strErrRtn = clsDetail.mfSaveISOMachineUsingReqHHistory(dtHeader, strUserID, strUserIP, sql.SqlCon, trans, strRtnStdNumber, strRtnStdSeq);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        
                        trans.Commit();

                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete(false)]
        public String mfApproveISOMachineUsingReqH(String StdNumberH, String StdSeqH, String VersionNumH, String strApproveDept, String strDeptApproveUserId, String strEtcDescApprove)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 변수
                SqlTransaction trans;

                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, StdNumberH, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, StdSeqH, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.VarChar, VersionNumH, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strDeptApproveUserId", ParameterDirection.Input, SqlDbType.VarChar, strDeptApproveUserId, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDescApprove", ParameterDirection.Input, SqlDbType.VarChar, strEtcDescApprove, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strApproveDept", ParameterDirection.Input, SqlDbType.VarChar, strApproveDept, 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISOMachineUsingReqHApprove", dtParam);

                    // 결과확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // Error가 발생했으면 Rollback하고 For문 빠져나간다
                        trans.Rollback();
                    }
                    else
                    {
                        trans.Commit();
                    }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 헤더정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOProcessInspectSpecH(String strPlantCode, String strStdNumber, String strStdSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();

                //공정검사규격서 DeleteFlag T 로변경
                string strErr = mfSaveISOProcInspectSpecH_MDMDeleteFlag(strPlantCode, strStdNumber, strStdSeq, "T", sql.SqlCon);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                {


                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 상세정보부터 삭제
                    QRPISO.BL.ISOPRC.ProcessInspectSpecD clsDetail = new ProcessInspectSpecD();

                    strErrRtn = clsDetail.mfDeleteISOProcessInspectSpecDAll(strPlantCode, strStdNumber, strStdSeq, sql.SqlCon, trans);
                    // 결과확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        strErrRtn = clsDetail.mfDeleteISOPrecessInspectSpecHHistory(strPlantCode, strStdNumber, strStdSeq, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                        else
                        {
                            // 헤더정보 삭제
                            DataTable dtParam = sql.mfSetParamDataTable();

                            sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                            sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                            sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                            sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                            // SP 실행
                            strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISOProcessInspectSpecH", dtParam);

                            // 결과확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                            else
                            {
                                trans.Commit();
                            }
                        }
                    }

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        //공정검사규격서 DeleteFlag T 로변경
                        strErr = mfSaveISOProcInspectSpecH_MDMDeleteFlag(strPlantCode, strStdNumber, strStdSeq, "F", sql.SqlCon);

                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 헤더정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOMachineUsingReqH(String strPlantCode, String strStdNumber, String strStdSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();

                    // 트랜잭션 시작
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    // 상세정보부터 삭제
                    QRPISO.BL.ISOPRC.ProcessInspectSpecD clsDetail = new ProcessInspectSpecD();

                    strErrRtn = clsDetail.mfDeleteISOMachineUsingReqDAll(strPlantCode, strStdNumber, strStdSeq, sql.SqlCon, trans);
                    // 결과확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        strErrRtn = clsDetail.mfDeleteISOMachineUsingReqMatDAll(strPlantCode, strStdNumber, strStdSeq, sql.SqlCon, trans);
                     
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }

                        strErrRtn = clsDetail.mfDeleteISOMachineUsingReqApproveDAll(strPlantCode, strStdNumber, strStdSeq, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            return strErrRtn;
                        }
                        else
                        {
                            strErrRtn = clsDetail.mfDeleteISOMachineUsingReqHHistory(strPlantCode, strStdNumber, strStdSeq, sql.SqlCon, trans);
                            
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                            else 
                            {
                                // 헤더정보 삭제
                                DataTable dtParam = sql.mfSetParamDataTable();

                                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                                // SP 실행
                                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISOMachineUsingReqH", dtParam);

                                // 결과확인
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum != 0)
                                {
                                    trans.Rollback();
                                    return strErrRtn;
                                }
                                else
                                {
                                    trans.Commit();
                                }
                            }
                        }
                    }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정검사규격서 헤더정보 MDM전송
        /// </summary>
        /// <param name="dtHeader"></param>
        /// <param name="dtDetail"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISOProcInspectSpecH_MDM_T(string strPlantCode, string strStdNumber, string strStdSeq, int intVersionNum, SqlConnection sqlcon)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 22);
                sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, intVersionNum.ToString());

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시져 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_ISOProcInspectSpecH_MDM_T", dtParam);

                // 결과확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;
                else
                {
                    strErrRtn = mfSaveISOProcInspectSpecD_MDM_T(strPlantCode, strStdNumber, strStdSeq, intVersionNum, sqlcon);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { 
                sql.Dispose();
            }
        }

        /// <summary>
        /// 공정검사규격서 상세 MDM 전송
        /// </summary>
        /// <param name="dtDetail"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISOProcInspectSpecD_MDM_T(string strPlantCode, string strStdNumber, string strStdSeq, int intVersionNum, SqlConnection sqlcon)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 22);
                sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, intVersionNum.ToString());

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행                    
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_ISOProcInspectSpecD_MDM_T", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { 
                sql.Dispose();
            }
        }


        /// <summary>
        /// 공정검사규격서 헤더정보 MDM전송
        /// </summary>
        /// <param name="dtHeader"></param>
        /// <param name="dtDetail"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISOProcInspectSpecH_MDM(DataTable dtHeader,DataTable dtDetail,string strStdNumber,string strStdSeq,SqlConnection sqlcon)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 파라미터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq,22);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CustomerCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 250);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc1", ParameterDirection.Input, SqlDbType.Text, dtHeader.Rows[i]["EtcDesc1"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc2", ParameterDirection.Input, SqlDbType.Text, dtHeader.Rows[i]["EtcDesc2"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc3", ParameterDirection.Input, SqlDbType.Text, dtHeader.Rows[i]["EtcDesc3"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc4", ParameterDirection.Input, SqlDbType.Text, dtHeader.Rows[i]["EtcDesc4"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_ISOProcInspectSpecH_MDM", dtParam);

                    // 결과확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        // Error가 발생했으면 For문 빠져나간다
                        break;
                    else
                    {
                        //공정검사 상세 삭제Flag T로 변경
                        strErrRtn = mfSaveISOProcInspectSpecD_MDMDeleteFlag(dtHeader.Rows[i]["PlantCode"].ToString(), strStdNumber, strStdSeq, sqlcon);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        
                        //공정검사규격서 상세 MDM 등록
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            if (dtDetail.Rows.Count > 0)
                            {
                                strErrRtn = mfSaveISOProcInspectSpecD_MDM(dtDetail, strStdNumber, strStdSeq, sqlcon);

                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                if (!ErrRtn.ErrNum.Equals(0))
                                    break;
                            }
                        }
                        else
                            break;
                        

                    }
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { sql.Dispose(); }
        }


        /// <summary>
        /// 공정검사규격서헤더 MDM DeleteFlag 변경
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="strDeleteFlag"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISOProcInspectSpecH_MDMDeleteFlag(string strPlantCode,
                                                                string strStdNumber,
                                                                string strStdSeq,
                                                                string strDeleteFlag,
                                                                SqlConnection sqlcon)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,30);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,4);
                sql.mfAddParamDataRow(dtParam,"@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar,strStdNumber,40);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq,22);
                sql.mfAddParamDataRow(dtParam,"@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.Char,strDeleteFlag,1);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_ISOProcInspectSpecH_MDMDeleteFlag", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (!ErrRtn.ErrNum.Equals(0))
                    return strErrRtn;


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { sql.Dispose(); }
        }



        /// <summary>
        /// 공정검사규격서 상세 MDM 전송
        /// </summary>
        /// <param name="dtDetail"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="sqlcon"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISOProcInspectSpecD_MDM(DataTable dtDetail, string strStdNumber, string strStdSeq,SqlConnection sqlcon)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq,22);
                    sql.mfAddParamDataRow(dtParam, "@i_intItemNum", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ItemNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ProcessCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectGroupCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectItemCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["StackSeq"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["Generation"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessInspectFlag", ParameterDirection.Input, SqlDbType.Char, dtDetail.Rows[i]["ProcessInspectFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductItemFlag", ParameterDirection.Input, SqlDbType.Char, dtDetail.Rows[i]["ProductItemFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualityItemFlag", ParameterDirection.Input, SqlDbType.Char, dtDetail.Rows[i]["QualityItemFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strInspectCondition", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectCondition"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strMethod", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["Method"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["SpecDesc"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["MeasureToolCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUpperSpec", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["UpperSpec"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strLowerSpec", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["LowerSpec"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["SpecRange"].ToString(), 40);
                    //sql.mfAddParamDataRow(dtParam, "@i_strSpecUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["SpecUnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSampleSize", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["SampleSize"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessInspectSS", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ProcessInspectSS"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductItemSS", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ProductItemSS"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualityItemSS", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["QualityItemSS"].ToString(),40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["UnitCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectPeriod", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectPeriod"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strPeriodUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PeriodUnitCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["DataType"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["EtcDesc"].ToString(), 40);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행                    
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_ISOProcInspectSpecD_MDM", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { sql.Dispose(); }
        }


        /// <summary>
        /// 공정검사규격서 상세 MDM DeleteFlag 변경
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="sqlCon"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISOProcInspectSpecD_MDMDeleteFlag(String strPlantCode, String strStdNumber, String strStdSeq, SqlConnection sqlCon)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq,22);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, "up_Update_ISOProcInspectSpecD_MDMDeleteFlag", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 공정검사규격서 MDM 전송성공 Flag 변경
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="strVersionNum"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISOProcInspectSpecH_MDMTFlag(string strPlantCode, 
                                                        string strStdNumber, 
                                                        string strStdSeq, 
                                                        string strVersionNum, 
                                                        string strUserIP, 
                                                        string strUserID,
                                                        SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, strVersionNum);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlcon,trans,"up_Update_ISOProcInspectSpecH_MDMFlag",dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcessInspectSpecD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcessInspectSpecD : ServicedComponent     
    {
        /// <summary>
        /// DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtDetail = new DataTable();
            try
            {
                dtDetail.Columns.Add("PlantCode", typeof(String));
                dtDetail.Columns.Add("StdNumber", typeof(String));
                dtDetail.Columns.Add("StdSeq", typeof(String));
                dtDetail.Columns.Add("VersionNum", typeof(Int32));
                dtDetail.Columns.Add("ItemNum", typeof(Int32));
                dtDetail.Columns.Add("Seq", typeof(Int32));
                dtDetail.Columns.Add("ProcessCode", typeof(String));
                dtDetail.Columns.Add("ProcessSeq", typeof(Int32));
                dtDetail.Columns.Add("InspectGroupCode", typeof(String));
                dtDetail.Columns.Add("InspectTypeCode", typeof(String));
                dtDetail.Columns.Add("InspectItemCode", typeof(String));
                dtDetail.Columns.Add("StackSeq", typeof(String));
                dtDetail.Columns.Add("Generation", typeof(String));    
                dtDetail.Columns.Add("ProcessInspectFlag", typeof(String));
                dtDetail.Columns.Add("ProductItemFlag", typeof(String));
                dtDetail.Columns.Add("QualityItemFlag", typeof(String));
                dtDetail.Columns.Add("InspectCondition", typeof(String));
                dtDetail.Columns.Add("Method", typeof(String));
                dtDetail.Columns.Add("SpecDesc", typeof(String));
                dtDetail.Columns.Add("MeasureToolCode", typeof(String));
                dtDetail.Columns.Add("UpperSpec", typeof(Double));
                dtDetail.Columns.Add("LowerSpec", typeof(Double));
                dtDetail.Columns.Add("SpecRange", typeof(String));
                dtDetail.Columns.Add("SpecUnitCode", typeof(String));
                dtDetail.Columns.Add("SampleSize", typeof(Double));
                dtDetail.Columns.Add("ProcessInspectSS", typeof(Double));
                dtDetail.Columns.Add("ProductItemSS", typeof(Double));
                dtDetail.Columns.Add("QualityItemSS", typeof(Double));
                dtDetail.Columns.Add("UnitCode", typeof(String));
                dtDetail.Columns.Add("InspectPeriod", typeof(String));
                dtDetail.Columns.Add("PeriodUnitCode", typeof(String));
                dtDetail.Columns.Add("CompareFlag", typeof(String));
                dtDetail.Columns.Add("DataType", typeof(String));
                dtDetail.Columns.Add("EtcDesc", typeof(String));
                dtDetail.Columns.Add("AQLFLag", typeof(string));

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_MachineUsingReqEquD()
        {
            DataTable dtDetail = new DataTable();
            try
            {
                dtDetail.Columns.Add("PlantCode", typeof(String));
                dtDetail.Columns.Add("StdNumber", typeof(String));
                dtDetail.Columns.Add("StdSeq", typeof(String));
                dtDetail.Columns.Add("VersionNum", typeof(Int32));
                dtDetail.Columns.Add("ItemNum", typeof(Int32));
                dtDetail.Columns.Add("Seq", typeof(Int32));
                dtDetail.Columns.Add("EquipTypeCode", typeof(String));
                dtDetail.Columns.Add("EquipCode", typeof(String));
                dtDetail.Columns.Add("StartDateTime", typeof(String));
                dtDetail.Columns.Add("EndDateTime", typeof(String));
                dtDetail.Columns.Add("StartDateTime2", typeof(String));
                dtDetail.Columns.Add("EndDateTime2", typeof(String));
                dtDetail.Columns.Add("EquipApproveUserId", typeof(String));

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo_MachineUsingReqMatD()
        {
            DataTable dtDetail = new DataTable();
            try
            {
                dtDetail.Columns.Add("PlantCode", typeof(String));
                dtDetail.Columns.Add("StdNumber", typeof(String));
                dtDetail.Columns.Add("StdSeq", typeof(String));
                dtDetail.Columns.Add("VersionNum", typeof(Int32));
                dtDetail.Columns.Add("ItemNum", typeof(Int32));
                dtDetail.Columns.Add("Seq", typeof(Int32));
                dtDetail.Columns.Add("ConsumableTypeCode", typeof(String));
                dtDetail.Columns.Add("MaterialName", typeof(String));

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                dtDetail.Dispose();
            }
        }

        public DataTable mfSetDataInfo_MachineUsingReqApproveD()
        {
            DataTable dtDetail = new DataTable();
            try
            {
                dtDetail.Columns.Add("PlantCode", typeof(String));
                dtDetail.Columns.Add("StdNumber", typeof(String));
                dtDetail.Columns.Add("StdSeq", typeof(String));
                dtDetail.Columns.Add("VersionNum", typeof(Int32));
                dtDetail.Columns.Add("ItemNum", typeof(Int32));
                dtDetail.Columns.Add("Seq", typeof(Int32));
                dtDetail.Columns.Add("DeptCode", typeof(String));
                dtDetail.Columns.Add("ApproveDeptUserId", typeof(String));

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOProcessInspectSpecD(String strPlantCode, String strStdNumber, String strStdSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcessInspectSpecD", dtParam);

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMachineUsingReqEquD(String strPlantCode, String strStdNumber, String strStdSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMachineUsingReqEquD", dtParam);

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMachineUsingReqMatD(String strPlantCode, String strStdNumber, String strStdSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMachineUsingReqMatD", dtParam);

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMachineUsingReqApproveD(String strPlantCode, String strStdNumber, String strStdSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMachineUsingReqApproveD", dtParam);

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 상세 이력정보 조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOProcessInspectSpecHHistory(String strPlantCode, String strStdNumber, String strStdSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDetailInfo = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDetailInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcessInspectSpecHHistory", dtParam);

                return dtDetailInfo;
            }
            catch (Exception ex)
            {
                return dtDetailInfo;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDetailInfo.Dispose();
            }
        }

        /// <summary>
        /// 상세 이력정보 저장
        /// </summary>
        /// <param name="dtDetail"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection변수 </param>
        /// <param name="trans"> Transaction변수 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호 순서 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOProcessInspectSpecHHistory(DataTable dtHeader, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans,
                                                    String strStdNumber, String strStdSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행                    
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOProcessInspectSpecHHistory", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세 이력정보 저장
        /// </summary>
        /// <param name="dtDetail"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection변수 </param>
        /// <param name="trans"> Transaction변수 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호 순서 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOMachineUsingReqHHistory(DataTable dtHeader, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans,
                                                    String strStdNumber, String strStdSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행                    
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOMachineUsingReqHHistory", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 복사할때 팝업 상세 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="intVersionNum"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfRedISOProcessInspectSpecDPop(string strPlantCode, string strStdNumber, string strStdSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcessInspectSpecDPop", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 복사할 검사항목 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strStdSeq"></param>
        /// <param name="intVersionNum"></param>
        /// <param name="intItemNum"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOProcessInspectSpecD_Copy(string strPlantCode, string strStdNumber, string strStdSeq, int intVersionNum, int intItemNum, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, intVersionNum);
                sql.mfAddParamDataRow(dtParam, "@i_intItemNum", ParameterDirection.Input, SqlDbType.Int, intItemNum);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang,3);
 
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcessInspectSpecD_Copy", dtParam);
                return dtRtn;

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 상세정보 저장
        /// </summary>
        /// <param name="dtDetail"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection변수 </param>
        /// <param name="trans"> Transaction변수 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호 순서 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOProcessInspectSpecD(DataTable dtDetail, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans,
                                                    String strStdNumber, String strStdSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();
                
                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intItemNum", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ItemNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intProcessSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ProcessSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectItemCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["StackSeq"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strGeneration", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["Generation"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessInspectFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ProcessInspectFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductItemFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ProductItemFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualityItemFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["QualityItemFlag"].ToString(), 1);                                        
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectCondition", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["InspectCondition"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMethod", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["Method"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["SpecDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["MeasureToolCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_dblUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["UpperSpec"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["LowerSpec"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["SpecRange"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["SpecUnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_dblSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["SampleSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblProcessInspectSS", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["ProcessInspectSS"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblProductItemSS", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["ProductItemSS"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblQualityItemSS", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["QualityItemSS"].ToString());                   
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectPeriod", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectPeriod"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPeriodUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PeriodUnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompareFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["CompareFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["DataType"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["EtcDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAQLFlag", ParameterDirection.Input, SqlDbType.Char, dtDetail.Rows[i]["AQLFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행                    
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOProcessInspectSpecD", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if(ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 저장
        /// </summary>
        /// <param name="dtDetail"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection변수 </param>
        /// <param name="trans"> Transaction변수 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호 순서 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOMachineUsingReqD(DataTable dtDetail, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans,
                                                    String strStdNumber, String strStdSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intItemNum", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ItemNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["Seq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["EquipTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["EquipCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStartDateTime", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["StartDateTime"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strEndDateTime", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["EndDateTime"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strStartDateTime2", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["StartDateTime2"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strEndDateTime2", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["EndDateTime2"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipApproveUserId", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["EquipApproveUserId"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행                    
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOMachineUsingReqD", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 저장
        /// </summary>
        /// <param name="dtDetail"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection변수 </param>
        /// <param name="trans"> Transaction변수 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호 순서 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOMachineUsingReqMatD(DataTable dtMatDetail, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans,
                                                    String strStdNumber, String strStdSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                for (int i = 0; i < dtMatDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMatDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtMatDetail.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intItemNum", ParameterDirection.Input, SqlDbType.Int, dtMatDetail.Rows[i]["ItemNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtMatDetail.Rows[i]["Seq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strConsumableTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtMatDetail.Rows[i]["ConsumableTypeCode"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialName", ParameterDirection.Input, SqlDbType.VarChar, dtMatDetail.Rows[i]["MaterialName"].ToString(), 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행                    
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOMachineUsingReqMatD", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 저장
        /// </summary>
        /// <param name="dtDetail"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection변수 </param>
        /// <param name="trans"> Transaction변수 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호 순서 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOMachineUsingReqApproveD(DataTable dtApproveDetail, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans,
                                                    String strStdNumber, String strStdSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                for (int i = 0; i < dtApproveDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtApproveDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtApproveDetail.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intItemNum", ParameterDirection.Input, SqlDbType.Int, dtApproveDetail.Rows[i]["ItemNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtApproveDetail.Rows[i]["Seq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtApproveDetail.Rows[i]["DeptCode"].ToString(), 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strApproveDeptUserId", ParameterDirection.Input, SqlDbType.VarChar, dtApproveDetail.Rows[i]["ApproveDeptUserId"].ToString(), 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행                    
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOMachineUsingReqApproveD", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 저장
        /// </summary>
        /// <param name="sqlCon"> SqlConnection변수 </param>
        /// <param name="trans"> Transaction변수 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호 순서 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOMachineUsingReqApproveSendEmail(SqlConnection sqlCon, SqlTransaction trans,
                                                    String strStdNumber, String strStdSeq)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행                    
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOMachineUsingReqApproveSendEmail", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        return strErrRtn;
                    }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 모두 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장정보 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="sqlCon"> SQLConnection </param>
        /// <param name="trans"> Transaction </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOProcessInspectSpecDAll(String strPlantCode, String strStdNumber, String strStdSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_ISOProcessInspectSpecDAll", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 모두 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장정보 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="sqlCon"> SQLConnection </param>
        /// <param name="trans"> Transaction </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOMachineUsingReqDAll(String strPlantCode, String strStdNumber, String strStdSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_ISOMachineUsingReqDAll", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 모두 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장정보 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="sqlCon"> SQLConnection </param>
        /// <param name="trans"> Transaction </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOMachineUsingReqMatDAll(String strPlantCode, String strStdNumber, String strStdSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_ISOMachineUsingReqMatDAll", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 모두 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장정보 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="sqlCon"> SQLConnection </param>
        /// <param name="trans"> Transaction </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOMachineUsingReqApproveDAll(String strPlantCode, String strStdNumber, String strStdSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_ISOMachineUsingReqApproveDAll", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세 이력정보 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장정보 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="sqlCon"> SQLConnection </param>
        /// <param name="trans"> Transaction </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOPrecessInspectSpecHHistory(String strPlantCode, String strStdNumber, String strStdSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_ISOProcessInspectSpecHHistory", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세 이력정보 삭제
        /// </summary>
        /// <param name="strPlantCode"> 공장정보 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="sqlCon"> SQLConnection </param>
        /// <param name="trans"> Transaction </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOMachineUsingReqHHistory(String strPlantCode, String strStdNumber, String strStdSeq, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_ISOMachineUsingReqHHistory", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }

    }
}
