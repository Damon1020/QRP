﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 표준문서관리                                          */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : clsISODOC.cs                                          */
/* 프로그램명   : 표준문서관리 대/중분류                                */
/* 작성자       : 정 결                                                 */
/* 작성일자     : 2011-08-08                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                                                                      */
/*----------------------------------------------------------------------*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;
using System.Data.SqlClient;
using System.EnterpriseServices;

//using System.Data.SqlClient;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                    Authentication = AuthenticationOption.None,
                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPISO.BL.ISODOC
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocLType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]


    public class ISODocLType : ServicedComponent
    {
        /// <summary>
        /// 표준문서 대분류 정보 컬럼 설정
        /// </summary>
        public DataTable mfSetDateInfo()
        {
            DataTable dtISODocLType = new DataTable();
            try
            {

                dtISODocLType.Columns.Add("PlantCode", typeof(String));
                dtISODocLType.Columns.Add("LTypeCode", typeof(String));
                dtISODocLType.Columns.Add("LTypeName", typeof(String));
                dtISODocLType.Columns.Add("LTypeNameCh", typeof(String));
                dtISODocLType.Columns.Add("LTypeNameEn", typeof(String));
                dtISODocLType.Columns.Add("AutoStdNumberFlag", typeof(String));
                dtISODocLType.Columns.Add("GRWFlag", typeof(String));
                dtISODocLType.Columns.Add("UseFlag", typeof(Char));
                //정보리턴
                return dtISODocLType;

            }
            catch (Exception ex)
            {
                return dtISODocLType;
                throw (ex);
            }
            finally
            {
                dtISODocLType.Dispose();
            }
        }
        /// <summary>
        /// 표준문서 대분류 정보
        /// </summary>
        /// <param name="strLang">언어</param>
        /// <returns>대분류 정보 전체</returns>
        [AutoComplete]
        public DataTable mfReadISODocLType(string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocLType = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //call pocedure
                dtISODocLType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocLType", dtParam);
                //리턴
                return dtISODocLType;
            }
            catch (Exception ex)
            {
                return dtISODocLType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocLType.Dispose();
            }
        }

        /// <summary>
        /// 대분류 전자결제 여부 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLTypeCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocLTypeGRWFlag(string strPlantCode, string strLTypeCode)
        {
            SQLS sql = new SQLS();
            DataTable dtLType = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);

                dtLType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHLTypeGRWFlag", dtParam);
                return dtLType;
            }
            catch (Exception ex)
            {
                return dtLType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtLType.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 대분류 저장
        /// </summary>
        /// <param name="dtGroupList">저장값이 담아있는DateTabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveISODocLType(DataTable dtISODocLType, string strUserIP, string strUserID, DataTable dtSaveMType, DataTable dtDelMType)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //open db
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dtISODocLType.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocLType.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocLType.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocLType.Rows[i]["LTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocLType.Rows[i]["LTypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocLType.Rows[i]["LTypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAutoStdNumberFlag", ParameterDirection.Input, SqlDbType.Char, dtISODocLType.Rows[i]["AutoStdNumberFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRWFlag", ParameterDirection.Input, SqlDbType.Char, dtISODocLType.Rows[i]["GRWFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtISODocLType.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocLType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        //성공시 처리
                        //삭제
                        if (dtDelMType.Rows.Count > 0)
                        {
                            ISODOC.ISODocMType dType = new ISODocMType();
                            strErrRtn = dType.mfDeleteISODocMType(dtDelMType, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        //저장
                        if (dtSaveMType.Rows.Count > 0)
                        {
                            ISODOC.ISODocMType sType = new ISODocMType();
                            strErrRtn = sType.mfSaveISODocMType(dtSaveMType, strUserID, strUserIP, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                    }
                }
                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        /// <summary>
        /// 표준문서 대분류 삭제
        /// </summary>
        /// <param name="dtISODocLType">저장값이 담아있는DateTabel</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteISODocLType(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    ////소분류
                    //ISODocSType clsStype = new ISODocSType();
                    //strErrRtn = clsStype.mfDeleteISODocSTypeAll(dt, sql, trans);
                    //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //if (ErrRtn.ErrNum != 0)
                    //{
                    //    trans.Rollback();
                    //    break;
                    //}
                    ////중분류
                    //ISODocMType clsMtype = new ISODocMType();
                    //strErrRtn = clsMtype.mfDeleteISODocMTypeAll(dt, sql, trans);
                    //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    ////검사
                    //if (ErrRtn.ErrNum != 0)
                    //{
                    //    trans.Rollback();
                    //    break;
                    //}

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocLType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }

        }
        /// <summary>
        /// 표준문서 대분류 ComboBox
        /// </summary>
        /// <param name="strLang"></param>
        /// <returns></returns>


        [AutoComplete]
        public DataTable mfReadISODocLTypeCombo(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocLTypeCombo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        public void aaa()
        {

        }

        /// <summary>
        /// 대분류 삭제시, ISODocH에서 사용하는 대분류인지 확인
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLTypeCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHLType(String strPlantCode, String strLTypeCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHLTypeCode", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
    }


    /// <summary>
    /// 표준문서 중분류
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocMType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class ISODocMType : ServicedComponent
    {
        /// <summary>
        /// 표준문서 중분류 정보
        /// </summary>
        [AutoComplete]
        public DataTable mfSetDateInfo()
        {
            DataTable dtISODocMType = new DataTable();
            try
            {

                dtISODocMType.Columns.Add("PlantCode", typeof(String));
                dtISODocMType.Columns.Add("LTypeCode", typeof(String));
                dtISODocMType.Columns.Add("MTypeCode", typeof(String));
                dtISODocMType.Columns.Add("MTypeName", typeof(String));
                dtISODocMType.Columns.Add("MTypeNameCh", typeof(String));
                dtISODocMType.Columns.Add("MTypeNameEn", typeof(String));
                dtISODocMType.Columns.Add("UseFlag", typeof(Char));
                //정보리턴
                return dtISODocMType;

            }
            catch (Exception ex)
            {
                return dtISODocMType;
                throw (ex);
            }
            finally
            {
                dtISODocMType.Dispose();
            }
        }
        /// <summary>
        /// 표준문서 중분류 정보
        /// </summary>
        /// <param name="strLang">언어</param>
        /// <returns>중분류 정보 전체</returns>

        [AutoComplete]
        public DataTable mfReadISODocMType(String strPlantCode, String strLTypeCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocMType = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //call pocedure
                dtISODocMType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocMType", dtParam);
                //리턴
                return dtISODocMType;
            }
            catch (Exception ex)
            {
                return dtISODocMType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocMType.Dispose();
            }
        }



        /// <summary>
        /// 표준문서 중분류 저장
        /// </summary>
        /// <param name="dtGroupList">저장값이 담아있는DateTabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <param name="sqlCon">sql Connection</param>
        /// <param name="trans">트렌젝션 변수</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveISODocMType(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                //open db
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //Transaction 시작
                    //trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장 
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MTypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MTypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["MTypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocMType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    //    trans.Rollback();
                    //else
                    //    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex); 
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }
        /// <summary>
        /// 표준문서 중분류 삭제
        /// </summary>
        /// <param name="dtGroupList">저장값이 담아있는DateTabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteISODocMType(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocMType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);

            }
            finally
            {
            }
        }

        /// <summary>
        /// MType전체 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public string mfDeleteISODocMTypeAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocMTypeALL", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
            }
        }
        public string mfDeleteISODocMTypeWithSType(DataTable dtMType)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtMType.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    ////소분류
                    //ISODocSType clsStype = new ISODocSType();
                    //strErrRtn = clsStype.mfDeleteISODocSTypeAll(dtMType, sql, trans);
                    //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //if (ErrRtn.ErrNum != 0)
                    //{
                    //    trans.Rollback();
                    //    break;
                    //}

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMType.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtMType.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtMType.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocMTypeWithSType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 중분류 콤보박스
        /// </summary>
        /// <param name="PlantCode"></param>
        /// <param name="LTypeCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>

        [AutoComplete]
        public DataTable mfReadISODocMTypeCombo(String strPlantCode, String strLTypeCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocMTypeCombo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }




    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocSType")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class ISODocSType : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        /// <summary>
        /// Debug모드용 생성자
        /// </summary>
        /// <param name="strDBConn"></param>
        public ISODocSType(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();

        }
        public ISODocSType()
        {

        }



        /// <summary>
        /// 표준문서 소분류 정보
        /// </summary>
        [AutoComplete]
        public DataTable mfSetDateInfo()
        {
            DataTable dtISODocSType = new DataTable();
            try
            {

                dtISODocSType.Columns.Add("PlantCode", typeof(String));
                dtISODocSType.Columns.Add("LTypeCode", typeof(String));
                dtISODocSType.Columns.Add("MTypeCode", typeof(String));
                dtISODocSType.Columns.Add("STypeCode", typeof(String));
                dtISODocSType.Columns.Add("STypeName", typeof(String));
                dtISODocSType.Columns.Add("STypeNameCh", typeof(String));
                dtISODocSType.Columns.Add("STypeNameEn", typeof(String));
                dtISODocSType.Columns.Add("USeFlag", typeof(Char));
                //정보리턴
                return dtISODocSType;

            }
            catch (Exception ex)
            {
                return dtISODocSType;
                throw (ex);
            }
            finally
            {
                dtISODocSType.Dispose();
            }
        }
        /// <summary>
        /// 표준문서 소분류 검색
        /// </summary>
        /// <param name="strLang">언어</param>
        /// <returns>소분류 정보 전체</returns>

        [AutoComplete]
        public DataTable mfReadISODocSType(string strPlantCode, string strLTypeCode, string strMTypeCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocSType = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTypeCode, 10);

                //call pocedure
                dtISODocSType = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocSType", dtParam);
                //리턴
                return dtISODocSType;
            }
            catch (Exception ex)
            {
                return dtISODocSType;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocSType.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 소분류 콤보박스
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLTypeCode"></param>
        /// <param name="strMTypeCode"></param>
        /// <param name="strLang"></param> 
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocSTypeCombo(String strPlantCode, String strLTypeCode, String strMTypeCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTypeCode, 10);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocSTypeCombo", dtParam);
                return dt;

            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }



        /// <summary>
        /// 표준문서 소분류 저장
        /// </summary>
        /// <param name="dtGroupList">저장값이 담아있는DateTabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveISODocSType(DataTable dt, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //open db
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["STypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSTypeName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["STypeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSTypeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["STypeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSTypeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["STypeNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocSType", dtParam);
                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_ISODocSType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        /// <summary>
        /// 표준문서 소분류 삭제
        /// </summary>
        /// <param name="dtGroupList">저장값이 담아있는DateTabel</param>
        /// <param name="strUserID">사용자 ID</param>
        /// <param name="strUserIP">사용자 IP</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public String mfDeleteISODocSType(DataTable dt)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["STypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocSType", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete]
        public String mfDeleteISODocSTypeWithMType(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocSTypeWithMType");
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            { }
        }

        /// <summary>
        /// 표준문서 소분류 전체 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocSTypeAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocSTypeAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
            }
        }
    }

    /// <summary>
    /// 표준문서 배포업체
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocVendor")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]




    public class ISODocVendor : ServicedComponent
    {
        /// <summary>
        /// 표준문서 배포업체정보 
        /// </summary>
        [AutoComplete]
        public DataTable mfSetDateInfo()
        {
            DataTable dtISODocVendor = new DataTable();
            try
            {
                dtISODocVendor.Columns.Add("PlantCode", typeof(String));
                dtISODocVendor.Columns.Add("StdNumber", typeof(String));
                dtISODocVendor.Columns.Add("VersionNum", typeof(String));
                dtISODocVendor.Columns.Add("Seq", typeof(int));
                dtISODocVendor.Columns.Add("VendorCode", typeof(String));
                //dtISODocVendor.Columns.Add("VendorName", typeof(String));
                dtISODocVendor.Columns.Add("DeptName", typeof(String));
                dtISODocVendor.Columns.Add("PersonName", typeof(String));
                dtISODocVendor.Columns.Add("Hp", typeof(String));
                dtISODocVendor.Columns.Add("Email", typeof(String));
                dtISODocVendor.Columns.Add("EtcDesc", typeof(String));
                return dtISODocVendor;
            }
            catch (Exception ex)
            {
                return dtISODocVendor;
                throw (ex);
            }
            finally
            {
                dtISODocVendor.Dispose();
            }

        }

        /// <summary>
        /// AVL팝업 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocVendorPopup(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocVendor = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtISODocVendor = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSAVLPopup", dtParam);

                return dtISODocVendor;
            }
            catch (Exception ex)
            {
                return dtISODocVendor;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocVendor.Dispose();
            }
        }



        /// <summary>
        /// 표준문서 배포업체 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strVersionNum"></param>
        /// <param name="strSeq"></param>
        /// <param name="strVendorCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocVendor(String strPlantCode, String strStdNumber, String strVersionNum, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocVendor = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocVendor = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocVendor", dtParam);
                //리턴
                return dtISODocVendor;
            }
            catch (Exception ex)
            {
                return dtISODocVendor;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocVendor.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 배포처 검색(그리드)
        /// </summary>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocVendorGird(String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocVendorGrid", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 표준문서 배포처 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocVendor(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDeptName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["DeptName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPersonName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["PersonName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHp", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Hp"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEmail", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Email"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocVendor", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {

            }
        }







        [AutoComplete(false)]
        public string mfSaveISODocVendorGrid(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDeptName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["DeptName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPersonName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["PersonName"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHp", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Hp"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEmail", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["Email"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocVendor", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {

            }
        }


        /// <summary>
        /// 표준문서 배포업체 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocVendor(DataTable dt, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                //sql.mfConnect();
                //SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocVendor", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    //     trans.Rollback();
                    //else
                    //     trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }



        /// <summary>
        /// 그리드 삭제용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocVendorGrid(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocVendor", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }

        }


        /// <summary>
        /// 표준문서 배포처 전체 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public string mfDeleteISODocVendorAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocVendorALL", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
            }
        }
    }



    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocDept")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class ISODocDept : ServicedComponent
    {

        /// <summary>
        /// 표준문서 적용범위
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDateInfo()
        {
            DataTable dtISODocDept = new DataTable();
            try
            {
                dtISODocDept.Columns.Add("PlantCode", typeof(String));
                dtISODocDept.Columns.Add("StdNumber", typeof(String));
                dtISODocDept.Columns.Add("VersionNum", typeof(String));
                dtISODocDept.Columns.Add("ProcessGroup", typeof(String));
                dtISODocDept.Columns.Add("EtcDesc", typeof(String));
                //정보리턴
                return dtISODocDept;

            }
            catch (Exception ex)
            {
                return dtISODocDept;
                throw (ex);
            }
            finally
            {
                dtISODocDept.Dispose();
            }
        }



        /// <summary>
        /// 표준문서 적용범위 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strVersionNum"></param>
        /// <param name="strDeptCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocDept(String strPlantCode, String strStdNumber, String strVersionNum, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocDept = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocDept", dtParam);
                //리턴
                return dtISODocDept;
            }
            catch (Exception ex)
            {
                return dtISODocDept;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocDept.Dispose();
            }
        }


        /// <summary>
        /// 표준문서 적용범의 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocDept(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장 
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocDept", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 표준문서 적용범위 저장(그리드)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocDeptGrid(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장 
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocDept", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 적용범위 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocDept(DataTable dt, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocDept", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 그리드 삭제용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocDeptGrid(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ProcessGroup"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocDept", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }

        }



        /// <summary>
        /// 표준문서 적용범위 전체 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public string mfDeleteISODocDeptAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocDeptALL", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
            }
        }
    }
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocAgreeUser")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class ISODocAgreeUser : ServicedComponent
    {
        /// <summary>
        /// 표준문서 승인자 정보
        /// </summary>
        [AutoComplete]
        public DataTable mfSetDateInfo()
        {
            DataTable dtISODocAgreeUser = new DataTable();
            try
            {
                dtISODocAgreeUser.Columns.Add("PlantCode", typeof(String));
                dtISODocAgreeUser.Columns.Add("StdNumber", typeof(String));
                dtISODocAgreeUser.Columns.Add("VersionNum", typeof(String));
                dtISODocAgreeUser.Columns.Add("AgreeUserID", typeof(String));
                dtISODocAgreeUser.Columns.Add("AgreeUserName", typeof(String));
                dtISODocAgreeUser.Columns.Add("EtcDesc", typeof(String));

                return dtISODocAgreeUser;

            }
            catch (Exception ex)
            {
                return dtISODocAgreeUser;
                throw (ex);
            }
            finally
            {
                dtISODocAgreeUser.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 승인자 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocAgreeUser(String strPlantCode, String strStdNumber, String strVersionNum, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocAgreeUser = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocAgreeUser = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocAgreeUser", dtParam);
                //리턴
                return dtISODocAgreeUser;
            }
            catch (Exception ex)
            {
                return dtISODocAgreeUser;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocAgreeUser.Dispose();
            }
        }



        /// <summary>
        /// 표준문서 승인자 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocAgreeUser(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장 
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAgreeUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreeUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocAgreeUser", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 표준문서 승인자 저장(그리드)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocAgreeUserGrid(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장 
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAgreeUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreeUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocAgreeUser", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 표준문서 승인자 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocAgreeUser(DataTable dt, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAgreeUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreeUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocAgreeUser", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }



        /// <summary>
        /// 그리드 삭제용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocAgreeUserGrid(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAgreeuserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AgreeUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocAgreeUser", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }

        }



        /// <summary>
        /// 표준문서 승인자 전체 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        public string mfDeleteISODocAgreeUserAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            //SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocAgreeUserALL", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class ISODocFile : ServicedComponent
    {

        /// <summary>
        /// 표준문서 첨부파일 데이터 테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDatainfo()
        {
            DataTable dtISODocFile = new DataTable();
            try
            {
                dtISODocFile.Columns.Add("PlantCode", typeof(String));
                dtISODocFile.Columns.Add("StdNumber", typeof(String));
                dtISODocFile.Columns.Add("VersionNum", typeof(String));
                dtISODocFile.Columns.Add("AdmitVersionNum", typeof(String));
                dtISODocFile.Columns.Add("Seq", typeof(int));
                dtISODocFile.Columns.Add("FileTitle", typeof(String));
                dtISODocFile.Columns.Add("FileName", typeof(String));
                dtISODocFile.Columns.Add("DWFlag", typeof(String));
                dtISODocFile.Columns.Add("EtcDesc", typeof(String));

                return dtISODocFile;
            }
            catch (Exception ex)
            {
                return dtISODocFile;
                throw (ex);
            }
            finally
            {
                dtISODocFile.Dispose();
            }
        }
        /// <summary>
        /// 표준문서 첨부파일 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocFile(String strPlantCode, String strStdNumber, String strVersionNum, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocFile = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocFile", dtParam);
                //리턴
                return dtISODocFile;
            }
            catch (Exception ex)
            {
                return dtISODocFile;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocFile.Dispose();
            }
        }

        /// <summary>
        /// 파일 삭제용 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="intVersionNum"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocFileForDelFile(String strPlantCode, String strStdNumber, String strVersionNum)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocFile = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);

                dtISODocFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocFileForDelFile", dtParam);

                return dtISODocFile;

            }
            catch (Exception ex)
            {
                return dtISODocFile;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocFile.Dispose();
            }
        }


        /// <summary>
        /// LOT SPECVIEW
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadLOT_SpecViewFileName(string strPlantCode, DataTable dtInfo)
        {
            // Return Datable
            DataTable dtRtn = new DataTable();
            dtRtn.Columns.Add("Bondcode", typeof(string));
            dtRtn.Columns.Add("Markcode", typeof(string));
            dtRtn.Columns.Add("PKcode", typeof(string));
            dtRtn.Columns.Add("TFcode", typeof(string));

            DataRow drRow = dtRtn.NewRow();
            drRow["Bondcode"] = string.Empty;
            drRow["Markcode"] = string.Empty;
            drRow["PKcode"] = string.Empty;
            drRow["TFcode"] = string.Empty;

            dtRtn.Rows.Add(drRow);

            bool bdflag = true;
            bool mkflag = true;
            bool pkflag = true;
            bool tfflag = true;

            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();

                for (int i = 0; i < dtInfo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strBondSpec", ParameterDirection.Input, SqlDbType.VarChar, dtInfo.Rows[i]["BondSpecCode"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMarkSpec", ParameterDirection.Input, SqlDbType.VarChar, dtInfo.Rows[i]["MarkSpecCode"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPKSpec", ParameterDirection.Input, SqlDbType.VarChar, dtInfo.Rows[i]["PKSpecCode"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strTFSpec", ParameterDirection.Input, SqlDbType.VarChar, dtInfo.Rows[i]["TFSpecCode"].ToString(), 100);

                    DataTable dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocFileTOMES", dtParam);

                    if (dtResult.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtResult.Rows.Count; j++)
                        {
                            if (dtResult.Rows[j]["SPECTYPE"].ToString().Equals("BONDSEPCPATH"))
                            {
                                dtRtn.Rows[0]["Bondcode"] = dtResult.Rows[j]["FileName"].ToString();
                                bdflag = false;
                            }
                            
                            if (dtResult.Rows[j]["SPECTYPE"].ToString().Equals("MARKSPECPATH"))
                            {
                                dtRtn.Rows[0]["Markcode"] = dtResult.Rows[j]["FileName"].ToString();
                                mkflag = false;
                            }
                            if (dtResult.Rows[j]["SPECTYPE"].ToString().Equals("PKSPECPATH"))
                            {
                                dtRtn.Rows[0]["PKcode"] = dtResult.Rows[j]["FileName"].ToString();
                                mkflag = false;
                            }

                            if (dtResult.Rows[j]["SPECTYPE"].ToString().Equals("PKSPECPATH"))
                            {
                                dtRtn.Rows[0]["TFcode"] = dtResult.Rows[j]["FileName"].ToString();
                                tfflag = false;
                            }
                        }

                        if (bdflag)
                        {
                            dtRtn.Rows[0]["Bondcode"] = "N";
                        }

                        if (mkflag)
                        {
                            dtRtn.Rows[0]["Markcode"] = "N";
                        }

                        if (pkflag)
                        {
                            dtRtn.Rows[0]["PKcode"] = "N";
                        }
                        if (tfflag)
                        {
                            dtRtn.Rows[0]["TFcode"] = "N";
                        }
                    }
                    else
                    {
                        dtRtn.Rows[0]["Bondcode"] = "N";
                        dtRtn.Rows[0]["Markcode"] = "N";
                        dtRtn.Rows[0]["PKcode"] = "N";
                        dtRtn.Rows[0]["TFcode"] = "N";
                        break;
                    }
                }

                // Return 데이터 정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                // Return Error Info
                dtRtn.Rows[0]["Bondcode"] = "N";
                dtRtn.Rows[0]["Markcode"] = "N";
                dtRtn.Rows[0]["PKcode"] = "N";
                dtRtn.Rows[0]["TFcode"] = "N";

                return dtRtn;
                throw (ex);

            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 표준문서 첨부파일 삭제(전체삭제)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strStdNumber"></param>
        /// <returns></returns>
        public string mfDeleteISODocFileAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocFileALL", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        public string mfDeleteISODocFile(DataTable dt, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 삭제용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocFileGrid(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }

        }




        /// <summary>
        /// 표준문서 첨부파일 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strStdNumber"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocFile(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans, String strStdNumber)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFileTitle", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FileTitle"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FileName"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDWFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["DWFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }
        /// <summary>
        /// 승인시 개정번호 받아서 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strStdNumber"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocFile_Admit(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans, String strAdmitVersionNum)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strAdmitVersionNum, 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFileTitle", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FileTitle"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FileName"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDWFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["DWFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocFile_Approve", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public string mfSaveISODocFileGrid(DataTable dt, string strUserID, string strUserIP, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFileTitle", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FileTitle"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["FileName"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDWFlag", ParameterDirection.Input, SqlDbType.Char, dt.Rows[i]["DWFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }



    }



    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    /// <summary>
    /// 표준문서 헤더
    /// 
    /// </summary>
    public class ISODocH : ServicedComponent
    {
        /// <summary>
        /// 표준문서 헤더 DataSet
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDateInfo()
        {
            DataTable dtISODocH = new DataTable();
            try
            {
                dtISODocH.Columns.Add("PlantCode", typeof(String));
                dtISODocH.Columns.Add("StdNumber", typeof(String));
                dtISODocH.Columns.Add("VersionNum", typeof(String));
                dtISODocH.Columns.Add("AdmitVersionNum", typeof(String));
                dtISODocH.Columns.Add("ProcessCode", typeof(String));
                dtISODocH.Columns.Add("LTypeCode", typeof(String));
                dtISODocH.Columns.Add("MTypeCode", typeof(String));
                dtISODocH.Columns.Add("STypeCode", typeof(String));
                dtISODocH.Columns.Add("MakeUserName", typeof(String));
                dtISODocH.Columns.Add("MakeDeptName", typeof(String));
                dtISODocH.Columns.Add("WriteID", typeof(String));
                dtISODocH.Columns.Add("WriteDate", typeof(String));
                dtISODocH.Columns.Add("AdmitID", typeof(String));
                dtISODocH.Columns.Add("AdmitDate", typeof(String));
                dtISODocH.Columns.Add("AdmitStatus", typeof(String));
                dtISODocH.Columns.Add("RevDisuseType", typeof(String));
                dtISODocH.Columns.Add("RevDisuseID", typeof(String));
                dtISODocH.Columns.Add("RevDisuseDate", typeof(String));
                dtISODocH.Columns.Add("RevDisuseReason", typeof(String));
                dtISODocH.Columns.Add("DisuseReason", typeof(String));
                dtISODocH.Columns.Add("DisuseDate", typeof(String));
                dtISODocH.Columns.Add("ChangeItem", typeof(String));
                dtISODocH.Columns.Add("FromDesc", typeof(String));
                dtISODocH.Columns.Add("ToDesc", typeof(String));
                dtISODocH.Columns.Add("DocTitle", typeof(String));
                dtISODocH.Columns.Add("DocKeyword", typeof(String));
                dtISODocH.Columns.Add("DocDesc", typeof(String));
                dtISODocH.Columns.Add("GRWComment", typeof(String));
                dtISODocH.Columns.Add("ApplyDateFrom", typeof(String));
                dtISODocH.Columns.Add("ApplyDateTo", typeof(String));
                dtISODocH.Columns.Add("CompleteDate", typeof(String));
                dtISODocH.Columns.Add("ReturnReason", typeof(String));
                dtISODocH.Columns.Add("Package", typeof(String));
                dtISODocH.Columns.Add("ChipSize", typeof(String));
                dtISODocH.Columns.Add("PadSize", typeof(String));

                //정보리턴
                return dtISODocH;

            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                dtISODocH.Dispose();
            }
        }
        /// <summary>
        /// VersionNum 최대값 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMaxVersion(String strPlantCode, String strStdNumber)
        {
            SQLS sql = new SQLS();
            DataTable dtVNum = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                dtVNum = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocVersionNum", dtParam);
                return dtVNum;

            }
            catch (Exception ex)
            {
                return dtVNum;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVNum.Dispose();
            }
        }

        /// <summary>
        /// 대분류코드의 자동채번여부 확인
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLTypeCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadLTypeUse(String strPlantCode, String strLTypeCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHLTypeCode", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 표준문서 승인화면 헤더 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDocTitle"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocH(String strPlantCode, String strDocTitle, String strStdNumber, String strAdmitStatus, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, strDocTitle, 2000);
                sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatus", ParameterDirection.Input, SqlDbType.VarChar, strAdmitStatus, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 200);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHGrid", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 배포 조회용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="intVersionNum"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHForDistReq(String strPlantCode, String strStdNumber, String strVersionNum, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHSearchForDistReq", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }
        /// <summary>
        /// 표준문서 헤더 검색(개정/폐기)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLTypeCode"></param>
        /// <param name="strMTypeCode"></param>
        /// <param name="strSTypeCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strProcessCode"></param>
        /// <param name="strDocTitle"></param>
        /// <param name="strApplyDateFrom"></param>
        /// <param name="strApplyDateTo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHForDisuse(String strPlantCode, String strLTypeCode, String strMTypeCode, String strSTypeCode, String strStdNumber,
            String strProcessCode, String strDocTitle, String strVersionCheck, String strApplyDateFrom, String strApplyDateTo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strSTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.VarChar, strDocTitle, 2000);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionCheck", ParameterDirection.Input, SqlDbType.Char, strVersionCheck, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strApplyDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strApplyDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strApplyDateTo", ParameterDirection.Input, SqlDbType.VarChar, strApplyDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHForDisuse", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }


        /// <summary>
        /// 표준문서 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLTypeCode"></param>
        /// <param name="strMTypeCode"></param>
        /// <param name="strSTypeCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strProcessCode"></param>
        /// <param name="strDocTitle"></param>
        /// <param name="strApplyDateFrom"></param>
        /// <param name="strApplyDateTo"></param>
        /// <param name="strRevDisuseType"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHForSearch(String strPlantCode, String strLTypeCode, String strMTypeCode, String strSTypeCode, String strStdNumber,
            String strProcessCode, String strDocTitle, String strVersionChcek, String strApplyDateFrom, String strApplyDateTo, String strAdmitStatus, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strSTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.VarChar, strDocTitle, 2000);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionCheck", ParameterDirection.Input, SqlDbType.Char, strVersionChcek, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strApplyDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strApplyDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strApplyDateTo", ParameterDirection.Input, SqlDbType.VarChar, strApplyDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatus", ParameterDirection.Input, SqlDbType.VarChar, strAdmitStatus, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHSearch", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }



        /// <summary>
        /// 표준문서 검색 텍스트박스용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="intVersionNum"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHTextBox(String strPlantCode, String strStdNumber, String strVersionNum, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocH", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 등록 조회용 메소드
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strVersionNum"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHReg(String strPlantCode, String strStdNumber, String strLTypeCode, String strMTypeCode, String strSTypeCode, String strDocTitle, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strSTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, strDocTitle, 2000);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHReg", dtParam);
                return dtISODocH;
            }
            catch (Exception ex)
            {

                return dtISODocH;
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }
        /// <summary>
        /// 표준문서 조회 상세검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="intVersionNum"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHForReWrite(String strPlantCode, String strStdNumber, String strVersionNum, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHForReWrite", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }
        /// <summary>
        /// 개정 폐기용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="intVersionNum"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHForRevision(String strPlantCode, String strStdNumber, String strVersionNum, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHForRevison", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }


        /// <summary>
        /// 개정번호 콤보박스용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHForVersionCombo(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHVersionNumComb", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }



        /// <summary>
        /// 표준문서 배포요청 그리드 검색용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="intVersionNum"></param>
        /// <param name="strLTypeCode"></param>
        /// <param name="strMTypeCode"></param>
        /// <param name="strSTypeCode"></param>
        /// <param name="strProcessCode"></param>
        /// <param name="strDocTitle"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocHForDistReq(String strPlantCode, String strStdNumber, String strVersionNum, String strLTypeCode, String strMTypeCode, String strSTypeCode
                                                    , String strProcessCode, String strDocTitle, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, strVersionNum, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strLTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strMTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strSTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, strDocTitle, 2000);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //call pocedure
                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHForDistReq", dtParam);
                //리턴
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }

        /// <summary>
        /// 승인시 자동폐기후, 메일 발송할 고객 테이블
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocH_SendLine(string strPlantCode, string strStdNumber, string strRevDisuseDate)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocH = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseDate", ParameterDirection.Input, SqlDbType.VarChar, strRevDisuseDate, 10);

                dtISODocH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocH_SendLine", dtParam);
                return dtISODocH;
            }
            catch (Exception ex)
            {
                return dtISODocH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocH.Dispose();
            }
        }





        /// <summary>
        /// 표준 문서 헤더 저장
        /// </summary>
        /// <param name="dtISODocH"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="SaveVendor"></param>
        /// <param name="DelVendor"></param>
        /// <param name="SaveAgreeUser"></param>
        /// <param name="DelAgreeUser"></param>
        /// <param name="SaveDept"></param>
        /// <param name="DelDept"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocH(DataTable dtISODocH, string strUserIP, string strUserID, DataTable SaveVendor, DataTable DelVendor, DataTable SaveAgreeUser, DataTable DelAgreeUser
            , DataTable SaveDept, DataTable DelDept, DataTable SaveFile, DataTable DelFile)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strDoc = "";
            try
            {
                //open db


                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dtISODocH.Rows.Count; i++)
                {

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["STypeCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitID"].ToString(), 20);
                    //sql.mfAddParamDataRow(dtParam, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatus", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitStatus"].ToString(), 2);
                    //sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseType", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseType"].ToString(), 1);
                    //sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseID"].ToString(), 20);
                    //sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseDate"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseReason", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["RevDisuseReason"].ToString(), 500);
                    //sql.mfAddParamDataRow(dtParam, "@i_strChangItem", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ChangeItem"].ToString(), 500);
                    //sql.mfAddParamDataRow(dtParam, "@i_strFromDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["FromDesc"].ToString(), 1000);
                    //sql.mfAddParamDataRow(dtParam, "@i_strToDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ToDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocTitle"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocKeyword", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocKeyword"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRWComment", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["GRWComment"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strApplyDateFrom", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ApplyDateFrom"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strApplyDateTo", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ApplyDateTo"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["CompleteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReturnReason", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ReturnReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strChipSize", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ChipSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPadSize", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["PadSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMakeUserName", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["MakeUserName"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strMakeDeptName", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["MakeDeptName"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocH", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        strDoc = strErrRtn;
                        String strStdNumber = ErrRtn.mfGetReturnValue(0);
                        //성공시 처리
                        //삭제
                        if (DelVendor.Rows.Count > 0)
                        {
                            ISODOC.ISODocVendor delVendor = new ISODocVendor();
                            strErrRtn = delVendor.mfDeleteISODocVendor(DelVendor, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        //저장
                        if (SaveVendor.Rows.Count > 0)
                        {
                            ISODOC.ISODocVendor saveVendor = new ISODocVendor();
                            strErrRtn = saveVendor.mfSaveISODocVendor(SaveVendor, strUserID, strUserIP, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        //trans.Commit();

                        if (DelAgreeUser.Rows.Count > 0)
                        {
                            ISODOC.ISODocAgreeUser delAgreeUser = new ISODocAgreeUser();
                            strErrRtn = delAgreeUser.mfDeleteISODocAgreeUser(DelAgreeUser, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveAgreeUser.Rows.Count > 0)
                        {
                            ISODOC.ISODocAgreeUser saveAgreeUser = new ISODocAgreeUser();
                            strErrRtn = saveAgreeUser.mfSaveISODocAgreeUser(SaveAgreeUser, strUserID, strUserIP, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (DelDept.Rows.Count > 0)
                        {
                            ISODOC.ISODocDept delDept = new ISODocDept();
                            strErrRtn = delDept.mfDeleteISODocDept(DelDept, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveDept.Rows.Count > 0)
                        {
                            ISODOC.ISODocDept saveDept = new ISODocDept();
                            strErrRtn = saveDept.mfSaveISODocDept(SaveDept, strUserID, strUserIP, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }


                        if (DelFile.Rows.Count > 0)
                        {
                            ISODOC.ISODocFile delFile = new ISODocFile();
                            strErrRtn = delFile.mfDeleteISODocFile(DelFile, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (SaveFile.Rows.Count > 0)
                        {
                            ISODOC.ISODocFile saveFile = new ISODocFile();
                            strErrRtn = saveFile.mfSaveISODocFile(SaveFile, strUserID, strUserIP, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        strErrRtn = strDoc;
                    }
                }
                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary> 
        /// 표준문서 개정/폐기 저장용
        /// </summary>
        /// <param name="dtISODocH"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="SaveVendor"></param>
        /// <param name="DelVendor"></param>
        /// <param name="SaveAgreeUser"></param>
        /// <param name="DelAgreeUser"></param>
        /// <param name="SaveDept"></param>
        /// <param name="DelDept"></param>
        /// <param name="SaveFile"></param>
        /// <param name="DelFile"></param>
        /// <returns></returns>

        [AutoComplete(false)]
        public string mfSaveISODocHForDisuse(DataTable dtISODocH, string strUserIP, string strUserID, DataTable SaveVendor, DataTable DelVendor, DataTable SaveAgreeUser, DataTable DelAgreeUser
            , DataTable SaveDept, DataTable DelDept, DataTable SaveFile, DataTable DelFile)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strDoc = string.Empty;
            try
            {
                //open db


                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dtISODocH.Rows.Count; i++)
                {



                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["STypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatus", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitStatus"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseType", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseReason", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["RevDisuseReason"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDisuseDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["DisuseDate"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDisuseReason", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DisuseReason"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strChangeItem", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ChangeItem"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strFromDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["FromDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strToDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ToDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocTitle"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocKeyword", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocKeyword"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strGRWComment", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["GRWComment"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strApplyDateFrom", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ApplyDateFrom"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strApplyDateTo", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ApplyDateTo"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["CompleteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReturnReason", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ReturnReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strChipSize", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ChipSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPadSize", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["PadSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMakeUserName", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["MakeUserName"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strMakeDeptName", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["MakeDeptName"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                    //SqlParameter parameter = new SqlParameter("@o_strStdNumber", SqlDbType.VarChar);
                    //parameter.Direction = ParameterDirection.Output;

                    //string StdNumber = (string)parameter.Value;

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocHDisuse", dtParam);
                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_ISODocHDisuse", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        strDoc = strErrRtn;
                        string strStdNumber = ErrRtn.mfGetReturnValue(0);
                        //성공시 처리
                        //삭제
                        if (DelVendor.Rows.Count > 0)
                        {
                            ISODOC.ISODocVendor delVendor = new ISODocVendor();
                            strErrRtn = delVendor.mfDeleteISODocVendor(DelVendor, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        //저장
                        if (SaveVendor.Rows.Count > 0)
                        {
                            ISODOC.ISODocVendor saveVendor = new ISODocVendor();
                            strErrRtn = saveVendor.mfSaveISODocVendor(SaveVendor, strUserID, strUserIP, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        //trans.Commit();

                        if (DelAgreeUser.Rows.Count > 0)
                        {
                            ISODOC.ISODocAgreeUser delAgreeUser = new ISODocAgreeUser();
                            strErrRtn = delAgreeUser.mfDeleteISODocAgreeUser(DelAgreeUser, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveAgreeUser.Rows.Count > 0)
                        {
                            ISODOC.ISODocAgreeUser saveAgreeUser = new ISODocAgreeUser();
                            strErrRtn = saveAgreeUser.mfSaveISODocAgreeUser(SaveAgreeUser, strUserID, strUserIP, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (DelDept.Rows.Count > 0)
                        {
                            ISODOC.ISODocDept delDept = new ISODocDept();
                            strErrRtn = delDept.mfDeleteISODocDept(DelDept, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        if (SaveDept.Rows.Count > 0)
                        {
                            ISODOC.ISODocDept saveDept = new ISODocDept();
                            strErrRtn = saveDept.mfSaveISODocDept(SaveDept, strUserID, strUserIP, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }


                        if (DelFile.Rows.Count > 0)
                        {
                            ISODOC.ISODocFile delFile = new ISODocFile();
                            strErrRtn = delFile.mfDeleteISODocFile(DelFile, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (SaveFile.Rows.Count > 0)
                        {
                            ISODOC.ISODocFile saveFile = new ISODocFile();
                            strErrRtn = saveFile.mfSaveISODocFile(SaveFile, strUserID, strUserIP, sql, trans, strStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        strErrRtn = strDoc;
                    }
                }
                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 표준문서 저장(승인)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>

        [AutoComplete(false)]
        public string mfSaveISODocHApprove(DataTable dtISODocH, string strUserIP, string strUserID, DataTable dtSaveFile)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            string strAdmitVersionNum = "";
            try
            {
                //open db
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dtISODocH.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitVersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["LTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["MTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["STypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatus", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["AdmitStatus"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseType", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseType"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseReason", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["RevDisuseReason"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDisuseDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["DisuseDate"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDisuseReason", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DisuseReason"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strChangeItem", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ChangeItem"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strFromDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["FromDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strToDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ToDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocTitle"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocKeyword", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocKeyword"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["DocDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strApplyDateFrom", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ApplyDateFrom"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strApplyDateTo", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["ApplyDateTo"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["CompleteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReturnReason", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ReturnReason"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strChipSize", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["ChipSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPadSize", ParameterDirection.Input, SqlDbType.NVarChar, dtISODocH.Rows[i]["PadSize"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMakeUserName", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["MakeUserName"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strMakeDeptName", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["MakeDeptName"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strAdmitVersionNum", ParameterDirection.Output, SqlDbType.VarChar, 50);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocHApprove", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        strAdmitVersionNum = strErrRtn;
                        String strAdmitVer = ErrRtn.mfGetReturnValue(0);
                        //성공시 ISODocFile저장
                        if (dtSaveFile.Rows.Count > 0)
                        {
                            ISODOC.ISODocFile saveFile = new ISODocFile();
                            strErrRtn = saveFile.mfSaveISODocFile_Admit(dtSaveFile, strUserID, strUserIP, sql, trans, strAdmitVer);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        strErrRtn = strAdmitVersionNum;
                    }
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }





        /// <summary>
        /// 문서 개정에 의한 자동폐기
        /// </summary>
        /// <param name="dtISODocH"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISODocHAutoDisus(DataTable dtISODocH, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dtISODocH.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strRevDisuseID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["RevDisuseID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDisuseDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["DisuseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocHAutoDisuse", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();

                }
                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 헤더 삭제
        /// </summary>
        /// <param name="dtISODocH"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocH(DataTable dtISODocH)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtISODocH.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    //////그리드 값부터 삭제
                    //첨부파일 삭제
                    ISODocFile DocFile = new ISODocFile();
                    strErrRtn = DocFile.mfDeleteISODocFileAll(dtISODocH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    //승인자 삭제
                    ISODocAgreeUser agreeUser = new ISODocAgreeUser();
                    strErrRtn = agreeUser.mfDeleteISODocAgreeUserAll(dtISODocH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    //적용범위 삭제
                    ISODocDept dept = new ISODocDept();
                    strErrRtn = dept.mfDeleteISODocDeptAll(dtISODocH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    //Vendor 삭제
                    ISODocVendor vendor = new ISODocVendor();
                    strErrRtn = vendor.mfDeleteISODocVendorAll(dtISODocH, sql, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dtISODocH.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocH", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();


                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        /// <summary>
        /// 표준문서 제/개정/폐지 그룹웨어 전송
        /// </summary>
        /// <param name="dtISODocH">표준문서 헤더 DataTable - 표준문서 저장시 DataTable과 동일</param>
        /// <param name="dtSendLine">frmCOM0012.dtSendLine</param>
        /// <param name="dtCcLine">frmCOM0012.dtCcLine</param>
        /// <param name="dtFormInfo">frmCOM0012.dtFormInfo</param>
        /// <param name="strUserIP">IP</param>
        /// <param name="strUserID">ID</param>
        /// <returns>
        ///             CD_CODE         : 결과 코드 "00" 성공 나머지 실패
        ///             CD_STATUS       : 결과 상태 TRUE 성공 FALSE 실패
        ///             CD_LEGACYKEY    : Transaction 구분 키 PlantCode + "||" + StdNumber + "||" + VersionNum
        ///             NO_EMPLOYEE     : Brains 코드
        ///             DS_POSITION     : Brains 사용 Method
        ///             MSG             : 메세지 "기안성공" 나머지 실패
        /// </returns>
        [AutoComplete(false)]
        public DataTable mfISODOCGRWApproval(DataTable dtISODocH, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID, string strGWComment)
        {
            // LegacyKey, DocNo 생성시 "||" ( double pipe ) 로 구분

            DataTable dtRtn = new DataTable();

            try
            {
                QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();

                // LegacyKey 
                string strPlantCode = dtISODocH.Rows[0]["PlantCode"].ToString();
                string strStdNumber = dtISODocH.Rows[0]["StdNumber"].ToString();
                string strVersionNum = dtISODocH.Rows[0]["VersionNum"].ToString();
                string strLegacyKey = strPlantCode + "||" + strStdNumber + "||" + strVersionNum;

                // 첨부파일 조회
                ISODocFile file = new ISODocFile();
                DataTable dtFile = file.mfReadISODocFile(strPlantCode, strStdNumber, strVersionNum, "KOR");

                // Brains 전송용 DataTable
                DataTable dtFileinfo = grw.mfSetFileInfoDataTable();

                // 표준문서 기본 파일경로 
                string strFilePath = "ISODocFile\\";

                // Brains 전송용 DataTable 만들기
                foreach (DataRow dr in dtFile.Rows)
                {
                    DataRow _dr = dtFileinfo.NewRow();
                    _dr["PlantCode"] = dr["PlantCode"].ToString();
                    _dr["NM_LOCATION"] = strFilePath + strStdNumber + "-" + strVersionNum;  // 표준문서 기본 경로 + 문서별 경로
                    _dr["NM_FILE"] = dr["FileName"].ToString();                             // 파일명
                    dtFileinfo.Rows.Add(_dr);
                }

                if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals(string.Empty)) // 생성
                {
                    string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
                    string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
                    string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
                    string strComment = strGWComment;

                    dtRtn = grw.CreateISODOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strDocUser, strComment, dtFormInfo, dtFileinfo, strUserIP, strUserID);
                }
                else if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals("1")) // 개정
                {
                    string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
                    string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
                    string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
                    string strComment = strGWComment;
                    string strDocChange = dtISODocH.Rows[0]["ChangeItem"].ToString();
                    string strFrom = dtISODocH.Rows[0]["FromDesc"].ToString();
                    string strTo = dtISODocH.Rows[0]["ToDesc"].ToString();

                    dtRtn = grw.ModifyISIDOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strDocUser, strComment, strDocChange, strFrom, strTo, dtFormInfo, dtFileinfo, strUserIP, strUserID);
                }
                else if (dtISODocH.Rows[0]["RevDisuseType"].ToString().Equals("2")) // 폐기
                {
                    string strDocNo = dtISODocH.Rows[0]["StdNumber"].ToString() + "||" + dtISODocH.Rows[0]["VersionNum"].ToString();
                    string strDocSubject = dtISODocH.Rows[0]["DocTitle"].ToString();
                    string strDocUser = dtISODocH.Rows[0]["MakeUserName"].ToString();
                    string strComment = strGWComment;
                    string strReason = dtISODocH.Rows[0]["DisuseReason"].ToString();

                    dtRtn = grw.CancelISODOC(strLegacyKey, dtSendLine, dtCcLine, strDocNo, strDocSubject, strReason, strDocUser, strComment, dtFormInfo, dtFileinfo, strUserIP, strUserID);
                }
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            return dtRtn;
        }

        /// <summary>
        /// 표준문서 개정시 동일표준 중복계정 방지
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <returns></returns>
        public DataTable mfReadISODocHAdmitstatus(String strPlantCode, String strStdNumber)
        {
            SQLS sql = new SQLS();
            DataTable dtAds = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                dtAds = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocHCheckDuplication", dtParam);
                return dtAds;
            }
            catch (Exception ex)
            {
                return dtAds;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtAds.Dispose();
            }
        }
    }

    /// <summary>
    /// 표준문서 배포요청 헤더
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocDistReqH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class ISODocDistReqH : ServicedComponent
    {

        /// <summary>
        /// 표준문서 배포요청 헤더 데이터테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtISODocDistReqH = new DataTable();
            try
            {
                dtISODocDistReqH.Columns.Add("PlantCode", typeof(String));
                dtISODocDistReqH.Columns.Add("DistReqNo", typeof(String));
                dtISODocDistReqH.Columns.Add("DistReqUserID", typeof(String));
                dtISODocDistReqH.Columns.Add("DIstReqDate", typeof(String));

                return dtISODocDistReqH;
            }
            catch (Exception ex)
            {
                return dtISODocDistReqH;
                throw (ex);
            }
            finally
            {
                dtISODocDistReqH.Dispose();
            }
        }


        /// <summary>
        /// 표준문서 헤더 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDistReqNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocDistReqH(String strPlantCode, String strDistReqNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocDistReqH = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                //save parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, strDistReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //Call Procedure
                dtISODocDistReqH = sql.mfExecReadStoredProc(sql.SqlCon, "", dtParam);
                //return
                return dtISODocDistReqH;

            }
            catch (Exception ex)
            {
                return dtISODocDistReqH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocDistReqH.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 배포요청 헤더 저장
        /// </summary>
        /// <param name="dtISODocDistReqH"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="SaveD"></param>
        /// <param name="DelD"></param>
        /// <returns></returns>
        /// 
        [AutoComplete(false)]
        public string mfSaveISODocDistReqH(DataTable dtISODocDistReqH, string strUserIP, string strUserID, DataTable SaveD)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //open DB

                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dtISODocDistReqH.Rows.Count; i++)
                {
                    //Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();
                    //trans = m_SqlConnDebug.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtISODocDistReqH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistReqUserID", ParameterDirection.Input, SqlDbType.VarChar, dtISODocDistReqH.Rows[i]["DistReqUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtISODocDistReqH.Rows[i]["DistReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strDistReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocDistReqH", dtParam);
                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_ISODocDistReqH", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        String strDistReqNo = ErrRtn.mfGetReturnValue(0);
                        //저장
                        if (SaveD.Rows.Count > 0)
                        {
                            ISODOC.ISODocDistReqD saveD = new ISODocDistReqD();
                            strErrRtn = saveD.mfSaveISODocDistReqD(SaveD, strUserIP, strUserID, sql, trans, strDistReqNo);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        trans.Commit();
                    }

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocDistReqH.Dispose();
            }
        }

    }


    /// <summary>
    /// 표준문서 배포요청 상세 테이블
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocDistReqD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class ISODocDistReqD : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        /// <summary>
        /// 표준문서 배포요청 상세정보 Debuging
        /// </summary>
        /// <param name="strDBConn"></param>
        public ISODocDistReqD(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }
        public ISODocDistReqD()
        {

        }

        /// <summary>
        /// 표준문서 배포요청 상세 테이블
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtISODocDistReqD = new DataTable();
            try
            {
                dtISODocDistReqD.Columns.Add("PlantCode", typeof(String));
                dtISODocDistReqD.Columns.Add("DistReqNo", typeof(String));
                dtISODocDistReqD.Columns.Add("DistReqSeq", typeof(int));
                dtISODocDistReqD.Columns.Add("StdNumber", typeof(String));
                dtISODocDistReqD.Columns.Add("VersionNum", typeof(String));
                dtISODocDistReqD.Columns.Add("DistDeptCode", typeof(String));
                dtISODocDistReqD.Columns.Add("DistNumber", typeof(int));
                dtISODocDistReqD.Columns.Add("DistVendorCode", typeof(String));
                dtISODocDistReqD.Columns.Add("ReqDesc", typeof(String));
                dtISODocDistReqD.Columns.Add("AdmitUserID", typeof(String));
                dtISODocDistReqD.Columns.Add("AdmitDate", typeof(String));
                dtISODocDistReqD.Columns.Add("AdmitStatus", typeof(String));

                return dtISODocDistReqD;
            }
            catch (Exception ex)
            {
                return dtISODocDistReqD;
                throw (ex);
            }
            finally
            {
                dtISODocDistReqD.Dispose();
            }
        }


        /// <summary>
        /// 표준문서 배포요청 상세정보  검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDistReqNo"></param>
        /// <param name="intDistReqSeq"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocDistReqD(String strPlantCode, String strStdNumber, String strProcessCode, String strDocTitle, String strDistVendorCode
                                                , String strDistReqDateFrom, String strDistReqDateTo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocDistReqD = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, strDocTitle, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strDistVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strDistVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strDistReqDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqDateTo", ParameterDirection.Input, SqlDbType.VarChar, strDistReqDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //Call Procedure
                dtISODocDistReqD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocDistReqD", dtParam);
                //return
                return dtISODocDistReqD;

            }
            catch (Exception ex)
            {
                return dtISODocDistReqD;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocDistReqD.Dispose();
            }
        }




        /// <summary>
        /// 배포/회수관리 조회용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strProcessCode"></param>
        /// <param name="strDocTitle"></param>
        /// <param name="strDistVendorCode"></param>
        /// <param name="strDistReqDateFrom"></param>
        /// <param name="strDistReqDateTo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocDistReqDMgn(String strPlantCode, String strStdNumber, String strDocTitle, String strProcessCode, String strDistVendorCode
                                                , String strDistReqDateFrom, String strDistReqDateTo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocDistReqD = new DataTable();
            try
            {
                //db open
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, strDocTitle, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strDistVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strDistVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strDistReqDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqDateTo", ParameterDirection.Input, SqlDbType.VarChar, strDistReqDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                //Call Procedure
                dtISODocDistReqD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocDistReqDMgn", dtParam);
                //return
                return dtISODocDistReqD;

            }
            catch (Exception ex)
            {
                return dtISODocDistReqD;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocDistReqD.Dispose();
            }
        }


        /// <summary>
        /// 배포.회수 관리 조회용
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strStdNumber"></param>
        /// <param name="strDocTitle"></param>
        /// <param name="strProcessCode"></param>
        /// <param name="strDistVendorCode"></param>
        /// <param name="strDistReqDateFrom"></param>
        /// <param name="strDistReqDateTo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocDistReqDCol(String strPlantCode, String strStdNumber, String strDocTitle, String strProcessCode, String strDistVendorCode
                                                    , String strDistReqDateFrom, String strDistReqDateTo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtISODocDistReqD = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDocTitle", ParameterDirection.Input, SqlDbType.NVarChar, strDocTitle, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strDistVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strDistVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strDistReqDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqDateTo", ParameterDirection.Input, SqlDbType.VarChar, strDistReqDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtISODocDistReqD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocDistReqCol", dtParam);
                return dtISODocDistReqD;

            }
            catch (Exception ex)
            {
                return dtISODocDistReqD;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocDistReqD.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 배포요청 승인 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strPlantCode"></param>
        /// <param name="strDistReqNo"></param>
        /// <param name="intDistReqSeq"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocDistReqDForApprove(DataTable dt, String strUserIP, String strUserID, DataTable dtSaveDistCollect)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intDistReqSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DistReqSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intDistNumber", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DistNumber"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strDistVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistVendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ReqDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AdmitUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatus", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AdmitStatus"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocDistReqD", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        //성공시 처리
                        if (dtSaveDistCollect.Rows.Count > 0)
                        {
                            ISODOC.ISODocDistCollect DCollect = new ISODocDistCollect();
                            strErrRtn = DCollect.mfSaveISODocDistCollect(dtSaveDistCollect, strUserIP, strUserID, sql, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        trans.Commit();
                    }

                }
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 배포요청 상세 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDistReqNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocDistReqD(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, String strDistReqNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, strDistReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intDistReqSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DistReqSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["StdNumber"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strVersionNum", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["VersionNum"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intDistNumber", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DistNumber"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strDistVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistVendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ReqDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AdmitUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AdmitDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitStatus", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["AdmitStatus"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocDistReqD", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 표준문서 배포요청 상세 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strDistReqNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISODocDistReqD(DataTable dt, SQLS sql, SqlTransaction trans, String strDistReqNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intDistReqSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DistReqSeq"].ToString());

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISODocDistReqD", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ISODocDistCollect")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class ISODocDistCollect : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        /// <summary>
        /// 표준문서 배포/회수구분 상세정보 Debuging
        /// </summary>
        /// <param name="strDBConn"></param>
        public ISODocDistCollect(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }
        public ISODocDistCollect()
        {

        }

        /// <summary>
        /// 표준문서 배포 / 회수구분 데이터 테이블
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtISODocDistCollect = new DataTable();
            try
            {
                dtISODocDistCollect.Columns.Add("PlantCode", typeof(String));
                dtISODocDistCollect.Columns.Add("DistReqNo", typeof(String));
                dtISODocDistCollect.Columns.Add("DistReqSeq", typeof(int));
                dtISODocDistCollect.Columns.Add("Seq", typeof(int));
                dtISODocDistCollect.Columns.Add("DistCollectType", typeof(String));
                dtISODocDistCollect.Columns.Add("WriteDate", typeof(String));
                dtISODocDistCollect.Columns.Add("WriteUserID", typeof(String));
                dtISODocDistCollect.Columns.Add("ActionUserID", typeof(String));
                dtISODocDistCollect.Columns.Add("Number", typeof(int));
                dtISODocDistCollect.Columns.Add("EtcDesc", typeof(String));

                return dtISODocDistCollect;
            }
            catch (Exception ex)
            {
                return dtISODocDistCollect;
                throw (ex);
            }
            finally
            {
                dtISODocDistCollect.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 배포.회수 상세 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDistReqNo"></param>
        /// <param name="intDistReqSeq"></param>
        /// <param name="intSeq"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocDistCollect(String strPlantCode, String strDistReqNo, int intDistReqSeq, String strLang)
        {

            SQLS sql = new SQLS();
            DataTable dtISODocDistCollect = new DataTable();
            try
            {

                //db
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //Save Paramete
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, strDistReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intDistReqSeq", ParameterDirection.Input, SqlDbType.Int, intDistReqSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //call procedure
                dtISODocDistCollect = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocDistCollect", dtParam);
                //리턴
                return dtISODocDistCollect;

            }
            catch (Exception ex)
            {
                return dtISODocDistCollect;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocDistCollect.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 배포.회수 상세 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDistReqNo"></param>
        /// <param name="intDistReqSeq"></param>
        /// <param name="intSeq"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISODocDistCollect_S(String strPlantCode, String strDistReqNo, int intDistReqSeq, String strLang)
        {

            SQLS sql = new SQLS();
            DataTable dtISODocDistCollect = new DataTable();
            try
            {

                //db
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //Save Paramete
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, strDistReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intDistReqSeq", ParameterDirection.Input, SqlDbType.Int, intDistReqSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //call procedure
                dtISODocDistCollect = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISODocDistCollect_PSTS", dtParam);
                //리턴
                return dtISODocDistCollect;

            }
            catch (Exception ex)
            {
                return dtISODocDistCollect;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtISODocDistCollect.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 배포/회수 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocDistCollect(DataTable dt, String strUserIP, String strUserID, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장 
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intDistReqSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DistReqSeq"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strDistCollectType", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistCollectType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intNumber", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Number"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocDistCollect", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 표준문서 배포/회수관리 저장용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocDistCollectOnly(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intDistReqSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DistReqSeq"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strDistCollectType", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistCollectType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intNumber", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Number"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocDistCollect", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                    }
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 표준문서 배포/회수관리 저장용
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveISODocDistCollectOnly_S(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strDistReqNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intDistReqSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DistReqSeq"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strDistCollectType", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["DistCollectType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionUserID", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ActionUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intNumber", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Number"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISODocDistCollect_PSTS", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                    }
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

}