﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : ISO관리                                               */
/* 프로그램ID   : clsISOIMP.cs                                          */
/* 프로그램명   : 수입검사관리                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-08-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]
namespace QRPISO.BL.ISOIMP
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialInspectSpecH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialInspectSpecH : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        /// <summary>
        /// 디버그 모드용 생성자
        /// </summary>
        /// <param name="strDbConn"></param>
        public MaterialInspectSpecH(String strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 생성자
        /// </summary>
        public MaterialInspectSpecH()
        {
        }

        /// <summary>
        /// 저장/삭제시 사용할 DataTable Column 설정 함수
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("StdNumber", typeof(String));
                dt.Columns.Add("StdSeq", typeof(String));
                dt.Columns.Add("VersionNum", typeof(Int32));
                dt.Columns.Add("VendorCode", typeof(String));
                dt.Columns.Add("MaterialCode", typeof(String));
                dt.Columns.Add("SpecNo", typeof(String));
                dt.Columns.Add("WriteID", typeof(String));
                dt.Columns.Add("WriteDate", typeof(String));
                dt.Columns.Add("EtcDesc", typeof(String));
                dt.Columns.Add("EtcDesc1", typeof(String));
                dt.Columns.Add("EtcDesc2", typeof(String));
                dt.Columns.Add("EtcDesc3", typeof(String));
                dt.Columns.Add("EtcDesc4", typeof(String));
                dt.Columns.Add("InitPeriod", typeof(string));
                dt.Columns.Add("InitPeriodUnitCode", typeof(string));
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// 조회용 Method(그리드)
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>        
        /// <param name="strConsumableType"> 자재종류 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMaterialInspectSpecH(String strPlantCode, String strVendorCode, String strConsumableType, String strMaterialCode, String strLang)
        {
            // Instance 생성
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            try
            {
                // 디비연결
                sql.mfConnect();

                // Parameter DataTable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter변수 설정
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strConsumableType", ParameterDirection.Input, SqlDbType.VarChar, strConsumableType, 40); 
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 프로시저 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpecH", dtParam);
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_ISOMaterialInspectSpecH", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 기존 등록된 정보가 있는지 조회하는 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strVendorCode"> 거래처코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMaterialInspectSpecHCheck(String strPlantCode, String strVendorCode, String strMaterialCode, String strSpecNo)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, strSpecNo, 50);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpecHCheck", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 규격서 상세조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMaterialInspectSpecHDetail(String strPlantCode, String strStdNumber, String strStdSeq, String strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();

                // 파리미터 변수저장용 데이터 테이블 설정 및 변수값 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 프로시저 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpecHDetail", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 수입검사규격서 저장 Method
        /// </summary>
        /// <param name="dtMInspectSpecH"> 헤더정보 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="dtSaveDetail"> 상세정보 DataTable </param>
        /// <param name="dtGrade"> 등급정보 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOMaterialInspectSpecH(DataTable dtMInspectSpecH, String strUserID, String strUserIP, DataTable dtSaveDetail, DataTable dtGrade)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB 연결
                sql.mfConnect();
                // 트랜잭션 변수 선언
                SqlTransaction trans;

                String strRtnStdNumber = "";
                String strPlantCode = "";

                for (int i = 0; i < dtMInspectSpecH.Rows.Count; i++)
                {
                    strPlantCode = dtMInspectSpecH.Rows[i]["PlantCode"].ToString();
                    
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // Parameter 용 DataTable 변수
                    DataTable dtParam = sql.mfSetParamDataTable();
                    // 매개변수로 넘어온 DataTable의값을 Parameter DataTable에 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["StdSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtMInspectSpecH.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecNo", ParameterDirection.Input, SqlDbType.NVarChar, dtMInspectSpecH.Rows[i]["SpecNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["WriteID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtMInspectSpecH.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc1", ParameterDirection.Input, SqlDbType.Text, dtMInspectSpecH.Rows[i]["EtcDesc1"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc2", ParameterDirection.Input, SqlDbType.Text, dtMInspectSpecH.Rows[i]["EtcDesc2"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc3", ParameterDirection.Input, SqlDbType.Text, dtMInspectSpecH.Rows[i]["EtcDesc3"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc4", ParameterDirection.Input, SqlDbType.Text, dtMInspectSpecH.Rows[i]["EtcDesc4"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInitPeriod", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["InitPeriod"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInitPeriodUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["InitPeriodUnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strStdNumber", ParameterDirection.Output, SqlDbType.VarChar,20);
                    //sql.mfAddParamDataRow(dtParam, "@o_strStdSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    
                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_ISOMaterialInspectSpecH", dtParam);

                    // 결과 확인
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // 성공시 상세정보 처리
                        // 삭제먼저
                        // 생성된 표준번호를 받는다
                        strRtnStdNumber = ErrRtn.mfGetReturnValue(0);

                        QRPISO.BL.ISOIMP.MaterialInspectSpecD clsDetail = new MaterialInspectSpecD();
                        // 삭제
                        strErrRtn = clsDetail.mfDeleteISOMaterialInspectSpecD(strPlantCode, strRtnStdNumber, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        if (dtSaveDetail.Rows.Count > 0)
                        {
                            // 상세정보 저장Method 실행
                            strErrRtn = clsDetail.mfSaveISOMaterialInspectSpecD(dtSaveDetail, strUserID, strUserIP, sql.SqlCon, trans, strPlantCode, strRtnStdNumber);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        // 등급정보 처리
                        // 삭제먼저수행
                        QRPISO.BL.ISOIMP.MaterialInspectSpecGrade clsGrade = new MaterialInspectSpecGrade();
                        
                        // 등급정보 삭제 Method
                        strErrRtn = clsGrade.mfDeleteMaterialInspectSpecGrade(strPlantCode, strRtnStdNumber, sql.SqlCon, trans);
                        // 결과확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                        if (dtGrade.Rows.Count > 0)
                        {
                            // 등급정보 저장 Method
                            strErrRtn = clsGrade.mfSaveMaterialInspectSpecGrade(dtGrade, strUserID, strUserIP, sql.SqlCon, trans, strPlantCode, strRtnStdNumber);
                            // 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
        
        /// <summary>
        /// 수입검사 규격서 삭제 Method
        /// </summary>
        /// <param name="dtMInspectSpecH"> 헤더정보 DataTable </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOMaterialInspectSpecH(DataTable dtMInspectSpecH)
        {
            String strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();
                
                // Transaction 변수
                SqlTransaction trans;

                for (int i = 0; i < dtMInspectSpecH.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();

                    String strPlantCode = dtMInspectSpecH.Rows[i]["PlantCode"].ToString();
                    String strStdNumber = dtMInspectSpecH.Rows[i]["StdNumber"].ToString() + dtMInspectSpecH.Rows[i]["StdSeq"].ToString();
                    
                    // Detail 부터 삭제
                    MaterialInspectSpecD clsDetail = new MaterialInspectSpecD();
                    strErrRtn = clsDetail.mfDeleteISOMaterialInspectSpecD(strPlantCode, strStdNumber, sql.SqlCon, trans);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    // Grade 삭제
                    MaterialInspectSpecGrade clsGrade = new MaterialInspectSpecGrade();
                    strErrRtn = clsGrade.mfDeleteMaterialInspectSpecGrade(strPlantCode, strStdNumber, sql.SqlCon, trans);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    // 헤더 삭제
                    DataTable dtParam = sql.mfSetParamDataTable();                    

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtMInspectSpecH.Rows[i]["StdSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_ISOMaterialInspectSpecH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 수입검사 규격서 SpepNo 팝업창 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strMaterialCode"> 자재코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISoMaterialInspectSpec_SpecNo(String strPlantCode, String strMaterialCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpec_SpecNo", dtParam);

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialInspectSpecD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialInspectSpecD : ServicedComponent
    {
        /// <summary>
        /// 데이터 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("StdNumber", typeof(String));
                dt.Columns.Add("StdSeq", typeof(String));
                dt.Columns.Add("VersionNum", typeof(Int32));
                dt.Columns.Add("ItemNum", typeof(Int32));
                dt.Columns.Add("Seq", typeof(Int32));
                dt.Columns.Add("InspectGroupCode", typeof(String));
                dt.Columns.Add("InspectTypeCode", typeof(String));
                dt.Columns.Add("InspectItemCode", typeof(String));
                dt.Columns.Add("InspectCondition", typeof(String));
                dt.Columns.Add("Method", typeof(String));
                dt.Columns.Add("SpecDesc", typeof(String));
                dt.Columns.Add("MeasureToolCode", typeof(String));
                dt.Columns.Add("UpperSpec", typeof(decimal));
                dt.Columns.Add("LowerSpec", typeof(decimal));                
                dt.Columns.Add("SpecRange", typeof(String));
                dt.Columns.Add("SpecUnitCode", typeof(String));
                dt.Columns.Add("Point", typeof(Int32));
                dt.Columns.Add("SampleSize", typeof(decimal));
                dt.Columns.Add("UnitCode", typeof(String));
                dt.Columns.Add("CompareFlag", typeof(String));
                dt.Columns.Add("DataType", typeof(String));
                dt.Columns.Add("EtcDesc", typeof(String));
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMaterialInspectSpecD(String strPlantCode, String strStdNumber, String strStdSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 5);

                dtDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpecD", dtParam);

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 조회용 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadISOMaterialInspectSpecD(String strPlantCode, String strStdNumber, String strStdSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDetail = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDetail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpecD_1", dtParam);

                return dtDetail;
            }
            catch (Exception ex)
            {
                return dtDetail;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDetail.Dispose();
            }
        }

        /// <summary>
        /// 상세정보 저장 Method
        /// </summary>
        /// <param name="dtDetail"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="sqlCon"> SqlConnection </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순서 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveISOMaterialInspectSpecD(DataTable dtDetail, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans
                                                    , String strPlantCode, String strFullStdNumber)
        {
            // Instance 객체 생성
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            SQLS sql = new SQLS();
            try
            {
                //sql.mfConnect();
                
                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    // Parameter Table 변수
                    DataTable dtParam = sql.mfSetParamDataTable();

                    // Parameter Table에 값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intItemNum", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ItemNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["InspectItemCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectCondition", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["InspectCondition"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMethod", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["Method"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["SpecDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["MeasureToolCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_dblUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["UpperSpec"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_dblLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["LowerSpec"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["SpecRange"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["SpecUnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intPoint", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["Point"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["SampleSize"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompareFlag", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["CompareFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["DataType"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["EtcDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOMaterialInspectSpecD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <param name="sqlCon"> SQLConnection </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteISOMaterialInspectSpecD(String strPlantCode, String strFullStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        {
            String strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();

                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_ISOMaterialInspectSpecDALL", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MaterialInspectSpecGrade")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MaterialInspectSpecGrade : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼 설정Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtGrade = new DataTable();
            try
            {
                dtGrade.Columns.Add("PlantCode", typeof(String));
                dtGrade.Columns.Add("StdNumber", typeof(String));
                dtGrade.Columns.Add("StdSeq", typeof(String));
                dtGrade.Columns.Add("VersionNum", typeof(Int32));
                dtGrade.Columns.Add("Seq", typeof(Int32));
                dtGrade.Columns.Add("InspectGroupCode", typeof(String));
                dtGrade.Columns.Add("InspectTypeCode", typeof(String));
                dtGrade.Columns.Add("InspectGradeCode", typeof(String));
                dtGrade.Columns.Add("EverySampleSize", typeof(decimal));
                dtGrade.Columns.Add("EveryUnitCode", typeof(String));
                dtGrade.Columns.Add("EveryTarget", typeof(String));
                dtGrade.Columns.Add("ShipPeriod", typeof(Int32));
                dtGrade.Columns.Add("ShipPeriodUnitCode", typeof(String));
                dtGrade.Columns.Add("ShipSampleSize", typeof(decimal));
                dtGrade.Columns.Add("ShipUnitCode", typeof(String));
                dtGrade.Columns.Add("ShipTarget", typeof(String));

                return dtGrade;
            }
            catch (Exception ex)
            {
                return dtGrade;
                throw (ex);
            }
            finally
            {
                dtGrade.Dispose();
            }
        }

        /// <summary>
        /// 등급 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strStdNumber"> 표준번호 </param>
        /// <param name="strStdSeq"> 표준번호순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMaterialInspectSpecGrade(String strPlantCode, String strStdNumber, String strStdSeq)
        {
            DataTable dtGrade = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter Datatable 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter DataTable에 매개변수로 받은 값 저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);

                // SP 실행
                dtGrade = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOMaterialInspectSpecGrade", dtParam);

                return dtGrade;
            }
            catch (Exception ex)
            {
                return dtGrade;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtGrade.Dispose();
            }
        }

        /// <summary>
        /// 등급 저장 Method
        /// </summary>
        /// <param name="dtGrade"> 저장할 정보가 들어있는 DataTable </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="sqlCon"> SqlConnection </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strFullStdNumber"> 표준번호+표준번호순번 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveMaterialInspectSpecGrade(DataTable dtGrade, String strUserID, String strUserIP, SqlConnection sqlCon, SqlTransaction trans
                                                    , String strPlantCode, String strFullStdNumber)
        {
            // Instance 객체 생성
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            SQLS sql = new SQLS();
            try
            {
                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                for (int i = 0; i < dtGrade.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtGrade.Rows[i]["VersionNum"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtGrade.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtGrade.Rows[i]["InspectGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtGrade.Rows[i]["InspectTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectGradeCode", ParameterDirection.Input, SqlDbType.VarChar, dtGrade.Rows[i]["InspectGradeCode"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblEverySampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtGrade.Rows[i]["EverySampleSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEveryUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtGrade.Rows[i]["EveryUnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEveryTarget", ParameterDirection.Input, SqlDbType.VarChar, dtGrade.Rows[i]["EveryTarget"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intShipPeriod", ParameterDirection.Input, SqlDbType.Int, dtGrade.Rows[i]["ShipPeriod"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strShipPeriodUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtGrade.Rows[i]["ShipPeriodUnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_dblShipSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtGrade.Rows[i]["ShipSampleSize"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strShipUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtGrade.Rows[i]["ShipUnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strShipTarget", ParameterDirection.Input, SqlDbType.VarChar, dtGrade.Rows[i]["ShipTarget"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_ISOMaterialInspectSpecGrade", dtParam);

                    // Error 체크
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 등급 삭제 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strFullStdNumber"> 표준번호 + 표준번호순번 </param>
        /// <param name="sqlCon"> SqlConnenction 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteMaterialInspectSpecGrade(String strPlantCode, String strFullStdNumber, SqlConnection sqlCon, SqlTransaction trans)
        {
            String strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();

                String strStdNumber = strFullStdNumber.Substring(0, 9);
                String strStdSeq = strFullStdNumber.Substring(9, 4);

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_ISOMaterialInspectSpecGradeALL", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.Dispose();
            }
        }
    }
}
