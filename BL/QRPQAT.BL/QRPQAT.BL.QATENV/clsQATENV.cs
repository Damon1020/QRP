﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질보증관리                                          */
/* 프로그램ID   : clsQATENV.cs                                          */
/* 프로그램명   : 환경유해물질관리                                      */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-09-14                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-21 : UNGreenMaterialH / D 클래스 추가 (이종호)*/
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using System.Diagnostics;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPQAT.BL.QATENV
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("UTC")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class UTC : ServicedComponent
    {
        /// <summary>
        /// UTC정보 컬럼셋
        /// </summary>
        /// <returns>UTC테이블컬럼정보가있음</returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtCoulmns = new DataTable();
            try
            {
                dtCoulmns.Columns.Add("PlantCode", typeof(string));
                dtCoulmns.Columns.Add("UTCNo", typeof(string));
                dtCoulmns.Columns.Add("EquipLocCode", typeof(string));
                dtCoulmns.Columns.Add("Temperature", typeof(string));
                dtCoulmns.Columns.Add("Humidity", typeof(string));
                dtCoulmns.Columns.Add("Particle", typeof(string));
                dtCoulmns.Columns.Add("WriteDate", typeof(string));
                dtCoulmns.Columns.Add("WriteUserID", typeof(string));
                dtCoulmns.Columns.Add("EtcDesc", typeof(string));

                return dtCoulmns;
            }
            catch (Exception ex)
            {
                return dtCoulmns;
                throw (ex);
            }
            finally
            {
                dtCoulmns.Dispose();
            }
        }

        /// <summary>
        /// UTC정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <returns>UTC정보</returns>
        [AutoComplete]
        public DataTable mfReadUTC(string strPlantCode, string strEquipLocCode, string strFromDate, string strToDate)
        {
            DataTable dtUTC = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비 연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                
                dtUTC = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATUTC", dtParam);

                return dtUTC;
            }
            catch (Exception ex)
            {
                return dtUTC;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtUTC.Dispose();
            }
        }


        /// <summary>
        /// UTC정보 등록
        /// </summary>
        /// <param name="dtUTC">등록할정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveUTC(DataTable dtUTC,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect();
                //Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtUTC.Rows.Count; i++)
                {
                    //파라미터값저장
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtUTC.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUTCNo", ParameterDirection.Input, SqlDbType.VarChar, dtUTC.Rows[i]["UTCNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, dtUTC.Rows[i]["EquipLocCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intTemperature", ParameterDirection.Input, SqlDbType.Decimal, dtUTC.Rows[i]["Temperature"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intHumidity", ParameterDirection.Input, SqlDbType.Decimal, dtUTC.Rows[i]["Humidity"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intParticle", ParameterDirection.Input, SqlDbType.Decimal, dtUTC.Rows[i]["Particle"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtUTC.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtUTC.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtUTC.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP , 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Input, SqlDbType.VarChar, 8000);
         
                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATUTC", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //실패시 롤백처리후 for문을 나간다,
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                // 성공이면 Commit
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// UTC정보 삭제
        /// </summary>
        /// <param name="dtUTC">삭제할정보</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfDeleteUTC(DataTable dtUTC)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();
                //Transaction시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                
                for (int i = 0; i < dtUTC.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터값 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtUTC.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUTCNo", ParameterDirection.Input, SqlDbType.VarChar,dtUTC.Rows[i]["UTCNo"].ToString() ,20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저실행
                   strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon,trans ,"up_Delete_QATUTC",dtParam);

                   ErrRtn =  ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 롤백처리후 for문을나간다
                   if (ErrRtn.ErrNum != 0)
                   {
                       trans.Rollback();
                       break;
                   }
                }
                // 성공이면 Commit
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                //처리결과값 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }

        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("UnGreenMaterialH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class UnGreenMaterialH : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDateInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("ItemName", typeof(string));
                dtRtn.Columns.Add("ItemNameDetail", typeof(string));
                dtRtn.Columns.Add("ReportNo", typeof(string));
                dtRtn.Columns.Add("Pb", typeof(string));
                dtRtn.Columns.Add("Cd", typeof(string));
                dtRtn.Columns.Add("Cr", typeof(string));
                dtRtn.Columns.Add("PBBs", typeof(string));
                dtRtn.Columns.Add("Hg", typeof(string));
                dtRtn.Columns.Add("PBDEs", typeof(string));
                dtRtn.Columns.Add("PFOS", typeof(string));
                dtRtn.Columns.Add("PFOA", typeof(string));
                dtRtn.Columns.Add("Sb", typeof(string));
                dtRtn.Columns.Add("BBP", typeof(string));
                dtRtn.Columns.Add("DBP", typeof(string));
                dtRtn.Columns.Add("DEHP", typeof(string));
                dtRtn.Columns.Add("HBCDD", typeof(string));
                dtRtn.Columns.Add("Br", typeof(string));
                dtRtn.Columns.Add("Cl", typeof(string));
                dtRtn.Columns.Add("RohsEndDate", typeof(string));
                dtRtn.Columns.Add("MSDS", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("NOLongerBuy", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }
        /// <summary>
        /// 환경유해물질관리 헤더 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        /// <param name="strRohsEndDateFrom">Rohs만료일From</param>
        /// <param name="strRohsEndDateTo">Rohs만료일To</param>
        /// 
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATUnGreenMaterialH(string strPlantCode, string strVendorCode, string strItemName
                                            , string strRohsEndDateFrom, string strRohsEndDateTo, string strRohsEndDateOverCheck, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, strItemName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strRohsEndDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strRohsEndDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRohsEndDateTo", ParameterDirection.Input, SqlDbType.VarChar, strRohsEndDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRohsEndDateOverCheck", ParameterDirection.Input, SqlDbType.VarChar, strRohsEndDateOverCheck, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATUnGreenMaterialH", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 환경유해물질관리 헤더 상세 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        /// <param name="strItemNameDetail">상세폼목명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATUnGreenMaterialHDetail(string strPlantCode, string strVendorCode, string strItemName, string strItemNameDetail, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, strItemName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, strItemNameDetail, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATUnGreenMaterialHDetail", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 환경유해물질 헤더 저장
        /// </summary>
        /// <param name="dtHeader">헤더정보 데이터 테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="dtDetail">첨부파일 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATUnGreenMaterialH(DataTable dtHeader, string strUserID, string strUserIP, DataTable dtDetail)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";
            try
            {
                // 디비연결
                sql.mfConnect();
                SqlTransaction trans;
                // 트랜잭션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 파라미터 데이터테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ItemName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ItemNameDetail"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strReportNo", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ReportNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPb", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Pb"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCd", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Cd"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCr", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Cr"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPBBs", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["PBBs"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strHg", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Hg"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPBDEs", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["PBDEs"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPFOS", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["PFOS"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strPFOA", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["PFOA"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strSb", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Sb"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strBBP", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["BBP"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDBP", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["DBP"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strDEHP", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["DEHP"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strHBCDD", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["HBCDD"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strBr", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Br"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCl", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Cl"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strRohsEndDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["RohsEndDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMSDS", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["MSDS"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strNOLongerBuy", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["NOLongerBuy"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["WriteUserID"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATUnGreenMaterialH", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // 저장실패시 롤백
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        UnGreenMaterialD clsDetail = new UnGreenMaterialD();

                        ////// 성공시 첨부파일 삭제/저장
                        ////// 삭제
                        ////strErrRtn = clsDetail.mfDeleteQATUnGreenMaterialD(dtHeader.Rows[i]["PlantCode"].ToString(), dtHeader.Rows[i]["VendorCode"].ToString()
                        ////                                                    , dtHeader.Rows[i]["ItemName"].ToString(), dtHeader.Rows[i]["ItemNameDetail"].ToString()
                        ////                                                    , sql.SqlCon, trans);

                        ////// 결과검사
                        ////ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        ////if (ErrRtn.ErrNum != 0)
                        ////{
                        ////    // 삭제실패시 롤백
                        ////    trans.Rollback();
                        ////    break;
                        ////}

                        // 저장
                        strErrRtn = clsDetail.mfSaveQATUnGreenMaterialD(dtDetail, strUserID, strUserIP, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            // 저장실패시 롤백
                            trans.Rollback();
                            break;
                        }
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                
                trans.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 환경유해물질 관리 헤더 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATUnGreenMaterialH(string strPlantCode, string strVendorCode, string strItemName, string strItemNameDetail)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                // 디비연결
                sql.mfConnect();
                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, strItemName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, strItemNameDetail, 100);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATUnGreenMaterialH", dtParam);
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();       // 저장실패시 롤백
                else
                    trans.Commit();

                #region 헤더 삭제 프로시져에서 상세까지 삭제로 변경에 의한 주석처리
                /*
                // 상세정보부터 먼저 삭제
                UnGreenMaterialD clsDetail = new UnGreenMaterialD();
                strErrRtn = clsDetail.mfDeleteQATUnGreenMaterialD(strPlantCode, strVendorCode, strItemName, strItemNameDetail, sql.SqlCon, trans);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    // 저장실패시 롤백
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, strItemName, 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, strItemNameDetail, 100);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATUnGreenMaterialH", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // 저장실패시 롤백
                        trans.Rollback();
                        return strErrRtn;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                 * */
                #endregion
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 환경유해물질 관리 헤더 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        /// <param name="strItemNameDetail"></param>
        /// <param name="dtUnGreenMaterialD"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATUnGreenMaterialH_B(string strPlantCode, 
                                                    string strVendorCode, 
                                                    string strItemName, 
                                                    string strItemNameDetail,
                                                    DataTable dtDetail,
                                                    string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {

                string strErrRtn = string.Empty;
                // 디비연결
                sql.mfConnect();
                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                if (dtDetail.Rows.Count > 0)
                {
                    UnGreenMaterialD clsMaterialD = new UnGreenMaterialD();
                    strErrRtn = clsMaterialD.mfSaveQATUnGreenMaterialD_TMPQMS(dtDetail, strUserID, strUserIP, sql.SqlCon, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, strItemName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, strItemNameDetail, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATUnGreenMaterialH_B", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();       // 저장실패시 롤백
                else
                    trans.Commit();

               
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        public DataTable mfSetDateInfo_CasNO() 
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("CasNO", typeof(string));
                dtRtn.Columns.Add("Substances", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadQATCasNO(string strCasNO, string strSubstances)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strCasNO", ParameterDirection.Input, SqlDbType.VarChar, strCasNO, 500);
                sql.mfAddParamDataRow(dtParam, "@i_strSubstances", ParameterDirection.Input, SqlDbType.NVarChar, strSubstances, 500);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATCasNO", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveQATCasNO(DataTable dtHeader, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";
            try
            {
                // 디비연결
                sql.mfConnect();
                SqlTransaction trans;
                // 트랜잭션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 파라미터 데이터테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCasNO", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CasNO"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strSubstances", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Substances"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["Seq"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATCasNO", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // 저장실패시 롤백
                        trans.Rollback();
                        break;
                    }

                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                trans.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CasNO")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CasNO : ServicedComponent
    {
        public DataTable mfSetDateInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("CasNO", typeof(string));
                dtRtn.Columns.Add("Substances", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfSaveQATCasNO(DataTable dtHeader, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";
            try
            {
                // 디비연결
                sql.mfConnect();
                SqlTransaction trans;
                // 트랜잭션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 파라미터 데이터테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strCasNO", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CasNO"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strSubstances", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["Substances"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["Seq"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATCasNO", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // 저장실패시 롤백
                        trans.Rollback();
                        break;
                    }

                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                trans.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadQATCasNO( string strCasNO,string strSubstances)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strCasNO", ParameterDirection.Input, SqlDbType.VarChar, strCasNO, 500);
                sql.mfAddParamDataRow(dtParam, "@i_strSubstances", ParameterDirection.Input, SqlDbType.NVarChar, strSubstances, 500);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATCasNO", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("UnGreenMaterialD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class UnGreenMaterialD : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDateInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                //dtRtn.Columns.Add("Check", typeof(bool));
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("VendorCode", typeof(string));
                dtRtn.Columns.Add("ItemName", typeof(string));
                dtRtn.Columns.Add("ItemNameDetail", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("FileTitle", typeof(string));
                dtRtn.Columns.Add("FileVarName", typeof(string));
                dtRtn.Columns.Add("FileVarValue", typeof(byte[]));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("DeleteFlag", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 환경유해물질 관리 첨부파일 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">품목명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATUnGreenMaterialD(string strPlantCode, string strVendorCode, string strItemName, string strItemNameDetail)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, strItemName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, strItemNameDetail, 100);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATUnGreenMaterialD", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        // <summary>
        /// 환경유해물질 관리 첨부파일 조회 Method(DeleteFlag상관없이)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">품목명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATUnGreenMaterialD_All(string strPlantCode, string strVendorCode, string strItemName, string strItemNameDetail)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, strItemName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, strItemNameDetail, 100);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATUnGreenMaterialD_All", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 환경유해물질 관리 첨부파일 저장 Method
        /// </summary>
        /// <param name="dtDetail">첨부파일정보 저장된 데이타 테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="sqlCon">SqlConnetion 변수</param>
        /// <param name="trans">Transaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATUnGreenMaterialD(DataTable dtDetail, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                SQLS sql = new SQLS();
                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    //파라미터 데이터 테이블 설정
                    DataTable dtParam = mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["VendorCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ItemName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ItemNameDetail"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFileTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["FileTitle"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strFileVarName", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["FileVarName"].ToString(), 200);
                    mfAddParamDataRow(dtParam, "@i_vbFileVarValue", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtDetail.Rows[i]["FileVarValue"]);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.Char, dtDetail.Rows[i]["DeleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = mfExecTransStoredProc(sqlCon, trans, "up_Update_QATUnGreenMaterialD", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 환경유해물질 관리 첨부파일 임시테이블(이력)에 저장 Method (헤더 삭제시만 사용)
        /// </summary>
        /// <param name="dtDetail">첨부파일정보 저장된 데이타 테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="sqlCon">SqlConnetion 변수</param>
        /// <param name="trans">Transaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATUnGreenMaterialD_TMPQMS(DataTable dtDetail, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                SQLS sql = new SQLS();
                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    if (dtDetail.Rows[i]["DeleteFlag"].ToString().Equals("F"))
                    {
                        //파라미터 데이터 테이블 설정
                        DataTable dtParam = mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["VendorCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ItemName"].ToString(), 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ItemNameDetail"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["Seq"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strFileTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["FileTitle"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strFileVarName", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["FileVarName"].ToString(), 200);
                        mfAddParamDataRow(dtParam, "@i_vbFileVarValue", ParameterDirection.Input, SqlDbType.VarBinary, (byte[])dtDetail.Rows[i]["FileVarValue"]);
                        sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["EtcDesc"].ToString(), 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.Char, "T", 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        // 프로시져 실행
                        strErrRtn = mfExecTransStoredProc(sqlCon, trans, "up_Update_QATUnGreenMaterialD_TMPQMS", dtParam);

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            break;
                        }
                    }
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 환경유해물질 관리 상세 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strVendorCode">거래처코드</param>
        /// <param name="strItemName">폼목명</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">Transaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATUnGreenMaterialD(string strPlantCode, string strVendorCode, string strItemName, string strItemNameDetail, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";
                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, strVendorCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strItemName", ParameterDirection.Input, SqlDbType.VarChar, strItemName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strItemNameDetail", ParameterDirection.Input, SqlDbType.NVarChar, strItemNameDetail, 100);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATUnGreenMaterialD", dtParam);
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="strValue">Paramter 인자값</param>
        private void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, byte[] Value)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = Value;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 데이터테이블 설정
        /// </summary>
        /// <returns>Parameter테이블</returns>
        private DataTable mfSetParamDataTable()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable();

                DataColumn dc = new DataColumn("ParamName", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("ParamDirect", typeof(ParameterDirection));
                dt.Columns.Add(dc);

                dc = new DataColumn("DBType", typeof(SqlDbType));
                dt.Columns.Add(dc);

                dc = new DataColumn("Value", typeof(object));
                dt.Columns.Add(dc);

                dc = new DataColumn("Length", typeof(int));
                dt.Columns.Add(dc);

                return dt;
            }
            catch (Exception ex)
            {
                //throw ex;
                return dt;
            }
        }

        /// <summary>
        /// 저장프로시저 트랙잭션(DML) 함수 실행 : SP함수명 및 인자(데이터테이블)가 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns></returns>
        private string mfExecTransStoredProc(SqlConnection Conn, SqlTransaction Trans, string strSPName, DataTable dtSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_Cmd = new SqlCommand();
                    m_Cmd.Connection = Conn;
                    m_Cmd.Transaction = Trans;
                    m_Cmd.CommandType = CommandType.StoredProcedure;
                    m_Cmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        param.SqlValue = dr["Value"];

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                            {
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                            }
                        }

                        m_Cmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_Cmd.ExecuteScalar());

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_Cmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_Cmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_Cmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    Debug.Print("DataBase 연결되지 않았습니다.");
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("StaticCheckH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class StaticCheckH : ServicedComponent
    {
        /// <summary>
        /// 정전기 예방점검 조회용 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipLocCode">측정위치코드</param>
        /// <param name="strCheckFromDate">측정일From</param>
        /// <param name="strCheckToDate">측정일To</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadStaticCheckH_Search(string strPlantCode, string strEquipLocCode, string strCheckFromDate, string strCheckToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCheckDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strCheckFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCheckDateTo", ParameterDirection.Input, SqlDbType.VarChar, strCheckToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATStaticCheckH", dtParam);

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 정전기 예방점검 헤더 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManageNo">관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadStaticCheckH_Detail(string strPlantCode, string strManageNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATStaticCheckHDetail", dtParam);

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtHeader"></param>
        /// <param name="dtDetail"></param>
        /// <param name="strUserID"></param>
        /// <param name="strUserIP"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveStaticCheckH(DataTable dtHeader, DataTable dtDetail, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ManageNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckTime", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["DocSpec"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["EquipLocCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEquipItem1", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsEquipITem1"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEquipItem2", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsEquipITem2"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEquipItem3", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsEquipITem3"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEquipItem4", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsEquipITem4"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEquipItem5", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsEquipITem5"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsWorkTableItem1", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsWorkTableItem1"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsWorkTableItem2", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsWorkTableItem2"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsWorkTableItem3", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsWorkTableItem3"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsWorkTableItem4", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsWorkTableItem4"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsResItem1", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsResItem1"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsResItem2", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsResItem2"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsResItem3", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InsResItem3"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem1", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem1"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem2", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem2"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem3", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem3"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem4", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem4"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem5", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem5"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem6", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem6"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem7", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem7"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem8", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem8"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem9", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem9"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem10", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem10"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem11", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem11"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem12", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem12"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem13", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem13"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem14", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem14"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem15", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem15"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem16", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem16"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem17", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem17"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem18", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem18"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem19", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem19"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsEtcItem20", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsEtcItem20"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInsResultFile", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InsResultFile"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strManageNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATStaticCheckH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // OUTPUT Parameter
                        string strManageNo = ErrRtn.mfGetReturnValue(0);

                        // 상세정보 삭제
                        StaticCheckD clsD = new StaticCheckD();
                        strErrRtn = clsD.mfDeleteStaticCheckD_ALL(dtHeader.Rows[i]["PlantCode"].ToString(), strManageNo, sql.SqlCon, trans);
                        
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            trans.Rollback();
                            break;
                        }

                        if (dtDetail.Rows.Count > 0)
                        {
                            // 상세정보 저장
                            strErrRtn = clsD.mfSaveStaticCheckD(dtDetail, dtHeader.Rows[i]["PlantCode"].ToString(), strManageNo, strUserID, strUserIP, sql.SqlCon, trans);

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                    }
                }

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 정전기 예방점검 정보 삭제(헤더, 상세정보)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManageNo">관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteStaticCheckH_All(string strPlantCode, string strManageNo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATStaticCheckAll", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("StaticCheckD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class StaticCheckD : ServicedComponent
    {
        /// <summary>
        /// 정전기예방점검 상세정보조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManageNo">관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadStaticCheckD(string strPlantCode, string strManageNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATStaticCheckD", dtParam);

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 정전기 예방점검 상세정보 저장
        /// </summary>
        /// <param name="dtDetail">상세정보 데이터 테이블</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManageNo">관리번호</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveStaticCheckD(DataTable dtDetail, string strPlantCode, string strManageNo, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItem", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["InspectItem"].ToString(), 300);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ActionDesc"].ToString(), 300);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionFile", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["ActionFile"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionDate", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ActionDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionUserID", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ActionUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strManageNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATStaticCheckD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 정전기 점검 상세정보 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManageNo">관리번호</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteStaticCheckD_ALL(string strPlantCode, string strManageNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strManageNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATStaticCheckD", dtParam);

                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("TempHumidityH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class TempHumidityH : ServicedComponent
    {
        /// <summary>
        /// Set DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManageNo", typeof(string));
                dtRtn.Columns.Add("CheckDate", typeof(string));
                dtRtn.Columns.Add("CheckTime", typeof(string));
                dtRtn.Columns.Add("CheckUserID", typeof(string));
                dtRtn.Columns.Add("DocumentNo", typeof(string));
                dtRtn.Columns.Add("EquipLocCode", typeof(string));
                dtRtn.Columns.Add("AbnormalDesc", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 온습도 측정정보 Header 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strEquipLocCode"></param>
        /// <param name="strCheckDateFrom"></param>
        /// <param name="strCheckDateTo"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATTempHumidityH(string strPlantCode, string strEquipLocCode, string strCheckDateFrom, string strCheckDateTo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLoc", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCheckDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strCheckDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCheckDateTo", ParameterDirection.Input, SqlDbType.VarChar, strCheckDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATTempHumidityH", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// Header 정보 저장
        /// </summary>
        /// <param name="dtHeader"></param>
        /// <param name="dtDetail"></param>
        /// <param name="dtValue"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveTempHumidityH(DataTable dtHeader, DataTable dtDetail, DataTable dtValue, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                string strErrRtn = "";
                string strManageNo = "";
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    //파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ManageNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckTime", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocumentNo", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["DocumentNo"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["EquipLocCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["AbnormalDesc"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 2000);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strManageNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATTempHumidityH", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    //공장정보, 관리번호 저장
                    string strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                    strManageNo = ErrRtn.mfGetReturnValue(0);

                    //상세 및 상세 데이터 삭제
                    //ParticleD clsD = new ParticleD();
                    TempHumidityD clsDetail = new TempHumidityD();
                    TempHumidityDValue clsdDetailValue = new TempHumidityDValue();

                    //상세 정보 등록
                    if (dtDetail.Rows.Count > 0)
                    {
                        strErrRtn = clsDetail.mfSaveHumidityD(dtDetail, strPlantCode, strManageNo, strUserIP, strUserID, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                    //상세데이터 등록
                    if (dtValue.Rows.Count > 0)
                    {
                        strErrRtn = clsdDetailValue.mfSaveTempHumidityDValue(dtValue, strPlantCode, strManageNo, strUserIP, strUserID, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                }

                if (ErrRtn.ErrNum == 0)
                {
                    ErrRtn.mfAddReturnValue(strManageNo);
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// Header 상세 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strManageNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATTempHumidityHDetail(string strPlantCode, string strManageNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATTempHumidityHDetail", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 온습도 측정 정보 삭제
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strManageNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATtempHumidityH(string strPlantCode, string strManageNo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATTempHumidityH", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("TempHumidityD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class TempHumidityD : ServicedComponent
    {
        /// <summary>
        /// Set DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("Type", typeof(string));
                dtRtn.Columns.Add("ValuePoint", typeof(string));
                dtRtn.Columns.Add("ValueAVG", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 온습도 측정정보값 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strManageNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadTempHumidityD(string strPlantCode, string strManageNo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATTempHumidityD", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// Detail 저장
        /// </summary>
        /// <param name="dtDetail"></param>
        /// <param name="strPlantCode"></param>
        /// <param name="strManageNo"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveHumidityD(DataTable dtDetail, string strPlantCode, string strManageNo, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    //파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strType", ParameterDirection.Input, SqlDbType.Char, dtDetail.Rows[i]["Type"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intValuePoint", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ValuePoint"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decValueAVG", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["ValueAVG"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_QATTempHumidityD", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;

                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("TempHumidityDValue")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class TempHumidityDValue : ServicedComponent
    {
        /// <summary>
        /// Set DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("Type", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(int));
                dtRtn.Columns.Add("Value", typeof(decimal));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Detail Value 저장
        /// </summary>
        /// <param name="dtValue"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveTempHumidityDValue(DataTable dValue, string strPlantCode, string strManageNo, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dValue.Rows.Count; i++)
                {
                    //파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strType", ParameterDirection.Input, SqlDbType.Char, dValue.Rows[i]["Type"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dValue.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decValue", ParameterDirection.Input, SqlDbType.Decimal, dValue.Rows[i]["Value"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_QATTempHumidityDValue", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;

                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }







    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ParticleH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ParticleH : ServicedComponent
    {
        /// <summary>
        /// 데이터 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManageNo", typeof(string));
                dtRtn.Columns.Add("CheckDate", typeof(string));
                dtRtn.Columns.Add("CheckTime", typeof(string));
                dtRtn.Columns.Add("CheckUserID", typeof(string));
                dtRtn.Columns.Add("DocumentNo", typeof(string));
                dtRtn.Columns.Add("EquipLocCode", typeof(string));
                dtRtn.Columns.Add("AbnormalDesc", typeof(string));
                dtRtn.Columns.Add("ActionDesc", typeof(string));
                dtRtn.Columns.Add("ActionFile", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 축정정보 헤더정보
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strEquipLocCode">위치정보</param>
        /// <param name="strFromDate">점검시작일</param>
        /// <param name="strToDate">점검종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>측정정보</returns>
        [AutoComplete]
        public DataTable mfReadParticleH(string strPlantCode, string strEquipLocCode, string strFromDate, string strToDate, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipLocCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATParticleH", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 축정정보 헤더정보_Detail
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strManageNo">관리번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>측정정보</returns>
        [AutoComplete]
        public DataTable mfReadParticleH_Detail(string strPlantCode, string strManageNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATParticleH_Detail", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 측정정보  저장
        /// </summary>
        /// <param name="dtHeader">헤더정보</param>
        /// <param name="dtDetail">상세정보</param>
        /// <param name="dtDetailValue">상세데이터 정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveParticleH(DataTable dtHeader, DataTable dtDetail, DataTable dtDetailValue, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                string strErrRtn = "";
                string strManageNo = "";
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    //파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ManageNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckTime", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCheckUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["CheckUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strDocumentNo", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["DocumentNo"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipLocCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["EquipLocCode"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strAbnormalDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["AbnormalDesc"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ActionDesc"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionFile", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ActionFile"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 2000);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strManageNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATParticleH", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    //공장정보, 관리번호 저장
                    string strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                    strManageNo = ErrRtn.mfGetReturnValue(0);

                    //상세 및 상세 데이터 삭제
                    ParticleD clsD = new ParticleD();
                    strErrRtn = clsD.mfDeleteParticleD(strPlantCode, strManageNo, sql.SqlCon, trans);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                    //상세 정보 등록
                    if (dtDetail.Rows.Count > 0)
                    {

                        strErrRtn = clsD.mfSaveParticleD(dtDetail, strPlantCode, strManageNo, strUserIP, strUserID, sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                    //상세데이터 등록
                    if (dtDetailValue.Rows.Count > 0)
                    {

                        strErrRtn = clsD.mfSaveParticleDValue(dtDetailValue, strPlantCode, strManageNo, strUserIP, strUserID, sql.SqlCon, trans);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                }

                if (ErrRtn.ErrNum == 0)
                {
                    ErrRtn.mfAddReturnValue(strManageNo);
                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 측정정보 삭제
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strManageNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteParticleH(string strPlantCode, string strManageNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                string strErrRtn = "";

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                //파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시져 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATParticleH", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ParticleD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ParticleD : ServicedComponent
    {
        #region Detail

        /// <summary>
        /// 데이터 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManageNo", typeof(string));
                dtRtn.Columns.Add("Type", typeof(string));
                dtRtn.Columns.Add("ValuePoint", typeof(string));
                dtRtn.Columns.Add("ValueAVG", typeof(string));

                return dtRtn;

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 축정정보 상세정보
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strManageNo">관리번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>측정정보</returns>
        [AutoComplete]
        public DataTable mfReadParticleD(string strPlantCode, string strManageNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATParticleD", dtParam);
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtRtn.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 측정정보상세 저장
        /// </summary>
        /// <param name="dtDetail">상세정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveParticleD(DataTable dtDetail, string strPlantCode, string strManageNo, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    //파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strType", ParameterDirection.Input, SqlDbType.Char, dtDetail.Rows[i]["Type"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intValuePoint", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ValuePoint"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intValueAVG", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["ValueAVG"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_QATParticleD", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;

                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 측정정보 상세 및 데이터 삭제
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strManageNo">관리번호</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteParticleD(string strPlantCode, string strManageNo, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                //파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시져 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATParticleD", dtParam);

                // 결과검사
                //ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        #endregion

        #region Value

        /// <summary>
        /// 데이터 컬럼설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfDataSetInfo_Value()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManageNo", typeof(string));
                dtRtn.Columns.Add("Type", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(string));
                dtRtn.Columns.Add("Value", typeof(string));

                return dtRtn;

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 측정Value정보  저장
        /// </summary>
        /// <param name="dtDetail">상세Value정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveParticleDValue(DataTable dtDetailValue, string strPlantCode, string strManageNo, string strUserIP, string strUserID, SqlConnection sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtDetailValue.Rows.Count; i++)
                {
                    //파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strType", ParameterDirection.Input, SqlDbType.Char, dtDetailValue.Rows[i]["Type"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDetailValue.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intValue", ParameterDirection.Input, SqlDbType.Decimal, dtDetailValue.Rows[i]["Value"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_QATParticleDValue", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;

                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        #endregion

    }



}
