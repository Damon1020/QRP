﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질보증관리                                          */
/* 프로그램ID   : clsQATMSA.cs                                          */
/* 프로그램명   : MSA관리                                               */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-09-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]
namespace QRPQAT.BL.QATMSA
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MSAH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MSAH : ServicedComponent
    {
        /// <summary>
        /// MSA 등록 저장용 DataTable 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfoRegist()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ProjectCode", typeof(string));
                dtRtn.Columns.Add("ProjectName", typeof(string));
                dtRtn.Columns.Add("ProcessCode", typeof(string));
                dtRtn.Columns.Add("MeasureToolCode", typeof(string));
                dtRtn.Columns.Add("MaterialCode", typeof(string));
                dtRtn.Columns.Add("InspectItemDesc", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("InspectUserID", typeof(string));
                dtRtn.Columns.Add("InspectDesc", typeof(string));
                dtRtn.Columns.Add("StabilityFlag", typeof(string));
                dtRtn.Columns.Add("AccuracyFlag", typeof(string));
                dtRtn.Columns.Add("AccuracyValue", typeof(decimal));
                dtRtn.Columns.Add("RRValueUseFlag", typeof(string));
                dtRtn.Columns.Add("RRValueChangeFlag", typeof(string));
                dtRtn.Columns.Add("RRValueChangeValue", typeof(decimal));
                dtRtn.Columns.Add("UpperSpec", typeof(decimal));
                dtRtn.Columns.Add("LowerSpec", typeof(decimal));
                dtRtn.Columns.Add("SpecRange", typeof(string));
                dtRtn.Columns.Add("UnitCode", typeof(string));
                dtRtn.Columns.Add("RepeatCount", typeof(Int32));
                dtRtn.Columns.Add("PartCount", typeof(Int32));
                dtRtn.Columns.Add("MeasureCount", typeof(Int32));
                dtRtn.Columns.Add("RNRDataInputFlag", typeof(string));
                dtRtn.Columns.Add("RNRAnalysisFlag", typeof(string));
                dtRtn.Columns.Add("RnRAnalysisDate", typeof(string));
                dtRtn.Columns.Add("RnRAnalysisRemark", typeof(string));
                dtRtn.Columns.Add("EquipmentVariation", typeof(decimal));
                dtRtn.Columns.Add("EquipmentPercent", typeof(decimal));
                dtRtn.Columns.Add("AppraiserVariation", typeof(decimal));
                dtRtn.Columns.Add("AppraiserPercent", typeof(decimal));
                dtRtn.Columns.Add("RNRValue", typeof(decimal));
                dtRtn.Columns.Add("RNRPercent", typeof(decimal));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MSA등록 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectname">프로젝트명</param>
        /// <param name="strInspectDateFrom">검사일From</param>
        /// <param name="strInspectDateTo">검사일To</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMSAH(string strPlantCode, string strProjectname, string strInspectDateFrom, string strInspectDateTo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // 디비연결
                sql.mfConnect();

                // 파라미터 데이터테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectname", ParameterDirection.Input, SqlDbType.NVarChar, strProjectname, 200);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMSAH", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MSA등록 헤더 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMSAHDetail(string strPlantCode, string strProjectCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMSAHDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MSA Data등록 헤더 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMSAHDetail_Data(string strPlantCode, string strProjectCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMSAHDetail_Data", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MSA R&R분석 헤더 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strMeasureToolCode"></param>
        /// <param name="strInspectDateFrom"></param>
        /// <param name="strInspectDateTo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMSAHRaR(string strPlantCode, string strMeasureToolCode, string strInspectDateFrom, string strInspectDateTo, string strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, strMeasureToolCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectDateTo", ParameterDirection.Input, SqlDbType.VarChar, strInspectDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMASH_RaR", dtParam);

                return dt;
            }
            catch(Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// MSA상세 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strProjectCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMSAHRnR_Detail(string strPlantCode, string strProjectCode, string strLang)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMSAHRnR_Detail", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// QATMSA R&R분석 저장 메소드
        /// </summary>
        /// <param name="dtHeader"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATMSAHRnR(DataTable dtHeader, string strUserIP, string strUserID, DataTable dtDetail)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ProjectCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strRNRAnalysisFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["RNRAnalysisFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strRnRAnalysisDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["RnRAnalysisDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_decEquipmentVariation", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["EquipmentVariation"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decEquipmentPercent", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["EquipmentPercent"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decAppraiserVariation", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["AppraiserVariation"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decAppraiserPercent", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["AppraiserPercent"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decRNRValue", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["RNRValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decRNRPercent", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["RNRPercent"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATMSARnRHeader", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        if (dtDetail.Rows.Count > 0)
                        {
                            MSAD clsDetail = new MSAD();
                            strErrRtn = clsDetail.mfSaveQATMSAD_RnR(dtDetail, strUserIP, strUserID, sql, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                }
                trans.Commit();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);        
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MSA등록 헤더 저장 Method
        /// </summary>
        /// <param name="dtHeader">헤더정보 데이터 테이블</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="dtDetail">상세정보 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATMSAH(DataTable dtHeader, string strUserID, string strUserIP, DataTable dtDetail)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // 트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ProjectCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProjectName", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ProjectName"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["MeasureToolCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectItemDesc", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectItemDesc"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InspectDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strStabilityFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["StabilityFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strAccuracyFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AccuracyFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblAccuracyValue", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["AccuracyValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strRRValueUseFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["RRValueUseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strRRValueChangeFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["RRValueChangeFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblRRValueChangeValue", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["RRValueChangeValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["UpperSpec"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtHeader.Rows[i]["LowerSpec"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["SpecRange"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["UnitCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_intRepeatCount", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["RepeatCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intPartCount", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["PartCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intMeasureCount", ParameterDirection.Input, SqlDbType.Int, dtHeader.Rows[i]["MeasureCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strRNRDataInputFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["RNRDataInputFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strRNRAnalysisFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["RNRAnalysisFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strProjectCode", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATMSAH_Regist", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // ProjectCode 저장
                        string strProjectCode = ErrRtn.mfGetReturnValue(0);

                        // 성공시 상세정보 저장
                        if (dtDetail.Rows.Count > 0)
                        {
                            MSAD clsDetail = new MSAD();
                            strErrRtn = clsDetail.mfSaveQATMSADRegist(dtDetail, strUserID, strUserIP, sql.SqlCon, trans, strProjectCode);
                            
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// MSA등록 헤더정보 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATMSAH(string strPlantCode, string strProjectCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = "";
                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                
                // 상세정보부터 삭제
                MSAD clsDetail = new MSAD();
                strErrRtn = clsDetail.mfDeleteQATMSAD(strPlantCode, strProjectCode, sql.SqlCon, trans);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                // 헤더정보 삭제
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATMSAH", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MSAD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MSAD : ServicedComponent
    {
        /// <summary>
        /// MSA등록 저장용 데이터테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfoRegist()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ProjectCode", typeof(string));
                dtRtn.Columns.Add("ItemSeq", typeof(Int32));
                dtRtn.Columns.Add("MachineName", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("EquipmentVariation", typeof(decimal));
                dtRtn.Columns.Add("EquipmentPercent", typeof(decimal));
                dtRtn.Columns.Add("AppraiserVariation", typeof(decimal));
                dtRtn.Columns.Add("AppraiserPercent", typeof(decimal));
                dtRtn.Columns.Add("RNRValue", typeof(decimal));
                dtRtn.Columns.Add("RNRPercent", typeof(decimal));
                dtRtn.Columns.Add("AccuracyValue", typeof(decimal));
                dtRtn.Columns.Add("StabilityValue", typeof(decimal));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MSA등록 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMSADRegist(string strPlantCode, string strProjectCode)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMSADRegist", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
        /// <summary>
        /// MSA R&R분석 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strProjectCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMSAD_RnR(string strPlantCode, string strProjectCode)
        {
            DataTable dt = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMSAD_RnR", dtParam);

                return dt;
            }
            catch(Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// MSA등록 상세정보 저장 Method
        /// </summary>
        /// <param name="dtDetail">상세정보 데이터 테이블</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATMSADRegist(DataTable dtDetail, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strProjectCode)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strMachineName", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["MachineName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATMSAD_Regist", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        [AutoComplete(false)]
        public string mfSaveQATMSAD_RnR(DataTable dtDetail, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    //저장시 EV, %EV, AV, %AV, R&R, %R&R 저장하고 
                    // 상세 그리드 "상세결과분석, 안정성,정확성분석" 에 테이블정보도 저장처리

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["ProjectCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDetail.Rows[i]["ItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipmentVariation", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["EquipmentVariation"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipmentPercent", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["EquipmentPercent"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strAppraiserVariation", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["AppraiserVariation"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strAppraiserPercent", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["AppraiserPercent"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strRNRValue", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["RNRValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strRNRPercent", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["RNRPercent"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strAccuracyValue", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["AccuracyValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strStabilityValue", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["StabilityValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATMSAD_RnR", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }

        /// <summary>
        /// MSA등록 상세정보 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProjectCode">프로젝트코드</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATMSAD(string strPlantCode, string strProjectCode, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                //MSAData삭제
                MSAData clsData = new MSAData();
                strErrRtn = clsData.mfDeleteQATMSAData(strPlantCode, strProjectCode, sql.SqlCon, trans);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATMSAD", dtParam);
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MSAData")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MSAData : ServicedComponent
    {
        /// <summary>
        /// MSAData 데이터테이블
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtData = new DataTable();
            try
            {
                dtData.Columns.Add("PlantCode", typeof(String));
                dtData.Columns.Add("ProjectCode", typeof(String));
                dtData.Columns.Add("ItemSeq", typeof(Int32));
                dtData.Columns.Add("DataSeq", typeof(Int32));
                dtData.Columns.Add("PartSeq", typeof(Int32));
                dtData.Columns.Add("RepeatSeq", typeof(Int32));
                dtData.Columns.Add("MeasureValue", typeof(Decimal));
                return dtData;
            }
            catch(Exception ex)
            {
                return dtData;
                throw(ex);
            }
            finally
            {
                dtData.Dispose();
            }
        }

        /// <summary>
        /// MSAData 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strProjectCode"></param>
        /// <param name="intItemSeq"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMSAData(String strPlantCode, String strProjectCode, int intItemSeq, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtData = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_intItemSeq", ParameterDirection.Input, SqlDbType.Int, intItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtData = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMSAData", dtParam);

                return dtData;
            }
            catch(Exception ex)
            {
                return dtData;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtData.Dispose();
            }
        }

        /// <summary>
        /// 상세 데이터 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveQATMSAData(DataTable dt, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["ProjectCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intItemSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["ItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intDataSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["DataSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intPartSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["PartSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intRepeatSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["RepeatSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_decMeasureValue", ParameterDirection.Input, SqlDbType.Decimal, dt.Rows[i]["MeasureValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATMSAData", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MSAData 삭제
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strProjectCode"></param>
        /// <param name="sqlCon"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATMSAData(string strPlantCode, string strProjectCode, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";
            try
            {
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProjectCode", ParameterDirection.Input, SqlDbType.VarChar, strProjectCode, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATMSAData", dtParam);
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { }
        }
    }
}
