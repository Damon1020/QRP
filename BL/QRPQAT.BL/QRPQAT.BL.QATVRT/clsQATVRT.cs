﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질보증관리                                          */
/* 프로그램ID   : clsQATENV.cs                                          */
/* 프로그램명   : 검교정관리                                            */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-09-14                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPQAT.BL.QATVRT
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InVerityH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InVerityH : ServicedComponent
    {
        /// <summary>
        /// 내부검증헤더 정보 데이터 컬럼셋
        /// </summary>
        /// <returns>내부검증헤더 컬럼</returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtInVerityH = new DataTable();

            try
            {
                dtInVerityH.Columns.Add("PlantCode", typeof(string));
                dtInVerityH.Columns.Add("InVerityNo", typeof(string));
                dtInVerityH.Columns.Add("VerityNumber", typeof(string));
                dtInVerityH.Columns.Add("VerityTarget", typeof(string));
                dtInVerityH.Columns.Add("VerityType", typeof(string));
                dtInVerityH.Columns.Add("VerityTargetCode", typeof(string));
                dtInVerityH.Columns.Add("VerityUserID", typeof(string));
                dtInVerityH.Columns.Add("VerityDate", typeof(string));
                dtInVerityH.Columns.Add("Temperature", typeof(string));
                dtInVerityH.Columns.Add("Humidity", typeof(string));
                dtInVerityH.Columns.Add("NextVerityDate", typeof(string));
                dtInVerityH.Columns.Add("VerityBasic", typeof(string));
                dtInVerityH.Columns.Add("VerityResult", typeof(string));
                dtInVerityH.Columns.Add("FileName", typeof(string));
                dtInVerityH.Columns.Add("EtcDesc", typeof(string));
                dtInVerityH.Columns.Add("ConfirmDate", typeof(string));
                dtInVerityH.Columns.Add("ConfirmUserID", typeof(string));
                dtInVerityH.Columns.Add("CompleteFlag", typeof(string));
                dtInVerityH.Columns.Add("SendMDM", typeof(string));

                return dtInVerityH;
            }
            catch (Exception ex)
            {
                return dtInVerityH;
                throw (ex);
            }
            finally
            {
                dtInVerityH.Dispose();
            }
        }

        /// <summary>
        /// 내부검증헤더 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strTargetCode">타겟코드(설비or계측기)</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>내부검증헤더정보</returns>
        [AutoComplete]
        public DataTable mfReadInVerityH(string strPlantCode, string strTarget, string strTargetCode ,string strFromDate , string strToDate, string strLang)
        {
            DataTable dtInVerityH = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityTarget", ParameterDirection.Input, SqlDbType.VarChar, strTarget, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityTargetCode", ParameterDirection.Input, SqlDbType.VarChar, strTargetCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtInVerityH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATInVerityH", dtParam);

                return dtInVerityH;
            }
            catch (Exception ex)
            {
                return dtInVerityH;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtInVerityH.Dispose();
            }
                
        }


        /// <summary>
        /// 내부검증헤더 상세
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strInVerityNo">일련번호</param>
        /// <param name="strVerityNumber">검증번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInVerityH_Detail(string strPlantCode, string strInVerityNo, string strVerityNumber, string strLang)
        {
            DataTable dtInVerityH = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInVerityNo", ParameterDirection.Input, SqlDbType.VarChar, strInVerityNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar, strVerityNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                dtInVerityH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATInVerityH_Detail", dtParam);

                return dtInVerityH;
            }
            catch (Exception ex)
            {
                return dtInVerityH;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtInVerityH.Dispose();
            }

        }

        /// <summary>
        /// 내부검증등록 설비등록시 MDM I/F
        /// </summary>
        /// <param name="dtInVerityH">헤더정보 데이터 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveInVerityH_MDMIF(DataTable dtInVerityH, string strInVerityNo, string strVerityNumber, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();
                string strErrRtn = string.Empty;
                //SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();

                // - 파라미터 값 저장 - //
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInVerityNo", ParameterDirection.Input, SqlDbType.VarChar, strInVerityNo, 7);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar, strVerityNumber, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityTargetCode", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["VerityTargetCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strNextVerityDate", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["NextVerityDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //- 프로시저 실행 - //
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_QATInVerityH_MDMIF", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                ////if (ErrRtn.ErrNum != 0)
                ////{
                ////    //trans.Rollback();
                ////}
                ////else
                ////{
                ////    //trans.Commit();
                ////}

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 내부검증헤더 등록
        /// </summary>
        /// <param name="dtInVerityH">등록할 정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveInVerityH(DataTable dtInVerityH,DataTable dtInVerityD,DataTable dtInVerityDel,string strUserIP,string strUserID )
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                if (dtInVerityH.Rows.Count <= 0)
                {
                    ErrRtn.ErrNum = -1;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);

                }
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();

                // - 파라미터 값 저장 - //
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInVerityNo", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["InVerityNo"].ToString(), 7);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["VerityNumber"].ToString(), 4);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityTarget", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["VerityTarget"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityType", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["VerityType"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityTargetCode", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["VerityTargetCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityUserID", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["VerityUserID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityDate", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["VerityDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strTemperature", ParameterDirection.Input, SqlDbType.NVarChar, dtInVerityH.Rows[0]["Temperature"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strHumidity", ParameterDirection.Input, SqlDbType.NVarChar, dtInVerityH.Rows[0]["Humidity"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strNextVerityDate", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["NextVerityDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityBasic", ParameterDirection.Input, SqlDbType.NVarChar, dtInVerityH.Rows[0]["VerityBasic"].ToString(), 100);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityResult", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["VerityResult"].ToString(), 2);
                sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtInVerityH.Rows[0]["FileName"].ToString(), 1000);
                sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtInVerityH.Rows[0]["EtcDesc"].ToString(), 200);
                sql.mfAddParamDataRow(dtParam, "@i_strConfirmUserID", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["ConfirmUserID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strConfirmDate", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityH.Rows[0]["ConfirmDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtInVerityH.Rows[0]["CompleteFlag"].ToString(), 1);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                
                sql.mfAddParamDataRow(dtParam, "@o_strInVerityNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strVerityNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);


                //- 프로시저 실행 - //
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATInVerityH", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 처리 실패시 롤백시킨다. /
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                // -- 처리 성공 시 상세를 처리한다 --//
                else
                {
                    string strInVerityNo = "";
                    string strVerityNumber = "";
                    string strPlantCode = "";

                    //-- 리턴값저장,공장 저장 --/
                    strPlantCode = dtInVerityH.Rows[0]["PlantCode"].ToString();
                    strInVerityNo = ErrRtn.mfGetReturnValue(0);
                    strVerityNumber = ErrRtn.mfGetReturnValue(1);

                    if (dtInVerityDel.Rows.Count > 0 || dtInVerityD.Rows.Count > 0)
                    {

                        InVerityD clsInVerityD = new InVerityD();                        

                        //--행삭제부터 삭제
                        if (dtInVerityDel.Rows.Count > 0)
                        {
                            strErrRtn = clsInVerityD.mfDeleteInVerityD(strPlantCode, dtInVerityDel, sql.SqlCon, trans);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }

                        // -- 상세저장 --//
                        if (dtInVerityH.Rows.Count > 0)
                        {
                            strErrRtn = clsInVerityD.mfSaveInVerityD(strPlantCode, strInVerityNo, strVerityNumber, dtInVerityD, strUserIP, strUserID, sql.SqlCon, trans);
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                return strErrRtn;
                            }
                        }
                    }

                    if (ErrRtn.ErrNum.Equals(0))
                        trans.Commit();

                    // 설비 등록이면 MDM I/F
                    if (dtInVerityH.Rows[0]["CompleteFlag"].ToString().Equals("T")
                        && dtInVerityH.Rows[0]["VerityTarget"].ToString().Equals("M")
                        && dtInVerityH.Rows[0]["SendMDM"].ToString().Equals("T"))
                    {
                        string strErr = mfSaveInVerityH_MDMIF(dtInVerityH, strInVerityNo, strVerityNumber, strUserIP, strUserID);
                    }
                }

                // - 처리결과값 - 
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 내부검증정보 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInVerityNo">관리번호</param>
        /// <param name="strVerityNumber">일련번호</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteInVerity(string strPlantCode,string strInVerityNo,string strVerityNumber)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction(); 

                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터값 저장
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strInVerityNo", ParameterDirection.Input, SqlDbType.VarChar,strInVerityNo,7);
                sql.mfAddParamDataRow(dtParam,"@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar,strVerityNumber,4);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATInVerityH", dtParam);

                // 결과값 Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 결과값이 실패인 경우 롤백  성공인경우 커밋처리를 한다.
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InVerityD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InVerityD : ServicedComponent
    {
        /// <summary>
        /// 내부검증상세 컬럼셋
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtInVerityD = new DataTable();

            try
            {
                //dtInVerityD.Columns.Add("PlantCode", typeof(string));
                dtInVerityD.Columns.Add("Seq", typeof(string));
                dtInVerityD.Columns.Add("MeasureToolCode", typeof(string));

                dtInVerityD.Columns.Add("InVerityNo", typeof(string));
                dtInVerityD.Columns.Add("VerityNumber", typeof(string));
                //dtInVerityD.Columns.Add("", typeof(string));
                //dtInVerityD.Columns.Add("", typeof(string));
                //dtInVerityD.Columns.Add("", typeof(string));
                //dtInVerityD.Columns.Add("", typeof(string));
                return dtInVerityD;
            }
            catch (Exception ex)
            {
                return dtInVerityD;
                throw (ex);
            }
            finally
            {
                dtInVerityD.Dispose();
            }
        }

        /// <summary>
        /// 내부검증 상세등록
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInVerityNo">검증번호</param>
        /// <param name="strVerityNumber">검증일련번호</param>
        /// <param name="dtInVerityD">등록할 정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveInVerityD(string strPlantCode,string strInVerityNo,string strVerityNumber ,DataTable dtInVerityD,string strUserIP,string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                for (int i = 0; i < dtInVerityD.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInVerityNo", ParameterDirection.Input, SqlDbType.VarChar, strInVerityNo, 7);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityD.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar, strVerityNumber, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityD.Rows[i]["MeasureToolCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@o_strInVerityNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strVerityNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Update_QATInVerityD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                    
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();   
            }
        }


        /// <summary>
        /// 내부검증상세 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="dtInVerityDel">삭제할정보</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteInVerityD(string strPlantCode, DataTable dtInVerityDel ,SqlConnection sqlcon,SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                for (int i = 0; i < dtInVerityDel.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInVerityNo", ParameterDirection.Input, SqlDbType.VarChar, dtInVerityDel.Rows[i]["InVerityNo"].ToString(), 7);
                    sql.mfAddParamDataRow(dtParam,"@i_intSeq", ParameterDirection.Input, SqlDbType.Int,dtInVerityDel.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam,"@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar,dtInVerityDel.Rows[i]["VerityNumber"].ToString(),4);

                    sql.mfAddParamDataRow(dtParam, "@o_strInVerityNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strVerityNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_QATInVerityD", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                    
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


        /// <summary>
        /// 내부검증상세 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInVerityNo">관리번호</param>
        /// <param name="strVerityNumber">일련번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>내부검증상세정보</returns>
        [AutoComplete]
        public DataTable mfReadInVerityD(string strPlantCode, string strInVerityNo,string strVerityNumber,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtInVerityD = new DataTable();
            try
            {
                //디비오픈
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터 저장
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strInVerityNo", ParameterDirection.Input, SqlDbType.VarChar,strInVerityNo,7);
                sql.mfAddParamDataRow(dtParam,"@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar,strVerityNumber,4);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);
                
                //프로시저 실행
                dtInVerityD = sql.mfExecReadStoredProc(sql.SqlCon,"up_Select_QATInVerityD",dtParam);

                //정보리턴
                return dtInVerityD;
            }
            catch (Exception ex)
            {
                return dtInVerityD;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtInVerityD.Dispose();
            }
        }

    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("OutVerity")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class OutVerity : ServicedComponent
    {
        /// <summary>
        /// 외부교정 의뢰 컬럼셋
        /// </summary>
        /// <returns>컬럼셋팅된 DataTable</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtOutVerity = new DataTable();
            try
            {
                dtOutVerity.Columns.Add("PlantCode", typeof(string));
                dtOutVerity.Columns.Add("OutVerityNo", typeof(string));
                dtOutVerity.Columns.Add("VerityNumber", typeof(string));
                dtOutVerity.Columns.Add("VerityTarget", typeof(string));
                dtOutVerity.Columns.Add("VerityTargetCode", typeof(string));
                dtOutVerity.Columns.Add("VerityType", typeof(string));
                dtOutVerity.Columns.Add("MeasureToolCode", typeof(string));
                dtOutVerity.Columns.Add("EtcDesc", typeof(string));
                dtOutVerity.Columns.Add("VerityStatus", typeof(string));
                dtOutVerity.Columns.Add("NextVerityDate", typeof(string));
                dtOutVerity.Columns.Add("LatelyVerityDate", typeof(string));
                dtOutVerity.Columns.Add("FileName", typeof(string));
                dtOutVerity.Columns.Add("EndVerityDate", typeof(string));
                dtOutVerity.Columns.Add("RequestUserID", typeof(string));
                dtOutVerity.Columns.Add("RequestDate", typeof(string));
                dtOutVerity.Columns.Add("RequestFlag", typeof(string));
                dtOutVerity.Columns.Add("RequestDesc", typeof(string));
                dtOutVerity.Columns.Add("ReceiptUserID", typeof(string));
                dtOutVerity.Columns.Add("ReceiptDate", typeof(string));
                dtOutVerity.Columns.Add("ReceiptFlag", typeof(string));
                dtOutVerity.Columns.Add("ReceiptDesc", typeof(string));
                dtOutVerity.Columns.Add("VerityUserID", typeof(string));
                dtOutVerity.Columns.Add("VerityDate", typeof(string));
                dtOutVerity.Columns.Add("VerityFlag", typeof(string));
                dtOutVerity.Columns.Add("VerityDesc", typeof(string));
                dtOutVerity.Columns.Add("CompleteUserID", typeof(string));
                dtOutVerity.Columns.Add("CompleteDate", typeof(string));
                dtOutVerity.Columns.Add("CompleteFlag", typeof(string));
                dtOutVerity.Columns.Add("CompleteDesc", typeof(string));
                dtOutVerity.Columns.Add("SendMDM", typeof(string));

                return dtOutVerity;
            }
            catch (Exception ex)
            {
                return dtOutVerity;
                throw (ex);
            }
            finally
            {
                dtOutVerity.Dispose();
            }
        }

        /// <summary>
        /// 외부교정의뢰 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strMeasureToolCode">계측기코드</param>
        /// <param name="strEndVerityDate">교정만료일</param>
        /// <param name="strVerityStatus">상태</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>외부교정의뢰정보</returns>
        [AutoComplete]
        public DataTable mfReadOutVerity(string strPlantCode, string strMeasureToolCode, string strFromDate,string strToDate, string strVerityStatus , string strLang)
        {
            DataTable dtOutVerity = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                // -- 파라미터 값 저장 
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, strMeasureToolCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityStatus", ParameterDirection.Input, SqlDbType.VarChar, strVerityStatus, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 실행
                dtOutVerity = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATOutVerity", dtParam);

                //외부 교정의뢰 정보 리턴
                return dtOutVerity;
            }
            catch (Exception ex)
            {
                return dtOutVerity;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtOutVerity.Dispose();
            }
        }


        /// <summary>
        /// 외부교정의뢰 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strTarget">설비 : M 계측기 : E</param>
        /// <param name="strTargetCode">TargetCode</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strVerityStatus">상태</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>외부교정의뢰정보</returns>
        [AutoComplete]
        public DataTable mfReadOutVerity_Target(string strPlantCode, string strTarget ,string strTargetCode, string strFromDate, string strToDate, string strVerityStatus, string strLang)
        {
            DataTable dtOutVerity = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                // -- 파라미터 값 저장 
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strTarget", ParameterDirection.Input, SqlDbType.VarChar, strTarget, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strTargetCode", ParameterDirection.Input, SqlDbType.VarChar, strTargetCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityStatus", ParameterDirection.Input, SqlDbType.VarChar, strVerityStatus, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 실행
                dtOutVerity = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATOutVerity_Target", dtParam);

                //외부 교정의뢰 정보 리턴
                return dtOutVerity;
            }
            catch (Exception ex)
            {
                return dtOutVerity;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtOutVerity.Dispose();
            }
        }

        /// <summary>
        /// 외부교정의뢰 정보 상세 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strOutVerityNo">관리번호</param>
        /// <param name="strVerityNumber">일련번호</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>외부교정의뢰상세정보</returns>
        [AutoComplete]
        public DataTable mfReadOutVerity_Detail(string strPlantCode, string strOutVerityNo, string strVerityNumber, string strTarget ,string strLang)
        {
            DataTable dtOutVerity = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                // -- 파라미터 값 저장 
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strOutVerityNo", ParameterDirection.Input, SqlDbType.VarChar, strOutVerityNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar, strVerityNumber, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strTarget", ParameterDirection.Input, SqlDbType.VarChar, strTarget, 1);
  
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 실행
                dtOutVerity = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATOutVerity_Detail", dtParam);

                //외부 교정의뢰 정보 리턴
                return dtOutVerity;
            }
            catch (Exception ex)
            {
                return dtOutVerity;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtOutVerity.Dispose();
            }
        }


        /// <summary>
        /// 외부교정 의뢰 등록
        /// </summary>
        /// <param name="dtOutVerity">등록할 정보</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <param name="strUserID">사용자 아이디</param>
        /// <returns> 처리결과,OutPut값</returns>
        [AutoComplete(false)]
        public string mfSaveOutVerity(DataTable dtOutVerity,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                if (dtOutVerity.Rows.Count <= 0)
                {
                    ErrRtn.ErrNum = -1;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);

                }
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strOutVerityNo", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["OutVerityNo"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["VerityNumber"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityTarget", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["VerityTarget"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityTargetCode", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["VerityTargetCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityType", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["VerityType"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["MeasureToolCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtOutVerity.Rows[0]["EtcDesc"].ToString(), 200);
                sql.mfAddParamDataRow(dtParam, "@i_strNextVerityDate", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["NextVerityDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityStatus", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["VerityStatus"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strEndVerityDate", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["EndVerityDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLatelyVerityDate", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["LatelyVerityDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtOutVerity.Rows[0]["FileName"].ToString(), 1000);

                sql.mfAddParamDataRow(dtParam, "@i_strRequestUserID", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["RequestUserID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strRequestDate", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["RequestDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strRequestFlag", ParameterDirection.Input, SqlDbType.Char, dtOutVerity.Rows[0]["RequestFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strRequestDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtOutVerity.Rows[0]["RequestDesc"].ToString(), 200);

                sql.mfAddParamDataRow(dtParam, "@i_strReceiptUserID", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["ReceiptUserID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["ReceiptDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReceiptFlag", ParameterDirection.Input, SqlDbType.Char, dtOutVerity.Rows[0]["ReceiptFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strReceiptDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtOutVerity.Rows[0]["ReceiptDesc"].ToString(), 200);

                sql.mfAddParamDataRow(dtParam, "@i_strVerityUserID", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["VerityUserID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityDate", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["VerityDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityFlag", ParameterDirection.Input, SqlDbType.Char, dtOutVerity.Rows[0]["VerityFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtOutVerity.Rows[0]["VerityDesc"].ToString(), 200);

                sql.mfAddParamDataRow(dtParam, "@i_strCompleteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["CompleteUserID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["CompleteDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtOutVerity.Rows[0]["CompleteFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strCompleteDescc", ParameterDirection.Input, SqlDbType.NVarChar, dtOutVerity.Rows[0]["CompleteDesc"].ToString(), 200);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_StrUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                
                sql.mfAddParamDataRow(dtParam, "@o_strOutVerityNo", ParameterDirection.Output, SqlDbType.VarChar, 20);   
                sql.mfAddParamDataRow(dtParam, "@o_strVerityNumber", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATOutVerity", dtParam);

                //-- Decoding --//
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // -- 처리결과에 따라서 롤백 하거나 커밋을 한다 --///
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                {
                    trans.Commit();

                    string strOutVerityNo = ErrRtn.mfGetReturnValue(0);
                    string strVerityNumber = ErrRtn.mfGetReturnValue(1);

                    //완료Flag가 T 인 설비정보 MDM의 설비보증인증날짜 업데이트
                    if (dtOutVerity.Rows[0]["VerityTarget"].ToString().Equals("M") 
                        && dtOutVerity.Rows[0]["CompleteFlag"].ToString().Equals("T")
                        && dtOutVerity.Rows[0]["SendMDM"].ToString().Equals("T"))
                    {
                        mfSaveOutVerity_MDMIF(dtOutVerity, strOutVerityNo, strVerityNumber, strUserIP, strUserID, sql.SqlCon);
                    }
                }


                //-- 처리결과 , OUTPUT값을 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 외부교정등록 설비등록시 MDM I/F
        /// </summary>
        /// <param name="dtInVerityH">헤더정보 데이터 테이블</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveOutVerity_MDMIF(DataTable dtOutVerity, string strOutVerityNo, string strVerityNumber, string strUserIP, string strUserID, SqlConnection sqlcon)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {

                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();

                // - 파라미터 값 저장 - //
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strOutVerityNo", ParameterDirection.Input, SqlDbType.VarChar, strOutVerityNo, 7);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar, strVerityNumber, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strVerityTargetCode", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["VerityTargetCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strNextVerityDate", ParameterDirection.Input, SqlDbType.VarChar, dtOutVerity.Rows[0]["NextVerityDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //- 프로시저 실행 - //
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, "up_Update_QATOutVerity_MDMIF", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 외부교정의뢰 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strOutVerityNo">관리번호</param>
        /// <param name="strVerityNumber">일련번호</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteOutVerity(string strPlantCode,string strOutVerityNo,string strVerityNumber)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();

                //트랜젝션 시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                //파라미터값 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strOutVerityNo", ParameterDirection.Input, SqlDbType.VarChar,strOutVerityNo,20);
                sql.mfAddParamDataRow(dtParam,"@i_strVerityNumber", ParameterDirection.Input, SqlDbType.VarChar,strVerityNumber,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);
                
                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATOutVerity", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과에 따라 롤백,커밋을 한다
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }

                //결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }
}
