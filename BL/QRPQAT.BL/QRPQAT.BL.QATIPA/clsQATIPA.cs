﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질보증관리                                          */
/* 프로그램ID   : clsQATIPA.cs                                          */
/* 프로그램명   : 내부공정사고관리                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-01-09                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]
namespace QRPQAT.BL.QATIPA
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InProcessAccidentH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InProcessAccidentH : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼 설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManagementNo", typeof(string));
                dtRtn.Columns.Add("AriseDate", typeof(string));
                dtRtn.Columns.Add("IssueDate", typeof(string));
                dtRtn.Columns.Add("InspectFaultTypeCode", typeof(string));
                dtRtn.Columns.Add("AriseDetailProcessOperationTypeCode", typeof(string));
                dtRtn.Columns.Add("AriseEquipCode", typeof(string));
                dtRtn.Columns.Add("ReasonDesc", typeof(string));
                dtRtn.Columns.Add("ActionDesc", typeof(string));
                dtRtn.Columns.Add("FilePath", typeof(string));
                dtRtn.Columns.Add("ManFlag", typeof(string));
                dtRtn.Columns.Add("MethodFlag", typeof(string));
                dtRtn.Columns.Add("MachineFlag", typeof(string));
                dtRtn.Columns.Add("MaterialFlag", typeof(string));
                dtRtn.Columns.Add("EnviromentFlag", typeof(string));
                dtRtn.Columns.Add("CompleteFlag", typeof(string));
                dtRtn.Columns.Add("ReasonDate", typeof(string));
                dtRtn.Columns.Add("ActionDate", typeof(string));
                dtRtn.Columns.Add("MaterialTreat", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("InspectResult", typeof(string));
                dtRtn.Columns.Add("CAAttachFile", typeof(string));
                dtRtn.Columns.Add("InspectUserID", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 내부공정사고 검색조건에 의한 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strAriseFromDate">발생일From</param>
        /// <param name="strAriseToDate">발생일To</param>
        /// <param name="strCustomerCode">고객코드</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strDetailProcessOperationType">공정Type</param>
        /// <param name="strInspectFaultItemCode">검사항목코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATInProcessAccidentH(string strPlantCode, string strAriseFromDate, string strAriseToDate, string strCustomerCode, string strPackage
                                                , string strLotNo, string strDetailProcessOperationType, string strInspectFaultItemCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strAriseFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAriseDateTo", ParameterDirection.Input, SqlDbType.VarChar, strAriseToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strDetailProcessOperationType", ParameterDirection.Input, SqlDbType.NVarChar, strDetailProcessOperationType, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectFaultItemCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATInProcessAccidentH", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 내부공정사고 헤더 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManagementNo">관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATInProcessAccidentH_Detail(string strPlantCode, string strManagementNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManagementNo", ParameterDirection.Input, SqlDbType.VarChar, strManagementNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATInProcessAccidentH_Detail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 내부공정사고 저장 메소드
        /// </summary>
        /// <param name="dtHeader">헤더정보 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="dtDetail">상세정보 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATInProcessAccidentH(DataTable dtHeader, string strUserID, string strUserIP, DataTable dtDetail)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;
                DataTable dtParam = new DataTable();

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // Parameter 변수 설정
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManagementNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ManagementNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AriseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strIssueDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["IssueDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseDetailProcessOperationTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AriseDetailProcessOperationTypeCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strAriseEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["AriseEquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReasonDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ReasonDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["ActionDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["FilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strManFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["ManFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMethodFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["MethodFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMachineFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["MachineFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["MaterialFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEnviromentFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["EnviromentFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.Char, dtHeader.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReasonDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReasonDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ActionDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialTreat", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["MaterialTreat"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResult", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["InspectResult"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strCAAttachFile", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["CAAttachFile"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strRegistUserID", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strManagementNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 호출7
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATInProcessAccidentH", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // 변수설정
                        string strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                        string strManagementNo = ErrRtn.mfGetReturnValue(0);

                        // 성공시 상세저장
                        // 삭제먼저
                        InProcessAccidentD clsDetail = new InProcessAccidentD();
                        strErrRtn = clsDetail.mfDeleteQATInProcessAccidentD(strPlantCode, strManagementNo, sql.SqlCon, trans);

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            trans.Rollback();
                            break;
                        }

                        // 저장 메소드 호출
                        strErrRtn = clsDetail.mfSaveQATInProcessAccidentD(dtDetail, strUserID, strUserIP, sql.SqlCon, trans, strManagementNo);

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            trans.Rollback();
                            break;
                        }
                    }
                }

                if (ErrRtn.ErrNum.Equals(0) && dtHeader.Rows.Count > 0)
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 내부공정사고등록 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManagementNo">관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATInProcessAccidentH(string strPlantCode, string strManagementNo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManagementNo", ParameterDirection.Input, SqlDbType.VarChar, strManagementNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATInProcessAccidentH", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (!ErrRtn.ErrNum.Equals(0))
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InProcessAccidentD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InProcessAccidentD : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼 설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManagementNo", typeof(string));
                dtRtn.Columns.Add("CustomerCode", typeof(string));
                dtRtn.Columns.Add("Package", typeof(string));                
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("EquipCode", typeof(string));
                dtRtn.Columns.Add("LotQty", typeof(decimal));
                dtRtn.Columns.Add("LossQty", typeof(decimal));
                dtRtn.Columns.Add("Yield", typeof(decimal));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 내부공정사고 등록 상세 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManagementNo">관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATInProcessAccidentD(string strPlantCode, string strManagementNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManagementNo", ParameterDirection.Input, SqlDbType.VarChar, strManagementNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 호출
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATInProcessAccidentD", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 내부공정사고 상세정보 저장메소드
        /// </summary>
        /// <param name="dtDetail">상세정보 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnention 변수</param>
        /// <param name="trans">Transaction 변수</param>
        /// <param name="strManagementNo">관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATInProcessAccidentD(DataTable dtDetail, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strManagementNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;
                DataTable dtParam = new DataTable();

                // 파라미터 변수 설정
                for (int i = 0; i < dtDetail.Rows.Count; i++)
                {
                    dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManagementNo", ParameterDirection.Input, SqlDbType.VarChar, strManagementNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["CustomerCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtDetail.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_dblLotQty", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["LotQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblLossQty", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["LossQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblYield", ParameterDirection.Input, SqlDbType.Decimal, dtDetail.Rows[i]["Yield"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtDetail.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strManagementNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATInProcessAccidentD", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 내부공정사고 등록 상세정보 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManagementNo">관리번호</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">Transaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATInProcessAccidentD(string strPlantCode, string strManagementNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManagementNo", ParameterDirection.Input, SqlDbType.VarChar, strManagementNo, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strManagementNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATInProcessAccidentD", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }
}
