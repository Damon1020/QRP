﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질보증관리                                          */
/* 프로그램ID   : clsQATREL.cs                                          */
/* 프로그램명   : 신뢰성검사관리                                        */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-09-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-09-23 : 신뢰성검사 클래스 추가 (이종호)          */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPQAT.BL.QATREL
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("BasicAnalysis")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class BasicAnalysis : ServicedComponent
    {
        /// <summary>
        /// 기본분석관리 데이터 컬럼셋
        /// </summary>
        /// <returns>기본분석관리에필요한 컬럼정보</returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtBasicAnalysis = new DataTable();
            try
            {
                dtBasicAnalysis.Columns.Add("PlantCode", typeof(string));
                dtBasicAnalysis.Columns.Add("BasicNo", typeof(string));
                dtBasicAnalysis.Columns.Add("Package", typeof(string));
                dtBasicAnalysis.Columns.Add("ReqUserID", typeof(string));
                dtBasicAnalysis.Columns.Add("ReqDate", typeof(string));
                dtBasicAnalysis.Columns.Add("AnalysisType", typeof(string));
                dtBasicAnalysis.Columns.Add("AnalysisObject", typeof(string));

                dtBasicAnalysis.Columns.Add("SEMFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("FTIRFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("MoireFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("CurveFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("TracerFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("TDRFlag1", typeof(string));
                dtBasicAnalysis.Columns.Add("EtcFlag1", typeof(string));
                dtBasicAnalysis.Columns.Add("SATFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("XrayFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("DECAPFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("CrossSectionFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("CurveTracerFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("TDRFlag2", typeof(string));
                dtBasicAnalysis.Columns.Add("EtcFlag2", typeof(string));

                dtBasicAnalysis.Columns.Add("LotNo", typeof(string));
                dtBasicAnalysis.Columns.Add("ReqQty", typeof(string));
                dtBasicAnalysis.Columns.Add("Comment", typeof(string));

                dtBasicAnalysis.Columns.Add("CustomerCode", typeof(string));
                dtBasicAnalysis.Columns.Add("CustomerName", typeof(string));

                dtBasicAnalysis.Columns.Add("ReqFileName", typeof(string));
                dtBasicAnalysis.Columns.Add("AnalysisReqFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("ReceipReturnFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("ReceipFinishFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("AnalysisResult", typeof(string));
                dtBasicAnalysis.Columns.Add("AnalyzeUserID", typeof(string));
                dtBasicAnalysis.Columns.Add("ResultFileName", typeof(string));

                dtBasicAnalysis.Columns.Add("EtcDesc", typeof(string));
                dtBasicAnalysis.Columns.Add("Exterior1", typeof(string));
                dtBasicAnalysis.Columns.Add("Exterior2", typeof(string));
                dtBasicAnalysis.Columns.Add("ReceipFinishDate", typeof(string));
                dtBasicAnalysis.Columns.Add("ReceipFinishTime", typeof(string));
                dtBasicAnalysis.Columns.Add("AnalysisCompleteFlag", typeof(string));
                dtBasicAnalysis.Columns.Add("AnalysisCompleteDate", typeof(string));
                dtBasicAnalysis.Columns.Add("AnalysisCompleteTime", typeof(string));
                dtBasicAnalysis.Columns.Add("IssueType", typeof(string));
                
                return dtBasicAnalysis;
            }
            catch (Exception ex)
            {
                return dtBasicAnalysis;
                throw (ex);
            }
            finally
            {
                dtBasicAnalysis.Dispose();
            }
        }

        /// <summary>
        /// 기본분석 관리 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strFromDate">시작일</param>
        /// <param name="strToDate">종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>기본분석관리 정보</returns>
        [AutoComplete]
        public DataTable mfReadBasicAnalysis(string strPlantCode, string strPackage, string strCustomerCode, string strReqUserID, string strFromDate, string strToDate, string strLang)
        {
            DataTable dtBasicAnalysis = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // 디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                //파라미터값저장
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqUserID", ParameterDirection.Input, SqlDbType.VarChar, strReqUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //프로시저 실행
                dtBasicAnalysis = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATBasicAnalysis", dtParam);

                //기본분석관리 정보
                return dtBasicAnalysis;
            }
            catch (Exception ex)
            {
                return dtBasicAnalysis;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtBasicAnalysis.Dispose();
            }
        }
        #region 기존 검색용 조회 메소드 주석처리
        ////public DataTable mfReadBasicAnalysis(string strPlantCode, string strProductCode, string strFromDate, string strToDate ,string strLang)
        ////{
        ////    DataTable dtBasicAnalysis = new DataTable();
        ////    SQLS sql = new SQLS();

        ////    try
        ////    {
        ////        // 디비연결
        ////        sql.mfConnect();

        ////        DataTable dtParam = sql.mfSetParamDataTable();

        ////        //파라미터값저장
        ////        sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
        ////        sql.mfAddParamDataRow(dtParam,"@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar,strProductCode,20);
        ////        sql.mfAddParamDataRow(dtParam,"@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar,strFromDate,10);
        ////        sql.mfAddParamDataRow(dtParam,"@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar,strToDate,10);
        ////        sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);

        ////        //프로시저 실행
        ////        dtBasicAnalysis = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATBasicAnalysis", dtParam);

        ////        //기본분석관리 정보
        ////        return dtBasicAnalysis;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        return dtBasicAnalysis;
        ////        throw (ex);
        ////    }
        ////    finally
        ////    {
        ////        //디비종료
        ////        sql.mfDisConnect();
        ////        sql.Dispose();
        ////        dtBasicAnalysis.Dispose();
        ////    }
        ////}
        #endregion

        /// <summary>
        /// 기본분석관리 (그리드 더블클릭시 ExpandGrouopBox 안의 내용)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strBasicNo">관리번호</param>
        /// <returns>기본분석관리 정보</returns>
        [AutoComplete]
        public DataTable mfReadBasicAnalysis_Detail(string strPlantCode,string strBasicNo, string strLang)
        {
            DataTable dtBasicAnalysis = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                //파라미터 값저장
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strBasicNo", ParameterDirection.Input, SqlDbType.VarChar,strBasicNo,20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //프로시저실행
                dtBasicAnalysis = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATBasicAnalysisDetail", dtParam);
                
                //기본분석관리 정보 리턴
                return dtBasicAnalysis;
            }
            catch (Exception ex)
            {
                return dtBasicAnalysis;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtBasicAnalysis.Dispose();
            }
        }

        /// <summary>
        /// 기본분석관리 저장
        /// </summary>
        /// <param name="dtBasicAnalysis">저장할 정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과값</returns>
        [AutoComplete(false)]
        public string mfSaveBasicAnalysis(DataTable dtBasicAnalysis,string strUserIP,string strUserID)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction 시작
                trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strBasicNo", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["BasicNo"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["Package"].ToString(), 40);
                sql.mfAddParamDataRow(dtParam, "@i_strReqUserID", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["ReqUserID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["ReqDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAnalysisType", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["AnalysisType"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strAnalysisObject", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["AnalysisObject"].ToString(), 100);

                sql.mfAddParamDataRow(dtParam, "@i_strSEMFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["SEMFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strFTIRFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["FTIRFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strMoireFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["MoireFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strCurveFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["CurveFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strTracerFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["TracerFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strTDRFlag1", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["TDRFlag1"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strEtcFlag1", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["EtcFlag1"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strSATFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["SATFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strXrayFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["XrayFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strDECAPFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["DECAPFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strCrossSectionFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["CrossSectionFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strCurveTracerFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["CurveTracerFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strTDRFlag2", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["TDRFlag2"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strEtcFlag2", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["EtcFlag2"].ToString(), 1);

                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["LotNo"].ToString(), 50);
                sql.mfAddParamDataRow(dtParam, "@i_strReqQty", ParameterDirection.Input, SqlDbType.Decimal, dtBasicAnalysis.Rows[0]["ReqQty"].ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strComment", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["Comment"].ToString(), 200);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["CustomerCode"].ToString(), 200);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerName", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["CustomerName"].ToString(), 200);
                
                sql.mfAddParamDataRow(dtParam, "@i_strReqFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["ReqFileName"].ToString(), 1000);
                sql.mfAddParamDataRow(dtParam, "@i_strAnalysisReqFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["AnalysisReqFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strReceipReturnFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["ReceipReturnFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strReceipFinishFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["ReceipFinishFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strAnalysisResult", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["AnalysisResult"].ToString(), 200);
                sql.mfAddParamDataRow(dtParam, "@i_strAnalyzeUserID", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["AnalyzeUserID"].ToString(), 20);
                sql.mfAddParamDataRow(dtParam, "@i_strResultFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["ResultFileName"].ToString(), 1000);

                sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtBasicAnalysis.Rows[0]["EtcDesc"].ToString(), 100);
                sql.mfAddParamDataRow(dtParam, "@i_strExterior1", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["Exterior1"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strExterior2", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["Exterior2"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strReceipFinishDate", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["ReceipFinishDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReceipFinishTime", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["ReceipFinishTime"].ToString(), 8);
                sql.mfAddParamDataRow(dtParam, "@i_strAnalysisCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["AnalysisCompleteFlag"].ToString(), 1);
                sql.mfAddParamDataRow(dtParam, "@i_strAnalysisCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["AnalysisCompleteDate"].ToString(), 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAnalysisCompleteTime", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["AnalysisCompleteTime"].ToString(), 8);
                sql.mfAddParamDataRow(dtParam, "@i_strIssueType", ParameterDirection.Input, SqlDbType.VarChar, dtBasicAnalysis.Rows[0]["IssueType"].ToString(), 1);

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@o_strBasicNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATBasicAnalysis", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // -- 처리결과에 따라 롤백, 커밋 처리를 한다 -- //
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                //처리 결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 기본분석관리 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strBasicNo">관리번호</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfDeleteBasicAnalysis(string strPlantCode, string strBasicNo)
        {
            string strErrRtn = "";
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //Transaction시작
                trans = sql.SqlCon.BeginTransaction();

                //파라미터 값 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strBasicNo", ParameterDirection.Input, SqlDbType.VarChar,strBasicNo,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //프로시저실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATBasicAnalysis", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //-- 처리결과에 따라 롤백하거나 커밋을 한다 --//
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ReliabilityInspect")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ReliabilityInspect : ServicedComponent
    {
        /// <summary>
        /// 데이터테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReliabilityNo", typeof(string));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("AppraisalObject", typeof(string));
                dtRtn.Columns.Add("ChangeBefore", typeof(string));
                dtRtn.Columns.Add("ChangeAfter", typeof(string));
                dtRtn.Columns.Add("ProductCode", typeof(string));
                dtRtn.Columns.Add("Chip_Size", typeof(string));
                dtRtn.Columns.Add("Gubun", typeof(string));
                dtRtn.Columns.Add("Adhesive_Model", typeof(string));
                dtRtn.Columns.Add("Adhesive_Vender", typeof(string));
                dtRtn.Columns.Add("LForPCBModel", typeof(string));
                dtRtn.Columns.Add("LForPCBVender", typeof(string));
                dtRtn.Columns.Add("WireModel", typeof(string));
                dtRtn.Columns.Add("WireVender", typeof(string));
                dtRtn.Columns.Add("EMCModel", typeof(string));
                dtRtn.Columns.Add("EMCVender", typeof(string));
                dtRtn.Columns.Add("SolderBallModel", typeof(string));
                dtRtn.Columns.Add("SolderBallVender", typeof(string));
                dtRtn.Columns.Add("ReqQty", typeof(decimal));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("TCOptionFlag", typeof(string));
                dtRtn.Columns.Add("TCOptionCondition", typeof(string));
                dtRtn.Columns.Add("TCOptionTime", typeof(string));
                dtRtn.Columns.Add("TCOptionDate", typeof(string));
                dtRtn.Columns.Add("BakeFlag", typeof(string));
                dtRtn.Columns.Add("BakeCondition", typeof(string));
                dtRtn.Columns.Add("BakeTime", typeof(string));
                dtRtn.Columns.Add("BakeDate", typeof(string));
                dtRtn.Columns.Add("HumidityFlag1", typeof(string));
                dtRtn.Columns.Add("HumidityCondition1", typeof(string));
                dtRtn.Columns.Add("HumidityTime1", typeof(string));
                dtRtn.Columns.Add("HumidityDate1", typeof(string));
                dtRtn.Columns.Add("ReflowFlag", typeof(string));
                dtRtn.Columns.Add("ReflowCondition", typeof(string));
                dtRtn.Columns.Add("ReflowTime", typeof(string));
                dtRtn.Columns.Add("ReflowDate", typeof(string));
                dtRtn.Columns.Add("TCFlag", typeof(string));
                dtRtn.Columns.Add("TCCondition", typeof(string));
                dtRtn.Columns.Add("TCTime", typeof(string));
                dtRtn.Columns.Add("TCDate", typeof(string));
                dtRtn.Columns.Add("PCTFlag", typeof(string));
                dtRtn.Columns.Add("PCTCondition", typeof(string));
                dtRtn.Columns.Add("PCTTime", typeof(string));
                dtRtn.Columns.Add("PCTDate", typeof(string));
                dtRtn.Columns.Add("HASTFlag", typeof(string));
                dtRtn.Columns.Add("HASTCondition", typeof(string));
                dtRtn.Columns.Add("HASTTime", typeof(string));
                dtRtn.Columns.Add("HASTDate", typeof(string));
                dtRtn.Columns.Add("HumidityFlag2", typeof(string));
                dtRtn.Columns.Add("HumidityCondition2", typeof(string));
                dtRtn.Columns.Add("HumidityTime2", typeof(string));
                dtRtn.Columns.Add("HumidityDate2", typeof(string));
                dtRtn.Columns.Add("HTSFlag", typeof(string));
                dtRtn.Columns.Add("HTSCondition", typeof(string));
                dtRtn.Columns.Add("HTSTime", typeof(string));
                dtRtn.Columns.Add("HTSDate", typeof(string));
                dtRtn.Columns.Add("InspectUserID", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("InspectFileName", typeof(string));
                dtRtn.Columns.Add("PassFailFlag", typeof(string));
                dtRtn.Columns.Add("CompleteFlag", typeof(string));
                dtRtn.Columns.Add("AcceptUserID", typeof(string));
                dtRtn.Columns.Add("AcceptDate", typeof(string));
                dtRtn.Columns.Add("AcceptFlag", typeof(string));
                dtRtn.Columns.Add("Customer", typeof(string));      // PSTS 2012-10-14 추가
                dtRtn.Columns.Add("Package", typeof(string));       // PSTS 2012-11-07 추가
                dtRtn.Columns.Add("ProductLN", typeof(string));     // PSTS 2012-11-07 추가
                dtRtn.Columns.Add("ProductPN", typeof(string));     // PSTS 2012-11-07 추가
                dtRtn.Columns.Add("ProcessType", typeof(string));   // PSTS 2012-11-07 추가
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 신뢰성검사 등록/조회 검색 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strWriteDateFrom">등록일자From</param>
        /// <param name="strWriteDateTo">등록일자To</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATReliabilityInspect(string strPlantCode, string strProductCode, string strWriteDateFrom, string strWriteDateTo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strWriteDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteDateTo", ParameterDirection.Input, SqlDbType.VarChar, strWriteDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATReliabilityInspect", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 신뢰성검사 상세조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReliabilityNo">관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATReliabilityInspectDetail(string strPlantCode, string strReliabilityNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReliabilityNo", ParameterDirection.Input, SqlDbType.VarChar, strReliabilityNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATReliabilityInspectDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 신뢰성검사 등록/조회 저장Method
        /// </summary>
        /// <param name="dtSave">저장정보가 담긴 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATReliabilityInspect(DataTable dtSave, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                string strErrRtn = "";

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    //트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReliabilityNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReliabilityNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAppraisalObject", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["AppraisalObject"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strChangeBefore", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ChangeBefore"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strChangeAfter", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ChangeAfter"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strChip_Size", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["Chip_Size"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdhesive_Model", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["Adhesive_Model"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdhesive_Vender", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["Adhesive_Vender"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLForPCBModel", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["LForPCBModel"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLForPCBVender", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["LForPCBVender"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strWireModel", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["WireModel"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strWireVender", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["WireVender"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEMCModel", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EMCModel"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEMCVender", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EMCVender"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strSolderBallModel", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["SolderBallModel"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strSolderBallVender", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["SolderBallVender"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_dblReqQty", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["ReqQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strTCOptionFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["TCOptionFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strTCOptionCondition", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["TCOptionCondition"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strTCOptionTime", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["TCOptionTime"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strTCOptionDate", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["TCOptionDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strBakeFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["BakeFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strBakeCondition", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["BakeCondition"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strBakeTime", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["BakeTime"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strBakeDate", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["BakeDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHumidityFlag1", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["HumidityFlag1"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strHumidityCondition1", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["HumidityCondition1"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strHumidityTime1", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HumidityTime1"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHumidityDate1", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HumidityDate1"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReflowFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReflowFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReflowCondition", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReflowCondition"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReflowTime", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ReflowTime"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReflowDate", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ReflowDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strTCFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["TCFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strTCCondition", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["TCCondition"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strTCTime", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["TCTime"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strTCDate", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["TCDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPCTFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PCTFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strPCTCondition", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["PCTCondition"].ToString(), 100);    // PCT 직접입력 변경 VARCHAR(1) -> NVARCHAR(100)
                    sql.mfAddParamDataRow(dtParam, "@i_strPCTTime", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["PCTTime"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPCTDate", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["PCTDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHASTFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["HASTFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strHASTCondition", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HASTCondition"].ToString(), 100);  // HAST 직접입력 변경 VARCHAR(1) -> NVARCHAR(100)
                    sql.mfAddParamDataRow(dtParam, "@i_strHASTTime", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HASTTime"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHASTDate", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HASTDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHumidityFlag2", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["HumidityFlag2"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strHumidityCondition2", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["HumidityCondition2"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strHumidityTime2", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HumidityTime2"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHumidityDate2", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HumidityDate2"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHTSFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["HTSFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strHTSCondition", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["HTSCondition"].ToString(), 1);    
                    sql.mfAddParamDataRow(dtParam, "@i_strHTSTime", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HTSTime"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strHTSDate", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["HTSDate"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["InspectFileName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strPassFailFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PassFailFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strGubun", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["Gubun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcceptUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AcceptUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcceptDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AcceptDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAcceptFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AcceptFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomer", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["Customer"].ToString(), 50);      // 2012-10-14 고객사 직접입력
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["Package"].ToString(), 50);          // PSTS 2012-11-07 Package 직접입력
                    sql.mfAddParamDataRow(dtParam, "@i_strProductLN", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ProductLN"].ToString(), 50);      // PSTS 2012-11-07 제품L/N 직접입력
                    sql.mfAddParamDataRow(dtParam, "@i_strProductPN", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ProductPN"].ToString(), 50);      // PSTS 2012-11-07 제품P/N 직접입력
                    sql.mfAddParamDataRow(dtParam, "@i_strProcessType", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ProcessType"].ToString(), 10);   // PSTS 2012-11-07 FS/HF선택
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    
                    sql.mfAddParamDataRow(dtParam, "@o_strReliabilityNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATReliabilityInspect", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();
                    }
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 신뢰성검사 등록/조회 삭제 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReliabilityNo">관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATReliabilityInspect(string strPlantCode, string strReliabilityNo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = "";
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strReliabilityNo", ParameterDirection.Input, SqlDbType.VarChar, strReliabilityNo,20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATReliabilityInspect", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                else
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }
}
