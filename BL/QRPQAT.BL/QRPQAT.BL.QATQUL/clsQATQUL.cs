﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질보증관리                                          */
/* 모듈(분류)명 : Qual관리                                              */
/* 프로그램ID   : clsQATQUL.cs                                          */
/* 프로그램명   : 신제품 Qual 관리                                      */
/* 작성자       : 정결                                                  */
/* 작성일자     : 2011-09-15                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                                                                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
              Authentication = AuthenticationOption.None,
             ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPQAT.BL.QATQUL
{

    /// <summary>
    /// 신제품 Qual관리 Popup
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualProduct")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class QATNewQualProduct : ServicedComponent
    {
        /// <summary>
        /// 신제품Qual 제품정보 
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtNewQualProduct = new DataTable();
            try
            {
                dtNewQualProduct.Columns.Add("PlantCode", typeof(String));
                dtNewQualProduct.Columns.Add("QualProductCode", typeof(String));
                dtNewQualProduct.Columns.Add("QualProductName", typeof(String));
                dtNewQualProduct.Columns.Add("QualProductNameCh", typeof(String));
                dtNewQualProduct.Columns.Add("QualProductNameEn", typeof(String));
                
                return dtNewQualProduct;
            }
            catch (System.Exception ex)
            {
                return dtNewQualProduct;
                throw (ex);
            }
            finally
            {
                dtNewQualProduct.Dispose();
            }
        }
        /// <summary>
        /// 신제품Qual ComboBox 설정용 Method
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualProductPopup(String strPlantCode, String strQualProductName, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualProductName", ParameterDirection.Input, SqlDbType.VarChar, strQualProductName, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATProductPopup", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 신제품 Qual제품정보 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATNewQualProduct(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans)
        {

            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++ )
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualProductCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QualProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualProductName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["QualProductName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualProductNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["QualProductNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualProductNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["QualProductNameEn"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATNewQualProduct", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신제품 Qual제품정보 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualProduct(DataTable dt)
        {

            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try 
            {
                sql.mfConnect();
                SqlTransaction trans;

                for(int i = 0; i < dt.Rows.Count; i++)
                {
                    trans = sql.SqlCon.BeginTransaction();
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualProductCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QualProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualProductName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["QualProductName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualProductNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["QualProductNameCH"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualProductNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["QualProductNameEn"].ToString(), 50);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualProduct", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                    else
                        trans.Commit();
                }
                return strErrRtn;
            }
            catch(Exception ex) 
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally 
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    /// <summary>
    /// QATNewQualH
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class QATNewQualH : ServicedComponent
    {

        /// <summary>
        /// 신제품 Qual 관리 헤더
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtNewQualH = new DataTable();
            try
            {
                dtNewQualH.Columns.Add("PlantCode", typeof(string));
                dtNewQualH.Columns.Add("QualNo", typeof(string));
                dtNewQualH.Columns.Add("CustomerCode", typeof(string));
                dtNewQualH.Columns.Add("QualType", typeof(string));
                dtNewQualH.Columns.Add("WriteUserID", typeof(string));
                dtNewQualH.Columns.Add("WriteDate", typeof(string));
                dtNewQualH.Columns.Add("FourMManFlag", typeof(string));
                dtNewQualH.Columns.Add("FourMEquipFlag", typeof(string));
                dtNewQualH.Columns.Add("FourMMethodFlag", typeof(string));
                dtNewQualH.Columns.Add("FourMEnviroFlag", typeof(string));
                dtNewQualH.Columns.Add("FourMMaterialFlag", typeof(string));
                dtNewQualH.Columns.Add("FourMOtherFlag", typeof(string));
                dtNewQualH.Columns.Add("Purpose", typeof(string));
                dtNewQualH.Columns.Add("ChangeBefore", typeof(string));
                dtNewQualH.Columns.Add("ChangeAfter", typeof(string));
                dtNewQualH.Columns.Add("CompleteFlag", typeof(string));
                dtNewQualH.Columns.Add("FinalInspectResultFlag", typeof(string));
                dtNewQualH.Columns.Add("FinalCompleteDate", typeof(string));

                return dtNewQualH;
            }
            catch (System.Exception ex)
            {
                return dtNewQualH;
                throw (ex);
            }
            finally
            {
                dtNewQualH.Dispose();
            }
        }

        /// <summary>
        /// 신제품 Qual 관리 검색조건에 의한 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strPackage">PACKAGE</param>
        /// <param name="strWriteDateFrom">등록일자 : FROM</param>
        /// <param name="strWriteDateTo">등록일자 : TO</param>
        /// <param name="strQualType">구분</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualH(String strPlantCode, String strCustomerCode, String strPackage, String strWriteDateFrom, String strWriteDateTo, string strQualType, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtNewQualH = new DataTable();
            try
            {
                //db 연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 변수 설정
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteDateFrom", ParameterDirection.Input, SqlDbType.VarChar, strWriteDateFrom, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strWriteDateTo", ParameterDirection.Input, SqlDbType.VarChar, strWriteDateTo, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualType", ParameterDirection.Input, SqlDbType.VarChar, strQualType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtNewQualH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualH", dtParam);
            }
            catch(Exception ex)
            {
                return dtNewQualH;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtNewQualH.Dispose();
            }
        }

        /// <summary>
        /// 신제품 Qual 관리 헤더 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualH_Detail(String strPlantCode, String strQualNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtNewQualH = new DataTable();
            try
            {
                 sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 변수 설정
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtNewQualH = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualH_Detail", dtParam);
            }
            catch(Exception ex)
            {
                return dtNewQualH;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtNewQualH.Dispose();
            }
        }


        [AutoComplete(false)]
        public string mfSaveQATNewQualH(DataTable dtNewQualH, string strUserIP, string strUserID, DataTable dtPackageList, DataTable dtEvidenceList, DataTable dtMaterialList, DataTable dtEmail)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtNewQualH.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["CustomerCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualType", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["QualType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFourMManFlag", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["FourMManFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFourMEquipFlag", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["FourMEquipFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFourMMethodFlag", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["FourMMethodFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFourMEnviroFlag", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["FourMEnviroFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFourMMaterialFlag", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["FourMMaterialFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFourMOtherFlag", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["FourMOtherFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strPurPose", ParameterDirection.Input, SqlDbType.NVarChar, dtNewQualH.Rows[i]["Purpose"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strChangeBefore", ParameterDirection.Input, SqlDbType.NVarChar, dtNewQualH.Rows[i]["ChangeBefore"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strChangeAfter", ParameterDirection.Input, SqlDbType.NVarChar, dtNewQualH.Rows[i]["ChangeAfter"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strFinalInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["FinalInspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strFinalCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtNewQualH.Rows[i]["FinalCompleteDate"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strQualNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATNewQualH", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // 관리번호 저장
                        string strPlantCode = dtNewQualH.Rows[i]["PlantCode"].ToString();
                        string strQualNo = ErrRtn.mfGetReturnValue(0);

                        // 하위 정보 삭제 메소드 호출
                        strErrRtn = mfDeleteNewQualH_DetailInfo(strPlantCode, strQualNo, sql.SqlCon, trans);

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            trans.Rollback();
                            break;
                        }

                        if (dtPackageList.Rows.Count > 0)
                        {
                            QATNewQualD clsDetail = new QATNewQualD();
                            strErrRtn = clsDetail.mfSaveQATNewQualD(dtPackageList, strUserID, strUserIP, sql.SqlCon, trans, strQualNo);

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (dtEvidenceList.Rows.Count > 0)
                        {
                            QATNewQualASEFile clsEvidence = new QATNewQualASEFile();
                            strErrRtn = clsEvidence.mfSaveQATNewQualASEFile(dtEvidenceList, strUserIP, strUserID, sql.SqlCon, trans, strQualNo);

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (dtMaterialList.Rows.Count > 0)
                        {
                            QATNewQualMaterial clsMaterial = new QATNewQualMaterial();
                            strErrRtn = clsMaterial.mfSaveQATNewQualMaterial(dtMaterialList, strUserID, strUserIP, sql.SqlCon, trans, strQualNo);

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                break;
                            }
                        }

                        if (dtEmail.Rows.Count > 0)
                        {
                            QATNewQualEMailUser clsEmail = new QATNewQualEMailUser();
                            strErrRtn = clsEmail.mfSaveQATNewQualEmailUser(dtEmail, strUserID, strUserIP, sql.SqlCon, trans, strQualNo);

                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (!ErrRtn.ErrNum.Equals(0))
                            {
                                trans.Rollback();
                                break;
                            }
                        }
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;

            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 헤더 하위 테이블 정보 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual관리번호</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        private string mfDeleteNewQualH_DetailInfo(string strPlantCode, string strQualNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();

                // 파라미터 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@o_strQualNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 호출
                string strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATNewQualH_DetailInfo", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신제품 Qual 전체 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDeleteNewQualH(string strPlantCode, string strQualNo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 파라미터 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualH", dtParam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }



    /// <summary>
    /// 신제품 Qual 관리 상세
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class QATNewQualD : ServicedComponent
    {

        /// <summary>
        /// 신제품 Qual 관리 상세 
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtNewQualD = new DataTable();
            try 
            {
                dtNewQualD.Columns.Add("PlantCode", typeof(string));
                dtNewQualD.Columns.Add("QualNo", typeof(string));
                dtNewQualD.Columns.Add("Seq", typeof(Int32));
                dtNewQualD.Columns.Add("Package", typeof(string));
                dtNewQualD.Columns.Add("EtcDesc", typeof(string));

                return dtNewQualD;
            }
            catch(Exception ex) 
            {
                return dtNewQualD;
                throw (ex);
            }
            finally 
            {
                dtNewQualD.Dispose();
            }
        }

        /// <summary>
        /// 신제품 Qual 관리 Package 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualD(String strPlantCode, String strQualNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtNewQualD = new DataTable();
            try
            {
                //DB 연결
                sql.mfConnect();

                // Parameter 변수 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtNewQualD = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualD", dtParam);
            }
            catch(Exception ex)
            {
                return dtNewQualD;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtNewQualD.Dispose();
            }
        }

        /// <summary>
        /// 신제품 Qual Package 정보 저장 메소드
        /// </summary>
        /// <param name="dtPackage">Package정보가 들어있는 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SqlTransaction 변수</param>
        /// <param name="strQualNo">Qual 관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATNewQualD(DataTable dtPackage, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = string.Empty;

                for (int i = 0; i < dtPackage.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtPackage.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtPackage.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtPackage.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtPackage.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strQualNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATNewQualD", dtParam);

                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            { 
            }
        }

        /// <summary>
        /// 신제품 Qual Package 정보 삭제 메소드
        /// </summary>
        /// <param name="dtDelete">삭제정보가 들어있는 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualD(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try 
            {
                sql.mfConnect();
                String strErrRtn = string.Empty;

                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDelete.Rows.Count; i++ )
                {
                    // 파라미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDelete.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualD", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }


    /// <summary>
    /// Evidence List Table
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualASEFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class QATNewQualASEFile : ServicedComponent
    {
        /// <summary>
        /// ASEFile 테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtASEFile = new DataTable();
            try
            {
                dtASEFile.Columns.Add("PlantCode", typeof(string));
                dtASEFile.Columns.Add("QualNo", typeof(string));
                dtASEFile.Columns.Add("Seq", typeof(Int32));
                dtASEFile.Columns.Add("EvidenceType", typeof(string));
                dtASEFile.Columns.Add("StartDate", typeof(string));
                dtASEFile.Columns.Add("CompleteDate", typeof(string));
                dtASEFile.Columns.Add("InspectFlag", typeof(string));
                dtASEFile.Columns.Add("WriteUserID", typeof(string));
                dtASEFile.Columns.Add("FilePath", typeof(string));
                dtASEFile.Columns.Add("EtcDesc", typeof(string));

                return dtASEFile;

            }
            catch (Exception ex)
            {
                return dtASEFile;
                throw (ex);
            }
            finally
            {
                dtASEFile.Dispose();
            }
        }

        /// <summary>
        /// Evidence List 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualASEFile(String strPlantCode, String strQualNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtASEFile = new DataTable();
            try
            {
                //DB 연결
                sql.mfConnect();

                // Set Parameter
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtASEFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualASEFile", dtParam);
            }
            catch(Exception ex)
            {
                return dtASEFile;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtASEFile.Dispose();
            }
        }

        /// <summary>
        /// Evidence List 저장 메소드
        /// </summary>
        /// <param name="dtSave">저장할 정보가 들어있는 데이터 테이블</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SqlTransaction 변수</param>
        /// <param name="strQualNo">Qual 관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATNewQualASEFile(DataTable dtSave, string strUserIP, string strUserID, SqlConnection sqlCon, SqlTransaction trans, string strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                String strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEvidenceType", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["EvidenceType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStartDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["StartDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["CompleteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["FilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strQualNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATNewQualASEFile", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// 신제품 Qual관리 Evidence List 삭제
        /// </summary>
        /// <param name="dtDelete">삭제정보 저장된데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualASEFile(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                String strErrRtn = string.Empty;

                // Transaction시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDelete.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualASEFile", dtParam);
                    
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualMaterial")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class QATNewQualMaterial : ServicedComponent
    {
        /// <summary>
        /// NewQualMaterial 데이터테이블
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDatainfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt.Columns.Add("PlantCode", typeof(String));
                dt.Columns.Add("QualNo", typeof(String));
                dt.Columns.Add("Seq", typeof(Int32));
                dt.Columns.Add("MaterialCode", typeof(String));
                dt.Columns.Add("EtcDesc", typeof(String));

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                dt.Dispose();
            }
        }

        /// <summary>
        /// Material 검색
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">Qual 관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        public DataTable mfReadQATNewQualMaterial(String strPlantCode, String strQualNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtMaterial = new DataTable();
            try
            {
                sql.mfConnect();

                // Set Parameter
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtMaterial = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualMaterial", dtParam);
            }
            catch (Exception ex)
            {
                return dtMaterial;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtMaterial.Dispose();
            }
        }

        /// <summary>
        /// 신제품 Qual Material 저장 메소드
        /// </summary>
        /// <param name="dtMaterial">Mateiral 정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="sqlCon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <param name="strQualNo">Qual 관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATNewQualMaterial(DataTable dtMaterial, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                SQLS sql = new SQLS();

                for (int i = 0; i < dtMaterial.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMaterial.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtMaterial.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtMaterial.Rows[i]["MaterialCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtMaterial.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strQualNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATNewQualMaterial", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신제품 Qual Material정보 삭제 메소드
        /// </summary>
        /// <param name="dtDelete">삭제정보가 담긴 데이터 테이블</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualMaterial(DataTable dtDelete)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                String strErrRtn = string.Empty;

                // TranSaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDelete.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dtDelete.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtDelete.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualMaterial", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


    }

    /// <summary>
    /// RelFile
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualRelFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class QATNewQualRelFile : ServicedComponent
    {

        /// <summary>
        /// RelFile 테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtRelFile = new DataTable();
            try
            {
                dtRelFile.Columns.Add("PlantCode", typeof(String));
                dtRelFile.Columns.Add("QualNo", typeof(String));
                dtRelFile.Columns.Add("Seq", typeof(int));
                dtRelFile.Columns.Add("RelFileName", typeof(String));
                dtRelFile.Columns.Add("EtcDesc", typeof(String));

                return dtRelFile;

            }
            catch (Exception ex)
            {
                return dtRelFile;
                throw (ex);
            }
            finally
            {
                dtRelFile.Dispose();
            }
        }

        /// <summary>
        /// RelFile 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQualNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualRelFile(String strPlantCode, String strQualNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRelFile = new DataTable();


            try
            {
                //DB 연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //Save Parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRelFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualRelFile", dtParam);

                return dtRelFile;

            }
            catch (Exception ex)
            {
                return dtRelFile;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRelFile.Dispose();
            }
        }

        /// <summary>
        /// 파일 네이밍을 위한 Seq최대값 구하기
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        public DataTable mfReadQATNewQualRelFileSeq(String strPlantCode, String strQualNo)
        {
            SQLS sql = new SQLS();
            DataTable dtRel = new DataTable();

            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                dtRel = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualRelFileSeq", dtParam);

                return dtRel;

            }
            catch (Exception ex)
            {
                return dtRel;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRel.Dispose();
            }
        }

        /// <summary>
        /// RelFile 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATNewQualRelFile(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, string strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strRelFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["RelFileName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATNewQualRelFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch(Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신제품 Qual관리 RelFile 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualRelFile(DataTable dt, SQLS sql, SqlTransaction trans, String strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualRelFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }



/// <summary>
/// RelFile 삭제
/// </summary>
/// <param name="dt"></param>
/// <param name="sql"></param>
/// <param name="trans"></param>
/// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualRelFileAll(DataTable dt, SQLS sql , SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualRelFileAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally { }
        }



    }

    /// <summary>
    /// ASPFile
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualASPFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class QATNewQualASPFile : ServicedComponent
    {

        /// <summary>
        /// ASPFile 테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtASPFile = new DataTable();
            try
            {
                dtASPFile.Columns.Add("PlantCode", typeof(String));
                dtASPFile.Columns.Add("QualNo", typeof(String));
                dtASPFile.Columns.Add("Seq", typeof(int));
                dtASPFile.Columns.Add("ASPFileName", typeof(String));
                dtASPFile.Columns.Add("EtcDesc", typeof(String));

                return dtASPFile;

            }
            catch (Exception ex)
            {
                return dtASPFile;
                throw (ex);
            }
            finally
            {
                dtASPFile.Dispose();
            }
        }
        /// <summary>
        /// ASPFile 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQualNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualASPFile(String strPlantCode, String strQualNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtASPFile = new DataTable();


            try
            {
                //DB 연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //Save Parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtASPFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualASPFile", dtParam);

                return dtASPFile;

            }
            catch (Exception ex)
            {
                return dtASPFile;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtASPFile.Dispose();
            }
        }

        /// <summary>
        /// 파일 네이밍을 위한 Seq최대값 구하기
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        public DataTable mfReadQATNewQualASPFileSeq(String strPlantCode, String strQualNo)
        {
            SQLS sql = new SQLS();
            DataTable dtASP = new DataTable();

            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                dtASP = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualASPFileSeq", dtParam);

                return dtASP;

            }
            catch (Exception ex)
            {
                return dtASP;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtASP.Dispose();
            }
        }

        /// <summary>
        /// ASPFile 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATNewQualASPFile(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, string strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strASPFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["ASPFileName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATNewQualASPFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신제품 Qual관리 ASP파일 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualASPFile(DataTable dt, SQLS sql, SqlTransaction trans, String strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualASPFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// ASPFile 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualASPFileAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try 
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualASPFileAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex) 
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally 
            { 
            }
        }

    }
    /// <summary>
    /// VolumeFile
    /// </summary>
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualVolFile")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class QATNewQualVolFile : ServicedComponent
    {

        /// <summary>
        /// VolumeFile 테이블
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtVolFile = new DataTable();
            try
            {
                dtVolFile.Columns.Add("PlantCode", typeof(String));
                dtVolFile.Columns.Add("QualNo", typeof(String));
                dtVolFile.Columns.Add("Seq", typeof(int));
                dtVolFile.Columns.Add("VolFileName", typeof(String));
                dtVolFile.Columns.Add("EtcDesc", typeof(String));

                return dtVolFile;

            }
            catch (Exception ex)
            {
                return dtVolFile;
                throw (ex);
            }
            finally
            {
                dtVolFile.Dispose();
            }
        }

        /// <summary>
        /// VolumeFile 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQualNo"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualVolFile(String strPlantCode, String strQualNo, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtVolFile = new DataTable();


            try
            {
                //DB 연결
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                //Save Parameter
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtVolFile = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualVolumeFile", dtParam);

                return dtVolFile;

            }
            catch (Exception ex)
            {
                return dtVolFile;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVolFile.Dispose();
            }
        }


        /// <summary>
        /// 파일 네이밍을 위한 Seq최대값 구하기
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        public DataTable mfReadQATNewQualVolFileSeq(String strPlantCode, String strQualNo)
        {
            SQLS sql = new SQLS();
            DataTable dtVol = new DataTable();

            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                dtVol = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualVolumeFileSeq", dtParam);

                return dtVol;

            }
            catch (Exception ex)
            {
                return dtVol;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtVol.Dispose();
            }
        }


        /// <summary>
        /// VolumeFile 저장
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATNewQualVolFile(DataTable dt, string strUserIP, string strUserID, SQLS sql, SqlTransaction trans, string strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strVolFileName", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["VolFileName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dt.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATNewQualVolumeFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신제품 Qual관리 VolumeFile 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <param name="strQualNo"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualVolFile(DataTable dt, SQLS sql, SqlTransaction trans, String strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dt.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualVolumeFile", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }


        /// <summary>
        /// VolFiel 삭제
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfDelQATNewQualVolFileAll(DataTable dt, SQLS sql, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";

            try
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dt.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATNewQualVolumeFileAll", dtParam);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("QATNewQualEMailUser")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // 신제품 Qual E-mail 송부 정보
    public class QATNewQualEMailUser : ServicedComponent
    {
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("QualNo", typeof(string));
                dtRtn.Columns.Add("UserID", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신제품Qual E-Mail송부 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strQualNo">관리번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATNewQualEMailUser(string strPlantCode, string strQualNo, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATNewQualEMailUser", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 신제품Qual E-Mail정보 저장
        /// </summary>
        /// <param name="dtEmail">E-Mail정보 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnenTion 변수</param>
        /// <param name="trans">SqlTransaction 변수</param>
        /// <param name="strQualNo">관리번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATNewQualEmailUser(DataTable dtEmail, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strQualNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtEmail.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtEmail.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, strQualNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, dtEmail.Rows[i]["UserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtEmail.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strLogUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strLogUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strQualNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATNewQualEMailUser", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                sql.Dispose();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MachineModelAdmit")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // Machine Model 승인정보
    public class MachineModelAdmit : ServicedComponent
    {
        /// <summary>
        /// [Machine Model 승인정보] DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("CustomerCode", typeof(string));
                dtRtn.Columns.Add("Package", typeof(string));
                dtRtn.Columns.Add("Stack", typeof(string));
                dtRtn.Columns.Add("MACHINEGROUPACTIONTYPE", typeof(string));
                dtRtn.Columns.Add("EquipGroupCode", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(int));
                dtRtn.Columns.Add("AdmitFlag", typeof(string));
                dtRtn.Columns.Add("QualNo", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("Reason", typeof(string));//SC
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [Machine Model 승인정보] List 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strEquipCode"> 설비코드 </param>        
        /// <param name="strProductCode"> 제품코드 </param>                   
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMahcineAdmitList(String strPlantCode, String strCustomerCode, String strPackage, String strEquipGroupCode, String strEquipTypeCode, String strStack, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipTypeCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipTypeCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStack", ParameterDirection.Input, SqlDbType.VarChar, strStack, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP실행
                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMachineModelAdmitList", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [Machine Model 승인정보] List 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strEquipCode"> 설비코드 </param>        
        /// <param name="strProductCode"> 제품코드 </param>                   
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATMahcineAdmitGroupList(String strPlantCode, String strCustomerCode, String strPackage,String strMachineGroupActionType)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strMachineGroupActionType", ParameterDirection.Input, SqlDbType.VarChar, strMachineGroupActionType, 10);

                // SP실행
                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMachineModelAdmitGroupList", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// OI Track In 的时候确认设备是否允许生产该产品
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadLOT_TKINCHK_REQ_TKINMACHINECHECK(string strPlantCode, string strProductCode, string strCustomer, string strPackage , DataTable dtSerialNo)
        {
            // Return Datable 설정
            DataTable dtRtn = new DataTable();

            // Columns 설정
            dtRtn.Columns.Add("ReturnCode", typeof(string));
            dtRtn.Columns.Add("ReturnMessage", typeof(string));

            // Columns 기본값 설정
            DataRow drRow = dtRtn.NewRow();

            drRow["ReturnCode"] = string.Empty;
            drRow["ReturnMessage"] = string.Empty;

            dtRtn.Rows.Add(drRow);

            SQLS sql = new SQLS();

            try
            {
                // sql 链接
                sql.mfConnect();

                // 循环MACHINE记录读取内容
                for (int i = 0; i < dtSerialNo.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10); // 工厂代码
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20); // 产品ID
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomer", ParameterDirection.Input, SqlDbType.VarChar, strCustomer, 10);  // 客户名称
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);       //PACKAGE

                    sql.mfAddParamDataRow(dtParam, "@i_strMachine", ParameterDirection.Input, SqlDbType.VarChar, dtSerialNo.Rows[i]["Machine"].ToString(), 10);  // 设备名
                    sql.mfAddParamDataRow(dtParam, "@i_strMachineType", ParameterDirection.Input, SqlDbType.VarChar, dtSerialNo.Rows[i]["MachineType"].ToString(), 10);  // 设备类型
                    sql.mfAddParamDataRow(dtParam, "@i_strMachineGroup", ParameterDirection.Input, SqlDbType.VarChar, dtSerialNo.Rows[i]["MachineGroup"].ToString(), 10);  // 设备组
                    
                    DataTable dtResult = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATMachineModelAdmit_MESTKINChk", dtParam);

                    if (dtResult.Rows.Count > 0)
                    {

                        // 返回 ReturnCode信息
                        dtRtn.Rows[0]["ReturnCode"] = dtResult.Rows[0]["ReturnCode"];
                        dtRtn.Rows[0]["ReturnMessage"] = dtResult.Rows[0]["ErrorMessage"];

                        // 判断返回的ReturnCode是否N
                        if (dtResult.Rows[0]["ReturnCode"].ToString().Equals("N"))
                            break;
                    }
                    else
                    {
                        dtRtn.Rows[0]["ReturnCode"] = "99";
                        dtRtn.Rows[0]["ReturnMessage"] = " QRP - 品质保证管理-Machine Model审批情报里没有的信息是无法处理.(" + dtSerialNo.Rows[i]["Machine"].ToString().Trim() + ")";
                        break;
                    }
                }

                // Return 데이터 정보
                return dtRtn;
            }
            catch (Exception ex)
            {
                // Return Error Info
                dtRtn.Rows[0]["ReturnCode"] = "99";
                dtRtn.Rows[0]["ReturnMessage"] = ex.Message;

                return dtRtn;
                throw (ex);

            }
            finally
            {
                dtRtn.Dispose();
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// Mahcine Admit 정보 저장
        /// </summary>
        /// <param name="dtMachineAdmitList">저장할 데이터</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATMahcineAdmitList(DataTable dtMachineAdmitList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;
                DataTable dtMDM = mfSetDataInfo();
                DataRow dr;
                dr = dtMDM.NewRow();

                for (int i = 0; i < dtMachineAdmitList.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["CustomerCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMACHINEGROUPACTIONTYPE", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["MACHINEGROUPACTIONTYPE"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStack", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["Stack"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAdmitFlag", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["AdmitFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualNo", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["QualNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    sql.mfAddParamDataRow(dtParam, "@i_strReason", ParameterDirection.Input, SqlDbType.NVarChar, dtMachineAdmitList.Rows[i]["Reason"].ToString(), 200);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATMachineModelAdmitList", dtParam);

                    //string strPlantCode = dtParam.Rows[i]["@i_strPlantCode"].ToString();
                    //string strCustomerCode = dtParam.Rows[i]["@i_strCustomerCode"].ToString();
                    //string strPackage = dtParam.Rows[i]["@i_strPackage"].ToString();
                    //string strEquipGroupCode = dtParam.Rows[i]["@i_strEquipGroupCode"].ToString();
                    //string strStack = dtParam.Rows[i]["@i_intStack"].ToString();
                    //string strAdmitFlag = dtParam.Rows[i]["@i_strAdmitFlag"].ToString();

                    //string strRtn = mfSaveQATMachineModelAdmit_MDM(strPlantCode, strCustomerCode, strPackage, strEquipGroupCode, strAdmitFlag, strStack);

                    //// MDM I/F할 정보를 데이터테이블에 저장
                    //dr["PlantCode"] = dtParam.Rows[i]["@i_strPlantCode"].ToString();
                    //dr["CustomerCode"] = dtParam.Rows[i]["@i_strCustomerCode"].ToString();
                    //dr["Package"] = dtParam.Rows[i]["@i_strPackage"].ToString();
                    //dr["Stack"] = dtParam.Rows[i]["@i_intStack"].ToString();
                    //dr["EquipGroupCode"] = dtParam.Rows[i]["@i_strEquipGroupCode"].ToString();
                    //dr["AdmitFlag"] = dtParam.Rows[i]["@i_strAdmitFlag"].ToString();

                    //dtMDM.Rows.Add(dr);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                    //// 저장되면 MDM I/F 시작
                    //for (int i = 0; i < dtMDM.Rows.Count; i++)
                    //{
                    //    string strPlantCode = dtMDM.Rows[i]["PlantCode"].ToString();
                    //    string strCustomerCode = dtMDM.Rows[i]["CustomerCode"].ToString();
                    //    string strPackage = dtMDM.Rows[i]["Package"].ToString();
                    //    string strEquipGroupCode = dtMDM.Rows[i]["EquipGroupCode"].ToString();
                    //    string strStack = dtMDM.Rows[i]["Stack"].ToString();
                    //    string strAdmitFlag = dtMDM.Rows[i]["AdmitFlag"].ToString();

                    //    string strRtn = mfSaveQATMachineModelAdmit_MDM(strPlantCode, strCustomerCode, strPackage, strEquipGroupCode, strAdmitFlag, strStack);
                    //    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    //}
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// Mahcine Admit 정보 삭제
        /// </summary>
        /// <param name="dtMachineAdmitList">삭제할 데이터</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATMahcineAdmitList(DataTable dtMachineAdmitList)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                string strErrRtn = string.Empty;

                for (int i = 0; i < dtMachineAdmitList.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    //파라미터 저장
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["CustomerCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtMachineAdmitList.Rows[i]["EquipGroupCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATMachineModelAdmitList", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// MachineModelAdmit 저장 후 MDM 전송
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strCustomerCode"></param>
        /// <param name="strPackage"></param>
        /// <param name="strEquipGroupCode"></param>
        /// <param name="strAdmitFlag"></param>
        /// <param name="intStack"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATMachineModelAdmit_MDM(string strPlantCode, string strCustomerCode, string strPackage, string strEquipGroupCode, string strAdmitFlag, string strStack)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                sql.mfConnect();
                //Parameter
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipGroupCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipGroupCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strStack", ParameterDirection.Input, SqlDbType.VarChar, strStack, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strAdmitFlag", ParameterDirection.Input, SqlDbType.VarChar, strAdmitFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_QATMachineModelAdmit_MDM", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }
}
