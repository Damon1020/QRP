﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질보증관리                                          */
/* 프로그램ID   : clsQATCLM.cs                                          */
/* 프로그램명   : 고객불만관리                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-09-21                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]
namespace QRPQAT.BL.QATCLM
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CustomerClaimH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CustomerClaimH : ServicedComponent
    {
        /// <summary>
        /// 데이터테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDateInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ClaimNo", typeof(string));
                dtRtn.Columns.Add("CustomerProductSpec", typeof(string));
                dtRtn.Columns.Add("Productcode", typeof(string));
                dtRtn.Columns.Add("CustomerCode", typeof(string));
                dtRtn.Columns.Add("Package", typeof(string));
                dtRtn.Columns.Add("IssueTypeCode", typeof(string));
                dtRtn.Columns.Add("FaultTypeDesc", typeof(string));
                dtRtn.Columns.Add("DetectProcessCode", typeof(string));
                dtRtn.Columns.Add("ReceiptCompleteFlag", typeof(string));
                dtRtn.Columns.Add("ReceiptUserID", typeof(string));
                dtRtn.Columns.Add("ReceiptDate", typeof(string));
                dtRtn.Columns.Add("CustomerAnalysisResult", typeof(string));
                dtRtn.Columns.Add("AttachmentFileName", typeof(string));                
                dtRtn.Columns.Add("SampleReceiptDate", typeof(string));
                dtRtn.Columns.Add("Comment", typeof(string));
                dtRtn.Columns.Add("ImputeProcessCode", typeof(string));
                dtRtn.Columns.Add("ImputeDeptCode", typeof(string));
                dtRtn.Columns.Add("FourMOneE", typeof(string));
                dtRtn.Columns.Add("CompleteDate", typeof(string));
                dtRtn.Columns.Add("CompleteFlag", typeof(string));
                dtRtn.Columns.Add("MaterialDisposal", typeof(string));
                dtRtn.Columns.Add("ClaimCancelFlag", typeof(string));
                dtRtn.Columns.Add("ManFlag", typeof(string));
                dtRtn.Columns.Add("MethodFlag", typeof(string));
                dtRtn.Columns.Add("MaterialFlag", typeof(string));
                dtRtn.Columns.Add("MachineFlag", typeof(string));
                dtRtn.Columns.Add("EnviromentFlag", typeof(string));
                dtRtn.Columns.Add("ReasonDesc", typeof(string));
                dtRtn.Columns.Add("ActionDesc", typeof(string));
                dtRtn.Columns.Add("DueDate", typeof(string));
                dtRtn.Columns.Add("AnalyseCompleteFlag", typeof(string));
                dtRtn.Columns.Add("InspectDate", typeof(string));
                dtRtn.Columns.Add("InspectUserID", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(string));
                dtRtn.Columns.Add("InspectComment", typeof(string));
                
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 조회용 DataTable Column 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetSearchDateInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("CustomerCode", typeof(string));
                dtRtn.Columns.Add("Package", typeof(string));
                dtRtn.Columns.Add("IssueTypeCode", typeof(string));
                dtRtn.Columns.Add("CompleteFlag", typeof(string));
                dtRtn.Columns.Add("ReceiptDateFrom", typeof(string));
                dtRtn.Columns.Add("ReceiptDateTo", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 검색조건에 따른 조회 Method
        /// </summary>
        /// <param name="dtSearch">검색조건변수가 저장된 데이터테이블</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATCustomerClaimH(DataTable dtSearch, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();
                for (int i = 0; i < dtSearch.Rows.Count; i++)
                {
                    // 파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["CustomerCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strIssueTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["IssueTypeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptDateFrom", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ReceiptDateFrom"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptDateTo", ParameterDirection.Input, SqlDbType.VarChar, dtSearch.Rows[i]["ReceiptDateTo"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATCustomerClaimH", dtParam);
                }
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 고객불만관리 헤더 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATCustomerClaimHDetail(string strPlantCode, string strClaimNo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                // 파리미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATCustomerClaimHDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 고객불만 접수시 메일 수신인 검색(품질팀, C/S팀, 공정기술팀)
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATCustomerSendMailLine(string strPlantCode)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATCustomerClaimHSendMailLine", dtParam);

                return dtRtn;
            }
            catch(Exception ex)
            {
                return dtRtn;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 고객불만관리 헤더정보 저장 Method
        /// </summary>
        /// <param name="dtSave">헤더정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATCustomerClaimH(DataTable dtSave, string strUserID, string strUserIP, DataTable dtDetail, DataTable dt4D8D,DataTable dtEmailUser,string strEmailGubun)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                // DB 연결
                sql.mfConnect();
                SqlTransaction trans;
                string strErrRtn = "";
                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    //트랜잭션 시작
                    trans = sql.SqlCon.BeginTransaction();

                    //파라미터 데이터 테이블 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ClaimNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerProductSpec", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["CustomerProductSpec"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["CustomerCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["Package"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strIssueTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["IssueTypeCode"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strFaultTypeDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["FaultTypeDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strDetectProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["DetectProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReceiptCompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReceiptUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ReceiptDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerAnalysisResult", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["CustomerAnalysisResult"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strAttachmentFileName", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["AttachmentFileName"].ToString(), 1000);                    
                    sql.mfAddParamDataRow(dtParam, "@i_strSampleReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["SampleReceiptDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strComment", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["Comment"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strImputeProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ImputeProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strImputeDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["ImputeDeptCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFourMOneE", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["FourMOneE"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["CompleteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialDisposal", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["MaterialDisposal"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strClaimCancelFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ClaimCancelFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strManFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ManFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMethodFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["MethodFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMachineFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["MachineFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["MaterialFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEnviromentFlag", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["EnviromentFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReasonDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ReasonDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strActionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["ActionDesc"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strDueDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["DueDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strAnalyseComleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["AnalyseCompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectDate", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectResultFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectComment", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["InspectComment"].ToString(), 200);                    
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strClaimNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATCustomerClaimH", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // 에러발생시 롤백하고 결과 리턴
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        // 발행번호 OUTPUT값 저장
                        string strClaimNo = ErrRtn.mfGetReturnValue(0);
                        string strPlantCode = dtSave.Rows[i]["PlantCode"].ToString();

                        // 고객불만관리 상세/4D8D 정보 삭제
                        CustomerClaimD clsDetail = new CustomerClaimD();
                        strErrRtn = clsDetail.mfDeleteQATCustomerClaimD(strPlantCode, strClaimNo, sql.SqlCon, trans);
                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            // 에러발생시 롤백하고 결과 리턴
                            trans.Rollback();
                            break;
                        }

                        //수신자 리스트 삭제
                        CustomerClaimEMailUser clsMail = new CustomerClaimEMailUser();
                        strErrRtn = clsMail.mfDeleteQATCustomerClaimEMailUser(strPlantCode, strClaimNo, strEmailGubun, sql.SqlCon, trans);

                        // 결과검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            // 에러발생시 롤백하고 결과 리턴
                            trans.Rollback();
                            break;
                        }

                        if (dtDetail.Rows.Count > 0)
                        {
                            // 고객불만관리 상세정보 저장
                            strErrRtn = clsDetail.mfSaveQATCustomerClaimD(dtDetail, strPlantCode, strClaimNo, strUserID, strUserIP, sql.SqlCon, trans);
                            // 결과검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                // 에러발생시 롤백하고 결과 리턴
                                trans.Rollback();
                                break;
                            }
                        }

                        if(dt4D8D.Rows.Count>0)
                        {

                            // 4D8D 정보 저장
                            CustomerClaim4D8D cls4D8D = new CustomerClaim4D8D();
                            strErrRtn = cls4D8D.mfSaveQATCustomerClaim4D8D(dt4D8D, strUserID, strUserIP, sql.SqlCon, trans, strPlantCode, strClaimNo);

                            if (ErrRtn.ErrNum != 0)
                            {
                                // 에러발생시 롤백하고 결과 리턴
                                trans.Rollback();
                                break;
                            }
                        }

                        //Email User Save
                        if (dtEmailUser.Rows.Count > 0)
                        {

                            strErrRtn = clsMail.mfSaveQATCustomerClaimEMailUser(dtEmailUser, strUserID, strUserIP, sql.SqlCon, trans, strPlantCode, strClaimNo);

                            if (ErrRtn.ErrNum != 0)
                            {
                                // 에러발생시 롤백하고 결과 리턴
                                trans.Rollback();
                                break;
                            }
                        }


                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                    }
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 고객불만관리 삭제 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATCustomerClaimH(string strPlantCode, string strClaimNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = "";
                // 디비연결
                sql.mfConnect();
                // 트랜잭션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 파라미터 데이터 테이블 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시져 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATCustomerClaimH", dtParam);
                
                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                {
                    // 에러발생시 롤백하고 결과 리턴
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CustomerClaimD")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CustomerClaimD : ServicedComponent
    {
        /// <summary>
        /// 상세정보 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                ////dtRtn.Columns.Add("PlantCode", typeof(string));
                ////dtRtn.Columns.Add("ClaimNo", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("FailureMode", typeof(string));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("Qty", typeof(double));
                dtRtn.Columns.Add("RejectQty", typeof(double));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 고객불만관리 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATCustomerClaimD(string strPlantCode, string strClaimNo)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATCustomerClaimD", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 고객불만관리 상세정보 저장
        /// </summary>
        /// <param name="dtSave">상세정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATCustomerClaimD(DataTable dtSave, string strPlantCode, string strClaimNo, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = "";

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strFailureMode", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["FailureMode"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_dblQty", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["Qty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblRejectQty", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["RejectQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strClaimNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATCustomerClaimD", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 고객불만관리 상세/4D8D 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <param name="sqlCon">SqlConnection변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATCustomerClaimD(string strPlantCode, string strClaimNo, SqlConnection sqlCon, SqlTransaction trans)
        {
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATCustomerClaimD_4D8D", dtParam);
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CustomerClaim4D8D")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CustomerClaim4D8D : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼 설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("Gubun", typeof(string));
                dtRtn.Columns.Add("SendDate", typeof(string));
                dtRtn.Columns.Add("FilePath", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 4D8D 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATCustomerClaim4D8D(string strPlantCode, string strClaimNo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATCustmerClaim4D8D", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 4D8D 정보 저장 메소드
        /// </summary>
        /// <param name="dtSave">4D8D 정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATCustomerClaim4D8D(DataTable dtSave, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strPlantCode, string strClaimNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGubun", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["Gubun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strSendDate", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["SendDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFilePath", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["FilePath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strClaimNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATCustomerClaim4D8D", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CustomerClaimEMailUser")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CustomerClaimEMailUser : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼 설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ClaimNo", typeof(string));
                dtRtn.Columns.Add("Gubun", typeof(string));
                dtRtn.Columns.Add("Seq", typeof(string));
                dtRtn.Columns.Add("MailUserID", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Email 수신자  조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATCustomerClaimEMailUser(string strPlantCode, string strClaimNo, string strGubun, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strGubun", ParameterDirection.Input, SqlDbType.Char, strGubun, 1);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATCustomerClaimEMailUser", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// Email 수신자 정보 저장 메소드
        /// </summary>
        /// <param name="dtSave">4D8D 정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATCustomerClaimEMailUser(DataTable dtSave, string strUserID, string strUserIP, string strPlantCode, string strClaimNo,string strGubun)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
               
                string strErrRtn = string.Empty;

                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                strErrRtn = mfDeleteQATCustomerClaimEMailUser(strPlantCode, strClaimNo, strGubun, sql.SqlCon, trans);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGubun", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["Gubun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMailUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MailUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strClaimNo", ParameterDirection.Output, SqlDbType.VarChar, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATCustomerClaimEMailUser", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();
               
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// Email 수신자 정보 저장 메소드
        /// </summary>
        /// <param name="dtSave">4D8D 정보가 저장된 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATCustomerClaimEMailUser(DataTable dtSave, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans, string strPlantCode, string strClaimNo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strGubun", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["Gubun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strMailUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["MailUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["EtcDesc"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strClaimNo", ParameterDirection.Output, SqlDbType.VarChar, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATCustomerClaimEMailUser", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }



        /// <summary>
        /// Email 수신자 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strClaimNo">발행번호</param>
        /// <param name="sqlCon">SqlConnection변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATCustomerClaimEMailUser(string strPlantCode, string strClaimNo, string strGubun,SqlConnection sqlCon, SqlTransaction trans)
        {
            try
            {
                SQLS sql = new SQLS();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strClaimNo", ParameterDirection.Input, SqlDbType.VarChar, strClaimNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strGubun", ParameterDirection.Input, SqlDbType.Char, strGubun, 1);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATCustomerClaimEMailUser", dtParam);
                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SRR")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SRR : ServicedComponent
    {

        /// <summary>
        /// DataTable 컬럼 설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ManageNo", typeof(string));
                dtRtn.Columns.Add("CumtomerCode", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("SPeriodQuarter", typeof(string));
                dtRtn.Columns.Add("SAccident", typeof(string));
                dtRtn.Columns.Add("SLowYield", typeof(string));
                dtRtn.Columns.Add("SICN", typeof(string));
                dtRtn.Columns.Add("SAssyYield", typeof(string));
                dtRtn.Columns.Add("SDCTestYield", typeof(string));
                dtRtn.Columns.Add("SQualRun", typeof(string));
                dtRtn.Columns.Add("SQualitySystem", typeof(string));
                dtRtn.Columns.Add("SResponse", typeof(string));
                dtRtn.Columns.Add("SExecution", typeof(string));
                dtRtn.Columns.Add("SQualityAction", typeof(string));
                dtRtn.Columns.Add("SInspect", typeof(string));
                dtRtn.Columns.Add("SScore", typeof(string));
                dtRtn.Columns.Add("SLanking", typeof(string));
                dtRtn.Columns.Add("MPeriodMonth", typeof(string));
                dtRtn.Columns.Add("MQualityLevel", typeof(string));
                dtRtn.Columns.Add("MQualitySystem", typeof(string));
                dtRtn.Columns.Add("MCooperation", typeof(string));
                dtRtn.Columns.Add("MResponse", typeof(string));
                dtRtn.Columns.Add("MPenaltyAward", typeof(string));
                dtRtn.Columns.Add("MScore", typeof(string));
                dtRtn.Columns.Add("MLanking", typeof(string));
                dtRtn.Columns.Add("SClaim", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }



        /// <summary>
        /// SRR 헤더정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strCumtomerCode">고객</param>
        /// <param name="strFromDate">검색시작일</param>
        /// <param name="strToDate">검색종료일</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSRR(string strPlantCode, string strCumtomerCode, string strFromDate, string strToDate,string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCumtomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCumtomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRR_HeaderList", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// SRR 정보  조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManageNo">발행번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSRR_Detail(string strPlantCode, string strManageNo, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRR_Detail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// SRR 정보 저장 메소드
        /// </summary>
        /// <param name="dtSave">SRR 등록정보</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveSRR(DataTable dtSRR, string strUserID, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {

                string strErrRtn = string.Empty;

                sql.mfConnect();

                SqlTransaction trans = sql.SqlCon.BeginTransaction();


                for (int i = 0; i < dtSRR.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSRR.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, dtSRR.Rows[i]["ManageNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCumtomerCode", ParameterDirection.Input, SqlDbType.VarChar, dtSRR.Rows[i]["CumtomerCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSRR.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSRR.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strSPeriodQuarter", ParameterDirection.Input, SqlDbType.Char, dtSRR.Rows[i]["SPeriodQuarter"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_dblSAccident", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SAccident"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSLowYield", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SLowYield"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSICN", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SICN"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSAssyYield", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SAssyYield"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSDCTestYield", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SDCTestYield"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSQualRun", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SQualRun"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSQualitySystem", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SQualitySystem"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSResponse", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SResponse"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSExecution", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SExecution"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSQualityAction", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SQualityAction"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSInspect", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SInspect"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSScore", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SScore"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblSLanking", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SLanking"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_strMPeriodMonth", ParameterDirection.Input, SqlDbType.Char, dtSRR.Rows[i]["MPeriodMonth"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_dblMQualityLevel", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["MQualityLevel"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMQualitySystem", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["MQualitySystem"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMCooperation", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["MCooperation"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMResponse", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["MResponse"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMPenaltyAward", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["MPenaltyAward"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMScore", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["MScore"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMLanking", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["MLanking"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@i_dblSClaim", ParameterDirection.Input, SqlDbType.Decimal, dtSRR.Rows[i]["SClaim"].ToString());    // 2012-11-16 추가 구

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATSRR", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// SRR 정보 삭제 Method
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strManageNo">발행번호</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteSRR(string strPlantCode, string strManageNo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strManageNo", ParameterDirection.Input, SqlDbType.VarChar, strManageNo, 20);
                
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATSRR", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


    }
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("SRRMasterHeader")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SRRMasterHeader : ServicedComponent
    {
        #region 압축

        /// <summary>
        /// 데이터테이블 압축
        /// </summary>
        /// <param name="dtData">압축할 데이터 테이블</param>
        /// <returns></returns>
        private string Compress_DT(DataTable dtData)
        {
            // DataTable Serialize
            dtData.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                bf.Serialize(ms, dtData);
                byte[] intbyt = ms.ToArray();

                // DataTable Compression
                using (System.IO.MemoryStream objStream = new System.IO.MemoryStream())
                {
                    System.IO.Compression.DeflateStream objZS = new System.IO.Compression.DeflateStream(objStream, System.IO.Compression.CompressionMode.Compress);
                    objZS.Write(intbyt, 0, intbyt.Length);
                    objZS.Flush();
                    objZS.Close();

                    return Convert.ToBase64String(objStream.ToArray(), 0, objStream.ToArray().Length);
                }
            }
        }

        /// <summary>
        /// 데이터셋 압축
        /// </summary>
        /// <param name="dtData">압축할 데이터셋</param>
        /// <returns></returns>
        private string Compress_DS(DataSet dsData)
        {
            // DataTable Serialize
            dsData.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                bf.Serialize(ms, dsData);
                byte[] intbyt = ms.ToArray();

                // DataTable Compression
                using (System.IO.MemoryStream objStream = new System.IO.MemoryStream())
                {
                    System.IO.Compression.DeflateStream objZS = new System.IO.Compression.DeflateStream(objStream, System.IO.Compression.CompressionMode.Compress);
                    objZS.Write(intbyt, 0, intbyt.Length);
                    objZS.Flush();
                    objZS.Close();

                    return Convert.ToBase64String(objStream.ToArray(), 0, objStream.ToArray().Length);
                }
            }
        }

        #endregion

        #region Datatable, DataSet -> XML

        private string ConvertDataTableToXML(DataTable dtTable)
        {
            try
            {
                DataTable dtClone = dtTable.Copy();
                dtClone.TableName = "XMLDT";

                System.Text.StringBuilder sb = new StringBuilder();
                using (System.IO.StringWriter stream = new System.IO.StringWriter(sb))
                {
                    dtClone.WriteXml(stream, XmlWriteMode.WriteSchema);
                    stream.Flush();
                    return sb.ToString();
                }
            }
            catch (System.Exception ex)
            {
                //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                //{
                //    clsLog.Log(this.GetType().FullName, ex);
                //}
                return string.Empty;
            }
        }

        private string ConvertDataSetToXML(DataSet dsTable)
        {
            try
            {
                DataSet dsClone = dsTable.Copy();
                dsTable.DataSetName = "XMLDS";

                System.Text.StringBuilder sb = new StringBuilder();
                using (System.IO.StringWriter stream = new System.IO.StringWriter(sb))
                {
                    dsClone.WriteXml(stream, XmlWriteMode.WriteSchema);
                    stream.Flush();
                    return sb.ToString();
                }
            }
            catch (System.Exception ex)
            {
                //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                //{
                //    clsLog.Log(this.GetType().FullName, ex);
                //}
                return string.Empty;
            }
        }

        #endregion

        /// <summary>
        /// SRR 고객사 구분 콤보 설정 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATSRRMasterHeader_CustomerProdType_Combo(string strPlantCode, string strGubun, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                DataTable dtCusProd = new DataTable();
                try
                {
                    // DB Connect
                    sql.mfConnect();

                    // Set Parameter DataTable
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strGubun", ParameterDirection.Input, SqlDbType.VarChar, strGubun, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    // Call SP
                    dtCusProd = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRMasterHeader_CustomerProdType", dtParam);

                    dtParam.Dispose();

                    return dtCusProd;
                }
                catch (Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    return dtCusProd;
                    throw (ex);
                }
                finally
                {
                    sql.mfDisConnect();
                    dtCusProd.Dispose();
                }
            }
        }

        /// <summary>
        /// SRR 입력항목등록 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfReadQATSRRMasterHeader(string strPlantCode, string strCustomerCode, string strProdType, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    // Connecting DataBase
                    sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    string strRtn = string.Empty;
                    using (DataTable dtHeader = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRMasterHeader", dtParam))
                    {
                        //strRtn = Compress_DT(dtHeader);
                        strRtn = ConvertDataTableToXML(dtHeader);
                    }

                    ErrRtn.ErrNum = 0;
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strRtn);

                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// SRR 입력항목등록 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadQATSRRMasterHeader_datatable(string strPlantCode, string strCustomerCode, string strProdType, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                DataTable dtHeader = new DataTable();
                try
                {
                    // Connecting DataBase
                    sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    return dtHeader = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRMasterHeader", dtParam);
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    return dtHeader;
                    throw (ex);
                }
                finally
                {
                    sql.mfDisConnect();
                    dtHeader.Dispose();
                }
            }
        }

        /// <summary>
        /// SRR 입력항목 상세조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평가주기</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfReadQATSRRMasterHeader_Item_Detail(string strPlantCode, string strCustomerCode, string strProdType
            , string strEvaluationPeriod, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    string strRtn = string.Empty;

                    sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    using (DataTable dtHeader_Detail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRMasterHeader_Detail", dtParam))
                    {
                        dtHeader_Detail.TableName = "Header";
                        using (DataTable dtItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRMasterItem", dtParam))
                        {
                            dtItem.TableName = "Item";
                            using (DataSet dsSRRInfo = new DataSet())
                            {
                                dsSRRInfo.Tables.Add(dtHeader_Detail);
                                dsSRRInfo.Tables.Add(dtItem);

                                //strRtn = Compress_DS(dsSRRInfo);
                                strRtn = ConvertDataSetToXML(dsSRRInfo);
                            }
                        }
                    }

                    ErrRtn.ErrNum = 0;
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strRtn);

                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// SRR 정보 저장
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평가주기</param>
        /// <param name="strTotalScore">총점</param>
        /// <param name="strUserID">사용자ID/param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strItem_XML">XML_Item 정보</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATSRRMasterHeader(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod, string strTotalScore
            , string strUserID, string strUserIP, string strItem_XML)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();

                try
                {
                    string strErrRtn = string.Empty;

                    // DB Connect
                    sql.mfConnect();
                    using (SqlTransaction trans = sql.SqlCon.BeginTransaction())
                    {
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParam, "@i_StrCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                        sql.mfAddParamDataRow(dtParam, "@i_strTotalScore", ParameterDirection.Input, SqlDbType.VarChar, strTotalScore, 30);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATSRRMasterHeader", dtParam);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            // Saving Item Info
                            using (QRPQAT.BL.QATCLM.SRRMasterItem clsItem = new QRPQAT.BL.QATCLM.SRRMasterItem())
                            {
                                // 기존 SRRItem 정보 삭제
                                strErrRtn = clsItem.mfDeleteQATSRRMasterItem(strPlantCode, strCustomerCode, strProdType, strEvaluationPeriod, sql.SqlCon, trans);

                                // 결과검사
                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (!ErrRtn.ErrNum.Equals(0))
                                {
                                    trans.Rollback();
                                    return strErrRtn;
                                }

                                // Save Item Info
                                strErrRtn = clsItem.mfSaveQATSRRMasterItem(strPlantCode, strCustomerCode, strProdType, strEvaluationPeriod
                                    , strUserID, strUserIP, strItem_XML, sql.SqlCon, trans);
                            }
                        }

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                        else
                            trans.Rollback();
                    }

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// Delete SRR Master Info
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평가주기</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATSRRMasterHeader(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    string strErrRtn = string.Empty;
                    sql.mfConnect();

                    using (SqlTransaction trans = sql.SqlCon.BeginTransaction())
                    {
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATSRRMasterHeader", dtParam);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                        else
                            trans.Rollback();
                    }

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }
    }


    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("SRRMasterItem")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SRRMasterItem : ServicedComponent
    {
        /// <summary>
        /// Save SRRItem 
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평가주기</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strItem_XML">XML_Item 정보</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATSRRMasterItem(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod
            , string strUserID, string strUserIP, string strItem_XML, SqlConnection sqlCon, SqlTransaction trans)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    string strErrRtn = string.Empty;

                    System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                    xmlDoc.LoadXml(strItem_XML);

                    System.Xml.XmlNodeList _NodeList = xmlDoc.SelectNodes("DocumentElement/*");

                    DataTable dtParam;
                    foreach (System.Xml.XmlNode _node in _NodeList)
                    {
                        dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                        sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, _node.SelectSingleNode("Seq").InnerText);
                        sql.mfAddParamDataRow(dtParam, "@i_strEvaluationType", ParameterDirection.Input, SqlDbType.NVarChar, _node.SelectSingleNode("EvaluationType").InnerText, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strEvaluationItem", ParameterDirection.Input, SqlDbType.NVarChar, _node.SelectSingleNode("EvaluationItem").InnerText, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_dblScore", ParameterDirection.Input, SqlDbType.Decimal, _node.SelectSingleNode("Score").InnerText);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATSRRMasterItem", dtParam);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0))
                            break;
                    }

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                }
            }
        }

        /// <summary>
        /// Delete SRRItem
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평가주기</param>
        /// <param name="sqlCon"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATSRRMasterItem(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod
            , SqlConnection sqlCon, SqlTransaction trans)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    string strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Delete_QATSRRMasterItem", dtParam);

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                }
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("SRRHeader")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SRRHeader : ServicedComponent
    {
        #region Data 압축

        /// <summary>
        /// 데이터테이블 압축
        /// </summary>
        /// <param name="dtData">압축할 데이터 테이블</param>
        /// <returns></returns>
        private string Compress_DT(DataTable dtData)
        {
            // DataTable Serialize
            dtData.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                bf.Serialize(ms, dtData);
                byte[] intbyt = ms.ToArray();

                // DataTable Compression
                using (System.IO.MemoryStream objStream = new System.IO.MemoryStream())
                {
                    System.IO.Compression.DeflateStream objZS = new System.IO.Compression.DeflateStream(objStream, System.IO.Compression.CompressionMode.Compress);
                    objZS.Write(intbyt, 0, intbyt.Length);
                    objZS.Flush();
                    objZS.Close();

                    return Convert.ToBase64String(objStream.ToArray(), 0, objStream.ToArray().Length);
                }
            }
        }

        /// <summary>
        /// 데이터셋 압축
        /// </summary>
        /// <param name="dtData">압축할 데이터셋</param>
        /// <returns></returns>
        private string Compress_DS(DataSet dsData)
        {
            // DataTable Serialize
            dsData.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                bf.Serialize(ms, dsData);
                byte[] intbyt = ms.ToArray();

                // DataTable Compression
                using (System.IO.MemoryStream objStream = new System.IO.MemoryStream())
                {
                    System.IO.Compression.DeflateStream objZS = new System.IO.Compression.DeflateStream(objStream, System.IO.Compression.CompressionMode.Compress);
                    objZS.Write(intbyt, 0, intbyt.Length);
                    objZS.Flush();
                    objZS.Close();

                    return Convert.ToBase64String(objStream.ToArray(), 0, objStream.ToArray().Length);
                }
            }
        }

        #endregion

        #region Datatable, DataSet -> XML

        private string ConvertDataTableToXML(DataTable dtTable)
        {
            try
            {
                DataTable dtClone = dtTable.Copy();
                dtClone.TableName = "XMLDT";

                System.Text.StringBuilder sb = new StringBuilder();
                using (System.IO.StringWriter stream = new System.IO.StringWriter(sb))
                {
                    dtClone.WriteXml(stream, XmlWriteMode.WriteSchema);
                    stream.Flush();
                    return sb.ToString();
                }
            }
            catch (System.Exception ex)
            {
                //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                //{
                //    clsLog.Log(this.GetType().FullName, ex);
                //}
                return string.Empty;
            }
        }

        private string ConvertDataSetToXML(DataSet dsTable)
        {
            try
            {
                DataSet dsClone = dsTable.Copy();
                dsTable.DataSetName = "XMLDS";

                System.Text.StringBuilder sb = new StringBuilder();
                using (System.IO.StringWriter stream = new System.IO.StringWriter(sb))
                {
                    dsClone.WriteXml(stream, XmlWriteMode.WriteSchema);
                    stream.Flush();
                    return sb.ToString();
                }
            }
            catch (System.Exception ex)
            {
                //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                //{
                //    clsLog.Log(this.GetType().FullName, ex);
                //}
                return string.Empty;
            }
        }

        #endregion

        /// <summary>
        /// SRR 점수등록 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfReadQATSRRHeader(string strPlantCode, string strCustomerCode, string strProdType, string strYear, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    // Connecting DataBase
                    sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    string strRtn = string.Empty;
                    using (DataTable dtHeader = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRHeader", dtParam))
                    {
                        //strRtn = Compress_DT(dtHeader);
                        strRtn = ConvertDataTableToXML(dtHeader);
                    }

                    ErrRtn.ErrNum = 0;
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strRtn);

                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// SRR 점수등록 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfReadQATSRRHeader_Chart(string strPlantCode, string strCustomerCode, string strProdType, string strYear, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    // Connecting DataBase
                    sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    string strRtn = string.Empty;
                    using (DataTable dtHeader = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRHeader_Chart", dtParam))
                    {
                        strRtn = ConvertDataTableToXML(dtHeader);
                    }

                    ErrRtn.ErrNum = 0;
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strRtn);

                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// SRR 점수등록 상세화면 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평기주기</param>
        /// <param name="strTargetCompany">대상업체</param>
        /// <param name="strYear">년도</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfReadQATSRRHeader_Item_Detail(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod
            , string strTargetCompany, string strYear, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    string strRtn = string.Empty;

                    sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strTargetCompany", ParameterDirection.Input, SqlDbType.NVarChar, strTargetCompany, 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    using (DataTable dtHeader_Detail = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRHeader_Detail", dtParam))
                    {
                        dtHeader_Detail.TableName = "Header";
                        using (DataTable dtItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRItem", dtParam))
                        {
                            dtItem.TableName = "Item";
                            using (DataSet dsSRRInfo = new DataSet())
                            {
                                dsSRRInfo.Tables.Add(dtHeader_Detail);
                                dsSRRInfo.Tables.Add(dtItem);

                                //strRtn = Compress_DS(dsSRRInfo);
                                strRtn = ConvertDataSetToXML(dsSRRInfo);
                            }
                        }
                    }

                    ErrRtn.ErrNum = 0;
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strRtn);

                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// SRR 점수등록 저장
        /// </summary>
        /// <param name="strHeader_XML">헤더 XML</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strItem_XML">상세정보 XML</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATSRRHeader(string strHeader_XML, string strUserID, string strUserIP, string strItem_XML)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    sql.mfConnect();
                    string strErrRtn = string.Empty;

                    using (SqlTransaction trans = sql.SqlCon.BeginTransaction())
                    {
                        System.Xml.XmlDocument xmlHeader = new System.Xml.XmlDocument();
                        xmlHeader.LoadXml(strHeader_XML);

                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/PlantCode").InnerText, 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/CustomerCode").InnerText, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, xmlHeader.SelectSingleNode("message/body/ProdType").InnerText, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/EvaluationPeriod").InnerText, 3);
                        sql.mfAddParamDataRow(dtParam, "@i_strTargetCompany", ParameterDirection.Input, SqlDbType.NVarChar, xmlHeader.SelectSingleNode("message/body/TargetCompany").InnerText, 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/Year").InnerText, 4);
                        sql.mfAddParamDataRow(dtParam, "@i_strM01", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M01").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM02", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M02").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM03", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M03").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM04", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M04").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM05", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M05").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM06", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M06").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM07", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M07").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM08", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M08").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM09", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M09").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM10", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M10").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM11", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M11").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strM12", ParameterDirection.Input, SqlDbType.VarChar, xmlHeader.SelectSingleNode("message/body/M12").InnerText, 1);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATSRRHeader", dtParam);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            // 성공시 상세정보 저장
                            using (QRPQAT.BL.QATCLM.SRRItem clsItem = new QRPQAT.BL.QATCLM.SRRItem())
                            {
                                strErrRtn = clsItem.mfSaveQATSRRItem(xmlHeader.SelectSingleNode("message/body/PlantCode").InnerText
                                    , xmlHeader.SelectSingleNode("message/body/CustomerCode").InnerText
                                    , xmlHeader.SelectSingleNode("message/body/ProdType").InnerText
                                    , xmlHeader.SelectSingleNode("message/body/EvaluationPeriod").InnerText
                                    , xmlHeader.SelectSingleNode("message/body/TargetCompany").InnerText
                                    , xmlHeader.SelectSingleNode("message/body/Year").InnerText
                                    , strUserID, strUserIP, strItem_XML, sql.SqlCon, trans);


                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum.Equals(0))
                                    strErrRtn = clsItem.mfSaveQATSRRItem_Rank(xmlHeader.SelectSingleNode("message/body/PlantCode").InnerText
                                        , xmlHeader.SelectSingleNode("message/body/CustomerCode").InnerText
                                        , xmlHeader.SelectSingleNode("message/body/ProdType").InnerText
                                        , xmlHeader.SelectSingleNode("message/body/Year").InnerText
                                        , strUserID, strUserIP, sql.SqlCon, trans);
                            }
                        }

                        // 결과에 따른 Commit, RollBack 처리
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                        else
                            trans.Rollback();
                    }

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// SRR 점수등록 저장
        /// </summary>
        /// <param name="strPlantCode">공장코드/param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평가주기</param>
        /// <param name="strTargetCompany">대상업체</param>
        /// <param name="strYear">년도</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strItem_XML">상세정보 XML</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATSRRHeader(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod
            , string strTargetCompany, string strYear, string strUserID, string strUserIP, string strItem_XML)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    sql.mfConnect();
                    string strErrRtn = string.Empty;

                    using (SqlTransaction trans = sql.SqlCon.BeginTransaction())
                    {
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                        sql.mfAddParamDataRow(dtParam, "@i_strTargetCompany", ParameterDirection.Input, SqlDbType.NVarChar, strTargetCompany, 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_QATSRRHeader", dtParam);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            // 성공시 상세정보 저장
                            using (QRPQAT.BL.QATCLM.SRRItem clsItem = new QRPQAT.BL.QATCLM.SRRItem())
                            {
                                strErrRtn = clsItem.mfSaveQATSRRItem(strPlantCode, strCustomerCode, strProdType, strEvaluationPeriod, strTargetCompany, strYear
                                    , strUserID, strUserIP, strItem_XML, sql.SqlCon, trans);


                                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                if (ErrRtn.ErrNum.Equals(0))
                                    strErrRtn = clsItem.mfSaveQATSRRItem_Rank(strPlantCode, strCustomerCode, strProdType, strYear
                                        , strUserID, strUserIP, sql.SqlCon, trans);
                            }
                        }

                        // 결과에 따른 Commit, RollBack 처리
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                        else
                            trans.Rollback();
                    }

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// SRR 점수등록 삭제
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strCustomerCode"></param>
        /// <param name="strProdType"></param>
        /// <param name="strEvaluationPeriod"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteQATSRRMasterHeader(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod
            , string strTargetCompany, string strYear)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    string strErrRtn = string.Empty;
                    sql.mfConnect();

                    using (SqlTransaction trans = sql.SqlCon.BeginTransaction())
                    {
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                        sql.mfAddParamDataRow(dtParam, "@i_strTargetCompany", ParameterDirection.Input, SqlDbType.NVarChar, strTargetCompany, 50);
                        sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_QATSRRHeader", dtParam);

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            // 삭제후 Rank 재설정을 위한 Rank 설정 메소드 호출
                            using (SRRItem clsItem = new SRRItem())
                            {
                                strErrRtn = clsItem.mfSaveQATSRRItem_Rank(strPlantCode, strCustomerCode, strProdType, strYear
                                    , "QRPServer", "QRPServer", sql.SqlCon, trans);
                            }
                        }

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                            trans.Commit();
                        else
                            trans.Rollback();
                    }

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("SRRItem")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SRRItem : ServicedComponent
    {
        #region 압축메소드

        /// <summary>
        /// 데이터테이블 압축
        /// </summary>
        /// <param name="dtData">압축할 데이터 테이블</param>
        /// <returns></returns>
        private string Compress_DT(DataTable dtData)
        {
            // DataTable Serialize
            dtData.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                bf.Serialize(ms, dtData);
                byte[] intbyt = ms.ToArray();

                // DataTable Compression
                using (System.IO.MemoryStream objStream = new System.IO.MemoryStream())
                {
                    System.IO.Compression.DeflateStream objZS = new System.IO.Compression.DeflateStream(objStream, System.IO.Compression.CompressionMode.Compress);
                    objZS.Write(intbyt, 0, intbyt.Length);
                    objZS.Flush();
                    objZS.Close();

                    return Convert.ToBase64String(objStream.ToArray(), 0, objStream.ToArray().Length);
                }
            }
        }

        /// <summary>
        /// 데이터셋 압축
        /// </summary>
        /// <param name="dtData">압축할 데이터셋</param>
        /// <returns></returns>
        private string Compress_DS(DataSet dsData)
        {
            // DataTable Serialize
            dsData.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                bf.Serialize(ms, dsData);
                byte[] intbyt = ms.ToArray();

                // DataTable Compression
                using (System.IO.MemoryStream objStream = new System.IO.MemoryStream())
                {
                    System.IO.Compression.DeflateStream objZS = new System.IO.Compression.DeflateStream(objStream, System.IO.Compression.CompressionMode.Compress);
                    objZS.Write(intbyt, 0, intbyt.Length);
                    objZS.Flush();
                    objZS.Close();

                    return Convert.ToBase64String(objStream.ToArray(), 0, objStream.ToArray().Length);
                }
            }
        }

        #endregion

        #region Datatable -> XML

        private string ConvertDataTableToXML(DataTable dtTable)
        {
            try
            {
                DataTable dtClone = dtTable.Copy();
                dtClone.TableName = "XMLDT";

                System.Text.StringBuilder sb = new StringBuilder();
                using (System.IO.StringWriter stream = new System.IO.StringWriter(sb))
                {
                    dtClone.WriteXml(stream, XmlWriteMode.WriteSchema);
                    stream.Flush();
                    return sb.ToString();
                }
            }
            catch (System.Exception ex)
            {
                //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                //{
                //    clsLog.Log(this.GetType().FullName, ex);
                //}
                return string.Empty;
            }
        }

        #endregion

        /// <summary>
        /// SRR Item 정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평가주기</param>
        /// <param name="strYear">년도</param>
        /// <param name="strTargetCompany">대상업체</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfReadQATSRRItem(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod
            , string strYear, string strTargetCompany, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    string strRtn = string.Empty;

                    sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strTargetCompany", ParameterDirection.Input, SqlDbType.NVarChar, strTargetCompany, 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                    ////sql.mfAddParamDataRow(dtParam, "@o_strEvaluationPeriod", ParameterDirection.Output, SqlDbType.VarChar, 3);
                    ////sql.mfAddParamDataRow(dtParam, "@o_strState", ParameterDirection.Output, SqlDbType.VarChar, 1);

                    using (DataTable dtSRRItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRItem", dtParam))
                    {
                        dtSRRItem.TableName = "Item";
                        //strRtn = Compress_DT(dtSRRItem);
                        strRtn = ConvertDataTableToXML(dtSRRItem);
                    }

                    ErrRtn.ErrNum = 0;
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strRtn);

                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// 랭킹판정을 위한 Item 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strYear">년도</param>
        /// <param name="strTargetCompany">대상업체</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfReadQATSRRItem_ForRank(string strPlantCode, string strCustomerCode, string strProdType, string strYear
            , string strTargetCompany, string strLang)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();

                try
                {
                    string strRtn = string.Empty;

                    sql.mfConnect();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strTargetCompany", ParameterDirection.Input, SqlDbType.NVarChar, strTargetCompany, 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                    using (DataTable dtSRRItem = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_QATSRRItem_ForRank", dtParam))
                    {
                        dtSRRItem.TableName = "Item";
                        //strRtn = Compress_DT(dtSRRItem);
                        strRtn = ConvertDataTableToXML(dtSRRItem);
                    }

                    ErrRtn.ErrNum = 0;
                    ErrRtn.mfInitReturnValue();
                    ErrRtn.mfAddReturnValue(strRtn);

                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                    sql.mfDisConnect();
                }
            }
        }

        /// <summary>
        /// SRR Item 정보 저장
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strEvaluationPeriod">평가주기</param>
        /// <param name="strTargetCompany">대상업체</param>
        /// <param name="strYear">년도</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strItem_XML">Item 정보 XML </param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATSRRItem(string strPlantCode, string strCustomerCode, string strProdType, string strEvaluationPeriod
            , string strTargetCompany, string strYear, string strUserID, string strUserIP, string strItem_XML, SqlConnection sqlCon, SqlTransaction trans)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    string strErrRtn = string.Empty;

                    // String -> XML
                    System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                    xmlDoc.LoadXml(strItem_XML);

                    System.Xml.XmlNodeList _NodeList = xmlDoc.SelectNodes("DocumentElement/*");

                    DataTable dtParam;
                    foreach (System.Xml.XmlNode _node in _NodeList)
                    {
                        System.Xml.XmlNodeList ndListPeriodType = _node.SelectNodes("PeriodType/*");
                        foreach (System.Xml.XmlNode ndChild in ndListPeriodType)
                        {
                            dtParam = sql.mfSetParamDataTable();
                            sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                            sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                            sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                            sql.mfAddParamDataRow(dtParam, "@i_strEvaluationPeriod", ParameterDirection.Input, SqlDbType.VarChar, strEvaluationPeriod, 3);
                            sql.mfAddParamDataRow(dtParam, "@i_strTargetCompany", ParameterDirection.Input, SqlDbType.NVarChar, strTargetCompany, 50);
                            sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                            sql.mfAddParamDataRow(dtParam, "@i_strPeriodType", ParameterDirection.Input, SqlDbType.VarChar, ndChild.Name, 10);
                            sql.mfAddParamDataRow(dtParam, "@i_strEvaluationType", ParameterDirection.Input, SqlDbType.NVarChar, _node.SelectSingleNode("EvaluationType").InnerText, 100);
                            sql.mfAddParamDataRow(dtParam, "@i_strEvaluationItem", ParameterDirection.Input, SqlDbType.NVarChar, _node.SelectSingleNode("EvaluationItem").InnerText, 100);
                            sql.mfAddParamDataRow(dtParam, "@i_strScore", ParameterDirection.Input, SqlDbType.VarChar, _node.SelectSingleNode("Score").InnerText, 10);
                            sql.mfAddParamDataRow(dtParam, "@i_strValue", ParameterDirection.Input, SqlDbType.VarChar, ndChild.InnerText, 5);
                            sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                            sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                            sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                            strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATSRRItem", dtParam);

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (!ErrRtn.ErrNum.Equals(0))
                                break;
                        }

                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (!ErrRtn.ErrNum.Equals(0))
                            break;
                    }

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                }
            }
        }

        /// <summary>
        /// SRR Item Rank 정보 저장
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <param name="strProdType">구분</param>
        /// <param name="strYear">년도</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">SQLTransaction 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveQATSRRItem_Rank(string strPlantCode, string strCustomerCode, string strProdType
            , string strYear, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            using (SQLS sql = new SQLS())
            {
                TransErrRtn ErrRtn = new TransErrRtn();
                try
                {
                    string strErrRtn = string.Empty;


                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProdType", ParameterDirection.Input, SqlDbType.NVarChar, strProdType, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strYear", ParameterDirection.Input, SqlDbType.VarChar, strYear, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_QATSRRItem_Rank", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    return strErrRtn;
                }
                catch (System.Exception ex)
                {
                    //using (QRPLOG.BL.QRPLOG clsLog = new QRPLOG.BL.QRPLOG())
                    //{
                    //    clsLog.Log(this.GetType().FullName, ex);
                    //}
                    ErrRtn.ErrNum = -999;
                    ErrRtn.SystemInnerException = ex.InnerException.ToString();
                    ErrRtn.SystemMessage = ex.Message;
                    ErrRtn.SystemStackTrace = ex.StackTrace;
                    return ErrRtn.mfEncodingErrMessage(ErrRtn);
                }
                finally
                {
                }
            }
        }
    }

}
