﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 다국어관리                                            */
/* 프로그램ID   : clsSYSLAN.cs                                          */
/* 프로그램명   : 다국어관리                                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2012-03-06                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;
using System.Xml;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]

[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true, AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                          Authentication = AuthenticationOption.None,
                                          ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPSYS.BL.SYSLAN
{
    #region SYSCommonLang

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CommonLang")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CommonLang : ServicedComponent
    {
        /// <summary>
        /// Datatable 컬럼 설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("LangID", typeof(string));
                dtRtn.Columns.Add("Lang", typeof(string));
                dtRtn.Columns.Add("LangCh", typeof(string));
                dtRtn.Columns.Add("LangEn", typeof(string));

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 다국어공통용어관리 조회 메소드
        /// </summary>
        /// <param name="strLangText">용어</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSCommonLang(string strLangText, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strLangText", ParameterDirection.Input, SqlDbType.NVarChar, strLangText, 4000);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSCommonLang", dtParam);
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 다국어공통용어관리 조회 메소드(중복체크용)
        /// </summary>
        /// <param name="strLangID">언어ID</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSCommonLang_Check(string strLangID, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strLangID", ParameterDirection.Input, SqlDbType.VarChar, strLangID, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSCommonLang_Check", dtParam);
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 다국어공통용어관리 저장 메소드
        /// </summary>
        /// <param name="dtLangList">저장정보 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveSYSCommonLang(DataTable dtLangList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtLangList.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLangID", ParameterDirection.Input, SqlDbType.VarChar, dtLangList.Rows[i]["LangID"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.NVarChar, dtLangList.Rows[i]["Lang"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strLangCh", ParameterDirection.Input, SqlDbType.NVarChar, dtLangList.Rows[i]["LangCh"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strLangEn", ParameterDirection.Input, SqlDbType.NVarChar, dtLangList.Rows[i]["LangEn"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSCommonLang", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 다국어공통용어관리 저장 메소드
        /// </summary>
        /// <param name="strLangID">언어ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteSYSCommonLang(string strLangID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLangID", ParameterDirection.Input, SqlDbType.VarChar, strLangID, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSCommonLang", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (!ErrRtn.ErrNum.Equals(0))
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    #endregion

    #region SYSMessage

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Message")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Message : ServicedComponent
    {
        /// <summary>
        /// Datatable 컬럼 설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("MessageCode", typeof(string));
                dtRtn.Columns.Add("Message", typeof(string));
                dtRtn.Columns.Add("MessageCh", typeof(string));
                dtRtn.Columns.Add("MessageEn", typeof(string));

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 다국어Message관리 조회 메소드
        /// </summary>
        /// <param name="strMessage">메세지</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSMessage(string strMessage, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strMessage", ParameterDirection.Input, SqlDbType.NVarChar, strMessage, 4000);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMessage", dtParam);
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 다국어Message관리 조회 메소드(중복체크용)
        /// </summary>
        /// <param name="strMessageCode">메시지코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSMessage_Check(string strMessageCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strMessageCode", ParameterDirection.Input, SqlDbType.VarChar, strMessageCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMessage_Check", dtParam);
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 다국어Message관리 저장 메소드
        /// </summary>
        /// <param name="dtMessageList">저장정보 데이터 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveSYSMessage(DataTable dtMessageList, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtMessageList.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strMessageCode", ParameterDirection.Input, SqlDbType.VarChar, dtMessageList.Rows[i]["MessageCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMessage", ParameterDirection.Input, SqlDbType.NVarChar, dtMessageList.Rows[i]["Message"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strMessageCh", ParameterDirection.Input, SqlDbType.NVarChar, dtMessageList.Rows[i]["MessageCh"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strMessageEn", ParameterDirection.Input, SqlDbType.NVarChar, dtMessageList.Rows[i]["MessageEn"].ToString(), 4000);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSMessage", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 다국어Message관리 저장 메소드
        /// </summary>
        /// <param name="strMessageCode">메세지코드</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteSYSMessage(string strMessageCode)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strMessageCode", ParameterDirection.Input, SqlDbType.VarChar, strMessageCode, 10);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSMessage", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (!ErrRtn.ErrNum.Equals(0))
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// Message 다국어 처리를 위한 XML 생성용 데이터테이블 조회 메소드
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSMessageForXML()
        {
            DataTable dtLangXml = new DataTable();

            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                // 프로시저 실행 / return
                return dtLangXml = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMessageForXML", dtParam);
            }
            catch (Exception ex)
            {
                return dtLangXml;
                throw (ex);
            }
            finally
            {
                //디비 종료
                sql.mfDisConnect();
                sql.Dispose();
                dtLangXml.Dispose();
            }
        }
    }

    #endregion

    #region 화면별 용어관리

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ProgramLang")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProgramLang : ServicedComponent
    {
        /// <summary>
        /// 화면별 용어관리 컬럼 정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtPGM = new DataTable();
            try
            {
                dtPGM.Columns.Add("ProgramID", typeof(string));
                dtPGM.Columns.Add("ControlName", typeof(string));
                dtPGM.Columns.Add("ControlKey", typeof(string));
                dtPGM.Columns.Add("LangID", typeof(string));

                return dtPGM;
            }
            catch (Exception ex)
            {
                return dtPGM;
                throw (ex);
            }
            finally
            {
                dtPGM.Dispose();
            }
        }

        /// <summary>
        /// 화면별 용어관리 조회
        /// </summary>
        /// <param name="strProgramID">프로그램ID/param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSProgramLang(string strProgramID, string strLang)
        {
            DataTable dtMultiLangPGM = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.VarChar, strProgramID, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // 프로시저 실행 / return
                return dtMultiLangPGM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSProgramLang", dtParam);
            }
            catch (Exception ex)
            {
                return dtMultiLangPGM;
                throw (ex);
            }
            finally
            {
                //디비 종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMultiLangPGM.Dispose();
            }
        }

        /// <summary>
        /// 화면별 용어관리 저장
        /// </summary>
        /// <param name="dtMultiLangPGM">화면별 용어관리 저장 정보</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveSYSProgramLang(DataTable dtSaveProgLang, DataTable dtDelProgLang, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                // 삭제할정보 있을경우 삭제
                if (dtDelProgLang.Rows.Count > 0)
                {
                    strErrRtn = mfDeleteSYSProgramLang(dtDelProgLang, sql.SqlCon, trans);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                for (int i = 0; i < dtSaveProgLang.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.VarChar, dtSaveProgLang.Rows[i]["ProgramID"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strControlName", ParameterDirection.Input, SqlDbType.VarChar, dtSaveProgLang.Rows[i]["ControlName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strControlKey", ParameterDirection.Input, SqlDbType.VarChar, dtSaveProgLang.Rows[i]["ControlKey"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strLangID", ParameterDirection.Input, SqlDbType.NVarChar, dtSaveProgLang.Rows[i]["LangID"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSProgramLang", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 실패시 롤백처리 
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }
                // Commit
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 화면별 용어관리 행삭제
        /// </summary>
        /// <param name="dtDelProgLang">행삭제 정보</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfDeleteSYSProgramLang(DataTable dtDelProgLang, SqlConnection sqlcon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                for (int i = 0; i < dtDelProgLang.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.VarChar, dtDelProgLang.Rows[i]["ProgramID"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strControlName", ParameterDirection.Input, SqlDbType.VarChar, dtDelProgLang.Rows[i]["ControlName"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strControlKey", ParameterDirection.Input, SqlDbType.VarChar, dtDelProgLang.Rows[i]["ControlKey"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //화면별 다국어설정 행 삭제 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_SYSProgramLang", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 For문 종료
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// 화면별 용어관리 삭제(전체)
        /// </summary>
        /// <param name="strProgramID">프로그램ID</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteSYSProgramLang(string strProgramID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                // 디비 연결
                sql.mfConnect();

                // 트랜젝션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                // 파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.VarChar, strProgramID, 100);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // 프로시저 실행
                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSProgramLang_All", dtParam);

                //Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                //처리결과 정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadSYSProgramLangForXML()
        {
            DataTable dtLangXml = new DataTable();
            
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                // 프로시저 실행 / return
                return dtLangXml = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSProgramLangForXML", dtParam);
            }
            catch (Exception ex)
            {
                return dtLangXml;
                throw (ex);
            }
            finally
            {
                //디비 종료
                sql.mfDisConnect();
                sql.Dispose();
                dtLangXml.Dispose();
            }
        }
    }

    #endregion
}
