﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 프로그램관리                                          */
/* 프로그램ID   : clsSYSPGM.cs                                          */
/* 프로그램명   : 프로그램관리                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-21                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Using 추가
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]

[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true, AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                          Authentication = AuthenticationOption.None,
                                          ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPSYS.BL.SYSPGM
{
    #region 메뉴

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Menu")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]

    public class Menu : ServicedComponent
    {
        private string m_strProgramID;          //프로그램ID
        private string m_strProgramName;        //프로그램명
        private string m_strProgramNameCh;      //프로그램명_중문            
        private string m_strProgramNameEn;      //프로그램명_영문
        private string m_strModuleID;           //모듈ID
        private string m_strUpperProgramID;     //상위프로그램ID
        private string m_strUseFlag;            //사용여부
        private int m_intMemuLevel;             //메뉴레벨
        private int m_intMenuOrder;             //메뉴순서
        private Menu m_clsUpperMenu;            //상위메뉴

        public string ProgramID
        {
            get { return m_strProgramID; }
            set { m_strProgramID = value; }
        }

        public string ProgramName
        {
            get { return m_strProgramName; }
            set { m_strProgramName = value; }
        }

        public string ProgramNameCh
        {
            get { return m_strProgramNameCh; }
            set { m_strProgramNameCh = value; }
        }

        public string ProgramNameEn
        {
            get { return m_strProgramNameEn; }
            set { m_strProgramNameEn = value; }
        }

        public string ModuleID
        {
            get { return m_strModuleID; }
            set { m_strModuleID = value; }
        }

        public string UpperProgramID
        {
            get { return m_strUpperProgramID; }
            set { m_strUpperProgramID = value; }
        }

        public string UseFlag
        {
            get { return m_strUseFlag; }
            set { m_strUseFlag = value; }
        }

        public int MemuLevel
        {
            get { return m_intMemuLevel; }
            set { m_intMemuLevel = value; }
        }

        public int MenuOrder
        {
            get { return m_intMenuOrder; }
            set { m_intMenuOrder = value; }
        }

        public Menu UpperMenu
        {
            get { return m_clsUpperMenu; }
            set { m_clsUpperMenu = value; }
        }

        /// <summary>
        /// 생성자 정의
        /// </summary>
        public Menu()
        {
            m_strProgramID = "";
            m_strProgramName = "";
            m_strProgramNameCh = "";
            m_strProgramNameEn = "";
            m_strModuleID = "";
            m_strUpperProgramID = "";
            m_strUseFlag = "";
            m_intMemuLevel = 0;
            m_intMenuOrder = 0;
        }

        /// <summary>
        /// 모듈 메뉴 읽기
        /// </summary>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadRootMenu(string strLang)
        {
            DataTable dt = new DataTable();
            SQLS Sql = new SQLS();
            //string rtn = "";
            try
            {
                Sql.mfConnect();
                //Table변수를 이용하는 방법//
                DataTable dtParam = Sql.mfSetParamDataTable();
                Sql.mfAddParamDataRow(dtParam, "@Lang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = Sql.mfExecReadStoredProc(Sql.SqlCon, "up_Select_SYSMenu_Root", dtParam);

                dtParam.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                Sql.mfDisConnect();
                dt.Dispose();
                Sql.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadFormVersion(string strFormName, string strFormVersion)
        {
            DataTable dt = new DataTable();
            SQLS Sql = new SQLS();
            try
            {
                Sql.mfConnect();
                DataTable dtParam = Sql.mfSetParamDataTable();
                Sql.mfAddParamDataRow(dtParam, "@FormName", ParameterDirection.Input, SqlDbType.NVarChar, strFormName, 30);
                Sql.mfAddParamDataRow(dtParam, "@FormVersion", ParameterDirection.Input, SqlDbType.NVarChar, strFormVersion, 20);
                dt = Sql.mfExecReadStoredProc(Sql.SqlCon, "up_Select_SYSMenu_Version", dtParam);
                dtParam.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                Sql.mfDisConnect();
                Sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 모듈에 대한 하위메뉴 읽기
        /// </summary>
        /// <param name="strUserID">로그인 유저ID</param>
        /// <param name="strModuleProgramID">모듈 프로그램ID</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadModuleMenu(string strUserID, string strModuleProgramID, string strLang)
        {
            DataTable dt = new DataTable();
            SQLS Sql = new SQLS();
            try
            {
                Sql.mfConnect();
                ////Table변수를 이용하는 방법//
                DataTable dtParam = Sql.mfSetParamDataTable();
                Sql.mfAddParamDataRow(dtParam, "@UserID", ParameterDirection.Input, SqlDbType.NVarChar, strUserID, 20);
                Sql.mfAddParamDataRow(dtParam, "@ModuleProgramID", ParameterDirection.Input, SqlDbType.NVarChar, strModuleProgramID, 100);
                Sql.mfAddParamDataRow(dtParam, "@Lang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = Sql.mfExecReadStoredProc(Sql.SqlCon, "up_Select_SYSMenu_Module", dtParam);
                
                dtParam.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                Sql.mfDisConnect();
                Sql.Dispose();
                dt.Dispose();
            }
        }

        #region 메뉴 정보

        #region STS

        /// <summary>
        /// 메뉴 정보 컬럼셋
        /// </summary>
        /// <returns>메뉴 컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtMenu = new DataTable();
            try
            {
                dtMenu.Columns.Add("ProgramID", typeof(string));
                dtMenu.Columns.Add("ProgramName", typeof(string));
                dtMenu.Columns.Add("ProgramNameCh", typeof(string));
                dtMenu.Columns.Add("ProgramNameEn", typeof(string));
                dtMenu.Columns.Add("ModuleID", typeof(string));
                dtMenu.Columns.Add("UpperProgramID", typeof(string));
                dtMenu.Columns.Add("UseFlag", typeof(string));
                dtMenu.Columns.Add("MenuLevel", typeof(string));
                dtMenu.Columns.Add("MenuOrder", typeof(string));

                return dtMenu;
            }
            catch (Exception ex)
            {
                return dtMenu;
                throw (ex);

            }
            finally
            {
                dtMenu.Dispose();
            }

        }

        /// <summary>
        /// 메뉴정보 순번구하기
        /// </summary>
        /// <param name="strUpperProgramID">상위프로그램ID</param>
        /// <param name="strMenuLevel">메뉴레벨</param>
        /// <returns>순번</returns>
        [AutoComplete]
        public DataTable mfReadMenuOrder(string strUpperProgramID, string strMenuLevel)
        {
            DataTable dtMenuOrder = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                // 파라미터 정보 저장
                DataTable dtPram = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtPram, "@i_strUpperProgramID", ParameterDirection.Input, SqlDbType.NVarChar, strUpperProgramID, 100);
                sql.mfAddParamDataRow(dtPram, "@i_intLevel", ParameterDirection.Input, SqlDbType.Int, strMenuLevel);

                //메뉴정보 순번구하는 프로시저 실행
                dtMenuOrder = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMenuOrder", dtPram);

                //순번정보
                return dtMenuOrder;
            }
            catch (Exception ex)
            {
                return dtMenuOrder;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMenuOrder.Dispose();
            }
        }

        /// <summary>
        /// 메뉴정보 조회
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns>메뉴정보</returns>
        [AutoComplete]
        public DataTable mfReadMenu(string strLang)
        {
            DataTable dtMenu = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                // 파라미터값 저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                
                // 메뉴정보 조회 프로시저 실행 //
                dtMenu = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMenu", dtParam);

                // 메뉴정보 리턴 //
                return dtMenu;

            }
            catch (Exception ex)
            {

                return dtMenu;
                throw (ex);

            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMenu.Dispose();
            }

        }

        /// <summary>
        /// 메뉴정보 상위프로그램 조회
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <param name="strUpperProgramID">상위프로그램명(공백으로 보낼 시 메뉴정보를 받음)</param>
        /// <returns>상위메뉴정보</returns>
        [AutoComplete]
        public DataTable mfReadMenu(string strProgramID,string strLang )
        {
            DataTable dtMeni = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                // 파라미터값 저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, strProgramID, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //상위 프로그램 조회 프로시저 실행/ /
                dtMeni = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMenuUpperProgram", dtParam);

                // 메뉴정보 리턴 //
                return dtMeni;

            }
            catch (Exception ex)
            {
                return dtMeni;
                throw (ex);

            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMeni.Dispose();
            }
        }


        /// <summary>
        /// 메뉴정보콤보
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns>메뉴정보콤보 정보</returns>
        [AutoComplete]
        public DataTable mfReadMenuCombo(string strLang)
        {
            DataTable dtMenuCombo = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                // 디비연결 
                sql.mfConnect();

                //파라미터정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);

                //메뉴정보 콤보 프로시저 실행
                dtMenuCombo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMenuCombo", dtParam);

                // 메뉴 정보 리턴
                return dtMenuCombo;
            }
            catch (Exception ex)
            {
                return dtMenuCombo;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMenuCombo.Dispose();
            }
        }


        /// <summary>
        /// 메뉴 등록
        /// </summary>
        /// <param name="dtMenu">등록할 메뉴 정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리정보</returns>
        [AutoComplete(false)]
        public string mfSaveMenu(DataTable dtMenu,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn ="";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();
                //트랜젝션 시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                //파라미터 정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam,"@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar,dtMenu.Rows[0]["ProgramID"].ToString(),100);          //프로그램ID
                sql.mfAddParamDataRow(dtParam,"@i_strProgramName", ParameterDirection.Input, SqlDbType.NVarChar,dtMenu.Rows[0]["ProgramName"].ToString(),100);      //프로그램명 
                sql.mfAddParamDataRow(dtParam,"@i_strProgramNameCh", ParameterDirection.Input, SqlDbType.NVarChar,dtMenu.Rows[0]["ProgramNameCh"].ToString(),100);  //중문
                sql.mfAddParamDataRow(dtParam,"@i_strProgramNameEn", ParameterDirection.Input, SqlDbType.NVarChar,dtMenu.Rows[0]["ProgramNameEn"].ToString(),100);  //영문
                sql.mfAddParamDataRow(dtParam,"@i_strModuleID", ParameterDirection.Input, SqlDbType.NVarChar,dtMenu.Rows[0]["ModuleID"].ToString(),50);             //모듈ID
                sql.mfAddParamDataRow(dtParam,"@i_strUpperProgramID", ParameterDirection.Input, SqlDbType.NVarChar,dtMenu.Rows[0]["UpperProgramID"].ToString(),100);//상위프로그램ID
                sql.mfAddParamDataRow(dtParam,"@i_strUseFlag", ParameterDirection.Input, SqlDbType.VarChar,dtMenu.Rows[0]["UseFlag"].ToString(),1);                 //사용여부
                sql.mfAddParamDataRow(dtParam,"@i_intMenuLevel", ParameterDirection.Input, SqlDbType.Int,dtMenu.Rows[0]["MenuLevel"].ToString());                   //메뉴레벨
                sql.mfAddParamDataRow(dtParam,"@i_intMenuOrder", ParameterDirection.Input, SqlDbType.Int,dtMenu.Rows[0]["MenuOrder"].ToString());                   //메뉴순서

                sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                // 메뉴 저장 프로시저 실행 
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSMenu", dtParam);

                // Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과가 실패시 트랜롤백, 성공시 트랜커밋을 한다.
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                // 처리결과 리턴
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 메뉴정보 삭제
        /// </summary>
        /// <param name="strProgramID">프로그램ID</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteMenu(string strProgramID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // 디비연결
                sql.mfConnect();
                
                //트랜젝션 시작
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();

                //파라미터 정보 저장
                DataTable dtPram = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtPram, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtPram,"@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar,strProgramID,100);

                sql.mfAddParamDataRow(dtPram,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //메뉴정보 삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSMenu", dtPram);

                //Decoding 
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                // 처리 실패시 트랜롤백, 성공시 트랜커밋 처리를 한다.
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                //처리결과정보를 리턴 시킨다
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        #endregion

        #region PSTS

        /// <summary>
        /// 메뉴정보 상세조회
        /// </summary>
        /// <param name="strProgramID">프로그램ID</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMenu_Detail(string strProgramID, string strLang)
        {
            DataTable dtMeni = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                // 파라미터값 저장 //
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, strProgramID, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //상위 프로그램 조회 프로시저 실행/ /
                dtMeni = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMenu_Detail", dtParam);

                // 메뉴정보 리턴 //
                return dtMeni;

            }
            catch (Exception ex)
            {
                return dtMeni;
                throw (ex);

            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMeni.Dispose();
            }
        }

        #endregion

        #endregion


    }

    #endregion

    #region 공통 코드

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CommonCode")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CommonCode : ServicedComponent
    {

        /// <summary>
        /// 시스템 공통코드 컬럼정보
        /// </summary>
        /// <returns>시스템 공통코드 컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtCommonCode = new DataTable();
            try
            {
                //컬럼설정
                dtCommonCode.Columns.Add("ComGubunCode", typeof(string));
                dtCommonCode.Columns.Add("ComGubunName", typeof(string));
                dtCommonCode.Columns.Add("ComGubunNameCh", typeof(string));
                dtCommonCode.Columns.Add("ComGubunNameEn", typeof(string));
                dtCommonCode.Columns.Add("ComCode", typeof(string));
                dtCommonCode.Columns.Add("ComCodeName", typeof(string));
                dtCommonCode.Columns.Add("ComCodeNameCh", typeof(string));
                dtCommonCode.Columns.Add("ComCodeNameEn", typeof(string));
                dtCommonCode.Columns.Add("DisplaySeq", typeof(string));
                dtCommonCode.Columns.Add("UseFlag", typeof(string));

                //컬럼정보 리턴
                return dtCommonCode;
            }
            catch (Exception ex)
            {
                return dtCommonCode;
                throw (ex);
            }
            finally
            {
                dtCommonCode.Dispose();
            }
        }

        /// <summary>
        /// 시스템 공통코드정보 조회용 Method(UI용)
        /// </summary>
        /// <param name="strComGubunCode"> ComGubunCode </param>
        /// <param name="strLang"> Language </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCommonCode(String strComGubunCode, String strLang)
        {
            // 변수설정
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter용 Datatable 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 설정
                sql.mfAddParamDataRow(dtParam, "@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strComGubunCode, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSCommonCode", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadFormVersion(string strFormName, string strFormVersion)
        {
            DataTable dt = new DataTable();
            SQLS Sql = new SQLS();
            try
            {
                Sql.mfConnect();
                DataTable dtParam = Sql.mfSetParamDataTable();
                Sql.mfAddParamDataRow(dtParam, "@FormName", ParameterDirection.Input, SqlDbType.NVarChar, strFormName, 30);
                Sql.mfAddParamDataRow(dtParam, "@FormVersion", ParameterDirection.Input, SqlDbType.NVarChar, strFormVersion, 20);
                dt = Sql.mfExecReadStoredProc(Sql.SqlCon, "up_Select_SYSMenu_Version", dtParam);
                dtParam.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                Sql.mfDisConnect();
                Sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 시스템공통코드 정보 조회(시스템관리 메뉴)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns>시스템공통코드 정보</returns>
        [AutoComplete]
        public DataTable mfReadCommonCode(string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtCommonCode = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //시스템공통코드 조회 프로시저 실행
                dtCommonCode = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSCommonCodeList", dtParam);

                //시스템공통코드 정보 리턴
                return dtCommonCode;
            }
            catch (Exception ex)
            {
                return dtCommonCode;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtCommonCode.Dispose();
            }
        }


        /// <summary>
        /// 시스템공통코드 정보 저장
        /// </summary>
        /// <param name="dtCommonCode"> 시스템공통코드 정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfSaveCommonCode(DataTable dtCommonCode, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //트랜젝션시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtCommonCode.Rows.Count; i++)
                {
                    
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam,"@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar,dtCommonCode.Rows[i]["ComGubunCode"].ToString(),100);        //구분코드
                    sql.mfAddParamDataRow(dtParam,"@i_strComCode", ParameterDirection.Input, SqlDbType.VarChar,dtCommonCode.Rows[i]["ComCode"].ToString(),100);                 //공통코드

                    sql.mfAddParamDataRow(dtParam,"@i_strComGubunName", ParameterDirection.Input, SqlDbType.NVarChar,dtCommonCode.Rows[i]["ComGubunName"].ToString(),200);      //구분명
                    sql.mfAddParamDataRow(dtParam,"@i_strComGubunNameCh", ParameterDirection.Input, SqlDbType.NVarChar,dtCommonCode.Rows[i]["ComGubunNameCh"].ToString(),200);  //구분명 중문
                    sql.mfAddParamDataRow(dtParam,"@i_strComGubunNameEn", ParameterDirection.Input, SqlDbType.NVarChar,dtCommonCode.Rows[i]["ComGubunNameEn"].ToString(),200);  //구분명 영문

                    sql.mfAddParamDataRow(dtParam,"@i_strComCodeName", ParameterDirection.Input, SqlDbType.NVarChar,dtCommonCode.Rows[i]["ComCodeName"].ToString(),200);        //공통코드명
                    sql.mfAddParamDataRow(dtParam,"@i_strComCodeNameCh", ParameterDirection.Input, SqlDbType.NVarChar,dtCommonCode.Rows[i]["ComCodeNameCh"].ToString(),200);    //공통코드명 중문
                    sql.mfAddParamDataRow(dtParam,"@i_strComCodeNameEn", ParameterDirection.Input, SqlDbType.NVarChar,dtCommonCode.Rows[i]["ComCodeNameEn"].ToString(),200);    //공통코드명 영문
                    
                    sql.mfAddParamDataRow(dtParam,"@i_intDisplaySeq", ParameterDirection.Input, SqlDbType.Int,dtCommonCode.Rows[i]["DisplaySeq"].ToString());                   //공통코드 순번
                    sql.mfAddParamDataRow(dtParam,"@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char,dtCommonCode.Rows[i]["UseFlag"].ToString(),1);                      //사용여부

                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //시스템 공통코드 저장 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSCommonCode", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 실패시 롤백 후 for문을 빠져나가고 성공일시 커밋처리를 한다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                    
                }
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }


                //처리결과정보 저장
                return strErrRtn;

            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();

            }
        }


        /// <summary>
        /// 시스템공통코드 삭제
        /// </summary>
        /// <param name="dtCommonCode">시스템공통코드정보</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfDeleteCommonCode(DataTable dtCommonCode)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();
                
                for (int i = 0; i < dtCommonCode.Rows.Count; i++)
                {
                    
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtCommonCode.Rows[i]["ComGubunCode"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strComCode", ParameterDirection.Input, SqlDbType.VarChar, dtCommonCode.Rows[i]["ComCode"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //시스템공통코드 삭제 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSCommonCode", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 롤백처리를 하고 for문종료, 성공시 커밋을한다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    #endregion

    #region 사용자공통코드

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("UserCommonCode")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class UserCommonCode : ServicedComponent
    {

        /// <summary>
        /// 사용자 공통코드 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtUserCommonCode = new DataTable();

            try
            {
                dtUserCommonCode.Columns.Add("ModuleGubun", typeof(string));
                dtUserCommonCode.Columns.Add("ComGubunCode", typeof(string));
                dtUserCommonCode.Columns.Add("ComGubunName", typeof(string));
                dtUserCommonCode.Columns.Add("ComGubunNameCh", typeof(string));
                dtUserCommonCode.Columns.Add("ComGubunNameEn", typeof(string));
                dtUserCommonCode.Columns.Add("ComCode", typeof(string));
                dtUserCommonCode.Columns.Add("ComCodeName", typeof(string));
                dtUserCommonCode.Columns.Add("ComCodeNameCh", typeof(string));
                dtUserCommonCode.Columns.Add("ComCodeNameEn", typeof(string));
                dtUserCommonCode.Columns.Add("DisplaySeq", typeof(string));
                dtUserCommonCode.Columns.Add("UseFlag", typeof(string));
                
                //컬럼정보 리턴
                return dtUserCommonCode;
            }
            catch (Exception ex)
            {
                return dtUserCommonCode;
                throw (ex);
            }
            finally
            {
                dtUserCommonCode.Dispose();
            }
        }


        /// <summary>
        /// 사용자 공통코드 삭제컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDeleteDataInfo()
        {
            DataTable dtUserCommonCode = new DataTable();

            try
            {
                dtUserCommonCode.Columns.Add("ModuleGubun", typeof(string));
                dtUserCommonCode.Columns.Add("ComGubunCode", typeof(string));
                dtUserCommonCode.Columns.Add("ComCode", typeof(string));

                //컬럼정보 리턴
                return dtUserCommonCode;
            }
            catch (Exception ex)
            {
                return dtUserCommonCode;
                throw (ex);
            }
            finally
            {
                dtUserCommonCode.Dispose();
            }
        }

        /// <summary>
        /// 사용자 공통코드 조회용 Method
        /// </summary>
        /// <param name="strModuleGubun"> ModuleGubun </param>
        /// <param name="strComGubunCode"> ComGubunCode </param>
        /// <param name="strLang"> Language </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadUserCommonCode(String strModuleGubun, String strComGubunCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter용 Datatable 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 설정
                sql.mfAddParamDataRow(dtParam, "@i_strModuleGubun", ParameterDirection.Input, SqlDbType.VarChar, strModuleGubun, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strComGubunCode, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUserCommonCode", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
        /// <summary>
        /// 사용자 공통코드 중 계측기 중분류 조회용 Method
        /// </summary>
        /// <param name="strModuleGubun"> ModuleGubun </param>
        /// <param name="strComGubunCode"> ComGubunCode </param>
        /// <param name="strComGubunCode"> ComCode </param>
        /// <param name="strLang"> Language </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadUserCommonCodeMMType(String strModuleGubun, String strComGubunCode, String strComCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter용 Datatable 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 설정
                sql.mfAddParamDataRow(dtParam, "@i_strModuleGubun", ParameterDirection.Input, SqlDbType.VarChar, strModuleGubun, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar, strComGubunCode, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strComCode", ParameterDirection.Input, SqlDbType.VarChar, strComCode, 5);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUserCommonCode_MTTYPE", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 사용자 공통코드 모듈에 따른 구분코드/명 가져오는 Method
        /// </summary>
        /// <param name="strModuleGubun"> 모듈코드 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadUserCommonGubunCode(String strModuleGubun, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strModuleGubun", ParameterDirection.Input, SqlDbType.VarChar, strModuleGubun, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_SYSUserCommonGubunCode", dtParam);

                return dtRtn;
            }
            catch (System.Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// 사용자 공통코드 조회(시스템관리)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns>사용자공통코드 정보(All) </returns>
        [AutoComplete]
        public DataTable mfReadUserCommonCode(string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtUserCommonCode = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //사용자 공통코드 정보 조회 프로시저 실행
                dtUserCommonCode = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUserCommonCodeList", dtParam);

                //사용자 공통코드 정보 리턴
                return dtUserCommonCode;

            }
            catch (Exception ex)
            {
                return dtUserCommonCode;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtUserCommonCode.Dispose();
            }
        }


        /// <summary>
        /// 사용자 공통코드 저장
        /// </summary>
        /// <param name="dtUserCommonCode">사용자 공통코드정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아아디</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfSaveUserCommonCode(DataTable dtUserCommonCode, string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtUserCommonCode.Rows.Count; i++)
                {
                    
                    //파라미터저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    
                    //모듈구분
                    sql.mfAddParamDataRow(dtParam, "@i_strModuleGubun", ParameterDirection.Input, SqlDbType.VarChar, dtUserCommonCode.Rows[i]["ModuleGubun"].ToString(), 20);
                    
                    //구분코드,구분명 , 구분명중문,구분명 영문
                    sql.mfAddParamDataRow(dtParam, "@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtUserCommonCode.Rows[i]["ComGubunCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strComGubunName", ParameterDirection.Input, SqlDbType.NVarChar, dtUserCommonCode.Rows[i]["ComGubunName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strComGubunNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtUserCommonCode.Rows[i]["ComGubunNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strComGubunNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtUserCommonCode.Rows[i]["ComGubunNameEn"].ToString(), 50);

                    //공통코드, 공통코드명,공통코드명 중문,공통코드명 영문
                    sql.mfAddParamDataRow(dtParam, "@i_strComCode", ParameterDirection.Input, SqlDbType.VarChar, dtUserCommonCode.Rows[i]["ComCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strComCodeName", ParameterDirection.Input, SqlDbType.NVarChar, dtUserCommonCode.Rows[i]["ComCodeName"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strComCodeNameCh", ParameterDirection.Input, SqlDbType.NVarChar, dtUserCommonCode.Rows[i]["ComCodeNameCh"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strComCodeNameEn", ParameterDirection.Input, SqlDbType.NVarChar, dtUserCommonCode.Rows[i]["ComCodeNameEn"].ToString(), 50);

                    //공통코드 순번,사용여부
                    sql.mfAddParamDataRow(dtParam, "@i_intDisplaySeq", ParameterDirection.Input, SqlDbType.Int, dtUserCommonCode.Rows[i]["DisplaySeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtUserCommonCode.Rows[i]["UseFlag"].ToString(), 1);
                    
                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);
                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //시스템사용자공통코드정보 저장 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSUserCommonCode", dtParam);

                    //Dncoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과가 실패시 롤백후 for문 종료, 성공시 커밋
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                //처리결과 정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 사용자 공통코드 삭제
        /// </summary>
        /// <param name="dtUserCommonCode">사용자공통코드정보</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteUserCommonCode(DataTable dtUserCommonCode)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtUserCommonCode.Rows.Count; i++)
                {
                    
                    //파라미터저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strModuleGubun", ParameterDirection.Input, SqlDbType.VarChar, dtUserCommonCode.Rows[i]["ModuleGubun"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strComGubunCode", ParameterDirection.Input, SqlDbType.VarChar, dtUserCommonCode.Rows[i]["ComGubunCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strComCode", ParameterDirection.Input, SqlDbType.VarChar, dtUserCommonCode.Rows[i]["ComCode"].ToString(), 5);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //시스템유저공통코드 삭제 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSUserCommonCode", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 롤백후 for문종료 성공시 커밋
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);

            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

    #endregion

    #region 시스템연결정보

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SystemAccessInfo")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SystemAccessInfo : ServicedComponent
    {

        /// <summary>
        /// 시스템연결정보 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtAccessInfo = new DataTable();

            try
            {
                dtAccessInfo.Columns.Add("PlantCode", typeof(string));
                dtAccessInfo.Columns.Add("Code", typeof(string));
                dtAccessInfo.Columns.Add("SystemName", typeof(string));
                dtAccessInfo.Columns.Add("SystemAddressPath", typeof(string));
                dtAccessInfo.Columns.Add("AccessID", typeof(string));
                dtAccessInfo.Columns.Add("AccessPassword", typeof(string));
                dtAccessInfo.Columns.Add("EtcAccessInfo1", typeof(string));
                dtAccessInfo.Columns.Add("EtcAccessInfo2", typeof(string));

                return dtAccessInfo;
            }
            catch (Exception ex)
            {
                return dtAccessInfo;
                throw (ex);
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// 시스템연결정보 상세정보조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSystemAccessInfoDetail(string strPlantCode, string strCode)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter용 Datatable 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 설정
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCode", ParameterDirection.Input, SqlDbType.VarChar, strCode, 10);

                // SP 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSSystemAccessInfoDetail", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 시스템 연결정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>시스템연결정보</returns>
        [AutoComplete]
        public DataTable mfReadSystemAccessInfoList(string strPlantCode, string strLang)
        {
            DataTable dtAccess = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //시스템연결정보 조회 프로시저 실행
                dtAccess = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSSystemAccessInfo", dtParam);

                //시스템연결정보 
                return dtAccess;
            }
            catch (Exception ex)
            {
                return dtAccess;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtAccess.Dispose();
            }
        }

        /// <summary>
        /// 시스템 연결정보 저장
        /// </summary>
        /// <param name="dtSystemAccessInfo">시스템연결정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveSystemAccessInfo(DataTable dtSystemAccessInfo,string strUserIP,string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                //디비저장
                sql.mfConnect();
                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSystemAccessInfo.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam,"@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSystemAccessInfo.Rows[i]["PlantCode"].ToString(), 10);                 //공장
                    sql.mfAddParamDataRow(dtParam, "@i_strCode", ParameterDirection.Input, SqlDbType.VarChar, dtSystemAccessInfo.Rows[i]["Code"].ToString(), 10);                           //코드
                    sql.mfAddParamDataRow(dtParam, "@i_strSystemName", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemAccessInfo.Rows[i]["SystemName"].ToString(), 100);             //시스템명
                    sql.mfAddParamDataRow(dtParam, "@i_strSystemAddressPath", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemAccessInfo.Rows[i]["SystemAddressPath"].ToString(), 2000);//시스템연결정보
                    sql.mfAddParamDataRow(dtParam, "@i_strAccessID", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemAccessInfo.Rows[i]["AccessID"].ToString(), 50);                      //연결ID
                    sql.mfAddParamDataRow(dtParam, "@i_strAccessPassword", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemAccessInfo.Rows[i]["AccessPassword"].ToString(), 50);          //연결Password
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcAccessInfo1", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemAccessInfo.Rows[i]["EtcAccessInfo1"].ToString(), 1000);        //기타연결정보1
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcAccessInfo2", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemAccessInfo.Rows[i]["EtcAccessInfo2"].ToString(), 1000);        //기타연결정보2

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //시스템연결정보 저장 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSSystemAccessInfo", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과에 따라 실패시 롤백후 for문을 종료한다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }

                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    //성공시 트랜커밋을 한다.
                    trans.Commit();
                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 시스템연결정보 삭제
        /// </summary>
        /// <param name="dtSystemAccessInfo">시스템연결정보</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteSystemAccessInfo(DataTable dtSystemAccessInfo)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSystemAccessInfo.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSystemAccessInfo.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCode", ParameterDirection.Input, SqlDbType.VarChar, dtSystemAccessInfo.Rows[i]["Code"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //시스템연결정보 삭제 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSSystemAccessInfo", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리 실패시 롤백후 for문을 종료한다
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                
                //처리성공시 트랜커밋
                if (ErrRtn.ErrNum.Equals(0))
                { 
                     trans.Commit();
                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                //에러 메세지들을 stringd으로 Encoding 하여 리턴시킨다
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


    }

    #endregion

    #region 첨부화일폴더정보

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SystemFilePath")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SystemFilePath : ServicedComponent
    {
        /// <summary>
        /// 첨부화일폴더정보 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfDataSetInfo()
        {
            DataTable dtSystemFilePath = new DataTable();

            try
            {
                dtSystemFilePath.Columns.Add("PlantCode", typeof(string));
                dtSystemFilePath.Columns.Add("Code", typeof(string));
                dtSystemFilePath.Columns.Add("FolderContents", typeof(string));
                dtSystemFilePath.Columns.Add("ServerPath", typeof(string));
                dtSystemFilePath.Columns.Add("FolderName", typeof(string));


                return dtSystemFilePath;
            }
            catch (Exception ex)
            {
                return dtSystemFilePath;
                throw (ex);
            }
            finally
            {
                dtSystemFilePath.Dispose();
            }
        }

        /// <summary>
        /// 시스템연결정보 상세정보조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSystemFilePathDetail(string strPlantCode, string strCode)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter용 Datatable 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 설정
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCode", ParameterDirection.Input, SqlDbType.VarChar, strCode, 10);

                // SP 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSSystemFilePathDetail", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 첨부화일정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>첨부화일 정보조회</returns>
        [AutoComplete]
        public DataTable mfReadSystemFilePathList(string strPlantCode,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSystemFilePath = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //첨부화일폴더정보 조회 프로시저실행
                dtSystemFilePath = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSSystemFilePath", dtParam);

                //첨부화일폴더정보 리턴
                return dtSystemFilePath;
            }
            catch (Exception ex)
            {
                return dtSystemFilePath;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtSystemFilePath.Dispose();
            }
        }


        /// <summary>
        /// 첨부화일폴더정보 저장
        /// </summary>
        /// <param name="dtSystemFilePath">첨부화일폴더정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns> 저장처리결과정보 </returns>
        [AutoComplete(false)]
        public string mfSaveSystemFilePath(DataTable dtSystemFilePath,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //트랜젝션시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSystemFilePath.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSystemFilePath.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCode", ParameterDirection.Input, SqlDbType.VarChar, dtSystemFilePath.Rows[i]["Code"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strFolderContents", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemFilePath.Rows[i]["FolderContents"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strServerPath", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemFilePath.Rows[i]["ServerPath"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strFolderName", ParameterDirection.Input, SqlDbType.NVarChar, dtSystemFilePath.Rows[i]["FolderName"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //첨부화일폴더정보 저장 프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSSystemFilePath", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 실패시 롤백처리후 for문을 빠져나간다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }
                //처리결과 성공시 커밋처리를 한다.
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                //처리결과 정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                //에러메세지를 string 으로 Encoding하여 리턴한다.
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 첨부화일폴더정보 삭제
        /// </summary>
        /// <param name="dtSystemFilePath">첨부화일폴더정보</param>
        /// <returns>삭제처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteSystemFilePath(DataTable dtSystemFilePath)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";

            try
            {
                //디비시작
                sql.mfConnect();
                SqlTransaction trans;

                //트랜젝션시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtSystemFilePath.Rows.Count; i++)
                {
                    //파라미터저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSystemFilePath.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam,"@i_strCode", ParameterDirection.Input, SqlDbType.VarChar,dtSystemFilePath.Rows[i]["Code"].ToString(),10);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //첨부화일 삭제 프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSSystemFilePath", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과에 따라 실패시 롤백후 for문을 종료 성공시 커밋을 한다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                //에러메세지를 string으로 Encoding하여 리턴시킨다.
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

    #endregion

    #region 외부시스템 연결정보

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("ExtInterfaceInfo")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ExtInterfaceInfo : ServicedComponent
    {

        /// <summary>
        /// 외부시스템 연결정보 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDateInfo()
        {
            DataTable dtExtInterfaceInfo = new DataTable();

            try
            {
                dtExtInterfaceInfo.Columns.Add("PlantCode", typeof(string));
                dtExtInterfaceInfo.Columns.Add("Code", typeof(string));
                dtExtInterfaceInfo.Columns.Add("InterfaceName", typeof(string));
                dtExtInterfaceInfo.Columns.Add("ProgramID", typeof(string));
                dtExtInterfaceInfo.Columns.Add("UseFlag", typeof(string));

                //컬럼정보 리턴
                return dtExtInterfaceInfo;
            }
            catch (Exception ex)
            {
                return dtExtInterfaceInfo;
                throw (ex);
            }
            finally
            {
                dtExtInterfaceInfo.Dispose();
            }
        }

        /// <summary>
        /// 외부시스템연동정보 상세정보조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadExtInterfaceInfoDetail(string strPlantCode, string strCode)
        {
            SQLS sql = new SQLS();
            DataTable dt = new DataTable();
            try
            {
                // DB 연결
                sql.mfConnect();

                // Parameter용 Datatable 생성
                DataTable dtParam = sql.mfSetParamDataTable();

                // Parameter 설정
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCode", ParameterDirection.Input, SqlDbType.VarChar, strCode, 10);

                // SP 실행
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSExtInterfaceInfoDetail", dtParam);

                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// 외부시스템연결정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>외부시스템연결정보</returns>
        [AutoComplete]
        public DataTable mfReadExtInterfaceInfoList(string strPlantCode , string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtExtInterfaceInfo = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //외부시스템연결정보 조회 프로시저실행
                dtExtInterfaceInfo = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSExtInterfaceInfo", dtParam);

                //외부시스템연결정보 리턴
                return dtExtInterfaceInfo;
            }
            catch (Exception ex)
            {
                return dtExtInterfaceInfo;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtExtInterfaceInfo.Dispose();
            }
        }


        /// <summary>
        /// 외부시스템연결정보 저장
        /// </summary>
        /// <param name="dtExtInterfaceInfo">외부시스템연결정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveExtInterfaceInfo(DataTable dtExtInterfaceInfo,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = ""; 
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtExtInterfaceInfo.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtExtInterfaceInfo.Rows[i]["PlantCode"].ToString(), 10);             //공장
                    sql.mfAddParamDataRow(dtParam, "@i_strCode", ParameterDirection.Input, SqlDbType.VarChar, dtExtInterfaceInfo.Rows[i]["Code"].ToString(), 10);                       //코드
                    sql.mfAddParamDataRow(dtParam, "@i_strInterfaceName", ParameterDirection.Input, SqlDbType.NVarChar, dtExtInterfaceInfo.Rows[i]["InterfaceName"].ToString(), 1000);  //외부시스템명
                    sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, dtExtInterfaceInfo.Rows[i]["ProgramID"].ToString(), 100);           //연결ProgramID
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtExtInterfaceInfo.Rows[i]["UseFlag"].ToString(), 1);                     //사용여부

                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //외부시스템연결정보 저장프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSExtInterfaceInfo", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //실패시 롤백 후 for문을 종료한다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                //성공시 커밋을 한다.
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 외부시스템연결정보 삭제
        /// </summary>
        /// <param name="dtExtInterfaceInfo">외부시스템연결정보</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteExtInterfaceInfo(DataTable dtExtInterfaceInfo)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtExtInterfaceInfo.Rows.Count; i++)
                {
                    //프로시저파라미터용 데이터테이블 생성
                    DataTable dtParam = sql.mfSetParamDataTable();

                    //프로시저 설정
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtExtInterfaceInfo.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCode", ParameterDirection.Input, SqlDbType.VarChar, dtExtInterfaceInfo.Rows[i]["Code"].ToString(), 10);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //외부시스템연결정보 삭제프로시저실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon,trans,"up_Delete_SYSExtInterfaceInfo",dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //실패시 롤백 후 for문종료
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                //성공시 커밋처리
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }
                //처리결과 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    #endregion

    #region Log-In

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("LogIn")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class LogIn : ServicedComponent
    {
        /// <summary>
        /// 로그인을 위한 공장 콤보박스용 Method
        /// </summary>
        /// <param name="strLang"> Language </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfReadPlantForCombo(String strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASPlantCombo", dtParam);
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASPlantCombo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }


        /// <summary>
        /// 로그인 체크
        /// </summary>
        /// <param name="strLang"> Language </param>
        /// <returns> DataTable </returns>
        [AutoComplete]
        public DataTable mfCheckLogin(string strPlantCode, string strUserID, string strPassword, string strLang)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());
            DataTable dt = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserPassword", ParameterDirection.Input, SqlDbType.NVarChar, strPassword, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                dt = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUserLogin", dtParam);
                //dt = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_MASPlantCombo", dtParam);
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dt.Dispose();
            }
        }
        
        /// <summary>
        /// 사용자 로그인접속이력정보 저장
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLoginUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveUserLoginCount(string strPlantCode, string strLoginUserID, string strUserIP, string strUserID)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;

                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();
                // Parameter용 DataTable 변수
                DataTable dtParam = sql.mfSetParamDataTable();
                // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLoginUserID", ParameterDirection.Input, SqlDbType.VarChar, strLoginUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSUserLoginHist", dtParam);
                // 결과값 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();
                return strErrRtn;

            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 사용자 화면 실행이력정보 저장
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLoginUserID"></param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveUserPGMRunCount(string strPlantCode, string strLoginUserID, string strProgramID,  string strUserIP, string strUserID)
        {
            // 변수들...
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            String strErrRtn = "";
            try
            {
                // DB연결
                sql.mfConnect();
                // Transaction 을 위한 변수 선언
                SqlTransaction trans;

                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();
                // Parameter용 DataTable 변수
                DataTable dtParam = sql.mfSetParamDataTable();
                // 매개변수로 넘어온 DataTable에서 Parameter 값을 추출
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLoginUserID", ParameterDirection.Input, SqlDbType.VarChar, strLoginUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, strProgramID, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // SP 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSUserPGMRunHist", dtParam);
                // 결과값 확인
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();
                return strErrRtn;

            }
            catch (System.Exception ex)
            {
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                //throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }

    #endregion

    #region 부서별 권한정보

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("DeptAuth")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class DeptAuth : ServicedComponent
    {

        /// <summary>
        /// 부서별 권한정보 DataInfo
        /// </summary>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfSetDataInfo()
        {
            DataTable dtDeptAuth = new DataTable();
            try
            {
                dtDeptAuth.Columns.Add("PlantCode", typeof(String));
                dtDeptAuth.Columns.Add("DeptCode", typeof(String));
                dtDeptAuth.Columns.Add("ProgramID", typeof(String));
                dtDeptAuth.Columns.Add("UseFlag", typeof(String));
                dtDeptAuth.Columns.Add("SerachFlag", typeof(String));
                dtDeptAuth.Columns.Add("SaveFlag", typeof(String));
                dtDeptAuth.Columns.Add("DeleteFlag", typeof(String));
                dtDeptAuth.Columns.Add("PrintFlag", typeof(String));
                dtDeptAuth.Columns.Add("ExcelFlag", typeof(String));

                return dtDeptAuth;
            }
            catch (Exception ex)
            {
                return dtDeptAuth;
                throw (ex);
            }
            finally
            {
                dtDeptAuth.Dispose();
            }
        }


        /// <summary>
        /// 부서권한 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDeptCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadDeptAuth(String strPlantCode, String strDeptCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDeptAuth = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar, strDeptCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDeptAuth = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSDeptAuth", dtParam);

                return dtDeptAuth;
            }
            catch (Exception ex)
            {
                return dtDeptAuth;
                throw(ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDeptAuth.Dispose();
            }
        }


        /// <summary>
        /// 부서별권한정보 저장
        /// </summary>
        /// <param name="dtDeptAuth">저장할 권한정보</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveDeptAuth(DataTable dtDeptAuth, string strUserIP, string strUserID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            SQLS sql = new SQLS();

            try
            {
                //디비 오픈
                sql.mfConnect();
                SqlTransaction trans;
                
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtDeptAuth.Rows.Count; i++)
                {
                    // Delete문 주석처리2011.11.29
                    //if (i==0)
                    //{
                    //     strErrRtn = mfDeleteDeptAuth(dtDeptAuth.Rows[i]["PlantCode"].ToString(), dtDeptAuth.Rows[i]["DeptCode"].ToString(), sql.SqlCon, trans);

                    //     ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    //     if (ErrRtn.ErrNum != 0)
                    //     {
                    //         trans.Rollback();
                    //         break;
                    //     }
                    //}

                    //파라미터 저장
                    DataTable dtPram = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtPram, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtPram, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDeptAuth.Rows[i]["PlantCode"].ToString(), 10);  //공장
                    sql.mfAddParamDataRow(dtPram, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar, dtDeptAuth.Rows[i]["DeptCode"].ToString(), 10);    //부서
                    sql.mfAddParamDataRow(dtPram, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, dtDeptAuth.Rows[i]["ProgramID"].ToString(), 100);//프로그램ID
                    sql.mfAddParamDataRow(dtPram, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtDeptAuth.Rows[i]["UseFlag"].ToString(), 1);          //사용여부
                    sql.mfAddParamDataRow(dtPram, "@i_strSearchFlag", ParameterDirection.Input, SqlDbType.Char, dtDeptAuth.Rows[i]["SerachFlag"].ToString(), 1);    //검색
                    sql.mfAddParamDataRow(dtPram, "@i_strSaveFlag", ParameterDirection.Input, SqlDbType.Char, dtDeptAuth.Rows[i]["SaveFlag"].ToString(), 1);        //저장
                    sql.mfAddParamDataRow(dtPram, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.Char, dtDeptAuth.Rows[i]["DeleteFlag"].ToString(), 1);    //삭제
                    sql.mfAddParamDataRow(dtPram, "@i_strPrintFlag", ParameterDirection.Input, SqlDbType.Char, dtDeptAuth.Rows[i]["PrintFlag"].ToString(), 1);      //출력
                    sql.mfAddParamDataRow(dtPram, "@i_strExcelFlag", ParameterDirection.Input, SqlDbType.Char, dtDeptAuth.Rows[i]["ExcelFlag"].ToString(), 1);      //엑셀

                    sql.mfAddParamDataRow(dtPram,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtPram,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtPram,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //부서권한정보  등록 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSDeptAuth", dtPram);

                    //DeCoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과가 실패인 경우 롤백 후 for문을 멈춘다.
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }
                //성공인경우 커밋후 계속 돈다.
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                // 처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 부서별 권한정보 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDeptCode">부서코드</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfDeleteDeptAuth(string strPlantCode, string strDeptCode)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();


            try
            {
                //디비연결
                sql.mfConnect();
                //트랜젝션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                
                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar,strDeptCode,10);
                
                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //부서별권한정보 삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSDeptAuth", dtParam);

                //strErrRtn Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과에 따라 롤백하거나 커밋을 한다.
                if (!ErrRtn.ErrNum.Equals(0))
                    trans.Rollback();
                else
                    trans.Commit();

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 부서별 권한정보 삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDeptCode">부서코드</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfDeleteDeptAuth(string strPlantCode, string strDeptCode,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();


            try
            {
                //파라미터값저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar, strDeptCode, 10);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //부서별권한정보 삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_SYSDeptAuth", dtParam);

                //strErrRtn Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과에 따라 롤백하거나 커밋을 한다.
                if (ErrRtn.ErrNum != 0)
                    return strErrRtn;
                

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

    #endregion

    #region 사용자별 권한정보

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("UserAuth")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class UserAuth : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;
        
        public UserAuth()
        {

        }

        public UserAuth(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 사용자 권한 테이블 컬럼설정
        /// </summary>
        /// <returns>테이블 컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtUserAuth = new DataTable();

            try
            {
                dtUserAuth.Columns.Add("UserID", typeof(string));
                dtUserAuth.Columns.Add("ProgramID", typeof(string));
                dtUserAuth.Columns.Add("UseFlag", typeof(string));
                dtUserAuth.Columns.Add("SearchFlag", typeof(string));
                dtUserAuth.Columns.Add("SaveFlag", typeof(string));
                dtUserAuth.Columns.Add("DeleteFlag", typeof(string));
                dtUserAuth.Columns.Add("PrintFlag", typeof(string));
                dtUserAuth.Columns.Add("ExcelFlag", typeof(string));

                return dtUserAuth;

            }
            catch (Exception ex)
            {
                return dtUserAuth;
                throw (ex);
            }
            finally
            {
                dtUserAuth.Dispose();
            }
        }

        /// <summary>
        /// 사용자별 권한정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strLang">사용언어</param>
        /// <return>사용자별 권한정보</return>
        [AutoComplete]
        public DataTable mfReadUserAuth(string strUserID, string strLang)
        {
            DataTable dtUserAuth = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();
                
                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                //sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);    //사용자아이디
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,5);         //사용언어

                //사용자별 권한정보 조회 프로시저 실행
                dtUserAuth = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUserAuth", dtParam);

                //사용자별 권한정보 리턴
                return dtUserAuth;


            }
            catch (Exception ex)
            {
                return dtUserAuth;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtUserAuth.Dispose();
            }
        }

        /// <summary>
        /// 사용자별 권한정보
        /// </summary>
        /// <param name="dtUserAuth">사용자별메뉴권한정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과</returns>
        [AutoComplete(false)]
        public string mfSaveUserAuth(DataTable dtUserAuth,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();
                
                SqlTransaction trans;

                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtUserAuth.Rows.Count; i++)
                {
                     if (i.Equals(0))
                    {
                         // 저장 전 삭제 실행
                        strErrRtn = mfDeleteUserAuth(dtUserAuth.Rows[i]["UserID"].ToString(), sql.SqlCon, trans);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            trans.Rollback();
                            break;
                        }
                    }

                    //파라미터 정보 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strUser", ParameterDirection.Input, SqlDbType.VarChar, dtUserAuth.Rows[i]["UserID"].ToString(), 20);         //유저ID
                    sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, dtUserAuth.Rows[i]["ProgramID"].ToString(), 100);//프로그램ID
                    sql.mfAddParamDataRow(dtParam, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.Char, dtUserAuth.Rows[i]["UseFlag"].ToString(), 1);         //사용여부
                    sql.mfAddParamDataRow(dtParam, "@i_strSearchFlag", ParameterDirection.Input, SqlDbType.Char, dtUserAuth.Rows[i]["SearchFlag"].ToString(), 1);   //조회
                    sql.mfAddParamDataRow(dtParam, "@i_strSaveFlag", ParameterDirection.Input, SqlDbType.Char, dtUserAuth.Rows[i]["SaveFlag"].ToString(), 1);       //저장
                    sql.mfAddParamDataRow(dtParam, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.Char, dtUserAuth.Rows[i]["DeleteFlag"].ToString(), 1);   //삭제
                    sql.mfAddParamDataRow(dtParam, "@i_strPrintFlag", ParameterDirection.Input, SqlDbType.Char, dtUserAuth.Rows[i]["PrintFlag"].ToString(), 1);     //출력
                    sql.mfAddParamDataRow(dtParam, "@i_strExcelFlag", ParameterDirection.Input, SqlDbType.Char, dtUserAuth.Rows[i]["ExcelFlag"].ToString(), 1);     //엑셀출력

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //사용자메뉴권한정보 저장 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSUserAuth", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과에 따라서 롤백 , 커밋을 한다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 사용자 권한정보 삭제
        /// </summary>
        /// <param name="strUserID">유저 아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteUserAuth(string strUserID)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                //파라미터 정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //사용자메뉴권한정보 삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSUserAuth", dtParam);

                // Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리 결과에 따라 롤백,커밋 처리를 한다.
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();
                
                //처리 결과정보를 리턴한다.
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            { 
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 사용자 권한정보삭제
        /// </summary>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteUserAuth(string strUserID,SqlConnection sqlcon,SqlTransaction trans)
        {
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();

            try
            {
 

                //파라미터 정보 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //사용자메뉴권한정보 삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_SYSUserAuth", dtParam);

                // Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

               
                //처리 결과정보를 리턴한다.
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                
            }
            finally
            {
                
            }
        }


        /// <summary>
        /// 사용자별 루트메뉴 권한정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strLang">사용언어</param>
        /// <return>사용자별 권한정보</return>
        [AutoComplete]
        public DataTable mfReadUserAuth_Root(string strPlantCode, string strUserID, string strLang)
        {
            DataTable dtUserAuth = new DataTable();
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            try
            {
                //디비연결
                sql.mfConnect();  //실행용

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);    //사용자아이디
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);         //사용언어

                //사용자별 권한정보 조회 프로시저 실행
                dtUserAuth = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSAuth_Root", dtParam);   //실행용
                //dtUserAuth = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_SYSAuth_Root", dtParam);   //디버깅용

                //사용자별 권한정보 리턴
                return dtUserAuth;


            }
            catch (Exception ex)
            {
                return dtUserAuth;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtUserAuth.Dispose();
            }
        }


        /// <summary>
        /// 사용자별 모듈메뉴 권한정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strLang">사용언어</param>
        /// <return>사용자별 권한정보</return>
        [AutoComplete]
        public DataTable mfReadUserAuth_Module(string strPlantCode, string strUserID, string strModuleID, string strLang)
        {
            DataTable dtUserAuth = new DataTable();
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            try
            {
                //디비연결
                sql.mfConnect();  //실행용

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);    //사용자아이디
                sql.mfAddParamDataRow(dtParam, "@i_strModuleProgramID", ParameterDirection.Input, SqlDbType.VarChar, strModuleID, 100);    //사용자아이디
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);         //사용언어

                //사용자별 권한정보 조회 프로시저 실행
                dtUserAuth = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSAuth_Module", dtParam); //실행용
                //dtUserAuth = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_SYSAuth_Module", dtParam); //디버깅용

                //사용자별 권한정보 리턴
                return dtUserAuth;


            }
            catch (Exception ex)
            {
                return dtUserAuth;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtUserAuth.Dispose();
            }
        }


        /// <summary>
        /// 사용자별 화면 권한정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strLang">사용언어</param>
        /// <return>사용자별 권한정보</return>
        [AutoComplete]
        public DataTable mfReadUserAuth_Program(string strPlantCode, string strUserID, string strProgramID, string strLang)
        {
            DataTable dtUserAuth = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);    //사용자아이디
                sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.VarChar, strProgramID, 100);    //사용자아이디
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);         //사용언어

                //사용자별 권한정보 조회 프로시저 실행
                dtUserAuth = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSAuth_Program", dtParam);

                //사용자별 권한정보 리턴
                return dtUserAuth;


            }
            catch (Exception ex)
            {
                return dtUserAuth;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtUserAuth.Dispose();
            }
        }
    }

    #endregion

    #region 화면별다국어설정

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MultiLangPGM")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MultiLangPGM : ServicedComponent
    {
        /// <summary>
        /// 화면별 다국어 설정 컬럼 정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtPGM = new DataTable();

            try
            {
                dtPGM.Columns.Add("ProgramID", typeof(string));
                dtPGM.Columns.Add("ContorlName", typeof(string));
                dtPGM.Columns.Add("KeyCode", typeof(string));
                dtPGM.Columns.Add("KORName", typeof(string));
                dtPGM.Columns.Add("CHNName", typeof(string));
                dtPGM.Columns.Add("ENGName", typeof(string));

                return dtPGM;
            }
            catch (Exception ex)
            {
                return dtPGM;
                throw (ex);
            }
            finally
            {
                dtPGM.Dispose();
            }
        }


        /// <summary>
        /// 화면별 다국어 설정 조회
        /// </summary>
        /// <param name="strProgramID">프로그램ID/param>
        /// <param name="strLang">사용언어</param>
        /// <returns>화면별 다국어 설정 정보</returns>
        [AutoComplete]
        public DataTable mfReadMultiLangPGM(string strProgramID ,string strLang)
        {
            DataTable dtMultiLangPGM = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, strProgramID, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.NVarChar, strLang, 3);

                //화면별 다국어 설정 조회 프로시저 실행
                dtMultiLangPGM = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMultiLangPGM", dtParam);

                //화면별 다국어 설정 정보 리턴
                return dtMultiLangPGM;

            }
            catch (Exception ex)
            {
                return dtMultiLangPGM;
                throw (ex);
            }
            finally
            {
                //디비 종료
                sql.mfDisConnect();
                sql.Dispose();
                dtMultiLangPGM.Dispose();
            }
        }

        /// <summary>
        /// 화면별 다국어 설정 저장
        /// </summary>
        /// <param name="dtMultiLangPGM">화면별 다국어 설정 저장 정보</param>
        /// <param name="strUserIP">사용자 아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveMultiLangPGM(DataTable dtMultiLangPGM,DataTable dtMultiLangPGMDel ,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;
                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtMultiLangPGM.Rows.Count; i++)
                {
                    if (i.Equals(0))
                    {
                        //행삭제 정보가 있을 경우 행삭제 매서드 실행
                        if (dtMultiLangPGMDel.Rows.Count > 0)
                        {
                            strErrRtn = mfDeleteMultiLangPGM(dtMultiLangPGMDel, sql.SqlCon, trans);

                            //Decoding
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            //처리 실패시 롤백시키고 for문 종료
                            if (ErrRtn.ErrNum != 0)
                            {
                                trans.Rollback();
                                break;
                            }

                        }
                    }
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, dtMultiLangPGM.Rows[i]["ProgramID"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strControlName", ParameterDirection.Input, SqlDbType.VarChar, dtMultiLangPGM.Rows[i]["ContorlName"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strKeyCode", ParameterDirection.Input, SqlDbType.VarChar, dtMultiLangPGM.Rows[i]["KeyCode"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strKORName", ParameterDirection.Input, SqlDbType.NVarChar, dtMultiLangPGM.Rows[i]["KORName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strCHNName", ParameterDirection.Input, SqlDbType.NVarChar, dtMultiLangPGM.Rows[i]["CHNName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParam, "@i_strENGName", ParameterDirection.Input, SqlDbType.NVarChar, dtMultiLangPGM.Rows[i]["ENGName"].ToString(), 1000);

                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //화면별 다국어 설정 저장프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSMultiLangPGM", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 실패시 롤백처리 
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                        break;
                    }
                }
                //성공시 커밋을한다
                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 화면별 다국어 설정 삭제
        /// </summary>
        /// <param name="strProgramID">프로그램ID</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteMultiLangPGM(string strProgramID)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            string strErrRtn = "";

            try
            {
                //디비 연결
                sql.mfConnect();

                SqlTransaction trans;

                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();
                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                sql.mfAddParamDataRow(dtParam,"@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar,strProgramID,100);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //화면별 다국어설정 삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSMultiLangPGM", dtParam);

                //Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                //처리결과 정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 화면별 다국어 설정 행삭제
        /// </summary>
        /// <param name="dtMultiLangPGM">행삭제 정보</param>
        /// <param name="sqlcon">SqlConnection</param>
        /// <param name="trans">SqlTransaction</param>
        /// <returns>처리결과 정보</returns>
        [AutoComplete(false)]
        public string mfDeleteMultiLangPGM(DataTable dtMultiLangPGM,SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {


                for (int i = 0; i < dtMultiLangPGM.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, dtMultiLangPGM.Rows[i]["ProgramID"].ToString(), 100);//프로그램ID
                    sql.mfAddParamDataRow(dtParam, "@i_srtControlName", ParameterDirection.Input, SqlDbType.VarChar, dtMultiLangPGM.Rows[i]["ContorlName"].ToString(), 200);//컨트롤명
                    sql.mfAddParamDataRow(dtParam, "@i_strKeyCode", ParameterDirection.Input, SqlDbType.VarChar, dtMultiLangPGM.Rows[i]["KeyCode"].ToString(), 200);//키코드

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //화면별 다국어설정 행 삭제 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_SYSMultiLangPGMRow", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 For문 종료
                    if (ErrRtn.ErrNum != 0)
                        break;

                }
                
                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

    #endregion

    #region 인터페이스 에러로그 정보

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("InterfaceErrorLog")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class InterfaceErrorLog : ServicedComponent
    {
        /// <summary>
        /// MES Interface 에러 로그 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDeptCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMESInterfaceErrorLog(String strFromDate, String strToDate, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDeptAuth = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDeptAuth = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMESIFErrorLog", dtParam);

                return dtDeptAuth;
            }
            catch (Exception ex)
            {
                return dtDeptAuth;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDeptAuth.Dispose();
            }
        }

        /// <summary>
        /// MDM Interface 에러 로그 검색
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDeptCode"></param>
        /// <param name="strLang"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadMDMInterfaceErrorLog(String strFromDate, String strToDate, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDeptAuth = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtDeptAuth = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMDMIFErrorLog", dtParam);

                return dtDeptAuth;
            }
            catch (Exception ex)
            {
                return dtDeptAuth;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDeptAuth.Dispose();
            }
        }
    }

    #endregion

    #region 공지사항

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Notice")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Notice : ServicedComponent
    {
        /// <summary>
        /// 공지사항 컬럼정보
        /// </summary>
        /// <returns>컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtNotice = new DataTable();
            try
            {
                dtNotice.Columns.Add("PlantCode", typeof(string));
                dtNotice.Columns.Add("DocCode", typeof(string));
                dtNotice.Columns.Add("Title", typeof(string));
                dtNotice.Columns.Add("Contents", typeof(string));
                dtNotice.Columns.Add("WriteID", typeof(string));
                dtNotice.Columns.Add("WriteDate", typeof(string));

                //컬럼정보
                return dtNotice;
            }
            catch (Exception ex)
            {
                return dtNotice;
                throw (ex);
            }
            finally
            {
                dtNotice.Dispose();
            }
        }


        /// <summary>
        /// 공지사항정보 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>공지사항정보</returns>
        [AutoComplete]
        public DataTable mfReadNotice(string strPlantCode,string strLang)
        {
            DataTable dtNotice = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //공지사항 조회 프로시저 실행
                dtNotice = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSNotice", dtParam);

                //공지사항정보
                return dtNotice;
            }
            catch (Exception ex)
            {
                return dtNotice;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
                dtNotice.Dispose();
            }
        }


        /// <summary>
        /// 공지사항정보 저장
        /// </summary>
        /// <param name="dtNotice">공지사항정보</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveNotice(DataTable dtNotice,string strUserIP,string strUserID)
        {
            SQLS sql = new SQLS();
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            
            try
            {
                //디비연결
                sql.mfConnect();
                SqlTransaction trans;

                //트랜젝션 시작
                trans = sql.SqlCon.BeginTransaction();

                for (int i = 0; i < dtNotice.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtNotice.Rows[i]["PlantCode"].ToString(), 10);   //공장
                    sql.mfAddParamDataRow(dtParam, "@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar, dtNotice.Rows[i]["DocCode"].ToString(), 20);       //코드
                    sql.mfAddParamDataRow(dtParam, "@i_strTitle", ParameterDirection.Input, SqlDbType.NVarChar, dtNotice.Rows[i]["Title"].ToString(), 1000);        //제목
                    sql.mfAddParamDataRow(dtParam, "@i_strContents", ParameterDirection.Input, SqlDbType.NVarChar, dtNotice.Rows[i]["Contents"].ToString(), 4000);  //내용
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteID", ParameterDirection.Input, SqlDbType.VarChar, dtNotice.Rows[i]["WriteID"].ToString(), 20);       //작성자
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtNotice.Rows[i]["WriteDate"].ToString(), 10);   //작성일

                    sql.mfAddParamDataRow(dtParam,"@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar,strUserIP,15);
                    sql.mfAddParamDataRow(dtParam,"@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar,strUserID,20);

                    sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                    //공지사항 저장 프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSNotice", dtParam);

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과에 따라 실패시 롤백후 for문을 종료한다 성공시 커밋
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                {
                    trans.Commit();
                }

                //처리결과 정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 공지사항삭제
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDocCode">코드</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfDeleteNotice(string strPlantCode, string strDocCode)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            try
            {
                //디비연결
                sql.mfConnect();

                //트랜젝션 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar,30);

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDocCode", ParameterDirection.Input, SqlDbType.VarChar,strDocCode,20);

                sql.mfAddParamDataRow(dtParam,"@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar,8000);

                //공지사항 삭제 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSNotice", dtParam);

                //Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 정보가 실패면 롤백 성공시 커밋을 한다.
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();


                //처리결과정보 리턴
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }
    #endregion

    #region MyMenu

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 10, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("MyMenu")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MyMenu : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public MyMenu()
        {

        }

        public MyMenu(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 사용자 권한 테이블 컬럼설정
        /// </summary>
        /// <returns>테이블 컬럼정보</returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtMyMenu = new DataTable();

            try
            {
                dtMyMenu.Columns.Add("UserID", typeof(string));
                dtMyMenu.Columns.Add("ProgramID", typeof(string));

                return dtMyMenu;

            }
            catch (Exception ex)
            {
                return dtMyMenu;
                throw (ex);
            }
            finally
            {
                dtMyMenu.Dispose();
            }
        }

        /// <summary>
        /// MyMenu 조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSMyMenu(String strPlantCode, String strUserID, String strLang)
        {
            SQLS sql = new SQLS();    //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtUser = new DataTable();
            try
            {
                sql.mfConnect();  //실행용

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtUser = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMyMenu", dtParam);    //실행용
                //dtUser = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_SYSMyMenu", dtParam);      //디버깅용

                return dtUser;
            }
            catch (Exception ex)
            {
                return dtUser;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect(); //실행용
                sql.Dispose();
                dtUser.Dispose();
            }
        }

        /// <summary>
        /// MyMenu 저장
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveSYSMyMenu(DataTable dtMyMenu, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();  //실행용
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            string strErrRtn = "";
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect(); //실행용
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();  //실행용
                //trans = m_SqlConnDebug.BeginTransaction();  //디버깅용

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                sql.mfAddParamDataRow(dtParam, "@i_strUser", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_SYSMyMenu", dtParam);    //실행용
                //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Delete_SYSMyMenu", dtParam);      //디버깅용

                // Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }

                for (int i = 0; i < dtMyMenu.Rows.Count; i++)
                {
                    DataTable dtParamDT = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParamDT, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);
                    sql.mfAddParamDataRow(dtParamDT, "@i_strUser", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamDT, "@i_strProgramID", ParameterDirection.Input, SqlDbType.NVarChar, dtMyMenu.Rows[i]["ProgramID"].ToString(), 100);          //프로그램ID
                    sql.mfAddParamDataRow(dtParamDT, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParamDT, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParamDT, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSMyMenu", dtParamDT);    //실행용
                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_SYSMyMenu", dtParamDT);    //디버깅용
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                return strErrRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();   //실행용
                sql.Dispose();
            }
        }
    }
    #endregion
}
