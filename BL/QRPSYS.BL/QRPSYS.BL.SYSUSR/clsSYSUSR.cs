﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 시스템관리                                            */
/* 모듈(분류)명 : 사용자관리                                            */
/* 프로그램ID   : clsMASQUA.cs                                          */
/* 프로그램명   : 사용자관리                                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
//using System.Data.OracleClient;
//using Oracle.DataAccess.Client;

using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPSYS.BL.SYSUSR
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("User")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class User : ServicedComponent
    {
        /// <summary>
        /// User 정보 조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSUser(String strPlantCode, String strUserID, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtUser = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtUser = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUser", dtParam);

                return dtUser;
            }
            catch(Exception ex)
            {
                return dtUser;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtUser.Dispose();
            }
        }


        /// <summary>
        /// 부서별 사용자 정보조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDeptCode">부서코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>부서별 사용자정보</returns>
        [AutoComplete]
        public DataTable mfReadSYSUser_Dept(string strPlantCode, string strDeptCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSYSUserDept = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보조회
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam,"@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar,strDeptCode,10);
                sql.mfAddParamDataRow(dtParam,"@i_strLang", ParameterDirection.Input, SqlDbType.VarChar,strLang,3);

                //부서별사용자정보조회 프로시저 실행
                dtSYSUserDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUser_Dept", dtParam);

                //부서별 사용자정보조회
                return dtSYSUserDept;
            }
            catch (Exception ex)
            {
                return dtSYSUserDept;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                dtSYSUserDept.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// User Popup 조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSUserPopup(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtUser = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtUser = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUserPopup", dtParam);

                return dtUser;
            }
            catch (Exception ex)
            {
                return dtUser;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtUser.Dispose();
            }
        }

        /// <summary>
        /// User 비밀번호변경
        /// </summary>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserPassword">사용자비밀번호</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns>처리결과정보</returns>
        [AutoComplete(false)]
        public string mfSaveSYSUserPassword(string strUserID, string strUserPassword, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //디비연결
                sql.mfConnect();

                //트랜젝션시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                //파라미터 저장
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar);

                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserPassword", ParameterDirection.Input, SqlDbType.VarChar, strUserPassword, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //사용자 비밀번호 변경 프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSUserPassword", dtParam);

                //Decoding
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과에 따라서 롤백 , 커밋을 한다.
                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                //처리결과정보 
                return strErrRtn;


            }
            catch (Exception ex)
            {
                //에러메세지들을 string 형식으로 Encoding하여 리턴시킨다.
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 사용자 정보조회(Like)
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserName">사용자명</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>사용자정보</returns>
        [AutoComplete]
        public DataTable mfReadSYSUser_Like(string strPlantCode, string strUserID,string strUserName, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSYSUser = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보조회
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserName", ParameterDirection.Input, SqlDbType.NVarChar, strUserName, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //사용자정보조회(Like) 프로시저 실행
                dtSYSUser = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUser_Like", dtParam);

                //사용자정보
                return dtSYSUser;
            }
            catch (Exception ex)
            {
                return dtSYSUser;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                dtSYSUser.Dispose();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 부서별 사용자정보 조회(사용자ID,명 Like조회)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDeptCode">부서코드</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserName">사용자명</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>부서별 사용자정보</returns>
        [AutoComplete]
        public DataTable mfReadSYSUser_DeptLike(string strPlantCode, string strDeptCode, string strUserID,string strUserName,string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSYSUserDept = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보조회
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar, strDeptCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserName", ParameterDirection.Input, SqlDbType.NVarChar, strUserName, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //부서별사용자정보조회 프로시저 실행
                dtSYSUserDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUser_DeptLike", dtParam);

                //부서별 사용자정보조회
                return dtSYSUserDept;
            }
            catch (Exception ex)
            {
                return dtSYSUserDept;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                dtSYSUserDept.Dispose();
                sql.Dispose();
            }
        }


        /// <summary>
        /// 부서별 사용자정보 조회(사용자ID,명 Like조회,퇴사자명단도포함)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDeptCode">부서코드</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserName">사용자명</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>부서별 사용자정보</returns>
        [AutoComplete]
        public DataTable mfReadSYSUser_DeptLikeAll(string strPlantCode, string strDeptCode, string strUserID, string strUserName, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSYSUserDept = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보조회
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar, strDeptCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strUserName", ParameterDirection.Input, SqlDbType.VarChar, strUserName, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //부서별사용자정보조회 프로시저 실행
                dtSYSUserDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUser_DeptLikeAll", dtParam);

                //부서별 사용자정보조회
                return dtSYSUserDept;
            }
            catch (Exception ex)
            {
                return dtSYSUserDept;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                dtSYSUserDept.Dispose();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 부서별 사용자 정보조회 ( IN 절 사용)
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strDeptCode">부서코드</param>
        /// <param name="strLang">사용언어</param>
        /// <returns>부서별 사용자정보</returns>
        [AutoComplete]
        public DataTable mfReadSYSUser_InDept(string strPlantCode, string strDeptCode, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtSYSUserDept = new DataTable();

            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터정보조회
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar, strDeptCode, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //부서별사용자정보조회 프로시저 실행
                dtSYSUserDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSUser_InDept", dtParam);

                //부서별 사용자정보조회
                return dtSYSUserDept;
            }
            catch (Exception ex)
            {
                return dtSYSUserDept;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
                dtSYSUserDept.Dispose();
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("Dept")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class Dept : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public Dept()
        {

        }

        public Dept(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }
        /// <summary>
        /// 콤보박스용 Method
        /// </summary>
        /// <param name="strPlantCode"> PlantCode </param>
        /// <param name="strLang"> Language </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSDeptForCombo(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDept = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                dtDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSDeptCombo", dtParam);
                return dtDept;
            }
            catch (Exception ex)
            {
                return dtDept;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDept.Dispose();
            }
        }

        /// <summary>
        /// Popup용 Method
        /// </summary>
        /// <param name="strPlantCode"> PlantCode </param>
        /// <param name="strLang"> Language </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSDeptPopup(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDept = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                dtDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSDeptPopup", dtParam);
                return dtDept;
            }
            catch (Exception ex)
            {
                return dtDept;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDept.Dispose();
            }
        }

        /// <summary>
        /// 조회 Method
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strDeptCode"></param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSYSDept(String strPlantCode, String strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtDept = new DataTable();
            try
            {
                sql.mfConnect();
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 5);
                dtDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSDept", dtParam);
                return dtDept;
            }
            catch (Exception ex)
            {
                return dtDept;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtDept.Dispose();
            }
        }

        /// <summary>
        /// 부서정보Like조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strDeptCode">부서코드</param>
        /// <param name="strDeptName">부서명</param>
        /// <returns>부서정보</returns>
        [AutoComplete]
        public DataTable mfReadSYSDept_Like(string strPlantCode, string strDeptCode, string strDeptName,string strLang)
        {
            SQLS sql = new  SQLS();
            DataTable dtDept = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                DataTable dtparam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtparam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar,strPlantCode,10);
                sql.mfAddParamDataRow(dtparam, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar,strDeptCode,10);
                sql.mfAddParamDataRow(dtparam, "@i_strDeptName", ParameterDirection.Input, SqlDbType.VarChar,strDeptName,50);
                sql.mfAddParamDataRow(dtparam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                //부서정보 Like검색프로시저 실행
                dtDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSDept_Like", dtparam);

                //부서정보
                return dtDept;
            }
            catch (Exception ex)
            {
                return dtDept;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                dtDept.Dispose();
                sql.Dispose();
            }
        }

        /////////// <summary>
        /////////// qrp -> wms i/f test
        /////////// </summary>
        /////////// <returns></returns>
        ////////[autocomplete(false)]
        ////////public string mfsavewmstestdept()
        ////////{
        ////////    string strerrrtn = "";
        ////////    transerrrtn errrtn = new transerrrtn();
        ////////    try
        ////////    {
        ////////        //wms oracle db 연결정보 읽기
        ////////        qrpsys.bl.syspgm.systemaccessinfo clspgm = new qrpsys.bl.syspgm.systemaccessinfo();
        ////////        datatable dtpgm = clspgm.mfreadsystemaccessinfodetail("2110", "s05");
        ////////        qrpdb.oradb oradb = new qrpdb.oradb();
        ////////        oradb.mfsetoradbconnectstring(dtpgm.rows[0]["systemaddresspath"].tostring(), dtpgm.rows[0]["accessid"].tostring(), dtpgm.rows[0]["accesspassword"].tostring());

        ////////        //wms oracle db연결하기
        ////////        oradb.mfconnect();

        ////////        //transaction 시작
        ////////        oracletransaction trans;
        ////////        trans = oradb.sqlcon.begintransaction();

        ////////        //wms 처리 sp 호출
        ////////        datatable dtparam = oradb.mfsetparamdatatable();
        ////////        oradb.mfaddparamdatarow(dtparam, "v_deptcode", parameterdirection.input, oracledbtype.varchar2, "1", 10);
        ////////        oradb.mfaddparamdatarow(dtparam, "v_deptname", parameterdirection.input, oracledbtype.varchar2, "test", 50);
        ////////        oradb.mfaddparamdatarow(dtparam, "v_userip", parameterdirection.input, oracledbtype.varchar2, "qrpip", 15);
        ////////        oradb.mfaddparamdatarow(dtparam, "v_userid", parameterdirection.input, oracledbtype.varchar2, "qrpid", 15);
        ////////        oradb.mfaddparamdatarow(dtparam, "rtn", parameterdirection.output, oracledbtype.int32);

        ////////        strerrrtn = oradb.mfexectransstoredproc(oradb.sqlcon, trans, "up_update_masdept", dtparam);
        ////////        errrtn = errrtn.mfdecodingerrmessage(strerrrtn);

        ////////        //wms 처리결과에 따른 transaction 처리
        ////////        if (errrtn.errnum != 0)
        ////////            trans.rollback();
        ////////        else
        ////////            trans.commit();

        ////////        return strerrrtn;

        ////////    }
        ////////    catch (system.exception ex)
        ////////    {
        ////////        return ex.message; //strerrrtn;
        ////////    }
        ////////    finally
        ////////    {
        ////////    }
        ////////}


        /////// <summary>
        /////// 조회 Method
        /////// </summary>
        /////// <param name="strPlantCode"></param>
        /////// <param name="strDeptCode"></param>
        /////// <returns></returns>
        ////[AutoComplete]
        ////public DataTable mfReadSYSDept(String strPlantCode, String strDeptCode)
        ////{
        ////    SQLS sql = new SQLS();
        ////    DataTable dtDept = new DataTable();
        ////    try
        ////    {
        ////        sql.mfConnect();
        ////        DataTable dtParam = sql.mfSetParamDataTable();
        ////        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        ////        sql.mfAddParamDataRow(dtParam, "@i_strDeptCode", ParameterDirection.Input, SqlDbType.VarChar, strDeptCode, 10);
        ////        dtDept = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSDeptCode", dtParam);
        ////        return dtDept;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        return dtDept;
        ////        throw (ex);
        ////    }
        ////    finally
        ////    {
        ////        sql.mfDisConnect();
        ////    }
        ////}
    }
}
