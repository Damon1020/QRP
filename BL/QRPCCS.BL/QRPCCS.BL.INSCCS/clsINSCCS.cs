﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 품질변경점관리                                        */
/* 프로그램ID   : clsINSCCS.cs                                          */
/* 프로그램명   : CCS검사관리                                           */
/* 작성자       : 윤경희                                                */
/* 작성일자     : 2011-08-31                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Using 추가
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using QRPDB;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPCCS.BL.INSCCS
{
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectReqH")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // [CCS검사 등록] Header 정보
    public class CCSInspectReqH : ServicedComponent // 라이브 모드
    //public class CCSInspectReqH //: ServicedComponent // 디버깅 모드
    {

        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자

        /// </summary>
        public CCSInspectReqH()
        {
        }
        /// <summary>
        /// 디버깅용 생성자

        /// </summary>
        /// <param name="strDBConn"></param>
        public CCSInspectReqH(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ProductCode", typeof(string));
                dtRtn.Columns.Add("WorkProcessCode", typeof(string));
                dtRtn.Columns.Add("NowProcessCode", typeof(string));
                dtRtn.Columns.Add("EquipCode", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(string));
                dtRtn.Columns.Add("MESTFlag", typeof(string));
                dtRtn.Columns.Add("CCSCreateType", typeof(string)); //CCSCreateType
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS 등록 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strProcessGroup">공정그룹</param>
        /// <param name="strReqFromDate">의뢰일From</param>
        /// <param name="strReqToDate">의뢰일To</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strCompleteFlag">완료여부</param>
        /// <param name="strInspectResultFlag">검사결과</param>
        /// <param name="strLang">언어</param>
        /// <param name="strFormName">폼명</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqH_Req(String strPlantCode, String strLotNo, String strEquipCode, String strProcessGroup
                                            , String strReqFromDate, String strReqToDate, String strPackage, string strCustomerCode, string strCompleteFlag
                                            , string strInspectResultFlag, String strLang, string strFormName, string strAreaCode)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();                                           // 라이브 모드
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString()); // 디버깅 모드
            try
            {
                // DB연결
                sql.mfConnect();        // 라이브 모드시 주석 삭제

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessGroup", ParameterDirection.Input, SqlDbType.NVarChar, strProcessGroup, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strReqFromDate", ParameterDirection.Input, SqlDbType.VarChar, strReqFromDate, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqToDate", ParameterDirection.Input, SqlDbType.VarChar, strReqToDate, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, strCompleteFlag, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, strInspectResultFlag, 2);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                sql.mfAddParamDataRow(dtParam, "@i_strFormName", ParameterDirection.Input, SqlDbType.VarChar, strFormName, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strAreaCode", ParameterDirection.Input, SqlDbType.VarChar, strAreaCode, 10);

                // SP실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqH_Req", dtParam);       // 라이브 모드
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSCCSInspectReqH", dtParam);       // 디버깅 모드

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        [AutoComplete]
        public DataTable mfReadFormVersion(string strFormName, string strFormVersion)
        {
            DataTable dt = new DataTable();
            SQLS Sql = new SQLS();
            try
            {
                Sql.mfConnect();
                DataTable dtParam = Sql.mfSetParamDataTable();
                Sql.mfAddParamDataRow(dtParam, "@FormName", ParameterDirection.Input, SqlDbType.NVarChar, strFormName, 30);
                Sql.mfAddParamDataRow(dtParam, "@FormVersion", ParameterDirection.Input, SqlDbType.NVarChar, strFormVersion, 20);
                dt = Sql.mfExecReadStoredProc(Sql.SqlCon, "up_Select_SYSMenu_Version", dtParam);
                dtParam.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                return dt;
                throw (ex);
            }
            finally
            {
                Sql.mfDisConnect();
                Sql.Dispose();
                dt.Dispose();
            }
        }

        /// <summary>
        /// CCS 조회 화면 검색 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strReqFromDate">의뢰일From</param>
        /// <param name="strReqToDate">의뢰일To</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strLang">언어</param>
        /// <param name="strFormName">폼이름</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqH_Search(String strPlantCode, String strLotNo, String strEquipCode, String strWorkProcessCode
                                            , String strReqFromDate, String strReqToDate, String strProductCode
            //, string strCompleteFlag, string strInspectResultFlag
                                            , String strLang, string strFormName)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();                                           // 라이브 모드
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString()); // 디버깅 모드
            try
            {
                // DB연결
                sql.mfConnect();        // 라이브 모드시 주석 삭제

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strWorkProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strWorkProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqFromDate", ParameterDirection.Input, SqlDbType.VarChar, strReqFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqToDate", ParameterDirection.Input, SqlDbType.VarChar, strReqToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);
                sql.mfAddParamDataRow(dtParam, "@i_strFormName", ParameterDirection.Input, SqlDbType.VarChar, strFormName, 20);

                // SP실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqH_Search", dtParam);       // 라이브 모드
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSCCSInspectReqH", dtParam);       // 디버깅 모드

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] 헤더정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqHDetail(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();                                                // 라이브 모드
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());        // 디버깅 모드
            try
            {
                // DB 연결
                sql.mfConnect();

                // 파리미터 설정
                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqHDetail", dtParam);     // 라이브 모드
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSCCSInspectReqHDetail", dtParam);     // 디버깅 모드

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] 헤더정보 저장 Method 
        /// <param name="dtHeader"> 헤더정보 DataTable </param>
        /// <param name="dtLot"> Lot정보 DataTable </param>
        /// <param name="dtItem"> Item 정보 DataTable </param>
        /// <param name="dtMeasure"> DataType가 계량인 검사결과 DataTable </param>
        /// <param name="dtCount"> DataType가 계수인 검사결과 DataTable </param>
        /// <param name="dtOkNg"> DataType가 OK/NG인 검사결과 DataTable </param>
        /// <param name="dtDesc"> DataType가 설명인 검사결과 DataTable </param>
        /// <param name="dtSelect"> DataType가 선택인 검사결과 DataTable </param>
        /// <param name="dtPara">가동조건 datatable</param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        public String mfSaveCCSInsepctReqH(DataTable dtHeader, DataTable dtLot, DataTable dtItem, DataTable dtMeasure, DataTable dtCount,
                                            DataTable dtOkNg, DataTable dtDesc, DataTable dtSelect, DataTable dtPara,
                                            String strUserID, String strUserIP, String strLang, String strFormName)
        {
            SQLS sql = new SQLS();                                         // 라이브 모드
            // SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString()); // 디버깅 모드

            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                String strReqNoCycle = "";
                String strReqSeqCycle = "";

                String strErrRtn = "";
                // 디비연결
                sql.mfConnect();                                  // 라이브 모드시 주석 삭제
                SqlTransaction trans;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();        // 라이브 모드
                    //trans = m_SqlConnDebug.BeginTransaction();      // 디버깅 모드
                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WorkProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strNowProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["NowProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSInspectReqH", dtParam);     // 라이브 모드
                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_INSCCSInspectReqH", dtParam);   // 디버깅 모드

                    // 실행 결과
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // 실패일경우 Rollback
                        trans.Rollback();
                        break;
                    }
                    else
                    {
                        trans.Commit();

                        // OUTPUT 값 저장
                        String strRtnReqNo = ErrRtn.mfGetReturnValue(0);
                        String strRtnReqSeq = ErrRtn.mfGetReturnValue(1);

                        strReqNoCycle = ErrRtn.mfGetReturnValue(0);
                        strReqSeqCycle = ErrRtn.mfGetReturnValue(1);

                        // Lot정보 저장
                        CCSInspectReqLot clsCCSLot = new CCSInspectReqLot();
                        strErrRtn = clsCCSLot.mfSaveCCSInspectReqLot(dtLot, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans); // 라이브 모드
                        //strErrRtn = clsCCSLot.mfSaveCCSInspectReqLot(dtLot, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, m_SqlConnDebug, trans); // 디버깅 모드

                        // 저장 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            //trans.Rollback();
                            break;
                        }

                        // Item 저장
                        if (dtItem.Rows.Count > 0)
                        {
                            CCSInspectReqItem clsCCSItem = new CCSInspectReqItem();
                            strErrRtn = clsCCSItem.mfSaveCCSInspectReqItem(dtItem, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);    // 라이브 모드    
                            //strErrRtn = clsCCSItem.mfSaveCCSInspectReqItem(dtItem, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, m_SqlConnDebug, trans);    // 디버깅 모드

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                //trans.Rollback();
                                break;
                            }
                        }
                        // 데이터 유형에 따른 검사결과 저장 Method 호출
                        // 계량형
                        if (dtMeasure.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultMeasure clsMeasure = new QRPCCS.BL.INSCCS.CCSInspectResultMeasure();
                            strErrRtn = clsMeasure.mfSaveCCSInspectResultMeasure(dtMeasure, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);    // 라이브 모드
                            //strErrRtn = clsMeasure.mfSaveCCSInspectResultMeasure(dtMeasure, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, m_SqlConnDebug, trans);    // 디버깅 모드    
                            // 결과 검사

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                //trans.Rollback();
                                break;
                            }
                        }
                        // 계수형
                        if (dtCount.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultCount clsCount = new QRPCCS.BL.INSCCS.CCSInspectResultCount();
                            strErrRtn = clsCount.mfSaveCCSInspectResultCount(dtCount, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);  // 라이브 모드
                            // strErrRtn = clsCount.mfSaveCCSInspectResultCount(dtCount, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, m_SqlConnDebug, trans); //디버깅 모드
                            // 결과 검사

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                //trans.Rollback();
                                break;
                            }
                        }
                        // OkNg
                        if (dtOkNg.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultOkNg clsOkNg = new QRPCCS.BL.INSCCS.CCSInspectResultOkNg();
                            strErrRtn = clsOkNg.mfSaveCCSInspectResultOkNg(dtOkNg, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);     // 라이브 모드
                            //strErrRtn = clsOkNg.mfSaveCCSInspectResultOkNg(dtOkNg, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, m_SqlConnDebug, trans);     // 디버깅 모드
                            // 결과 검사                            
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                //trans.Rollback();
                                break;
                            }
                        }
                        // 설명
                        if (dtDesc.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultDesc clsDesc = new QRPCCS.BL.INSCCS.CCSInspectResultDesc();
                            strErrRtn = clsDesc.mfSaveCCSInspectResultDesc(dtDesc, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);       //라이브 모드
                            //strErrRtn = clsDesc.mfSaveCCSInspectResultDesc(dtDesc, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, m_SqlConnDebug, trans);         //디버깅 모드

                            // 결과 검사

                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                //trans.Rollback();
                                break;
                            }
                        }
                        // 선택
                        if (dtSelect.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultSelect clsSelect = new QRPCCS.BL.INSCCS.CCSInspectResultSelect();
                            strErrRtn = clsSelect.mfSaveCCSInspectResultSelect(dtSelect, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);     // 라이브 모드
                            //strErrRtn = clsSelect.mfSaveCCSInspectResultSelect(dtSelect, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, m_SqlConnDebug, trans);       // 디버깅 모드
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                //trans.Rollback();
                                break;
                            }
                        }

                        if (dtPara.Rows.Count > 0)
                        {
                            CCSInspectReqPara clsPara = new CCSInspectReqPara();
                            strErrRtn = clsPara.mfSaveCCSInspectReqPara(dtPara, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                            {
                                //trans.Rollback();
                                break;
                            }
                        }
                    }

                    // 현재 Lot의 순회검사 모니터링 여부 체크하여 있으면 모니터링 정보 Skip, 없으면 Insert      
                    if (dtLot.Rows[0]["CompleteFlag"].ToString() == "True" && dtLot.Rows[0]["InspectResultFlag"].ToString() == "OK")
                    {
                        strErrRtn = mfSaveCCSCycleMonitoring(dtLot, strReqNoCycle, strReqSeqCycle, strUserIP, strUserID);
                        // 결과 검사
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                            break;
                    }

                    #region MESIF
                    //// 모든 처리결과가 성공시 MES CCS의뢰 접수 완료요청
                    if (ErrRtn.ErrNum == 0)
                    {
                        //MES에서 요청된 CCS일경우 (CreateType = M 이고 MESTFlag = F)
                        if (dtHeader.Rows[0]["CCSCreateType"].ToString() == "M" && dtHeader.Rows[0]["MESTFlag"].ToString() == "F")
                        {
                            //MES 연결정보 가져오기
                            QRPCCS.BL.INSCCS.CCSMESInterface clsMesCode = new QRPCCS.BL.INSCCS.CCSMESInterface();
                            QRPSYS.BL.SYSPGM.SystemAccessInfo SysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                            //DataTable dtSysAcce = SysAcce.mfReadSystemAccessInfoDetail(dtHeader.Rows[i]["PlantCode"].ToString(), "S04");        //Live Server
                            //DataTable dtSysAcce = SysAcce.mfReadSystemAccessInfoDetail(dtHeader.Rows[i]["PlantCode"].ToString(), "S07");        //Test Server
                            DataTable dtSysAcce = SysAcce.mfReadSystemAccessInfoDetail(dtHeader.Rows[i]["PlantCode"].ToString(), clsMesCode.MesCode);

                            //MES 연결정보를 보내고 그 연결정보로 정보를 보낸다.?
                            QRPMES.IF.Tibrv Tibrv = new QRPMES.IF.Tibrv(dtSysAcce); //실행용
                            //QRPMES.IF.Tibrv Tibrv = new QRPMES.IF.Tibrv("1",dtSysAcce); //디버깅용

                            //설비,LotNo 저장
                            string strEquipCode = dtHeader.Rows[i]["EquipCode"].ToString();
                            string strLotNo = dtLot.Rows[0]["LotNo"].ToString();

                            //CCS의뢰접수완료 요청매서드 실행 하여 처리결과를 받아온다.
                            DataTable dtMES = Tibrv.CCS_ACCEPT_REQ(strFormName, strUserID, strEquipCode, strLotNo, strUserIP);

                            if (dtMES.Rows.Count > 0)
                            {
                                //CCS의뢰접수완료요청처리결과가 성공일 경우
                                if (dtMES.Rows[0][0].ToString() == "0")
                                {
                                    //CCS테이블에 MES결과를 업데이트 한다.

                                    //디비연결을 한다.(MES쪽에서? 종료를하여 다시연결한다)
                                    sql.mfConnect();    //실행용

                                    SqlTransaction transt;
                                    transt = sql.SqlCon.BeginTransaction();     //실행용
                                    //transt = m_SqlConnDebug.BeginTransaction();          //디버깅용

                                    //파라미터 정보 저장
                                    DataTable dtParamt = sql.mfSetParamDataTable();

                                    sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 30);

                                    sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[0]["PlantCode"].ToString(), 10);
                                    sql.mfAddParamDataRow(dtParamt, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[0]["ReqNo"].ToString(), 20);
                                    sql.mfAddParamDataRow(dtParamt, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[0]["ReqSeq"].ToString(), 4);
                                    sql.mfAddParamDataRow(dtParamt, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[0]["ReqLotSeq"].ToString());
                                    sql.mfAddParamDataRow(dtParamt, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[0]["LotNo"].ToString(), 50);
                                    sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                                    sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                                    sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                                    //CCS의뢰 : MES전송결과 프로시저실행
                                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, transt, "up_Update_CCSInspectReqLotMESFlag", dtParamt); //실해용
                                    //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug,transt,"up_Update_CCSInspectReqLotMESFlag",dtParamt); //디버깅용

                                    //Decoding
                                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                                    //처리 실패시 롤백처리, 성공시 커밋처리를 한다.
                                    if (ErrRtn.ErrNum != 0)
                                        transt.Rollback();
                                    else
                                        transt.Commit();

                                }

                                //성공,실패 MES 메세지를 처리한다.
                                ErrRtn.InterfaceResultCode = dtMES.Rows[0][0].ToString();
                                ErrRtn.InterfaceResultMessage = dtMES.Rows[0][1].ToString();
                                //MES
                                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                            }
                        }
                    }
                    #endregion

                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }

        }

        /// <summary>
        /// [CCS검사등록] 순회검사 모니터링 저장
        /// </summary>
        /// <param name="dtLot"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="Sqlcon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        //[AutoComplete(false)]
        public String mfSaveCCSCycleMonitoring(DataTable dtLot, String strReqNo, String strReqSeq, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();                                            // 라이브 모드
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    // 디버깅 모드

            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                String strErrRtn = "";
                // 디비연결
                sql.mfConnect();                                  // 라이브 모드시 주석 삭제
                SqlTransaction trans;

                // Transaction 시작
                trans = sql.SqlCon.BeginTransaction();        // 라이브 모드
                //trans = m_SqlConnDebug.BeginTransaction();      // 디버깅 모드

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    if (dtLot.Rows[i]["CompleteFlag"].ToString() == "True" && dtLot.Rows[i]["InspectResultFlag"].ToString() == "OK")
                    {
                        // Parameter DataTable 설정
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                        sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["ReqLotSeq"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        // SP 실행
                        strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSCycleMonitoring", dtParam); // 라이브 모드
                        //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_INSCCSCycleMonitoring", dtParam);   // 디버깅 모드

                        // 실행결과
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                        {
                            // 실패이면 Rollback
                            trans.Rollback();
                            break;
                        }
                    }
                }
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// CCS의뢰 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="sqlCon">SqlConnection 변수</param>
        /// <param name="trans">트랜잭션 변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteCCSInsepctReqH(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans;
                trans = sql.SqlCon.BeginTransaction();        // 라이브 모드

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_CCSInspectReq", dtParam);

                // 실행결과
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0)
                    // 실패이면 Rollback
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] 헤더정보 저장 Method(Transaction 통합)
        /// <param name="dtHeader"> 헤더정보 DataTable </param>
        /// <param name="dtLot"> Lot정보 DataTable </param>
        /// <param name="dtItem"> Item 정보 DataTable </param>
        /// <param name="dtMeasure"> DataType가 계량인 검사결과 DataTable </param>
        /// <param name="dtCount"> DataType가 계수인 검사결과 DataTable </param>
        /// <param name="dtOkNg"> DataType가 OK/NG인 검사결과 DataTable </param>
        /// <param name="dtDesc"> DataType가 설명인 검사결과 DataTable </param>
        /// <param name="dtSelect"> DataType가 선택인 검사결과 DataTable </param>
        /// <param name="dtPara">가동조건 datatable</param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        public String mfSaveCCSInsepctReqH_PSTS(DataTable dtHeader, DataTable dtLot, DataTable dtItem, DataTable dtMeasure, DataTable dtCount,
                                            DataTable dtOkNg, DataTable dtDesc, DataTable dtSelect, DataTable dtPara,
                                            String strUserID, String strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                String strErrRtn = string.Empty;
                // 디비연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction();;

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WorkProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strNowProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["NowProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSInspectReqH", dtParam);

                    // 실행 결과
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                    else
                    {
                        // OUTPUT 값 저장
                        String strRtnReqNo = ErrRtn.mfGetReturnValue(0);
                        String strRtnReqSeq = ErrRtn.mfGetReturnValue(1);

                        // Lot정보 저장
                        QRPCCS.BL.INSCCS.CCSInspectReqLot clsCCSLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                        strErrRtn = clsCCSLot.mfSaveCCSInspectReqLot_PSTS(dtLot, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);

                        // 저장 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                            break;

                        // Item 저장
                        if (dtItem.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectReqItem clsCCSItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                            strErrRtn = clsCCSItem.mfSaveCCSInspectReqItem_PSTS(dtItem, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // 데이터 유형에 따른 검사결과 저장 Method 호출
                        // 계량형
                        if (dtMeasure.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultMeasure clsMeasure = new QRPCCS.BL.INSCCS.CCSInspectResultMeasure();
                            strErrRtn = clsMeasure.mfSaveCCSInspectResultMeasure_PSTS(dtMeasure, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // 계수형
                        if (dtCount.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultCount clsCount = new QRPCCS.BL.INSCCS.CCSInspectResultCount();
                            strErrRtn = clsCount.mfSaveCCSInspectResultCount_PSTS(dtCount, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // OkNg
                        if (dtOkNg.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultOkNg clsOkNg = new QRPCCS.BL.INSCCS.CCSInspectResultOkNg();
                            strErrRtn = clsOkNg.mfSaveCCSInspectResultOkNg_PSTS(dtOkNg, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // 설명
                        if (dtDesc.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultDesc clsDesc = new QRPCCS.BL.INSCCS.CCSInspectResultDesc();
                            strErrRtn = clsDesc.mfSaveCCSInspectResultDesc_PSTS(dtDesc, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // 선택
                        if (dtSelect.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultSelect clsSelect = new QRPCCS.BL.INSCCS.CCSInspectResultSelect();
                            strErrRtn = clsSelect.mfSaveCCSInspectResultSelect_PSTS(dtSelect, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);     // 라이브 모드
                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }

                        // 가동조건
                        if (dtPara.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                            strErrRtn = clsPara.mfSaveCCSInspectReqPara_PSTS(dtPara, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);
                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }

                        //// 현재 Lot의 순회검사 모니터링 여부 체크하여 있으면 모니터링 정보 Skip, 없으면 Insert      2012-11-19 주석처리 (DB Lock)
                        //if (dtLot.Rows[0]["CompleteFlag"].ToString().ToUpper() == "TRUE" && dtLot.Rows[0]["InspectResultFlag"].ToString() == "OK")
                        //{
                        //    strErrRtn = mfSaveCCSCycleMonitoring_PSTS(dtLot, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);
                        //    // 결과 검사
                        //    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        //    if (ErrRtn.ErrNum != 0)
                        //        break;
                        //}

                        if (ErrRtn.ErrNum == 0)
                        {
                            if (ErrRtn.mfGetReturnValue(0) == null || ErrRtn.mfGetReturnValue(0).Equals(string.Empty))
                            {
                                ErrRtn.mfAddReturnValue(strRtnReqNo);
                                ErrRtn.mfAddReturnValue(strRtnReqSeq);
                                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                            }
                        }

                        ////ErrRtn.InterfaceResultCode = "0";
                        ////ErrRtn.InterfaceResultMessage = string.Empty;

                        #region MESIF 주석처리
                        // MES I/F Log 저장시 새로운 SqlConnection 생성, Log 저장 완료후 CCS에서 사용하던 SqlConnection 이 null 처리됨 ㅡㅡ;;;;
                        /*
                        //// 모든 처리결과가 성공시 MES CCS의뢰 접수 완료요청
                        if (ErrRtn.ErrNum == 0)
                        {

                            #region MES로 부터 CCS자동의뢰된경우 접수완료정보 전달
                            // MES에서 요청된 CCS일경우 (CreateType = M 이고 MESTFlag = F)
                            if (dtHeader.Rows[0]["CCSCreateType"].ToString() == "M" && dtHeader.Rows[0]["MESTFlag"].ToString() == "F")
                            {
                                // 변수 설정
                                string strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                                string strEquipCode = dtHeader.Rows[i]["EquipCode"].ToString();
                                string strLotNo = dtLot.Rows[0]["LotNo"].ToString();
                                string strReqUserID = dtLot.Rows[0]["ReqUserId"].ToString();

                                // CCS의뢰접수완료 요청매서드 실행
                                CCSMESInterface clsMESIF = new CCSMESInterface();
                                DataTable dtIFResult = clsMESIF.mfSend_CCS_ACCEPT_REQ(strPlantCode, strFormName, strReqUserID, strEquipCode, strLotNo, strUserIP, dtSysAcce);

                                if (dtIFResult.Rows.Count > 0)
                                {
                                    //CCS의뢰접수완료요청처리결과가 성공일 경우
                                    if (dtIFResult.Rows[0]["returncode"].ToString() == "0")
                                    {
                                        int intReqLotSeq = Convert.ToInt32(dtLot.Rows[0]["ReqLotSeq"]);
                                        // MES전송결과 저장 메소드 실행
                                        strErrRtn = clsCCSLot.mfSaveCCSInspectReqLot_MESIF_ResultFlag(strPlantCode, strRtnReqNo, strRtnReqSeq, intReqLotSeq
                                                                                , "CCSREQ", "T", strUserID, strUserIP, sql.SqlCon, trans);
                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                        if (!ErrRtn.ErrNum.Equals(0))
                                        {
                                            // MES I/F 결과 InterfaceResult에 저장
                                            ErrRtn.InterfaceResultCode = dtIFResult.Rows[0]["returncode"].ToString();
                                            ErrRtn.InterfaceResultMessage = dtIFResult.Rows[0]["returnmessage"].ToString();
                                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                            break;
                                        }
                                        else
                                        {
                                            // MES I/F 결과 InterfaceResult에 저장
                                            ErrRtn.InterfaceResultCode = dtIFResult.Rows[0]["returncode"].ToString();
                                            ErrRtn.InterfaceResultMessage = dtIFResult.Rows[0]["returnmessage"].ToString();
                                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                        }
                                    }
                                    else
                                    {
                                        // MES I/F 결과 InterfaceResult에 저장
                                        ErrRtn.InterfaceResultCode = dtIFResult.Rows[0]["returncode"].ToString();
                                        ErrRtn.InterfaceResultMessage = dtIFResult.Rows[0]["returnmessage"].ToString();
                                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                    }
                                }
                            }
                            #endregion

                            #region CCS의뢰대상 장비 Lockin/UnLocking

                            // CCS 의뢰의경우 설비가 MOLD 아닌경우 Locking I/F MOLD 공정인경우 PASS
                            // CCS 의뢰현황의 경우 MOLD 공정인경우 불합격인경우 Locking I/F 합격인경우 PASS
                            //                     MOLD 공정이 아닌경우 합격이면 UnLocking I/F 불합격인경우 1차의뢰일때 Locking I/F
                            if (dtMESEQPDown.Rows.Count > 0)
                            {
                                // 변수 설정
                                string strPlantCode = dtMESEQPDown.Rows[0]["PlantCode"].ToString();
                                string strInspectUserID = dtMESEQPDown.Rows[0]["InspectUserID"].ToString();
                                string strLotNo = dtMESEQPDown.Rows[0]["LotNo"].ToString();
                                string strEquipCode = dtMESEQPDown.Rows[0]["EquipCode"].ToString();
                                string strDownCode = dtMESEQPDown.Rows[0]["DownCode"].ToString();
                                string strLockingFlag = dtMESEQPDown.Rows[0]["LockingFlag"].ToString();
                                string strComment = dtMESEQPDown.Rows[0]["Comment"].ToString();
                                string strInterfaceType = dtMESEQPDown.Rows[0]["InterfaceType"].ToString();

                                // CCS 의뢰대상 장비 Locking/Unlocking 메소드 호출
                                CCSMESInterface clsMESIF = new CCSMESInterface();
                                DataTable dtDowninfo = clsMESIF.mfSend_EQP_DOWN4QC(strPlantCode, strFormName, strInspectUserID, strLotNo, strEquipCode, strDownCode
                                                        , strLockingFlag, strComment, strUserID, strUserIP, dtSysAcce);

                                if(sql == null)
                                    mfWindowsLogcheck("7");

                                mfWindowsLogcheck(sql.SqlCon.ConnectionString);

                                mfWindowsLogcheck("8");

                                if (dtDowninfo.Rows.Count > 0)
                                {
                                    if (dtDowninfo.Rows[0]["returncode"].ToString().Equals("0"))
                                    {
                                        int intReqLotSeq = Convert.ToInt32(dtLot.Rows[0]["ReqLotSeq"]);
                                        mfWindowsLogcheck("ReqLotSeq : " + intReqLotSeq.ToString());
                                        //string strFlagValue = string.Empty;
                                        ////if (strInterfaceType.Equals("LOCKING"))
                                        ////    strFlagValue = "T";
                                        ////else if (strInterfaceType.Equals("UNLOCKING"))
                                        ////    strFlagValue = "T";

                                        string strFlagValue = "T";
                                        if(sql == null)
                                            mfWindowsLogcheck("SQL is NULL!!!");
                                        mfWindowsLogcheck("Check SQLConnection");
                                        if(sql.SqlCon == null)
                                            mfWindowsLogcheck("SQLConnection is NULL!!!");
                                        mfWindowsLogcheck("Check Transaction");

                                        // MES전송결과 저장 메소드 실행
                                        strErrRtn = clsCCSLot.mfSaveCCSInspectReqLot_MESIF_ResultFlag(strPlantCode, strRtnReqNo, strRtnReqSeq, intReqLotSeq
                                                                                , strInterfaceType, strFlagValue, strUserID, strUserIP, sql.SqlCon, trans);

                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                        if (!ErrRtn.ErrNum.Equals(0))
                                        {
                                            // MES I/F 결과 InterfaceResult에 저장
                                            ErrRtn.InterfaceResultCode = dtDowninfo.Rows[0]["returncode"].ToString();
                                            ErrRtn.InterfaceResultMessage = dtDowninfo.Rows[0]["returnmessage"].ToString();
                                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                            break;
                                        }
                                        else
                                        {
                                            // MES I/F 결과 InterfaceResult에 저장
                                            ErrRtn.InterfaceResultCode = dtDowninfo.Rows[0]["returncode"].ToString();
                                            ErrRtn.InterfaceResultMessage = dtDowninfo.Rows[0]["returnmessage"].ToString();
                                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                        }
                                    }
                                    else
                                    {
                                        // MES I/F 결과 InterfaceResult에 저장
                                        ErrRtn.InterfaceResultCode = dtDowninfo.Rows[0]["returncode"].ToString();
                                        ErrRtn.InterfaceResultMessage = dtDowninfo.Rows[0]["returnmessage"].ToString();
                                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                        break;
                                    }
                                }
                                else
                                {
                                    // MES I/F 결과 InterfaceResult에 저장
                                    ErrRtn.InterfaceResultCode = "-999";
                                    ErrRtn.InterfaceResultMessage = "MES Interface Connection Error!";
                                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                    break;
                                }
                            }
                            #endregion
                        }
                         * */
                        #endregion
                    }
                }
                // 저장결과에 따라 RollBack 할지 Commit 할지 결정
                //if (ErrRtn.ErrNum.Equals(0) && ErrRtn.InterfaceResultCode.Equals("0"))
                if(ErrRtn.ErrNum == 0)
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] 헤더정보 저장 Method(Transaction 통합)
        /// <param name="dtHeader"> 헤더정보 DataTable </param>
        /// <param name="dtLot"> Lot정보 DataTable </param>
        /// <param name="dtItem"> Item 정보 DataTable </param>
        /// <param name="dtMeasure"> DataType가 계량인 검사결과 DataTable </param>
        /// <param name="dtCount"> DataType가 계수인 검사결과 DataTable </param>
        /// <param name="dtOkNg"> DataType가 OK/NG인 검사결과 DataTable </param>
        /// <param name="dtDesc"> DataType가 설명인 검사결과 DataTable </param>
        /// <param name="dtSelect"> DataType가 선택인 검사결과 DataTable </param>
        /// <param name="dtPara">가동조건 datatable</param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        public String mfSaveCCSInsepctReqH_PSTS(DataTable dtHeader, DataTable dtLot, DataTable dtItem, DataTable dtMeasure, DataTable dtCount,
                                            DataTable dtOkNg, DataTable dtDesc, DataTable dtSelect, DataTable dtPara, DataTable dtFaultType,
                                            String strUserID, String strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                String strErrRtn = string.Empty;
                // 디비연결
                sql.mfConnect();
                // Transaction 시작
                SqlTransaction trans = sql.SqlCon.BeginTransaction(); 

                for (int i = 0; i < dtHeader.Rows.Count; i++)
                {
                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["ProductCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWorkProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["WorkProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strNowProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["NowProcessCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["EquipCode"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtHeader.Rows[i]["EtcDesc"].ToString(), 500);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtHeader.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // 프로시져 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSInspectReqH", dtParam);

                    // 실행 결과
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        break;
                    }
                    else
                    {
                        // OUTPUT 값 저장
                        String strRtnReqNo = ErrRtn.mfGetReturnValue(0);
                        String strRtnReqSeq = ErrRtn.mfGetReturnValue(1);

                        // Lot정보 저장
                        QRPCCS.BL.INSCCS.CCSInspectReqLot clsCCSLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                        strErrRtn = clsCCSLot.mfSaveCCSInspectReqLot_PSTS(dtLot, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);

                        // 저장 결과 확인
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                            break;

                        // Item 저장
                        if (dtItem.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectReqItem clsCCSItem = new QRPCCS.BL.INSCCS.CCSInspectReqItem();
                            strErrRtn = clsCCSItem.mfSaveCCSInspectReqItem_PSTS(dtItem, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        //Item 불량유형 저장
                        if (dtFaultType.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty clsCCSItemFaulty = new QRPCCS.BL.INSCCS.CCSInspectReqItemFaulty();
                            strErrRtn = clsCCSItemFaulty.mfSaveInspectReqItemFaulty(dtFaultType, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }

                        // 데이터 유형에 따른 검사결과 저장 Method 호출
                        // 계량형
                        if (dtMeasure.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultMeasure clsMeasure = new QRPCCS.BL.INSCCS.CCSInspectResultMeasure();
                            strErrRtn = clsMeasure.mfSaveCCSInspectResultMeasure_PSTS(dtMeasure, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // 계수형
                        if (dtCount.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultCount clsCount = new QRPCCS.BL.INSCCS.CCSInspectResultCount();
                            strErrRtn = clsCount.mfSaveCCSInspectResultCount_PSTS(dtCount, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // OkNg
                        if (dtOkNg.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultOkNg clsOkNg = new QRPCCS.BL.INSCCS.CCSInspectResultOkNg();
                            strErrRtn = clsOkNg.mfSaveCCSInspectResultOkNg_PSTS(dtOkNg, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // 설명
                        if (dtDesc.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultDesc clsDesc = new QRPCCS.BL.INSCCS.CCSInspectResultDesc();
                            strErrRtn = clsDesc.mfSaveCCSInspectResultDesc_PSTS(dtDesc, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);

                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }
                        // 선택
                        if (dtSelect.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectResultSelect clsSelect = new QRPCCS.BL.INSCCS.CCSInspectResultSelect();
                            strErrRtn = clsSelect.mfSaveCCSInspectResultSelect_PSTS(dtSelect, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);     // 라이브 모드
                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }

                        // 가동조건
                        if (dtPara.Rows.Count > 0)
                        {
                            QRPCCS.BL.INSCCS.CCSInspectReqPara clsPara = new QRPCCS.BL.INSCCS.CCSInspectReqPara();
                            strErrRtn = clsPara.mfSaveCCSInspectReqPara_PSTS(dtPara, strRtnReqNo, strRtnReqSeq, strUserID, strUserIP, sql.SqlCon, trans);
                            // 저장 결과 확인
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }

                        // 현재 Lot의 순회검사 모니터링 여부 체크하여 있으면 모니터링 정보 Skip, 없으면 Insert      
                        if (dtLot.Rows[0]["CompleteFlag"].ToString().ToUpper() == "TRUE" && dtLot.Rows[0]["InspectResultFlag"].ToString() == "OK")
                        {
                            strErrRtn = mfSaveCCSCycleMonitoring_PSTS(dtLot, strRtnReqNo, strRtnReqSeq, strUserIP, strUserID, sql.SqlCon, trans);
                            // 결과 검사
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum != 0)
                                break;
                        }

                        if (ErrRtn.ErrNum == 0)
                        {
                            if (ErrRtn.mfGetReturnValue(0) == null || ErrRtn.mfGetReturnValue(0).Equals(string.Empty))
                            {
                                ErrRtn.mfAddReturnValue(strRtnReqNo);
                                ErrRtn.mfAddReturnValue(strRtnReqSeq);
                                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                            }
                        }
                        ////ErrRtn.InterfaceResultCode = "0";
                        ////ErrRtn.InterfaceResultMessage = string.Empty;

                        #region MESIF 주석처리
                        // MES I/F Log 저장시 새로운 SqlConnection 생성, Log 저장 완료후 CCS에서 사용하던 SqlConnection 이 null 처리됨 ㅡㅡ;;;;
                        /*
                        //// 모든 처리결과가 성공시 MES CCS의뢰 접수 완료요청
                        if (ErrRtn.ErrNum == 0)
                        {

                            #region MES로 부터 CCS자동의뢰된경우 접수완료정보 전달
                            // MES에서 요청된 CCS일경우 (CreateType = M 이고 MESTFlag = F)
                            if (dtHeader.Rows[0]["CCSCreateType"].ToString() == "M" && dtHeader.Rows[0]["MESTFlag"].ToString() == "F")
                            {
                                // 변수 설정
                                string strPlantCode = dtHeader.Rows[i]["PlantCode"].ToString();
                                string strEquipCode = dtHeader.Rows[i]["EquipCode"].ToString();
                                string strLotNo = dtLot.Rows[0]["LotNo"].ToString();
                                string strReqUserID = dtLot.Rows[0]["ReqUserId"].ToString();

                                // CCS의뢰접수완료 요청매서드 실행
                                CCSMESInterface clsMESIF = new CCSMESInterface();
                                DataTable dtIFResult = clsMESIF.mfSend_CCS_ACCEPT_REQ(strPlantCode, strFormName, strReqUserID, strEquipCode, strLotNo, strUserIP, dtSysAcce);

                                if (dtIFResult.Rows.Count > 0)
                                {
                                    //CCS의뢰접수완료요청처리결과가 성공일 경우
                                    if (dtIFResult.Rows[0]["returncode"].ToString() == "0")
                                    {
                                        int intReqLotSeq = Convert.ToInt32(dtLot.Rows[0]["ReqLotSeq"]);
                                        // MES전송결과 저장 메소드 실행
                                        strErrRtn = clsCCSLot.mfSaveCCSInspectReqLot_MESIF_ResultFlag(strPlantCode, strRtnReqNo, strRtnReqSeq, intReqLotSeq
                                                                                , "CCSREQ", "T", strUserID, strUserIP, sql.SqlCon, trans);
                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                        if (!ErrRtn.ErrNum.Equals(0))
                                        {
                                            // MES I/F 결과 InterfaceResult에 저장
                                            ErrRtn.InterfaceResultCode = dtIFResult.Rows[0]["returncode"].ToString();
                                            ErrRtn.InterfaceResultMessage = dtIFResult.Rows[0]["returnmessage"].ToString();
                                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                            break;
                                        }
                                        else
                                        {
                                            // MES I/F 결과 InterfaceResult에 저장
                                            ErrRtn.InterfaceResultCode = dtIFResult.Rows[0]["returncode"].ToString();
                                            ErrRtn.InterfaceResultMessage = dtIFResult.Rows[0]["returnmessage"].ToString();
                                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                        }
                                    }
                                    else
                                    {
                                        // MES I/F 결과 InterfaceResult에 저장
                                        ErrRtn.InterfaceResultCode = dtIFResult.Rows[0]["returncode"].ToString();
                                        ErrRtn.InterfaceResultMessage = dtIFResult.Rows[0]["returnmessage"].ToString();
                                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                    }
                                }
                            }
                            #endregion

                            #region CCS의뢰대상 장비 Lockin/UnLocking

                            // CCS 의뢰의경우 설비가 MOLD 아닌경우 Locking I/F MOLD 공정인경우 PASS
                            // CCS 의뢰현황의 경우 MOLD 공정인경우 불합격인경우 Locking I/F 합격인경우 PASS
                            //                     MOLD 공정이 아닌경우 합격이면 UnLocking I/F 불합격인경우 1차의뢰일때 Locking I/F
                            if (dtMESEQPDown.Rows.Count > 0)
                            {
                                // 변수 설정
                                string strPlantCode = dtMESEQPDown.Rows[0]["PlantCode"].ToString();
                                string strInspectUserID = dtMESEQPDown.Rows[0]["InspectUserID"].ToString();
                                string strLotNo = dtMESEQPDown.Rows[0]["LotNo"].ToString();
                                string strEquipCode = dtMESEQPDown.Rows[0]["EquipCode"].ToString();
                                string strDownCode = dtMESEQPDown.Rows[0]["DownCode"].ToString();
                                string strLockingFlag = dtMESEQPDown.Rows[0]["LockingFlag"].ToString();
                                string strComment = dtMESEQPDown.Rows[0]["Comment"].ToString();
                                string strInterfaceType = dtMESEQPDown.Rows[0]["InterfaceType"].ToString();

                                // CCS 의뢰대상 장비 Locking/Unlocking 메소드 호출
                                CCSMESInterface clsMESIF = new CCSMESInterface();
                                DataTable dtDowninfo = clsMESIF.mfSend_EQP_DOWN4QC(strPlantCode, strFormName, strInspectUserID, strLotNo, strEquipCode, strDownCode
                                                        , strLockingFlag, strComment, strUserID, strUserIP, dtSysAcce);

                                if(sql == null)
                                    mfWindowsLogcheck("7");

                                mfWindowsLogcheck(sql.SqlCon.ConnectionString);

                                mfWindowsLogcheck("8");

                                if (dtDowninfo.Rows.Count > 0)
                                {
                                    if (dtDowninfo.Rows[0]["returncode"].ToString().Equals("0"))
                                    {
                                        int intReqLotSeq = Convert.ToInt32(dtLot.Rows[0]["ReqLotSeq"]);
                                        mfWindowsLogcheck("ReqLotSeq : " + intReqLotSeq.ToString());
                                        //string strFlagValue = string.Empty;
                                        ////if (strInterfaceType.Equals("LOCKING"))
                                        ////    strFlagValue = "T";
                                        ////else if (strInterfaceType.Equals("UNLOCKING"))
                                        ////    strFlagValue = "T";

                                        string strFlagValue = "T";
                                        if(sql == null)
                                            mfWindowsLogcheck("SQL is NULL!!!");
                                        mfWindowsLogcheck("Check SQLConnection");
                                        if(sql.SqlCon == null)
                                            mfWindowsLogcheck("SQLConnection is NULL!!!");
                                        mfWindowsLogcheck("Check Transaction");

                                        // MES전송결과 저장 메소드 실행
                                        strErrRtn = clsCCSLot.mfSaveCCSInspectReqLot_MESIF_ResultFlag(strPlantCode, strRtnReqNo, strRtnReqSeq, intReqLotSeq
                                                                                , strInterfaceType, strFlagValue, strUserID, strUserIP, sql.SqlCon, trans);

                                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                                        if (!ErrRtn.ErrNum.Equals(0))
                                        {
                                            // MES I/F 결과 InterfaceResult에 저장
                                            ErrRtn.InterfaceResultCode = dtDowninfo.Rows[0]["returncode"].ToString();
                                            ErrRtn.InterfaceResultMessage = dtDowninfo.Rows[0]["returnmessage"].ToString();
                                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                            break;
                                        }
                                        else
                                        {
                                            // MES I/F 결과 InterfaceResult에 저장
                                            ErrRtn.InterfaceResultCode = dtDowninfo.Rows[0]["returncode"].ToString();
                                            ErrRtn.InterfaceResultMessage = dtDowninfo.Rows[0]["returnmessage"].ToString();
                                            strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                        }
                                    }
                                    else
                                    {
                                        // MES I/F 결과 InterfaceResult에 저장
                                        ErrRtn.InterfaceResultCode = dtDowninfo.Rows[0]["returncode"].ToString();
                                        ErrRtn.InterfaceResultMessage = dtDowninfo.Rows[0]["returnmessage"].ToString();
                                        strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                        break;
                                    }
                                }
                                else
                                {
                                    // MES I/F 결과 InterfaceResult에 저장
                                    ErrRtn.InterfaceResultCode = "-999";
                                    ErrRtn.InterfaceResultMessage = "MES Interface Connection Error!";
                                    strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);
                                    break;
                                }
                            }
                            #endregion
                        }
                         * */
                        #endregion
                    }
                }
                // 저장결과에 따라 RollBack 할지 Commit 할지 결정
                //if (ErrRtn.ErrNum.Equals(0) && ErrRtn.InterfaceResultCode.Equals("0"))
                if (ErrRtn.ErrNum == 0)
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] 순회검사 모니터링 저장 PSTS(Transaction 통합)
        /// </summary>
        /// <param name="dtLot"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="Sqlcon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSCycleMonitoring_PSTS(DataTable dtLot, String strReqNo, String strReqSeq, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                
                String strErrRtn = string.Empty;

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    if (dtLot.Rows[i]["CompleteFlag"].ToString().ToUpper() == "TRUE" && dtLot.Rows[i]["InspectResultFlag"].ToString() == "OK")
                    {
                        // Parameter DataTable 설정
                        DataTable dtParam = sql.mfSetParamDataTable();
                        sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PlantCode"].ToString(), 10);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                        sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["ReqLotSeq"].ToString());
                        sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                        sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                        // SP 실행
                        strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSCycleMonitoring", dtParam);

                        // 실행결과
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum != 0)
                            break;
                    }
                }
                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// CCS 의뢰정보 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot 순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteCCSInspectReq_PSTS(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_CCSInspectReq_PSTS", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] 헤더정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqH_Check(String strPlantCode, String strEquipCode, String strProcessCode,String strLotNo, String strReqLotSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();                                                // 라이브 모드
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());        // 디버깅 모드
            try
            {
                // DB 연결
                sql.mfConnect();

                // 파리미터 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);       //공장
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);       //설비
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);   //작업공정
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);               //LotNo
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, strReqLotSeq);               //의뢰차수

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);                  //사용언어

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqH_Check", dtParam);     // 라이브 모드
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSCCSInspectReqH_Check", dtParam);     // 디버깅 모드

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }



        /// <summary>
        /// [CCS검사등록] MES 설비상태관리 && CCS의뢰정보(완료Flag) 조회
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="strEquipCode">설비번호</param>
        /// <param name="strProcessCode">현재공정</param>
        /// <param name="strReqLotSeq">Lot의뢰 차수</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReq_MESDB(String strPlantCode, String strEquipCode, String strProcessCode, String strLotNo, String strReqLotSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();                                                // 라이브 모드
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());        // 디버깅 모드
            try
            {
                // DB 연결
                sql.mfConnect();

                // 파리미터 설정
                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);       //공장
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);       //설비
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);   //작업공정
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 40);               //LotNo
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, strReqLotSeq);               //의뢰차수

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);                  //사용언어

                // SP 실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_CCSInspectReq_MESDB", dtParam);     // 라이브 모드
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_CCSInspectReq_MESDB", dtParam);     // 디버깅 모드

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// CCS 의뢰정보 삭제 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot 순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteCCSInspectReq_Cancel(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_CCSInspectReq_Cancel", dtParam);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectReqLot")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // [CCS검사 등록] Lot 정보    
    public class CCSInspectReqLot : ServicedComponent       // 라이브 모드
    //public class CCSInspectReqLot //: ServicedComponent     // 디버깅 모드
    {

        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자

        /// </summary>
        public CCSInspectReqLot()
        {
        }
        /// <summary>
        /// 디버깅용 생성자

        /// </summary>
        /// <param name="strDBConn"></param>
        public CCSInspectReqLot(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// [CCS검사등록] Lot 데이터 테이블 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(int));
                dtRtn.Columns.Add("LotNo", typeof(string));
                dtRtn.Columns.Add("CCSReqTypeCode", typeof(string));
                dtRtn.Columns.Add("ReqUserID", typeof(string));
                dtRtn.Columns.Add("StepUserID", typeof(string));
                dtRtn.Columns.Add("ReqDate", typeof(string));
                dtRtn.Columns.Add("ReqTime", typeof(string));
                dtRtn.Columns.Add("CauseReason", typeof(string));
                dtRtn.Columns.Add("CorrectAction", typeof(string));
                dtRtn.Columns.Add("CompleteDate", typeof(string));
                dtRtn.Columns.Add("CompleteTime", typeof(string));
                dtRtn.Columns.Add("InspectUserID", typeof(string));
                dtRtn.Columns.Add("InspectFaultTypeCode", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(string));
                dtRtn.Columns.Add("CompleteFlag", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(string));
                dtRtn.Columns.Add("LotReqSeq", typeof(string));
                dtRtn.Columns.Add("LotMonthCount", typeof(int));
                dtRtn.Columns.Add("LotYearCount", typeof(int));
                dtRtn.Columns.Add("CCSCreateType", typeof(string));
                dtRtn.Columns.Add("StackSeq", typeof(string));
                dtRtn.Columns.Add("ChaseAll", typeof(char));
                dtRtn.Columns.Add("Chase1", typeof(char));
                dtRtn.Columns.Add("Chase2", typeof(char));
                dtRtn.Columns.Add("Chase3", typeof(char));
                dtRtn.Columns.Add("Chase4", typeof(char));
                dtRtn.Columns.Add("Chase5", typeof(char));
                dtRtn.Columns.Add("Chase6", typeof(char));
                dtRtn.Columns.Add("ReceiptDate", typeof(string));
                dtRtn.Columns.Add("ReceiptTime", typeof(string));
                dtRtn.Columns.Add("MESLockFlag", typeof(string));
                dtRtn.Columns.Add("MESUnLockFlag", typeof(string));

                dtRtn.Columns.Add("CCSFile", typeof(string));
                dtRtn.Columns.Add("CCSErrorImage", typeof(string));
                dtRtn.Columns.Add("ProgramSource", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] Lot정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqLot(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();

            SQLS sql = new SQLS();                                            // 라이브 모드
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    // 디버깅 모드
            try
            {
                sql.mfConnect();      // 디버깅 모드 시에만 주석처리

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqLot", dtParam);       // 라이브 모드
                //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSCCSInspectReqLot", dtParam);         // 디버깅 모드

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] Lot 정보 저장 Method
        /// </summary>
        /// <param name="dtLot"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="Sqlcon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectReqLot(DataTable dtLot, String strReqNo, String strReqSeq, String strUserID, String strUserIP
                                                , SqlConnection Sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();                                    // 라이브 모드
                //SQLS sql = new SQLS(Sqlcon.ConnectionString.ToString());    // 디버깅 모드
                String strErrRtn = "";

                trans = Sqlcon.BeginTransaction();

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CCSReqTypeCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReqUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqTime", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReqTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseReason", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["CauseReason"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCorrectAction", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["CorrectAction"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CompleteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteTime", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CompleteTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["LotReqSeq"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParam, "@i_intLotMonthCount", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["LotMonthCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intLotYearCount", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["LotYearCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSCreateType", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["CCSCreateType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["StackSeq"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strChaseAll", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["ChaseAll"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase1", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase1"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase2", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase2"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase3", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase3"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase4", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase4"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase5", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase5"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase6", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase6"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReceiptDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptTime", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReceiptTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_INSCCSInspectReqLot", dtParam);

                    // 결과 검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        [AutoComplete]
        public DataTable mfReadCCSInspectReq_ForStackCombo(string strPlantCode, string strProductCode, string strProcessCode, string strReqItemType, string strStackSeq)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.VarChar, strReqItemType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, strStackSeq, 40);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_ISOProcInspectSpecD_ForCCSStackCombo", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        [AutoComplete(false)]
        public string mfCancelCCSInspectReqLot(string strPlantCode, string strUserID, string strEquipCode, string strLotNo,
            string strProductQuantity, string strProductSpecName, string strCustomerProductSpecName, string strProcessCode, string strUserIP)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.VarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strProductQuantity", ParameterDirection.Input, SqlDbType.Decimal, strProductQuantity);
                sql.mfAddParamDataRow(dtParam, "@i_strProductSpecName", ParameterDirection.Input, SqlDbType.VarChar, strProductSpecName, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerProductSpecName", ParameterDirection.Input, SqlDbType.VarChar, strCustomerProductSpecName, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_CCSInspectReqLotCancel_MESIF", dtParam);

                // 결과검사

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum != 0 || !ErrRtn.ErrMessage.Equals(string.Empty))
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        public DataTable mfRtnMESDataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS MES Hold코드 전송 메소드
        /// </summary>
        /// <param name="strFormName">폼명</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strComment">Comment</param>
        /// <param name="strHoldCode">Hold코드</param>
        /// <param name="dtLotList">Lot List DataTable</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="dtStdInfo">기본정보(공장, 의뢰번호, 의뢰번호순번, Lot순번</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCCSInspectReqLot_MESHold(string strFormName, string strUserID, string strComment, string strHoldCode, DataTable dtLotList, string strUserIP, DataTable dtStdInfo)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                //MES서버경로 가져오기
                QRPCCS.BL.INSCCS.CCSMESInterface clsMesCode = new QRPCCS.BL.INSCCS.CCSMESInterface();
                QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S04");        //Live Server
                //DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), "S07");        //Test Server
                DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(dtStdInfo.Rows[0]["PlantCode"].ToString(), clsMesCode.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                DataTable dtTrackInInfo = clsTibrv.LOT_HOLD4QC_REQ(strFormName, strUserID, strComment, strHoldCode, dtLotList, strUserIP);

                //clsTibrv.Dispose();

                // MES I/F 성공시
                if (dtTrackInInfo.Rows[0]["returncode"].ToString().Equals("0"))
                {
                    sql.mfConnect();
                    SqlTransaction trans = sql.SqlCon.BeginTransaction();

                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtStdInfo.Rows[0]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtStdInfo.Rows[0]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_CCSInspectReqLot_MESHold", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Commit();
                    }
                    else
                    {
                        trans.Rollback();
                    }
                }
                ErrRtn.InterfaceResultCode = dtTrackInInfo.Rows[0]["returncode"].ToString();
                ErrRtn.InterfaceResultMessage = dtTrackInInfo.Rows[0]["returnmessage"].ToString();
                strErrRtn = ErrRtn.mfEncodingErrMessage(ErrRtn);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// CCS 결과 MES 전송 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCCSInspectReqLot_MESResult(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                // DB 연결
                sql.mfConnect();
                ////// 트랜잭션 시작
                ////SqlTransaction trans = sql.SqlCon.BeginTransaction();
                string strErrRtn = string.Empty;

                // 파라미터 변수 설정
                DataTable dtparam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtparam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtparam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtparam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtparam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtparam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtparam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_CCSInspectReqLot_Send_MESResult", dtparam);

                // 결과검사
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum.Equals(0))
                {
                    // 성공시 MESTCompleteFlag 'T' Update
                    SQLS sql1 = new SQLS();
                    // DB 연결
                    sql1.mfConnect();
                    // 트랜잭션 시작
                    SqlTransaction trans = sql1.SqlCon.BeginTransaction();

                    dtparam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtparam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtparam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                    sql.mfAddParamDataRow(dtparam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtparam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtparam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                    sql.mfAddParamDataRow(dtparam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtparam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtparam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql1.mfExecTransStoredProc(sql1.SqlCon, trans, "up_Update_CCSInspectReqLot_MESResult_Flag", dtparam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        trans.Rollback();
                    }
                    else
                    {
                        trans.Commit();
                        sql1.mfDisConnect();
                        sql1.Dispose();
                    }
                }
                else
                {
                }

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// CCS 접수시간 저장 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호</param>
        /// <param name="strReqLotSeq">관리번호</param>
        /// <param name="strReceiptDate">접수일</param>
        /// <param name="strReceiptTime">접수시간</param>
        /// <param name="strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete]
        public string mfSaveINSCCSInspectReqLot_ReceiptTime(string strPlantCode, string strReqNo, string strReqSeq, string strReqLotSeq
                                                            , string strReceiptDate, string strReceiptTime, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqLotSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqLotSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, strReceiptDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReceiptTime", ParameterDirection.Input, SqlDbType.VarChar, strReceiptTime, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSInspectReqLot_ReceiptTime", dtParam);

                if (ErrRtn.ErrNum != 0 || !ErrRtn.ErrMessage.Equals(string.Empty))
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// 원인 / 조치사항 업데이트
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strReqNo"></param>
        /// <param name="intReqSeq"></param>
        /// <param name="strReqLotSeq"></param>
        /// <param name="strCauseDesc"></param>
        /// <param name="strCorrectDesc"></param>
        /// <param name="i_strUserIP"></param>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [AutoComplete]
        public string mfSaveINSCCSInspectReqLotDesc(string strPlantCode, string strReqNo, string strReqSeq, string strReqLotSeq, string strCauseDesc, string strCorrectDesc
                                                    , string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strReqLotSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqLotSeq, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strCauseReason", ParameterDirection.Input, SqlDbType.NVarChar, strCauseDesc, 200);
                sql.mfAddParamDataRow(dtParam, "@i_strCorrectAction", ParameterDirection.Input, SqlDbType.NVarChar, strCorrectDesc, 200);
                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSInspectReqLotDesc", dtParam);

                if (ErrRtn.ErrNum != 0 || !ErrRtn.ErrMessage.Equals(string.Empty))
                {
                    trans.Rollback();
                }
                else
                {
                    trans.Commit();
                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] Lot 정보 저장 Method(Transaction 통합)
        /// </summary>
        /// <param name="dtLot"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <param name="strUserID"> 사용자ID </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="Sqlcon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectReqLot_PSTS(DataTable dtLot, String strReqNo, String strReqSeq, String strUserID, String strUserIP
                                                , SqlConnection Sqlcon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            SQLS sql = new SQLS();   
            try
            {
                                                // 라이브 모드
                String strErrRtn = string.Empty;

                for (int i = 0; i < dtLot.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSReqTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CCSReqTypeCode"].ToString(), 5);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReqUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStepUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["StepUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReqDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqTime", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReqTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseReason", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["CauseReason"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCorrectAction", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["CorrectAction"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CompleteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteTime", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CompleteTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectUserID", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strCompleteFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["CompleteFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["EtcDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strLotReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["LotReqSeq"].ToString(), 3);
                    sql.mfAddParamDataRow(dtParam, "@i_intLotMonthCount", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["LotMonthCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intLotYearCount", ParameterDirection.Input, SqlDbType.Int, dtLot.Rows[i]["LotYearCount"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSCreateType", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["CCSCreateType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["StackSeq"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strChaseAll", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["ChaseAll"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase1", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase1"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase2", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase2"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase3", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase3"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase4", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase4"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase5", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase5"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strChase6", ParameterDirection.Input, SqlDbType.Char, dtLot.Rows[i]["Chase6"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptDate", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReceiptDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReceiptTime", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["ReceiptTime"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strMESLockFlag", ParameterDirection.Input, SqlDbType.VarChar, dtLot.Rows[i]["MESLockFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParam, "@i_strCCSFailFile", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["CCSFile"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strCCSErrorImage", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["CCSErrorImage"].ToString(), 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strProgramSource", ParameterDirection.Input, SqlDbType.NVarChar, dtLot.Rows[i]["ProgramSource"].ToString(), 50);

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(Sqlcon, trans, "up_Update_INSCCSInspectReqLot_PSTS", dtParam);

                    // 결과 검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// MES I/F 성공시 Flag Update 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">의뢰차수</param>
        /// <param name="strInterfaceType">I/F 종류</param>
        /// <param name="strFlagValue">Flag값</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCCSInspectReqLot_MESIF_ResultFlag(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq
                                                        , string strInterfaceType, string strFlagValue, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                //파라미터 정보 저장
                DataTable dtParamt = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamt, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParamt, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParamt, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParamt, "@i_strInterfaceType", ParameterDirection.Input, SqlDbType.NVarChar, strInterfaceType, 20);
                sql.mfAddParamDataRow(dtParamt, "@i_strFlagValue", ParameterDirection.Input, SqlDbType.VarChar, strFlagValue, 1);
                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParamt, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                sql.mfAddParamDataRow(dtParamt, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // MES I/F 결과 Flag 저장 SP 호출
                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSInspectReqLot_MESIF_ResultFlag", dtParamt);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                if (ErrRtn.ErrNum == 0)
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사등록] Lot정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqLot_PSTS(String strPlantCode, String strReqNo, String strReqSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();      // 디버깅 모드 시에만 주석처리

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqLot_PSTS", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }



        /// <summary>
        /// CCS 취소여부 저장
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">의뢰순번</param>
        /// <param name="strReqLotSeq">의뢰차수</param>
        /// <param name="strCancelFlag">취소여부</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="strUserID">사용자ID</param>
        /// <returns></returns>
        [AutoComplete]
        public string mfSaveINSCCSInspectReqLot_Cancel(string strPlantCode, string strReqNo, string strReqSeq, string strReqLotSeq
                                                            , string strCancelFlag, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);   // 공장
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);           // 의뢰번호
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);         // 의뢰순번
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, strReqLotSeq);           // 의뢰차수
                sql.mfAddParamDataRow(dtParam, "@i_strCancelFlag", ParameterDirection.Input, SqlDbType.Char, strCancelFlag, 1);     // 취소여부

                sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);     // 사용자IP
                sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);     // 사용자ID

                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);    // ErrorMessage Return

                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSCCSInspectReqLot_Cancel", dtParam);

                if (ErrRtn.ErrNum != 0)
                    trans.Rollback();
                else
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



        /// <summary>
        /// 설비상태 체크
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">의뢰차수</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <param name="strGubun">구분(WB , Other)</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCCSInspectReqLot_StateCheck(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq,
                                                        string strEquipCode, string strGubun, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();

                //파라미터 정보 저장
                DataTable dtParamt = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParamt, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                
                sql.mfAddParamDataRow(dtParamt, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParamt, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParamt, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParamt, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParamt, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.NVarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParamt, "@i_strGubun", ParameterDirection.Input, SqlDbType.VarChar, strGubun, 10);

                sql.mfAddParamDataRow(dtParamt, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParamt, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParamt, "@o_strResult", ParameterDirection.Output, SqlDbType.VarChar, 10);
                sql.mfAddParamDataRow(dtParamt, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                // MES I/F 결과 Flag 저장 SP 호출
                string strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_CCSInspectReqLot_StateCheck", dtParamt);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();
                else
                    trans.Rollback();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }



    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectReqItem")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // [CCS검사 등록] Item 정보
    public class CCSInspectReqItem : ServicedComponent  // 라이브 모드
    //public class CCSInspectReqItem                        // 디버깅 모드
    {
        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자
        /// </summary>
        public CCSInspectReqItem()
        {
        }
        /// <summary>
        /// 디버깅용 생성자
        /// </summary>
        /// <param name="strDBConn"></param>
        public CCSInspectReqItem(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemType", typeof(char));
                dtRtn.Columns.Add("Seq", typeof(Int32));
                dtRtn.Columns.Add("LotNo", typeof(String));
                ////dtRtn.Columns.Add("ProcessCode", typeof(String));
                ////dtRtn.Columns.Add("ProcessSeq", typeof(String));
                ////dtRtn.Columns.Add("InspectGroupCode", typeof(String));
                ////dtRtn.Columns.Add("InspectTypeCode", typeof(String));
                ////dtRtn.Columns.Add("InspectItemCode", typeof(String));
                ////dtRtn.Columns.Add("ProcessInspectFlag", typeof(String));
                ////dtRtn.Columns.Add("ProductInspectFlag", typeof(String));
                ////dtRtn.Columns.Add("QualityInspectFlag", typeof(String));
                ////dtRtn.Columns.Add("InspectCondition", typeof(String));
                ////dtRtn.Columns.Add("Method", typeof(String));
                ////dtRtn.Columns.Add("SpecDesc", typeof(String));
                ////dtRtn.Columns.Add("MeasureToolCode", typeof(String));
                ////dtRtn.Columns.Add("UpperSpec", typeof(Double));
                ////dtRtn.Columns.Add("LowerSpec", typeof(Double));
                ////dtRtn.Columns.Add("SpecRange", typeof(String));
                ////dtRtn.Columns.Add("SampleSize", typeof(Double));
                ////dtRtn.Columns.Add("ProcessInspectSS", typeof(Double));
                dtRtn.Columns.Add("InspectIngFlag", typeof(char));
                dtRtn.Columns.Add("ProductInspectSS", typeof(Double));
                dtRtn.Columns.Add("QualityInspectSS", typeof(Double));
                ////dtRtn.Columns.Add("UnitCode", typeof(String));
                ////dtRtn.Columns.Add("InspectPeriod", typeof(String));
                ////dtRtn.Columns.Add("PeriodUnitCode", typeof(String));
                ////dtRtn.Columns.Add("CompareFlag", typeof(String));
                ////dtRtn.Columns.Add("DataType", typeof(String));
                dtRtn.Columns.Add("EtcDesc", typeof(String));
                dtRtn.Columns.Add("InspectFaultTypeCode", typeof(string));
                dtRtn.Columns.Add("FaultQty", typeof(decimal));
                dtRtn.Columns.Add("InspectResultFlag", typeof(String));
                dtRtn.Columns.Add("Mean", typeof(Double));
                dtRtn.Columns.Add("StdDev", typeof(Double));
                dtRtn.Columns.Add("MaxValue", typeof(Double));
                dtRtn.Columns.Add("MinValue", typeof(Double));
                dtRtn.Columns.Add("DataRange", typeof(Double));
                dtRtn.Columns.Add("Cp", typeof(Double));
                dtRtn.Columns.Add("Cpk", typeof(Double));
                dtRtn.Columns.Add("StdNumber", typeof(String));
                dtRtn.Columns.Add("StdSeq", typeof(String));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        ///// <summary>
        ///// [CCS검사 등록] 아이템 조회 Method
        ///// </summary>
        ///// <param name="strPlantCode"> 공장코드 </param>
        ///// <param name="strReqNo"> 의뢰번호 </param>
        ///// <param name="strReqSeq"> 의뢰순번 </param>
        ///// <returns></returns>
        //[AutoComplete]
        //public DataTable mfReadCCSInspectReqItem(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq, String strLang)
        //{
        //    DataTable dtRtn = new DataTable();

        //    SQLS sql = new SQLS();                                                  // 라이브 모드
        //    //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());        // 디버깅 모드
        //    try
        //    {
        //        sql.mfConnect();      // 디버깅 모드 시에만 주석처리

        //        DataTable dtParam = sql.mfSetParamDataTable();
        //        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        //        sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
        //        sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
        //        sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
        //        sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

        //        dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqItem", dtParam);      // 라이브 모드
        //        //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSCCSInspectReqItem", dtParam);        // 디버깅 모드

        //        return dtRtn;
        //    }
        //    catch (Exception ex)
        //    {
        //        return dtRtn;
        //        throw (ex);
        //    }
        //    finally
        //    {
        //        sql.mfDisConnect();
        //    }
        //}

        /// <summary>
        /// [CCS검사 등록] 아이템 저장 Method
        /// </summary>
        /// <param name="dtItem"> 아이템 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectReqItem(DataTable dtItem, String strReqNo, String strReqSeq, String strUserID, String strUserIP
                                                , SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();                                    // 라이브 모드
                //SQLS sql = new SQLS(sqlCon.ConnectionString.ToString());    // 디버깅 모드
                String strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtItem.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtItem.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["LotNo"].ToString(), 50);
                    //sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["ProcessCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_intProcessSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ProcessSeq"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_strInspectGroupCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectGroupCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strInspectTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectTypeCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectItemCode"].ToString(), 20);
                    //sql.mfAddParamDataRow(dtParam, "@i_strProcessInspectFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["ProcessInspectFlag"].ToString(), 1);
                    //sql.mfAddParamDataRow(dtParam, "@i_strProductItemFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["ProductItemFlag"].ToString(), 1);
                    //sql.mfAddParamDataRow(dtParam, "@i_strQualityItemFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["QualityItemFlag"].ToString(), 1);
                    //sql.mfAddParamDataRow(dtParam, "@i_strInspectCondition", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["InspectCondition"].ToString(), 50);
                    //sql.mfAddParamDataRow(dtParam, "@i_strMethod", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["Method"].ToString(), 50);
                    //sql.mfAddParamDataRow(dtParam, "@i_strSpecDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["SpecDesc"].ToString(), 50);
                    //sql.mfAddParamDataRow(dtParam, "@i_strMeasureToolCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["MeasureToolCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_dblUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["UpperSpec"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_dblLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["LowerSpec"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["SpecRange"].ToString(), 50);
                    //sql.mfAddParamDataRow(dtParam, "@i_dblSampleSize", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["SampleSize"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_dblProcessInspectSS", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["ProcessInspectSS"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectIngFlag", ParameterDirection.Input, SqlDbType.Char, dtItem.Rows[i]["InspectIngFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblProductItemSS", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["ProductInspectSS"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblQualityItemSS", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["QualityInspectSS"].ToString());
                    //sql.mfAddParamDataRow(dtParam, "@i_strUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["UnitCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strInspectPeriod", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectPeriod"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strPeriodUnitCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["PeriodUnitCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strCompareFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["CompareFlag"].ToString(), 1);
                    //sql.mfAddParamDataRow(dtParam, "@i_strDataType", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["DataType"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["EtcDesc"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_dblMean", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Mean"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblStdDev", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["StdDev"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMaxValue", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["MaxValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMinValue", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["MinValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblDataRange", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["DataRange"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCp", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Cp"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCpk", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Cpk"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["StdSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectReqItem", dtParam);

                    // 결과 검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// [CCS검사 등록] 검사값 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqItemValue(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq, string strReqItemType, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();                                                  // 라이브 모드
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());        // 디버깅 모드
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, strReqItemType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqItemValue", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        ///// <summary>
        ///// CCSItem테이블에 저장된 후 : 검사항목에 따른 DataType이 선택일때 콤보박스 유저공통테이블에서 Key, Value 가져오는 Method
        ///// </summary>
        ///// <param name="strPlantCode">공장코드</param>
        ///// <param name="strReqNo">의뢰번호</param>
        ///// <param name="strReqSeq"> 의뢰번호순번 </param>
        ///// <param name="intReqLotSeq">의뢰Lot순번</param>
        ///// <param name="intReqItemSeq"> 검사항목순번 </param>
        ///// <param name="strLang"> 언어 </param>
        ///// <returns></returns>
        //[AutoComplete]
        //public DataTable mfReadCCSInspectReqItem_DataTypeSelect(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq, int intReqItemSeq, String strLang)
        //{            
        //    DataTable dtRtn = new DataTable();

        //    SQLS sql = new SQLS();                                            // 라이브 모드
        //    //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    // 디버깅 모드

        //    try
        //    {
        //        sql.mfConnect();      // 디버깅 모드 시에만 주석처리

        //        DataTable dtParam = sql.mfSetParamDataTable();
        //        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        //        sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
        //        sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
        //        sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
        //        sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
        //        sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

        //        dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqItem_DataTypeSelect", dtParam);   // 라이브 모드
        //        //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSCCSInspectReqItem_DataTypeSelect", dtParam);     // 디버깅 모드

        //        return dtRtn;
        //    }
        //    catch (System.Exception ex)
        //    {
        //        return dtRtn;
        //        throw (ex);
        //    }
        //    finally
        //    {
        //        sql.mfDisConnect();       // 디버깅 모드 시에만 주석처리
        //    }
        //}

        ///// <summary>
        ///// CCSItem테이블에 저장 전 : 검사항목에 따른 DataType이 선택일때 콤보박스 유저공통테이블에서 Key, Value 가져오는 Method
        ///// </summary>
        ///// <param name="strPlantCode">공장코드</param>
        ///// <param name="strStdNumber">표준번호</param>
        ///// <param name="strStdSeq"> 표준번호순번 </param>        
        ///// <param name="intReqItemSeq"> 검사항목순번 </param>
        ///// <param name="strLang"> 언어 </param>
        ///// <returns></returns>
        //[AutoComplete]
        //public DataTable mfReadCCSInspectReqItem_DataTypeSelect2(String strPlantCode, String strStdNumber, String strStdSeq, int intReqItemSeq, String strLang)
        //{
        //    DataTable dtRtn = new DataTable();

        //    SQLS sql = new SQLS();                                            // 라이브 모드
        //    //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    // 디버깅 모드

        //    try
        //    {
        //        sql.mfConnect();      // 디버깅 모드 시에만 주석처리

        //        DataTable dtParam = sql.mfSetParamDataTable();
        //        sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        //        sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, strStdNumber, 20);
        //        sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, strStdSeq, 4);                
        //        sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
        //        sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

        //        dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqItem_DataTypeSelect2", dtParam);   // 라이브 모드
        //        //dtRtn = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSCCSInspectReqItem_DataTypeSelect2", dtParam);     // 디버깅 모드

        //        return dtRtn;
        //    }
        //    catch (System.Exception ex)
        //    {
        //        return dtRtn;
        //        throw (ex);
        //    }
        //    finally
        //    {
        //        sql.mfDisConnect();       // 디버깅 모드 시에만 주석처리
        //    }
        //}

        /// <summary>
        /// 데이터 유형이 선택일시 콤보박스 설정하기위한 데이터 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strInspectItemCode">검사항목코드</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqItem_DataTypeSelect3(string strPlantCode, string strInspectItemCode, string strProductCode, string strProcessCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, strInspectItemCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqItem_DataTypeSelect3", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 데이터 유형 선택이고 검사유형의 BomCheckFlag가 T 일경우 자동 합부판정 내리기 위한 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strMaterialCode">자재코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqItem_DataTypeSelectInspectResult_MASBOM(string strPlantCode, string strProductCode, string strProcessCode, string strMaterialCode)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, strMaterialCode, 20);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_MASBOM_CCSInspectResult", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// MES I/F 후 검사항목 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strReqItemType">유형(품질or생산)</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqItem_Init(string strPlantCode, string strProcessCode, string strProductCode, string strReqItemType, string strStackSeq, string strLang)
        {
            SQLS sql = new SQLS();
            DataTable dtRtn = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, strReqItemType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, strStackSeq, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReq_Init", dtParam);
                return dtRtn;

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [CCS검사 등록] 아이템 저장 Method(Transaction 통합)
        /// </summary>
        /// <param name="dtItem"> 아이템 정보가 담긴 DataTable </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> 트랜잭션 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectReqItem_PSTS(DataTable dtItem, String strReqNo, String strReqSeq, String strUserID, String strUserIP
                                                , SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                
                String strErrRtn = string.Empty;

                for (int i = 0; i < dtItem.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtItem.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intSeq", ParameterDirection.Input, SqlDbType.Int, dtItem.Rows[i]["Seq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["LotNo"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectIngFlag", ParameterDirection.Input, SqlDbType.Char, dtItem.Rows[i]["InspectIngFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_dblProductItemSS", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["ProductInspectSS"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblQualityItemSS", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["QualityInspectSS"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtItem.Rows[i]["EtcDesc"].ToString(), 100);
                    sql.mfAddParamDataRow(dtParam, "@i_dblFaultQty", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_dblMean", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Mean"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblStdDev", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["StdDev"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMaxValue", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["MaxValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblMinValue", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["MinValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblDataRange", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["DataRange"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCp", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Cp"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblCpk", ParameterDirection.Input, SqlDbType.Decimal, dtItem.Rows[i]["Cpk"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["StdNumber"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["StdSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtItem.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectReqItem_PSTS", dtParam);
                    // 결과 검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }
                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectResultMeasure")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // [CCS검사 등록] 계량형 검사값 정보
    public class CCSInspectResultMeasure : ServicedComponent        // 라이브 모드
    //public class CCSInspectResultMeasure                              // 디버깅 모드
    {
        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자

        /// </summary>
        public CCSInspectResultMeasure()
        {
        }
        /// <summary>
        /// 디버깅용 생성자

        /// </summary>
        /// <param name="strDBConn"></param>
        public CCSInspectResultMeasure(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemType", typeof(char));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(Double));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 계량형 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultMeasure(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID
                                                        , SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();                                    // 라이브 모드
                //SQLS sql = new SQLS(sqlCon.ConnectionString.ToString());    // 디버깅 모드
                String strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultMeasure", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 계량형 검사결과 저장 Method PSTS(Transaction 통합)
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultMeasure_PSTS(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID
                                                        , SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                
                String strErrRtn = string.Empty;
                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultMeasure_PSTS", dtParam);

                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }


    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectResultCount")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // [CCS검사 등록] 계수형 검사값 정보
    public class CCSInspectResultCount : ServicedComponent  // 라이브 모드    
    //public class CCSInspectResultCount // 디버깅 모드    
    {
        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자
        /// </summary>
        public CCSInspectResultCount()
        {
        }
        /// <summary>
        /// 디버깅용 생성자
        /// </summary>
        /// <param name="strDBConn"></param>
        public CCSInspectResultCount(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemType", typeof(char));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(Double));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 계수형 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultCount(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID
                                                    , SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();                                    // 라이브 모드
                //SQLS sql = new SQLS(sqlCon.ConnectionString.ToString());    // 디버깅 모드

                String strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultCount", dtParam);
                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 계수형 검사결과 저장 Method PSTS(Transaction 통합)
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultCount_PSTS(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID
                                                    , SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                
                String strErrRtn = string.Empty;

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_dblInspectValue", ParameterDirection.Input, SqlDbType.Decimal, dtSave.Rows[i]["InspectValue"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultCount_PSTS", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectResultOkNg")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // [CCS검사 등록] OK/NG 검사값 정보
    public class CCSInspectResultOkNg : ServicedComponent           // 라이브 모드
    //public class CCSInspectResultOkNg                                 // 디버깅 모드
    {
        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자

        /// </summary>
        public CCSInspectResultOkNg()
        {
        }
        /// <summary>
        /// 디버깅용 생성자

        /// </summary>
        /// <param name="strDBConn"></param>
        public CCSInspectResultOkNg(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemType", typeof(char));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(String));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// OkNg 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultOkNg(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID
                                                    , SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();                                    // 라이브 모드
                //SQLS sql = new SQLS(sqlCon.ConnectionString.ToString());    // 디버깅 모드
                String strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectValue"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultOkNg", dtParam);
                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 순회검사 Monitoring 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        /// --> 주석처리하겠음. 이 메소드 사용해야 되면 저한테 먼저 문의 바랍니다. 11.10.11 Yoon
        //[AutoComplete(false)]
        //public String mfSaveCCSCycleMonitoring(String strPlantCode, String strEquipCode, String strProcessCode, String strProductCode, String strUserIP, String strUserID
        //                                            , SqlConnection sqlCon, SqlTransaction trans)
        //{
        //    TransErrRtn ErrRtn = new TransErrRtn();
        //    try
        //    {
        //        SQLS sql = new SQLS();                                    // 라이브 모드
        //        //SQLS sql = new SQLS(sqlCon.ConnectionString.ToString());    // 디버깅 모드
        //        String strErrRtn = "";

        //        for (int i = 0; i < 1; i++)
        //        {
        //            // 파리미터 변수 설정
        //            DataTable dtParam = sql.mfSetParamDataTable();
        //            sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
        //            sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
        //            sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
        //            sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
        //            sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
        //            sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
        //            sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
        //            sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

        //            // SP 실행
        //            strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSCycleMonitoring", dtParam);

        //            // 결과검사
        //            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
        //            if (ErrRtn.ErrNum != 0)
        //                break;
        //        }
        //        return strErrRtn;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrRtn.SystemInnerException = ex.InnerException.ToString();
        //        ErrRtn.SystemMessage = ex.Message;
        //        ErrRtn.SystemStackTrace = ex.StackTrace;
        //        return ErrRtn.mfEncodingErrMessage(ErrRtn);
        //    }
        //    finally
        //    {
        //    }
        //}

        /// <summary>
        /// OkNg 검사결과 저장 Method PSTS(Transaction 통합)
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultOkNg_PSTS(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID
                                                    , SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                
                String strErrRtn = string.Empty;
                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectValue"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultOkNg_PSTS", dtParam);
                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectResultDesc")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // [CCS검사 등록] 설명 검사값 정보
    public class CCSInspectResultDesc : ServicedComponent     // 라이브 모드
    //public class CCSInspectResultDesc                           // 디버깅 모드
    {
        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자

        /// </summary>
        public CCSInspectResultDesc()
        {
        }
        /// <summary>
        /// 디버깅용 생성자

        /// </summary>
        /// <param name="strDBConn"></param>
        public CCSInspectResultDesc(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemType", typeof(char));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(String));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 설명 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultDesc(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID,
                                                    SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();                                    // 라이브 모드
                //SQLS sql = new SQLS(sqlCon.ConnectionString.ToString());    // 디버깅 모드
                String strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["InspectValue"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultDesc", dtParam);
                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설명 검사결과 저장 Method PSTS(Transaction 통합)
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultDesc_PSTS(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID,
                                                    SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
             
                String strErrRtn = string.Empty;
                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.NVarChar, dtSave.Rows[i]["InspectValue"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultDesc_PSTS", dtParam);
                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectResultSelect")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    // [CCS검사 등록] 선택 검사값 정보
    public class CCSInspectResultSelect : ServicedComponent       // 라이브 모드
    //public class CCSInspectResultSelect                             // 디버깅 모드
    {
        private SqlConnection m_SqlConnDebug;
        /// <summary>
        /// 생성자
        /// </summary>
        public CCSInspectResultSelect()
        {
        }
        /// <summary>
        /// 디버깅용 생성자
        /// </summary>
        /// <param name="strDBConn"></param>
        public CCSInspectResultSelect(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);
            m_SqlConnDebug.Open();
        }

        /// <summary>
        /// 데이터 테이블 컬럼 설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemType", typeof(char));
                dtRtn.Columns.Add("ReqResultSeq", typeof(Int32));
                dtRtn.Columns.Add("InspectValue", typeof(String));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 선택 검사결과 저장 Method
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultSelect(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                SQLS sql = new SQLS();                                    // 라이브 모드
                //SQLS sql = new SQLS(sqlCon.ConnectionString.ToString());    // 디버깅 모드
                String strErrRtn = "";

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectValue"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultSelect", dtParam);
                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 선택 검사결과 저장 Method PSTS(Transaction 통합)
        /// </summary>
        /// <param name="dtSave"> 검사결과값이 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자 IP </param>
        /// <param name="strUserID"> 사용자 ID </param>
        /// <param name="sqlCon"> SqlConnection 변수 </param>
        /// <param name="trans"> Transaction 변수 </param>
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveCCSInspectResultSelect_PSTS(DataTable dtSave, String strReqNo, String strReqSeq, String strUserIP, String strUserID, SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                
                String strErrRtn = string.Empty;
                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    // 파리미터 변수 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtSave.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqResultSeq", ParameterDirection.Input, SqlDbType.Int, dtSave.Rows[i]["ReqResultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtSave.Rows[i]["InspectValue"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_INSCCSInspectResultSelect_PSTS", dtParam);
                    // 결과검사

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

    }




    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("SPCN")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SPCN : ServicedComponent
    {
        /// <summary>
        /// DataTable 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(int));
                dtRtn.Columns.Add("ReqItemSeq", typeof(int));
                dtRtn.Columns.Add("WriteUserID", typeof(string));
                dtRtn.Columns.Add("WriteDate", typeof(string));
                dtRtn.Columns.Add("OCP", typeof(string));
                dtRtn.Columns.Add("UpperRun", typeof(string));
                dtRtn.Columns.Add("LowerRun", typeof(string));
                dtRtn.Columns.Add("UpperTrend", typeof(string));
                dtRtn.Columns.Add("LowerTrend", typeof(string));
                dtRtn.Columns.Add("Cycle", typeof(string));
                dtRtn.Columns.Add("ProblemDesc", typeof(string));
                dtRtn.Columns.Add("ProblemUserID", typeof(string));
                dtRtn.Columns.Add("ProblemDate", typeof(string));
                dtRtn.Columns.Add("CauseDesc", typeof(string));
                dtRtn.Columns.Add("CauseUserID", typeof(string));
                dtRtn.Columns.Add("CauseDate", typeof(string));
                dtRtn.Columns.Add("PreventionDesc", typeof(string));
                dtRtn.Columns.Add("PreventionUserID", typeof(string));
                dtRtn.Columns.Add("PreventionDate", typeof(string));
                dtRtn.Columns.Add("ResultDesc", typeof(string));
                dtRtn.Columns.Add("ResultUserID", typeof(string));
                dtRtn.Columns.Add("ResultDate", typeof(string));

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [SPCN LIST] List 검색 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strProductCode"> 제품코드 </param>
        /// <param name="strLotNo"> LotNo </param>        
        /// <param name="strProcessCode"> 공정 </param>
        /// <param name="strWriteFromDate"> 등록일 From </param>
        /// <param name="strWriteToDate"> 등록일 To </param>        
        /// <param name="strLang"> 언어 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSPCNList(String strPlantCode, String strProductCode, String strLotNo, String strProcessCode,
                                        String strWriteFromDate, String strWriteToDate, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                // DB연결
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLotNo", ParameterDirection.Input, SqlDbType.NVarChar, strLotNo, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqFromDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteFromDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqToDate", ParameterDirection.Input, SqlDbType.VarChar, strWriteToDate, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                // SP실행
                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcSPCNList", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [SPCN LIST] 상세정보 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="intReqLotSeq"> 의뢰Lot순번 </param>
        /// <param name="intReqItemSeq"> 검사항목순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSPCNDetail(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq, int intReqItemSeq, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcSPCNDetail", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// [SPCN LIST] 저장 Method
        /// </summary>
        /// <param name="dtSPCN"> 저장할 정보가 담긴 DataTable </param>
        /// <param name="strUserIP"> 사용자IP </param>
        /// <param name="strUserID"> 사용자ID </param>        
        /// <returns></returns>
        [AutoComplete(false)]
        public String mfSaveSPCNSave(DataTable dtSPCN, String strUserIP, String strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                String strErrRtn = "";
                // 디비연결
                sql.mfConnect();
                SqlTransaction trans;

                for (int i = 0; i < dtSPCN.Rows.Count; i++)
                {
                    // Transaction 시작
                    trans = sql.SqlCon.BeginTransaction();

                    // Parameter DataTable 설정
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtSPCN.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqitemSeq", ParameterDirection.Input, SqlDbType.Int, dtSPCN.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["WriteUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strWriteDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["WriteDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strOCP", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["OCP"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUpperRun", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["UpperRun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLowerRun", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["LowerRun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strUpperTrend", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["UpperTrend"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strLowerTrend", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["LowerTrend"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strCycle", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["Cycle"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_strProblemDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSPCN.Rows[i]["ProblemDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strProblemUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ProblemUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strProblemDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ProblemDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSPCN.Rows[i]["CauseDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["CauseUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strCauseDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["CauseDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strPreventionDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSPCN.Rows[i]["PreventionDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strPreventionUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["PreventionUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strPreventionDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["PreventionDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtSPCN.Rows[i]["ResultDesc"].ToString(), 200);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultUserID", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ResultUserID"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strResultDate", ParameterDirection.Input, SqlDbType.VarChar, dtSPCN.Rows[i]["ResultDate"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    // SP 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcSPCN", dtParam);

                    // 실행결과
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                    {
                        // 실패이면 Rollback
                        trans.Rollback();
                        break;
                    }
                    trans.Commit();

                }
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// [SPCN LIST] 측정값 Data 조회 Method
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번 </param>
        /// <param name="intReqLotSeq"> 의뢰Lot순번 </param>
        /// <param name="intReqItemSeq"> 검사항목순번 </param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadSPCNDetailValue(String strPlantCode, String strReqNo, String strReqSeq, int intReqLotSeq, int intReqItemSeq)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();

            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, intReqItemSeq.ToString());

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcSPCNDetail_Value", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectReqPara")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CCSInspectReqPara : ServicedComponent
    {
        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ParaSeq", typeof(Int32));
                dtRtn.Columns.Add("LinkId", typeof(string));
                dtRtn.Columns.Add("CCSParameterCode", typeof(string));
                dtRtn.Columns.Add("DATATYPE", typeof(string));
                dtRtn.Columns.Add("VALIDATIONTYPE", typeof(string));
                dtRtn.Columns.Add("ParaVALUE", typeof(string));
                dtRtn.Columns.Add("LOWERLIMIT", typeof(string));
                dtRtn.Columns.Add("UPPERLIMIT", typeof(string));
                dtRtn.Columns.Add("InspectValue", typeof(string));
                dtRtn.Columns.Add("QualityValue", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(string));
                dtRtn.Columns.Add("MaterialSpecName", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// GLOMIS에 등록된 TOOL KIT현황 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <param name="strPackageCode">Package</param>
        /// <param name="strCustomerCode">고객사코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqPara_ToolValue(string strPlantCode, string strEquipCode, string strPackageCode, string strCustomerCode)
        {
            SQLS sql = new SQLS();
            DataTable dtToolValue = new DataTable();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackageCode, 50);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                dtToolValue = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqItem_TOOL_Value", dtParam);

                return dtToolValue;
            }
            catch (Exception ex)
            {
                return dtToolValue;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtToolValue.Dispose();
            }
        }

        /// <summary>
        /// CCS 가동조건 테이블 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰번호순번</param>
        /// <param name="intReqLotSeq">의뢰Lot순번</param>
        /// <param name="intParaSeq">가동조건 순번(현재사용안함)</param>
        /// <param name="strLang">언어(사용안함)</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqPara(string strPlantCode, string strReqNo, string strReqSeq, Int32 intReqLotSeq, Int32 intParaSeq, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                //sql.mfAddParamDataRow(dtParam, "@i_intParaSeq", ParameterDirection.Input, SqlDbType.Int, intParaSeq.ToString());

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_CCSInspectReqPara", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS 가동조건 테이블 저장 메소드
        /// </summary>
        /// <param name="dtPara">저장정보 저장된 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCCSInspectReqPara(DataTable dtPara, string strReqNo, string strReqSeq, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                SQLS sql = new SQLS();

                trans = sqlCon.BeginTransaction();

                for (int i = 0; i < dtPara.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["PlantCode"].ToString(), 10);
                    //sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["ReqNo"].ToString(), 20);
                    //sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtPara.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intParaSeq", ParameterDirection.Input, SqlDbType.Int, dtPara.Rows[i]["ParaSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSParameterCode", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["CCSParameterCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strDATATYPE", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["DATATYPE"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strVALIDATIONTYPE", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["VALIDATIONTYPE"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strParaVALUE", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["ParaVALUE"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strLOWERLIMIT", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["LOWERLIMIT"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUPPERLIMIT", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["UPPERLIMIT"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["InspectValue"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualityValue", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["QualityValue"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_CCSInspectReqPara", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        break;
                    }
                }

                if (ErrRtn.ErrNum.Equals(0))
                    trans.Commit();
                else
                    trans.Rollback();

                sql.Dispose();
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
            }
        }

        /// <summary>
        /// CCS 가동조건 초기값 가져오는 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strPackage">Package코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strEquipCode">설비코드</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqPara_Init(string strPlantCode, string strPackage, string strProcessCode, string strEquipCode, string strCustomerCode)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strCustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSCCSInspectReqPara_Init", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS 가동조건 테이블 저장 메소드 PSTS(Transaction 통합
        /// </summary>
        /// <param name="dtPara">저장정보 저장된 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveCCSInspectReqPara_PSTS(DataTable dtPara, string strReqNo, string strReqSeq, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                
                for (int i = 0; i < dtPara.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtPara.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intParaSeq", ParameterDirection.Input, SqlDbType.Int, dtPara.Rows[i]["ParaSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strLinkId", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["LinkId"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strCCSParameterCode", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["CCSParameterCode"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strDATATYPE", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["DATATYPE"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strVALIDATIONTYPE", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["VALIDATIONTYPE"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strParaVALUE", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["ParaVALUE"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strLOWERLIMIT", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["LOWERLIMIT"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strUPPERLIMIT", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["UPPERLIMIT"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectValue", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["InspectValue"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strQualityValue", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["QualityValue"].ToString(), 40);
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectResultFlag", ParameterDirection.Input, SqlDbType.VarChar, dtPara.Rows[i]["InspectResultFlag"].ToString(), 2);
                    sql.mfAddParamDataRow(dtParam, "@i_strMaterialSpecName", ParameterDirection.Input, SqlDbType.NVarChar, dtPara.Rows[i]["MaterialSpecName"].ToString(), 50);  // 2012-12-14 추가

                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqNo", ParameterDirection.Output, SqlDbType.VarChar, 20);
                    sql.mfAddParamDataRow(dtParam, "@o_strReqSeq", ParameterDirection.Output, SqlDbType.VarChar, 4);
                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_CCSInspectReqPara_PSTS", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (!ErrRtn.ErrNum.Equals(0))
                        break;
                }

                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// CCS 가동조건 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="intReqLotSeq">Lot순번</param>
        /// <param name="strProductCode">제품코드</param>
        /// <param name="strProcessCode">공정코드</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <param name="strLang">언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqPara_PSTS(string strPlantCode, string strReqNo, string strReqSeq, int intReqLotSeq
                                                , string strProductCode, string strProcessCode, string strEquipCode, string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, intReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                return dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_CCSInspectReqPara_PSTS", dtParam);
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// CCS 가동조건 조회 메소드
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strReqNo">의뢰번호</param>
        /// <param name="strReqSeq">의뢰순번</param>
        /// <param name="strReqLotSeq">의뢰차수</param>
        /// <param name="strPackage">Package</param>
        /// <param name="strProcessCode">공정</param>
        /// <param name="strCustomerCode">고객</param>
        /// <param name="strEquipCode">거래처</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadCCSInspectReqPara_MASCCSPara(string strPlantCode, string strReqNo, string strReqSeq, int strReqLotSeq
                                                            , string strProductCode, string strPackage, string strProcessCode
                                                            , string strEquipCode, string strCustomerCode
                                                            , string strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();

                DataTable dtParam = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, strReqLotSeq.ToString());
                sql.mfAddParamDataRow(dtParam, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, strProductCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strPackage", ParameterDirection.Input, SqlDbType.NVarChar, strPackage, 40);
                sql.mfAddParamDataRow(dtParam, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, strProcessCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strEquipCode", ParameterDirection.Input, SqlDbType.VarChar, strEquipCode, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strCoustomerCode", ParameterDirection.Input, SqlDbType.VarChar, strCustomerCode, 20);

                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_CCSInspectReqPara_MASCCSPara", dtParam);

                if (dtRtn.Rows.Count == 0)
                    dtRtn = mfCoulmnsInfo();

                return dtRtn;

            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 데이터 테이블 컬럼설정 메소드
        /// </summary>
        /// <returns></returns>
        public DataTable mfCoulmnsInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(string));
                dtRtn.Columns.Add("ReqNo", typeof(string));
                dtRtn.Columns.Add("ReqSeq", typeof(string));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ParaSeq", typeof(Int32));
                dtRtn.Columns.Add("CCSparameterCode", typeof(string));
                dtRtn.Columns.Add("DATATYPE", typeof(string));
                dtRtn.Columns.Add("VALIDATIONTYPE", typeof(string));
                dtRtn.Columns.Add("ParaVALUE", typeof(string));
                dtRtn.Columns.Add("LOWERLIMIT", typeof(string));
                dtRtn.Columns.Add("UPPERLIMIT", typeof(string));
                dtRtn.Columns.Add("InspectValue", typeof(string));
                dtRtn.Columns.Add("QualityValue", typeof(string));
                dtRtn.Columns.Add("InspectResultFlag", typeof(string));
                dtRtn.Columns.Add("MaterialSpecName", typeof(string));
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSMESInterface")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CCSMESInterface : ServicedComponent
    {
        private string m_strMesCode;

        public string MesCode
        {
            get { return m_strMesCode; }
            set { m_strMesCode = value; }
        }

        public CCSMESInterface()
        {
            MesCode = "S07";
        }

        /// <summary>
        /// LotInfo 정보 I/F 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strLotNo">LotNo</param>
        /// <returns></returns>
        public DataTable mfRead_LOT_INFO_REQ(string strLotNo, DataTable dtSysAcce)
        {
            DataTable dtLotInfo = new DataTable();
            try
            {
                /////// MES 서버경로 가져오기
                ////MESCodeReturn mcr = new MESCodeReturn();
                ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                ////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, mcr.MesCode);

                // I/F 메소드 호출
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                dtLotInfo = clsTibrv.LOT_INFO_REQ(strLotNo);

                return dtLotInfo;
            }
            catch (Exception ex)
            {
                dtLotInfo.Columns.Add("returncode", typeof(string));
                dtLotInfo.Columns.Add("returnmessage", typeof(string));
                DataRow _dr = dtLotInfo.NewRow();
                _dr["returncode"] = "-999";
                _dr["returnmessage"] = "Exception Error<br/>" + ex.Message;
                dtLotInfo.Rows.Add(_dr);
                return dtLotInfo;
            }
            finally
            {
                dtLotInfo.Dispose();
            }
        }

        /// <summary>
        /// LotHold MES I/F 메소드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFormName">화면ID</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strComment">비고</param>
        /// <param name="strHoldCode">Hold코드</param>
        /// <param name="dtLotList">Lot리스트</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        public DataTable mfSend_LOT_HOLD4QC_REQ(string strPlantCode, string strFormName, string strUserID, string strComment, string strHoldCode
                                , DataTable dtLotList, string strUserIP, DataTable dtSysAcce)
        {
            DataTable dtHoldInfo = new DataTable();
            try
            {
                //MES서버경로 가져오기
                ////QRPCCS.BL.INSCCS.MESCodeReturn clsMesCode = new QRPCCS.BL.INSCCS.MESCodeReturn();
                ////QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                ////DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, clsMesCode.MesCode);

                // MES I/F
                QRPMES.IF.Tibrv clsTibrv = new QRPMES.IF.Tibrv(dtSysAcce);
                dtHoldInfo = clsTibrv.LOT_HOLD4QC_REQ(strFormName, strUserID, strComment, strHoldCode, dtLotList, strUserIP);

                clsTibrv.Dispose();

                return dtHoldInfo;
            }
            catch (Exception ex)
            {
                dtHoldInfo.Columns.Add("returncode", typeof(string));
                dtHoldInfo.Columns.Add("returnmessage", typeof(string));
                DataRow _dr = dtHoldInfo.NewRow();
                _dr["returncode"] = "-999";
                _dr["returnmessage"] = "Exception Error<br/>" + ex.Message;
                dtHoldInfo.Rows.Add(_dr);
                return dtHoldInfo;
            }
            finally
            {
                dtHoldInfo.Dispose();
            }
        }

        /// <summary>
        /// CCS 의뢰접수완료 MES I/F
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strFormName">화면ID</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strEquipCode">설비번호</param>
        /// <param name="strLotNo">LotNo</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <returns></returns>
        public DataTable mfSend_CCS_ACCEPT_REQ(string strPlantCode, string strFormName, string strUserID, string strEquipCode, string strLotNo, string strUserIP, DataTable dtSysAcce)
        {
            DataTable dtAcceptInfo = new DataTable();
            try
            {
                // MES 연결정보를 보내고 그 연결정보로 정보를 보낸다
                QRPMES.IF.Tibrv Tibrv = new QRPMES.IF.Tibrv(dtSysAcce);

                //CCS의뢰접수완료 요청매서드 실행 하여 처리결과를 받아온다.
                dtAcceptInfo = Tibrv.CCS_ACCEPT_REQ(strFormName, strUserID, strEquipCode, strLotNo, strUserIP);
                return dtAcceptInfo;
            }
            catch (Exception ex)
            {
                dtAcceptInfo.Columns.Add("returncode", typeof(string));
                dtAcceptInfo.Columns.Add("returnmessage", typeof(string));
                DataRow _dr = dtAcceptInfo.NewRow();
                _dr["returncode"] = "-999";
                _dr["returnmessage"] = "Exception Error<br/>" + ex.Message;
                dtAcceptInfo.Rows.Add(_dr);
                return dtAcceptInfo;
            }
            finally
            {
                dtAcceptInfo.Dispose();
            }
        }

        /// <summary>
        /// CCS 의뢰 설비 Locking/UnLocking 메소드
        /// </summary>
        /// <param name="dtEQPDownInfo">I/F 정보</param>
        /// <param name="strUserID">사용자ID</param>
        /// <param name="strUserIP">사용자IP</param>
        /// <param name="dtSysAcce">연결정보</param>
        /// <returns></returns>
        public DataTable mfSend_EQP_DOWN4QC(DataTable dtEQPDownInfo, string strUserID, string strUserIP, DataTable dtSysAcce)
        {
            DataTable dtDownInfo = new DataTable();
            try
            {
                if (dtEQPDownInfo.Rows.Count > 0)
                {
                    // 변수 설정
                    string strFormName = dtEQPDownInfo.Rows[0]["FormName"].ToString();
                    string strPlantCode = dtEQPDownInfo.Rows[0]["PlantCode"].ToString();
                    string strInspectUserID = dtEQPDownInfo.Rows[0]["InspectUserID"].ToString();
                    string strLotNo = dtEQPDownInfo.Rows[0]["LotNo"].ToString();
                    string strEquipCode = dtEQPDownInfo.Rows[0]["EquipCode"].ToString();
                    string strDownCode = dtEQPDownInfo.Rows[0]["DownCode"].ToString();
                    string strLockingFlag = dtEQPDownInfo.Rows[0]["LockingFlag"].ToString();
                    string strComment = dtEQPDownInfo.Rows[0]["Comment"].ToString();
                    string strInterfaceType = dtEQPDownInfo.Rows[0]["InterfaceType"].ToString();

                    // MES 연결정보를 보내고 그 연결정보로 정보를 보낸다
                    QRPMES.IF.Tibrv Tibrv = new QRPMES.IF.Tibrv(dtSysAcce);


                    //CCS의뢰접수완료 요청매서드 실행 하여 처리결과를 받아온다.
                    dtDownInfo = Tibrv.EQP_DOWN4QC(strFormName, strPlantCode, strInspectUserID, strLotNo, strEquipCode
                                                , strDownCode, strLockingFlag, strComment, strUserID, strUserIP);
                }
                return dtDownInfo;
            }
            catch (Exception ex)
            {
                dtDownInfo.Columns.Add("returncode", typeof(string));
                dtDownInfo.Columns.Add("returnmessage", typeof(string));
                DataRow _dr = dtDownInfo.NewRow();
                _dr["returncode"] = "-999";
                _dr["returnmessage"] = "Exception Error<br/>" + ex.Message;
                dtDownInfo.Rows.Add(_dr);
                return dtDownInfo;
            }
            finally
            {
                dtDownInfo.Dispose();
            }
        }

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    //[ObjectPooling(MinPoolSize = 20, MaxPoolSize = 1048576, CreationTimeout = 60000)]
    [Serializable]
    [System.EnterpriseServices.Description("CCSInspectReqItemFaulty")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class CCSInspectReqItemFaulty : ServicedComponent
    {
        /// <summary>
        /// 컬럼설정 Method
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetDataInfo()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                dtRtn.Columns.Add("PlantCode", typeof(String));
                dtRtn.Columns.Add("ReqNo", typeof(String));
                dtRtn.Columns.Add("ReqSeq", typeof(String));
                dtRtn.Columns.Add("ReqLotSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemSeq", typeof(Int32));
                dtRtn.Columns.Add("ReqItemType", typeof(char));
                dtRtn.Columns.Add("FaultSeq", typeof(Int32));
                dtRtn.Columns.Add("FaultQty", typeof(decimal));
                dtRtn.Columns.Add("InspectFaultTypeCode", typeof(string));
                dtRtn.Columns.Add("EtcDesc", typeof(String));
                
                
                
                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// CCS 검사항목 불량정보 조회 Method 
        /// </summary>
        /// <param name="strPlantCode"> 공장코드 </param>
        /// <param name="strReqNo"> 의뢰번호 </param>
        /// <param name="strReqSeq"> 의뢰순번</param>
        /// <returns></returns>
        [AutoComplete]
        public DataTable mfReadInspectReqItemFaulty(String strPlantCode, String strReqNo, String strReqSeq,
                                                            String strReqLotSeq, String strReqItemSeq, String strReqItemType, String strLang)
        {
            DataTable dtRtn = new DataTable();
            SQLS sql = new SQLS();
            try
            {
                sql.mfConnect();     

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, strReqLotSeq);
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, strReqItemSeq);
                sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, strReqItemType, 1);
                sql.mfAddParamDataRow(dtParam, "@i_strLang", ParameterDirection.Input, SqlDbType.VarChar, strLang, 3);

                dtRtn = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_CCSInspectReqItemFaulty", dtParam);

                return dtRtn;
            }
            catch (Exception ex)
            {
                return dtRtn;
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
                dtRtn.Dispose();
            }
        }


        /// <summary>
        /// CCS 검사항목 불량유형상세정보 저장 메소드 
        /// </summary>
        /// <param name="dtPara">저장정보 저장된 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <param name="sqlCon">SQLConnection 변수</param>
        /// <param name="trans">트랜잭션변수</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveInspectReqItemFaulty(DataTable dtFaultType, string strReqNo, string strReqSeq, string strUserID, string strUserIP, SqlConnection sqlCon, SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = string.Empty;
            try
            {
                
                
                for (int i = 0; i < dtFaultType.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFaultType.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFaultType.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFaultType.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtFaultType.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFaultType.Rows[i]["FaultSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultQty", ParameterDirection.Input, SqlDbType.Int, dtFaultType.Rows[i]["FaultQty"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFaultType.Rows[i]["InspectFaultTypeCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtFaultType.Rows[i]["EtcDesc"].ToString(), 1000);
                    
                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sqlCon, trans, "up_Update_CCSInspectReqItemFaulty", dtParam);
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        break;
                }

                
                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }

        /// <summary>
        /// CCS 검사항목 불량유형상세정보 저장 메소드 
        /// </summary>
        /// <param name="dtPara">저장정보 저장된 테이블</param>
        /// <param name="strUserID">사용자아이디</param>
        /// <param name="strUserIP">사용자아이피</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfSaveInspectReqItemFaulty(DataTable dtFaultType, string strPlantCode,string strReqNo,string strReqSeq,string strReqLotSeq,
                                                string strReqItemSeq,string strReqItemType, string strUserID, string strUserIP)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;
                
                //Db연결
                sql.mfConnect();

                //Transaction 설정
                SqlTransaction trans = sql.SqlCon.BeginTransaction();


                //해당정보 전체삭제매서드 실행
                strErrRtn = mfDeleteInspectReqItemFaulty(strPlantCode, strReqNo,
                                                        strReqSeq, strReqLotSeq,
                                                        strReqItemSeq, strReqItemType,
                                                        "0", sql.SqlCon, trans);
                //결과정보
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과가 실패한경우 Rollback 후 처리를 멈춘다.
                if (ErrRtn.ErrNum != 0)
                {
                    trans.Rollback();
                    return strErrRtn;
                }
                

                //불량유형정보를 DB에 저장
                for (int i = 0; i < dtFaultType.Rows.Count; i++)
                {
                    
                    DataTable dtParam = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100); //ReturnValue

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtFaultType.Rows[i]["PlantCode"].ToString(), 10); // 공장
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtFaultType.Rows[i]["ReqNo"].ToString(), 20);        //관리번호
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtFaultType.Rows[i]["ReqSeq"].ToString(), 4);       //순번
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtFaultType.Rows[i]["ReqLotSeq"].ToString());        //검사Lot순번
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtFaultType.Rows[i]["ReqItemSeq"].ToString());      //검사항목순번
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtFaultType.Rows[i]["ReqItemType"].ToString(), 1); //검사유형 Q: 품질,P:생산팀
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtFaultType.Rows[i]["FaultSeq"].ToString());          //불량순번
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultQty", ParameterDirection.Input, SqlDbType.Int, dtFaultType.Rows[i]["FaultQty"].ToString());          //불량수
                    sql.mfAddParamDataRow(dtParam, "@i_strInspectFaultTypeCode", ParameterDirection.Input, SqlDbType.VarChar, dtFaultType.Rows[i]["InspectFaultTypeCode"].ToString(), 10); //불량
                    sql.mfAddParamDataRow(dtParam, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.VarChar, dtFaultType.Rows[i]["EtcDesc"].ToString(), 1000); // 비고

                    sql.mfAddParamDataRow(dtParam, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15); //사용자IP
                    sql.mfAddParamDataRow(dtParam, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20); //사용자

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000); //ErrorMessage

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_CCSInspectReqItemFaulty", dtParam); // 저장된Parameter 해당 SP로 보내 처리한다.
                    // 결과검사
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리실패시 RollBack 후 처리를 멈춘다.
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        break;
                    }
                }

                //모든처리가 완료되었다면 Commit하여 최종저장처리
                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                //처리결과 
                return strErrRtn;
            }
            catch (Exception ex)
            {
                //ErrorMessage EnCoding
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }


        /// <summary>
        /// CCS 검사항목 불량유형상세정보 삭제 메소드 
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strReqNo">관리번호</param>
        /// <param name="strReqSeq">관리번호순번</param>
        /// <param name="strReqLotSeq">Lot 순번</param>
        /// <param name="strReqItemSeq"></param>
        /// <param name="strReqItemType"></param>
        /// <param name="strFaultySeq"></param>
        /// <param name="sqlcon"></param>
        /// <param name="trans"></param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteInspectReqItemFaulty(string strPlantCode, string strReqNo, string strReqSeq, string strReqLotSeq, 
                                                   string strReqItemSeq, string strReqItemType,string strFaultSeq,
                                                    SqlConnection sqlcon,SqlTransaction trans)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {

                DataTable dtParam = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, strPlantCode, 10);
                sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, strReqNo, 20);
                sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, strReqSeq, 4);
                sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, strReqLotSeq);
                sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, strReqItemSeq);
                sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, strReqItemType,1);
                sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, strFaultSeq);
                sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                string strErrRtn = sql.mfExecTransStoredProc(sqlcon, trans, "up_Delete_CCSInspectReqItemFaulty", dtParam);

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.Dispose();
            }
        }



        /// <summary>
        /// CCS 검사항목 불량유형상세정보 삭제 메소드 
        /// </summary>
        /// <param name="dtDelFaulty">CCS 검사항목 불량유형삭제정보</param>
        /// <returns></returns>
        [AutoComplete(false)]
        public string mfDeleteInspectReqItemFaulty(DataTable dtDelFaulty)
        {
            SQLS sql = new SQLS();
            TransErrRtn ErrRtn = new TransErrRtn();
            try
            {
                string strErrRtn = string.Empty;

                sql.mfConnect();
                SqlTransaction trans = sql.SqlCon.BeginTransaction();
                for (int i = 0; i < dtDelFaulty.Rows.Count; i++)
                {
                    DataTable dtParam = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParam, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParam, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtDelFaulty.Rows[i]["PlantCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqNo", ParameterDirection.Input, SqlDbType.VarChar, dtDelFaulty.Rows[i]["ReqNo"].ToString(), 20);
                    sql.mfAddParamDataRow(dtParam, "@i_strReqSeq", ParameterDirection.Input, SqlDbType.VarChar, dtDelFaulty.Rows[i]["ReqSeq"].ToString(), 4);
                    sql.mfAddParamDataRow(dtParam, "@i_intReqLotSeq", ParameterDirection.Input, SqlDbType.Int, dtDelFaulty.Rows[i]["ReqLotSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_intReqItemSeq", ParameterDirection.Input, SqlDbType.Int, dtDelFaulty.Rows[i]["ReqItemSeq"].ToString());
                    sql.mfAddParamDataRow(dtParam, "@i_strReqItemType", ParameterDirection.Input, SqlDbType.Char, dtDelFaulty.Rows[i]["ReqItemType"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParam, "@i_intFaultSeq", ParameterDirection.Input, SqlDbType.Int, dtDelFaulty.Rows[i]["FaultSeq"].ToString());

                    sql.mfAddParamDataRow(dtParam, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Delete_CCSInspectReqItemFaulty", dtParam);

                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                    if (ErrRtn.ErrNum != 0)
                        trans.Rollback();
                        
                }

                if (ErrRtn.ErrNum == 0)
                    trans.Commit();

                return strErrRtn;
            }
            catch (Exception ex)
            {
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
                sql.Dispose();
            }
        }
    }
}
