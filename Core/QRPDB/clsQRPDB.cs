﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//추가 참조
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
//using System.Data.OracleClient;

using System.Data;
using System.Collections;
using System.Configuration;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;

using Microsoft.Web.Administration;
//using System.Collections.Specialized;
using System.Xml;

[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true, AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                          Authentication = AuthenticationOption.None,
                                          ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPDB
{

    #region MS-SQL DB 처리 Class
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("QRPDB")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class SQLS : ServicedComponent
    {
        private string m_strConnString;
        private static SqlConnection m_SqlCon;

        public string ConnString
        {
            get { return m_strConnString; }
        }

        public SqlConnection SqlCon
        {
            get { return m_SqlCon; }
        }

        /// <summary>
        /// 생성자 : Web.Config에서 DB연결정보 얻기
        /// </summary>
        public SQLS()
        {
            try
            {

                //m_strConnString = "data source=localhost;user id=QRPUSR;password=jinjujin;Initial Catalog=QRP_STS_DEV;persist security info=true";

                //Web.Config의 appSetting에 있는 서버정보를 읽는다.
                ServerManager srvManage = new ServerManager();
                Microsoft.Web.Administration.Configuration config = srvManage.GetWebConfiguration("Default Web Site/QRP_PSTS_DEV"); //개발서버
                //Microsoft.Web.Administration.Configuration config = srvManage.GetWebConfiguration("Default Web Site/QRP_PSTS_App"); //운영서버
                Microsoft.Web.Administration.ConfigurationSection sect = config.GetSection("appSettings");
                Microsoft.Web.Administration.ConfigurationElementCollection appSettingCol = sect.GetCollection();

                for (int i = 0; i < appSettingCol.Count; i++)
                {
                    if (appSettingCol[i].GetAttributeValue("key").ToString() == "ConnectionString")
                        m_strConnString = appSettingCol[i].GetAttributeValue("value").ToString();
                }

            }
            catch (Exception ex)
            {
                throw(ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 생성자 : Web.Config에서 DB연결정보 얻기
        /// </summary>
        public SQLS(bool bolAsync)
        {
            try
            {

                //m_strConnString = "data source=localhost;user id=QRPUSR;password=jinjujin;Initial Catalog=QRP_STS_DEV;persist security info=true";

                //Web.Config의 appSetting에 있는 서버정보를 읽는다.
                ServerManager srvManage = new ServerManager();
                Microsoft.Web.Administration.Configuration config = srvManage.GetWebConfiguration("Default Web Site/QRP_STS_DEV"); //개발서버
                //Microsoft.Web.Administration.Configuration config = srvManage.GetWebConfiguration("Default Web Site/QRP_STS_App"); //운영서버
                Microsoft.Web.Administration.ConfigurationSection sect = config.GetSection("appSettings");
                Microsoft.Web.Administration.ConfigurationElementCollection appSettingCol = sect.GetCollection();

                for (int i = 0; i < appSettingCol.Count; i++)
                {
                    if (appSettingCol[i].GetAttributeValue("key").ToString() == "ConnectionString")
                        m_strConnString = appSettingCol[i].GetAttributeValue("value").ToString();
                }

                //if (bolAsync == true)
                //    m_strConnString = m_strConnString + ";Extended Properties='async=true'";

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 생성자 : DB연결정보 얻기
        /// </summary>
        /// <param name="strConnection">연결문자</param>
        public SQLS(string strConnection)
        {
            try
            {
                //if (strConnection != "")
                    m_strConnString = strConnection;
                //else
                //{
                //    //Web.Config의 appSetting에 있는 서버정보를 읽는다.
                //    ServerManager srvManage = new ServerManager();
                //    Microsoft.Web.Administration.Configuration config = srvManage.GetWebConfiguration("Default Web Site/QRP_STS_DEV");
                //    Microsoft.Web.Administration.ConfigurationSection sect = config.GetSection("appSettings");
                //    Microsoft.Web.Administration.ConfigurationElementCollection appSettingCol = sect.GetCollection();

                //    for (int i = 0; i < appSettingCol.Count; i++)
                //    {
                //        if (appSettingCol[i].GetAttributeValue("key").ToString() == "ConnectionString")
                //            m_strConnString = appSettingCol[i].GetAttributeValue("value").ToString();
                //    }
                //}
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void FindSection(SectionGroup group)
        {
            foreach (SectionGroup grp in group.SectionGroups)
            {
                FindSection(grp);
            }

            m_strConnString = m_strConnString + "groupname:" + group.Name + "\r\n";

            //foreach (SectionDefinition def in group.Sections)
            //{
            //    m_strConnString = m_strConnString + "sectionname:" + def.Name + "\r\n";
            //    m_strConnString = m_strConnString + "sectionname-AllowDefinition:" + def.AllowDefinition + "\r\n";
            //    m_strConnString = m_strConnString + "sectionname-AllowLocation:" + def.AllowLocation + "\r\n";
            //    m_strConnString = m_strConnString + "sectionname-OverrideModeDefault:" + def.OverrideModeDefault + "\r\n";
            //    m_strConnString = m_strConnString + "sectionname-RequirePermission:" + def.RequirePermission + "\r\n";
            //    m_strConnString = m_strConnString + "sectionname-Type:" + def.Type + "\r\n\r\n ";
            //}

        }

        #region MS-SQL 서버 연결 및 해제
        /// <summary>
        /// MS-SQL 서버 연결.
        /// </summary>
        /// <returns>연결여부</returns>
        public bool mfConnect()
        {
            try
            {
                //연결정보가 없는 경우
                if (m_strConnString == "")
                    return false;
                else
                {
                    m_SqlCon = new SqlConnection(m_strConnString);
                    //Open상태가 아니면 Open한다.
                    if (m_SqlCon.State != ConnectionState.Open)
                        m_SqlCon.Open();

                    if (m_SqlCon.State != ConnectionState.Open)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                //throw (ex);
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// MS-SQL 서버 연결 끊기.
        /// </summary>
        public void mfDisConnect()
        {
            try
            {
                if (m_SqlCon.State != ConnectionState.Closed)
                {
                    m_SqlCon.Close();
                    m_SqlCon = null;
                }
                else
                    m_SqlCon = null;
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }
        #endregion

        #region 저장프로시저 실행
        /// <summary>
        /// 읽기 저장프로시저 실행 : SP함수명만 있는 경우
        /// </summary>
        /// <param name="Conn">DB연결객체</param>
        /// <param name="strSPName">SP명</param>
        /// <returns>Select결과</returns>
        public DataTable mfExecReadStoredProc(SqlConnection Conn, string strSPName)
        {
            DataTable dt = new DataTable();
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;

                    SqlDataAdapter da = new SqlDataAdapter(m_SqlCmd);
                    da.Fill(dt);
                }                    
                return dt;
            }
            catch (Exception ex)
            {
                //throw (ex);
                return dt;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 읽기 저장프로시저 실행 : 인자(param배열)가 있는 경우
        /// </summary>
        /// <param name="Conn">DB연결객체</param>
        /// <param name="strSPName">SP명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns>Select결과</returns>
        public DataTable mfExecReadStoredProc(SqlConnection Conn, string strSPName, params SqlParameter[] parSPParameter)
        {
            DataTable dt = new DataTable();
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    m_SqlCmd.Parameters.AddRange(parSPParameter);
                    //for (int i = 0; i < parSPParameter.Length; i++)
                    //    m_SqlCmd.Parameters.Add(parSPParameter[i]);

                    SqlDataAdapter da = new SqlDataAdapter(m_SqlCmd);
                    da.Fill(dt);
                }                    
                return dt;
            }
            catch (Exception ex)
            {
                //throw (ex);
                return dt;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 읽기 저장프로시저 실행 : 인자(데이터테이블)가 있는 경우
        /// </summary>
        /// <param name="Conn">DB연결객체</param>
        /// <param name="strSPName">SP명</param>
        /// <param name="dtSPParameter">인자 배열</param>
        /// <returns>Select결과</returns>
        public DataTable mfExecReadStoredProc(SqlConnection Conn, string strSPName, DataTable dtSPParameter)
        {
            DataTable dt = new DataTable();
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    m_SqlCmd.CommandTimeout = 10000; //搜索结果数据量大时会超时，增大CommandTimeout(2015-04-16)
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];

                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();
                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }
                        m_SqlCmd.Parameters.Add(param);
                    }

                    SqlDataAdapter da = new SqlDataAdapter(m_SqlCmd);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception ex)
            {
                //throw (ex);
                return dt;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 日别品质综合现状，搜索结果数据量大时会超时，增大CommandTimeout
        /// </summary>  
        public DataTable mfExecReadStoredProc_frmSTA0072(SqlConnection Conn, string strSPName, DataTable dtSPParameter) 
        {
            DataTable dt = new DataTable();
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    m_SqlCmd.CommandTimeout = 10000; //日别品质综合现状，搜索结果数据量大时会超时，增大CommandTimeout(2015-03-27)
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];

                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();
                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }
                        m_SqlCmd.Parameters.Add(param);
                    }

                    SqlDataAdapter da = new SqlDataAdapter(m_SqlCmd);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception ex)
            {
                //throw (ex);
                return dt;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 트랜잭션(DML) 저장프로시저 실행 : 인자(param배열)가 있는 경우
        /// </summary>
        /// <param name="Conn">DB연결객체</param>
        /// <param name="Trans">트랜잭션객체</param>
        /// <param name="strSPName">SP명</param>
        /// <param name="parSPParameter">인자배열</param>
        /// <returns>DML실행결과</returns>
        public string mfExecTransStoredProc(SqlConnection Conn, SqlTransaction Trans, string strSPName, params SqlParameter[] parSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn; // m_SqlCon;
                    m_SqlCmd.Transaction = Trans;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    m_SqlCmd.Parameters.AddRange(parSPParameter);
                    //for (int i = 0; i < parSPParameter.Length; i++)
                    //    m_SqlCmd.Parameters.Add(parSPParameter[i]);

                    string strReturn = Convert.ToString(m_SqlCmd.ExecuteScalar());
                    //m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_SqlCmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_SqlCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    //Result.mfInitReturnValue();
                    for (int i = 0; i < parSPParameter.Length; i++)
                    {
                        if (parSPParameter[i].ParameterName != "@ErrorMessage" && parSPParameter[i].Direction == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_SqlCmd.Parameters[i].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 트랜잭션(DML) 저장프로시저 실행 : 인자(데이터테이블)가 있는 경우
        /// </summary>
        /// <param name="Conn">DB연결객체</param>
        /// <param name="Trans">트랜잭션객체</param>
        /// <param name="strSPName">SP명</param>
        /// <param name="dtSPParameter">인자배열</param>
        /// <returns>DML실행결과</returns>
        public string mfExecTransStoredProc(SqlConnection Conn, SqlTransaction Trans, string strSPName, DataTable dtSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    m_SqlCmd.Transaction = Trans;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        //if (dr["Value"].ToString() != "")
                            param.Value = dr["Value"].ToString();

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }

                        m_SqlCmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_SqlCmd.ExecuteScalar());
                    //m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_SqlCmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_SqlCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_SqlCmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                
            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }        
        #endregion


        /// <summary>
        /// 트랜잭션(DML) 저장프로시저 실행 : 인자(데이터테이블)가 있는 경우, 트랜잭션을 DB에서 처리하는 경우
        /// </summary>
        /// <param name="Conn">DB연결객체</param>
        /// <param name="Trans">트랜잭션객체</param>
        /// <param name="strSPName">SP명</param>
        /// <param name="dtSPParameter">인자배열</param>
        /// <returns>DML실행결과</returns>
        public string mfExecTransStoredProc(SqlConnection Conn, string strSPName, DataTable dtSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    //m_SqlCmd.Transaction = Trans;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        //if (dr["Value"].ToString() != "")
                            param.Value = dr["Value"].ToString();

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }

                        m_SqlCmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_SqlCmd.ExecuteScalar());
                    //m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_SqlCmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_SqlCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_SqlCmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                
            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }        
        #endregion

        #region 저장프로시져에 넘겨줄 Parameter 정의
        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 데이터테이블 설정
        /// </summary>
        /// <returns>Parameter테이블</returns>
        public DataTable mfSetParamDataTable()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable();

                DataColumn dc = new DataColumn("ParamName", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("ParamDirect", typeof(ParameterDirection));
                dt.Columns.Add(dc);

                dc = new DataColumn("DBType", typeof(SqlDbType));
                dt.Columns.Add(dc);

                dc = new DataColumn("Value", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("Length", typeof(int));
                dt.Columns.Add(dc);

                return dt;

                dt.Clear();
                dt = null;

                dc = null;
            }
            catch (Exception ex)
            {
                //throw ex;
                return dt;
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="strName">Paramter 명</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="strValue">Paramter 인자값</param>
        /// <param name="intSize">Parameter 크기</param>
        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, string strValue, int intSize)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = strValue;
                dr["Length"] = intSize;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="strValue">Paramter 인자값</param>
        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, string strValue)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = strValue;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="strName">Paramter 명</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="strName">Paramter 명</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="intSize">Parameter 크기</param>
        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, SqlDbType DBType, int intSize)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Length"] = intSize;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }
    }
    #endregion

    #region Oracle DB 처리 Class
    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("ORADB")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ORADB : ServicedComponent
    {
        private string m_strConnString;
        private static OracleConnection m_OraCon;

        public string ConnString
        {
            get { return m_strConnString; }
        }

        public OracleConnection SqlCon
        {
            get { return m_OraCon; }
        }

        public ORADB()
        {

        }

        /// <summary>
        /// 생성자 : DB연결문자 생성하기
        /// </summary>
        /// <param name="strDataSource"></param>
        /// <param name="strUserID"></param>
        /// <param name="strPassword"></param>
        //public ORADB(string strDataSource, string strUserID, string strPassword)
        //{
        //    m_strConnString = "data source=" + strDataSource + ";user id=" + strUserID + ";password=" + strPassword + ";persist security info=true";
        //}

        public void mfSetORADBConnectString(string strDataSource, string strUserID, string strPassword)
        {
            m_strConnString = "data source=" + strDataSource + ";user id=" + strUserID + ";password=" + strPassword + ";persist security info=true";
        }

        public void mfDispose()
        {
            mfDisConnect();
            //this.Dispose();
        }

        /// <summary>
        /// Oracle DB연결
        /// </summary>
        /// <returns>연결여부</returns>
        [AutoComplete]
        public bool mfConnect()
        {
            try
            {
                //연결정보가 없는 경우
                if (m_strConnString == "")
                    return false;
                else
                {
                    m_OraCon = new OracleConnection(m_strConnString);
                    //Open상태가 아니면 Open한다.
                    if (m_OraCon.State != ConnectionState.Open)
                        m_OraCon.Open();

                    if (m_OraCon.State != ConnectionState.Open)
                        return false;
                    else
                        return true;
                }
            }
            catch (Exception ex)
            {
                //throw (ex);
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Oracle DB연결 끊기
        /// </summary>
        [AutoComplete]
        public void mfDisConnect()
        {
            try
            {
                if (m_OraCon.State != ConnectionState.Closed)
                {
                    m_OraCon.Close();
                    m_OraCon = null;
                }
                else
                    m_OraCon = null;
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }

        #region 저장프로시저 실행
        /// <summary>
        /// 저장프로시저 읽기 함수 실행 : SP함수명만 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <returns></returns>
        public DataTable mfExecReadStoredProc(OracleConnection Conn, string strSPName)
        {
            DataTable dt = new DataTable();
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    OracleCommand m_SqlCmd = new OracleCommand();
                    m_SqlCmd.Connection = Conn;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;

                    OracleDataAdapter da = new OracleDataAdapter(m_SqlCmd);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception ex)
            {
                //throw (ex);
                return dt;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시저 읽기 함수 실행 : SP함수명 및 인자(param배열)가 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns></returns>
        public DataTable mfExecReadStoredProc(OracleConnection Conn, string strSPName, params OracleParameter[] parSPParameter)
        {
            DataTable dt = new DataTable();
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    OracleCommand m_OraCmd = new OracleCommand();
                    m_OraCmd.Connection = Conn;
                    m_OraCmd.CommandType = CommandType.StoredProcedure;
                    m_OraCmd.CommandText = strSPName;
                    m_OraCmd.Parameters.AddRange(parSPParameter);
                    //for (int i = 0; i < parSPParameter.Length; i++)
                    //    m_SqlCmd.Parameters.Add(parSPParameter[i]);

                    //SqlDataAdapter da = new SqlDataAdapter(m_OraCmd);
                    OracleDataAdapter da = new OracleDataAdapter(m_OraCmd);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception ex)
            {
                //throw (ex);
                return dt;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시저 읽기 함수 실행 : SP함수명 및 인자(데이터테이블)가 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns></returns>
        public DataTable mfExecReadStoredProc(OracleConnection Conn, string strSPName, DataTable dtSPParameter)
        {
            DataTable dt = new DataTable();
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    OracleCommand m_OraCmd = new OracleCommand();
                    m_OraCmd.Connection = Conn;
                    m_OraCmd.CommandType = CommandType.StoredProcedure;
                    m_OraCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        OracleParameter param = new OracleParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.OracleDbType = (OracleDbType)dr["DBType"];
                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();
                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }
                        m_OraCmd.Parameters.Add(param);
                    }

                    //SqlDataAdapter da = new SqlDataAdapter(m_SqlCmd);
                    OracleDataAdapter da = new OracleDataAdapter(m_OraCmd);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception ex)
            {
                //throw (ex);
                return dt;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시저 트랙잭션(DML) 함수 실행 : SP함수명 및 인자(param배열)가 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns></returns>
        public string mfExecTransStoredProc(OracleConnection Conn, OracleTransaction Trans, string strSPName, params OracleParameter[] parSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                if (Conn.State == ConnectionState.Open)
                {
                    OracleCommand m_OraCmd = new OracleCommand();
                    m_OraCmd.Connection = Conn; // m_SqlCon;
                    m_OraCmd.Transaction = Trans;
                    m_OraCmd.CommandType = CommandType.StoredProcedure;
                    m_OraCmd.CommandText = strSPName;
                    m_OraCmd.Parameters.AddRange(parSPParameter);
                    //for (int i = 0; i < parSPParameter.Length; i++)
                    //    m_SqlCmd.Parameters.Add(parSPParameter[i]);

                    string strReturn = Convert.ToString(m_OraCmd.ExecuteScalar());
                    //m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_OraCmd.Parameters["RTN"].Value);
                    //Result.ErrMessage = m_OraCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    //Result.mfInitReturnValue();
                    for (int i = 0; i < parSPParameter.Length; i++)
                    {
                        if (parSPParameter[i].ParameterName != "@ErrorMessage" && parSPParameter[i].Direction == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_OraCmd.Parameters[i].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시저 트랙잭션(DML) 함수 실행 : SP함수명 및 인자(데이터테이블)가 있는 경우
        /// </summary>
        /// <param name="strSPName">SP함수명</param>
        /// <param name="parSPParameter">인자 배열</param>
        /// <returns></returns>
        public string mfExecTransStoredProc(OracleConnection Conn, OracleTransaction Trans, string strSPName, DataTable dtSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                //if (mfConnect() == true)
                if (Conn.State == ConnectionState.Open)
                {
                    OracleCommand m_OraCmd = new OracleCommand();
                    m_OraCmd.Connection = Conn;
                    m_OraCmd.Transaction = Trans;
                    m_OraCmd.CommandType = CommandType.StoredProcedure;
                    m_OraCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        OracleParameter param = new OracleParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.OracleDbType = (OracleDbType)dr["DBType"];
                        param.IsNullable = true;

                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }

                        m_OraCmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_OraCmd.ExecuteScalar());
                    //m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    //Result.ErrNum = Convert.ToInt32(m_OraCmd.Parameters["RTN"].Value);
                    string strRtn = m_OraCmd.Parameters["RTN"].Value.ToString();
                    Result.ErrNum = Convert.ToInt32(strRtn);
                    //Result.ErrMessage = m_OraCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_OraCmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }
                else
                {
                    Result.ErrNum = 99;
                    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                }

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }
        #endregion


        #region 저장프로시져에 넘겨줄 Parameter 정의
        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 테이블 설정
        /// </summary>
        /// <returns></returns>
        public DataTable mfSetParamDataTable()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable();

                DataColumn dc = new DataColumn("ParamName", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("ParamDirect", typeof(ParameterDirection));
                dt.Columns.Add(dc);

                dc = new DataColumn("DBType", typeof(OracleDbType));
                dt.Columns.Add(dc);

                dc = new DataColumn("Value", typeof(string));
                dt.Columns.Add(dc);

                dc = new DataColumn("Length", typeof(int));
                dt.Columns.Add(dc);

                return dt;

                dt.Clear();
                dt = null;

                dc = null;
            }
            catch (Exception ex)
            {
                //throw ex;
                return dt;
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="strName">Paramter 명</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="strValue">Paramter 인자값</param>
        /// <param name="intSize">Parameter 크기</param>
        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, OracleDbType DBType, string strValue, int intSize)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = strValue;
                dr["Length"] = intSize;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="strName">Paramter 명</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="strValue">Paramter 인자값</param>
        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, OracleDbType DBType, string strValue)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Value"] = strValue;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="strName">Paramter 명</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, OracleDbType DBType)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장프로시져에 넘겨줄 Parameter 추가
        /// </summary>
        /// <param name="dt">Parameter 테이블</param>
        /// <param name="strName">Paramter 명</param>
        /// <param name="Direction">Parameter 방향(in/out)</param>
        /// <param name="DBType">Parameter DB유형</param>
        /// <param name="intSize">Parameter 크기</param>
        public void mfAddParamDataRow(DataTable dt, string strName, ParameterDirection Direction, OracleDbType DBType, int intSize)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ParamName"] = strName;
                dr["ParamDirect"] = Direction;
                dr["DBType"] = DBType;
                dr["Length"] = intSize;
                dt.Rows.Add(dr);
            }
            catch (Exception ex)
            {
                //throw (ex);
            }
            finally
            {
            }
        }
        #endregion
    }
    #endregion

    #region 트랜잭션 처리시 Return 처리
    [Serializable]
    public class TransErrRtn
    {
        private int intErrNum;              //에러번호
        private string strErrMessage;       //에러메세지    
        //private ArrayList arrReturnValue;   //반환할 결과값
        private ArrayList arrReturnValue = new ArrayList();
        private string strSystemMessage;
        private string strSystemStackTrace;
        private string strSystemInnerException;
        private string strInterfaceResultCode;
        private string strInterfaceResultMessage;

        public int ErrNum
        {
            get { return intErrNum; }
            set { intErrNum = value; }
        }

        public string ErrMessage
        {
            get { return strErrMessage; }
            set { strErrMessage = value; }
        }

        public string SystemMessage
        {
            get { return strSystemMessage; }
            set { strSystemMessage = value; }
        }

        public string SystemStackTrace
        {
            get { return strSystemStackTrace; }
            set { strSystemStackTrace = value; }
        }

        public string SystemInnerException
        {
            get { return strSystemInnerException; }
            set { strSystemInnerException = value; }
        }

        public string InterfaceResultCode
        {
            get { return strInterfaceResultCode; }
            set { strInterfaceResultCode = value; }
        }

        public string InterfaceResultMessage
        {
            get { return strInterfaceResultMessage; }
            set { strInterfaceResultMessage = value; }
        }

        public TransErrRtn()
        {
            intErrNum = 0;
            strErrMessage = "";
            //arrReturnValue = null;
            //arrReturnValue.Clear();
            strSystemMessage = "";
            strSystemStackTrace = "";
            strSystemInnerException = "";
            strInterfaceResultCode = "";
            strInterfaceResultMessage = "";
        }

        /// <summary>
        /// 리턴값 배열 초기화
        /// </summary>
        public void mfInitReturnValue()
        {
            arrReturnValue.Clear();
        }

        /// <summary>
        /// 리턴값 배열에 값을 추가
        /// </summary>
        /// <param name="strValue"></param>
        public void mfAddReturnValue(string strValue)
        {
            arrReturnValue.Add(strValue);
        }

        /// <summary>
        /// 리턴갑 배열에 값을 삭제
        /// </summary>
        /// <param name="intIndex"></param>
        public void mfDeleteReturnValue(int intIndex)
        {
            arrReturnValue.Remove(intIndex);
        }

        /// <summary>
        /// 리턴값 배열 얻기
        /// </summary>
        /// <returns></returns>
        public ArrayList mfGetReturnValue()
        {
            return arrReturnValue;
        }

        /// <summary>
        /// 리턴갑 배열중 특정값 얻기
        /// </summary>
        /// <param name="intIndex"></param>
        /// <returns></returns>
        public string mfGetReturnValue(int intIndex)
        {
            if (arrReturnValue.Count >= intIndex + 1)
            {
                return arrReturnValue[intIndex].ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 에러메세지 구조체 정보를 문자열로 변환
        /// </summary>
        /// <param name="Err">트랜잭션처리정보 구조체</param>
        /// <returns>Encoding값</returns>
        public string mfEncodingErrMessage(TransErrRtn Err)
        {
            string strErr = "";
            string strErrSep = "<Err>";
            string strOutSep = "<OUT>";
            try
            {
                strErr = Err.intErrNum.ToString() + strErrSep +
                         Err.strErrMessage + strErrSep +
                         Err.strSystemMessage + strErrSep +
                         Err.strSystemStackTrace + strErrSep +
                         Err.strSystemInnerException + strErrSep +
                         Err.strInterfaceResultCode + strErrSep +       //추가
                         Err.strInterfaceResultMessage + strErrSep;     //추가

                if (Err.arrReturnValue.Count > 0)
                {
                    //strErr = strErr + "OUTPUT";
                    for (int i = 0; i < Err.arrReturnValue.Count; i++)
                    {
                        strErr = strErr + Err.arrReturnValue[i].ToString() + strOutSep;
                    }
                }
                return strErr;

            }
            catch (Exception ex)
            {
                return strErr;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 에러메시지 문자를 구조체로 변환
        /// </summary>
        /// <param name="strErr">트랜잭션처리정보 문자열</param>
        /// <returns>Decoding값</returns>
        public TransErrRtn mfDecodingErrMessage(string strErr)
        {
            TransErrRtn errMsg = new TransErrRtn();
            try
            {
                string[] arrErrSep = { "<Err>" };
                string[] arrOutSep = { "<OUT>" };

                string[] arrErrMsg = strErr.Split(arrErrSep, StringSplitOptions.None);

                errMsg.intErrNum = Convert.ToInt32(arrErrMsg[0]);
                if(arrErrMsg.Length > 1)
                    errMsg.strErrMessage = arrErrMsg[1];

                if(arrErrMsg.Length > 2)
                    errMsg.strSystemMessage = arrErrMsg[2];

                if(arrErrMsg.Length > 3)
                    errMsg.strSystemStackTrace = arrErrMsg[3];

                if(arrErrMsg.Length > 4)
                    errMsg.strSystemInnerException = arrErrMsg[4];

                if(arrErrMsg.Length > 5)
                    errMsg.strInterfaceResultCode = arrErrMsg[5];       //추가

                if(arrErrMsg.Length > 6)
                    errMsg.strInterfaceResultMessage = arrErrMsg[6];    //추가

                
                if (strErr.Split(arrOutSep, StringSplitOptions.None).Length > 0)
                {
                    string strtemp = strErr.Substring(strErr.LastIndexOf("<Err>") + arrErrSep[0].Length, strErr.Length - strErr.LastIndexOf("<Err>") - arrErrSep[0].Length);
                    string[] arrOutput = strtemp.Split(arrOutSep, StringSplitOptions.None);

                    for (int i = 0; i < arrOutput.Length - 1; i++)
                        errMsg.mfAddReturnValue(arrOutput[i]);
                }
                return errMsg;
            }
            catch (Exception ex)
            {
                return errMsg;
            }
            finally
            {
            }
        }
    }
    #endregion
}
