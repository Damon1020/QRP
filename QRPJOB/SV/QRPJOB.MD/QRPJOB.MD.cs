﻿/*----------------------------------------------------------------------*/
/* 시스템명     : QRPJOB.MD                                              */
/* 모듈(분류)명 : MDM Interface                                          */
/* 프로그램ID   : QRPJOB.MD                                         */
/* 프로그램명   : QRPJOB.MD                                          */
/* 작성자       : 이종민                                             */
/* 작성일자     : 2011-10-14                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Net;

using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting;
using System.Configuration;


namespace QRPJOB.MD
{
    public partial class BATJOB : ServiceBase
    {
        #region Var
        private System.Threading.Timer m_Timer;      //사용안함
        private System.Windows.Forms.Timer m_t = null; // 사용안함
        private System.Timers.Timer m_tt = null;

        private static IPHostEntry ip;
        private const string m_strUserID = "QRPServer";

        #endregion

        public BATJOB()
        {
            InitializeComponent();

            #region Timer 설정
            //this.m_t = new System.Windows.Forms.Timer();

            //this.m_t.Interval = 6000;
            //this.m_t.Enabled = true;
            //this.m_t.Tick += new System.EventHandler(this.m_T_Tick);

            m_tt = new System.Timers.Timer();
            m_tt.Elapsed += new System.Timers.ElapsedEventHandler(m_tt_Tick);
            m_tt.AutoReset = false;
            m_tt.Interval = 10000;
            //m_tt.Enabled = true;
            #endregion

            // Garbage Collector 에서 타이머를 Dispose 시키지 않게 설정
            GC.KeepAlive(m_tt);
        }

        /// <summary>
        /// 윈도우즈 서비스가 시작될때 발생
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            ip = Dns.GetHostEntry(Dns.GetHostName());

            System.Threading.Thread.Sleep(15000);
            
            #region 설정값 Read
            //m_Timer = new System.Threading.Timer(new TimerCallback(m_Timer_Tick), null, 1000, 60000);
            //m_t.Start();
            m_tt.Start(); //Timer Tick 시작
            #endregion
        }

        protected override void OnStop()
        {
            //m_Timer.Dispose();
            //m_t.Stop();
            m_tt.Stop();
        }
        
        /// <summary>
        /// System.Threading.Timer m_Timer 용 Tick 이벤트
        /// (사용안함)
        /// </summary>
        /// <param name="SPName"></param>
        private void m_Timer_Tick(object SPName)
        {
            #region BL
            QRPCOM.QRPGLO.TransErrRtn errRtn = new QRPCOM.QRPGLO.TransErrRtn();
            int i = 0;
            //DataTable dtMDM;
            try
            {
                mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.MDM), "MDM");
                QRPJOB.BL.BATJOB.MDM clsImport = new QRPJOB.BL.BATJOB.MDM();
                mfCredentials(clsImport);

                DataTable dtMDM = clsImport.mfReadMDMIF();
                
                for (i = 0; i < dtMDM.Rows.Count; i++)
                {
                    string strSPName = dtMDM.Rows[i]["SPName"].ToString();
                    string strIF_ID = dtMDM.Rows[i]["IF_ID"].ToString();
                    string strIF_VIEW = dtMDM.Rows[i]["IF_VIEW"].ToString();

                    string strRtn = clsImport.mfSaveMDMIF(strSPName, strIF_ID, strIF_VIEW, ip.AddressList[0].ToString(), m_strUserID);

                    errRtn = errRtn.mfDecodingErrMessage(strRtn);

                    if (!errRtn.ErrNum.Equals(0))
                    {
                        mfWindowsLogcheck(errRtn);
                    }
                }

                //clsImport.mfDisconnect();
            }
            catch (Exception ex)
            {
                errRtn.ErrNum = 99;
                errRtn.ErrMessage = ex.Message.ToString();
                errRtn.SystemStackTrace = ex.StackTrace.ToString();
                mfWindowsLogcheck(errRtn);
            }
            finally
            {
            }
            #endregion
        }

        /// <summary>
        /// System.Windows.Forms.Timer m_t 용 Tick 이벤트
        /// (사용안함)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_T_Tick(object sender, EventArgs e)
        {
            #region BL
            QRPCOM.QRPGLO.TransErrRtn errRtn = new QRPCOM.QRPGLO.TransErrRtn();
            
            int i = 0;
            //DataTable dtMDM;
            try
            {
                m_t.Stop();
                mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.MDM), "MDM");
                QRPJOB.BL.BATJOB.MDM clsImport = new QRPJOB.BL.BATJOB.MDM();
                mfCredentials(clsImport);

                DataTable dtMDM = clsImport.mfReadMDMIF();

                for (i = 0; i < dtMDM.Rows.Count; i++)
                {
                    string strSPName = dtMDM.Rows[i]["SPName"].ToString();
                    string strIF_ID = dtMDM.Rows[i]["IF_ID"].ToString();
                    string strIF_VIEW = dtMDM.Rows[i]["IF_VIEW"].ToString();

                    string strRtn = clsImport.mfSaveMDMIF(strSPName, strIF_ID, strIF_VIEW, ip.AddressList[0].ToString(), m_strUserID);

                    errRtn = errRtn.mfDecodingErrMessage(strRtn);

                    if (!errRtn.ErrNum.Equals(0))
                    {
                        mfWindowsLogcheck(errRtn);
                    }
                }
                //clsImport.mfDisconnect();
            }
            catch (Exception ex)
            {
                errRtn.ErrNum = 99;
                errRtn.ErrMessage = ex.Message.ToString();
                errRtn.SystemStackTrace = ex.StackTrace.ToString();
                mfWindowsLogcheck(errRtn);
            }
            finally
            {
                m_t.Start();
            }
            #endregion
        }

        /// <summary>
        /// System.Timers.Timer m_tt 용 Elapsed 이벤트 (Tick)
        /// (사용)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_tt_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            #region BL
            m_tt.Stop();
            QRPCOM.QRPGLO.TransErrRtn errRtn = new QRPCOM.QRPGLO.TransErrRtn();

            try
            {
                mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.MDM), "MDM");
                QRPJOB.BL.BATJOB.MDM clsImport = new QRPJOB.BL.BATJOB.MDM(); //"data source=10.60.24.173;user id=QRPUSR;password=jinjujin; Initial Catalog=QRP_STS_DEV;persist security info=true");
                mfCredentials(clsImport);

                /// SYSMDMIFMaster JOIN MDM..MDMUSER.IF_DETL 해서 I/F 할 IF_ID, IF_VIEW, SPName Read
                DataTable dtMDM = clsImport.mfReadMDMIF();

                if (dtMDM.Rows.Count.Equals(0))
                {
                    m_tt.Start();
                    return;
                }

                string strRtn = clsImport.mfSaveMDMIF_Total(dtMDM, ip.AddressList[0].ToString(), m_strUserID);

                errRtn = errRtn.mfDecodingErrMessage(strRtn);

                if (!errRtn.ErrNum.Equals(0))
                {
                    mfWindowsLogcheck(errRtn);
                    m_tt.Start();
                    return;
                }
            }
            catch (Exception ex)
            {
                errRtn.ErrNum = 99;
                errRtn.ErrMessage = ex.Message.ToString();
                errRtn.SystemStackTrace = ex.StackTrace.ToString();
                mfWindowsLogcheck(errRtn);
            }
            finally
            {
                m_tt.Start();
            }
            #endregion
        }

        void mfWindowsLogcheck(QRPCOM.QRPGLO.TransErrRtn _errRtn)
        {
            string sSource = "QRPJOB.MDM.EventLog";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);
            
            EventLog.WriteEntry(sSource, _errRtn.ErrMessage.ToString() , EventLogEntryType.Error, _errRtn.ErrNum);
        }

        #region Remoting
        /// <summary>
        /// .NET Remoting 인증 설정
        /// </summary>
        /// <param name="obj"></param>
        private void mfCredentials(object obj)
        {
            try
            {
                //char[] sep = { ';' };
                //string[] cre = ConfigurationManager.AppSettings["Credentials"].ToString().Split(sep);
                IDictionary Props = ChannelServices.GetChannelSinkProperties(obj);
                //IChannel ii = ChannelServices.GetChannel("

                //Props["credentials"] = new NetworkCredential(cre[0], cre[1]);
                Props["credentials"] = CredentialCache.DefaultCredentials;
                Props["preauthenticate"] = true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// .NET Remoting을 위한 채널 및 Proxy 생성
        /// </summary>
        /// <param name="TypeName"></param>
        /// <param name="strClassName"></param>
        private void mfRegisterChannel(Type TypeName, string strClassName)
        {
            try
            {
                WellKnownClientTypeEntry[] entry = RemotingConfiguration.GetRegisteredWellKnownClientTypes();
                foreach (WellKnownClientTypeEntry E in entry)
                {
                    if (E.TypeName.ToString() == TypeName.FullName.ToString())
                        return;
                }

                //채널 생성
                bool bolMakeChannel = false;
                foreach (IChannel chn in ChannelServices.RegisteredChannels)
                {
                    if (chn.ChannelName == "http")
                    {
                        bolMakeChannel = true;
                        break;
                    }
                }
                if (bolMakeChannel == false)
                {
                    HttpChannel channel = new HttpChannel();
                    string ch = channel.ChannelName;
                    ChannelServices.RegisterChannel(channel, true);
                }

                //서버활성화방식(Server Activated Object : SAO - WellKnwon 객체)에서 Proxy 객체 생성
                string RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString();
                //RemotingConfiguration.CustomErrorsMode = CustomErrorsModes.Off; //신규추가

                RemotingConfiguration.RegisterWellKnownClientType(TypeName, RemoteServer + strClassName + ".rem");

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }
        #endregion
    }
}
