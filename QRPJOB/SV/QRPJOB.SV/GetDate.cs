﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QRPJOB.SV
{
    static class GetDate
    {
        public static DateTime NearestQuarterEnd(this DateTime date)
        {
            IEnumerable<DateTime> candidates =
                QuartersInYear(date.Year).Union(QuartersInYear(date.Year - 1));
            return candidates.Where(d => d <= date).OrderBy(d => d).Last();
        }

        public static DateTime FirstDayInQuarter(this DateTime date)
        {
            IEnumerable<DateTime> candidates =
                QuartersInYear2(date.Year);

            return candidates.Where(d => d <= date).OrderBy(d => d).Last();
        }

        public static string MinusOneSec(this string strTime)
        {
            string[] str = strTime.Split(new char[] { ':' });
            return (new TimeSpan(Convert.ToInt32(str[0]), Convert.ToInt32(str[1]), Convert.ToInt32(str[2])) - new TimeSpan(0, 0, 1)).ToString();
        }

        static IEnumerable<DateTime> QuartersInYear(int year)
        {
            return new List<DateTime>() {
                    new DateTime(year, 3, 31),
                    new DateTime(year, 6, 30),
                    new DateTime(year, 9, 30),
                    new DateTime(year, 12, 31),
                                         };
        }

        static IEnumerable<DateTime> QuartersInYear2(int year)
        {
            return new List<DateTime>() {
                    new DateTime(year, 1, 1),
                    new DateTime(year, 4, 1),
                    new DateTime(year, 7, 1),
                    new DateTime(year, 10, 1),
                                         };
        }

        public static int Quarter(this DateTime date)
        {
            if (DateTime.Compare(date.Date, new DateTime(date.Year, 1, 1)) == 0)
            {
                return 1;
            }
            else if (DateTime.Compare(date.Date, new DateTime(date.Year, 4, 1)) == 0)
            {
                return 2;
            }
            else if (DateTime.Compare(date.Date, new DateTime(date.Year, 7, 1)) == 0)
            {
                return 3;
            }
            else
            {
                return 4;
            }
        }

        public static int GetWeekOfMonth(DateTime dt)
        {
            DateTime now = dt;

            int basisWeekOfDay = (now.Day - 1) % 7;
            int thisWeek = (int)now.DayOfWeek;

            double val = Math.Ceiling((double)now.Day / 7);

            if (basisWeekOfDay > thisWeek) val++;

            return Convert.ToInt32(val);

        }
    }
}
