﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Net;

using QRPCOM;
using System.Threading;
using QRPSTA;
using System.Globalization;
using System.Configuration;

namespace QRPJOB.SV
{
    public partial class BATJOB : ServiceBase
    {
        #region Var
        //System.Threading.Timer m_Timer;

        private static string m_strDaily;
        private static string m_strDailyUnit;
        private static string m_strDailyGubun
            ;
        private static string m_strWeekly;
        private static string m_strWeeklyUnit;
        private static string m_strWeeklyGubun;

        private static string m_strMonthly;
        private static string m_strMonthlyUnit;
        private static string m_strMonthlyGubun;

        private static string m_strQuarter;
        private static string m_strQuarterUnit;
        private static string m_strQuarterGubun;

        private static string m_strAnnual;
        private static string m_strAnnualUnit;
        private static string m_strAnnualGubun;

        private static IPHostEntry ip;
        private const string m_strUserID = "QRPServer";
        private System.Timers.Timer m_tt;

        private System.Timers.Timer tmDay;
        private System.Timers.Timer tmWeek;
        private System.Timers.Timer tmMonth;
        private System.Timers.Timer tmQuarter;
        private System.Timers.Timer tmYear;
        private string Server { get; set; }
        #endregion

        public BATJOB()
        {
            Server = string.Empty;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // 디버깅용 스레드.슬립
            //System.Threading.Thread.Sleep(10000);

            // 서비스 시작시 아규먼트로 메인서버인지 보조서버인지 구분 - Default 보조서버
            if (args.Length > 0)
            {
                if (args[0].ToLower().Contains("/main") || args[0].ToLower().Contains("/sub") || args[0].ToLower().Contains("/주") || args[0].ToLower().Contains("/부"))
                {
                    if (args[0].ToLower().Contains("/main") || args[0].ToLower().Contains("/주"))
                        Server = "Main";
                    else if (args[0].ToLower().Contains("/sub") || args[0].ToLower().Contains("/부"))
                        Server = "Sub";
                    else
                        Server = "Sub";
                }
            }
            else
            {
                Server = "Sub";
            }

            if (Server.Equals("Sub"))
            {
                mfReadMaster();
            }

            // 메인서버인 경우 보조서버 활동여부만 판단
            else if (Server.Equals("Main"))
            {
                m_tt = new System.Timers.Timer();
                m_tt.Elapsed += new System.Timers.ElapsedEventHandler(m_tt_Tick);
                m_tt.AutoReset = false;
                m_tt.Interval = 600000;

                m_tt.Start();
            }
            else
            {
                mfReadMaster();
            }

            ip = Dns.GetHostEntry(Dns.GetHostName());
        }

        private void mfReadMaster()
        {
            #region 설정값 Read
            //// 품질 batchjob 설정값 읽어오기 - SYSJOBMaster
            //// 서버에서 동작하는 프로그램이라 채널설정 필요 없다.
            ////QRPCOM.QRPGLO.QRPBrowser ////brwChannel = new ////QRPCOM.QRPGLO.QRPBrowser();

            ////brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.BatchJob), "BatchJob");
            QRPJOB.BL.BATJOB.BatchJob clsBatchJob = new QRPJOB.BL.BATJOB.BatchJob();
            ////brwChannel.mfCredentials(clsBatchJob);

            DataTable dtMaster = clsBatchJob.mfReadJOBMaster();
            #endregion

            #region Timer 설정
            // 각 조건별 스레드 이벤트 생성  // 일간/주간/월간/분기별/연간
            for (int i = 0; i < dtMaster.Rows.Count; i++)
            {
                if (dtMaster.Rows[i]["UseFlag"].ToString() == "T")
                {
                    switch (dtMaster.Rows[i]["TimerCode"].ToString())
                    {
                        case "DAY":
                            m_strDaily = dtMaster.Rows[i]["TickTime"].ToString();
                            m_strDailyUnit = dtMaster.Rows[i]["UnitTime"].ToString();
                            m_strDailyGubun = dtMaster.Rows[i]["TimerGubun"].ToString();

                            tmDay = new System.Timers.Timer();
                            tmDay.Elapsed += new System.Timers.ElapsedEventHandler(m_tDaily_Tick);
                            //tmDay.AutoReset = false;
                            tmDay.Interval = 60000;
                            
                            tmDay.Start();

                            //m_Timer = new Timer(new TimerCallback(m_tDaily_Tick), null, 0, 60000);
                            break;
                        case "WEEK":
                            m_strWeekly = dtMaster.Rows[i]["TickTime"].ToString();
                            m_strWeeklyUnit = dtMaster.Rows[i]["UnitTime"].ToString();
                            m_strWeeklyGubun = dtMaster.Rows[i]["TimerGubun"].ToString();

                            tmWeek = new System.Timers.Timer();
                            tmWeek.Elapsed += new System.Timers.ElapsedEventHandler(m_tWeekly_Tick);
                            //tmWeek.AutoReset = false;
                            tmWeek.Interval = 60000;

                            tmWeek.Start();

                            //m_Timer = new Timer(new TimerCallback(m_tWeekly_Tick), null, 0, 60000);
                            break;
                        case "MONTH":
                            m_strMonthly = dtMaster.Rows[i]["TickTime"].ToString();
                            m_strMonthlyUnit = dtMaster.Rows[i]["UnitTime"].ToString();
                            m_strMonthlyGubun = dtMaster.Rows[i]["TimerGubun"].ToString();


                            tmMonth = new System.Timers.Timer();
                            tmMonth.Elapsed += new System.Timers.ElapsedEventHandler(m_tMonthly_Tick);
                            //tmMonth.AutoReset = false;
                            tmMonth.Interval = 60000;

                            tmMonth.Start();

                            //m_Timer = new Timer(new TimerCallback(m_tMonthly_Tick), null, 0, 60000);
                            break;
                        case "QUARTER":
                            m_strQuarter = dtMaster.Rows[i]["TickTime"].ToString();
                            m_strQuarterUnit = dtMaster.Rows[i]["UnitTime"].ToString();
                            m_strQuarterGubun = dtMaster.Rows[i]["TimerGubun"].ToString();

                            tmQuarter = new System.Timers.Timer();
                            tmQuarter.Elapsed += new System.Timers.ElapsedEventHandler(m_tQuarter_Tick);
                            //tmQuarter.AutoReset = false;
                            tmQuarter.Interval = 60000;

                            tmQuarter.Start();

                            //m_Timer = new Timer(new TimerCallback(m_tQuarter_Tick), null, 0, 60000);
                            break;
                        case "YEAR":
                            m_strAnnual = dtMaster.Rows[i]["TickTime"].ToString();
                            m_strAnnualUnit = dtMaster.Rows[i]["UnitTime"].ToString();
                            m_strAnnualGubun = dtMaster.Rows[i]["TimerGubun"].ToString();

                            tmYear = new System.Timers.Timer();
                            tmYear.Elapsed += new System.Timers.ElapsedEventHandler(m_tAnnual_Tick);
                            //tmYear.AutoReset = false;
                            tmYear.Interval = 60000;

                            tmYear.Start();

                            //m_Timer = new Timer(new TimerCallback(m_tAnnual_Tick), null, 0, 60000);
                            break;
                    }
                }
            }
            #endregion
        }

        protected override void OnStop()
        {
            if (m_tt != null)
                m_tt.Stop();

            if (tmDay != null)
                tmDay.Stop();

            if (tmWeek != null)
                tmWeek.Stop();

            if (tmMonth != null)
                tmMonth.Stop();

            if (tmQuarter != null)
                tmQuarter.Stop();

            if (tmYear != null)
                tmYear.Stop();
        }

        #region Timer Event
        /// <summary>
        /// 일별 검사 Data 수집
        /// </summary>
        protected void m_tDaily_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmDay.Stop();
            string strFromDate = "";
            string strToDate = "";
            Boolean blCheck = false;

            try
            {
                #region DateCheck
                if (m_strDailyGubun == "T")
                {
                    string strUnitDate = DateTime.Now.ToString("yyyy-MM-dd") + " " + m_strDailyUnit;
                    string strTickTime = DateTime.Now.ToString("yyyy-MM-dd") + " " + m_strDaily;

                    if (Convert.ToDateTime(strUnitDate) >= Convert.ToDateTime(strTickTime))
                    {
                        strToDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + " " + GetDate.MinusOneSec(m_strDailyUnit);
                        strFromDate = DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd") + " " + m_strDailyUnit;
                    }
                    else
                    {
                        strToDate = DateTime.Now.ToString("yyyy-MM-dd") + " " + GetDate.MinusOneSec(m_strDailyUnit);
                        strFromDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + " " + m_strDailyUnit;
                    }

                    if (DateTime.Now.ToString("HH:mm") == m_strDaily.Substring(0, 5))
                    {
                        blCheck = true;
                    }
                    else
                    {
                        blCheck = false;
                    }
                }
                else if (m_strDailyGubun == "U")
                {
                    strFromDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + " " + m_strDailyUnit;
                    strToDate = DateTime.Now.ToString("yyyy-MM-dd") + " " + GetDate.MinusOneSec(m_strDailyUnit);

                    if (DateTime.Now.ToString("HH:mm") == m_strDailyUnit.Substring(0, 5))
                    {
                        blCheck = true;
                    }
                    else
                    {
                        blCheck = false;
                    }
                }
                #endregion

                #region ImportInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                    QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                    //brwChannel.mfCredentials(clsImport);
                    DataTable dtINS = new DataTable();
                    DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                        string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND VendorCode = '" + strVendorCode
                                        + "' AND MaterialCode = '" + strMaterialCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpect.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsImport.mfSaveImportInspectJob_Day(strFromDate.Substring(0, 10), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }

                    #endregion
                }
                #endregion

                #region ProcessInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                    QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                    //brwChannel.mfCredentials(clsProcess);
                    DataTable dtINS = new DataTable();
                    DataTable dtPackage = new DataTable();
                    DataTable dtPackageValue = new DataTable();

                    DataTable dtInpectValue = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                    #region ProcessInspect
                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                        string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND ProductCode = '" + strProductCode
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpectValue.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Day(strFromDate.Substring(0, 10), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #region ProcessInspectPackage
                    for (int i = 0; i < dtPackage.Rows.Count; i++)
                    {
                        string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                        string strPackage = dtPackage.Rows[i]["Package"].ToString();
                        string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND Package = '" + strPackage
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drPack = dtPackageValue.Select(strFilter);

                        if (drPack.Length != 0)
                        {
                            double[] arrValue = new double[drPack.Length];

                            for (int j = 0; j < drPack.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Day_Package(strFromDate.Substring(0, 10), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                //MessageBox.Show(ex.Message.ToString() + "\n" + ex.StackTrace.ToString() + "\n" + ex.TargetSite.ToString());
            }
            finally
            {
                tmDay.Start();
            }
        }

        /// <summary>
        /// 주별 검사 Data 수집
        /// </summary>
        protected void m_tWeekly_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmWeek.Stop();

            string strFromDate = "";
            string strToDate = "";
            Boolean blCheck = false;
            string[] strTime = m_strWeeklyUnit.Split(new char[] { ':' });

            try
            {
                #region DateCheck
                CultureInfo cl = CultureInfo.CurrentCulture;
                Calendar cal = cl.Calendar;
                DateTime _now = DateTime.Now;

                int weekoftoday = cal.GetWeekOfYear(_now, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
                int weekofyesterday = cal.GetWeekOfYear(_now.AddDays(-1), CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
                int weekofbefore = 0;
                if (weekoftoday - weekofyesterday == 1)
                {
                    strToDate = GetDate.MinusOneSec(_now.Date.ToString("yyyy-MM-dd") + " " + m_strWeeklyUnit);

                    Boolean bl = false;
                    int i = 1;
                    while (bl == false)
                    {
                        weekofbefore = cal.GetWeekOfYear(_now.AddDays(-1).AddDays(i * -1), CalendarWeekRule.FirstDay, DayOfWeek.Sunday);

                        if (weekofyesterday - weekofbefore == 1)
                        {
                            strFromDate = _now.AddDays(-1).AddDays(i * -1).ToString("yyyy-mm-dd") + " " + m_strWeeklyUnit;
                            bl = true;
                        }
                    }
                }
                else
                {
                    blCheck = false;
                }

                if (!blCheck)
                    return;


                if (m_strWeeklyGubun.Equals("T"))
                {
                    if (_now == DateTime.Parse(_now.Date.ToString("yyyy-MM-dd") + " " + m_strWeekly) &&
                        DateTime.Parse(strToDate) <= DateTime.Parse(_now.Date.ToString("yyyy-MM-dd") + " " + m_strWeekly))
                    {
                        blCheck = true;
                    }
                    else
                    {
                        blCheck = false;
                    }
                }
                else if (m_strWeeklyGubun.Equals("U"))
                {
                    if (_now == DateTime.Parse(_now.Date.ToString("yyyy-MM-dd") + " " + m_strWeeklyUnit))
                    {
                        blCheck = true;
                    }
                    else
                    {
                        blCheck = false;
                    }
                }
                #endregion

                #region ImportInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                    QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                    //brwChannel.mfCredentials(clsImport);
                    DataTable dtINS = new DataTable();
                    DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                        string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND VendorCode = '" + strVendorCode
                                        + "' AND MaterialCode = '" + strMaterialCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpect.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsImport.mfSaveImportInspectJob_Week(strFromDate.Substring(0, 4), strFromDate.Substring(4, 2), weekoftoday.ToString(), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }

                    #endregion
                }
                #endregion

                #region ProcessInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                    QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                    //brwChannel.mfCredentials(clsProcess);
                    DataTable dtINS = new DataTable();
                    DataTable dtPackage = new DataTable();
                    DataTable dtPackageValue = new DataTable();
                    DataTable dtInpectValue = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                    #region ProcessInspect
                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                        string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND ProductCode = '" + strProductCode
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpectValue.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Week(strFromDate.Substring(0, 4), strFromDate.Substring(4, 2), weekoftoday.ToString(), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #region ProcessInspectPackage
                    for (int i = 0; i < dtPackage.Rows.Count; i++)
                    {
                        string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                        string strPackage = dtPackage.Rows[i]["Package"].ToString();
                        string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND Package = '" + strPackage
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drPack = dtPackageValue.Select(strFilter);

                        if (drPack.Length != 0)
                        {
                            double[] arrValue = new double[drPack.Length];

                            for (int j = 0; j < drPack.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Week_Package(strFromDate.Substring(0, 4), strFromDate.Substring(4, 2), weekoftoday.ToString(), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                //MessageBox.Show(ex.Message.ToString() + "\n" + ex.StackTrace.ToString() + "\n" + ex.TargetSite.ToString());
            }
            finally
            {
                tmWeek.Start();
            }
        }

        /// <summary>
        /// 월별 검사 Data 수집
        /// </summary>
        protected void m_tMonthly_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmMonth.Stop();
            string strFromDate = "";
            string strToDate = "";
            Boolean blCheck = false;
            string[] strTime = m_strMonthlyUnit.Split(new char[] { ':' });

            try
            {
                #region DateCheck
                string strUnitDate = DateTime.Now.AddDays(1 - DateTime.Now.Day).ToString("yyyy-MM-dd") + " " + m_strMonthlyUnit;
                string strTickTime = DateTime.Now.ToString("yyyy-MM-dd") + " " + m_strMonthly;

                if (m_strMonthlyGubun == "T")
                {
                    if (mfCheckMinValue(DateTime.Parse(strUnitDate), DateTime.Parse(strTickTime)) &&
                        DateTime.Now.ToString("HH:mm") == m_strMonthly.Substring(0, 5))
                    {
                        strFromDate = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-01") + " " + m_strMonthlyUnit;
                        strToDate = DateTime.Now.ToString("yyyy-MM-01") + " " + GetDate.MinusOneSec(m_strMonthlyUnit);
                        blCheck = true;
                    }
                }
                else if (m_strMonthlyGubun == "U")
                {
                    if (mfCheckMinValue(DateTime.Parse(strUnitDate), DateTime.Now) &&
                        DateTime.Now.ToString("HH:mm") == m_strMonthlyUnit.Substring(0, 5))
                    {
                        strFromDate = DateTime.Now.AddMonths(-1).ToString("yyyy-MM-01") + " " + m_strMonthlyUnit;
                        strToDate = DateTime.Now.ToString("yyyy-MM-dd") + " " + GetDate.MinusOneSec(m_strMonthlyUnit);
                        blCheck = true;
                    }
                }
                #endregion

                #region ImportInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                    QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                    //brwChannel.mfCredentials(clsImport);
                    DataTable dtINS = new DataTable();
                    DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                        string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND VendorCode = '" + strVendorCode
                                        + "' AND MaterialCode = '" + strMaterialCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpect.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsImport.mfSaveImportInspectJob_Month(strFromDate.Substring(0, 7), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }

                    #endregion
                }
                #endregion

                #region ProcessInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                    QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                    //brwChannel.mfCredentials(clsProcess);
                    DataTable dtINS = new DataTable();
                    DataTable dtPackage = new DataTable();
                    DataTable dtPackageValue = new DataTable();
                    DataTable dtInpectValue = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                    #region ProcessInspect
                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                        string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND ProductCode = '" + strProductCode
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpectValue.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Month(strFromDate.Substring(0, 7), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #region ProcessInspectPackage
                    for (int i = 0; i < dtPackage.Rows.Count; i++)
                    {
                        string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                        string strPackage = dtPackage.Rows[i]["Package"].ToString();
                        string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND Package = '" + strPackage
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drPack = dtPackageValue.Select(strFilter);

                        if (drPack.Length != 0)
                        {
                            double[] arrValue = new double[drPack.Length];

                            for (int j = 0; j < drPack.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Month_Package(strFromDate.Substring(0, 7), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                //MessageBox.Show(ex.Message.ToString() + "\n" + ex.StackTrace.ToString() + "\n" + ex.TargetSite.ToString());
            }
            finally
            {
                tmMonth.Start();
            }
        }

        protected static Boolean mfCheckMinValue(DateTime dtUnitDate, DateTime dtTickDate)
        {
            if (DateTime.Compare(dtUnitDate, dtTickDate) > 0)
            {
                return false;
            }
            else
            {
                TimeSpan ts = dtTickDate - dtUnitDate;
                if (ts.Days > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

        }

        /// <summary>
        /// 분기별 검사 Data 수집
        /// </summary>
        protected void m_tQuarter_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmQuarter.Stop();
            string strUnitDate = DateTime.Now.AddDays(1 - DateTime.Now.Day).ToString("yyyy-MM-dd") + " " + m_strQuarterUnit;
            string strTickTime = DateTime.Now.ToString("yyyy-MM-dd") + " " + m_strQuarter;
            Boolean blCheck = false;
            DateTime dtQuarterFirst = DateTime.Parse(GetDate.NearestQuarterEnd(DateTime.Now).AddMonths(-2).ToString("yyyy-MM-01") + " " + m_strQuarterUnit);
            DateTime dtQuarterEnd = DateTime.Parse(GetDate.NearestQuarterEnd(DateTime.Now).AddDays(1).ToString("yyyy-MM-dd") + " " + GetDate.MinusOneSec(m_strQuarterUnit));
            string strFromDate = dtQuarterFirst.ToString("yyyy-MM-dd HH:mm:ss");
            string strToDate = dtQuarterEnd.ToString("yyyy-MM-dd HH:mm:ss");
            int numQuarter = GetDate.Quarter(dtQuarterFirst);

            try
            {
                #region DateCheck
                if (m_strQuarterGubun == "T")
                {
                    if (mfCheckMinValue(DateTime.Parse(strUnitDate), DateTime.Parse(strTickTime)) &&
                        DateTime.Now.ToString("HH:mm") == m_strQuarter.Substring(0, 5))
                    {
                        blCheck = true;
                    }
                }
                else if (m_strQuarterGubun == "U")
                {
                    if (mfCheckMinValue(DateTime.Parse(strUnitDate), DateTime.Now) &&
                        DateTime.Now.ToString("HH:mm") == m_strQuarterUnit.Substring(0, 5))
                    {
                        blCheck = true;
                    }
                }
                #endregion

                #region ImportInspectJob
                if (blCheck)
                {
                    #region ImportInspect
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                    QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                    //brwChannel.mfCredentials(clsImport);
                    DataTable dtINS = new DataTable();
                    DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                        string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND VendorCode = '" + strVendorCode
                                        + "' AND MaterialCode = '" + strMaterialCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpect.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsImport.mfSaveImportInspectJob_Quarter(dtQuarterFirst.Year.ToString(), numQuarter, dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }

                    #endregion

                    #region ImportInspectCL

                    DataTable dtReqItemCL = clsImport.mfReadImportInspectJob_CL(strFromDate, strToDate);

                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                        string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND VendorCode = '" + strVendorCode
                                        + "' AND MaterialCode = '" + strMaterialCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtReqItemCL.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            //double[] arrValue = new double[drINS.Length];
                            DataTable dtMean = new DataTable();
                            dtMean.Columns.Add("Mean");
                            DataTable dtDataRange = new DataTable();
                            dtDataRange.Columns.Add("DataRange");
                            DataTable dtStdDev = new DataTable();
                            dtStdDev.Columns.Add("StdDev");
                            DataTable dtInspectValue = new DataTable();
                            dtInspectValue.Columns.Add("InspectValue");

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                DataRow drMean = dtMean.NewRow();
                                DataRow drDataRange = dtDataRange.NewRow();
                                DataRow drStdDev = dtStdDev.NewRow();

                                drMean["Mean"] = drINS[j]["Mean"].ToString();
                                drDataRange["DataRange"] = drINS[j]["DataRange"].ToString();
                                drStdDev["StdDev"] = drINS[j]["StdDev"].ToString();

                                dtMean.Rows.Add(drMean);
                                dtDataRange.Rows.Add(drDataRange);
                                dtStdDev.Rows.Add(drStdDev);

                            }

                            DataRow[] drArrInspectValue = dtInpect.Select(strFilter);

                            if (!drArrInspectValue.Length.Equals(0))
                            {
                                for (int j = 0; j < drArrInspectValue.Length; j++)
                                {
                                    DataRow drInspectValue = dtInspectValue.NewRow();
                                    drInspectValue["InspectValue"] = drArrInspectValue[j]["InspectValue"].ToString();
                                    dtInspectValue.Rows.Add(drInspectValue);
                                }
                            }

                            STASPC cls = new STASPC();
                            STAControlLimit limit = new STAControlLimit();
                            limit.InitSTAControlLimit();
                            limit = cls.mfCalcControlLimit(dtInspectValue, dtMean, dtDataRange, dtStdDev);

                            DataTable dtValue = mfMakeDataTable_ImportInspetJob_CL(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                    dtQuarterFirst.Year.ToString(), numQuarter, limit);

                            clsImport.mfSaveImportInspectJob_Quarter_CL(dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion
                }
                #endregion

                #region ProcessInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                    QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                    //brwChannel.mfCredentials(clsProcess);
                    DataTable dtINS = new DataTable();
                    DataTable dtPackage = new DataTable();
                    DataTable dtPackageValue = new DataTable();
                    DataTable dtInspect = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                    #region ProcessInspect
                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                        string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND ProductCode = '" + strProductCode
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInspect.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Quarter(dtQuarterFirst.Year.ToString(), numQuarter, dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #region ProcessInspectCL
                    DataTable dtProcCL = clsProcess.mfReadProcessInspectJobCL(strFromDate, strToDate, "F");
                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                        string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND ProductCode = '" + strProductCode
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtProcCL.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            DataTable dtMean = new DataTable();
                            dtMean.Columns.Add("Mean");
                            DataTable dtStdDev = new DataTable();
                            dtStdDev.Columns.Add("StdDev");
                            DataTable dtDataRange = new DataTable();
                            dtDataRange.Columns.Add("DataRange");

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                DataRow drMean = dtMean.NewRow();
                                DataRow drDataRange = dtDataRange.NewRow();
                                DataRow drStdDev = dtStdDev.NewRow();

                                drMean["Mean"] = drINS[j]["Mean"].ToString();
                                drDataRange["DataRange"] = drINS[j]["DataRange"].ToString();
                                drStdDev["StdDev"] = drINS[j]["StdDev"].ToString();

                                dtMean.Rows.Add(drMean);
                                dtDataRange.Rows.Add(drDataRange);
                                dtStdDev.Rows.Add(drStdDev);
                            }

                            DataTable dtInspectValue = new DataTable();
                            dtInspectValue.Columns.Add("InspectValue");

                            DataRow[] drArrInspectValue = dtInspectValue.Select(strFilter);

                            for (int j = 0; j < drArrInspectValue.Length; j++)
                            {
                                DataRow drInspectValue = dtInspectValue.NewRow();
                                drInspectValue["InspectValue"] = drArrInspectValue[j]["InspectValue"].ToString();
                                dtInspectValue.Rows.Add(drInspectValue);
                            }

                            STASPC cls = new STASPC();
                            STAControlLimit limit = new STAControlLimit();
                            limit.InitSTAControlLimit();
                            limit = cls.mfCalcControlLimit(dtInspectValue, dtMean, dtDataRange, dtStdDev);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob_CL(strPlantCode, "", strProductCode, strStackSeq, strGeneration,
                                                strProcessCode, strInspectItemCode, dtQuarterFirst.Year.ToString(), numQuarter, limit);

                            clsProcess.mfSaveProcessInspectCL(dtValue, ip.AddressList[0].ToString(), m_strUserID, "F");
                        }
                    }
                    #endregion

                    #region ProcessInspectPackage
                    for (int i = 0; i < dtPackage.Rows.Count; i++)
                    {
                        string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                        string strPackage = dtPackage.Rows[i]["Package"].ToString();
                        string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND Package = '" + strPackage
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drPack = dtPackageValue.Select(strFilter);

                        if (drPack.Length != 0)
                        {
                            double[] arrValue = new double[drPack.Length];

                            for (int j = 0; j < drPack.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Quarter_Package(dtQuarterFirst.Year.ToString(), numQuarter, dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #region ProcessInspectPackageCL
                    dtProcCL = clsProcess.mfReadProcessInspectJobCL(strFromDate, strToDate, "T");
                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strPackage = dtINS.Rows[i]["Package"].ToString();
                        string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND Package = '" + strPackage
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtProcCL.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            DataTable dtMean = new DataTable();
                            dtMean.Columns.Add("Mean");
                            DataTable dtStdDev = new DataTable();
                            dtStdDev.Columns.Add("StdDev");
                            DataTable dtDataRange = new DataTable();
                            dtDataRange.Columns.Add("DataRange");

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                DataRow drMean = dtMean.NewRow();
                                DataRow drDataRange = dtDataRange.NewRow();
                                DataRow drStdDev = dtStdDev.NewRow();

                                drMean["Mean"] = drINS[j]["Mean"].ToString();
                                drDataRange["DataRange"] = drINS[j]["DataRange"].ToString();
                                drStdDev["StdDev"] = drINS[j]["StdDev"].ToString();

                                dtMean.Rows.Add(drMean);
                                dtDataRange.Rows.Add(drDataRange);
                                dtStdDev.Rows.Add(drStdDev);
                            }

                            DataTable dtInspectValue = new DataTable();
                            dtInspectValue.Columns.Add("InspectValue");

                            DataRow[] drArrInspectValue = dtInspect.Select(strFilter);

                            for (int j = 0; j < drArrInspectValue.Length; j++)
                            {
                                DataRow drInspectValue = dtInspectValue.NewRow();
                                drInspectValue["InspectValue"] = drArrInspectValue[j]["InspectValue"].ToString();
                                dtInspectValue.Rows.Add(drInspectValue);
                            }

                            STASPC cls = new STASPC();
                            STAControlLimit limit = new STAControlLimit();
                            limit.InitSTAControlLimit();
                            limit = cls.mfCalcControlLimit(dtInspectValue, dtMean, dtDataRange, dtStdDev);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob_CL(strPlantCode, strPackage, "", strStackSeq, strGeneration,
                                                strProcessCode, strInspectItemCode, dtQuarterFirst.Year.ToString(), numQuarter, limit);

                            clsProcess.mfSaveProcessInspectCL(dtValue, ip.AddressList[0].ToString(), m_strUserID, "T");
                        }
                    }
                    #endregion
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                //MessageBox.Show(ex.Message.ToString() + "\n" + ex.StackTrace.ToString() + "\n" + ex.TargetSite.ToString());
            }
            finally
            {
                tmQuarter.Start();
            }
        }

        /// <summary>
        /// 년별 검사 Data 수집
        /// </summary>
        protected void m_tAnnual_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmYear.Stop();
            string strUnitDate = DateTime.Now.AddDays(1 - DateTime.Now.Day).AddMonths(1 - DateTime.Now.Month).ToString("yyyy-MM-dd") + " " + m_strAnnualUnit;
            string strTickTime = DateTime.Now.ToString("yyyy-MM-dd") + " " + m_strAnnual;
            string strFromDate = "";
            string strToDate = "";
            Boolean blCheck = false;

            try
            {
                #region DateCheck
                if (m_strAnnualGubun == "T")
                {
                    if (mfCheckMinValue(DateTime.Parse(strUnitDate), DateTime.Parse(strTickTime)) &&
                        DateTime.Now.ToString("HH:mm") == m_strAnnual.Substring(0, 5))
                    {
                        strFromDate = DateTime.Parse(strUnitDate).AddYears(-1).ToString("yyyy-MM-dd") + " " + m_strAnnualUnit;
                        strToDate = DateTime.Now.ToString("yyyy-MM-01") + " " + GetDate.MinusOneSec(m_strAnnualUnit);
                        blCheck = true;
                    }
                }
                else if (m_strAnnualGubun == "U")
                {
                    if (mfCheckMinValue(DateTime.Parse(strUnitDate), DateTime.Now) &&
                        DateTime.Now.ToString("HH:mm") == m_strAnnualUnit.Substring(0, 5))
                    {
                        strFromDate = DateTime.Parse(strUnitDate).AddYears(-1).ToString("yyyy-MM-dd") + " " + m_strAnnualUnit;
                        strToDate = DateTime.Now.ToString("yyyy-MM-dd") + " " + GetDate.MinusOneSec(m_strAnnualUnit);
                        blCheck = true;
                    }
                }
                #endregion

                #region ImportInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                    QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                    //brwChannel.mfCredentials(clsImport);
                    DataTable dtINS = new DataTable();
                    DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                        string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND VendorCode = '" + strVendorCode
                                        + "' AND MaterialCode = '" + strMaterialCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpect.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsImport.mfSaveImportInspectJob_Year(strFromDate.Substring(0, 4), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }

                    #endregion
                }
                #endregion

                #region ProcessInspectJob
                if (blCheck)
                {
                    #region BL
                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                    QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                    //brwChannel.mfCredentials(clsProcess);
                    DataTable dtINS = new DataTable();
                    DataTable dtPackage = new DataTable();
                    DataTable dtPackageValue = new DataTable();
                    DataTable dtInpect = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                    #region ProcessInspect
                    for (int i = 0; i < dtINS.Rows.Count; i++)
                    {
                        string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                        string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                        string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND ProductCode = '" + strProductCode
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drINS = dtInpect.Select(strFilter);

                        if (drINS.Length != 0)
                        {
                            double[] arrValue = new double[drINS.Length];

                            for (int j = 0; j < drINS.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Year(strFromDate.Substring(0, 4), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #region ProcessInspectPackage
                    for (int i = 0; i < dtPackage.Rows.Count; i++)
                    {
                        string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                        string strPackage = dtPackage.Rows[i]["Package"].ToString();
                        string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                        string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                        string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                        string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                        string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                        string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                        int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                        double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                        double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                        string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                        string strFilter = "PlantCode = '" + strPlantCode
                                        + "' AND Package = '" + strPackage
                                        + "' AND StackSeq = '" + strStackSeq
                                        + "' AND Generation = '" + strGeneration
                                        + "' AND ProcessCode = '" + strProcessCode
                                        + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                        DataRow[] drPack = dtPackageValue.Select(strFilter);

                        if (drPack.Length != 0)
                        {
                            double[] arrValue = new double[drPack.Length];

                            for (int j = 0; j < drPack.Length; j++)
                            {
                                arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                            }

                            STASummary clsSum = new STASummary();
                            clsSum.InitSTASummary();

                            STABAS clsBas = new STABAS();
                            clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                            DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                                strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                                clsSum);

                            clsProcess.mfSaveProcessInspectJob_Year_Package(strFromDate.Substring(0, 4), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        }
                    }
                    #endregion

                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                //MessageBox.Show(ex.Message.ToString() + "\n" + ex.StackTrace.ToString() + "\n" + ex.TargetSite.ToString());
            }
            finally
            {
                tmYear.Start();
            }
        }
        #endregion

        #region DataTable
        private DataTable mfMakeDataTable_ImportInspectJob(string strPlantCode, string strMaterialCode, string strVendorCode,
                                           string strInspectItemCode, string strStdNumber, string strStdSeq,
                                           int numVersionNum, double numUpperSpec, double numLowerSpec,
                                           string strSpecRange, STASummary clsSum)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("MaterialCode");
                dtValue.Columns.Add("VendorCode");
                dtValue.Columns.Add("InspectItemCode");
                dtValue.Columns.Add("StdNumber");
                dtValue.Columns.Add("StdSeq");
                dtValue.Columns.Add("VersionNum");
                dtValue.Columns.Add("UpperSpec");
                dtValue.Columns.Add("LowerSpec");
                dtValue.Columns.Add("SpecRange");

                dtValue.Columns.Add("CountDA");
                dtValue.Columns.Add("SumDA");
                dtValue.Columns.Add("MeanDA");
                dtValue.Columns.Add("MinDA");
                dtValue.Columns.Add("MaxDA");
                dtValue.Columns.Add("RangeDA");
                dtValue.Columns.Add("VarianceDA");
                dtValue.Columns.Add("StdDevDA");
                dtValue.Columns.Add("MedianDA");
                dtValue.Columns.Add("CpDA");
                dtValue.Columns.Add("CpkDA");
                dtValue.Columns.Add("CplDA");
                dtValue.Columns.Add("CpuDA");
                dtValue.Columns.Add("CountDR");
                dtValue.Columns.Add("SumDR");
                dtValue.Columns.Add("MeanDR");
                dtValue.Columns.Add("MinDR");
                dtValue.Columns.Add("MaxDR");
                dtValue.Columns.Add("RangeDR");
                dtValue.Columns.Add("VarianceDR");
                dtValue.Columns.Add("StdDevDR");
                dtValue.Columns.Add("MedianDR");
                dtValue.Columns.Add("CpDR");
                dtValue.Columns.Add("CpkDR");
                dtValue.Columns.Add("CplDR");
                dtValue.Columns.Add("CpuDR");
                dtValue.Columns.Add("USLUpperCount");
                dtValue.Columns.Add("LSLLowerCount");
                dtValue.Columns.Add("UpwardRunCount");
                dtValue.Columns.Add("DownwardRunCount");
                dtValue.Columns.Add("UpwardTrendCount");
                dtValue.Columns.Add("DownwardTrendCount");
                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["MaterialCode"] = strMaterialCode;
                drValue["VendorCode"] = strVendorCode;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["StdNumber"] = strStdNumber;
                drValue["StdSeq"] = strStdSeq;
                drValue["VersionNum"] = numVersionNum;
                drValue["UpperSpec"] = numUpperSpec.ToString("#########0.#####");
                drValue["LowerSpec"] = numLowerSpec.ToString("#########0.#####");
                drValue["SpecRange"] = strSpecRange;

                drValue["CountDA"] = clsSum.CountDA;
                drValue["SumDA"] = clsSum.SumDA.ToString("##############0.#####");
                drValue["MeanDA"] = clsSum.MeanDA.ToString("##############0.#####");
                drValue["MinDA"] = clsSum.MinDA.ToString("##############0.#####");
                drValue["MaxDA"] = clsSum.MaxDA.ToString("##############0.#####");
                drValue["RangeDA"] = clsSum.RangeDA.ToString("##############0.#####");
                drValue["VarianceDA"] = clsSum.VarianceDA.ToString("##############0.#####");
                drValue["StdDevDA"] = clsSum.StdDevDA.ToString("##############0.#####");
                drValue["MedianDA"] = clsSum.MedianDA.ToString("##############0.#####");
                drValue["CpDA"] = clsSum.CpDA.ToString("#######0.#####");
                drValue["CpkDA"] = clsSum.CpkDA.ToString("#######0.#####");
                drValue["CplDA"] = clsSum.CplDA.ToString("#######0.#####");
                drValue["CpuDA"] = clsSum.CpuDA.ToString("#######0.#####");
                drValue["CountDR"] = clsSum.CountDR;
                drValue["SumDR"] = clsSum.SumDR.ToString("##############0.#####");
                drValue["MeanDR"] = clsSum.MeanDR.ToString("##############0.#####");
                drValue["MinDR"] = clsSum.MinDR.ToString("##############0.#####");
                drValue["MaxDR"] = clsSum.MaxDR.ToString("##############0.#####");
                drValue["RangeDR"] = clsSum.RangeDR.ToString("##############0.#####");
                drValue["VarianceDR"] = clsSum.VarianceDR.ToString("##############0.#####");
                drValue["StdDevDR"] = clsSum.StdDevDR.ToString("##############0.#####");
                drValue["MedianDR"] = clsSum.MedianDR.ToString("##############0.#####");
                drValue["CpDR"] = clsSum.CpDR.ToString("#######0.#####");
                drValue["CpkDR"] = clsSum.CpkDR.ToString("#######0.#####");
                drValue["CplDR"] = clsSum.CplDR.ToString("#######0.#####");
                drValue["CpuDR"] = clsSum.CpuDR.ToString("#######0.#####");
                drValue["USLUpperCount"] = clsSum.USLUpperCount;
                drValue["LSLLowerCount"] = clsSum.LSLLowerCount;
                drValue["UpwardRunCount"] = clsSum.UpwardRunCount;
                drValue["DownwardRunCount"] = clsSum.DownwardRunCount;
                drValue["UpwardTrendCount"] = clsSum.UpwardTrendCount;
                drValue["DownwardTrendCount"] = clsSum.DownwardTrendCount;
                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                return dtValue;
            }
            finally
            {

            }
        }

        private DataTable mfMakeDataTable_ProcessInspectJob(string strPlantCode, string strProductCode, string strStackSeq, string strGeneration,
                                           string strInspectItemCode, string strProcessCode, string strStdNumber, string strStdSeq,
                                           int numVersionNum, double numUpperSpec, double numLowerSpec,
                                           string strSpecRange, STASummary clsSum)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("ProductCode");
                dtValue.Columns.Add("StackSeq");
                dtValue.Columns.Add("Generation");
                dtValue.Columns.Add("InspectItemCode");
                dtValue.Columns.Add("ProcessCode");
                dtValue.Columns.Add("StdNumber");
                dtValue.Columns.Add("StdSeq");
                dtValue.Columns.Add("VersionNum");
                dtValue.Columns.Add("UpperSpec");
                dtValue.Columns.Add("LowerSpec");
                dtValue.Columns.Add("SpecRange");

                dtValue.Columns.Add("CountDA");
                dtValue.Columns.Add("SumDA");
                dtValue.Columns.Add("MeanDA");
                dtValue.Columns.Add("MinDA");
                dtValue.Columns.Add("MaxDA");
                dtValue.Columns.Add("RangeDA");
                dtValue.Columns.Add("VarianceDA");
                dtValue.Columns.Add("StdDevDA");
                dtValue.Columns.Add("MedianDA");
                dtValue.Columns.Add("CpDA");
                dtValue.Columns.Add("CpkDA");
                dtValue.Columns.Add("CplDA");
                dtValue.Columns.Add("CpuDA");
                dtValue.Columns.Add("CountDR");
                dtValue.Columns.Add("SumDR");
                dtValue.Columns.Add("MeanDR");
                dtValue.Columns.Add("MinDR");
                dtValue.Columns.Add("MaxDR");
                dtValue.Columns.Add("RangeDR");
                dtValue.Columns.Add("VarianceDR");
                dtValue.Columns.Add("StdDevDR");
                dtValue.Columns.Add("MedianDR");
                dtValue.Columns.Add("CpDR");
                dtValue.Columns.Add("CpkDR");
                dtValue.Columns.Add("CplDR");
                dtValue.Columns.Add("CpuDR");
                dtValue.Columns.Add("USLUpperCount");
                dtValue.Columns.Add("LSLLowerCount");
                dtValue.Columns.Add("UpwardRunCount");
                dtValue.Columns.Add("DownwardRunCount");
                dtValue.Columns.Add("UpwardTrendCount");
                dtValue.Columns.Add("DownwardTrendCount");
                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["ProductCode"] = strProductCode;
                drValue["StackSeq"] = strStackSeq;
                drValue["Generation"] = strGeneration;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["ProcessCode"] = strProcessCode;
                drValue["StdNumber"] = strStdNumber;
                drValue["StdSeq"] = strStdSeq;
                drValue["VersionNum"] = numVersionNum;
                drValue["UpperSpec"] = numUpperSpec;
                drValue["LowerSpec"] = numLowerSpec;
                drValue["SpecRange"] = strSpecRange;

                drValue["CountDA"] = clsSum.CountDA;
                drValue["SumDA"] = clsSum.SumDA;
                drValue["MeanDA"] = clsSum.MeanDA;
                drValue["MinDA"] = clsSum.MinDA;
                drValue["MaxDA"] = clsSum.MaxDA;
                drValue["RangeDA"] = clsSum.RangeDA;
                drValue["VarianceDA"] = clsSum.VarianceDA;
                drValue["StdDevDA"] = clsSum.StdDevDA;
                drValue["MedianDA"] = clsSum.MedianDA;
                drValue["CpDA"] = clsSum.CpDA;
                drValue["CpkDA"] = clsSum.CpkDA;
                drValue["CplDA"] = clsSum.CplDA;
                drValue["CpuDA"] = clsSum.CpuDA;
                drValue["CountDR"] = clsSum.CountDR;
                drValue["SumDR"] = clsSum.SumDR;
                drValue["MeanDR"] = clsSum.MeanDR;
                drValue["MinDR"] = clsSum.MinDR;
                drValue["MaxDR"] = clsSum.MaxDR;
                drValue["RangeDR"] = clsSum.RangeDR;
                drValue["VarianceDR"] = clsSum.VarianceDR;
                drValue["StdDevDR"] = clsSum.StdDevDR;
                drValue["MedianDR"] = clsSum.MedianDR;
                drValue["CpDR"] = clsSum.CpDR;
                drValue["CpkDR"] = clsSum.CpkDR;
                drValue["CplDR"] = clsSum.CplDR;
                drValue["CpuDR"] = clsSum.CpuDR;
                drValue["USLUpperCount"] = clsSum.USLUpperCount;
                drValue["LSLLowerCount"] = clsSum.LSLLowerCount;
                drValue["UpwardRunCount"] = clsSum.UpwardRunCount;
                drValue["DownwardRunCount"] = clsSum.DownwardRunCount;
                drValue["UpwardTrendCount"] = clsSum.UpwardTrendCount;
                drValue["DownwardTrendCount"] = clsSum.DownwardTrendCount;
                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                return dtValue;
            }
            finally
            {

            }
        }

        private DataTable mfMakeDataTable_ProcessInspectJob_Package(string strPlantCode, string strPackage, string strStackSeq, string strGeneration,
                                   string strInspectItemCode, string strProcessCode, string strStdNumber, string strStdSeq,
                                   int numVersionNum, double numUpperSpec, double numLowerSpec,
                                   string strSpecRange, STASummary clsSum)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("Package");
                dtValue.Columns.Add("StackSeq");
                dtValue.Columns.Add("Generation");
                dtValue.Columns.Add("InspectItemCode");
                dtValue.Columns.Add("ProcessCode");
                dtValue.Columns.Add("StdNumber");
                dtValue.Columns.Add("StdSeq");
                dtValue.Columns.Add("VersionNum");
                dtValue.Columns.Add("UpperSpec");
                dtValue.Columns.Add("LowerSpec");
                dtValue.Columns.Add("SpecRange");

                dtValue.Columns.Add("CountDA");
                dtValue.Columns.Add("SumDA");
                dtValue.Columns.Add("MeanDA");
                dtValue.Columns.Add("MinDA");
                dtValue.Columns.Add("MaxDA");
                dtValue.Columns.Add("RangeDA");
                dtValue.Columns.Add("VarianceDA");
                dtValue.Columns.Add("StdDevDA");
                dtValue.Columns.Add("MedianDA");
                dtValue.Columns.Add("CpDA");
                dtValue.Columns.Add("CpkDA");
                dtValue.Columns.Add("CplDA");
                dtValue.Columns.Add("CpuDA");
                dtValue.Columns.Add("CountDR");
                dtValue.Columns.Add("SumDR");
                dtValue.Columns.Add("MeanDR");
                dtValue.Columns.Add("MinDR");
                dtValue.Columns.Add("MaxDR");
                dtValue.Columns.Add("RangeDR");
                dtValue.Columns.Add("VarianceDR");
                dtValue.Columns.Add("StdDevDR");
                dtValue.Columns.Add("MedianDR");
                dtValue.Columns.Add("CpDR");
                dtValue.Columns.Add("CpkDR");
                dtValue.Columns.Add("CplDR");
                dtValue.Columns.Add("CpuDR");
                dtValue.Columns.Add("USLUpperCount");
                dtValue.Columns.Add("LSLLowerCount");
                dtValue.Columns.Add("UpwardRunCount");
                dtValue.Columns.Add("DownwardRunCount");
                dtValue.Columns.Add("UpwardTrendCount");
                dtValue.Columns.Add("DownwardTrendCount");
                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["Package"] = strPackage;
                drValue["StackSeq"] = strStackSeq;
                drValue["Generation"] = strGeneration;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["ProcessCode"] = strProcessCode;
                drValue["StdNumber"] = strStdNumber;
                drValue["StdSeq"] = strStdSeq;
                drValue["VersionNum"] = numVersionNum;
                drValue["UpperSpec"] = numUpperSpec;
                drValue["LowerSpec"] = numLowerSpec;
                drValue["SpecRange"] = strSpecRange;

                drValue["CountDA"] = clsSum.CountDA;
                drValue["SumDA"] = clsSum.SumDA;
                drValue["MeanDA"] = clsSum.MeanDA;
                drValue["MinDA"] = clsSum.MinDA;
                drValue["MaxDA"] = clsSum.MaxDA;
                drValue["RangeDA"] = clsSum.RangeDA;
                drValue["VarianceDA"] = clsSum.VarianceDA;
                drValue["StdDevDA"] = clsSum.StdDevDA;
                drValue["MedianDA"] = clsSum.MedianDA;
                drValue["CpDA"] = clsSum.CpDA;
                drValue["CpkDA"] = clsSum.CpkDA;
                drValue["CplDA"] = clsSum.CplDA;
                drValue["CpuDA"] = clsSum.CpuDA;
                drValue["CountDR"] = clsSum.CountDR;
                drValue["SumDR"] = clsSum.SumDR;
                drValue["MeanDR"] = clsSum.MeanDR;
                drValue["MinDR"] = clsSum.MinDR;
                drValue["MaxDR"] = clsSum.MaxDR;
                drValue["RangeDR"] = clsSum.RangeDR;
                drValue["VarianceDR"] = clsSum.VarianceDR;
                drValue["StdDevDR"] = clsSum.StdDevDR;
                drValue["MedianDR"] = clsSum.MedianDR;
                drValue["CpDR"] = clsSum.CpDR;
                drValue["CpkDR"] = clsSum.CpkDR;
                drValue["CplDR"] = clsSum.CplDR;
                drValue["CpuDR"] = clsSum.CpuDR;
                drValue["USLUpperCount"] = clsSum.USLUpperCount;
                drValue["LSLLowerCount"] = clsSum.LSLLowerCount;
                drValue["UpwardRunCount"] = clsSum.UpwardRunCount;
                drValue["DownwardRunCount"] = clsSum.DownwardRunCount;
                drValue["UpwardTrendCount"] = clsSum.UpwardTrendCount;
                drValue["DownwardTrendCount"] = clsSum.DownwardTrendCount;
                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                return dtValue;
            }
            finally
            {

            }
        }

        private DataTable mfMakeDataTable_ImportInspetJob_CL(string strPlantCode, string strMaterialCode, string strVendorCode,
                                            string strInspectItemCode, string strAYear, int intAQuarter, STAControlLimit clsLimit)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("MaterialCode");
                dtValue.Columns.Add("VendorCode");
                dtValue.Columns.Add("InspectItemCode");
                dtValue.Columns.Add("AYear");
                dtValue.Columns.Add("AQuarter");

                dtValue.Columns.Add("XLCL");
                dtValue.Columns.Add("XCL");
                dtValue.Columns.Add("XUCL");
                dtValue.Columns.Add("XBRLCL");
                dtValue.Columns.Add("XBCL");
                dtValue.Columns.Add("XBRUCL");
                dtValue.Columns.Add("XBSLCL");
                dtValue.Columns.Add("XBSUCL");

                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["MaterialCode"] = strMaterialCode;
                drValue["VendorCode"] = strVendorCode;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["AYear"] = strAYear;
                drValue["AQuarter"] = intAQuarter.ToString();

                drValue["XLCL"] = clsLimit.XLCL;
                drValue["XCL"] = clsLimit.XCL;
                drValue["XUCL"] = clsLimit.XUCL;
                drValue["XBRLCL"] = clsLimit.XBRLCL;
                drValue["XBCL"] = clsLimit.XBCL;
                drValue["XBRUCL"] = clsLimit.XBRUCL;
                drValue["XBSLCL"] = clsLimit.XBSLCL;
                drValue["XBSUCL"] = clsLimit.XBSUCL;

                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                return dtValue;
            }
            finally
            {

            }
        }

        private DataTable mfMakeDataTable_ProcessInspectJob_CL(string strPlantCode, string strPackage, string strProductCode, string strStackSeq, string strGeneration,
                                                    string strProcessCode, string strInspectItemCode, string strAYear, int intAQuarter, STAControlLimit clsLimit)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("ProductCode");
                dtValue.Columns.Add("Package");
                dtValue.Columns.Add("StackSeq");
                dtValue.Columns.Add("Generation");
                dtValue.Columns.Add("ProcessCode");
                dtValue.Columns.Add("InspectItemCode");

                dtValue.Columns.Add("XLCL");
                dtValue.Columns.Add("XCL");
                dtValue.Columns.Add("XUCL");
                dtValue.Columns.Add("XBRLCL");
                dtValue.Columns.Add("XBCL");
                dtValue.Columns.Add("XBRUCL");
                dtValue.Columns.Add("XBSLCL");
                dtValue.Columns.Add("XBSUCL");

                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["ProductCode"] = strProductCode;
                drValue["Package"] = strPackage;
                drValue["StackSeq"] = strStackSeq;
                drValue["Generation"] = strGeneration;
                drValue["ProcessCode"] = strProcessCode;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["AYear"] = strAYear;
                drValue["AQuarter"] = intAQuarter.ToString();

                drValue["XLCL"] = clsLimit.XLCL;
                drValue["XCL"] = clsLimit.XCL;
                drValue["XUCL"] = clsLimit.XUCL;
                drValue["XBRLCL"] = clsLimit.XBRLCL;
                drValue["XBCL"] = clsLimit.XBCL;
                drValue["XBRUCL"] = clsLimit.XBRUCL;
                drValue["XBSLCL"] = clsLimit.XBSLCL;
                drValue["XBSUCL"] = clsLimit.XBSUCL;

                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                return dtValue;
            }
            finally
            {

            }
        }
        #endregion

        private void m_tt_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            m_tt.Stop();
            try
            {
                if (!CheckQRPServer()) // Sub 서버가 작동하지 않을시
                {
                    mfReadMaster();
                }
                else
                {
                    m_tt.Start();
                }
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }

        private bool CheckQRPServer()
        {
            string RemoteServer = ConfigurationManager.AppSettings["SubRemoteServer"].ToString() + "menu.rem?wsdl"; 

            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(RemoteServer);
            httpReq.AllowAutoRedirect = false;
            try
            {
                HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse();
                //MessageBox.Show(httpRes.StatusCode.ToString());
                if (httpRes.StatusCode == HttpStatusCode.OK)
                {
                    httpRes.Close();
                    return true;
                }
                else
                {
                    httpRes.Close();
                    return false;
                }

            }
            catch
            {
                return false;
            }
            finally
            {

            }
        }

        /// <summary>
        /// 에러 발생시 윈도우 이벤트에 기록하는 Method
        /// 관리도구 > 이벤트 뷰어 > 응용프로그램에서 확인 가능
        /// </summary>
        /// <param name="ex"></param>
        void mfWindowsLogcheck(Exception ex)
        {
            string sSource = "QRP BatchJob.EventLog";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, ex.Message.ToString(), EventLogEntryType.Error, 55);
        }
        void mfWindowsLogcheck(string ex)
        {
            string sSource = "QRP BatchJob.EventLog";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, ex, EventLogEntryType.Error, 55);
        }
    }
}
