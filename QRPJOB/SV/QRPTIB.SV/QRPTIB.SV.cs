﻿/*----------------------------------------------------------------------*/
/* 시스템명     : QRPTIB.SV                                             */
/* 모듈(분류)명 : MES Interface                                         */
/* 프로그램ID   : QRPTIB.SV                                             */
/* 프로그램명   : QRPTIB.SV                                             */
/* 작성자       : 이종민                                                */
/* 작성일자     : 2011-10-14                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */

/* 64bit Server에 Tib 32bit 설치 시 Property-빌드에서 반드시 플랫폼 대상을 x86으로 지정할 것!!!! */
/* 디버깅 방법 
 * OnStart() Method - Thread.Sleep() 다음 줄에 중단점 설정
 * 컴파일 후 QRPTib.SV.exe /i 실행 -> 서비스에 등록 됨
 * 서비스에서 시작을 누른후 메뉴-디버그-프로세스에 연결 선택
 * QRPTib.SV.exe 선택후 연결
 * Thread.Sleep(xx)에 설정된 시간만큼 기다리면 됨
/*----------------------------------------------------------------------*/
using System;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using TIBCO.Rendezvous;
using System.Xml;
using System.Net;
using System.Configuration;

namespace QRPTIB.SV
{
    public partial class QRPTIB : ServiceBase
    {
        #region var - Tibrv 용 변수
        private static Dispatcher dispatcher;
        private static TIBCO.Rendezvous.Queue queue;
        private static Transport transport;
        private static Listener listner;
        #endregion

        #region var - 일반
        private static bool isRun = false;
        private static string m_IP;
        private const string m_xmlString = "<message><header><sourcesubject></sourcesubject><targetsubject></targetsubject><messagename></messagename><transactionid></transactionid><locale></locale></header><body></body><return><returncode></returncode><returnmessage></returnmessage></return></message>";
        public string Server { get; set; }
        private System.Timers.Timer m_tt;

        #endregion

        public QRPTIB()
        {
            InitializeComponent();

            //서버 설정 읽어오기
            //현재는 하드코딩 -> app.config 에서 읽어오는 형태로 변경
            //mfReadTIBMaster();
            Server = string.Empty;
            //IPHostEntry ip = Dns.GetHostEntry(Dns.GetHostName());
            //m_IP = ip.AddressList[0]..AddressFamily.ToString();

            // IP주소 IPV4 방식으로 받기
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            string ipAddr = string.Empty;
            for (int i = 0; i < host.AddressList.Length; i++)
            {
                if (host.AddressList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    ipAddr = host.AddressList[i].ToString();
                }
            }

            m_IP = ipAddr;

        }

        /// <summary>
        /// QRP로 전달되는 메세지를 수신하기 위한 Listner 초기화
        /// </summary>
        public void Initialize()
        {
            try
            {
                if (isRun)
                {
                    Terminate();
                }

                TIBCO.Rendezvous.Environment.Open();
                transport = new NetTransport(Service, Network, Daemon);

                queue = new TIBCO.Rendezvous.Queue();
                dispatcher = new Dispatcher(queue);

                if ((!SourceSubject.Contains("*")) && (!string.IsNullOrEmpty(SourceSubject)))
                {
                    // Listner 생성 및 메세지를 받았을때 발생할 이벤트 설정
                    listner = new Listener(queue, transport, SourceSubject, null);
                    listner.MessageReceived += new MessageReceivedEventHandler(OnMessageReceived);
                }

                isRun = true;
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }

        private void Terminate()
        {
            if (!isRun) return;

            try
            {
                dispatcher.Destroy();
                queue.Destroy();
                transport.Destroy();

                TIBCO.Rendezvous.Environment.Close();
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
            finally
            {
                isRun = false;
            }
        }

        #region Static Var
        private static string strService;
        private static string Service
        {
            get { return strService; }
            set { strService = value; }
        }

        private static string strNetwork;
        private static string Network
        {
            get { return strNetwork; }
            set { strNetwork = value; }
        }

        private static string strDaemon;
        private static string Daemon
        {
            get { return strDaemon; }
            set { strDaemon = value; }
        }

        private static string strSourceSubject;
        private static string SourceSubject
        {
            get { return strSourceSubject; }
            set { strSourceSubject = value; }
        }

        private static string strTargetSubject;
        private static string TargetSubject
        {
            get { return strTargetSubject; }
            set { strTargetSubject = value; }
        }
        #endregion

        /// <summary>
        /// Message Service 설정 읽어오기
        /// </summary>
        private void mfReadTIBMaster()
        {
            try
            {
                // 메인서버의 경우 항상 수신 대기
                if (Server.Equals("Main"))
                {
                    XmlDocument tibrv = new XmlDocument();
                    tibrv.Load(System.Windows.Forms.Application.StartupPath + @"\qrptib.xml");

                    Service = tibrv.SelectSingleNode("tibrv/Service").InnerText;
                    Network = tibrv.SelectSingleNode("tibrv/Network").InnerText;
                    Daemon = tibrv.SelectSingleNode("tibrv/Daemon").InnerText;
                    SourceSubject = tibrv.SelectSingleNode("tibrv/SourceSubject").InnerText;
                    TargetSubject = tibrv.SelectSingleNode("tibrv/TargetSubject").InnerText;

                    Initialize();
                }
                // 부-서버의 경우 메인서버가 연결되지 않는 경우 수신대기
                else
                {
                    m_tt = new System.Timers.Timer();
                    m_tt.Elapsed += new System.Timers.ElapsedEventHandler(m_tt_Tick);
                    m_tt.AutoReset = false;
                    m_tt.Interval = 600000;

                    m_tt.Start();
                }
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }
        
        private void mfReadTIBMaster(object obj)
        {
            try
            {
                // qrptib.xml파일에서 서버 설정 읽어오기
                XmlDocument tibrv = new XmlDocument();
                tibrv.Load(System.Windows.Forms.Application.StartupPath + @"\qrptib.xml");

                Service = tibrv.SelectSingleNode("tibrv/Service").InnerText;
                Network = tibrv.SelectSingleNode("tibrv/Network").InnerText;
                Daemon = tibrv.SelectSingleNode("tibrv/Daemon").InnerText;
                SourceSubject = tibrv.SelectSingleNode("tibrv/SourceSubject").InnerText;
                TargetSubject = tibrv.SelectSingleNode("tibrv/TargetSubject").InnerText;

                Initialize();
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }

        /// <summary>
        /// MES에서 보낸 메세지를 받았을 때 발생되는 이벤트
        /// </summary>
        /// <param name="listener"></param>
        /// <param name="e"></param>
        private void OnMessageReceived(object listener, MessageReceivedEventArgs e)
        {
            try
            {
                // 받은 Message
                TIBCO.Rendezvous.Message _get = e.Message;
                XmlDocument Getxml = new XmlDocument();
                Getxml.LoadXml(_get.GetField("xmlData").Value.ToString());

                //실행중인지 확인 // 재연결
                if (!isRun)
                {
                    Initialize();
                }

                string strMessageName = Getxml.SelectSingleNode("message/header/messagename").InnerText;

                #region IF Method Call strMessageName에 따라 Method Call
                TibAction action = new TibAction();
                action.TargetSubject = TargetSubject;
                switch (strMessageName)
                {
                    case "LOW_YIELD_ISSUE":
                        action.Low_Yield_Issue(e.Message, Getxml, m_IP, transport);
                        break;
                    case "BIN_LOW_YIELD_ISSUE":
                        action.Bin_Low_Yield_Issue(e.Message, Getxml, m_IP, transport);
                        break;
                    case "CCS_REQ":
                        action.CCS_Req(e.Message, Getxml, m_IP, transport);
                        break;
                    case "OCAP_REQ":
                        action.Ocap_Req(e.Message, Getxml, m_IP, transport);
                        break;
                    case "EQP_CHANGESTATE":
                        action.Eqp_ChangeState(e.Message, Getxml, m_IP, transport);
                        break;
                    case "PM_LIST_REQ":
                        action.PM_LIST_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "PM_RESULT_REQ":
                        action.PM_RESULT_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "TOOL_INFO_REQ":
                        action.TOOL_INFO_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "CCS_CANCEL_REQ":
                        action.CCS_CANCEL_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "EQP_CHANGESTATE_RPT":
                        action.EQP_CHANGESTATE_RPT(e.Message, Getxml, m_IP, transport);
                        break;
                    case "MOLD_LIFETYPE_REQ":
                        action.MOLD_LIFETYPE_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "LOT_TKINCHK_REQ":
                        action.LOT_TKINCHK_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "LOT_SPECVIEW_REQ":
                        action.LOT_SPECVIEW_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "LOT_RECIPEVIEW_REQ":
                        action.LOT_RECIPEVIEW_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "CONSUMABLE_JIG_REQ":
                        action.CONSUMABLE_JIG_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                    case "LOT_SOCKET_REQ":

                        System.IO.StreamWriter log = new System.IO.StreamWriter("I:/" + "LOT_SOCKET_REQ.txt", true);

                        log.WriteLine("case LOT_SOCKET_REQ");

                        action.LOT_SOCKET_REQ(e.Message, Getxml, m_IP, transport);
                        break;
                }
                #endregion

            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }

        /// <summary>
        /// 받은 메세지 XML을 보낼 메세지 XML 형태로 변경
        /// </summary>
        /// <param name="_SendXmlHeader"></param>
        /// <param name="_GetXmlHeader"></param>
        private static void mfSetHeader(ref XmlNodeList _SendXmlHeader, XmlNodeList _GetXmlHeader)
        {
            string source, target, message, tran, locale;
            source = ""; target = ""; message = ""; tran = ""; locale = "";
            foreach (XmlElement _node in _GetXmlHeader)
            {
                switch (_node.Name)
                {
                    case "sourcesubject":
                        target = _node.InnerText;
                        break;
                    case "targetsubject":
                        source = _node.InnerText;
                        break;
                    case "messagename":
                        message = _node.InnerText;
                        break;
                    case "transactionid":
                        tran = _node.InnerText;
                        break;
                    case "locale":
                        locale = _node.InnerText;
                        break;
                }
            }
            foreach (XmlElement _node in _SendXmlHeader)
            {
                switch (_node.Name)
                {
                    case "sourcesubject":
                        _node.InnerText = source;
                        break;
                    case "targetsubject":
                        _node.InnerText = target;
                        break;
                    case "messagename":
                        _node.InnerText = message;
                        break;
                    case "transactionid":
                        _node.InnerText = tran;
                        break;
                    case "locale":
                        _node.InnerText = locale;
                        break;
                }
            }
        }

        /// <summary>
        /// 윈도우즈 서비스가 시작될때 발생
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            // 디버깅 시 주석 해제후 바로 아래 if 절에 중단점 설정
            //System.Threading.Thread.Sleep(30000);

            // 서비스 시작 시 argument에 /main 또는 /sub 입력 -> 아무 입력 없을 경우 /main으로 간주
            if (args.Length > 0)
            {
                if (args[0].ToLower().Contains("/main") || args[0].ToLower().Contains("/sub") || args[0].ToLower().Contains("/주") || args[0].ToLower().Contains("/부"))
                {
                    if (args[0].ToLower().Contains("/main") || args[0].ToLower().Contains("/주"))
                        Server = "Main";
                    else if (args[0].ToLower().Contains("/sub") || args[0].ToLower().Contains("/부"))
                        Server = "Sub";
                    else
                        Server = "Main";
                }
            }
            else
            {
                Server = "Main";
            }
            mfReadTIBMaster();

            //else
            //{
            //    System.Windows.Forms.MessageBox.Show("QRPTib 서비스 시작시 매개변수 /Main (/주) 또는 /Sub (/부) 를 지정 하셔야합니다.", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly);
            //    this.Stop();
            //    //OnStop();
            //    //mfWindowsLogcheck("QRPTib 서비스 시작시 매개변수 /Main (/주) 또는 /Sub (/부) 를 지정 하셔야합니다.");
            //}
        }

        /// <summary>
        /// 윈도우즈 서비스가 중지될때 발생
        /// </summary>
        protected override void OnStop()
        {
            if (m_tt != null)
                m_tt.Stop();

            Terminate();
        }

        /// <summary>
        /// 에러 발생시 윈도우 이벤트에 기록하는 Method
        /// 관리도구 > 이벤트 뷰어 > 응용프로그램에서 확인 가능
        /// </summary>
        /// <param name="ex"></param>
        void mfWindowsLogcheck(Exception ex)
        {
            string sSource = "QRPTIB.SV.EventLog";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, ex.Message.ToString(), EventLogEntryType.Error, 55);
        }
        void mfWindowsLogcheck(string ex)
        {
            string sSource = "QRPTIB.SV.EventLog";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, ex, EventLogEntryType.Error, 55);
        }

        private void m_tt_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            m_tt.Stop();
            try
            {
                if (!CheckQRPServer()) // Main 서버가 작동하지 않을시
                {
                    mfReadTIBMaster("1");
                }
                else
                {
                    m_tt.Start();
                }
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }

        private bool CheckQRPServer()
        {
            //string RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString() + "menu.rem?wsdl";
            XmlDocument tibrv = new XmlDocument();
            tibrv.Load(System.Windows.Forms.Application.StartupPath + @"\qrptib.xml");
            string RemoteServer = tibrv.SelectSingleNode("tibrv/IIS/Main").InnerText;

            RemoteServer += "menu.rem?wsdl";

            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(RemoteServer);
            httpReq.AllowAutoRedirect = false;
            try
            {
                HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse();
                //MessageBox.Show(httpRes.StatusCode.ToString());
                if (httpRes.StatusCode == HttpStatusCode.OK)
                {
                    httpRes.Close();
                    return true;
                }
                else
                {
                    httpRes.Close();
                    return false;
                }

            }
            catch
            {
                return false;
            }
            finally
            {

            }
        }
    }

    /// <summary>
    /// MES에서 받은 메세지에 따라서 실행할 각 Method 집합 클래스
    /// </summary>
    public class TibAction
    {
        #region Var
        private string strTargetSubject = "";
        public string TargetSubject
        {
            get { return strTargetSubject; }
            set { strTargetSubject = value; }
        }
        #endregion

        #region SetReturnMessage - MES 서버에서 Send로 보내므로 SendReply가 아닌 Send로 리턴
        /// <summary>
        /// 받은 메세지 처리 후 다리 MES로 전송
        /// </summary>
        /// <param name="SendXml">보낼 메세지 XML</param>
        /// <param name="ErrRtn">QRP 처리 후 생성된 Return Message</param>
        /// <param name="msg">Tibrv 메세지</param>
        /// <param name="trans">Tibrv NetTransport</param>
        void SetReturnMessage(XmlDocument SendXml, QRPCOM.QRPGLO.TransErrRtn ErrRtn, TIBCO.Rendezvous.Message msg, Transport trans)
        {
            try
            {
                // 받은 Message
                TIBCO.Rendezvous.Message _get = msg;
                XmlDocument Getxml = new XmlDocument();
                Getxml.LoadXml(_get.GetField("xmlData").Value.ToString());

                string strGetMessage = Getxml.OuterXml.ToString();

                XmlNode _RtnNode = SendXml.SelectSingleNode("message/return/returncode");
                _RtnNode.InnerText = ErrRtn.ErrNum.ToString();
                _RtnNode = SendXml.SelectSingleNode("message/return/returnmessage");
                _RtnNode.InnerText = ErrRtn.ErrMessage;

                Message _reply = new Message();
                _reply.AddField("xmlData", SendXml.OuterXml.ToString());
                _reply.SendSubject = TargetSubject;
                trans.Send(_reply);

                mfSaveMEFIFLog("QRPServer", SendXml.OuterXml.ToString(), strGetMessage, "QRPServer", "QRPServer");
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }
        /// <summary>
        /// 받은 메세지 처리 후 다시 MES로 전송
        /// </summary>
        /// <param name="SendXml">보낼 메세지 XML</param>
        /// <param name="msg">Tibrv 메세지</param>
        /// <param name="trans">Tibrv NetTransport</param>
        void SetReturnMessage(XmlDocument SendXml, TIBCO.Rendezvous.Message msg, Transport trans)
        {
            try
            {
                // 받은 Message
                TIBCO.Rendezvous.Message _get = msg;
                XmlDocument Getxml = new XmlDocument();
                Getxml.LoadXml(_get.GetField("xmlData").Value.ToString());

                string strGetMessage = Getxml.OuterXml.ToString();

                XmlNode _RtnNode = SendXml.SelectSingleNode("message/return/returncode");
                _RtnNode.InnerText = "0";
                _RtnNode = SendXml.SelectSingleNode("message/return/returnmessage");
                _RtnNode.InnerText = "";

                Message _reply = new Message();
                _reply.AddField("xmlData", SendXml.OuterXml.ToString());
                _reply.SendSubject = TargetSubject;
                trans.Send(_reply);

                mfSaveMEFIFLog("QRPServer", SendXml.OuterXml.ToString(), strGetMessage, "QRPServer", "QRPServer");
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }
        /// <summary>
        /// 받은 메세지 처리 후 다리 MES로 전송
        /// </summary>
        /// <param name="SendXml">보낼 메세지 XML</param>
        /// <param name="msg">Tibrv 메세지</param>
        /// <param name="trans">Tibrv NetTransport</param>
        /// <param name="strReturnCode">MES로 전송할 ReturnCode typeof(string)</param>
        /// <param name="strReturnMessage">MES로 전송할 ReturnMessage typeof(string)</param>
        void SetReturnMessage(XmlDocument SendXml, TIBCO.Rendezvous.Message msg, Transport trans, string strReturnCode, string strReturnMessage)
        {
            try
            {
                // 받은 Message
                TIBCO.Rendezvous.Message _get = msg;
                XmlDocument Getxml = new XmlDocument();
                Getxml.LoadXml(_get.GetField("xmlData").Value.ToString());

                string strGetMessage = Getxml.OuterXml.ToString();

                XmlNode _RtnNode = SendXml.SelectSingleNode("message/return/returncode");
                _RtnNode.InnerText = strReturnCode;
                _RtnNode = SendXml.SelectSingleNode("message/return/returnmessage");
                _RtnNode.InnerText = strReturnMessage;

                Message _reply = new Message();
                _reply.AddField("xmlData", SendXml.OuterXml.ToString());
                _reply.SendSubject = TargetSubject;
                trans.Send(_reply);
                mfSaveMEFIFLog("QRPServer", SendXml.OuterXml.ToString(), strGetMessage, "QRPServer", "QRPServer");
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }
        /// <summary>
        /// 받은 메세지 처리 후 다리 MES로 전송
        /// </summary>
        /// <param name="SendXml">보낼 메세지 XML</param>
        /// <param name="msg">Tibrv 메세지</param>
        /// <param name="trans">Tibrv NetTranspor</param>
        /// <param name="ex">Exception 발생시</param>
        void SetReturnMessage(XmlDocument SendXml, TIBCO.Rendezvous.Message msg, Transport trans, Exception ex)
        {
            try
            {
                // 받은 Message
                TIBCO.Rendezvous.Message _get = msg;
                XmlDocument Getxml = new XmlDocument();
                Getxml.LoadXml(_get.GetField("xmlData").Value.ToString());

                string strGetMessage = Getxml.OuterXml.ToString();

                XmlNode _RtnNode = SendXml.SelectSingleNode("message/return/returncode");
                _RtnNode.InnerText = ex.Message.ToString();
                _RtnNode = SendXml.SelectSingleNode("message/return/returnmessage");
                _RtnNode.InnerText = ex.StackTrace.ToString();

                Message _reply = new Message();
                _reply.AddField("xmlData", SendXml.OuterXml.ToString());
                _reply.SendSubject = TargetSubject;
                trans.Send(_reply);

                mfSaveMEFIFLog("QRPServer", SendXml.OuterXml.ToString(), strGetMessage, "QRPServer", "QRPServer");
            }
            catch (Exception exx)
            {
                mfWindowsLogcheck(exx);
            }
        }

        private void mfSaveMEFIFLog(string strFormName, string strSendMessage, string strReplyMessage, string strUserIP, string strUserID)
        {
            try
            {
                QRPJOB.BL.BATJOB.MESIF clsMESID;
                //if (m_Debug.Equals("F"))
                //{
                //    mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.MESIF), "MESIF");
                //    clsMESID = new QRPJOB.BL.BATJOB.MESIF();
                //    mfCredentials(clsMESID);
                //}
                //else
                //{
                clsMESID = new QRPJOB.BL.BATJOB.MESIF();
                //}

                XmlDocument _Send = new XmlDocument();
                _Send.LoadXml(strSendMessage);
                XmlDocument _Reply = new XmlDocument();
                _Reply.LoadXml(strReplyMessage);

                string strReturnCode = _Send.SelectSingleNode("message/return/returncode").InnerText;
                string strReturnMessage = _Send.SelectSingleNode("message/return/returnmessage").InnerText;
                string strsend = _Send.SelectSingleNode("message").OuterXml.ToString();
                string strreply = _Reply.SelectSingleNode("message").OuterXml.ToString();
                clsMESID.mfSaveMESIFErrorLog(strFormName, strsend, strreply, strReturnCode, strReturnMessage, strUserIP, strUserID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        XmlDocument ChangeSubjectName(XmlDocument GetXml, string strSubjectName)
        {
            GetXml.SelectSingleNode("message/header/messagename").InnerText = strSubjectName;
            return GetXml;
        }
        #endregion

        #region Action - 각 항목별 Method
        /// <summary>
        /// 저수율 발생정보
        /// </summary>
        public void Low_Yield_Issue(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var 받은메세지로 입력해야할 변수 생성
                string strDate = GetXml.SelectSingleNode("message/body/ISSUEDATE").InnerText;
                string strPLANTNAME = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strISSUEDATE = strDate.Substring(0, 4) + "-" + strDate.Substring(4, 2) + "-" + strDate.Substring(6, 2) + " " + strDate.Substring(8, 2) + ":" + strDate.Substring(10, 2) + ":" + strDate.Substring(12, 2);
                string strUSERID = GetXml.SelectSingleNode("message/body/USERID").InnerText;
                string strLOTID = GetXml.SelectSingleNode("message/body/LOTID").InnerText;
                string strOPERID = GetXml.SelectSingleNode("message/body/OPERID").InnerText;
                string strEQPID = GetXml.SelectSingleNode("message/body/EQPID").InnerText;
                string strPRODUCTSPECNAME = GetXml.SelectSingleNode("message/body/PRODUCTSPECNAME").InnerText;
                //string strCUSTOMERPRODUCTSPECNAME = GetXml.SelectSingleNode("message/body/CUSTOMERPRODUCTSPECNAME").InnerText;
                //string strCUSTOMER = GetXml.SelectSingleNode("message/body/CUSTOMER").InnerText;
                string strPRODUCTQUANTITY = GetXml.SelectSingleNode("message/body/PRODUCTQUANTITY").InnerText;
                string strYIELD = GetXml.SelectSingleNode("message/body/YIELD").InnerText;
                DataTable dtScrap = new DataTable();
                dtScrap.Columns.Add("SCRAPCODE", typeof(string));
                dtScrap.Columns.Add("SCRAPQTY", typeof(string));

                XmlNodeList xmlNode = GetXml.SelectNodes("message/body/SCRAPLIST/*");

                for (int i = 0; i < xmlNode.Count; i++)
                {
                    DataRow drScrap = dtScrap.NewRow();
                    for (int j = 0; j < xmlNode.Item(i).ChildNodes.Count; j++)
                    {
                        if (drScrap.Table.Columns.Contains(xmlNode.Item(i).ChildNodes[j].Name))
                        {
                            drScrap[xmlNode.Item(i).ChildNodes[j].Name] = xmlNode.Item(i).ChildNodes[j].InnerText;
                        }
                    }
                    dtScrap.Rows.Add(drScrap);
                }
                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action 변수로 처리
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                QRPINS.BL.INSSTS.INSProcLowYield clsYield = new QRPINS.BL.INSSTS.INSProcLowYield();
                //brwChannel.mfCredentials(clsYield);

                string strRtn = clsYield.mfSaveINSProcLowYield_IF(strPLANTNAME, strISSUEDATE, strLOTID, strOPERID, strEQPID, strPRODUCTSPECNAME,
                                                                  strPRODUCTQUANTITY, "", strYIELD, dtScrap, m_IP, strUSERID);
                #endregion

                #region Final
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                string strReturnCode = string.Empty;
                string strReturMessage = string.Empty;

                //SetReturnMessage(GetXml, ErrRtn, msg, trans);
                if (ErrRtn.ErrMessage.Equals(string.Empty))
                {
                    strReturnCode = "Y";
                    strReturMessage = "Low_Yield_Issue Success";
                    SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);
                    mfWindowsLogcheck("Low_Yield_Issue Success");
                }
                else
                {
                    strReturnCode = "N";
                    strReturMessage = "Low_Yield_Issue Failed";
                    SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);
                    mfWindowsLogcheck("Low_Yield_Issue Failed");
                }
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// BIN 저수율 발생정보
        /// </summary>
        public void Bin_Low_Yield_Issue(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var 받은메세지로 입력해야할 변수 생성
                string strUSERID = GetXml.SelectSingleNode("message/body/USERID").InnerText;
                string strPLANTNAME = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strISSUEDATE = DateTime.Parse(GetXml.SelectSingleNode("message/body/ISSUEDATE").InnerText).ToString("yyyy-MM-dd hh:mm:ss");
                string strLOTID = GetXml.SelectSingleNode("message/body/LOTID").InnerText;
                string strOPERID = GetXml.SelectSingleNode("message/body/OPERID").InnerText;
                string strEQPID = GetXml.SelectSingleNode("message/body/EQPID").InnerText;
                string strPRODUCTSPECNAME = GetXml.SelectSingleNode("message/body/PRODUCTSPECNAME").InnerText;
                string strCUSTOMERPRODUCTSPECNAME = GetXml.SelectSingleNode("message/body/CUSTOMERPRODUCTSPECNAME").InnerText;
                string strCUSTOMER = GetXml.SelectSingleNode("message/body/CUSTOMER").InnerText;
                string strPRODUCTQUANTITY = GetXml.SelectSingleNode("message/body/PRODUCTQUANTITY").InnerText;
                string strBINNAME = GetXml.SelectSingleNode("message/body/BINNAME").InnerText;
                string strYIELD = GetXml.SelectSingleNode("message/body/YIELD").InnerText;
                DataTable dtScrap = new DataTable();
                dtScrap.Columns.Add("SCRAPCODE", typeof(string));
                dtScrap.Columns.Add("SCRAPQTY", typeof(string));

                XmlNodeList xmlNode = GetXml.SelectNodes("message/body/SCRAPLIST/*");

                for (int i = 0; i < xmlNode.Count; i++)
                {
                    DataRow drScrap = dtScrap.NewRow();
                    for (int j = 0; j < xmlNode.Item(i).ChildNodes.Count; j++)
                    {
                        if (drScrap.Table.Columns.Contains(xmlNode.Item(i).ChildNodes[j].Name))
                        {
                            drScrap[xmlNode.Item(i).ChildNodes[j].Name] = xmlNode.Item(i).ChildNodes[j].InnerText;
                        }
                    }
                    dtScrap.Rows.Add(drScrap);
                }
                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action 변수로 처리
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.INSProcLowYield), "INSProcLowYield");
                QRPINS.BL.INSSTS.INSProcLowYield clsYield = new QRPINS.BL.INSSTS.INSProcLowYield();
                //brwChannel.mfCredentials(clsYield);

                string strRtn = clsYield.mfSaveINSProcLowYield_IF(strPLANTNAME, strISSUEDATE, strLOTID, strOPERID, strEQPID, strPRODUCTSPECNAME,
                                                                  strPRODUCTQUANTITY, strBINNAME, strYIELD, dtScrap, m_IP, strUSERID);
                #endregion

                #region Final
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                //SetReturnMessage(GetXml, ErrRtn, msg, trans);
                if(ErrRtn.ErrMessage.Equals(string.Empty))
                    mfWindowsLogcheck("Bin_Low_Yield_Issue Success");
                else
                    mfWindowsLogcheck("Bin_Low_Yield_Issue Failed");
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// CCS 의뢰 요청
        /// </summary>
        public void CCS_Req(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var
                string strPLANTCODE = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strUSERID = GetXml.SelectSingleNode("message/body/USERID").InnerText;
                string strEQPID = GetXml.SelectSingleNode("message/body/EQPID").InnerText;
                string strLOTID = GetXml.SelectSingleNode("message/body/LOTID").InnerText;
                string strPRODUCTQUANTITY = GetXml.SelectSingleNode("message/body/PRODUCTQUANTITY").InnerText;
                string strPRODUCTSPECNAME = GetXml.SelectSingleNode("message/body/PRODUCTSPECNAME").InnerText;
                string strCUSTOMERPRODUCTSPECNAME = GetXml.SelectSingleNode("message/body/CUSTOMERPRODUCTSPECNAME").InnerText;
                string strCUSTOMER = GetXml.SelectSingleNode("message/body/CUSTOMER").InnerText;
                string strOPERID = GetXml.SelectSingleNode("message/body/OPERID").InnerText;
                string strCCSCode = GetXml.SelectSingleNode("message/body/CCSCODE").InnerText;
                //string strWorkProcessCode = GetXml.SelectSingleNode("message/body/WORKOPERID").InnerText;

                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action
                string strRtn = mfSaveAuto(strPLANTCODE, strUSERID, strPRODUCTSPECNAME, strOPERID,
                    strEQPID, strLOTID, strCCSCode, strUSERID, m_IP);
                #endregion

                #region Final
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                GetXml = ChangeSubjectName(GetXml, "CCS_REP");
                SetReturnMessage(GetXml, ErrRtn, msg, trans);
                if(ErrRtn.ErrMessage.Equals(string.Empty))
                    mfWindowsLogcheck("CCS_Req Success");
                else
                    mfWindowsLogcheck("CCS_Req Failed");
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }
        private string mfSaveAuto(string strPlantCode, string strUserID, string strProductCode, string strProcessCode, string strEquipCode, string strLotNo, string strCCSCode, string strWorkUserID, string strUserIP)
        {
            DataTable dtSaveHeader = new DataTable();
            DataTable dtSaveLot = new DataTable();
            DataTable dtSaveItem = new DataTable();
            DataTable dtSaveMeasure = new DataTable();
            DataTable dtSaveCount = new DataTable();
            DataTable dtSaveOkNo = new DataTable();
            DataTable dtSaveDesc = new DataTable();
            DataTable dtSaveSelect = new DataTable();

            try
            {
                //헤더 정보 BL연결
                QRPCCS.BL.INSCCS.CCSInspectReqH clsHeader;

                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqH), "CCSInspectReqH");
                clsHeader = new QRPCCS.BL.INSCCS.CCSInspectReqH();
                //brwChannel.mfCredentials(clsHeader);


                // Header 정보 Column 설정
                dtSaveHeader = clsHeader.mfSetDataInfo();
                DataRow drRow = dtSaveHeader.NewRow();
                drRow["PlantCode"] = strPlantCode;
                drRow["ReqNo"] = "";
                drRow["ReqSeq"] = "";
                drRow["ProductCode"] = strProductCode;
                //drRow["ProcessCode"] = strProcessCode;
                drRow["EquipCode"] = strEquipCode;
                drRow["EtcDesc"] = "";
                drRow["WorkProcessCode"] = strProcessCode;
                drRow["NowProcessCode"] = strProcessCode;
                drRow["InspectResultFlag"] = "";
                dtSaveHeader.Rows.Add(drRow);

                //Lot 정보 BL연결
                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot;
                //brwChannel.mfRegisterChannel(typeof(QRPCCS.BL.INSCCS.CCSInspectReqLot), "CCSInspectReqLot");
                clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                //brwChannel.mfCredentials(clsLot);


                // Lot 정보 Column 설정
                dtSaveLot = clsLot.mfSetDataInfo();
                DataRow drRowLot;
                // 데이터 저장
                drRowLot = dtSaveLot.NewRow();
                drRowLot["PlantCode"] = strPlantCode;
                drRowLot["ReqNo"] = "";
                drRowLot["ReqSeq"] = "";
                drRowLot["ReqLotSeq"] = 1;  // 자동의뢰되는 경우 이므로 무조건 1        
                drRowLot["LotNo"] = strLotNo;
                drRowLot["CCSReqTypeCode"] = strCCSCode;
                drRowLot["ReqUserId"] = strWorkUserID;
                drRowLot["ReqDate"] = DateTime.Today;
                drRowLot["ReqTime"] = "";
                drRowLot["CauseReason"] = "";
                drRowLot["CorrectAction"] = "";
                drRowLot["LotMonthCount"] = 0;
                drRowLot["LotYearCount"] = 0;
                drRowLot["CCSCreateType"] = "M";
                dtSaveLot.Rows.Add(drRowLot);
                DataTable dtPara = new DataTable();

                // 헤더, Lot 정보만 저장 한다.                
                string strErrRtn = "";

                // Method 호출
                strErrRtn = clsHeader.mfSaveCCSInsepctReqH(dtSaveHeader, dtSaveLot, dtSaveItem, dtSaveMeasure, dtSaveCount, dtSaveOkNo, dtSaveDesc, dtSaveSelect, dtPara
                                    , strWorkUserID, strUserIP, "KOR", "QRPServer");

                return strErrRtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        /// <summary>
        /// CCS 취소 요청
        /// </summary>
        public void CCS_CANCEL_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var
                string strPLANTCODE = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strUSERID = GetXml.SelectSingleNode("message/body/USERID").InnerText;
                string strEQPID = GetXml.SelectSingleNode("message/body/EQPID").InnerText;
                string strLOTID = GetXml.SelectSingleNode("message/body/LOTID").InnerText;
                string strPRODUCTQUANTITY = GetXml.SelectSingleNode("message/body/PRODUCTQUANTITY").InnerText;
                string strPRODUCTSPECNAME = GetXml.SelectSingleNode("message/body/PRODUCTSPECNAME").InnerText;
                string strCUSTOMERPRODUCTSPECNAME = GetXml.SelectSingleNode("message/body/CUSTOMERPRODUCTSPECNAME").InnerText;
                //string strCUSTOMER = GetXml.SelectSingleNode("message/body/CUSTOMER").InnerText;
                string strOPERID = GetXml.SelectSingleNode("message/body/OPERID").InnerText;
                //string strCCSCode = GetXml.SelectSingleNode("message/body/CCSCODE").InnerText;

                #endregion

                #region Action
                //string strRtn = mfSaveAuto(strPLANTCODE, strUSERID, strPRODUCTSPECNAME, strOPERID,
                //    strEQPID, strLOTID, strCCSCode, strUSERID, m_IP);
                QRPCCS.BL.INSCCS.CCSInspectReqLot clsLot = new QRPCCS.BL.INSCCS.CCSInspectReqLot();
                string strRtn = clsLot.mfCancelCCSInspectReqLot(strPLANTCODE, strUSERID, strEQPID, strLOTID,
                    strPRODUCTQUANTITY, strPRODUCTSPECNAME, strCUSTOMERPRODUCTSPECNAME, strOPERID, m_IP);
                #endregion

                #region Final
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                GetXml = ChangeSubjectName(GetXml, "CCS_CANCEL_REP");
                SetReturnMessage(GetXml, ErrRtn, msg, trans);
                if(ErrRtn.ErrMessage.Equals(string.Empty))
                    mfWindowsLogcheck("CCS_Cancel_Req Success");
                else
                    mfWindowsLogcheck("CCS_Cancel_Req Failed");
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// OCAP에서 발생한 Inform 전달
        /// </summary>
        public void Ocap_Req(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var

                DataTable dtVar = new DataTable();
                dtVar.Columns.Add("USERID", typeof(string));
                dtVar.Columns.Add("FACTORYID", typeof(string));
                dtVar.Columns.Add("CUSTOMERID", typeof(string));
                dtVar.Columns.Add("PACKAGE", typeof(string));
                dtVar.Columns.Add("EQPID", typeof(string));
                dtVar.Columns.Add("LOTID", typeof(string));
                dtVar.Columns.Add("OCAPTYPE", typeof(string));
                dtVar.Columns.Add("REASONOPERID", typeof(string));
                dtVar.Columns.Add("ACTIONOPERID", typeof(string));
                dtVar.Columns.Add("ACTIONITEM", typeof(string));

                DataRow drVar = dtVar.NewRow();

                drVar["USERID"] = GetXml.SelectSingleNode("message/body/USERID").InnerText;//작업자ID
                drVar["FACTORYID"] = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;//공장코드
                drVar["CUSTOMERID"] = GetXml.SelectSingleNode("message/body/CUSTOMERID").InnerText;//고객사ID
                drVar["PACKAGE"] = GetXml.SelectSingleNode("message/body/PACKAGE").InnerText;//Package
                drVar["EQPID"] = GetXml.SelectSingleNode("message/body/EQPID").InnerText;//설비 ID
                drVar["LOTID"] = GetXml.SelectSingleNode("message/body/LOTID").InnerText;//Lot ID
                drVar["OCAPTYPE"] = GetXml.SelectSingleNode("message/body/OCAPTYPE").InnerText;//CCS, 불량, 정비 등의 분류 구분
                drVar["REASONOPERID"] = GetXml.SelectSingleNode("message/body/REASONOPERID").InnerText;//발생 공정 ID
                drVar["ACTIONOPERID"] = GetXml.SelectSingleNode("message/body/ACTIONOPERID").InnerText;//조치 공정 ID
                drVar["ACTIONITEM"] = GetXml.SelectSingleNode("message/body/ACTIONITEM").InnerText;//조치 내용

                dtVar.Rows.Add(drVar);
                #endregion

                #region Action
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPINS.BL.INSSTS.OCAP), "OCAP");
                QRPINS.BL.INSSTS.OCAP clsOCAP = new QRPINS.BL.INSSTS.OCAP();
                //brwChannel.mfCredentials(clsOCAP);

                string strRtn = clsOCAP.mfSaveOCAP_MESIF(dtVar, m_IP);

                #endregion

                #region Final
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                GetXml = ChangeSubjectName(GetXml, "OCAP_REP");
                SetReturnMessage(GetXml, ErrRtn, msg, trans);
                if(ErrRtn.ErrMessage.Equals(string.Empty))
                    mfWindowsLogcheck("Ocap_Req Success");
                else
                    mfWindowsLogcheck("Ocap_Req Failed");
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// 설비 상태(PM, Idle) 변경 보고
        /// </summary>
        public void Eqp_ChangeState(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var
                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action
                #endregion

                #region Final
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// 설비 및 점검일에 대한 설비PM정보 및 점검결과 기준정보 전송
        /// </summary>
        public void PM_LIST_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var
                string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strEquipCode = GetXml.SelectSingleNode("message/body/EQPID").InnerText;
                string strPMPlanDate = GetXml.SelectSingleNode("message/body/CHECKDATE").InnerText;
                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                //brwChannel.mfCredentials(clsPMPlanD);

                DataTable dtPlanD = clsPMPlanD.mfReadPMPlanD_MESIF(strPlantCode, strEquipCode, strPMPlanDate);

                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsResultType = new QRPMAS.BL.MASEQU.PMResultType();
                //brwChannel.mfCredentials(clsResultType);

                DataTable dtResultType = clsResultType.mfReadPMResultType_MESIF(strPlantCode);
                #endregion

                #region Create ReturnMessage
                XmlNode _node = GetXml.CreateElement("PMITEMLIST");

                for (int i = 0; i < dtPlanD.Rows.Count; i++)
                {
                    XmlNode _item = GetXml.CreateElement("PMITEM");

                    XmlNode _child = GetXml.CreateElement("PMID");
                    _child.InnerText = dtPlanD.Rows[i]["Seq"].ToString();
                    _item.AppendChild(_child);

                    _child = GetXml.CreateElement("PERIOD");
                    _child.InnerText = dtPlanD.Rows[i]["PMPeriodName"].ToString();
                    _item.AppendChild(_child);

                    _child = GetXml.CreateElement("CHECKPOSITION");
                    _child.InnerText = dtPlanD.Rows[i]["PMInspectRegion"].ToString();
                    _item.AppendChild(_child);

                    _child = GetXml.CreateElement("CHECKITEM");
                    _child.InnerText = dtPlanD.Rows[i]["PMInspectName"].ToString();
                    _item.AppendChild(_child);

                    _node.AppendChild(_item);
                }

                GetXml.AppendChild(_node);

                if (dtPlanD.Rows.Count > 0)
                    GetXml.SelectSingleNode("message/body/REQUESTID").InnerText = dtPlanD.Rows[0]["PlanYear"].ToString();

                _node = GetXml.CreateElement("CHECKRESULTLIST");

                for (int i = 0; i < dtResultType.Rows.Count; i++)
                {
                    XmlNode _item = GetXml.CreateElement("CHECKRESULT");

                    XmlNode _child = GetXml.CreateElement("CHECKCODE");
                    _child.InnerText = dtResultType.Rows[i]["PMResultCode"].ToString();
                    _item.AppendChild(_child);

                    _child = GetXml.CreateElement("CHECKDESC");
                    _child.InnerText = dtResultType.Rows[i]["PMResultName"].ToString();
                    _item.AppendChild(_child);

                    _node.AppendChild(_item);
                }

                GetXml.SelectSingleNode("message/body").AppendChild(_node);

                #endregion

                #region Final
                string strreturncode = "0";
                string strreturnmessage = "";
                if (dtPlanD.Rows.Count.Equals(0))
                {
                    strreturncode = "99";
                    strreturnmessage = "Don’t Exist PM Item List";
                }
                GetXml = ChangeSubjectName(GetXml, "PM_LIST_REP");
                SetReturnMessage(GetXml, msg, trans, strreturncode, strreturnmessage);
                mfWindowsLogcheck("PM_LIST_REQ Success");
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// PM점검결과를 등록
        /// </summary>
        public void PM_RESULT_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var
                string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strEquipCode = GetXml.SelectSingleNode("message/body/EQPID").InnerText;
                string strPMPlanDate = GetXml.SelectSingleNode("message/body/CHECKDATE").InnerText;
                string strPlanYear = GetXml.SelectSingleNode("message/body/REQUESTID").InnerText;
                string strPMWorkID = GetXml.SelectSingleNode("message/body/USERID").InnerText;

                DataTable dtVar = new DataTable();
                dtVar.Columns.Add("EquipCode", typeof(string));
                dtVar.Columns.Add("Seq", typeof(string));
                dtVar.Columns.Add("PMResultCode", typeof(string));
                dtVar.Columns.Add("ChangeFlag", typeof(string));
                dtVar.Columns.Add("RepairFlag", typeof(string));
                dtVar.Columns.Add("PMWorkDesc", typeof(string));
                dtVar.Columns.Add("PMPlanDate", typeof(string));

                XmlNodeList _node = GetXml.SelectNodes("message/body/PMITEMLIST/*");

                for (int i = 0; i < _node.Count; i++)
                {
                    DataRow dr = dtVar.NewRow();

                    dr["Seq"] = _node.Item(i).SelectSingleNode("PMID").InnerText;
                    dr["PMResultCode"] = _node.Item(i).SelectSingleNode("CHECKCODE").InnerText;
                    dr["ChangeFlag"] = _node.Item(i).SelectSingleNode("REPLACEFLAG").InnerText;
                    dr["RepairFlag"] = _node.Item(i).SelectSingleNode("REPAIRFLAG").InnerText;
                    dr["PMWorkDesc"] = _node.Item(i).SelectSingleNode("COMMENT").InnerText;

                    dr["PMPlanDate"] = strPMPlanDate;
                    dr["EquipCode"] = strEquipCode;

                    dtVar.Rows.Add(dr);
                }


                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.PMPlanD), "PMPlanD");
                QRPEQU.BL.EQUMGM.PMPlanD clsPMPlanD = new QRPEQU.BL.EQUMGM.PMPlanD();
                //brwChannel.mfCredentials(clsPMPlanD);

                string strRtn = clsPMPlanD.mfSavePMPlanDResult(strPlantCode, strPlanYear, strPMWorkID, dtVar, m_IP, strPMWorkID);
                #endregion

                #region Final
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                //GetXml = ChangeSubjectName(GetXml, "PM_RESULT_REP");
                //SetReturnMessage(GetXml, ErrRtn, msg, trans);
                if(ErrRtn.ErrMessage.Equals(string.Empty))
                    mfWindowsLogcheck("PM_RESULT_REQ Success");
                else
                    mfWindowsLogcheck("PM_RESULT_REQ Failed");
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// 치공구LotNo에 대한 치공구 정보 재공여부 전송
        /// </summary>
        public void TOOL_INFO_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var
                string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strDurableLotNo = GetXml.SelectSingleNode("message/body/TOOLID").InnerText;
                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                //brwChannel.mfCredentials(clsDurableLot);

                DataTable dtVar = clsDurableLot.mfReadTOOL_INFO_REQ(strPlantCode, strDurableLotNo);
                #endregion

                #region Create ReturnMessage
                XmlNode _node = GetXml.CreateElement("TOOLSPECID");
                _node.InnerText = dtVar.Rows[0]["DurableMatCode"].ToString();
                GetXml.SelectSingleNode("message/body").AppendChild(_node);

                string strReturnCode = dtVar.Rows[0]["Qty"].ToString();
                string strReturMessage = "";

                if (strReturnCode.Equals("0"))
                {
                    strReturMessage = "Don’t Exist Available Stock In Inventory";
                }
                #endregion

                #region Final
                GetXml = ChangeSubjectName(GetXml, "TOOL_INFO_REP");
                SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);
                mfWindowsLogcheck("TOOL_INFO_REQ Success");
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// OI를 이용하여 설비 상태 변경 처리 완료시 전송
        /// </summary>
        public void EQP_CHANGESTATE_RPT(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            try
            {
                #region Create Var
                string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strUserID = GetXml.SelectSingleNode("message/body/USERID").InnerText;

                DataTable dtStateList = new DataTable();
                dtStateList.Columns.Add("EquipCode", typeof(string));
                dtStateList.Columns.Add("EquipState", typeof(string));
                dtStateList.Columns.Add("OldProductSpecID", typeof(string));
                dtStateList.Columns.Add("ProductSpecID", typeof(string));
                dtStateList.Columns.Add("MachineRecipe", typeof(string));
                dtStateList.Columns.Add("RequestDownCode", typeof(string));
                dtStateList.Columns.Add("ReasonCode", typeof(string));
                dtStateList.Columns.Add("Comment", typeof(string));
                dtStateList.Columns.Add("RequestTime", typeof(string));
                dtStateList.Columns.Add("RequestUser", typeof(string));
                dtStateList.Columns.Add("AcceptTime", typeof(string));
                dtStateList.Columns.Add("AcceptUser", typeof(string));
                dtStateList.Columns.Add("CompleteTime", typeof(string));
                dtStateList.Columns.Add("CompleteUser", typeof(string));
                dtStateList.Columns.Add("ReqComment", typeof(string));
                dtStateList.Columns.Add("AcceptComment", typeof(string));

                XmlNodeList _node = GetXml.SelectNodes("message/body/STATELIST/*");
                DataRow drRow;

                for (int i = 0; i < _node.Count; i++)
                {
                    drRow = dtStateList.NewRow();
                    drRow["EquipCode"] = _node.Item(i).SelectSingleNode("EQPID").InnerText;
                    drRow["EquipState"] = _node.Item(i).SelectSingleNode("EQPSTATE").InnerText;
                    drRow["OldProductSpecID"] = _node.Item(i).SelectSingleNode("OLDPRODUCTSPECID").InnerText;
                    drRow["ProductSpecID"] = _node.Item(i).SelectSingleNode("PRODUCTSPECID").InnerText;
                    drRow["MachineRecipe"] = _node.Item(i).SelectSingleNode("MACHINERECIPE").InnerText;
                    drRow["RequestDownCode"] = _node.Item(i).SelectSingleNode("REQUESTDOWNCODE").InnerText;
                    drRow["ReasonCode"] = _node.Item(i).SelectSingleNode("REASONCODE").InnerText;
                    drRow["Comment"] = _node.Item(i).SelectSingleNode("COMMENT").InnerText;
                    drRow["RequestTime"] = _node.Item(i).SelectSingleNode("REQUESTTIME").InnerText;
                    drRow["RequestUser"] = _node.Item(i).SelectSingleNode("REQUESTUSER").InnerText;
                    drRow["AcceptTime"] = _node.Item(i).SelectSingleNode("ACCEPTTIME").InnerText;
                    drRow["AcceptUser"] = _node.Item(i).SelectSingleNode("ACCEPTUSER").InnerText;
                    drRow["CompleteTime"] = _node.Item(i).SelectSingleNode("COMPLETETIME").InnerText;
                    drRow["CompleteUser"] = _node.Item(i).SelectSingleNode("COMPLETEUSER").InnerText;
                    drRow["ReqComment"] = _node.Item(i).SelectSingleNode("REQCOMMENT").InnerText;
                    drRow["AcceptComment"] = _node.Item(i).SelectSingleNode("ACCEPTCOMMENT").InnerText;
                    dtStateList.Rows.Add(drRow);
                }
                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action
                QRPEQU.BL.EQUREP.RepairReq clsRepair = new QRPEQU.BL.EQUREP.RepairReq();
                string strErrRtn = clsRepair.mfSaveEquipRepair_MESIF_CHANGESTATE_RPT(strPlantCode, strUserID, dtStateList, "QRPUSERVER", m_IP);
                #endregion

                #region Final
                QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //GetXml = ChangeSubjectName(GetXml, "EQP_CHANGESTATE_RQT");
                //SetReturnMessage(GetXml, ErrRtn, msg, trans);
                if (ErrRtn.ErrMessage.Equals(string.Empty))
                    mfWindowsLogcheck("EQP_CHANGESTATE_RPT Success");
                else
                    mfWindowsLogcheck("EQP_CHANGESTATE_RPT Failed");
                #endregion
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                SetReturnMessage(GetXml, msg, trans, ex);
            }
        }

        /// <summary>
        /// OI Track In // Track Out 시 LotNo(Serial No), 생산수량을 받아 LotNo 수명체크
        /// </summary>
        public void MOLD_LIFETYPE_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            string strSourcesubject = GetXml.SelectSingleNode("message/header/sourcesubject").InnerText;        
            string strtargetsubject = GetXml.SelectSingleNode("message/header/targetsubject").InnerText;          

            try
            {
                #region Create Var

                string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;          // 공장
                string strUserID = GetXml.SelectSingleNode("message/body/USERID").InnerText;                // 작업자
                string strLotNo = GetXml.SelectSingleNode("message/body/LOTNAME").InnerText;                // 작업진행중인 LotNo
                string strProductCode = GetXml.SelectSingleNode("message/body/PRODUCTSPECNAME").InnerText;  // 제품코드
                string strProductQty = GetXml.SelectSingleNode("message/body/PRODUCTQUANTITY").InnerText;   // 생산수량
                string strEvenvtName = GetXml.SelectSingleNode("message/body/EVENTNAME").InnerText;   // EVENTNAME
                string strEquipCode = GetXml.SelectSingleNode("message/body/MACHINENAME").InnerText;   // 작업중인설비코드

                string strOLD_EMC = "";
                string strNEW_EMC = "";
                string strOLD_PKG = "";
                string strNEW_PKG = "";

                if (strEvenvtName.Trim().ToUpper().Equals("TKIN") || strEvenvtName.Trim().ToUpper().Equals("TKOUT"))
                {
                    strOLD_EMC = GetXml.SelectSingleNode("message/body/OLD_EMC").InnerText;
                }

                if (strEvenvtName.Trim().ToUpper().Equals("TKIN") || strEvenvtName.Trim().ToUpper().Equals("TKOUT"))
                {
                    strNEW_EMC = GetXml.SelectSingleNode("message/body/NEW_EMC").InnerText;
                }

                if (strEvenvtName.Trim().ToUpper().Equals("TKIN") || strEvenvtName.Trim().ToUpper().Equals("TKOUT"))
                {
                    strOLD_PKG = GetXml.SelectSingleNode("message/body/OLD_PKG").InnerText;
                }

                if (strEvenvtName.Trim().ToUpper().Equals("TKIN") || strEvenvtName.Trim().ToUpper().Equals("TKOUT"))
                {
                    strNEW_PKG = GetXml.SelectSingleNode("message/body/NEW_PKG").InnerText;
                }

                string strProcessCode = GetXml.SelectSingleNode("message/body/PROCESSOPERATIONNAME").InnerText; 
                
                DataTable dtDurableInfo = new DataTable();

                dtDurableInfo.Columns.Add("CONSUMABLENAME", typeof(string));  // SerialNo
                dtDurableInfo.Columns.Add("CONSUMABLESPEC", typeof(string));  // SpecNo


                XmlNodeList _node = GetXml.SelectNodes("message/body/CONSUMABLELIST/*");
                //DataRow drRow;

                // 설비안에 장착하는 SerialNo(치공구LotNo)가 여러개 있을 수있다.
                #region 修改TIB 发送的内容
                //for (int i = 0; i < _node.Count; i++)
                //{
                //    if (dtDurableInfo.Columns.Contains(_node[i].Name))
                //    {
                //        drRow = dtDurableInfo.NewRow();

                //        drRow[_node[i].Name] = _node[i].InnerText;  // OI에서 받아온 작업중인 SrialNo(치공구LotNo)

                //        dtDurableInfo.Rows.Add(drRow);
                //    }
                //}
                for (int i = 0; i < _node.Count; i++)
                {
                    DataRow drRow = dtDurableInfo.NewRow();
                    for (int j = 0; j < _node.Item(i).ChildNodes.Count; j++)
                    {
                        if (drRow.Table.Columns.Contains(_node.Item(i).ChildNodes[j].Name))
                        {
                            drRow[_node.Item(i).ChildNodes[j].Name] = _node.Item(i).ChildNodes[j].InnerText;
                        }
                    }
                    dtDurableInfo.Rows.Add(drRow);
                }
                #endregion

                dtDurableInfo.Columns["CONSUMABLENAME"].ColumnName = "SerialNo";
                dtDurableInfo.Columns["CONSUMABLESPEC"].ColumnName = "SpecNo";

                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action

                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpec = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                DataTable dtRtn = new DataTable();
                string strErrRtn = string.Empty;

                // EVENTNAME = "TKin" 
                if (strEvenvtName.Trim().ToUpper().Equals("TKIN"))
                {

                    #region Create ReturnMessage

                    // ReturnCode 정의 Y : 안정, N : 초과, 90 : 90%이상, 99  : 실패, 공백 : Serial정보없음, F : 이 Serial은 작업중인 설비에 사용할 수 없다.
                    string strReturnCode = string.Empty;
                    string strReturMessage = string.Empty;

                    if (dtDurableInfo.Rows.Count > 0)
                    {
                        if (strProcessCode.Trim().ToUpper().Equals("A7100")) 
                        {
                            dtRtn = clsLotSpec.mfReadMOLD_LIFETYPE_REQ_Tkin_EMC(strPlantCode, strEquipCode, strProductQty, dtDurableInfo, strProductCode, strOLD_EMC, strNEW_EMC, strOLD_PKG, strNEW_PKG);
                        }
                        else
                        {
                            dtRtn = clsLotSpec.mfReadMOLD_LIFETYPE_REQ_Tkin(strPlantCode, strEquipCode, strProductQty, dtDurableInfo);
                        }

                        strReturnCode = dtRtn.Rows[0]["ReturnCode"].ToString().Trim();
                        strReturMessage = dtRtn.Rows[0]["ReturnMessage"].ToString().Trim();
                    }
                    
                    #endregion

                    #region Final

                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;          
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;          
                    
                    //GetXml = ChangeSubjectName(GetXml, "MOLD_LIFETYPE_REQ");
                    SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);

                    mfWindowsLogcheck("MOLD_LIFETYPE_REQ_Tkin Success("+strReturnCode+" : "+ strReturMessage +")");



                    #endregion

                }
                else if (strEvenvtName.Trim().ToUpper().Equals("TKOUT")) //  EVENTNAME = "TKout"
                {
                    string strReturnCode = string.Empty;
                    string strReturMessage = string.Empty;

                    if (strProcessCode.Trim().ToUpper().Equals("A7100"))
                    {
                        strErrRtn = clsLotSpec.mfSaveMOLD_LIFETYPE_REQ_TkOut_EMC(strPlantCode, strEquipCode, strLotNo,
                                                                            strProductCode, strProductQty,
                                                                            strEvenvtName, dtDurableInfo,strNEW_EMC,strNEW_PKG,
                                                                            m_IP, "QRPServer");
                    }
                    else
                    {
                        strErrRtn = clsLotSpec.mfSaveMOLD_LIFETYPE_REQ_TkOut(strPlantCode, strEquipCode, strLotNo,
                                                                            strProductCode, strProductQty,
                                                                            strEvenvtName, dtDurableInfo,
                                                                            m_IP, "QRPServer");
                    }

                    #region Final

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                    {
                        strReturnCode = "Y";
                        strReturMessage = "Success";
                        SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);
                        mfWindowsLogcheck("MOLD_LIFETYPE_REQ_Tkout Success");
                    }
                    else
                    {
                        strReturnCode = "N";
                        strReturMessage = "Failed--" + ErrRtn.ErrMessage;
                        SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);
                        mfWindowsLogcheck("MOLD_LIFETYPE_REQ_Tkout Failed(" + ErrRtn.ErrMessage + ")");
                    }

                    #endregion

                }
                else if (strEvenvtName.Trim().ToUpper().Equals("KIT")) //  EVENTNAME = "KIT"
                {

                    #region Create ReturnMessage

                    // ReturnCode 정의 Y : 안정, N : 초과, 90 : 90%이상, 99  : 실패, 공백 : Serial정보없음, F : 이 Serial은 작업중인 설비에 사용할 수 없다.
                    string strReturnCode = string.Empty;
                    string strReturMessage = string.Empty;

                    if (dtDurableInfo.Rows.Count > 0)
                    {
                        dtRtn = clsLotSpec.mfReadMOLD_LIFETYPE_REQ_KIT(strPlantCode, strEquipCode, strProductQty, dtDurableInfo);

                        strReturnCode = dtRtn.Rows[0]["ReturnCode"].ToString().Trim();
                        strReturMessage = dtRtn.Rows[0]["ReturnMessage"].ToString().Trim();

                    }

                    #endregion

                    #region Final

                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    //GetXml = ChangeSubjectName(GetXml, "MOLD_LIFETYPE_REQ");
                    SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);

                    mfWindowsLogcheck("MOLD_LIFETYPE_REQ_KIT Success(" + strReturnCode + " : " + strReturMessage + ")");
                    #endregion
                }
                else if (strEvenvtName.Trim().ToUpper().Equals("RECEIVE")) //  EVENTNAME = "KIT"
                {
                    #region Create ReturnMessage

                    // ReturnCode 정의 Y : 안정, N : 초과, 90 : 90%이상, 99  : 실패, 공백 : Serial정보없음, F : 이 Serial은 작업중인 설비에 사용할 수 없다.
                    string strReturnCode = string.Empty;
                    string strReturMessage = string.Empty;

                    if (dtDurableInfo.Rows.Count > 0)
                    {
                        dtRtn = clsLotSpec.mfReadMOLD_LIFETYPE_REQ_RECEVE(strPlantCode, strEquipCode, strProductQty, dtDurableInfo);

                        strReturnCode = dtRtn.Rows[0]["ReturnCode"].ToString().Trim();
                        strReturMessage = dtRtn.Rows[0]["ReturnMessage"].ToString().Trim();
                    }

                    #endregion

                    #region Final

                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    //GetXml = ChangeSubjectName(GetXml, "MOLD_LIFETYPE_REQ");
                    SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);

                    mfWindowsLogcheck("MOLD_LIFETYPE_REQ_KIT Success(" + strReturnCode + " : " + strReturMessage + ")");

                    #endregion
                }
                else
                {
                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    // EVENTNAME 공백인경우 Return ErrorData
                    SetReturnMessage(GetXml, msg, trans, "99", "EVENTNAME IS NULL");

                    mfWindowsLogcheck("MOLD_LIFETYPE_REQ Failed (99 : EVENTNAME IS NULL)");
                }

                #endregion

                // 리소스 해제
                clsLotSpec.Dispose();
                dtDurableInfo.Dispose();
                dtRtn.Dispose();

            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
                
                // 변경하여 ReturnMessage
                GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;  

                //SetReturnMessage(GetXml, msg, trans, ex);
                SetReturnMessage(GetXml, msg, trans, "99", ex.Message);
            }
        }

        /// <summary>
        /// socket寿命管理
        /// </summary>
        public void LOT_SOCKET_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans) 
        {
            System.IO.StreamWriter log = new System.IO.StreamWriter("I:/" + "LOT_SOCKET_REQ_method.txt", true);

            log.WriteLine("1--");

            string strSourcesubject = GetXml.SelectSingleNode("message/header/sourcesubject").InnerText;
            string strtargetsubject = GetXml.SelectSingleNode("message/header/targetsubject").InnerText;

            try
            {
                #region Create Var


                //string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;          // 공장
                //string strUserID = GetXml.SelectSingleNode("message/body/USERID").InnerText;                // 작업자
                //string strLotNo = GetXml.SelectSingleNode("message/body/LOTNAME").InnerText;                // 작업진행중인 LotNo
                string strProductCode = GetXml.SelectSingleNode("message/body/PRODUCTSPECNAME").InnerText;  // 제품코드
                string strProductQty = GetXml.SelectSingleNode("message/body/PRODUCTQUANTITY").InnerText;   // 생산수량
                //string strEvenvtName = GetXml.SelectSingleNode("message/body/EVENTNAME").InnerText;   // EVENTNAME
                //string strEquipCode = GetXml.SelectSingleNode("message/body/MACHINENAME").InnerText;   // 작업중인설비코드

                string strPlantCode = "2210";
                string strEvenvtName = "SOCKET";

                DataTable dtDurableInfo = new DataTable();

                dtDurableInfo.Columns.Add("CONSUMABLENAME", typeof(string));  // SerialNo
                dtDurableInfo.Columns.Add("CONSUMABLETYPE", typeof(string));  // SpecNo


                XmlNodeList _node = GetXml.SelectNodes("message/body/CONSUMABLELIST/*");
                //DataRow drRow;

                // 설비안에 장착하는 SerialNo(치공구LotNo)가 여러개 있을 수있다.
                #region 修改TIB 发送的内容

                for (int i = 0; i < _node.Count; i++)
                {
                    DataRow drRow = dtDurableInfo.NewRow();
                    for (int j = 0; j < _node.Item(i).ChildNodes.Count; j++)
                    {
                        if (drRow.Table.Columns.Contains(_node.Item(i).ChildNodes[j].Name))
                        {
                            drRow[_node.Item(i).ChildNodes[j].Name] = _node.Item(i).ChildNodes[j].InnerText;
                        }
                    }
                    dtDurableInfo.Rows.Add(drRow);
                }
                #endregion

                dtDurableInfo.Columns["CONSUMABLENAME"].ColumnName = "SerialNo";
                dtDurableInfo.Columns["CONSUMABLETYPE"].ColumnName = "SerialType";

                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action

                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpec = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                DataTable dtRtn = new DataTable();
                string strErrRtn = string.Empty;

                // EVENTNAME = "SOCKET" 
                if (strEvenvtName.Trim().ToUpper().Equals("SOCKET"))
                {

                    #region Create ReturnMessage

                    string strReturnCode = string.Empty;
                    string strReturMessage = string.Empty;

                    if (dtDurableInfo.Rows.Count > 0)
                    {
                        dtRtn = clsLotSpec.mfReadLOT_SOCKET_REQ_Tkin(strPlantCode, strProductQty, dtDurableInfo);

                        strReturnCode = dtRtn.Rows[0]["ReturnCode"].ToString().Trim();
                        strReturMessage = dtRtn.Rows[0]["ReturnMessage"].ToString().Trim();

                        log.WriteLine("2--");
                        log.WriteLine( strReturnCode + strReturMessage );
                    }

                    #endregion

                    #region Final

                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    //GetXml = ChangeSubjectName(GetXml, "MOLD_LIFETYPE_REQ");
                    SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);

                    log.WriteLine("3--");

                    mfWindowsLogcheck("LOT_SOCKET_REQ Success(" + strReturnCode + " : " + strReturMessage + ")");

                    log.WriteLine("4--");

                    log.Close();

                    #endregion

                }
                else
                {
                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    // EVENTNAME 공백인경우 Return ErrorData
                    SetReturnMessage(GetXml, msg, trans, "99", "EVENTNAME IS NULL");

                    mfWindowsLogcheck("LOT_SOCKET_REQ Failed (99 : EVENTNAME IS NULL)");
                }

                #endregion

                // 리소스 해제
                clsLotSpec.Dispose();
                dtDurableInfo.Dispose();
                dtRtn.Dispose();

            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);

                // 변경하여 ReturnMessage
                GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                //SetReturnMessage(GetXml, msg, trans, ex);
                SetReturnMessage(GetXml, msg, trans, "99", ex.Message);
            }
        }

        /// <summary>
        /// JIG次数检查
        /// </summary>
        public void CONSUMABLE_JIG_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans) 
        {

            string strSourcesubject = GetXml.SelectSingleNode("message/header/sourcesubject").InnerText;
            string strtargetsubject = GetXml.SelectSingleNode("message/header/targetsubject").InnerText;

            try
            {
                #region Create Var


                //string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;          // 공장
                //string strUserID = GetXml.SelectSingleNode("message/body/USERID").InnerText;                // 작업자
                //string strLotNo = GetXml.SelectSingleNode("message/body/LOTNAME").InnerText;
                string SerialNo = GetXml.SelectSingleNode("message/body/CONSUMABLENAME").InnerText;  // 제품코드
                string SpecNo = GetXml.SelectSingleNode("message/body/CONSUMABLESPECNAME").InnerText; 
                //string strEvenvtName = GetXml.SelectSingleNode("message/body/EVENTNAME").InnerText;   // EVENTNAME

                string strPlantCode = "2210";
                string strEvenvtName = "JIG";

                #endregion

                //#region Server Check
                //if (!CheckQRPServer())
                //    ChangeRemoteServerAddress();

                //CheckQRPServer("1");
                //#endregion

                #region Action

                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpec = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                DataTable dtRtn = new DataTable();
                string strErrRtn = string.Empty;

                if (strEvenvtName.Trim().ToUpper().Equals("JIG"))
                {

                    #region Create ReturnMessage

                    // ReturnCode 정의 Y : 안정, N : 초과, 90 : 90%이상, 99  : 실패, 공백 : Serial정보없음, F : 이 Serial은 작업중인 설비에 사용할 수 없다.
                    string strReturnCode = string.Empty;
                    string strReturMessage = string.Empty;

                        dtRtn = clsLotSpec.mfReadCONSUMABLE_JIG_REQ(strPlantCode, SerialNo);

                        strReturnCode = dtRtn.Rows[0]["ReturnCode"].ToString().Trim();
                        strReturMessage = dtRtn.Rows[0]["ReturnMessage"].ToString().Trim();

                    #endregion

                    #region Final

                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);

                    mfWindowsLogcheck("CONSUMABLE_JIG_REQ  Success(" + strReturnCode + " : " + strReturMessage + ")");

                    #endregion

                }
                else
                {

                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    // EVENTNAME 공백인경우 Return ErrorData
                    SetReturnMessage(GetXml, msg, trans, "99", "EVENTNAME IS NULL");

                    mfWindowsLogcheck("CONSUMABLE_JIG_REQ  Failed (99 : EVENTNAME IS NULL)");
                }

                #endregion

                // 리소스 해제
                clsLotSpec.Dispose();
                dtRtn.Dispose();

            }
            catch (Exception ex)
            {

                mfWindowsLogcheck(ex);

                // 변경하여 ReturnMessage
                GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                //SetReturnMessage(GetXml, msg, trans, ex);
                SetReturnMessage(GetXml, msg, trans, "99", ex.Message);

            }
        }

        /// <summary>
        /// OI Track In 确认事项
        /// </summary>
        public void LOT_TKINCHK_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            string strSourcesubject = GetXml.SelectSingleNode("message/header/sourcesubject").InnerText;
            string strtargetsubject = GetXml.SelectSingleNode("message/header/targetsubject").InnerText;

            try
            {
                #region Create Var

                string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;          // 获取工厂ID
                string strUserID = GetXml.SelectSingleNode("message/body/USERID").InnerText;                // USERID
                string strLotNo = GetXml.SelectSingleNode("message/body/LOTNAME").InnerText;                // 获取LOTNAME
                string strEvenvtName = GetXml.SelectSingleNode("message/body/EVENTNAME").InnerText;   // EVENTNAME
                string strProductCode = GetXml.SelectSingleNode("message/body/PRODUCTSPECNAME").InnerText;  // 产片ID
                string strProductQty = GetXml.SelectSingleNode("message/body/PRODUCTQUANTITY").InnerText;   // LOT数量
                string strCustomer = GetXml.SelectSingleNode("message/body/CUSTOMER").InnerText;      //获取客户名
                string strProcessCode = GetXml.SelectSingleNode("message/body/PROCESSOPERATIONNAME").InnerText;      //获取工序CODE
                string strPackage = GetXml.SelectSingleNode("message/body/PACKAGE").InnerText;      //获取PACKAGE
                string strMaterialType = GetXml.SelectSingleNode("message/body/MATERIALTYPE").InnerText;      //获取PACKAGE

                DataTable dtDurableInfo = new DataTable();

                dtDurableInfo.Columns.Add("MACHINENAME", typeof(string));  // 设备名称
                dtDurableInfo.Columns.Add("MACHINEACTIONTYPE", typeof(string));  // 设备组类型
                dtDurableInfo.Columns.Add("MACHINEGROUPNAME", typeof(string));   //设备组

                XmlNodeList _node = GetXml.SelectNodes("message/body/MACHINELIST/*");

                // 循环获取XML里面的MACHINELIST里面的内容
                #region TIB 发送的内容
                for (int i = 0; i < _node.Count; i++)
                {
                    DataRow drRow = dtDurableInfo.NewRow();
                    for (int j = 0; j < _node.Item(i).ChildNodes.Count; j++)
                    {
                        if (drRow.Table.Columns.Contains(_node.Item(i).ChildNodes[j].Name))
                        {
                            drRow[_node.Item(i).ChildNodes[j].Name] = _node.Item(i).ChildNodes[j].InnerText;
                        }
                    }
                    dtDurableInfo.Rows.Add(drRow);
                }
                #endregion

                dtDurableInfo.Columns["MACHINENAME"].ColumnName = "Machine";
                dtDurableInfo.Columns["MACHINEACTIONTYPE"].ColumnName = "MachineType";
                dtDurableInfo.Columns["MACHINEGROUPNAME"].ColumnName = "MachineGroup";
                #endregion

                #region Action

                QRPQAT.BL.QATQUL.MachineModelAdmit clsLotTkinchk = new QRPQAT.BL.QATQUL.MachineModelAdmit();
                DataTable dtRtn = new DataTable();
                string strErrRtn = string.Empty;

                // EVENTNAME = "TKin"   TKIN时确认改产品是否允许在该设备上生产 - Machine Model审批情报
                if (strEvenvtName.Trim().ToUpper().Equals("TKINMACHINECHK"))
                {

                    #region Create ReturnMessage

                    string strReturnCode = string.Empty;
                    string strReturMessage = string.Empty;

                    if (dtDurableInfo.Rows.Count > 0)
                    {
                        dtRtn = clsLotTkinchk.mfReadLOT_TKINCHK_REQ_TKINMACHINECHECK(strPlantCode, strProductCode, strCustomer, strPackage, dtDurableInfo);

                        strReturnCode = dtRtn.Rows[0]["ReturnCode"].ToString().Trim();
                        strReturMessage = dtRtn.Rows[0]["ReturnMessage"].ToString().Trim();
                    }

                    #endregion

                    #region Final

                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    //GetXml = ChangeSubjectName(GetXml, "LOT_TKINCHK_REQ");
                    SetReturnMessage(GetXml, msg, trans, strReturnCode, strReturMessage);

                    mfWindowsLogcheck("mfReadLOT_TKINCHK_REQ_TKINMACHINECHECK Success(" + strReturnCode + " : " + strReturMessage + ")");
                    #endregion

                }
                else
                {
                    // 변경하여 ReturnMessage
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    // EVENTNAME 공백인경우 Return ErrorData
                    SetReturnMessage(GetXml, msg, trans, "99", "EVENTNAME IS NULL");

                    mfWindowsLogcheck("LOT_TKINCHK_REQ Failed (99 : EVENTNAME IS NULL)");
                }

                #endregion

                // 리소스 해제
                clsLotTkinchk.Dispose();
                dtDurableInfo.Dispose();
                dtRtn.Dispose();

            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);

                // 변경하여 ReturnMessage
                GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                //SetReturnMessage(GetXml, msg, trans, ex);
                SetReturnMessage(GetXml, msg, trans, "99", ex.Message);
            }
        }

        /// <summary>
        /// OI LOT_SPECVIEW
        /// </summary>
        public void LOT_SPECVIEW_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            string strSourcesubject = GetXml.SelectSingleNode("message/header/sourcesubject").InnerText;
            string strtargetsubject = GetXml.SelectSingleNode("message/header/targetsubject").InnerText;

            try
            {
                #region Create Var

                string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText; 
                string strUserID = GetXml.SelectSingleNode("message/body/USERID").InnerText;
                string strEvenvtName = GetXml.SelectSingleNode("message/body/EVENTNAME").InnerText;

                DataTable dtInfo = new DataTable();
                dtInfo.Columns.Add("BD_DIAGRAM", typeof(string));
                dtInfo.Columns.Add("MK_LAYOUT", typeof(string));
                dtInfo.Columns.Add("PK_SPEC", typeof(string));
                dtInfo.Columns.Add("TF_SPEC", typeof(string));

                XmlNodeList _node = GetXml.SelectNodes("message/body/VIEWLIST/*");

                #region TIB 发送的内容
                for (int i = 0; i < _node.Count; i++)
                {
                    DataRow drRow = dtInfo.NewRow();
                    for (int j = 0; j < _node.Item(i).ChildNodes.Count; j++)
                    {
                        if (drRow.Table.Columns.Contains(_node.Item(i).ChildNodes[j].Name))
                        {
                            drRow[_node.Item(i).ChildNodes[j].Name] = _node.Item(i).ChildNodes[j].InnerText;
                        }
                    }
                    dtInfo.Rows.Add(drRow);
                }
                #endregion

                dtInfo.Columns["BD_DIAGRAM"].ColumnName = "BondSpecCode";
                dtInfo.Columns["MK_LAYOUT"].ColumnName = "MarkSpecCode";
                dtInfo.Columns["PK_SPEC"].ColumnName = "PKSpecCode";
                dtInfo.Columns["TF_SPEC"].ColumnName = "TFSpecCode";
                #endregion

                #region Action
                QRPISO.BL.ISODOC.ISODocFile clsSpec = new QRPISO.BL.ISODOC.ISODocFile();
                DataTable dtRtn = new DataTable();
                string strErrRtn = string.Empty;

                if (strEvenvtName.Trim().ToUpper().Equals("SPECFILEVIEW"))
                {

                    #region Create ReturnMessage

                    string strBondspec = string.Empty;
                    string strMarkspec = string.Empty;
                    string strPKspec = string.Empty;
                    string strTFspec = string.Empty;

                    if (dtInfo.Rows.Count > 0)
                    {
                        dtRtn = clsSpec.mfReadLOT_SpecViewFileName(strPlantCode, dtInfo);

                        strBondspec = dtRtn.Rows[0]["Bondcode"].ToString().Trim();
                        strMarkspec = dtRtn.Rows[0]["Markcode"].ToString().Trim();
                        strPKspec = dtRtn.Rows[0]["PKcode"].ToString().Trim();
                        strTFspec = dtRtn.Rows[0]["TFcode"].ToString().Trim();
                    }

                    #endregion

                    #region Final
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    XmlDocument _xml = new XmlDocument();

                    XmlNode _body = GetXml.SelectSingleNode("message/body");

                    XmlElement _path = _xml.CreateElement("PATH");

                    XmlElement _itemBD = _xml.CreateElement("BD_DIAGRAMPATH");
                    _itemBD.InnerText = strBondspec;
                    XmlElement _itemMK = _xml.CreateElement("MK_LAYOUTPATH");
                    _itemMK.InnerText = strMarkspec;

                    XmlElement _itemPK = _xml.CreateElement("PK_SPECPATH");
                    _itemPK.InnerText = strPKspec;

                    XmlElement _itemTF = _xml.CreateElement("TF_SPECPATH");
                    _itemTF.InnerText = strTFspec;

                    _path.AppendChild(_itemBD);
                    _path.AppendChild(_itemMK);
                    _path.AppendChild(_itemPK);
                    _path.AppendChild(_itemTF);

                    XmlElement _pathlist = _xml.CreateElement("PATHLIST");
                    _pathlist.AppendChild(_path);

                    XmlNode _xn = _xml.CreateElement("ROOT");
                    _xn.AppendChild(_pathlist);

                    foreach (XmlNode oNode in _xn)
                    {
                        _body.AppendChild(GetXml.ImportNode(oNode, true));
                    }

                    //SetReturnMessage(GetXml, msg, trans);
                    SetReturnMessage(GetXml, msg, trans, "Y", "");

                    mfWindowsLogcheck("LOT_SPECVIEW_REQ Success: Return Message" +
                                      "\nSend:\n BD_DIAGRAM: " + dtInfo.Rows[0]["BondSpecCode"].ToString() +
                                      "\n MK_LAYOUT: " + dtInfo.Rows[0]["MarkSpecCode"].ToString() +
                                      "\n PK_SPEC: " + dtInfo.Rows[0]["PKSpecCode"].ToString() +
                                      "\n TF_SPEC: " + dtInfo.Rows[0]["TFSpecCode"].ToString() +
                                      "\nReply:\n BD_DIAGRAM: " + strBondspec +
                                      "\n MK_LAYOUT: " + strMarkspec +
                                      "\n PK_SPEC: " + strPKspec +
                                      "\n TF_SPEC: " + strTFspec);
                    #endregion
                }
                else
                {
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    // EVENTNAME 공백인경우 Return ErrorData
                    SetReturnMessage(GetXml, msg, trans, "99", "EVENTNAME IS NULL");

                    mfWindowsLogcheck("LOT_SPECVIEW_REQ Failed (99 : EVENTNAME IS NULL)");
                }

                #endregion

                // 리소스 해제
                clsSpec.Dispose();
                dtInfo.Dispose();
                dtRtn.Dispose();

            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);

                // 변경하여 ReturnMessage
                GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                //SetReturnMessage(GetXml, msg, trans, ex);
                SetReturnMessage(GetXml, msg, trans, "99", ex.Message);
            }
        }


        public void LOT_RECIPEVIEW_REQ(TIBCO.Rendezvous.Message msg, XmlDocument GetXml, string m_IP, Transport trans)
        {
            string strSourcesubject = GetXml.SelectSingleNode("message/header/sourcesubject").InnerText;
            string strtargetsubject = GetXml.SelectSingleNode("message/header/targetsubject").InnerText;

            try
            {
                #region Create Var

                string strPlantCode = GetXml.SelectSingleNode("message/body/FACTORYID").InnerText;
                string strUserID = GetXml.SelectSingleNode("message/body/USERID").InnerText;
                string strEvenvtName = GetXml.SelectSingleNode("message/body/EVENTNAME").InnerText;
                
                #endregion

                #region Action
                QRPQAT.BL.QATQUL.MachineModelAdmit clsSpec = new QRPQAT.BL.QATQUL.MachineModelAdmit();
                DataTable dtRtn = new DataTable();
                string strErrRtn = string.Empty;

                if (strEvenvtName.Trim().ToUpper().Equals("ISSUERECIPEVIEW"))
                {
                    string strCustomer = GetXml.SelectSingleNode("message/body/CUSTOMER").InnerText;
                    string strLocation = GetXml.SelectSingleNode("message/body/LOCATION").InnerText;
                    string strPackage = GetXml.SelectSingleNode("message/body/PACKAGE").InnerText;

                    #region Create ReturnMessage

                    string strMachineGroupCode = string.Empty;

                    dtRtn = clsSpec.mfReadQATMahcineAdmitGroupList(strPlantCode, strCustomer, strPackage, strLocation);

                    #endregion

                    #region Final
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    XmlDocument _xml = new XmlDocument();

                    XmlNode _body = GetXml.SelectSingleNode("message/body");

                    XmlElement _pathlist = _xml.CreateElement("EQUIPGROUPLIST");

                    for (int i = 0; i < dtRtn.Rows.Count; i++)
                    {
                        XmlElement _path = _xml.CreateElement("EQUIPGROUP");

                        if (dtRtn != null || dtRtn.Rows.Count > 0)
                        {
                            strMachineGroupCode = dtRtn.Rows[i]["EquipGroupCode"].ToString().Trim();

                            XmlElement _itemEquipGroupCode = _xml.CreateElement("EquipGroupCode");
                            _itemEquipGroupCode.InnerText = strMachineGroupCode;
                            _path.AppendChild(_itemEquipGroupCode);
                        }

                        _pathlist.AppendChild(_path);
                    }

                    XmlNode _xn = _xml.CreateElement("ROOT");
                    _xn.AppendChild(_pathlist);


                    foreach (XmlNode oNode in _xn)
                    {
                        _body.AppendChild(GetXml.ImportNode(oNode, true));
                    }

                    //SetReturnMessage(GetXml, msg, trans);
                    SetReturnMessage(GetXml, msg, trans, "Y", "");

                    mfWindowsLogcheck("LOT_RECIPEVIEW_REQ =》 ISSUERECIPEVIEW Success: Return Message");
                    #endregion
                }
                else
                {
                    GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                    GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                    // EVENTNAME 공백인경우 Return ErrorData
                    SetReturnMessage(GetXml, msg, trans, "99", "EVENTNAME IS NULL");

                    mfWindowsLogcheck("LOT_RECIPEVIEW_REQ Failed (99 : EVENTNAME IS NULL)");
                }

                #endregion

                // 리소스 해제
                clsSpec.Dispose();
                dtRtn.Dispose();

            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);

                // 변경하여 ReturnMessage
                GetXml.SelectSingleNode("message/header/sourcesubject").InnerText = strtargetsubject;
                GetXml.SelectSingleNode("message/header/targetsubject").InnerText = strSourcesubject;

                //SetReturnMessage(GetXml, msg, trans, ex);
                SetReturnMessage(GetXml, msg, trans, "99", ex.Message);
            }
        }

        #endregion

        #region Server Check
        /// <summary>
        /// QRP IIS서버 상태 체크
        /// </summary>
        /// <returns>연결됨 true else false</returns>
        private bool CheckQRPServer()
        {
            string RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString() + "menu.rem?wsdl";
            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(RemoteServer);
            httpReq.AllowAutoRedirect = false;
            try
            {
                HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse();
                //MessageBox.Show(httpRes.StatusCode.ToString());
                if (httpRes.StatusCode == HttpStatusCode.OK)
                {
                    httpRes.Close();
                    return true;
                }
                else
                {
                    httpRes.Close();
                    return false;
                }

            }
            catch
            {
                return false;
            }
            finally
            {

            }
        }
        private void CheckQRPServer(object obj)
        {
            string RemoteServer = ConfigurationManager.AppSettings["RemoteServer"].ToString() + "menu.rem?wsdl";
            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(RemoteServer);
            httpReq.AllowAutoRedirect = false;
            try
            {
                HttpWebResponse httpRes = (HttpWebResponse)httpReq.GetResponse();
                //MessageBox.Show(httpRes.StatusCode.ToString());
                if (httpRes.StatusCode == HttpStatusCode.OK)
                {
                    httpRes.Close();
                }
                else
                {
                    httpRes.Close();
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {

            }
        }
        private void ChangeRemoteServerAddress()
        {
            try
            {
                XmlDocument tibrv = new XmlDocument();
                //Configuration _tibrv = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                tibrv.Load(System.Windows.Forms.Application.StartupPath + @"\qrptib.xml");

                string MainString = string.Empty;
                string SubString = string.Empty;

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                MainString = tibrv.SelectSingleNode("tibrv/IIS/Main").InnerText.ToString();
                SubString = tibrv.SelectSingleNode("tibrv/IIS/Sub").InnerText.ToString();
                if (config.AppSettings.Settings["RemoteServer"].Value.ToString().Equals(MainString))
                    config.AppSettings.Settings["RemoteServer"].Value = SubString;
                else
                    config.AppSettings.Settings["RemoteServer"].Value = MainString;

                config.Save();
            }
            catch (Exception ex)
            {
                mfWindowsLogcheck(ex);
            }
        }
        #endregion

        #region Error Event
        /// <summary>
        /// 에러 발생시 윈도우 이벤트에 기록하는 Method
        /// 관리도구 > 이벤트 뷰어 > 응용프로그램에서 확인 가능
        /// </summary>
        /// <param name="ex"></param>
        void mfWindowsLogcheck(Exception ex)
        {
            //System.Windows.Forms.MessageBox.Show(ex.Message.ToString(), string.Empty, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly);
            string sSource = "QRPTIB.SV.EventLog";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, ex.Message.ToString(), EventLogEntryType.Error, 55);
        }
        void mfWindowsLogcheck(string End)
        {
            //System.Windows.Forms.MessageBox.Show(ex.Message.ToString(), string.Empty, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1, System.Windows.Forms.MessageBoxOptions.DefaultDesktopOnly);
            string sSource = "QRPTIB.SV.EventLog";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, End, EventLogEntryType.SuccessAudit, 0);
        }
        #endregion
    }
}

