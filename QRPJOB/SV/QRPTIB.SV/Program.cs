﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using System.Collections;
using System.Configuration.Install;
using System.Diagnostics;
using System.Windows.Forms;

namespace QRPTIB.SV
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        static void Main(string[] args)
        {
            
            //프로그램 실행시 qrptib.sv.exe /i 또는 /install, /u 또는 /uninstall 입력시
            //자동으로 윈도우즈 서비스에 등록(제거)됨
            if (args.Length == 1)
            {
                try
                {
                    using (TransactedInstaller ti = new TransactedInstaller())
                    {
                        using (ProjectInstaller pi = new ProjectInstaller())
                        {
                            ti.Installers.Add(pi);
                            string[] cmdline = { string.Format("/assemblypath={0}", System.Reflection.Assembly.GetExecutingAssembly().Location) };
                            pi.Context = new InstallContext(null, cmdline);
                            if (args[0].ToLower() == "/install" || args[0].ToLower() == "/i")
                                pi.Install(new Hashtable());
                            else if (args[0].ToLower() == "/uninstall" || args[0].ToLower() == "/u")
                                pi.Uninstall(null);
                            else
                                throw new Exception("Invalid command line");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine(ex.Message);
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			    { 
				    new QRPTIB()
			    };
                ServiceBase.Run(ServicesToRun);
            }

            // 주석처리 -- Default 프로그램 시작 선언부
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new QRPTIB() 
            //};
            //ServiceBase.Run(ServicesToRun);
        }
    }
}
