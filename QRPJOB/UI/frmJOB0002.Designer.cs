﻿namespace QRPJOB.UI
{
    partial class frmJOB0002
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton3 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton4 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton5 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmJOB0002));
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelUnitTime = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonYear = new Infragistics.Win.Misc.UltraButton();
            this.uDateYear = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateYearUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelYear = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonQuarter = new Infragistics.Win.Misc.UltraButton();
            this.uDateQuarter = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateQuarterUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelQuarter = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonMonth = new Infragistics.Win.Misc.UltraButton();
            this.uDateMonth = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateMonthUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelMonth = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonWeek = new Infragistics.Win.Misc.UltraButton();
            this.uDateWeek = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateWeekUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelWeek = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDay = new Infragistics.Win.Misc.UltraButton();
            this.uDateDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateDayUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelDay = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateYearUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateQuarter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateQuarterUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMonthUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWeekUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDayUnit)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uLabelUnitTime);
            this.ultraGroupBox1.Controls.Add(this.uButtonYear);
            this.ultraGroupBox1.Controls.Add(this.uDateYear);
            this.ultraGroupBox1.Controls.Add(this.uDateYearUnit);
            this.ultraGroupBox1.Controls.Add(this.uLabelYear);
            this.ultraGroupBox1.Controls.Add(this.uButtonQuarter);
            this.ultraGroupBox1.Controls.Add(this.uDateQuarter);
            this.ultraGroupBox1.Controls.Add(this.uDateQuarterUnit);
            this.ultraGroupBox1.Controls.Add(this.uLabelQuarter);
            this.ultraGroupBox1.Controls.Add(this.uButtonMonth);
            this.ultraGroupBox1.Controls.Add(this.uDateMonth);
            this.ultraGroupBox1.Controls.Add(this.uDateMonthUnit);
            this.ultraGroupBox1.Controls.Add(this.uLabelMonth);
            this.ultraGroupBox1.Controls.Add(this.uButtonWeek);
            this.ultraGroupBox1.Controls.Add(this.uDateWeek);
            this.ultraGroupBox1.Controls.Add(this.uDateWeekUnit);
            this.ultraGroupBox1.Controls.Add(this.uLabelWeek);
            this.ultraGroupBox1.Controls.Add(this.uButtonDay);
            this.ultraGroupBox1.Controls.Add(this.uDateDay);
            this.ultraGroupBox1.Controls.Add(this.uDateDayUnit);
            this.ultraGroupBox1.Controls.Add(this.uLabelDay);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(449, 247);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "ultraGroupBox1";
            // 
            // uLabelUnitTime
            // 
            this.uLabelUnitTime.Location = new System.Drawing.Point(228, 28);
            this.uLabelUnitTime.Name = "uLabelUnitTime";
            this.uLabelUnitTime.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnitTime.TabIndex = 63;
            this.uLabelUnitTime.Text = "ultraLabel2";
            // 
            // uButtonYear
            // 
            this.uButtonYear.Location = new System.Drawing.Point(336, 204);
            this.uButtonYear.Name = "uButtonYear";
            this.uButtonYear.Size = new System.Drawing.Size(88, 24);
            this.uButtonYear.TabIndex = 62;
            this.uButtonYear.Text = "ultraButton4";
            this.uButtonYear.Click += new System.EventHandler(this.uButtonYear_Click);
            // 
            // uDateYear
            // 
            this.uDateYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateYear.Location = new System.Drawing.Point(112, 204);
            this.uDateYear.MaskInput = "{LOC}yyyy/mm";
            this.uDateYear.Name = "uDateYear";
            this.uDateYear.Size = new System.Drawing.Size(100, 21);
            this.uDateYear.TabIndex = 61;
            // 
            // uDateYearUnit
            // 
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateYearUnit.ButtonsRight.Add(spinEditorButton1);
            this.uDateYearUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateYearUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateYearUnit.Location = new System.Drawing.Point(228, 204);
            this.uDateYearUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateYearUnit.Name = "uDateYearUnit";
            this.uDateYearUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateYearUnit.TabIndex = 60;
            // 
            // uLabelYear
            // 
            this.uLabelYear.Location = new System.Drawing.Point(8, 204);
            this.uLabelYear.Name = "uLabelYear";
            this.uLabelYear.Size = new System.Drawing.Size(100, 20);
            this.uLabelYear.TabIndex = 59;
            this.uLabelYear.Text = "ultraLabel2";
            // 
            // uButtonQuarter
            // 
            this.uButtonQuarter.Location = new System.Drawing.Point(336, 168);
            this.uButtonQuarter.Name = "uButtonQuarter";
            this.uButtonQuarter.Size = new System.Drawing.Size(88, 24);
            this.uButtonQuarter.TabIndex = 57;
            this.uButtonQuarter.Text = "ultraButton3";
            this.uButtonQuarter.Click += new System.EventHandler(this.uButtonQuarter_Click);
            // 
            // uDateQuarter
            // 
            this.uDateQuarter.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateQuarter.Location = new System.Drawing.Point(112, 168);
            this.uDateQuarter.MaskInput = "{LOC} yyyy-mm";
            this.uDateQuarter.Name = "uDateQuarter";
            this.uDateQuarter.Size = new System.Drawing.Size(100, 21);
            this.uDateQuarter.TabIndex = 56;
            // 
            // uDateQuarterUnit
            // 
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateQuarterUnit.ButtonsRight.Add(spinEditorButton2);
            this.uDateQuarterUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateQuarterUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateQuarterUnit.Location = new System.Drawing.Point(228, 168);
            this.uDateQuarterUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateQuarterUnit.Name = "uDateQuarterUnit";
            this.uDateQuarterUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateQuarterUnit.TabIndex = 55;
            // 
            // uLabelQuarter
            // 
            this.uLabelQuarter.Location = new System.Drawing.Point(8, 168);
            this.uLabelQuarter.Name = "uLabelQuarter";
            this.uLabelQuarter.Size = new System.Drawing.Size(100, 20);
            this.uLabelQuarter.TabIndex = 54;
            this.uLabelQuarter.Text = "ultraLabel3";
            // 
            // uButtonMonth
            // 
            this.uButtonMonth.Location = new System.Drawing.Point(336, 132);
            this.uButtonMonth.Name = "uButtonMonth";
            this.uButtonMonth.Size = new System.Drawing.Size(88, 24);
            this.uButtonMonth.TabIndex = 52;
            this.uButtonMonth.Text = "ultraButton2";
            this.uButtonMonth.Click += new System.EventHandler(this.uButtonMonth_Click);
            // 
            // uDateMonth
            // 
            this.uDateMonth.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateMonth.Location = new System.Drawing.Point(112, 132);
            this.uDateMonth.MaskInput = "{LOC} yyyy-mm";
            this.uDateMonth.Name = "uDateMonth";
            this.uDateMonth.Size = new System.Drawing.Size(100, 21);
            this.uDateMonth.TabIndex = 51;
            // 
            // uDateMonthUnit
            // 
            spinEditorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateMonthUnit.ButtonsRight.Add(spinEditorButton3);
            this.uDateMonthUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateMonthUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateMonthUnit.Location = new System.Drawing.Point(228, 132);
            this.uDateMonthUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateMonthUnit.Name = "uDateMonthUnit";
            this.uDateMonthUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateMonthUnit.TabIndex = 50;
            // 
            // uLabelMonth
            // 
            this.uLabelMonth.Location = new System.Drawing.Point(8, 132);
            this.uLabelMonth.Name = "uLabelMonth";
            this.uLabelMonth.Size = new System.Drawing.Size(100, 20);
            this.uLabelMonth.TabIndex = 49;
            this.uLabelMonth.Text = "ultraLabel2";
            // 
            // uButtonWeek
            // 
            this.uButtonWeek.Location = new System.Drawing.Point(336, 96);
            this.uButtonWeek.Name = "uButtonWeek";
            this.uButtonWeek.Size = new System.Drawing.Size(88, 24);
            this.uButtonWeek.TabIndex = 47;
            this.uButtonWeek.Text = "ultraButton1";
            this.uButtonWeek.Click += new System.EventHandler(this.uButtonWeek_Click);
            // 
            // uDateWeek
            // 
            this.uDateWeek.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWeek.Location = new System.Drawing.Point(112, 96);
            this.uDateWeek.MaskInput = "{date}";
            this.uDateWeek.Name = "uDateWeek";
            this.uDateWeek.Size = new System.Drawing.Size(100, 21);
            this.uDateWeek.TabIndex = 46;
            // 
            // uDateWeekUnit
            // 
            spinEditorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateWeekUnit.ButtonsRight.Add(spinEditorButton4);
            this.uDateWeekUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWeekUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateWeekUnit.Location = new System.Drawing.Point(228, 96);
            this.uDateWeekUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateWeekUnit.Name = "uDateWeekUnit";
            this.uDateWeekUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateWeekUnit.TabIndex = 45;
            // 
            // uLabelWeek
            // 
            this.uLabelWeek.Location = new System.Drawing.Point(8, 96);
            this.uLabelWeek.Name = "uLabelWeek";
            this.uLabelWeek.Size = new System.Drawing.Size(100, 20);
            this.uLabelWeek.TabIndex = 44;
            this.uLabelWeek.Text = "ultraLabel2";
            // 
            // uButtonDay
            // 
            this.uButtonDay.Location = new System.Drawing.Point(336, 60);
            this.uButtonDay.Name = "uButtonDay";
            this.uButtonDay.Size = new System.Drawing.Size(88, 24);
            this.uButtonDay.TabIndex = 43;
            this.uButtonDay.Text = "ultraButton1";
            this.uButtonDay.Click += new System.EventHandler(this.uButtonDay_Click);
            // 
            // uDateDay
            // 
            this.uDateDay.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateDay.Location = new System.Drawing.Point(112, 60);
            this.uDateDay.MaskInput = "{date}";
            this.uDateDay.Name = "uDateDay";
            this.uDateDay.Size = new System.Drawing.Size(100, 21);
            this.uDateDay.TabIndex = 27;
            // 
            // uDateDayUnit
            // 
            spinEditorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateDayUnit.ButtonsRight.Add(spinEditorButton5);
            this.uDateDayUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateDayUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateDayUnit.Location = new System.Drawing.Point(228, 60);
            this.uDateDayUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateDayUnit.Name = "uDateDayUnit";
            this.uDateDayUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateDayUnit.TabIndex = 26;
            // 
            // uLabelDay
            // 
            this.uLabelDay.Location = new System.Drawing.Point(8, 60);
            this.uLabelDay.Name = "uLabelDay";
            this.uLabelDay.Size = new System.Drawing.Size(100, 20);
            this.uLabelDay.TabIndex = 21;
            this.uLabelDay.Text = "ultraLabel2";
            // 
            // frmJOB0002
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(449, 247);
            this.Controls.Add(this.ultraGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmJOB0002";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BatchJob 강제실행";
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateYearUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateQuarter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateQuarterUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMonthUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWeekUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDayUnit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraLabel uLabelDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDayUnit;
        private Infragistics.Win.Misc.UltraButton uButtonDay;
        private Infragistics.Win.Misc.UltraButton uButtonQuarter;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateQuarter;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateQuarterUnit;
        private Infragistics.Win.Misc.UltraLabel uLabelQuarter;
        private Infragistics.Win.Misc.UltraButton uButtonMonth;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateMonth;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateMonthUnit;
        private Infragistics.Win.Misc.UltraLabel uLabelMonth;
        private Infragistics.Win.Misc.UltraButton uButtonWeek;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWeek;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWeekUnit;
        private Infragistics.Win.Misc.UltraLabel uLabelWeek;
        private Infragistics.Win.Misc.UltraLabel uLabelUnitTime;
        private Infragistics.Win.Misc.UltraButton uButtonYear;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateYear;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateYearUnit;
        private Infragistics.Win.Misc.UltraLabel uLabelYear;
    }
}