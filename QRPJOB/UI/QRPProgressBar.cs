﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
namespace QRPJOB.UI
{
    public class QRPProgressBar
    {
        private frmProgress frmProgressPopup; //= new frmProgress();   //팝업창 Instance
        private System.Threading.Thread threadPopup = null;         //팝업창 처리 Thread
        private Form m_frmForm;                                     //팝업창 호출 Form
        private string m_strFunctionName;                           //팝업창 처리기능명

        private delegate void CloseCallback();

        /// <summary>
        /// 팝업창을 처리하기위한 Thread 생성
        /// </summary>
        /// <returns></returns>
        public Thread mfStartThread()
        {
            threadPopup = new Thread(new ThreadStart(OpenPopup));
            return threadPopup;
        }

        /// <summary>
        /// 진행 팝업창 열기
        /// </summary>
        /// <param name="frmForm"></param>
        /// <param name="strFunctioName"></param>
        public void mfOpenProgressPopup(Form frmForm, string strFunctioName)
        {
            try
            {
                m_frmForm = frmForm;
                m_strFunctionName = strFunctioName;
                m_frmForm.Cursor = Cursors.WaitCursor;
                threadPopup.Start();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        public void mfStartThread(Form frmForm, string strFunctioName)
        {
            try
            {
                threadPopup = new Thread(new ThreadStart(OpenPopup));
                m_frmForm = frmForm;
                m_strFunctionName = strFunctioName;
                m_frmForm.Cursor = Cursors.WaitCursor;
                threadPopup.Start();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// 진행 팝업창 열기
        /// </summary>
        private void OpenPopup()
        {

            frmProgressPopup = new frmProgress();
            frmProgressPopup.ProgressName = m_strFunctionName;
            frmProgressPopup.StartPosition = FormStartPosition.CenterScreen;
            frmProgressPopup.ShowDialog();
        }

        /// <summary>
        /// Thread 종료 및 진행 팝업창 닫기
        /// </summary>
        public void mfCloseProgressPopup(Form MdiForm)
        {
            try
            {
                frmProgressPopup.Invoke(new CloseCallback(frmProgressPopup.Close));
                //frmProgressPopup.Dispose();
                threadPopup.Join();
                m_frmForm.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }
    }
}
