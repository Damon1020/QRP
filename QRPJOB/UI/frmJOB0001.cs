﻿/*----------------------------------------------------------------------*/
/* 시스템명     : QRPJOB.UI                                              */
/* 모듈(분류)명 : QRP Server Program                                          */
/* 프로그램ID   : QRPJOB.UI                                         */
/* 프로그램명   : QRPJOB.UI                                          */
/* 작성자       : 이종민                                             */
/* 작성일자     : 2011-10-14                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/* 쓰레드 충돌 문제 때문에 프로그래스바 주석 처리!!
 * 
/*----------------------------------------------------------------------*/

using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Net;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.ServiceProcess;
using System.Threading;
using System.Xml;
using System.Configuration;
using System.Resources;
using System.IO;

namespace QRPJOB.UI
{
    public partial class frmQRPJOB0001 : Form
    {
        #region VAr
        private ServiceController svBatchjob;   // 품질 BatchJob ServiceController
        private ServiceController svTibrv;      // tibrv ServiceController
        private System.Windows.Threading.DispatcherTimer dt_timer;      //품질 Batchjob Timer
        private System.Windows.Threading.DispatcherTimer dt_tibtimer;   //tibrv Timer (Sub-Server 용)
        private XmlDocument _xml;
        private string strfilename;        
        private IPHostEntry ip;
        private Configuration con;
        QRPGlobal SysRes = new QRPGlobal();
        #endregion

        public frmQRPJOB0001()
        {
            InitializeComponent();

            InitTab();
            InitGroupBox();
            InitLabel();
            InitButton();
            InitGrid();

            ip = Dns.GetHostEntry(Dns.GetHostName());

            #region Server Type
            GetSystemResource();
            #endregion
        }

        #region Initialize
        private void InitLabel()
        {
            try
            {
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelDaily, "일별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelWeekly, "주별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelMonthly, "월별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelQuarter, "분기별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelAnnual, "연별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelTick, "시간지정", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelUnit, "생산기준시간", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelWarning, " * 동작중인 QRP BatchJob 서비스가 존재하지 않습니다", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelWait, " * QRP BatchJob 서비스를 검색중입니다 잠시만 기다려주세요", "굴림", true, true);


                wLabel.mfSetLabel(this.uLabelPath, "경로", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelDataSource, "DataSource", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelCatalog, "Initial Catalog", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelUserID, "user id", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelPassword, "password", "굴림", true, true);

                wLabel.mfSetLabel(this.uLabelService, "Service", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelNetwork, "Network", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelDaemon, "Daemon", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelSourceSubject, "SourceSubject", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelTargetSubject, "TargetSubject", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelIISMain, "MainServer", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelIISSub, "SUbServer", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabeltibWait, " * QRP BatchJob 서비스를 검색중입니다 잠시만 기다려주세요", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelTibWarning, " * 동작중인 QRP TIBrv 서비스가 존재하지 않습니다", "굴림", true, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n"
                    + ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonInit, "초기화", "굴림", Properties.Resources.btn_Refresh);
                wButton.mfSetButton(this.uButtonSave, "저장", "굴림", Properties.Resources.btn_Save);
                wButton.mfSetButton(this.uButtonStart, "시작", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.uButtonStop, "중지", "굴림", Properties.Resources.lbl_title);
                wButton.mfSetButton(this.uButtonExit, "종료", "굴림", Properties.Resources.btn_Stop);
                wButton.mfSetButton(this.uButtonDoNow, "BatchJob 강제실행", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.ultraButton5, "BatchJob 설치", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.ultraButton6, "BatchJob 제거", "굴림", Properties.Resources.btn_Stop);

                wButton.mfSetButton(this.uButtonMDMExit, "종료", "굴림", Properties.Resources.btn_Stop);
                wButton.mfSetButton(this.uButtonMDMSave, "저장", "굴림", Properties.Resources.btn_Save);
                wButton.mfSetButton(this.uButtonDelRow, "행삭제", "굴림", Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonRead, "조회", "굴림", Properties.Resources.btn_Search);
                
                wButton.mfSetButton(this.uButtonWebConfig, "Web.config", "굴림", Properties.Resources.btn_Search);
                wButton.mfSetButton(this.uButtonConfigSave, "저장", "굴림", Properties.Resources.btn_Save);
                wButton.mfSetButton(this.uButtonConfigGridSave, "저장", "굴림", Properties.Resources.btn_Save);
                wButton.mfSetButton(this.uButtonConfigGridInit, "초기화", "굴림", Properties.Resources.btn_Refresh);
                wButton.mfSetButton(this.uButtonConfigInit, "초기화", "굴림", Properties.Resources.btn_Refresh);
                wButton.mfSetButton(this.uButtonconfigGridDelete, "행삭제", "굴림", Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonConfigExit, "종료", "굴림", Properties.Resources.btn_Stop);

                wButton.mfSetButton(this.uButtontibStart, "TIBrv 시작", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.uButtontibStop, "TIBrv 중지", "굴림", Properties.Resources.lbl_title);
                wButton.mfSetButton(this.uButtontibUninstall, "TIBrv 제거", "굴림", Properties.Resources.btn_Stop);
                wButton.mfSetButton(this.uButtontibInstall, "TIBrv 설치", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.uButtontibInit, "초기화", "굴림", Properties.Resources.btn_Refresh);
                wButton.mfSetButton(this.uButtontibSave, "저장", "굴림", Properties.Resources.btn_Save);
                wButton.mfSetButton(this.uButtonTibExit, "종료", "굴림", Properties.Resources.btn_Stop);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void InitGroupBox()
        {
            try
            {
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupBoxBatchJob, GroupBoxType.INFO, "BatchJob 설정", "굴림"
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupBoxBatchJob.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxBatchJob.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupMDM, GroupBoxType.INFO, "MDM I/F 설정", "굴림"
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupMDM.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupMDM.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupConfig, GroupBoxType.INFO, "Web.config 설정", "굴림"
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupConfig.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupConfig.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;


                wGroupBox.mfSetGroupBox(this.ultraGroupBox7, GroupBoxType.INFO, "Server Type 설정", "굴림"
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.ultraGroupBox7.HeaderAppearance.FontData.SizeInPoints = 9;
                this.ultraGroupBox7.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.ultraGroupBox1, GroupBoxType.INFO, "QRP-MES Tibrv 설정", "굴림"
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.ultraGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                this.ultraGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        private void InitTab()
        {
            QRPCOM.QRPUI.WinTabControl tab = new WinTabControl();
            tab.mfInitGeneralTabControl(this.uTabJob,
                                        Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage,
                                        Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never,
                                        Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None,
                                        "굴림");
        }

        private void InitGrid()
        {
            WinGrid wGrid = new WinGrid();

            // 일반설정
            wGrid.mfInitGeneralGrid(this.uGridMDM, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true, Infragistics.Win.DefaultableBoolean.False
                , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.True
                , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                , 0, false, "굴림");

            // Set Column
            wGrid.mfSetGridColumn(this.uGridMDM, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 10
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

            wGrid.mfSetGridColumn(this.uGridMDM, 0, "InterfaceID", "Interface ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            wGrid.mfSetGridColumn(this.uGridMDM, 0, "UseFlag", "사용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, false, false, 10
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

            wGrid.mfSetGridColumn(this.uGridMDM, 0, "InterfaceName", "Interface Name", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 200
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            wGrid.mfSetGridColumn(this.uGridMDM, 0, "SPName", "Stored Procedure Name", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            wGrid.mfSetGridColumn(this.uGridMDM, 0, "IF_VIEW", "Interface Veiw Name", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            wGrid.mfSetGridColumn(this.uGridMDM, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            wGrid.mfSetGridColumn(this.uGridMDM, 0, "name", "spname", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 100
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            // 빈행추가
            wGrid.mfAddRowGrid(this.uGridMDM, 0);
            this.uGridMDM.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
            this.uGridMDM.DisplayLayout.Bands[0].Columns["UseFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

            this.uGridMDM.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;

            // 일반설정
            wGrid.mfInitGeneralGrid(this.uGridcon, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide
                , Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true, Infragistics.Win.DefaultableBoolean.False
                , Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                , Infragistics.Win.UltraWinGrid.SelectType.Default, Infragistics.Win.DefaultableBoolean.True
                , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                , 0, false, "굴림");

            // Set Column
            wGrid.mfSetGridColumn(this.uGridcon, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 10
                , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

            wGrid.mfSetGridColumn(this.uGridcon, 0, "objectUri", "objectUri", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 180, false, false, 100
                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            wGrid.mfSetGridColumn(this.uGridcon, 0, "type", "type", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 250, false, false, 100
                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            wGrid.mfSetGridColumn(this.uGridcon, 0, "class", "class", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 180, false, false, 100
                , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

            // 빈행추가
            wGrid.mfAddRowGrid(this.uGridcon, 0);
            this.uGridcon.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

            this.uGridcon.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
        }
        #endregion

        #region UI Event
        private void frmQRPJOB0001_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show("프로그램을 종료하시겠습니까?", "QRP BatchJob 관리 종료", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (result == DialogResult.No)
            {
                e.Cancel = true;
            }
            else if (result == DialogResult.Cancel)
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
            }
            QRPTray.Dispose();
        }
        private void frmQRPJOB0001_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.Hide();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }
        #endregion

        #region Tab 품질 BatchJob
        
        #region SYSQRPJobMaster 설정값 Read
        /// <summary>
        /// SYSQRPJobMaster 설정값 Read
        /// </summary>
        private void OnStart()
        {
            try
            {
                //QRPCOM.QRPGLO.QRPBrowser ////brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.BatchJob), "BatchJob");
                QRPJOB.BL.BATJOB.BatchJob clsBatchJob = new QRPJOB.BL.BATJOB.BatchJob();
                //brwChannel.mfCredentials(clsBatchJob);

                DataTable dtMaster = clsBatchJob.mfReadJOBMaster();

                for (int i = 0; i < dtMaster.Rows.Count; i++)
                {
                    switch (dtMaster.Rows[i]["TimerCode"].ToString())
                    {
                        case "DAY":
                            uDateDay.Value = dtMaster.Rows[i]["TickTime"].ToString();
                            uDateDayUnit.Value = dtMaster.Rows[i]["UnitTime"].ToString();
                            if (dtMaster.Rows[i]["TimerGubun"].ToString() == "T")
                            {
                                rdDayTick.Checked = true;
                            }
                            else
                            {
                                rdDayUnit.Checked = true;
                            }
                            uCheckDay.Checked = dtMaster.Rows[0]["UseFlag"].ToString() == "T" ? true : false;
                            break;
                        case "WEEK":
                            uDateWeek.Value = dtMaster.Rows[i]["TickTime"].ToString();
                            uDateWeekUnit.Value = dtMaster.Rows[i]["UnitTime"].ToString();
                            if (dtMaster.Rows[i]["TimerGubun"].ToString() == "T")
                            {
                                rdWeekTick.Checked = true;
                            }
                            else
                            {
                                rdWeekUnit.Checked = true;
                            }
                            uCheckWeek.Checked = dtMaster.Rows[0]["UseFlag"].ToString() == "T" ? true : false;
                            break;
                        case "MONTH":
                            uDateMonth.Value = dtMaster.Rows[i]["TickTime"].ToString();
                            uDateMonthUnit.Value = dtMaster.Rows[i]["UnitTime"].ToString();
                            if (dtMaster.Rows[i]["TimerGubun"].ToString() == "T")
                            {
                                rdMonthTick.Checked = true;
                            }
                            else
                            {
                                rdMonthUnit.Checked = true;
                            }
                            uCheckMonth.Checked = dtMaster.Rows[0]["UseFlag"].ToString() == "T" ? true : false;
                            break;
                        case "QUARTER":
                            uDateQuarter.Value = dtMaster.Rows[i]["TickTime"].ToString();
                            uDateQuarterUnit.Value = dtMaster.Rows[i]["UnitTime"].ToString();
                            if (dtMaster.Rows[i]["TimerGubun"].ToString() == "T")
                            {
                                rdQuarterTick.Checked = true;
                            }
                            else
                            {
                                rdQuarterUnit.Checked = true;
                            }
                            uCheckQuarter.Checked = dtMaster.Rows[0]["UseFlag"].ToString() == "T" ? true : false;
                            break;
                        case "YEAR":
                            uDateYear.Value = dtMaster.Rows[i]["TickTime"].ToString();
                            uDateYearUnit.Value = dtMaster.Rows[i]["UnitTime"].ToString();
                            if (dtMaster.Rows[i]["TimerGubun"].ToString() == "T")
                            {
                                rdYearTick.Checked = true;
                            }
                            else
                            {
                                rdYearUnit.Checked = true;
                            }
                            uCheckYear.Checked = dtMaster.Rows[0]["UseFlag"].ToString() == "T" ? true : false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }
        #endregion

        #region QRP BatchJob Service Status Read
        private void mfCheckQRPJOBState()
        {
            try
            {
                Boolean blCheck = false;
                //svBatchjob = new ServiceController("QRP BatchJob");
                // 윈도우 서비스 목록
                ServiceController[] svc = ServiceController.GetServices();
                for (int i = 0; i < svc.Length; i++)
                {
                    if (svc[i].ServiceName == "QRP BatchJob")
                    {
                        blCheck = true;
                        svBatchjob = svc[i];
                    }
                }

                // 서비스 설치/실행 여부에 따라 컨트롤 설정
                if (blCheck)
                {
                    if (svBatchjob.Status == ServiceControllerStatus.Running ||
                        svBatchjob.Status == ServiceControllerStatus.StartPending)
                    {
                        uButtonStart.Enabled = false;
                        uButtonStop.Enabled = true;
                        tsStart.Enabled = false;
                        tsStop.Enabled = true;
                    }
                    else if (svBatchjob.Status == ServiceControllerStatus.Stopped ||
                        svBatchjob.Status == ServiceControllerStatus.StopPending)
                    {
                        uButtonStart.Enabled = true;
                        uButtonStop.Enabled = false;
                        tsStart.Enabled = true;
                        tsStop.Enabled = false;
                    }

                    uButtonSave.Enabled = true;
                    uButtonInit.Enabled = true;
                    uLabelWarning.Visible = false;

                    ultraButton5.Enabled = false;
                    ultraButton5.Visible = false;

                    ultraButton6.Enabled = true;
                    ultraButton6.Visible = true;
                }
                else
                {
                    uButtonStart.Enabled = false;
                    uButtonStop.Enabled = false;
                    uButtonSave.Enabled = false;
                    uButtonInit.Enabled = false;
                    uLabelWarning.Visible = true;
                    tsStart.Enabled = false;
                    tsStop.Enabled = false;

                    ultraButton5.Enabled = true;
                    ultraButton5.Visible = true;
                    ultraButton6.Enabled = false;
                    ultraButton6.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());

                uButtonStart.Enabled = false;
                uButtonStop.Enabled = false;
                uButtonSave.Enabled = false;
                uButtonInit.Enabled = false;
                tsStart.Enabled = false;
                tsStop.Enabled = false;   
            }
        }
        private void mfCheckQRPJOBState(object sender, EventArgs e)
        {
            mfCheckQRPJOBState();
        }
        #endregion

        #region Button Event
        private void uButtonExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        private void uButtonSave_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();

            try
            {
                #region Insdispensable
                if (!rdDayTick.Checked && !rdDayUnit.Checked)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "사용할 시간을 선택하세요", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (!rdWeekTick.Checked && !rdWeekUnit.Checked)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "사용할 시간을 선택하세요", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (!rdMonthTick.Checked && !rdMonthUnit.Checked)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "사용할 시간을 선택하세요", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (!rdQuarterTick.Checked && !rdQuarterUnit.Checked)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "사용할 시간을 선택하세요", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (!rdYearTick.Checked && !rdYearUnit.Checked)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", "사용할 시간을 선택하세요", Infragistics.Win.HAlign.Right);
                    return;
                }
                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "확인창", "저장확인", "입력한 정보를 저장하시겠습니까?",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                //컨트롤을 조정하는 쓰레드 때문에 해당 구간 lock
                lock (this)
                {
                    //if (svBatchjob.CanStop)
                    //{
                    //    svBatchjob.Stop();
                    //    svBatchjob.WaitForStatus(ServiceControllerStatus.Stopped);
                    //}
                    // 서비스 중지
                    uButtonStop.PerformClick();
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.Cursor = Cursors.WaitCursor;
                    ////QRPCOM.QRPGLO.QRPBrowser ////brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                    ////brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.BatchJob), "BatchJob");
                    QRPJOB.BL.BATJOB.BatchJob clsBatch = new QRPJOB.BL.BATJOB.BatchJob();
                    ////brwChannel.mfCredentials(clsBatch);

                    DataTable dtValue = clsBatch.mfDataSetInfo();

                    #region Day
                    DataRow drValue = dtValue.NewRow();

                    drValue["TimerCode"] = "DAY";

                    if (rdDayTick.Checked)
                        drValue["TimerGubun"] = "T";
                    else
                        drValue["TimerGubun"] = "U";

                    drValue["TickTime"] = Convert.ToDateTime(uDateDay.Value).ToString("HH:mm:ss");
                    drValue["UnitTime"] = Convert.ToDateTime(uDateDayUnit.Value).ToString("HH:mm:ss");
                    drValue["UseFlag"] = uCheckDay.Checked == true ? "T" : "F";

                    dtValue.Rows.Add(drValue);
                    #endregion

                    #region Week
                    drValue = dtValue.NewRow();

                    drValue["TimerCode"] = "WEEK";

                    if (rdWeekTick.Checked)
                        drValue["TimerGubun"] = "T";
                    else
                        drValue["TimerGubun"] = "U";

                    drValue["TickTime"] = Convert.ToDateTime(uDateWeek.Value).ToString("HH:mm:ss");
                    drValue["UnitTime"] = Convert.ToDateTime(uDateWeekUnit.Value).ToString("HH:mm:ss");
                    drValue["UseFlag"] = uCheckWeek.Checked == true ? "T" : "F";

                    dtValue.Rows.Add(drValue);
                    #endregion

                    #region Month
                    drValue = dtValue.NewRow();

                    drValue["TimerCode"] = "MONTH";

                    if (rdMonthTick.Checked)
                        drValue["TimerGubun"] = "T";
                    else
                        drValue["TimerGubun"] = "U";

                    drValue["TickTime"] = Convert.ToDateTime(uDateMonth.Value).ToString("HH:mm:ss");
                    drValue["UnitTime"] = Convert.ToDateTime(uDateMonthUnit.Value).ToString("HH:mm:ss");
                    drValue["UseFlag"] = uCheckMonth.Checked == true ? "T" : "F";

                    dtValue.Rows.Add(drValue);
                    #endregion

                    #region Quarter
                    drValue = dtValue.NewRow();

                    drValue["TimerCode"] = "QUARTER";

                    if (rdQuarterTick.Checked)
                        drValue["TimerGubun"] = "T";
                    else
                        drValue["TimerGubun"] = "U";

                    drValue["TickTime"] = Convert.ToDateTime(uDateQuarter.Value).ToString("HH:mm:ss");
                    drValue["UnitTime"] = Convert.ToDateTime(uDateQuarterUnit.Value).ToString("HH:mm:ss");
                    drValue["UseFlag"] = uCheckQuarter.Checked == true ? "T" : "F";

                    dtValue.Rows.Add(drValue);
                    #endregion

                    #region Year
                    drValue = dtValue.NewRow();

                    drValue["TimerCode"] = "YEAR";

                    if (rdYearTick.Checked)
                        drValue["TimerGubun"] = "T";
                    else
                        drValue["TimerGubun"] = "U";

                    drValue["TickTime"] = Convert.ToDateTime(uDateYear.Value).ToString("HH:mm:ss");
                    drValue["UnitTime"] = Convert.ToDateTime(uDateYearUnit.Value).ToString("HH:mm:ss");
                    drValue["UseFlag"] = uCheckYear.Checked == true ? "T" : "F";

                    dtValue.Rows.Add(drValue);
                    #endregion

                    string strRtn = clsBatch.mfSaveJOBMaster(dtValue, ip.AddressList[0].ToString(), "QRPServer");

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    // 저장 끝

                    m_ProgressPopup.mfCloseProgressPopup(this);
                    this.Cursor = Cursors.Default;

                    System.Windows.Forms.DialogResult result;
                    
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        //OnStart();
                        //svBatchjob.Start();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, "굴림", 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                        //OnStart();
                    }
                    // 서비스 시작
                    //uButtonStart.PerformClick();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        private void uButtonStart_Click(object sender, EventArgs e)
        {
            ServiceStart();
        }

        private void uButtonStop_Click(object sender, EventArgs e)
        {
            ServiceStop();
        }

        private void uButtonInit_Click(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// QRPJob 윈도우 서비스 설치
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ultraButton5_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            System.Windows.Forms.DialogResult result;
            
            if (Directory.Exists(Application.StartupPath + "\\Services\\Job"))
            {
                //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //Thread t1 = m_ProgressPopup.mfStartThread();
                //m_ProgressPopup.mfOpenProgressPopup(this, "서비스 설치중...");
                this.Cursor = Cursors.WaitCursor;
                FileInfo file = new FileInfo(Directory.GetFiles(Application.StartupPath + "\\Services\\Job", "QRPJOB.Service.exe")[0].ToString());

                System.Diagnostics.Process P = new System.Diagnostics.Process();
                P.StartInfo.FileName = file.FullName.ToString();
                P.StartInfo.Arguments = "/i";

                P.StartInfo.UseShellExecute = false;
                P.StartInfo.RedirectStandardOutput = true;

                Boolean bl = P.Start();
                System.Threading.Thread.Sleep(5000);
                if (bl)
                {
                    //m_ProgressPopup.mfCloseProgressPopup(this);
                    //t1.Join();
                    mfCheckQRPJOBState();
                    this.Cursor = Cursors.Default;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 설치했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //m_ProgressPopup.mfCloseProgressPopup(this);
                    //t1.Join();
                    mfCheckQRPJOBState();
                    this.Cursor = Cursors.Default;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스 설치에 실패했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
            }
        }

        private void ultraButton6_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            System.Windows.Forms.DialogResult result;
            if (Directory.Exists(Application.StartupPath + "\\Services\\Job"))
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "서비스 제거중...");
                this.Cursor = Cursors.WaitCursor;

                FileInfo file = new FileInfo(Directory.GetFiles(Application.StartupPath + "\\Services\\Job", "QRPJOB.Service.exe")[0].ToString());

                System.Diagnostics.Process P = new System.Diagnostics.Process();
                P.StartInfo.FileName = file.FullName.ToString();
                P.StartInfo.Arguments = "/u";

                P.StartInfo.UseShellExecute = false;
                P.StartInfo.RedirectStandardOutput = true;

                Boolean bl = P.Start();

                if (bl)
                {
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    this.Cursor = Cursors.Default;
                    mfCheckQRPJOBState();
                    this.Cursor = Cursors.Default;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 제거했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
                else
                {

                    m_ProgressPopup.mfCloseProgressPopup(this);
                    this.Cursor = Cursors.Default;

                    mfCheckQRPJOBState();
                    this.Cursor = Cursors.Default;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스 제거에 실패했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
            }
        }

        private void uButtonDoNow_Click(object sender, EventArgs e)
        {
            frmJOB0002 frm = new frmJOB0002();
            frm.ShowDialog();
        }
        #endregion

        #region Service

        // BatchJob 시작
        private void ServiceStart()
        {
            WinMessageBox msg = new WinMessageBox();

            try
            {
                // 상태 확인 타이머 정지
                dt_timer.Stop();

                // 서비스가 중지됐거나 중지 중인 경우 시작
                if (svBatchjob.Status.Equals(ServiceControllerStatus.Stopped) ||
                    svBatchjob.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "서비스 시작중...");
                    this.Cursor = Cursors.WaitCursor;
                    
                    this.Cursor = Cursors.WaitCursor;
                    string[] args = new string[1];
                    args[0] = "/" + mGlobalVal.ServerPath;

                    svBatchjob.Start(args);
                    svBatchjob.WaitForStatus(ServiceControllerStatus.Running);
                    svBatchjob.Refresh();
                    Thread.Sleep(2000);
                    lock (this)
                    {
                        mfCheckQRPJOBState();
                    }

                    m_ProgressPopup.mfCloseProgressPopup(this);
                    this.Cursor = Cursors.Default;

                    System.Windows.Forms.DialogResult result;
                    this.Cursor = Cursors.Default;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 시작했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
            }
            catch (InvalidOperationException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
                dt_timer.Start();
            }
            
        }
        private void ServiceStop()
        {
            WinMessageBox msg = new WinMessageBox();

            try
            {
                // 서비스가 중지 가능한 상태일 경우 중지
                if (svBatchjob.CanStop)
                {
                    dt_timer.Stop();

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "서비스 중지중...");
                    this.Cursor = Cursors.WaitCursor;
                    svBatchjob.Stop();
                    
                    svBatchjob.WaitForStatus(ServiceControllerStatus.Stopped);
                    svBatchjob.Refresh();
                    Thread.Sleep(2000);
                    // 컨트롤 변경 쓰레드 때문에 해당 구간 lock
                    lock (this)
                    {
                        mfCheckQRPJOBState();
                    }

                    m_ProgressPopup.mfCloseProgressPopup(this);
                    this.Cursor = Cursors.Default;

                    System.Windows.Forms.DialogResult result;
                    this.Cursor = Cursors.Default;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 중지했습니다.",
                                                Infragistics.Win.HAlign.Right);

                }
            }
            catch (InvalidOperationException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
                dt_timer.Start();
            }
        }
        #endregion
        
        #endregion

        #region 트레이 이벤트
        /// <summary>
        /// 트레이 아이콘 더블클리 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QRPTray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == MouseButtons.Left)
                {
                    if (FormWindowState.Minimized == this.WindowState)
                    {
                        this.Show();
                        this.WindowState = FormWindowState.Normal;
                    }
                    else
                    {
                        this.WindowState = FormWindowState.Minimized;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// 트레이 메뉴 종료 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// 트레이 메뉴 서비스 시작 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsStart_Click(object sender, EventArgs e)
        {
            ServiceStart();
        }

        /// <summary>
        /// 트레이 메뉴 서비스 중지 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tsStop_Click(object sender, EventArgs e)
        {
            ServiceStop();
        }
        #endregion

        #region Tab MDM IF BatchJob -- 변경 Batchjob 폐기 >> DB에서 SQL agent로 실행

        #region Button Event
        private void uButtonMDMStart_Click(object sender, EventArgs e)
        {
            //ServiceStart(1);
        }

        private void uButtonMDMStop_Click(object sender, EventArgs e)
        {
            //ServiceStop(1);
        }

        private void uButtonMDMSave_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            uTabJob.Focus();
            try
            {
                #region Insdispensable
                for (int i = 0; i < uGridMDM.Rows.Count; i++)
                {
                    if (uGridMDM.Rows[i].Cells["InterfaceID"].Value.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "Interface ID를 입력하세요", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    //if (uGridMDM.Rows[i].Cells["SPName"].Value.Equals(string.Empty))
                    //{
                    //    msg.mfSetMessageBox(MessageBoxType.Error, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                , "확인창", "필수입력사항 확인", "Stored Procedure 명을 입력하세요", Infragistics.Win.HAlign.Right);
                    //    return;
                    //}
                }
                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "확인창", "저장확인", "입력한 정보를 저장하시겠습니까?",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                lock (this)
                {
                    //if (mdBatchJob.CanStop)
                    //{
                    //    mdBatchJob.Stop();
                    //}

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");

                    //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();
                    //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.BatchJob), "BatchJob");
                    QRPJOB.BL.BATJOB.BatchJob clsBatch = new QRPJOB.BL.BATJOB.BatchJob();
                    //brwChannel.mfCredentials(clsBatch);
                    this.Cursor = Cursors.WaitCursor;

                    DataTable dtValue = clsBatch.mfDataSetInfoMDM();

                    for (int i = 0; i < uGridMDM.Rows.Count; i++)
                    {
                        if (uGridMDM.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            DataRow drvalue = dtValue.NewRow();
                            drvalue["InterfaceID"] = uGridMDM.Rows[i].Cells["InterfaceID"].Value;
                            drvalue["UseFlag"] = Convert.ToBoolean(uGridMDM.Rows[i].Cells["UseFlag"].Value) == true ? "T" : "F";
                            drvalue["InterfaceName"] = uGridMDM.Rows[i].Cells["InterfaceName"].Value;
                            drvalue["SPName"] = uGridMDM.Rows[i].Cells["SPName"].Value;
                            drvalue["IF_VIEW"] = uGridMDM.Rows[i].Cells["IF_VIEW"].Value;
                            drvalue["EtcDesc"] = uGridMDM.Rows[i].Cells["EtcDesc"].Value;
                            if (uGridMDM.Rows[i].Hidden)
                            {
                                drvalue["DeleteFlag"] = Convert.ToBoolean(uGridMDM.Rows[i].Cells["Check"].Value) == true ? "T" : "F";
                            }
                            else
                            {
                                drvalue["DeleteFlag"] = "F";
                            }
                            dtValue.Rows.Add(drvalue);
                        }
                    }

                    string strRtn = clsBatch.mfSaveMDMIFMaster(dtValue, ip.AddressList[0].ToString(), "QRPServer");

                    QRPCOM.QRPGLO.TransErrRtn ErrRtn = new QRPCOM.QRPGLO.TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    // 저장 끝

                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    this.Cursor = Cursors.Default;
                    //System.Threading.Thread.Sleep(2000);
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        OnStart(1);
                        //svBatchjob.Start();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, "굴림", 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        private void uButtonMDMExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }
        #endregion

        //private void //mfCheckQRPJOBState_MDM()
        //{
        //    try
        //    {
        //        Boolean blCheck = false;
        //        ServiceController[] svc = ServiceController.GetServices();
        //        for (int i = 0; i < svc.Length; i++)
        //        {
        //            if (svc[i].ServiceName == "QRP BatchJob MDM IF")
        //            {
        //                blCheck = true;
        //                mdBatchJob = svc[i];
        //            }
        //        }

        //        if (blCheck)
        //        {
        //            if (mdBatchJob.Status == ServiceControllerStatus.Running ||
        //                mdBatchJob.Status == ServiceControllerStatus.StartPending)
        //            {
        //                uButtonMDMStart.Enabled = false;
        //                uButtonMDMStop.Enabled = true;
        //                tsMDMStart.Enabled = false;
        //                tsMDMStop.Enabled = true;
        //            }
        //            else if (mdBatchJob.Status == ServiceControllerStatus.Stopped ||
        //                mdBatchJob.Status == ServiceControllerStatus.StopPending)
        //            {
        //                uButtonMDMStart.Enabled = true;
        //                uButtonMDMStop.Enabled = false;
        //                tsMDMStart.Enabled = true;
        //                tsMDMStop.Enabled = false;
        //            }

        //            uButtonMDMSave.Enabled = true;
        //            uLabelMDMWarning.Visible = false;
        //            uLabelMDMWait.Visible = false;
        //        }
        //        else
        //        {
        //            uButtonMDMStart.Enabled = false;
        //            uButtonMDMStop.Enabled = false;
        //            uButtonMDMSave.Enabled = false;
        //            uLabelMDMWarning.Visible = true;
        //            uLabelMDMWait.Visible = false;
        //            tsMDMStart.Enabled = false;
        //            tsMDMStop.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message.ToString() + "\n" +
        //            ex.StackTrace.ToString());

        //        uButtonMDMStart.Enabled = false;
        //        uButtonMDMStop.Enabled = false;
        //        uButtonMDMSave.Enabled = false;
        //        uLabelMDMWarning.Visible = true;
        //        uLabelMDMWait.Visible = false;
        //        tsMDMStart.Enabled = false;
        //        tsMDMStop.Enabled = false;
        //    }
        //    finally
        //    {
        //        uButtonMDMSave.Enabled = true; // Test용 사용시 주석처리
        //    }
        //}
        //private void //mfCheckQRPJOBState_MDM(object senser, EventArgs e)
        //{
        //    //mfCheckQRPJOBState_MDM();
        //}

        private void uGridMDM_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Cell 수정시 RowSelector 이미지 변경
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridMDM, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n"
                    + ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        /// <summary>
        /// MDM IF BatchJob 설정값 읽기
        /// </summary>
        /// <param name="sender"></param>
        private void OnStart(object sender)
        {
            try
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "조회중...");
                this.Cursor = Cursors.WaitCursor;

                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.BatchJob), "BatchJob");
                QRPJOB.BL.BATJOB.BatchJob clsBatchJob = new QRPJOB.BL.BATJOB.BatchJob();
                //brwChannel.mfCredentials(clsBatchJob);

                DataTable dtMaster = clsBatchJob.mfReadMDMIFMaster();

                uGridMDM.DataSource = dtMaster;
                uGridMDM.DataBind();

                for (int i = 0; i < uGridMDM.Rows.Count; i++)
                {
                    if (uGridMDM.Rows[i].Cells["name"].Value.ToString().Equals(string.Empty))
                    {
                        uGridMDM.DisplayLayout.Rows[i].Appearance.BackColor = Color.Gray;
                    }
                }

                m_ProgressPopup.mfCloseProgressPopup(this);
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        //private void ServiceStart(object sender)
        //{
        //    WinMessageBox msg = new WinMessageBox();

        //    try
        //    {
        //        dt_timer2.Stop();

        //        if (mdBatchJob.Status.Equals(ServiceControllerStatus.Stopped) ||
        //            mdBatchJob.Status.Equals(ServiceControllerStatus.StopPending))
        //        {
        //            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
        //            Thread t1 = m_ProgressPopup.mfStartThread();
        //            m_ProgressPopup.mfOpenProgressPopup(this, "서비스 시작중...");

        //            mdBatchJob.Start();
        //            mdBatchJob.WaitForStatus(ServiceControllerStatus.Running);
        //            mdBatchJob.Refresh();

        //            m_ProgressPopup.mfCloseProgressPopup(this);
        //            //m_ProgressPopup.Join();
        //            System.Windows.Forms.DialogResult result;

        //            result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
        //                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                                         "처리결과", "서비스", "서비스를 시작했습니다.",
        //                                        Infragistics.Win.HAlign.Right);
        //        }
        //        lock (this)
        //        {
        //            //mfCheckQRPJOBState_MDM();
        //        }
        //        //dt_timer.Start();
        //    }
        //    catch (InvalidOperationException ex)
        //    {
        //        MessageBox.Show(ex.Message.ToString() + "\n" +
        //            ex.StackTrace.ToString());
        //    }
        //    finally
        //    {
        //        dt_timer2.Start();
        //    }

        //}
        //private void ServiceStop(object sender)
        //{
        //    WinMessageBox msg = new WinMessageBox();

        //    try
        //    {
        //        if (mdBatchJob.CanStop)
        //        {
        //            dt_timer2.Stop();

        //            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
        //            Thread t1 = m_ProgressPopup.mfStartThread();
        //            m_ProgressPopup.mfOpenProgressPopup(this, "서비스 중지중...");

        //            mdBatchJob.Stop();
        //            mdBatchJob.WaitForStatus(ServiceControllerStatus.Stopped);
        //            mdBatchJob.Refresh();

        //            m_ProgressPopup.mfCloseProgressPopup(this);
        //            //m_ProgressPopup.Join();
        //            System.Windows.Forms.DialogResult result;

        //            result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
        //                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                                         "처리결과", "서비스", "서비스를 중지했습니다.",
        //                                        Infragistics.Win.HAlign.Right);
        //            lock (this)
        //            {
        //                //mfCheckQRPJOBState_MDM();
        //            }
        //        }
        //    }
        //    catch (InvalidOperationException ex)
        //    {
        //        MessageBox.Show(ex.Message.ToString() + "\n" +
        //            ex.StackTrace.ToString());
        //    }
        //    finally
        //    {
        //        dt_timer2.Start();
        //    }
        //}

        private void uButtonDelRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < uGridMDM.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridMDM.Rows[i].Cells["Check"].Value))
                    {
                        uGridMDM.Rows[i].Hidden = true;
                    }
                }
            }
            catch
            {
            }
        }
        private void uButtonRead_Click(object sender, EventArgs e)
        {
            OnStart(1);
        }
        #endregion

        #region WebConfig 설정

        // web.config 에 저장되어 있는 DB connectionstring을 가져와서 텍스트박스에 지정
        void ConfigConnectionString()
        {
            if (_xml == null)
                return;

            char[] param = { ';' };

            if (_xml.SelectSingleNode("configuration/appSettings/add") != null)
            {
                if (_xml.SelectSingleNode("configuration/appSettings/add").Attributes[0].Value.Equals("ConnectionString"))
                {
                    string[] conn = _xml.SelectSingleNode("configuration/appSettings/add").Attributes[1].Value.ToString().Split(param);

                    if (conn.Length > 0)
                    {
                        uTextDataSource.Text = conn[0].ToString().Substring(conn[0].IndexOf("=") + 1, conn[0].Length - conn[0].IndexOf("=") - 1);
                        uTextUserID.Text = conn[1].ToString().Substring(conn[1].IndexOf("=") + 1, conn[1].Length - conn[1].IndexOf("=") - 1);

                        //uTextPassword.PasswordChar = '*';
                        uTextPassword.Text = conn[2].ToString().Substring(conn[2].IndexOf("=") + 1, conn[2].Length - conn[2].IndexOf("=") - 1);
                        uTextCatalog.Text = conn[3].ToString().Substring(conn[3].IndexOf("=") + 1, conn[3].Length - conn[3].IndexOf("=") - 1);
                    }
                }
            }
        }

        // web.config 에 저장되어 있는 remoting 설정 값을 읽어 그리드에 지정
        void ConfigGridBinding()
        {
            if (_xml == null)
                return;

            XmlNode _node = _xml.SelectSingleNode("configuration/system.runtime.remoting/application/service");

            DataTable dt = new DataTable();
            dt.Columns.Add("objectUri", typeof(string));
            dt.Columns.Add("type", typeof(string));
            dt.Columns.Add("class", typeof(string));
            char[] s = { ',' };
            foreach (XmlNode child in _node)
            {
                if (!child.Attributes["objectUri"].Value.ToString().Equals(string.Empty))
                {
                    DataRow dr = dt.NewRow();
                    dr["objectUri"] = child.Attributes["objectUri"].Value.ToString().Trim();
                    dr["type"] = child.Attributes["type"].Value.ToString().Split(s)[0].ToString().Trim();
                    dr["class"] = child.Attributes["type"].Value.ToString().Split(s)[1].ToString().Trim();

                    dt.Rows.Add(dr);
                }
            }

            DataTable dtnew = new DataTable();
            DataRow[] drnew = dt.Select("", "class, type");
            dtnew = drnew.CopyToDataTable();

            uGridcon.DataSource = dtnew;
            uGridcon.DataBind();

            for (int i = 0; i < uGridcon.Rows.Count; i++)
            {
                for (int j = 0; j < uGridcon.Rows[i].Cells.Count; j++)
                {
                    uGridcon.Rows[i].Cells[j].Tag = uGridcon.Rows[i].Cells[j].Value.ToString().Trim();
                }
            }

        }

        // web.config 파일 찾기
        // 파일을 지정하면 해당 파일의 위치를 기억하여 다음번 프로그램 실행시에도 가져온다
        private void uButtonWebConfig_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog config = new OpenFileDialog();
            config.Filter = "config File|Web.config";
            config.FileName = "Web.config";
            config.InitialDirectory = "c:\\";
            config.RestoreDirectory = true;
            DialogResult dr = config.ShowDialog();

            if (dr != DialogResult.OK && dr != DialogResult.Yes)
                return;
            
            if (config.CheckFileExists)
            {
                _xml = new XmlDocument();    
                _xml.Load(config.FileName.ToString());
                strfilename = config.FileName.ToString();

                con.AppSettings.Settings["Web.config"].Value = strfilename;
                con.Save();
                uLabelPath.Text = strfilename;

                ConfigConnectionString();
                ConfigGridBinding();
            }
        }

        private void uGridcon_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Cell 수정시 RowSelector 이미지 변경
                if (e.Cell.Row.RowSelectorAppearance.Image == null)
                {
                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                }

                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridMDM, 0, e.Cell.Row.Index))
                {
                    e.Cell.Row.Delete(false);
                }

                if (e.Cell.Tag == null)
                {
                    e.Cell.Tag = e.Cell.Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n"
                    + ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void uButtonConfigSave_Click(object sender, EventArgs e)
        {
            if (_xml == null)
                return;

            WinMessageBox msg = new WinMessageBox();

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "확인창", "저장확인", "ConnectionString을 저장하시겠습니까?",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }

            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            Thread t1 = m_ProgressPopup.mfStartThread();
            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
            this.Cursor = Cursors.WaitCursor;

            XmlNode node = _xml.SelectSingleNode("configuration/appSettings/add");
            
            if(uTextDataSource.Text.ToString().Equals(string.Empty))
                return;

            if(uTextUserID.Text.ToString().Equals(string.Empty))
                return;

            if(uTextPassword.Text.ToString().Equals(string.Empty))
                return;

            if(uTextCatalog.Text.ToString().Equals(string.Empty))
                return;

            string strvalue = "data source=" + uTextDataSource.Text.Trim().ToString() +
                              ";user id=" + uTextUserID.Text.Trim().ToString() +
                              ";password=" + uTextPassword.Text.Trim().ToString() +
                              ";Initial Catalog=" + uTextCatalog.Text.Trim().ToString() +
                              ";persist security info=true";

            _xml.SelectSingleNode("configuration/appSettings/add").Attributes[1].Value = strvalue;
            _xml.Save(strfilename);
            ConfigConnectionString();

            m_ProgressPopup.mfCloseProgressPopup(this);
            this.Cursor = Cursors.Default;
        }

        private void uButtonConfigGridSave_Click(object sender, EventArgs e)
        {
            if (_xml == null)
                return;

            WinMessageBox msg = new WinMessageBox();

            if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "확인창", "저장확인", "Service 목록을 저장하시겠습니까?",
                        Infragistics.Win.HAlign.Right) == DialogResult.No)
            {
                return;
            }

            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            Thread t1 = m_ProgressPopup.mfStartThread();
            m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
            this.Cursor = Cursors.WaitCursor;
            
            for (int i = 0; i < uGridcon.Rows.Count; i++)
            {
                if (uGridcon.Rows[i].RowSelectorAppearance.Image != null)
                {
                    string strobjectUritag, strtypetag;
                    strobjectUritag = uGridcon.Rows[i].Cells["objectUri"].Tag.ToString().Trim();
                    strtypetag = uGridcon.Rows[i].Cells["type"].Tag.ToString().Trim() + ", " + uGridcon.Rows[i].Cells["class"].Tag.ToString().Trim();
                    string att = "wellknown[@objectUri=\"" + strobjectUritag + "\"]";

                    XmlNodeList _node = _xml.SelectSingleNode("configuration/system.runtime.remoting/application/service").SelectNodes(att);
                    XmlNode old, _new;

                    if (_node.Count <= 0)
                    {
                        _new = _xml.CreateElement("wellknown");

                        XmlAttribute mode = _xml.CreateAttribute("mode");
                        XmlAttribute objectUri = _xml.CreateAttribute("objectUri");
                        XmlAttribute type = _xml.CreateAttribute("type");

                        mode.Value = "SingleCall";
                        objectUri.Value = uGridcon.Rows[i].Cells["objectUri"].Value.ToString();
                        string strtype = uGridcon.Rows[i].Cells["type"].Value.ToString().Trim() + ", " + uGridcon.Rows[i].Cells["class"].Value.ToString().Trim();
                        type.Value = strtype;

                        _new.Attributes.Append(mode);
                        _new.Attributes.Append(objectUri);
                        _new.Attributes.Append(type);

                        if(!uGridcon.Rows[i].Hidden)
                            _xml.SelectSingleNode("configuration/system.runtime.remoting/application/service").AppendChild(_new);
                    }
                    else
                    {
                        old = _node[_node.Count - 1];
                        if (!uGridcon.Rows[i].Hidden)
                        {
                            _new = _xml.CreateElement("wellknown");

                            XmlAttribute mode = _xml.CreateAttribute("mode");
                            XmlAttribute objectUri = _xml.CreateAttribute("objectUri");
                            XmlAttribute type = _xml.CreateAttribute("type");

                            mode.Value = "SingleCall";
                            objectUri.Value = uGridcon.Rows[i].Cells["objectUri"].Value.ToString();
                            string strtype = uGridcon.Rows[i].Cells["type"].Value.ToString() + ", " + uGridcon.Rows[i].Cells["class"].Value.ToString();
                            type.Value = strtype;

                            _new.Attributes.Append(mode);
                            _new.Attributes.Append(objectUri);
                            _new.Attributes.Append(type);

                            _xml.SelectSingleNode("configuration/system.runtime.remoting/application/service").ReplaceChild(_new, old);
                        }
                        else
                        {
                            _xml.SelectSingleNode("configuration/system.runtime.remoting/application/service").RemoveChild(old);
                        }
                    }
                }
            }

            _xml.Save(strfilename);
            ConfigGridBinding();

            m_ProgressPopup.mfCloseProgressPopup(this);
            this.Cursor = Cursors.Default;
        }

        private void uButtonConfigCancel_Click(object sender, EventArgs e)
        {
            ConfigConnectionString();
            ConfigGridBinding();
        }

        private void uButtonconfigGridDelete_Click(object sender, EventArgs e)
        {

            int cnt = 0;
            for (int i = 0; i < uGridcon.Rows.Count; i++)
            {
                if (Convert.ToBoolean(uGridcon.Rows[i].Cells["Check"].Value))
                {
                    cnt++;
                    //uGridcon.Rows[i].Hidden = true;
                }
            }
            if (!cnt.Equals(0))
            {
                WinMessageBox msg = new WinMessageBox();

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "확인창", "저장확인", "선택한 Service 목록을 삭제하시겠습니까?",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                for (int i = 0; i < uGridcon.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(uGridcon.Rows[i].Cells["Check"].Value))
                    {
                        uGridcon.Rows[i].Hidden = true;
                    }
                }
            }
        }

        private void uButtonConfigExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        private void uGridcon_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].Tag = string.Empty;
            }
        }

        private void uButtonConfigInit_Click(object sender, EventArgs e)
        {
            ConfigConnectionString();
        }
        #endregion

        #region Server Type

        private static QRPCOM.QRPGLO.QRPGlobal mGlobalVal = new QRPGlobal();
        // 프로그램이 실행되는 서버가 main인지 sub인지 system resource에 저장
        private void SetSystemResource()
        {
            try
            {
                
                DataTable dt = new DataTable();
                dt.Columns.Add("NameKey", typeof(string));
                dt.Columns.Add("NameValue", typeof(string));
                
                DataRow dr;
                //서버경로
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_SERVERPATH";
                dr["NameValue"] = mGlobalVal.ServerPath;
                dt.Rows.Add(dr);

                //DB에서 서버관련 정보를 읽기 추가)
                mGlobalVal.mfMakeSystemInfoResource(dt);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
            }
        }
        void SetSystemResource(string strType)
        {
            mGlobalVal.ServerPath = strType;
            SetSystemResource();
        }
        // system resource에서 실행되는 서버의 타입을 가져온다 ( main/sub ) - 없는 경우 main으로 간주
        void GetSystemResource()
        {
            try
            {
                ResourceSet res = new ResourceSet(SysRes.SystemInfoRes);
                res.Dispose();
            }
            catch
            {
                QRPCOM.QRPGLO.QRPGlobal GlobalVal = new QRPGlobal();
                DataTable dt = new DataTable();
                dt.Columns.Add("NameKey", typeof(string));
                dt.Columns.Add("NameValue", typeof(string));

                DataRow dr;
                //서버경로
                dr = dt.NewRow();
                dr["NameKey"] = "SYS_SERVERPATH";
                dr["NameValue"] = "Main";
                dt.Rows.Add(dr);
                
                //DB에서 서버관련 정보를 읽기 추가)
                GlobalVal.mfMakeSystemInfoResource(dt);
            }

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            if (m_resSys.GetString("SYS_SERVERPATH").ToString().Equals("Main"))
            {
                m_resSys.Dispose();
                radioButton1.Checked = true;
            }
            else if (m_resSys.GetString("SYS_SERVERPATH").ToString().Equals("Sub"))
            {
                m_resSys.Dispose();
                radioButton2.Checked = true;
            }
            else
            {
                m_resSys.Dispose();
                radioButton1.Checked = true;
            }
        }
        
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked && !radioButton2.Checked)
                SetSystemResource("Main");
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (!radioButton1.Checked && radioButton2.Checked)
                SetSystemResource("Sub");
        }
        #endregion

        #region Tab QRPTib Service

        /// <summary>
        /// tibrv 서버 설정을 읽어온다
        /// </summary>
        void mfReadTibState()
        {
            if (Directory.Exists(Application.StartupPath + "\\Services\\Tib"))
            {
                if (Directory.GetFiles(Application.StartupPath + "\\Services\\Tib", "qrptib.xml").Length > 0)
                {
                    XmlDocument tib = new XmlDocument();
                    tib.Load(Application.StartupPath + "\\Services\\Tib\\qrptib.xml");

                    uTextService.Text = tib.SelectSingleNode("tibrv/Service").InnerText;
                    uTextNetwork.Text = tib.SelectSingleNode("tibrv/Network").InnerText;
                    uTextSourceSubJect.Text = tib.SelectSingleNode("tibrv/SourceSubject").InnerText;
                    uTextTargetSubject.Text = tib.SelectSingleNode("tibrv/TargetSubject").InnerText;
                    uTextDaemon.Text = tib.SelectSingleNode("tibrv/Daemon").InnerText;

                    uTextIISMain.Text = tib.SelectSingleNode("tibrv/IIS/Main").InnerText;
                    uTextIISSub.Text = tib.SelectSingleNode("tibrv/IIS/Sub").InnerText;

                    uButtontibSave.Enabled = true;
                    uButtontibInit.Enabled = true;
                }
                else
                {
                    uButtontibSave.Enabled = false;
                    uButtontibInit.Enabled = false;
                }
            }
        }

        /// <summary>
        /// qrptib windows 서비스의 상태를 가져온다
        /// </summary>
        void mfCheckTibState()
        {
            try
            {
                Boolean blCheck = false;
                //svTibrv = new ServiceController("QRP BatchJob");
                ServiceController[] svc = ServiceController.GetServices();
                for (int i = 0; i < svc.Length; i++)
                {
                    if (svc[i].ServiceName == "QRPTIB.SV")
                    {
                        blCheck = true;
                        svTibrv = svc[i];
                    }
                }

                if (blCheck)
                {
                    if (svTibrv.Status == ServiceControllerStatus.Running ||
                        svTibrv.Status == ServiceControllerStatus.StartPending)
                    {
                        uButtontibStart.Enabled = false;
                        uButtontibStop.Enabled = true;
                        tstibStart.Enabled = false;
                        tstibStop.Enabled = true;
                    }
                    else if (svTibrv.Status == ServiceControllerStatus.Stopped ||
                        svTibrv.Status == ServiceControllerStatus.StopPending)
                    {
                        uButtontibStart.Enabled = true;
                        uButtontibStop.Enabled = false;
                        tstibStart.Enabled = true;
                        tstibStop.Enabled = false;
                    }

                    uLabeltibWait.Visible = false;
                    uLabelTibWarning.Visible = false;

                    uButtontibInstall.Enabled = false;
                    uButtontibInstall.Visible = false;

                    uButtontibUninstall.Enabled = true;
                    uButtontibUninstall.Visible = true;
                }
                else
                {
                    uButtontibStart.Enabled = false;
                    uButtontibStop.Enabled = false;
                    uLabelTibWarning.Visible = true;
                    tstibStart.Enabled = false;
                    tstibStop.Enabled = false;

                    uButtontibInstall.Enabled = true;
                    uButtontibInstall.Visible = true;

                    uButtontibUninstall.Enabled = false;
                    uButtontibUninstall.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());

                uButtontibStart.Enabled = false;
                uButtontibStop.Enabled = false;
                tstibStart.Enabled = false;
                tstibStop.Enabled = false;
            }
        }
        void mfCheckTibState(object sender, EventArgs e)
        {
            try
            {
                Boolean blCheck = false;
                //svTibrv = new ServiceController("QRP BatchJob");
                ServiceController[] svc = ServiceController.GetServices();
                for (int i = 0; i < svc.Length; i++)
                {
                    if (svc[i].ServiceName == "QRPTIB.SV")
                    {
                        blCheck = true;
                        svTibrv = svc[i];
                    }
                }

                if (blCheck)
                {
                    if (svTibrv.Status == ServiceControllerStatus.Running ||
                        svTibrv.Status == ServiceControllerStatus.StartPending)
                    {
                        uButtontibStart.Enabled = false;
                        uButtontibStop.Enabled = true;
                        tstibStart.Enabled = false;
                        tstibStop.Enabled = true;
                    }
                    else if (svTibrv.Status == ServiceControllerStatus.Stopped ||
                        svTibrv.Status == ServiceControllerStatus.StopPending)
                    {
                        uButtontibStart.Enabled = true;
                        uButtontibStop.Enabled = false;
                        tstibStart.Enabled = true;
                        tstibStop.Enabled = false;
                    }

                    uLabeltibWait.Visible = false;
                    uLabelTibWarning.Visible = false;

                    uButtontibInstall.Enabled = false;
                    uButtontibInstall.Visible = false;

                    uButtontibUninstall.Enabled = true;
                    uButtontibUninstall.Visible = true;
                }
                else
                {
                    uButtontibStart.Enabled = false;
                    uButtontibStop.Enabled = false;
                    uLabelTibWarning.Visible = true;
                    tstibStart.Enabled = false;
                    tstibStop.Enabled = false;

                    uButtontibInstall.Enabled = true;
                    uButtontibInstall.Visible = true;
                    uButtontibUninstall.Enabled = false;
                    uButtontibUninstall.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());

                uButtontibStart.Enabled = false;
                uButtontibStop.Enabled = false;
                tstibStart.Enabled = false;
                tstibStop.Enabled = false;
            }
        }


        #region ButtonEvent
        /// <summary>
        /// tibrv listening 서비스 시작
        /// </summary>
        private void TibStart()
        {
            WinMessageBox msg = new WinMessageBox();

            try
            {
                dt_tibtimer.Stop();

                if (svTibrv.Status.Equals(ServiceControllerStatus.Stopped) ||
                    svTibrv.Status.Equals(ServiceControllerStatus.StopPending))
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "서비스 시작중...");
                    
                    this.Cursor = Cursors.WaitCursor;
                    string[] args = new string[1];
                    args[0] = "/" + mGlobalVal.ServerPath;

                    svTibrv.Start(args);
                    svTibrv.WaitForStatus(ServiceControllerStatus.Running);
                    svTibrv.Refresh();
                    Thread.Sleep(2000);
                    lock (this)
                    {
                        mfCheckTibState();
                    }

                    m_ProgressPopup.mfCloseProgressPopup(this);

                    System.Windows.Forms.DialogResult result;
                    this.Cursor = Cursors.Default;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 시작했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }

                //dt_timer.Start();
            }
            catch (InvalidOperationException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
                dt_tibtimer.Start();
            }
        }
        /// <summary>
        /// tibrv listening 서비스 중지
        /// </summary>
        private void TibStop()
        {
            WinMessageBox msg = new WinMessageBox();

            try
            {
                if (svTibrv.CanStop)
                {
                    dt_tibtimer.Stop();

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "서비스 중지중...");
                    this.Cursor = Cursors.WaitCursor;
                    svTibrv.Stop();

                    svTibrv.WaitForStatus(ServiceControllerStatus.Stopped);
                    svTibrv.Refresh();
                    Thread.Sleep(2000);
                    lock (this)
                    {
                        mfCheckTibState();
                    }

                    m_ProgressPopup.mfCloseProgressPopup(this);

                    System.Windows.Forms.DialogResult result;
                    this.Cursor = Cursors.Default;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 중지했습니다.",
                                                Infragistics.Win.HAlign.Right);

                }
            }
            catch (InvalidOperationException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
                dt_tibtimer.Start();
            }
        }

        /// <summary>
        /// 프로그램 종료
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonTibExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// tibrv 윈도우 서비스 프로그램 설치
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtontibInstall_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();

            System.Windows.Forms.DialogResult result;
            if (Directory.Exists(Application.StartupPath + "\\Services\\Tib"))
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "서비스 설치중...");
                this.Cursor = Cursors.WaitCursor;

                FileInfo file = new FileInfo(Directory.GetFiles(Application.StartupPath + "\\Services\\Tib", "QRPTIB.SV.exe")[0].ToString());

                System.Diagnostics.Process P = new System.Diagnostics.Process();
                P.StartInfo.FileName = file.FullName.ToString();
                P.StartInfo.Arguments = "/i";

                P.StartInfo.UseShellExecute = false;
                P.StartInfo.RedirectStandardOutput = true;

                Boolean bl = P.Start();
                Thread.Sleep(2000);
                m_ProgressPopup.mfCloseProgressPopup(this);
                mfCheckTibState();
                this.Cursor = Cursors.Default;
                if (bl)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 설치했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스 설치에 실패했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
            }
        }

        /// <summary>
        /// tibrv 윈도우 서비스 프로그램 제거
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtontibUninstall_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            System.Windows.Forms.DialogResult result;

            if (Directory.Exists(Application.StartupPath + "\\Services\\Tib"))
            {
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "서비스 제거중...");
                this.Cursor = Cursors.WaitCursor;
                FileInfo file = new FileInfo(Directory.GetFiles(Application.StartupPath + "\\Services\\Tib", "QRPTIB.SV.exe")[0].ToString());

                System.Diagnostics.Process P = new System.Diagnostics.Process();
                P.StartInfo.FileName = file.FullName.ToString();
                P.StartInfo.Arguments = "/u";

                P.StartInfo.UseShellExecute = false;
                P.StartInfo.RedirectStandardOutput = true;

                Boolean bl = P.Start();
                Thread.Sleep(2000);
                m_ProgressPopup.mfCloseProgressPopup(this);
                mfCheckTibState();
                this.Cursor = Cursors.Default;
                if (bl)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 제거했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "서비스", "서비스를 제거에 실패했습니다.",
                                                Infragistics.Win.HAlign.Right);
                }
            }
        }

        private void uButtontibStart_Click(object sender, EventArgs e)
        {
            TibStart();
        }

        private void uButtontibStop_Click(object sender, EventArgs e)
        {
            TibStop();
        }

        private void uButtontibSave_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Application.StartupPath + "\\Services\\Tib"))
            {
                uButtontibStop.PerformClick();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.Cursor = Cursors.WaitCursor;

                XmlDocument tib = new XmlDocument();
                tib.Load(Application.StartupPath + "\\Services\\Tib\\qrptib.xml");

                tib.SelectSingleNode("tibrv/Service").InnerText = uTextService.Text;
                tib.SelectSingleNode("tibrv/Network").InnerText = uTextNetwork.Text;
                tib.SelectSingleNode("tibrv/SourceSubject").InnerText = uTextSourceSubJect.Text;
                tib.SelectSingleNode("tibrv/TargetSubject").InnerText = uTextTargetSubject.Text;
                tib.SelectSingleNode("tibrv/Daemon").InnerText = uTextDaemon.Text;

                tib.SelectSingleNode("tibrv/IIS/Main").InnerText = uTextIISMain.Text;
                tib.SelectSingleNode("tibrv/IIS/Sub").InnerText = uTextIISSub.Text;
                tib.Save(Application.StartupPath + "\\Services\\Tib\\qrptib.xml");
                mfReadTibState();

                m_ProgressPopup.mfCloseProgressPopup(this);
                this.Cursor = Cursors.Default;

                //uButtontibStart.PerformClick();


            }
        }

        private void uButtontibInit_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Application.StartupPath + "\\Services\\Tib"))
            {
                if (Directory.GetFiles(Application.StartupPath + "\\Services\\Tib", "qrptib.xml").Length > 0)
                {
                    XmlDocument tib = new XmlDocument();
                    tib.Load(Application.StartupPath + "\\Services\\Tib\\qrptib.xml");

                    uTextService.Text = tib.SelectSingleNode("tibrv/Service").InnerText;
                    uTextNetwork.Text = tib.SelectSingleNode("tibrv/Network").InnerText;
                    uTextSourceSubJect.Text = tib.SelectSingleNode("tibrv/SourceSubject").InnerText;
                    uTextTargetSubject.Text = tib.SelectSingleNode("tibrv/TargetSubject").InnerText;
                    uTextDaemon.Text = tib.SelectSingleNode("tibrv/Daemon").InnerText;

                    uTextIISMain.Text = tib.SelectSingleNode("tibrv/IIS/Main").InnerText;
                    uTextIISSub.Text = tib.SelectSingleNode("tibrv/IIS/Sub").InnerText;

                    uButtontibSave.Enabled = true;
                    uButtontibInit.Enabled = true;
                }
                else
                {
                    uButtontibSave.Enabled = false;
                    uButtontibInit.Enabled = false;
                }
            }
        }
        #endregion

        #region trayicon 우클릭메뉴
        
        private void tstibStart_Click(object sender, EventArgs e)
        {
            TibStart();
        }

        private void tstibStop_Click(object sender, EventArgs e)
        {
            TibStop();
        }
        #endregion

        #endregion

        private void uTabJob_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            try
            {

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();


                if (e.Tab.Key.Equals("INS"))
                {
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.Cursor = Cursors.WaitCursor;

                    #region Tab 품질 BatchJob

                    OnStart();

                    mfCheckQRPJOBState();

                    dt_timer = new System.Windows.Threading.DispatcherTimer();
                    dt_timer.Tick += new EventHandler(mfCheckQRPJOBState);
                    dt_timer.Interval = new TimeSpan(0, 0, 10);
                    dt_timer.Start();
                    #endregion

                    this.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else if(e.Tab.Key.Equals("MDM"))
                {
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.Cursor = Cursors.WaitCursor;

                    #region Tab MDM IF BatchJob

                    OnStart(1);

                    #endregion

                    this.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else if (e.Tab.Key.Equals("MES"))
                {
                    //Thread threadPop = m_ProgressPopup.mfStartThread();
                    //m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    //this.Cursor = Cursors.WaitCursor;

                    #region Tibrv
                    mfReadTibState();
                    mfCheckTibState();
                    dt_tibtimer = new System.Windows.Threading.DispatcherTimer();
                    dt_tibtimer.Tick += new EventHandler(mfCheckTibState);
                    dt_tibtimer.Interval = new TimeSpan(0, 0, 10);
                    dt_tibtimer.Start();
                    #endregion

                    //this.Cursor = Cursors.Default;
                    //m_ProgressPopup.mfCloseProgressPopup(this);
                }
                else if (e.Tab.Key.Equals("WEB"))
                {
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    this.Cursor = Cursors.WaitCursor;

                    #region Tab Web.config
                    Boolean blcon = false;
                    con = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    KeyValueConfigurationCollection key = con.AppSettings.Settings;
                    uLabelPath.Text = string.Empty;
                    foreach (string _key in key.AllKeys)
                    {
                        if (_key.Equals("Web.config"))
                        {
                            uLabelPath.Text = key[_key].Value;
                            blcon = true;
                            break;
                        }
                    }

                    if (!blcon)
                    {
                        con.AppSettings.Settings.Add("Web.config", "");
                    }

                    if (!uLabelPath.Text.Equals(string.Empty))
                    {
                        strfilename = uLabelPath.Text;
                        if (File.Exists(strfilename))
                        {
                            _xml = new XmlDocument();
                            _xml.Load(strfilename);

                            ConfigConnectionString();
                            ConfigGridBinding();
                        }
                    }
                    #endregion

                    this.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
            }
            catch (Exception ex)
            {
                throw(ex);
            }
            finally
            {

            }  
        }
    }

}
