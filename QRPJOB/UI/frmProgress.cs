﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QRPJOB.UI
{
    public partial class frmProgress : Form
    {

        private string m_strProgressName;
        private DateTime m_datStartDate;

        public string ProgressName
        {
            set { m_strProgressName = value; }
        }

        public frmProgress()
        {
            InitializeComponent();
        }

        private void frmProgress_Load(object sender, EventArgs e)
        {
            uLabelFunction.Text = m_strProgressName;
            uLabelFunction.AutoSize = true;

            uActivityIndicator.AnimationEnabled = true;
            uActivityIndicator.MarqueeAnimationStyle = Infragistics.Win.UltraActivityIndicator.MarqueeAnimationStyle.BounceBack;
            uActivityIndicator.AnimationSpeed = 30;
            uActivityIndicator.MarqueeMarkerWidth = 100;
            uActivityIndicator.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            uActivityIndicator.ViewStyle = Infragistics.Win.UltraActivityIndicator.ActivityIndicatorViewStyle.Aero;
            uActivityIndicator.ResetAnimation();
            uActivityIndicator.Start();

            m_datStartDate = DateTime.Now;
            timerProgress.Interval = 1000;
            timerProgress.Start();
            
            this.Cursor = Cursors.WaitCursor;
        }

        private void frmProgress_FormClosing(object sender,
            FormClosingEventArgs e)
        {
            timerProgress.Stop();
            this.Cursor = Cursors.Default;
        }

        private void timerProgress_Tick(object sender, EventArgs e)
        {
            DateTime datCurDate = DateTime.Now;
            TimeSpan timSpendTime = DateTime.Now.Subtract(m_datStartDate);

            string strSpendDate = "";
            if (timSpendTime.Hours > 0)
                strSpendDate = timSpendTime.Hours.ToString() + "시";
            if (timSpendTime.Minutes > 0)
                strSpendDate = strSpendDate + timSpendTime.Minutes.ToString() + "분";
            if (timSpendTime.Seconds > 0)
                strSpendDate = strSpendDate + timSpendTime.Seconds.ToString() + "초";

            uLabelTime.Text = strSpendDate;
            
        }

        private void timerProgress_Tick(object sender)
        {
            DateTime datCurDate = DateTime.Now;
            TimeSpan timSpendTime = DateTime.Now.Subtract(m_datStartDate);

            string strSpendDate = "";
            if (timSpendTime.Hours > 0)
                strSpendDate = timSpendTime.Hours.ToString() + "시";
            if (timSpendTime.Minutes > 0)
                strSpendDate = strSpendDate + timSpendTime.Minutes.ToString() + "분";
            if (timSpendTime.Seconds > 0)
                strSpendDate = strSpendDate + timSpendTime.Seconds.ToString() + "초";

            uLabelTime.Text = strSpendDate;

        }

    }
}
