﻿namespace QRPJOB.UI
{
    partial class frmQRPJOB0001
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton2 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton3 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton4 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton5 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton6 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton7 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton8 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton9 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton10 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQRPJOB0001));
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupBoxBatchJob = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraButton6 = new Infragistics.Win.Misc.UltraButton();
            this.ultraButton5 = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDoNow = new Infragistics.Win.Misc.UltraButton();
            this.uButtonExit = new Infragistics.Win.Misc.UltraButton();
            this.uLabelUnit = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTick = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox6 = new Infragistics.Win.Misc.UltraGroupBox();
            this.rdYearUnit = new System.Windows.Forms.RadioButton();
            this.rdYearTick = new System.Windows.Forms.RadioButton();
            this.ultraGroupBox5 = new Infragistics.Win.Misc.UltraGroupBox();
            this.rdQuarterUnit = new System.Windows.Forms.RadioButton();
            this.rdQuarterTick = new System.Windows.Forms.RadioButton();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.rdMonthUnit = new System.Windows.Forms.RadioButton();
            this.rdMonthTick = new System.Windows.Forms.RadioButton();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.rdWeekUnit = new System.Windows.Forms.RadioButton();
            this.rdWeekTick = new System.Windows.Forms.RadioButton();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.rdDayUnit = new System.Windows.Forms.RadioButton();
            this.rdDayTick = new System.Windows.Forms.RadioButton();
            this.uDateYearUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateQuarterUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateMonthUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateWeekUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateDayUnit = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uCheckYear = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckQuarter = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckMonth = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uCheckWeek = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uLabelWarning = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonStart = new Infragistics.Win.Misc.UltraButton();
            this.uButtonInit = new Infragistics.Win.Misc.UltraButton();
            this.uButtonStop = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSave = new Infragistics.Win.Misc.UltraButton();
            this.uCheckDay = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uDateYear = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateQuarter = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateMonth = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateWeek = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uDateDay = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAnnual = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelQuarter = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMonthly = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWeekly = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDaily = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupMDM = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonRead = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDelRow = new Infragistics.Win.Misc.UltraButton();
            this.uButtonMDMExit = new Infragistics.Win.Misc.UltraButton();
            this.uButtonMDMSave = new Infragistics.Win.Misc.UltraButton();
            this.uGridMDM = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGroupConfig = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonConfigInit = new Infragistics.Win.Misc.UltraButton();
            this.uLabelPath = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonConfigExit = new Infragistics.Win.Misc.UltraButton();
            this.uLabelPassword = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCatalog = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDataSource = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonconfigGridDelete = new Infragistics.Win.Misc.UltraButton();
            this.uButtonConfigGridSave = new Infragistics.Win.Misc.UltraButton();
            this.uButtonConfigGridInit = new Infragistics.Win.Misc.UltraButton();
            this.uButtonConfigSave = new Infragistics.Win.Misc.UltraButton();
            this.uGridcon = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextCatalog = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPassword = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDataSource = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonWebConfig = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabeltibWait = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWait = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelIISSub = new Infragistics.Win.Misc.UltraLabel();
            this.uTextIISSub = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelIISMain = new Infragistics.Win.Misc.UltraLabel();
            this.uTextIISMain = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDaemon = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDaemon = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtontibInit = new Infragistics.Win.Misc.UltraButton();
            this.uButtontibSave = new Infragistics.Win.Misc.UltraButton();
            this.uLabelTargetSubject = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSourceSubject = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelNetwork = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelService = new Infragistics.Win.Misc.UltraLabel();
            this.uTextNetwork = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTargetSubject = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSourceSubJect = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextService = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtontibStart = new Infragistics.Win.Misc.UltraButton();
            this.uButtontibStop = new Infragistics.Win.Misc.UltraButton();
            this.uButtontibInstall = new Infragistics.Win.Misc.UltraButton();
            this.uButtontibUninstall = new Infragistics.Win.Misc.UltraButton();
            this.uLabelTibWarning = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonTibExit = new Infragistics.Win.Misc.UltraButton();
            this.QRPTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsStart = new System.Windows.Forms.ToolStripMenuItem();
            this.tsStop = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tstibStart = new System.Windows.Forms.ToolStripMenuItem();
            this.tstibStop = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsExit = new System.Windows.Forms.ToolStripMenuItem();
            this.uTabJob = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraGroupBox7 = new Infragistics.Win.Misc.UltraGroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxBatchJob)).BeginInit();
            this.uGroupBoxBatchJob.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).BeginInit();
            this.ultraGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).BeginInit();
            this.ultraGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateYearUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateQuarterUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMonthUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWeekUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDayUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckQuarter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateQuarter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDay)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupMDM)).BeginInit();
            this.uGroupMDM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMDM)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupConfig)).BeginInit();
            this.uGroupConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCatalog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDataSource)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextIISSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextIISMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDaemon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNetwork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTargetSubject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSourceSubJect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextService)).BeginInit();
            this.TrayMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTabJob)).BeginInit();
            this.uTabJob.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).BeginInit();
            this.ultraGroupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGroupBoxBatchJob);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(2, 24);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(674, 400);
            // 
            // uGroupBoxBatchJob
            // 
            this.uGroupBoxBatchJob.Controls.Add(this.ultraButton6);
            this.uGroupBoxBatchJob.Controls.Add(this.ultraButton5);
            this.uGroupBoxBatchJob.Controls.Add(this.uButtonDoNow);
            this.uGroupBoxBatchJob.Controls.Add(this.uButtonExit);
            this.uGroupBoxBatchJob.Controls.Add(this.uLabelUnit);
            this.uGroupBoxBatchJob.Controls.Add(this.uLabelTick);
            this.uGroupBoxBatchJob.Controls.Add(this.ultraGroupBox6);
            this.uGroupBoxBatchJob.Controls.Add(this.ultraGroupBox5);
            this.uGroupBoxBatchJob.Controls.Add(this.ultraGroupBox4);
            this.uGroupBoxBatchJob.Controls.Add(this.ultraGroupBox3);
            this.uGroupBoxBatchJob.Controls.Add(this.ultraGroupBox2);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateYearUnit);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateQuarterUnit);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateMonthUnit);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateWeekUnit);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateDayUnit);
            this.uGroupBoxBatchJob.Controls.Add(this.uCheckYear);
            this.uGroupBoxBatchJob.Controls.Add(this.uCheckQuarter);
            this.uGroupBoxBatchJob.Controls.Add(this.uCheckMonth);
            this.uGroupBoxBatchJob.Controls.Add(this.uCheckWeek);
            this.uGroupBoxBatchJob.Controls.Add(this.uLabelWarning);
            this.uGroupBoxBatchJob.Controls.Add(this.uButtonStart);
            this.uGroupBoxBatchJob.Controls.Add(this.uButtonInit);
            this.uGroupBoxBatchJob.Controls.Add(this.uButtonStop);
            this.uGroupBoxBatchJob.Controls.Add(this.uButtonSave);
            this.uGroupBoxBatchJob.Controls.Add(this.uCheckDay);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateYear);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateQuarter);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateMonth);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateWeek);
            this.uGroupBoxBatchJob.Controls.Add(this.uDateDay);
            this.uGroupBoxBatchJob.Controls.Add(this.uLabelAnnual);
            this.uGroupBoxBatchJob.Controls.Add(this.uLabelQuarter);
            this.uGroupBoxBatchJob.Controls.Add(this.uLabelMonthly);
            this.uGroupBoxBatchJob.Controls.Add(this.uLabelWeekly);
            this.uGroupBoxBatchJob.Controls.Add(this.uLabelDaily);
            this.uGroupBoxBatchJob.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBoxBatchJob.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxBatchJob.Name = "uGroupBoxBatchJob";
            this.uGroupBoxBatchJob.Size = new System.Drawing.Size(674, 400);
            this.uGroupBoxBatchJob.TabIndex = 1;
            this.uGroupBoxBatchJob.Text = "ultraGroupBox1";
            // 
            // ultraButton6
            // 
            this.ultraButton6.Enabled = false;
            this.ultraButton6.Location = new System.Drawing.Point(472, 236);
            this.ultraButton6.Name = "ultraButton6";
            this.ultraButton6.Size = new System.Drawing.Size(148, 36);
            this.ultraButton6.TabIndex = 64;
            this.ultraButton6.Text = "ultraButton3";
            this.ultraButton6.Visible = false;
            this.ultraButton6.Click += new System.EventHandler(this.ultraButton6_Click);
            // 
            // ultraButton5
            // 
            this.ultraButton5.Enabled = false;
            this.ultraButton5.Location = new System.Drawing.Point(472, 236);
            this.ultraButton5.Name = "ultraButton5";
            this.ultraButton5.Size = new System.Drawing.Size(148, 36);
            this.ultraButton5.TabIndex = 63;
            this.ultraButton5.Text = "ultraButton3";
            this.ultraButton5.Visible = false;
            this.ultraButton5.Click += new System.EventHandler(this.ultraButton5_Click);
            // 
            // uButtonDoNow
            // 
            this.uButtonDoNow.Location = new System.Drawing.Point(216, 328);
            this.uButtonDoNow.Name = "uButtonDoNow";
            this.uButtonDoNow.Size = new System.Drawing.Size(140, 36);
            this.uButtonDoNow.TabIndex = 62;
            this.uButtonDoNow.Text = "ultraButton1";
            this.uButtonDoNow.Click += new System.EventHandler(this.uButtonDoNow_Click);
            // 
            // uButtonExit
            // 
            this.uButtonExit.Location = new System.Drawing.Point(360, 328);
            this.uButtonExit.Name = "uButtonExit";
            this.uButtonExit.Size = new System.Drawing.Size(104, 36);
            this.uButtonExit.TabIndex = 61;
            this.uButtonExit.Text = "ultraButton1";
            this.uButtonExit.Click += new System.EventHandler(this.uButtonExit_Click);
            // 
            // uLabelUnit
            // 
            this.uLabelUnit.Location = new System.Drawing.Point(308, 28);
            this.uLabelUnit.Name = "uLabelUnit";
            this.uLabelUnit.Size = new System.Drawing.Size(100, 20);
            this.uLabelUnit.TabIndex = 60;
            this.uLabelUnit.Text = "ultraLabel2";
            // 
            // uLabelTick
            // 
            this.uLabelTick.Location = new System.Drawing.Point(120, 28);
            this.uLabelTick.Name = "uLabelTick";
            this.uLabelTick.Size = new System.Drawing.Size(100, 20);
            this.uLabelTick.TabIndex = 59;
            this.uLabelTick.Text = "ultraLabel2";
            // 
            // ultraGroupBox6
            // 
            this.ultraGroupBox6.Controls.Add(this.rdYearUnit);
            this.ultraGroupBox6.Controls.Add(this.rdYearTick);
            this.ultraGroupBox6.Location = new System.Drawing.Point(224, 196);
            this.ultraGroupBox6.Name = "ultraGroupBox6";
            this.ultraGroupBox6.Size = new System.Drawing.Size(80, 36);
            this.ultraGroupBox6.TabIndex = 58;
            // 
            // rdYearUnit
            // 
            this.rdYearUnit.AutoSize = true;
            this.rdYearUnit.Location = new System.Drawing.Point(56, 12);
            this.rdYearUnit.Name = "rdYearUnit";
            this.rdYearUnit.Size = new System.Drawing.Size(14, 13);
            this.rdYearUnit.TabIndex = 1;
            this.rdYearUnit.TabStop = true;
            this.rdYearUnit.UseVisualStyleBackColor = true;
            // 
            // rdYearTick
            // 
            this.rdYearTick.AutoSize = true;
            this.rdYearTick.Location = new System.Drawing.Point(12, 12);
            this.rdYearTick.Name = "rdYearTick";
            this.rdYearTick.Size = new System.Drawing.Size(14, 13);
            this.rdYearTick.TabIndex = 0;
            this.rdYearTick.TabStop = true;
            this.rdYearTick.UseVisualStyleBackColor = true;
            // 
            // ultraGroupBox5
            // 
            this.ultraGroupBox5.Controls.Add(this.rdQuarterUnit);
            this.ultraGroupBox5.Controls.Add(this.rdQuarterTick);
            this.ultraGroupBox5.Location = new System.Drawing.Point(224, 160);
            this.ultraGroupBox5.Name = "ultraGroupBox5";
            this.ultraGroupBox5.Size = new System.Drawing.Size(80, 36);
            this.ultraGroupBox5.TabIndex = 57;
            // 
            // rdQuarterUnit
            // 
            this.rdQuarterUnit.AutoSize = true;
            this.rdQuarterUnit.Location = new System.Drawing.Point(56, 12);
            this.rdQuarterUnit.Name = "rdQuarterUnit";
            this.rdQuarterUnit.Size = new System.Drawing.Size(14, 13);
            this.rdQuarterUnit.TabIndex = 1;
            this.rdQuarterUnit.TabStop = true;
            this.rdQuarterUnit.UseVisualStyleBackColor = true;
            // 
            // rdQuarterTick
            // 
            this.rdQuarterTick.AutoSize = true;
            this.rdQuarterTick.Location = new System.Drawing.Point(12, 12);
            this.rdQuarterTick.Name = "rdQuarterTick";
            this.rdQuarterTick.Size = new System.Drawing.Size(14, 13);
            this.rdQuarterTick.TabIndex = 0;
            this.rdQuarterTick.TabStop = true;
            this.rdQuarterTick.UseVisualStyleBackColor = true;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.Controls.Add(this.rdMonthUnit);
            this.ultraGroupBox4.Controls.Add(this.rdMonthTick);
            this.ultraGroupBox4.Location = new System.Drawing.Point(224, 124);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(80, 36);
            this.ultraGroupBox4.TabIndex = 56;
            // 
            // rdMonthUnit
            // 
            this.rdMonthUnit.AutoSize = true;
            this.rdMonthUnit.Location = new System.Drawing.Point(56, 12);
            this.rdMonthUnit.Name = "rdMonthUnit";
            this.rdMonthUnit.Size = new System.Drawing.Size(14, 13);
            this.rdMonthUnit.TabIndex = 1;
            this.rdMonthUnit.TabStop = true;
            this.rdMonthUnit.UseVisualStyleBackColor = true;
            // 
            // rdMonthTick
            // 
            this.rdMonthTick.AutoSize = true;
            this.rdMonthTick.Location = new System.Drawing.Point(12, 12);
            this.rdMonthTick.Name = "rdMonthTick";
            this.rdMonthTick.Size = new System.Drawing.Size(14, 13);
            this.rdMonthTick.TabIndex = 0;
            this.rdMonthTick.TabStop = true;
            this.rdMonthTick.UseVisualStyleBackColor = true;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.rdWeekUnit);
            this.ultraGroupBox3.Controls.Add(this.rdWeekTick);
            this.ultraGroupBox3.Location = new System.Drawing.Point(224, 88);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(80, 36);
            this.ultraGroupBox3.TabIndex = 55;
            // 
            // rdWeekUnit
            // 
            this.rdWeekUnit.AutoSize = true;
            this.rdWeekUnit.Location = new System.Drawing.Point(56, 12);
            this.rdWeekUnit.Name = "rdWeekUnit";
            this.rdWeekUnit.Size = new System.Drawing.Size(14, 13);
            this.rdWeekUnit.TabIndex = 1;
            this.rdWeekUnit.TabStop = true;
            this.rdWeekUnit.UseVisualStyleBackColor = true;
            // 
            // rdWeekTick
            // 
            this.rdWeekTick.AutoSize = true;
            this.rdWeekTick.Location = new System.Drawing.Point(12, 12);
            this.rdWeekTick.Name = "rdWeekTick";
            this.rdWeekTick.Size = new System.Drawing.Size(14, 13);
            this.rdWeekTick.TabIndex = 0;
            this.rdWeekTick.TabStop = true;
            this.rdWeekTick.UseVisualStyleBackColor = true;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.rdDayUnit);
            this.ultraGroupBox2.Controls.Add(this.rdDayTick);
            this.ultraGroupBox2.Location = new System.Drawing.Point(224, 52);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(80, 36);
            this.ultraGroupBox2.TabIndex = 54;
            // 
            // rdDayUnit
            // 
            this.rdDayUnit.AutoSize = true;
            this.rdDayUnit.Location = new System.Drawing.Point(56, 12);
            this.rdDayUnit.Name = "rdDayUnit";
            this.rdDayUnit.Size = new System.Drawing.Size(14, 13);
            this.rdDayUnit.TabIndex = 1;
            this.rdDayUnit.TabStop = true;
            this.rdDayUnit.UseVisualStyleBackColor = true;
            // 
            // rdDayTick
            // 
            this.rdDayTick.AutoSize = true;
            this.rdDayTick.Location = new System.Drawing.Point(12, 12);
            this.rdDayTick.Name = "rdDayTick";
            this.rdDayTick.Size = new System.Drawing.Size(14, 13);
            this.rdDayTick.TabIndex = 0;
            this.rdDayTick.TabStop = true;
            this.rdDayTick.UseVisualStyleBackColor = true;
            // 
            // uDateYearUnit
            // 
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateYearUnit.ButtonsRight.Add(spinEditorButton1);
            this.uDateYearUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateYearUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateYearUnit.Location = new System.Drawing.Point(308, 204);
            this.uDateYearUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateYearUnit.Name = "uDateYearUnit";
            this.uDateYearUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateYearUnit.TabIndex = 53;
            // 
            // uDateQuarterUnit
            // 
            spinEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateQuarterUnit.ButtonsRight.Add(spinEditorButton2);
            this.uDateQuarterUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateQuarterUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateQuarterUnit.Location = new System.Drawing.Point(308, 168);
            this.uDateQuarterUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateQuarterUnit.Name = "uDateQuarterUnit";
            this.uDateQuarterUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateQuarterUnit.TabIndex = 52;
            // 
            // uDateMonthUnit
            // 
            spinEditorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateMonthUnit.ButtonsRight.Add(spinEditorButton3);
            this.uDateMonthUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateMonthUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateMonthUnit.Location = new System.Drawing.Point(308, 132);
            this.uDateMonthUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateMonthUnit.Name = "uDateMonthUnit";
            this.uDateMonthUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateMonthUnit.TabIndex = 51;
            // 
            // uDateWeekUnit
            // 
            spinEditorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateWeekUnit.ButtonsRight.Add(spinEditorButton4);
            this.uDateWeekUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWeekUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateWeekUnit.Location = new System.Drawing.Point(308, 96);
            this.uDateWeekUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateWeekUnit.Name = "uDateWeekUnit";
            this.uDateWeekUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateWeekUnit.TabIndex = 50;
            // 
            // uDateDayUnit
            // 
            spinEditorButton5.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateDayUnit.ButtonsRight.Add(spinEditorButton5);
            this.uDateDayUnit.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateDayUnit.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateDayUnit.Location = new System.Drawing.Point(308, 60);
            this.uDateDayUnit.MaskInput = "{LOC}hh:mm:ss";
            this.uDateDayUnit.Name = "uDateDayUnit";
            this.uDateDayUnit.Size = new System.Drawing.Size(100, 21);
            this.uDateDayUnit.TabIndex = 49;
            // 
            // uCheckYear
            // 
            this.uCheckYear.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckYear.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckYear.Location = new System.Drawing.Point(424, 204);
            this.uCheckYear.Name = "uCheckYear";
            this.uCheckYear.Size = new System.Drawing.Size(48, 20);
            this.uCheckYear.TabIndex = 48;
            this.uCheckYear.Text = "사용";
            this.uCheckYear.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckQuarter
            // 
            this.uCheckQuarter.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckQuarter.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckQuarter.Location = new System.Drawing.Point(424, 168);
            this.uCheckQuarter.Name = "uCheckQuarter";
            this.uCheckQuarter.Size = new System.Drawing.Size(48, 20);
            this.uCheckQuarter.TabIndex = 47;
            this.uCheckQuarter.Text = "사용";
            this.uCheckQuarter.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckMonth
            // 
            this.uCheckMonth.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckMonth.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckMonth.Location = new System.Drawing.Point(424, 132);
            this.uCheckMonth.Name = "uCheckMonth";
            this.uCheckMonth.Size = new System.Drawing.Size(48, 20);
            this.uCheckMonth.TabIndex = 46;
            this.uCheckMonth.Text = "사용";
            this.uCheckMonth.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uCheckWeek
            // 
            this.uCheckWeek.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckWeek.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckWeek.Location = new System.Drawing.Point(424, 96);
            this.uCheckWeek.Name = "uCheckWeek";
            this.uCheckWeek.Size = new System.Drawing.Size(48, 20);
            this.uCheckWeek.TabIndex = 45;
            this.uCheckWeek.Text = "사용";
            this.uCheckWeek.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uLabelWarning
            // 
            this.uLabelWarning.Location = new System.Drawing.Point(8, 244);
            this.uLabelWarning.Name = "uLabelWarning";
            this.uLabelWarning.Size = new System.Drawing.Size(448, 20);
            this.uLabelWarning.TabIndex = 43;
            this.uLabelWarning.Text = " * 동작중인 QRP BatchJob 서비스가 존재하지 않습니다";
            this.uLabelWarning.Visible = false;
            // 
            // uButtonStart
            // 
            this.uButtonStart.Enabled = false;
            this.uButtonStart.Location = new System.Drawing.Point(4, 284);
            this.uButtonStart.Name = "uButtonStart";
            this.uButtonStart.Size = new System.Drawing.Size(104, 36);
            this.uButtonStart.TabIndex = 42;
            this.uButtonStart.Text = "ultraButton1";
            this.uButtonStart.Click += new System.EventHandler(this.uButtonStart_Click);
            // 
            // uButtonInit
            // 
            this.uButtonInit.Enabled = false;
            this.uButtonInit.Location = new System.Drawing.Point(252, 284);
            this.uButtonInit.Name = "uButtonInit";
            this.uButtonInit.Size = new System.Drawing.Size(104, 36);
            this.uButtonInit.TabIndex = 41;
            this.uButtonInit.Text = "ultraButton3";
            this.uButtonInit.Visible = false;
            // 
            // uButtonStop
            // 
            this.uButtonStop.Enabled = false;
            this.uButtonStop.Location = new System.Drawing.Point(112, 284);
            this.uButtonStop.Name = "uButtonStop";
            this.uButtonStop.Size = new System.Drawing.Size(104, 36);
            this.uButtonStop.TabIndex = 40;
            this.uButtonStop.Text = "ultraButton2";
            this.uButtonStop.Click += new System.EventHandler(this.uButtonStop_Click);
            // 
            // uButtonSave
            // 
            this.uButtonSave.Enabled = false;
            this.uButtonSave.Location = new System.Drawing.Point(360, 284);
            this.uButtonSave.Name = "uButtonSave";
            this.uButtonSave.Size = new System.Drawing.Size(104, 36);
            this.uButtonSave.TabIndex = 39;
            this.uButtonSave.Text = "ultraButton1";
            this.uButtonSave.Click += new System.EventHandler(this.uButtonSave_Click);
            // 
            // uCheckDay
            // 
            this.uCheckDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uCheckDay.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckDay.Location = new System.Drawing.Point(424, 60);
            this.uCheckDay.Name = "uCheckDay";
            this.uCheckDay.Size = new System.Drawing.Size(48, 20);
            this.uCheckDay.TabIndex = 34;
            this.uCheckDay.Text = "사용";
            this.uCheckDay.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uDateYear
            // 
            spinEditorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateYear.ButtonsRight.Add(spinEditorButton6);
            this.uDateYear.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateYear.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateYear.Location = new System.Drawing.Point(120, 204);
            this.uDateYear.MaskInput = "{LOC}hh:mm:ss";
            this.uDateYear.Name = "uDateYear";
            this.uDateYear.Size = new System.Drawing.Size(100, 21);
            this.uDateYear.TabIndex = 33;
            // 
            // uDateQuarter
            // 
            spinEditorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateQuarter.ButtonsRight.Add(spinEditorButton7);
            this.uDateQuarter.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateQuarter.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateQuarter.Location = new System.Drawing.Point(120, 168);
            this.uDateQuarter.MaskInput = "{LOC}hh:mm:ss";
            this.uDateQuarter.Name = "uDateQuarter";
            this.uDateQuarter.Size = new System.Drawing.Size(100, 21);
            this.uDateQuarter.TabIndex = 31;
            // 
            // uDateMonth
            // 
            spinEditorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateMonth.ButtonsRight.Add(spinEditorButton8);
            this.uDateMonth.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateMonth.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateMonth.Location = new System.Drawing.Point(120, 132);
            this.uDateMonth.MaskInput = "{LOC}hh:mm:ss";
            this.uDateMonth.Name = "uDateMonth";
            this.uDateMonth.Size = new System.Drawing.Size(100, 21);
            this.uDateMonth.TabIndex = 29;
            // 
            // uDateWeek
            // 
            spinEditorButton9.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateWeek.ButtonsRight.Add(spinEditorButton9);
            this.uDateWeek.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWeek.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateWeek.Location = new System.Drawing.Point(120, 96);
            this.uDateWeek.MaskInput = "{LOC}hh:mm:ss";
            this.uDateWeek.Name = "uDateWeek";
            this.uDateWeek.Size = new System.Drawing.Size(100, 21);
            this.uDateWeek.TabIndex = 27;
            // 
            // uDateDay
            // 
            spinEditorButton10.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uDateDay.ButtonsRight.Add(spinEditorButton10);
            this.uDateDay.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateDay.DropDownButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
            this.uDateDay.Location = new System.Drawing.Point(120, 60);
            this.uDateDay.MaskInput = "{LOC}hh:mm:ss";
            this.uDateDay.Name = "uDateDay";
            this.uDateDay.Size = new System.Drawing.Size(100, 21);
            this.uDateDay.TabIndex = 25;
            // 
            // uLabelAnnual
            // 
            this.uLabelAnnual.Location = new System.Drawing.Point(12, 204);
            this.uLabelAnnual.Name = "uLabelAnnual";
            this.uLabelAnnual.Size = new System.Drawing.Size(100, 20);
            this.uLabelAnnual.TabIndex = 24;
            this.uLabelAnnual.Text = "ultraLabel2";
            // 
            // uLabelQuarter
            // 
            this.uLabelQuarter.Location = new System.Drawing.Point(12, 168);
            this.uLabelQuarter.Name = "uLabelQuarter";
            this.uLabelQuarter.Size = new System.Drawing.Size(100, 20);
            this.uLabelQuarter.TabIndex = 23;
            this.uLabelQuarter.Text = "ultraLabel2";
            // 
            // uLabelMonthly
            // 
            this.uLabelMonthly.Location = new System.Drawing.Point(12, 132);
            this.uLabelMonthly.Name = "uLabelMonthly";
            this.uLabelMonthly.Size = new System.Drawing.Size(100, 20);
            this.uLabelMonthly.TabIndex = 22;
            this.uLabelMonthly.Text = "ultraLabel2";
            // 
            // uLabelWeekly
            // 
            this.uLabelWeekly.Location = new System.Drawing.Point(12, 96);
            this.uLabelWeekly.Name = "uLabelWeekly";
            this.uLabelWeekly.Size = new System.Drawing.Size(100, 20);
            this.uLabelWeekly.TabIndex = 21;
            this.uLabelWeekly.Text = "ultraLabel2";
            // 
            // uLabelDaily
            // 
            this.uLabelDaily.Location = new System.Drawing.Point(12, 60);
            this.uLabelDaily.Name = "uLabelDaily";
            this.uLabelDaily.Size = new System.Drawing.Size(100, 20);
            this.uLabelDaily.TabIndex = 20;
            this.uLabelDaily.Text = "ultraLabel2";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGroupMDM);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(674, 400);
            // 
            // uGroupMDM
            // 
            this.uGroupMDM.Controls.Add(this.uButtonRead);
            this.uGroupMDM.Controls.Add(this.uButtonDelRow);
            this.uGroupMDM.Controls.Add(this.uButtonMDMExit);
            this.uGroupMDM.Controls.Add(this.uButtonMDMSave);
            this.uGroupMDM.Controls.Add(this.uGridMDM);
            this.uGroupMDM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupMDM.Location = new System.Drawing.Point(0, 0);
            this.uGroupMDM.Name = "uGroupMDM";
            this.uGroupMDM.Size = new System.Drawing.Size(674, 400);
            this.uGroupMDM.TabIndex = 0;
            this.uGroupMDM.Text = "ultraGroupBox1";
            // 
            // uButtonRead
            // 
            this.uButtonRead.Location = new System.Drawing.Point(348, 336);
            this.uButtonRead.Name = "uButtonRead";
            this.uButtonRead.Size = new System.Drawing.Size(104, 36);
            this.uButtonRead.TabIndex = 66;
            this.uButtonRead.Text = "ultraButton1";
            this.uButtonRead.Click += new System.EventHandler(this.uButtonRead_Click);
            // 
            // uButtonDelRow
            // 
            this.uButtonDelRow.Location = new System.Drawing.Point(8, 28);
            this.uButtonDelRow.Name = "uButtonDelRow";
            this.uButtonDelRow.Size = new System.Drawing.Size(104, 28);
            this.uButtonDelRow.TabIndex = 65;
            this.uButtonDelRow.Text = "ultraButton2";
            this.uButtonDelRow.Click += new System.EventHandler(this.uButtonDelRow_Click);
            // 
            // uButtonMDMExit
            // 
            this.uButtonMDMExit.Location = new System.Drawing.Point(564, 336);
            this.uButtonMDMExit.Name = "uButtonMDMExit";
            this.uButtonMDMExit.Size = new System.Drawing.Size(104, 36);
            this.uButtonMDMExit.TabIndex = 62;
            this.uButtonMDMExit.Text = "ultraButton1";
            this.uButtonMDMExit.Click += new System.EventHandler(this.uButtonMDMExit_Click);
            // 
            // uButtonMDMSave
            // 
            this.uButtonMDMSave.Location = new System.Drawing.Point(456, 336);
            this.uButtonMDMSave.Name = "uButtonMDMSave";
            this.uButtonMDMSave.Size = new System.Drawing.Size(104, 36);
            this.uButtonMDMSave.TabIndex = 43;
            this.uButtonMDMSave.Text = "ultraButton1";
            this.uButtonMDMSave.Click += new System.EventHandler(this.uButtonMDMSave_Click);
            // 
            // uGridMDM
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMDM.DisplayLayout.Appearance = appearance13;
            this.uGridMDM.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMDM.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMDM.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMDM.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridMDM.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMDM.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGridMDM.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMDM.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMDM.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMDM.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.uGridMDM.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMDM.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMDM.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMDM.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridMDM.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMDM.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMDM.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.uGridMDM.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.uGridMDM.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMDM.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGridMDM.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGridMDM.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMDM.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.uGridMDM.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMDM.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMDM.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMDM.Location = new System.Drawing.Point(4, 59);
            this.uGridMDM.Name = "uGridMDM";
            this.uGridMDM.Size = new System.Drawing.Size(668, 243);
            this.uGridMDM.TabIndex = 0;
            this.uGridMDM.Text = "ultraGrid1";
            this.uGridMDM.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMDM_AfterCellUpdate);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.uGroupConfig);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(674, 400);
            // 
            // uGroupConfig
            // 
            this.uGroupConfig.Controls.Add(this.uButtonConfigInit);
            this.uGroupConfig.Controls.Add(this.uLabelPath);
            this.uGroupConfig.Controls.Add(this.uButtonConfigExit);
            this.uGroupConfig.Controls.Add(this.uLabelPassword);
            this.uGroupConfig.Controls.Add(this.uLabelUserID);
            this.uGroupConfig.Controls.Add(this.uLabelCatalog);
            this.uGroupConfig.Controls.Add(this.uLabelDataSource);
            this.uGroupConfig.Controls.Add(this.uButtonconfigGridDelete);
            this.uGroupConfig.Controls.Add(this.uButtonConfigGridSave);
            this.uGroupConfig.Controls.Add(this.uButtonConfigGridInit);
            this.uGroupConfig.Controls.Add(this.uButtonConfigSave);
            this.uGroupConfig.Controls.Add(this.uGridcon);
            this.uGroupConfig.Controls.Add(this.uTextCatalog);
            this.uGroupConfig.Controls.Add(this.uTextPassword);
            this.uGroupConfig.Controls.Add(this.uTextUserID);
            this.uGroupConfig.Controls.Add(this.uTextDataSource);
            this.uGroupConfig.Controls.Add(this.uButtonWebConfig);
            this.uGroupConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupConfig.Location = new System.Drawing.Point(0, 0);
            this.uGroupConfig.Name = "uGroupConfig";
            this.uGroupConfig.Size = new System.Drawing.Size(674, 400);
            this.uGroupConfig.TabIndex = 1;
            // 
            // uButtonConfigInit
            // 
            this.uButtonConfigInit.Location = new System.Drawing.Point(564, 60);
            this.uButtonConfigInit.Name = "uButtonConfigInit";
            this.uButtonConfigInit.Size = new System.Drawing.Size(104, 28);
            this.uButtonConfigInit.TabIndex = 82;
            this.uButtonConfigInit.Text = "ultraButton2";
            this.uButtonConfigInit.Click += new System.EventHandler(this.uButtonConfigInit_Click);
            // 
            // uLabelPath
            // 
            this.uLabelPath.Location = new System.Drawing.Point(116, 24);
            this.uLabelPath.Name = "uLabelPath";
            this.uLabelPath.Size = new System.Drawing.Size(440, 20);
            this.uLabelPath.TabIndex = 81;
            this.uLabelPath.Text = "ultraLabel2";
            // 
            // uButtonConfigExit
            // 
            this.uButtonConfigExit.Location = new System.Drawing.Point(564, 348);
            this.uButtonConfigExit.Name = "uButtonConfigExit";
            this.uButtonConfigExit.Size = new System.Drawing.Size(104, 28);
            this.uButtonConfigExit.TabIndex = 80;
            this.uButtonConfigExit.Text = "ultraButton1";
            this.uButtonConfigExit.Click += new System.EventHandler(this.uButtonConfigExit_Click);
            // 
            // uLabelPassword
            // 
            this.uLabelPassword.Location = new System.Drawing.Point(344, 70);
            this.uLabelPassword.Name = "uLabelPassword";
            this.uLabelPassword.Size = new System.Drawing.Size(88, 20);
            this.uLabelPassword.TabIndex = 79;
            this.uLabelPassword.Text = "ultraLabel3";
            // 
            // uLabelUserID
            // 
            this.uLabelUserID.Location = new System.Drawing.Point(344, 46);
            this.uLabelUserID.Name = "uLabelUserID";
            this.uLabelUserID.Size = new System.Drawing.Size(88, 20);
            this.uLabelUserID.TabIndex = 78;
            this.uLabelUserID.Text = "ultraLabel2";
            // 
            // uLabelCatalog
            // 
            this.uLabelCatalog.Location = new System.Drawing.Point(116, 70);
            this.uLabelCatalog.Name = "uLabelCatalog";
            this.uLabelCatalog.Size = new System.Drawing.Size(88, 20);
            this.uLabelCatalog.TabIndex = 77;
            this.uLabelCatalog.Text = "ultraLabel2";
            // 
            // uLabelDataSource
            // 
            this.uLabelDataSource.Location = new System.Drawing.Point(116, 46);
            this.uLabelDataSource.Name = "uLabelDataSource";
            this.uLabelDataSource.Size = new System.Drawing.Size(88, 20);
            this.uLabelDataSource.TabIndex = 76;
            this.uLabelDataSource.Text = "ultraLabel2";
            // 
            // uButtonconfigGridDelete
            // 
            this.uButtonconfigGridDelete.Location = new System.Drawing.Point(8, 64);
            this.uButtonconfigGridDelete.Name = "uButtonconfigGridDelete";
            this.uButtonconfigGridDelete.Size = new System.Drawing.Size(104, 28);
            this.uButtonconfigGridDelete.TabIndex = 75;
            this.uButtonconfigGridDelete.Click += new System.EventHandler(this.uButtonconfigGridDelete_Click);
            // 
            // uButtonConfigGridSave
            // 
            this.uButtonConfigGridSave.Location = new System.Drawing.Point(348, 348);
            this.uButtonConfigGridSave.Name = "uButtonConfigGridSave";
            this.uButtonConfigGridSave.Size = new System.Drawing.Size(104, 28);
            this.uButtonConfigGridSave.TabIndex = 74;
            this.uButtonConfigGridSave.Text = "ultraButton3";
            this.uButtonConfigGridSave.Click += new System.EventHandler(this.uButtonConfigGridSave_Click);
            // 
            // uButtonConfigGridInit
            // 
            this.uButtonConfigGridInit.Location = new System.Drawing.Point(456, 348);
            this.uButtonConfigGridInit.Name = "uButtonConfigGridInit";
            this.uButtonConfigGridInit.Size = new System.Drawing.Size(104, 28);
            this.uButtonConfigGridInit.TabIndex = 73;
            this.uButtonConfigGridInit.Text = "ultraButton2";
            this.uButtonConfigGridInit.Click += new System.EventHandler(this.uButtonConfigCancel_Click);
            // 
            // uButtonConfigSave
            // 
            this.uButtonConfigSave.Location = new System.Drawing.Point(564, 31);
            this.uButtonConfigSave.Name = "uButtonConfigSave";
            this.uButtonConfigSave.Size = new System.Drawing.Size(104, 28);
            this.uButtonConfigSave.TabIndex = 72;
            this.uButtonConfigSave.Text = "ultraButton2";
            this.uButtonConfigSave.Click += new System.EventHandler(this.uButtonConfigSave_Click);
            // 
            // uGridcon
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridcon.DisplayLayout.Appearance = appearance25;
            this.uGridcon.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridcon.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridcon.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridcon.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.uGridcon.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridcon.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.uGridcon.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridcon.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridcon.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridcon.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.uGridcon.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridcon.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGridcon.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridcon.DisplayLayout.Override.CellAppearance = appearance32;
            this.uGridcon.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridcon.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridcon.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.uGridcon.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.uGridcon.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridcon.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.uGridcon.DisplayLayout.Override.RowAppearance = appearance35;
            this.uGridcon.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridcon.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.uGridcon.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridcon.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridcon.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridcon.Location = new System.Drawing.Point(4, 93);
            this.uGridcon.Name = "uGridcon";
            this.uGridcon.Size = new System.Drawing.Size(664, 250);
            this.uGridcon.TabIndex = 71;
            this.uGridcon.Text = "ultraGrid1";
            this.uGridcon.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridcon_AfterCellUpdate);
            this.uGridcon.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridcon_AfterRowInsert);
            // 
            // uTextCatalog
            // 
            this.uTextCatalog.Location = new System.Drawing.Point(208, 68);
            this.uTextCatalog.Name = "uTextCatalog";
            this.uTextCatalog.Size = new System.Drawing.Size(120, 21);
            this.uTextCatalog.TabIndex = 70;
            // 
            // uTextPassword
            // 
            this.uTextPassword.Location = new System.Drawing.Point(436, 70);
            this.uTextPassword.Name = "uTextPassword";
            this.uTextPassword.PasswordChar = '*';
            this.uTextPassword.Size = new System.Drawing.Size(120, 21);
            this.uTextPassword.TabIndex = 69;
            // 
            // uTextUserID
            // 
            this.uTextUserID.Location = new System.Drawing.Point(436, 46);
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(120, 21);
            this.uTextUserID.TabIndex = 68;
            // 
            // uTextDataSource
            // 
            this.uTextDataSource.Location = new System.Drawing.Point(208, 46);
            this.uTextDataSource.Name = "uTextDataSource";
            this.uTextDataSource.Size = new System.Drawing.Size(120, 21);
            this.uTextDataSource.TabIndex = 67;
            // 
            // uButtonWebConfig
            // 
            this.uButtonWebConfig.Location = new System.Drawing.Point(8, 33);
            this.uButtonWebConfig.Name = "uButtonWebConfig";
            this.uButtonWebConfig.Size = new System.Drawing.Size(104, 28);
            this.uButtonWebConfig.TabIndex = 66;
            this.uButtonWebConfig.Text = "ultraButton2";
            this.uButtonWebConfig.Click += new System.EventHandler(this.uButtonWebConfig_Click);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.ultraGroupBox1);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(674, 400);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uLabeltibWait);
            this.ultraGroupBox1.Controls.Add(this.uLabelWait);
            this.ultraGroupBox1.Controls.Add(this.uLabelIISSub);
            this.ultraGroupBox1.Controls.Add(this.uTextIISSub);
            this.ultraGroupBox1.Controls.Add(this.uLabelIISMain);
            this.ultraGroupBox1.Controls.Add(this.uTextIISMain);
            this.ultraGroupBox1.Controls.Add(this.uLabelDaemon);
            this.ultraGroupBox1.Controls.Add(this.uTextDaemon);
            this.ultraGroupBox1.Controls.Add(this.uButtontibInit);
            this.ultraGroupBox1.Controls.Add(this.uButtontibSave);
            this.ultraGroupBox1.Controls.Add(this.uLabelTargetSubject);
            this.ultraGroupBox1.Controls.Add(this.uLabelSourceSubject);
            this.ultraGroupBox1.Controls.Add(this.uLabelNetwork);
            this.ultraGroupBox1.Controls.Add(this.uLabelService);
            this.ultraGroupBox1.Controls.Add(this.uTextNetwork);
            this.ultraGroupBox1.Controls.Add(this.uTextTargetSubject);
            this.ultraGroupBox1.Controls.Add(this.uTextSourceSubJect);
            this.ultraGroupBox1.Controls.Add(this.uTextService);
            this.ultraGroupBox1.Controls.Add(this.uButtontibStart);
            this.ultraGroupBox1.Controls.Add(this.uButtontibStop);
            this.ultraGroupBox1.Controls.Add(this.uButtontibInstall);
            this.ultraGroupBox1.Controls.Add(this.uButtontibUninstall);
            this.ultraGroupBox1.Controls.Add(this.uLabelTibWarning);
            this.ultraGroupBox1.Controls.Add(this.uButtonTibExit);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(674, 400);
            this.ultraGroupBox1.TabIndex = 1;
            this.ultraGroupBox1.Text = "ultraGroupBox1";
            // 
            // uLabeltibWait
            // 
            this.uLabeltibWait.Location = new System.Drawing.Point(12, 268);
            this.uLabeltibWait.Name = "uLabeltibWait";
            this.uLabeltibWait.Size = new System.Drawing.Size(448, 20);
            this.uLabeltibWait.TabIndex = 97;
            this.uLabeltibWait.Text = " * 동작중인 QRP BatchJob 서비스가 존재하지 않습니다";
            this.uLabeltibWait.Visible = false;
            // 
            // uLabelWait
            // 
            this.uLabelWait.Location = new System.Drawing.Point(16, 268);
            this.uLabelWait.Name = "uLabelWait";
            this.uLabelWait.Size = new System.Drawing.Size(448, 20);
            this.uLabelWait.TabIndex = 96;
            this.uLabelWait.Text = " * 동작중인 QRP Tibrv 서비스가 존재하지 않습니다";
            this.uLabelWait.Visible = false;
            // 
            // uLabelIISSub
            // 
            this.uLabelIISSub.Location = new System.Drawing.Point(20, 188);
            this.uLabelIISSub.Name = "uLabelIISSub";
            this.uLabelIISSub.Size = new System.Drawing.Size(132, 20);
            this.uLabelIISSub.TabIndex = 95;
            this.uLabelIISSub.Text = "ultraLabel8";
            // 
            // uTextIISSub
            // 
            this.uTextIISSub.Location = new System.Drawing.Point(159, 188);
            this.uTextIISSub.Name = "uTextIISSub";
            this.uTextIISSub.Size = new System.Drawing.Size(244, 21);
            this.uTextIISSub.TabIndex = 94;
            // 
            // uLabelIISMain
            // 
            this.uLabelIISMain.Location = new System.Drawing.Point(20, 164);
            this.uLabelIISMain.Name = "uLabelIISMain";
            this.uLabelIISMain.Size = new System.Drawing.Size(132, 20);
            this.uLabelIISMain.TabIndex = 93;
            this.uLabelIISMain.Text = "ultraLabel9";
            // 
            // uTextIISMain
            // 
            this.uTextIISMain.Location = new System.Drawing.Point(159, 164);
            this.uTextIISMain.Name = "uTextIISMain";
            this.uTextIISMain.Size = new System.Drawing.Size(244, 21);
            this.uTextIISMain.TabIndex = 92;
            // 
            // uLabelDaemon
            // 
            this.uLabelDaemon.Location = new System.Drawing.Point(20, 140);
            this.uLabelDaemon.Name = "uLabelDaemon";
            this.uLabelDaemon.Size = new System.Drawing.Size(132, 20);
            this.uLabelDaemon.TabIndex = 91;
            this.uLabelDaemon.Text = "ultraLabel7";
            // 
            // uTextDaemon
            // 
            this.uTextDaemon.Location = new System.Drawing.Point(159, 140);
            this.uTextDaemon.Name = "uTextDaemon";
            this.uTextDaemon.Size = new System.Drawing.Size(244, 21);
            this.uTextDaemon.TabIndex = 90;
            // 
            // uButtontibInit
            // 
            this.uButtontibInit.Location = new System.Drawing.Point(248, 216);
            this.uButtontibInit.Name = "uButtontibInit";
            this.uButtontibInit.Size = new System.Drawing.Size(104, 28);
            this.uButtontibInit.TabIndex = 89;
            this.uButtontibInit.Text = "ultraButton2";
            this.uButtontibInit.Click += new System.EventHandler(this.uButtontibInit_Click);
            // 
            // uButtontibSave
            // 
            this.uButtontibSave.Location = new System.Drawing.Point(140, 216);
            this.uButtontibSave.Name = "uButtontibSave";
            this.uButtontibSave.Size = new System.Drawing.Size(104, 28);
            this.uButtontibSave.TabIndex = 88;
            this.uButtontibSave.Text = "ultraButton2";
            this.uButtontibSave.Click += new System.EventHandler(this.uButtontibSave_Click);
            // 
            // uLabelTargetSubject
            // 
            this.uLabelTargetSubject.Location = new System.Drawing.Point(20, 116);
            this.uLabelTargetSubject.Name = "uLabelTargetSubject";
            this.uLabelTargetSubject.Size = new System.Drawing.Size(132, 20);
            this.uLabelTargetSubject.TabIndex = 87;
            this.uLabelTargetSubject.Text = "ultraLabel3";
            // 
            // uLabelSourceSubject
            // 
            this.uLabelSourceSubject.Location = new System.Drawing.Point(20, 92);
            this.uLabelSourceSubject.Name = "uLabelSourceSubject";
            this.uLabelSourceSubject.Size = new System.Drawing.Size(132, 20);
            this.uLabelSourceSubject.TabIndex = 86;
            this.uLabelSourceSubject.Text = "ultraLabel2";
            // 
            // uLabelNetwork
            // 
            this.uLabelNetwork.Location = new System.Drawing.Point(20, 72);
            this.uLabelNetwork.Name = "uLabelNetwork";
            this.uLabelNetwork.Size = new System.Drawing.Size(132, 20);
            this.uLabelNetwork.TabIndex = 85;
            this.uLabelNetwork.Text = "ultraLabel2";
            // 
            // uLabelService
            // 
            this.uLabelService.Location = new System.Drawing.Point(20, 48);
            this.uLabelService.Name = "uLabelService";
            this.uLabelService.Size = new System.Drawing.Size(132, 20);
            this.uLabelService.TabIndex = 84;
            this.uLabelService.Text = "ultraLabel2";
            // 
            // uTextNetwork
            // 
            this.uTextNetwork.Location = new System.Drawing.Point(159, 70);
            this.uTextNetwork.Name = "uTextNetwork";
            this.uTextNetwork.Size = new System.Drawing.Size(244, 21);
            this.uTextNetwork.TabIndex = 83;
            // 
            // uTextTargetSubject
            // 
            this.uTextTargetSubject.Location = new System.Drawing.Point(159, 116);
            this.uTextTargetSubject.Name = "uTextTargetSubject";
            this.uTextTargetSubject.Size = new System.Drawing.Size(244, 21);
            this.uTextTargetSubject.TabIndex = 82;
            // 
            // uTextSourceSubJect
            // 
            this.uTextSourceSubJect.Location = new System.Drawing.Point(159, 92);
            this.uTextSourceSubJect.Name = "uTextSourceSubJect";
            this.uTextSourceSubJect.Size = new System.Drawing.Size(244, 21);
            this.uTextSourceSubJect.TabIndex = 81;
            // 
            // uTextService
            // 
            this.uTextService.Location = new System.Drawing.Point(159, 48);
            this.uTextService.Name = "uTextService";
            this.uTextService.Size = new System.Drawing.Size(244, 21);
            this.uTextService.TabIndex = 80;
            // 
            // uButtontibStart
            // 
            this.uButtontibStart.Enabled = false;
            this.uButtontibStart.Location = new System.Drawing.Point(16, 332);
            this.uButtontibStart.Name = "uButtontibStart";
            this.uButtontibStart.Size = new System.Drawing.Size(104, 36);
            this.uButtontibStart.TabIndex = 72;
            this.uButtontibStart.Text = "ultraButton1";
            this.uButtontibStart.Click += new System.EventHandler(this.uButtontibStart_Click);
            // 
            // uButtontibStop
            // 
            this.uButtontibStop.Enabled = false;
            this.uButtontibStop.Location = new System.Drawing.Point(124, 332);
            this.uButtontibStop.Name = "uButtontibStop";
            this.uButtontibStop.Size = new System.Drawing.Size(104, 36);
            this.uButtontibStop.TabIndex = 71;
            this.uButtontibStop.Text = "ultraButton2";
            this.uButtontibStop.Click += new System.EventHandler(this.uButtontibStop_Click);
            // 
            // uButtontibInstall
            // 
            this.uButtontibInstall.Enabled = false;
            this.uButtontibInstall.Location = new System.Drawing.Point(480, 260);
            this.uButtontibInstall.Name = "uButtontibInstall";
            this.uButtontibInstall.Size = new System.Drawing.Size(148, 36);
            this.uButtontibInstall.TabIndex = 70;
            this.uButtontibInstall.Text = "ultraButton3";
            this.uButtontibInstall.Visible = false;
            this.uButtontibInstall.Click += new System.EventHandler(this.uButtontibInstall_Click);
            // 
            // uButtontibUninstall
            // 
            this.uButtontibUninstall.Enabled = false;
            this.uButtontibUninstall.Location = new System.Drawing.Point(480, 260);
            this.uButtontibUninstall.Name = "uButtontibUninstall";
            this.uButtontibUninstall.Size = new System.Drawing.Size(148, 36);
            this.uButtontibUninstall.TabIndex = 69;
            this.uButtontibUninstall.Text = "ultraButton3";
            this.uButtontibUninstall.Visible = false;
            this.uButtontibUninstall.Click += new System.EventHandler(this.uButtontibUninstall_Click);
            // 
            // uLabelTibWarning
            // 
            this.uLabelTibWarning.Location = new System.Drawing.Point(16, 268);
            this.uLabelTibWarning.Name = "uLabelTibWarning";
            this.uLabelTibWarning.Size = new System.Drawing.Size(448, 20);
            this.uLabelTibWarning.TabIndex = 67;
            this.uLabelTibWarning.Text = " * 동작중인 QRP Tibrv 서비스가 존재하지 않습니다";
            this.uLabelTibWarning.Visible = false;
            // 
            // uButtonTibExit
            // 
            this.uButtonTibExit.Location = new System.Drawing.Point(524, 332);
            this.uButtonTibExit.Name = "uButtonTibExit";
            this.uButtonTibExit.Size = new System.Drawing.Size(104, 36);
            this.uButtonTibExit.TabIndex = 62;
            this.uButtonTibExit.Text = "ultraButton1";
            this.uButtonTibExit.Click += new System.EventHandler(this.uButtonTibExit_Click);
            // 
            // QRPTray
            // 
            this.QRPTray.ContextMenuStrip = this.TrayMenu;
            this.QRPTray.Icon = ((System.Drawing.Icon)(resources.GetObject("QRPTray.Icon")));
            this.QRPTray.Text = "QRPJob.UI";
            this.QRPTray.Visible = true;
            this.QRPTray.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.QRPTray_MouseDoubleClick);
            // 
            // TrayMenu
            // 
            this.TrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStart,
            this.tsStop,
            this.toolStripSeparator2,
            this.tstibStart,
            this.tstibStop,
            this.toolStripSeparator1,
            this.tsExit});
            this.TrayMenu.Name = "contextMenuStrip1";
            this.TrayMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.TrayMenu.ShowImageMargin = false;
            this.TrayMenu.ShowItemToolTips = false;
            this.TrayMenu.Size = new System.Drawing.Size(168, 126);
            // 
            // tsStart
            // 
            this.tsStart.Enabled = false;
            this.tsStart.Name = "tsStart";
            this.tsStart.Size = new System.Drawing.Size(167, 22);
            this.tsStart.Text = "QRPJOB 서비스 시작";
            this.tsStart.Click += new System.EventHandler(this.tsStart_Click);
            // 
            // tsStop
            // 
            this.tsStop.Enabled = false;
            this.tsStop.Name = "tsStop";
            this.tsStop.Size = new System.Drawing.Size(167, 22);
            this.tsStop.Text = "QRPJOB 서비스 중지";
            this.tsStop.Click += new System.EventHandler(this.tsStop_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(164, 6);
            // 
            // tstibStart
            // 
            this.tstibStart.Enabled = false;
            this.tstibStart.Name = "tstibStart";
            this.tstibStart.Size = new System.Drawing.Size(167, 22);
            this.tstibStart.Text = "Tibrv 서비스 시작";
            this.tstibStart.Click += new System.EventHandler(this.tstibStart_Click);
            // 
            // tstibStop
            // 
            this.tstibStop.Enabled = false;
            this.tstibStop.Name = "tstibStop";
            this.tstibStop.Size = new System.Drawing.Size(167, 22);
            this.tstibStop.Text = "Tibrv 서비스 중지";
            this.tstibStop.Click += new System.EventHandler(this.tstibStop_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(164, 6);
            // 
            // tsExit
            // 
            this.tsExit.Name = "tsExit";
            this.tsExit.Size = new System.Drawing.Size(167, 22);
            this.tsExit.Text = "종료";
            this.tsExit.Click += new System.EventHandler(this.tsExit_Click);
            // 
            // uTabJob
            // 
            this.uTabJob.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabJob.Controls.Add(this.ultraTabPageControl1);
            this.uTabJob.Controls.Add(this.ultraTabPageControl2);
            this.uTabJob.Controls.Add(this.ultraTabPageControl3);
            this.uTabJob.Controls.Add(this.ultraTabPageControl4);
            this.uTabJob.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uTabJob.Location = new System.Drawing.Point(0, 68);
            this.uTabJob.Name = "uTabJob";
            this.uTabJob.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabJob.Size = new System.Drawing.Size(678, 426);
            this.uTabJob.TabIndex = 1;
            ultraTab1.Key = "INS";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "품질정보 BatchJob";
            ultraTab2.Key = "MDM";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "MDM IF BatchJob";
            ultraTab3.Key = "WEB";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "QRP Web Server 설정";
            ultraTab4.Key = "MES";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "QPR-MES Tibrv 설정";
            this.uTabJob.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.uTabJob.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.uTabJob_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(674, 400);
            // 
            // ultraGroupBox7
            // 
            this.ultraGroupBox7.Controls.Add(this.radioButton2);
            this.ultraGroupBox7.Controls.Add(this.radioButton1);
            this.ultraGroupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox7.Name = "ultraGroupBox7";
            this.ultraGroupBox7.Size = new System.Drawing.Size(678, 64);
            this.ultraGroupBox7.TabIndex = 2;
            this.ultraGroupBox7.Text = "ultraGroupBox7";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(106, 36);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(45, 16);
            this.radioButton2.TabIndex = 5;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Sub";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(12, 36);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(51, 16);
            this.radioButton1.TabIndex = 4;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Main";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // frmQRPJOB0001
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(678, 494);
            this.Controls.Add(this.ultraGroupBox7);
            this.Controls.Add(this.uTabJob);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmQRPJOB0001";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "품질검사관리 BatchJob 설정";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmQRPJOB0001_FormClosing);
            this.Resize += new System.EventHandler(this.frmQRPJOB0001_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxBatchJob)).EndInit();
            this.uGroupBoxBatchJob.ResumeLayout(false);
            this.uGroupBoxBatchJob.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox6)).EndInit();
            this.ultraGroupBox6.ResumeLayout(false);
            this.ultraGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox5)).EndInit();
            this.ultraGroupBox5.ResumeLayout(false);
            this.ultraGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uDateYearUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateQuarterUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMonthUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWeekUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDayUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckQuarter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateQuarter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateDay)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupMDM)).EndInit();
            this.uGroupMDM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMDM)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupConfig)).EndInit();
            this.uGroupConfig.ResumeLayout(false);
            this.uGroupConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCatalog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDataSource)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextIISSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextIISMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDaemon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextNetwork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTargetSubject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSourceSubJect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextService)).EndInit();
            this.TrayMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTabJob)).EndInit();
            this.uTabJob.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox7)).EndInit();
            this.ultraGroupBox7.ResumeLayout(false);
            this.ultraGroupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon QRPTray;
        private System.Windows.Forms.ContextMenuStrip TrayMenu;
        private System.Windows.Forms.ToolStripMenuItem tsExit;
        private System.Windows.Forms.ToolStripMenuItem tsStart;
        private System.Windows.Forms.ToolStripMenuItem tsStop;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabJob;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxBatchJob;
        private Infragistics.Win.Misc.UltraButton uButtonDoNow;
        private Infragistics.Win.Misc.UltraButton uButtonExit;
        private Infragistics.Win.Misc.UltraLabel uLabelUnit;
        private Infragistics.Win.Misc.UltraLabel uLabelTick;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox6;
        private System.Windows.Forms.RadioButton rdYearUnit;
        private System.Windows.Forms.RadioButton rdYearTick;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox5;
        private System.Windows.Forms.RadioButton rdQuarterUnit;
        private System.Windows.Forms.RadioButton rdQuarterTick;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
        private System.Windows.Forms.RadioButton rdMonthUnit;
        private System.Windows.Forms.RadioButton rdMonthTick;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private System.Windows.Forms.RadioButton rdWeekUnit;
        private System.Windows.Forms.RadioButton rdWeekTick;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.RadioButton rdDayUnit;
        private System.Windows.Forms.RadioButton rdDayTick;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateYearUnit;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateQuarterUnit;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateMonthUnit;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWeekUnit;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDayUnit;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckYear;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckQuarter;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckMonth;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckWeek;
        private Infragistics.Win.Misc.UltraLabel uLabelWarning;
        private Infragistics.Win.Misc.UltraButton uButtonStart;
        private Infragistics.Win.Misc.UltraButton uButtonInit;
        private Infragistics.Win.Misc.UltraButton uButtonStop;
        private Infragistics.Win.Misc.UltraButton uButtonSave;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckDay;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateYear;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateQuarter;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateMonth;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWeek;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateDay;
        private Infragistics.Win.Misc.UltraLabel uLabelAnnual;
        private Infragistics.Win.Misc.UltraLabel uLabelQuarter;
        private Infragistics.Win.Misc.UltraLabel uLabelMonthly;
        private Infragistics.Win.Misc.UltraLabel uLabelWeekly;
        private Infragistics.Win.Misc.UltraLabel uLabelDaily;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private Infragistics.Win.Misc.UltraGroupBox uGroupMDM;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMDM;
        private Infragistics.Win.Misc.UltraButton uButtonMDMExit;
        private Infragistics.Win.Misc.UltraButton uButtonMDMSave;
        private Infragistics.Win.Misc.UltraButton uButtonDelRow;
        private Infragistics.Win.Misc.UltraButton uButtonRead;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.Misc.UltraGroupBox uGroupConfig;
        private Infragistics.Win.Misc.UltraButton uButtonWebConfig;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDataSource;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPassword;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCatalog;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridcon;
        private Infragistics.Win.Misc.UltraButton uButtonConfigGridSave;
        private Infragistics.Win.Misc.UltraButton uButtonConfigGridInit;
        private Infragistics.Win.Misc.UltraButton uButtonConfigSave;
        private Infragistics.Win.Misc.UltraButton uButtonconfigGridDelete;
        private Infragistics.Win.Misc.UltraLabel uLabelPassword;
        private Infragistics.Win.Misc.UltraLabel uLabelUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCatalog;
        private Infragistics.Win.Misc.UltraLabel uLabelDataSource;
        private Infragistics.Win.Misc.UltraButton uButtonConfigExit;
        private Infragistics.Win.Misc.UltraLabel uLabelPath;
        private Infragistics.Win.Misc.UltraButton uButtonConfigInit;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton uButtonTibExit;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox7;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private Infragistics.Win.Misc.UltraButton ultraButton5;
        private Infragistics.Win.Misc.UltraButton ultraButton6;
        private Infragistics.Win.Misc.UltraButton uButtontibStart;
        private Infragistics.Win.Misc.UltraButton uButtontibStop;
        private Infragistics.Win.Misc.UltraButton uButtontibInstall;
        private Infragistics.Win.Misc.UltraButton uButtontibUninstall;
        private Infragistics.Win.Misc.UltraLabel uLabelTibWarning;
        private Infragistics.Win.Misc.UltraButton uButtontibInit;
        private Infragistics.Win.Misc.UltraButton uButtontibSave;
        private Infragistics.Win.Misc.UltraLabel uLabelTargetSubject;
        private Infragistics.Win.Misc.UltraLabel uLabelSourceSubject;
        private Infragistics.Win.Misc.UltraLabel uLabelNetwork;
        private Infragistics.Win.Misc.UltraLabel uLabelService;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextNetwork;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTargetSubject;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSourceSubJect;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextService;
        private Infragistics.Win.Misc.UltraLabel uLabelIISSub;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextIISSub;
        private Infragistics.Win.Misc.UltraLabel uLabelIISMain;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextIISMain;
        private Infragistics.Win.Misc.UltraLabel uLabelDaemon;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDaemon;
        private Infragistics.Win.Misc.UltraLabel uLabelWait;
        private System.Windows.Forms.ToolStripMenuItem tstibStart;
        private System.Windows.Forms.ToolStripMenuItem tstibStop;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private Infragistics.Win.Misc.UltraLabel uLabeltibWait;

    }
}

