﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace QRPJOB.UI
{
    static class Program
    {

        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new frmQRPJOB0001());

            /// 출처:http://dotnetperls.com/single-instance-windows-form
            if (ProcessChecker.IsOnlyProcess("QRPJOB"))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmQRPJOB0001());
            }
        }
    }
}
