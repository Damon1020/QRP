﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;
using System.Collections;
using System.ServiceProcess;
using System.Threading;
using System.Net;
using System.Globalization;

using QRPSTA;
namespace QRPJOB.UI
{
    public partial class frmJOB0002 : Form
    {

        private static IPHostEntry ip;
        private const string m_strUserID = "QRPServer";

        public frmJOB0002()
        {
            InitializeComponent();

            InitValue();
            InitLabel();
            InitButton();
            InitGroupBox();

            ip = Dns.GetHostEntry(Dns.GetHostName());
        }

        #region Initialize
        private void InitLabel()
        {
            try
            {
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelDay, "일별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelWeek, "주별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelMonth, "월별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelQuarter, "분기별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelYear, "연별", "굴림", true, true);
                wLabel.mfSetLabel(this.uLabelUnitTime, "기준시간", "굴림", true, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n"
                    + ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDay, "실행", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.uButtonWeek, "실행", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.uButtonMonth, "실행", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.uButtonQuarter, "실행", "굴림", Properties.Resources.btn_Record);
                wButton.mfSetButton(this.uButtonYear, "실행", "굴림", Properties.Resources.btn_Record);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void InitValue()
        {
            try
            {
                DateTime now = DateTime.Now;
                uDateDay.Value = now;
                uDateDayUnit.Value = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 10:00:00");

                uDateWeek.Value = now;
                uDateWeekUnit.Value = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 10:00:00");

                uDateMonth.Value = now;
                uDateMonthUnit.Value = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 10:00:00");

                uDateQuarter.Value = now;
                uDateQuarterUnit.Value = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 10:00:00");

                uDateYear.Value = now;
                uDateYearUnit.Value = DateTime.Parse(now.ToString("yyyy-MM-dd") + " 10:00:00");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n"
                    + ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void InitGroupBox()
        {
            try
            {
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.ultraGroupBox1, GroupBoxType.INFO, "BatchJob 강제실행", "굴림"
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.ultraGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                this.ultraGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
        }
        #endregion

        #region Button Event
        private void uButtonDay_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            try
            {
                string strFromDate = DateTime.Parse(uDateDay.Value.ToString()).ToString("yyyy-MM-dd")
                        + " " + DateTime.Parse(uDateDayUnit.Value.ToString()).ToString("HH:mm:ss");
                string strToDate = DateTime.Parse(uDateDayUnit.Value.ToString()).AddDays(1).ToString("yyyy-MM-dd")
                    + " " + GetDate.MinusOneSec(DateTime.Parse(uDateDayUnit.Value.ToString()).ToString("HH:mm:ss"));

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "확인창", "저장확인", "BatchJob을 실행하시겠습니까?",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //Thread t1 = m_ProgressPopup.mfStartThread();
                //m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.Cursor = Cursors.WaitCursor;
                #region BL
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                //brwChannel.mfCredentials(clsImport);
                DataTable dtINS = new DataTable();
                DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                    string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND VendorCode = '" + strVendorCode
                                    + "' AND MaterialCode = '" + strMaterialCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }
                        
                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsImport.mfSaveImportInspectJob_Day(strFromDate.Substring(0, 10), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }

                #endregion

                #region BL
                //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                //brwChannel.mfCredentials(clsProcess);
                dtINS = new DataTable();
                dtInpect = new DataTable();
                DataTable dtPackage = new DataTable();
                DataTable dtPackageValue = new DataTable();

                dtInpect = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                    string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();
                    
                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND ProductCode = '" + strProductCode
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Day(strFromDate.Substring(0, 10), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }

                #region ProcessInspectPackage
                for (int i = 0; i < dtPackage.Rows.Count; i++)
                {
                    string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                    string strPackage = dtPackage.Rows[i]["Package"].ToString();
                    string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND Package = '" + strPackage
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drPack = dtPackageValue.Select(strFilter);

                    if (drPack.Length != 0)
                    {
                        double[] arrValue = new double[drPack.Length];

                        for (int j = 0; j < drPack.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Day_Package(strFromDate.Substring(0, 10), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion
                #endregion

                //m_ProgressPopup.mfCloseProgressPopup(this);
                System.Windows.Forms.DialogResult result;
                this.Cursor = Cursors.Default;
                result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "처리결과", "BatchJob", "BatchJob을 실행했습니다.",
                                            Infragistics.Win.HAlign.Right);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void uButtonWeek_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            try
            {
                DateTime dtWeek = DateTime.Parse(uDateWeek.Value.ToString());
                DateTime dtWeekUnit = DateTime.Parse(uDateWeekUnit.Value.ToString());
                string strFromDate = dtWeek.AddDays(0 - (int)dtWeek.DayOfWeek).ToString("yyyy-MM-dd")
                    + " " + dtWeekUnit.ToString("HH:mm:ss");
                string strToDate = DateTime.Parse(strFromDate).AddDays(7).ToString("yyyy-MM-dd")
                    + " " + GetDate.MinusOneSec(dtWeekUnit.ToString("HH:mm:ss"));

                CultureInfo cl = CultureInfo.CurrentCulture;
                Calendar cal = cl.Calendar;
                
                int weekoftoday = cal.GetWeekOfYear(dtWeek, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "확인창", "저장확인", "BatchJob을 실행하시겠습니까?",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }
                #region BL
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                //brwChannel.mfCredentials(clsImport);
                DataTable dtINS = new DataTable();
                DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                    string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND VendorCode = '" + strVendorCode
                                    + "' AND MaterialCode = '" + strMaterialCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsImport.mfSaveImportInspectJob_Week(strFromDate.Substring(0, 4),strFromDate.Substring(4, 2), weekoftoday.ToString(), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }

                #endregion

                #region BL
                //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                //brwChannel.mfCredentials(clsProcess);
                dtINS = new DataTable();
                DataTable dtPackage = new DataTable();
                DataTable dtPackageValue = new DataTable();
                DataTable dtInpectValue = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                #region ProcessInspect
                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                    string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                    string strGeneraion = dtINS.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND ProductCode = '" + strProductCode
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneraion
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpectValue.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneraion, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Week(strFromDate.Substring(0, 4), strFromDate.Substring(4, 2), weekoftoday.ToString(), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion

                #region ProcessInspectPackage
                for (int i = 0; i < dtPackage.Rows.Count; i++)
                {
                    string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                    string strPackage = dtPackage.Rows[i]["Package"].ToString();
                    string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND Package = '" + strPackage
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drPack = dtPackageValue.Select(strFilter);

                    if (drPack.Length != 0)
                    {
                        double[] arrValue = new double[drPack.Length];

                        for (int j = 0; j < drPack.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Week_Package(strFromDate.Substring(0, 4), strFromDate.Substring(4, 2), weekoftoday.ToString(), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion
                #endregion

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void uButtonMonth_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            try
            {
                DateTime dtMonth = DateTime.Parse(uDateMonth.Value.ToString());
                DateTime dtMonthUnit = DateTime.Parse(uDateMonthUnit.Value.ToString());

                string strFromDate = dtMonth.AddDays(1 - dtMonth.Day).ToString("yyyy-MM-dd")
                    + " " + dtMonthUnit.ToString("HH:mm:ss");
                string strToDate = DateTime.Parse(strFromDate).AddMonths(1).ToString("yyyy-MM-dd")
                    + " " + GetDate.MinusOneSec(dtMonthUnit.ToString("HH:mm:ss"));

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "확인창", "저장확인", "BatchJob을 실행하시겠습니까?",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                #region BL
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                //brwChannel.mfCredentials(clsImport);
                DataTable dtINS = new DataTable();
                DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                    string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND VendorCode = '" + strVendorCode
                                    + "' AND MaterialCode = '" + strMaterialCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsImport.mfSaveImportInspectJob_Month(strFromDate.Substring(0, 7), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }

                #endregion

                #region BL
                //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                //brwChannel.mfCredentials(clsProcess);
                dtINS = new DataTable();
                dtInpect = new DataTable();
                DataTable dtPackage = new DataTable();
                DataTable dtPackageValue = new DataTable();
                dtInpect = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                    string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND ProductCode = '" + strProductCode
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Month(strFromDate.Substring(0, 7), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }

                #region ProcessInspectPackage
                for (int i = 0; i < dtPackage.Rows.Count; i++)
                {
                    string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                    string strPackage = dtPackage.Rows[i]["Package"].ToString();
                    string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND Package = '" + strPackage
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drPack = dtPackageValue.Select(strFilter);

                    if (drPack.Length != 0)
                    {
                        double[] arrValue = new double[drPack.Length];

                        for (int j = 0; j < drPack.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Month_Package(strFromDate.Substring(0, 7), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void uButtonQuarter_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            try
            {
                DateTime dtQuarter = DateTime.Parse(uDateQuarter.Value.ToString());
                DateTime dtQuarterUnit = DateTime.Parse(uDateQuarterUnit.Value.ToString());

                string strFromDate = GetDate.FirstDayInQuarter(dtQuarter).ToString("yyyy-MM-dd")
                    + " " + dtQuarterUnit.ToString("HH:mm:ss");
                string strToDate = DateTime.Parse(strFromDate).AddMonths(3).ToString("yyyy-MM-dd")
                    + " " + GetDate.MinusOneSec(dtQuarterUnit.ToString("HH:mm:ss"));
                int numQuarter = GetDate.Quarter(DateTime.Parse(strFromDate));
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "확인창", "저장확인", "BatchJob을 실행하시겠습니까?",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }
                #region ImportInspect
                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                //brwChannel.mfCredentials(clsImport);
                DataTable dtINS = new DataTable();
                DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                    string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND VendorCode = '" + strVendorCode
                                    + "' AND MaterialCode = '" + strMaterialCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsImport.mfSaveImportInspectJob_Quarter(dtQuarter.Year.ToString(), numQuarter, dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }

                #endregion

                DataTable dtReqItemCL = clsImport.mfReadImportInspectJob_CL(strFromDate, strToDate);

                #region ImportInspectCL
                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                    string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND VendorCode = '" + strVendorCode
                                    + "' AND MaterialCode = '" + strMaterialCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtReqItemCL.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        //double[] arrValue = new double[drINS.Length];
                        DataTable dtMean = new DataTable();
                        dtMean.Columns.Add("Mean");
                        DataTable dtDataRange = new DataTable();
                        dtDataRange.Columns.Add("DataRange");
                        DataTable dtStdDev = new DataTable();
                        dtStdDev.Columns.Add("StdDev");
                        DataTable dtInspectValue = new DataTable();
                        dtInspectValue.Columns.Add("InspectValue");

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            DataRow drMean = dtMean.NewRow();
                            DataRow drDataRange = dtDataRange.NewRow();
                            DataRow drStdDev = dtStdDev.NewRow();
                            
                            drMean["Mean"] = drINS[j]["Mean"].ToString();
                            drDataRange["DataRange"] = drINS[j]["DataRange"].ToString();
                            drStdDev["StdDev"] = drINS[j]["StdDev"].ToString();
                            
                            dtMean.Rows.Add(drMean);
                            dtDataRange.Rows.Add(drDataRange);
                            dtStdDev.Rows.Add(drStdDev);
                            
                        }

                        DataRow[] drArrInspectValue = dtInpect.Select(strFilter);

                        if (!drArrInspectValue.Length.Equals(0))
                        {
                            for (int j = 0; j < drArrInspectValue.Length; j++)
                            {
                                DataRow drInspectValue = dtInspectValue.NewRow();
                                drInspectValue["InspectValue"] = drArrInspectValue[j]["InspectValue"].ToString();
                                dtInspectValue.Rows.Add(drInspectValue);
                            }
                        }

                        STASPC cls = new STASPC();
                        STAControlLimit limit = new STAControlLimit();
                        limit.InitSTAControlLimit();
                        limit = cls.mfCalcControlLimit(dtInspectValue, dtMean, dtDataRange, dtStdDev);

                        DataTable dtValue = mfMakeDataTable_ImportInspetJob_CL(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                dtQuarter.Year.ToString(), numQuarter, limit);

                        clsImport.mfSaveImportInspectJob_Quarter_CL(dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion

                #region ProcessInspect
                //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                //brwChannel.mfCredentials(clsProcess);
                dtINS = new DataTable();
                dtInpect = new DataTable();
                DataTable dtPackage = new DataTable();
                DataTable dtPackageValue = new DataTable();
                dtInpect = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                    string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND ProductCode = '" + strProductCode
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Quarter(dtQuarter.Year.ToString(), numQuarter, dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion

                DataTable dtProcCL = clsProcess.mfReadProcessInspectJobCL(strFromDate, strToDate, "F");
                #region ProcessInspectCL
                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                    string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND ProductCode = '" + strProductCode
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtProcCL.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        DataTable dtMean = new DataTable();
                        dtMean.Columns.Add("Mean");
                        DataTable dtStdDev = new DataTable();
                        dtStdDev.Columns.Add("StdDev");
                        DataTable dtDataRange = new DataTable();
                        dtDataRange.Columns.Add("DataRange");

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            DataRow drMean = dtMean.NewRow();
                            DataRow drDataRange = dtDataRange.NewRow();
                            DataRow drStdDev = dtStdDev.NewRow();

                            drMean["Mean"] = drINS[j]["Mean"].ToString();
                            drDataRange["DataRange"] = drINS[j]["DataRange"].ToString();
                            drStdDev["StdDev"] = drINS[j]["StdDev"].ToString();

                            dtMean.Rows.Add(drMean);
                            dtDataRange.Rows.Add(drDataRange);
                            dtStdDev.Rows.Add(drStdDev);
                        }

                        DataTable dtInspectValue = new DataTable();
                        dtInspectValue.Columns.Add("InspectValue");

                        DataRow[] drArrInspectValue = dtInpect.Select(strFilter);

                        for (int j = 0; j < drArrInspectValue.Length; j++)
                        {
                            DataRow drInspectValue = dtInspectValue.NewRow();
                            drInspectValue["InspectValue"] = drArrInspectValue[j]["InspectValue"].ToString();
                            dtInspectValue.Rows.Add(drInspectValue);
                        }

                        STASPC cls = new STASPC();
                        STAControlLimit limit = new STAControlLimit();
                        limit.InitSTAControlLimit();
                        limit = cls.mfCalcControlLimit(dtInspectValue, dtMean, dtDataRange, dtStdDev);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob_CL(strPlantCode, "", strProductCode, strStackSeq, strGeneration,
                                            strProcessCode, strInspectItemCode, dtQuarter.Year.ToString(), numQuarter, limit);

                        clsProcess.mfSaveProcessInspectCL(dtValue, ip.AddressList[0].ToString(), m_strUserID, "F");
                    }
                }
                #endregion

                #region ProcessInspectPackage
                for (int i = 0; i < dtPackage.Rows.Count; i++)
                {
                    string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                    string strPackage = dtPackage.Rows[i]["Package"].ToString();
                    string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND Package = '" + strPackage
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drPack = dtPackageValue.Select(strFilter);

                    if (drPack.Length != 0)
                    {
                        double[] arrValue = new double[drPack.Length];

                        for (int j = 0; j < drPack.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Quarter_Package(dtQuarter.Year.ToString(), numQuarter, dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion                    

                dtProcCL = clsProcess.mfReadProcessInspectJobCL(strFromDate, strToDate, "T");
                #region ProcessInspectPackageCL
                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strPackage = dtINS.Rows[i]["Package"].ToString();
                    string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND Package = '" + strPackage
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtProcCL.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        DataTable dtMean = new DataTable();
                        dtMean.Columns.Add("Mean");
                        DataTable dtStdDev = new DataTable();
                        dtStdDev.Columns.Add("StdDev");
                        DataTable dtDataRange = new DataTable();
                        dtDataRange.Columns.Add("DataRange");

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            DataRow drMean = dtMean.NewRow();
                            DataRow drDataRange = dtDataRange.NewRow();
                            DataRow drStdDev = dtStdDev.NewRow();

                            drMean["Mean"] = drINS[j]["Mean"].ToString();
                            drDataRange["DataRange"] = drINS[j]["DataRange"].ToString();
                            drStdDev["StdDev"] = drINS[j]["StdDev"].ToString();

                            dtMean.Rows.Add(drMean);
                            dtDataRange.Rows.Add(drDataRange);
                            dtStdDev.Rows.Add(drStdDev);
                        }

                        DataTable dtInspectValue = new DataTable();
                        dtInspectValue.Columns.Add("InspectValue");

                        DataRow[] drArrInspectValue = dtInpect.Select(strFilter);

                        for (int j = 0; j < drArrInspectValue.Length; j++)
                        {
                            DataRow drInspectValue = dtInspectValue.NewRow();
                            drInspectValue["InspectValue"] = drArrInspectValue[j]["InspectValue"].ToString();
                            dtInspectValue.Rows.Add(drInspectValue);
                        }

                        STASPC cls = new STASPC();
                        STAControlLimit limit = new STAControlLimit();
                        limit.InitSTAControlLimit();
                        limit = cls.mfCalcControlLimit(dtInspectValue, dtMean, dtDataRange, dtStdDev);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob_CL(strPlantCode, strPackage, "", strStackSeq, strGeneration,
                                            strProcessCode, strInspectItemCode, dtQuarter.Year.ToString(), numQuarter, limit);

                        clsProcess.mfSaveProcessInspectCL(dtValue, ip.AddressList[0].ToString(), m_strUserID, "T");
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
            }
        }

        private void uButtonYear_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            try
            {
                DateTime dtYear = DateTime.Parse(uDateYear.Value.ToString());
                DateTime dtYearUnit = DateTime.Parse(uDateYearUnit.Value.ToString());

                string strFromDate = dtYear.AddDays(1 - dtYear.DayOfYear).ToString("yyyy-MM-dd")
                    + " " + dtYearUnit.ToString("HH:mm:ss");
                string strToDate = DateTime.Parse(strFromDate).AddYears(1).ToString("yyyy-MM-dd")
                    + " " + GetDate.MinusOneSec(dtYearUnit.ToString("HH:mm:ss"));

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "확인창", "저장확인", "BatchJob을 실행하시겠습니까?",
                            Infragistics.Win.HAlign.Right) == DialogResult.No)
                {
                    return;
                }

                //QRPCOM.QRPGLO.QRPBrowser //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ImportInspectJob), "ImportInspectJob");
                QRPJOB.BL.BATJOB.ImportInspectJob clsImport = new QRPJOB.BL.BATJOB.ImportInspectJob();
                //brwChannel.mfCredentials(clsImport);
                DataTable dtINS = new DataTable();
                DataTable dtInpect = clsImport.mfReadImportInspectJob(strFromDate, strToDate, ref dtINS);

                #region ImportInspect


                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strVendorCode = dtINS.Rows[i]["VendorCode"].ToString();
                    string strMaterialCode = dtINS.Rows[i]["MaterialCode"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND VendorCode = '" + strVendorCode
                                    + "' AND MaterialCode = '" + strMaterialCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ImportInspectJob(strPlantCode, strMaterialCode, strVendorCode, strInspectItemCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsImport.mfSaveImportInspectJob_Year(strFromDate.Substring(0, 4), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                        
                    }
                }

                #endregion

                //brwChannel = new //QRPCOM.QRPGLO.QRPBrowser();

                //brwChannel.mfRegisterChannel(typeof(QRPJOB.BL.BATJOB.ProcessInspectJob), "ProcessInspectJob");
                QRPJOB.BL.BATJOB.ProcessInspectJob clsProcess = new QRPJOB.BL.BATJOB.ProcessInspectJob();
                //brwChannel.mfCredentials(clsProcess);
                dtINS = new DataTable();
                dtInpect = new DataTable();
                DataTable dtPackage = new DataTable();
                DataTable dtPackageValue = new DataTable();
                dtInpect = clsProcess.mfReadProcessInspectJob(strFromDate, strToDate, ref dtINS, ref dtPackage, ref dtPackageValue);

                #region ProcessInspect

                for (int i = 0; i < dtINS.Rows.Count; i++)
                {
                    string strPlantCode = dtINS.Rows[i]["PlantCode"].ToString();
                    string strProductCode = dtINS.Rows[i]["ProductCode"].ToString();
                    string strStackSeq = dtINS.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtINS.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtINS.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtINS.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtINS.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtINS.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtINS.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtINS.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtINS.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtINS.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND ProductCode = '" + strProductCode
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drINS = dtInpect.Select(strFilter);

                    if (drINS.Length != 0)
                    {
                        double[] arrValue = new double[drINS.Length];

                        for (int j = 0; j < drINS.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drINS[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();
                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob(strPlantCode, strProductCode, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Year(strFromDate.Substring(0, 4), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion

                #region ProcessInspectCL
                //STASPC cls = new STASPC();
                //cls.mfCalcControlLimit( "MeasureValue", "ReqItemMean", "ReqItemDataRange", "ReqItemStdDev")
                #endregion

                #region ProcessInspectPackage
                for (int i = 0; i < dtPackage.Rows.Count; i++)
                {
                    string strPlantCode = dtPackage.Rows[i]["PlantCode"].ToString();
                    string strPackage = dtPackage.Rows[i]["Package"].ToString();
                    string strStackSeq = dtPackage.Rows[i]["StackSeq"].ToString();
                    string strGeneration = dtPackage.Rows[i]["Generation"].ToString();
                    string strInspectItemCode = dtPackage.Rows[i]["InspectItemCode"].ToString();
                    string strProcessCode = dtPackage.Rows[i]["ProcessCode"].ToString();
                    string strStdNumBer = dtPackage.Rows[i]["StdNumBer"].ToString();
                    string strStdSeq = dtPackage.Rows[i]["StdSeq"].ToString();
                    int numVersionNum = Convert.ToInt32(dtPackage.Rows[i]["VersionNum"]);
                    double numUpperSpec = Convert.ToDouble(dtPackage.Rows[i]["UpperSpec"]);
                    double numLoweSpec = Convert.ToDouble(dtPackage.Rows[i]["LowerSpec"]);
                    string strSpecRange = dtPackage.Rows[i]["SpecRange"].ToString();

                    string strFilter = "PlantCode = '" + strPlantCode
                                    + "' AND Package = '" + strPackage
                                    + "' AND StackSeq = '" + strStackSeq
                                    + "' AND Generation = '" + strGeneration
                                    + "' AND ProcessCode = '" + strProcessCode
                                    + "' AND InspectItemCode = '" + strInspectItemCode + "'";

                    DataRow[] drPack = dtPackageValue.Select(strFilter);

                    if (drPack.Length != 0)
                    {
                        double[] arrValue = new double[drPack.Length];

                        for (int j = 0; j < drPack.Length; j++)
                        {
                            arrValue[j] = Convert.ToDouble(drPack[j]["InspectValue"]);
                        }

                        STASummary clsSum = new STASummary();
                        clsSum.InitSTASummary();

                        STABAS clsBas = new STABAS();

                        clsSum = clsBas.mfCalcSummaryStat(arrValue, numLoweSpec, numUpperSpec, strSpecRange, false, 0, 0);

                        DataTable dtValue = mfMakeDataTable_ProcessInspectJob_Package(strPlantCode, strPackage, strStackSeq, strGeneration, strInspectItemCode, strProcessCode,
                                                            strStdNumBer, strStdSeq, numVersionNum, numUpperSpec, numLoweSpec, strSpecRange,
                                                            clsSum);

                        clsProcess.mfSaveProcessInspectJob_Year_Package(strFromDate.Substring(0, 4), dtValue, ip.AddressList[0].ToString(), m_strUserID);
                    }
                }
                #endregion


                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString() + "\n" +
                    ex.StackTrace.ToString());
            }
            finally
            {
            }
        }
        #endregion

        #region DataTable
        private static DataTable mfMakeDataTable_ImportInspectJob(string strPlantCode, string strMaterialCode, string strVendorCode,
                                           string strInspectItemCode, string strStdNumber, string strStdSeq,
                                           int numVersionNum, double numUpperSpec, double numLowerSpec,
                                           string strSpecRange, STASummary clsSum)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("MaterialCode");
                dtValue.Columns.Add("VendorCode");
                dtValue.Columns.Add("InspectItemCode");
                dtValue.Columns.Add("StdNumber");
                dtValue.Columns.Add("StdSeq");
                dtValue.Columns.Add("VersionNum");
                dtValue.Columns.Add("UpperSpec");
                dtValue.Columns.Add("LowerSpec");
                dtValue.Columns.Add("SpecRange");

                dtValue.Columns.Add("CountDA");
                dtValue.Columns.Add("SumDA");
                dtValue.Columns.Add("MeanDA");
                dtValue.Columns.Add("MinDA");
                dtValue.Columns.Add("MaxDA");
                dtValue.Columns.Add("RangeDA");
                dtValue.Columns.Add("VarianceDA");
                dtValue.Columns.Add("StdDevDA");
                dtValue.Columns.Add("MedianDA");
                dtValue.Columns.Add("CpDA");
                dtValue.Columns.Add("CpkDA");
                dtValue.Columns.Add("CplDA");
                dtValue.Columns.Add("CpuDA");
                dtValue.Columns.Add("CountDR");
                dtValue.Columns.Add("SumDR");
                dtValue.Columns.Add("MeanDR");
                dtValue.Columns.Add("MinDR");
                dtValue.Columns.Add("MaxDR");
                dtValue.Columns.Add("RangeDR");
                dtValue.Columns.Add("VarianceDR");
                dtValue.Columns.Add("StdDevDR");
                dtValue.Columns.Add("MedianDR");
                dtValue.Columns.Add("CpDR");
                dtValue.Columns.Add("CpkDR");
                dtValue.Columns.Add("CplDR");
                dtValue.Columns.Add("CpuDR");
                dtValue.Columns.Add("USLUpperCount");
                dtValue.Columns.Add("LSLLowerCount");
                dtValue.Columns.Add("UpwardRunCount");
                dtValue.Columns.Add("DownwardRunCount");
                dtValue.Columns.Add("UpwardTrendCount");
                dtValue.Columns.Add("DownwardTrendCount");
                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["MaterialCode"] = strMaterialCode;
                drValue["VendorCode"] = strVendorCode;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["StdNumber"] = strStdNumber;
                drValue["StdSeq"] = strStdSeq;
                drValue["VersionNum"] = numVersionNum;
                drValue["UpperSpec"] = numUpperSpec.ToString("#########0.#####");
                drValue["LowerSpec"] = numLowerSpec.ToString("#########0.#####");
                drValue["SpecRange"] = strSpecRange;

                drValue["CountDA"] = clsSum.CountDA;
                drValue["SumDA"] = clsSum.SumDA.ToString("##############0.#####");
                drValue["MeanDA"] = clsSum.MeanDA.ToString("##############0.#####");
                drValue["MinDA"] = clsSum.MinDA.ToString("##############0.#####");
                drValue["MaxDA"] = clsSum.MaxDA.ToString("##############0.#####");
                drValue["RangeDA"] = clsSum.RangeDA.ToString("##############0.#####");
                drValue["VarianceDA"] = clsSum.VarianceDA.ToString("##############0.#####");
                drValue["StdDevDA"] = clsSum.StdDevDA.ToString("##############0.#####");
                drValue["MedianDA"] = clsSum.MedianDA.ToString("##############0.#####");
                drValue["CpDA"] = clsSum.CpDA.ToString("#######0.#####");
                drValue["CpkDA"] = clsSum.CpkDA.ToString("#######0.#####");
                drValue["CplDA"] = clsSum.CplDA.ToString("#######0.#####");
                drValue["CpuDA"] = clsSum.CpuDA.ToString("#######0.#####");
                drValue["CountDR"] = clsSum.CountDR;
                drValue["SumDR"] = clsSum.SumDR.ToString("##############0.#####");
                drValue["MeanDR"] = clsSum.MeanDR.ToString("##############0.#####");
                drValue["MinDR"] = clsSum.MinDR.ToString("##############0.#####");
                drValue["MaxDR"] = clsSum.MaxDR.ToString("##############0.#####");
                drValue["RangeDR"] = clsSum.RangeDR.ToString("##############0.#####");
                drValue["VarianceDR"] = clsSum.VarianceDR.ToString("##############0.#####");
                drValue["StdDevDR"] = clsSum.StdDevDR.ToString("##############0.#####");
                drValue["MedianDR"] = clsSum.MedianDR.ToString("##############0.#####");
                drValue["CpDR"] = clsSum.CpDR.ToString("#######0.#####");
                drValue["CpkDR"] = clsSum.CpkDR.ToString("#######0.#####");
                drValue["CplDR"] = clsSum.CplDR.ToString("#######0.#####");
                drValue["CpuDR"] = clsSum.CpuDR.ToString("#######0.#####");
                drValue["USLUpperCount"] = clsSum.USLUpperCount;
                drValue["LSLLowerCount"] = clsSum.LSLLowerCount;
                drValue["UpwardRunCount"] = clsSum.UpwardRunCount;
                drValue["DownwardRunCount"] = clsSum.DownwardRunCount;
                drValue["UpwardTrendCount"] = clsSum.UpwardTrendCount;
                drValue["DownwardTrendCount"] = clsSum.DownwardTrendCount;
                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                return dtValue;
                throw ex;
            }
            finally
            {

            }
        }

        private static DataTable mfMakeDataTable_ProcessInspectJob(string strPlantCode, string strProductCode, string strStackSeq, string strGeneration,
                                           string strInspectItemCode, string strProcessCode, string strStdNumber, string strStdSeq,
                                           int numVersionNum, double numUpperSpec, double numLowerSpec,
                                           string strSpecRange, STASummary clsSum)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("ProductCode");
                dtValue.Columns.Add("StackSeq");
                dtValue.Columns.Add("Generation");
                dtValue.Columns.Add("InspectItemCode");
                dtValue.Columns.Add("ProcessCode");
                dtValue.Columns.Add("StdNumber");
                dtValue.Columns.Add("StdSeq");
                dtValue.Columns.Add("VersionNum");
                dtValue.Columns.Add("UpperSpec");
                dtValue.Columns.Add("LowerSpec");
                dtValue.Columns.Add("SpecRange");

                dtValue.Columns.Add("CountDA");
                dtValue.Columns.Add("SumDA");
                dtValue.Columns.Add("MeanDA");
                dtValue.Columns.Add("MinDA");
                dtValue.Columns.Add("MaxDA");
                dtValue.Columns.Add("RangeDA");
                dtValue.Columns.Add("VarianceDA");
                dtValue.Columns.Add("StdDevDA");
                dtValue.Columns.Add("MedianDA");
                dtValue.Columns.Add("CpDA");
                dtValue.Columns.Add("CpkDA");
                dtValue.Columns.Add("CplDA");
                dtValue.Columns.Add("CpuDA");
                dtValue.Columns.Add("CountDR");
                dtValue.Columns.Add("SumDR");
                dtValue.Columns.Add("MeanDR");
                dtValue.Columns.Add("MinDR");
                dtValue.Columns.Add("MaxDR");
                dtValue.Columns.Add("RangeDR");
                dtValue.Columns.Add("VarianceDR");
                dtValue.Columns.Add("StdDevDR");
                dtValue.Columns.Add("MedianDR");
                dtValue.Columns.Add("CpDR");
                dtValue.Columns.Add("CpkDR");
                dtValue.Columns.Add("CplDR");
                dtValue.Columns.Add("CpuDR");
                dtValue.Columns.Add("USLUpperCount");
                dtValue.Columns.Add("LSLLowerCount");
                dtValue.Columns.Add("UpwardRunCount");
                dtValue.Columns.Add("DownwardRunCount");
                dtValue.Columns.Add("UpwardTrendCount");
                dtValue.Columns.Add("DownwardTrendCount");
                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["ProductCode"] = strProductCode;
                drValue["StackSeq"] = strStackSeq;
                drValue["Generation"] = strGeneration;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["ProcessCode"] = strProcessCode;
                drValue["StdNumber"] = strStdNumber;
                drValue["StdSeq"] = strStdSeq;
                drValue["VersionNum"] = numVersionNum;
                drValue["UpperSpec"] = numUpperSpec;
                drValue["LowerSpec"] = numLowerSpec;
                drValue["SpecRange"] = strSpecRange;

                drValue["CountDA"] = clsSum.CountDA;
                drValue["SumDA"] = clsSum.SumDA;
                drValue["MeanDA"] = clsSum.MeanDA;
                drValue["MinDA"] = clsSum.MinDA;
                drValue["MaxDA"] = clsSum.MaxDA;
                drValue["RangeDA"] = clsSum.RangeDA;
                drValue["VarianceDA"] = clsSum.VarianceDA;
                drValue["StdDevDA"] = clsSum.StdDevDA;
                drValue["MedianDA"] = clsSum.MedianDA;
                drValue["CpDA"] = clsSum.CpDA;
                drValue["CpkDA"] = clsSum.CpkDA;
                drValue["CplDA"] = clsSum.CplDA;
                drValue["CpuDA"] = clsSum.CpuDA;
                drValue["CountDR"] = clsSum.CountDR;
                drValue["SumDR"] = clsSum.SumDR;
                drValue["MeanDR"] = clsSum.MeanDR;
                drValue["MinDR"] = clsSum.MinDR;
                drValue["MaxDR"] = clsSum.MaxDR;
                drValue["RangeDR"] = clsSum.RangeDR;
                drValue["VarianceDR"] = clsSum.VarianceDR;
                drValue["StdDevDR"] = clsSum.StdDevDR;
                drValue["MedianDR"] = clsSum.MedianDR;
                drValue["CpDR"] = clsSum.CpDR;
                drValue["CpkDR"] = clsSum.CpkDR;
                drValue["CplDR"] = clsSum.CplDR;
                drValue["CpuDR"] = clsSum.CpuDR;
                drValue["USLUpperCount"] = clsSum.USLUpperCount;
                drValue["LSLLowerCount"] = clsSum.LSLLowerCount;
                drValue["UpwardRunCount"] = clsSum.UpwardRunCount;
                drValue["DownwardRunCount"] = clsSum.DownwardRunCount;
                drValue["UpwardTrendCount"] = clsSum.UpwardTrendCount;
                drValue["DownwardTrendCount"] = clsSum.DownwardTrendCount;
                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                return dtValue;
                throw ex;
            }
            finally
            {

            }
        }

        private static DataTable mfMakeDataTable_ProcessInspectJob_Package(string strPlantCode, string strPackage, string strStackSeq, string strGeneration,
                                   string strInspectItemCode, string strProcessCode, string strStdNumber, string strStdSeq,
                                   int numVersionNum, double numUpperSpec, double numLowerSpec,
                                   string strSpecRange, STASummary clsSum)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("Package");
                dtValue.Columns.Add("StackSeq");
                dtValue.Columns.Add("Generation");
                dtValue.Columns.Add("InspectItemCode");
                dtValue.Columns.Add("ProcessCode");
                dtValue.Columns.Add("StdNumber");
                dtValue.Columns.Add("StdSeq");
                dtValue.Columns.Add("VersionNum");
                dtValue.Columns.Add("UpperSpec");
                dtValue.Columns.Add("LowerSpec");
                dtValue.Columns.Add("SpecRange");

                dtValue.Columns.Add("CountDA");
                dtValue.Columns.Add("SumDA");
                dtValue.Columns.Add("MeanDA");
                dtValue.Columns.Add("MinDA");
                dtValue.Columns.Add("MaxDA");
                dtValue.Columns.Add("RangeDA");
                dtValue.Columns.Add("VarianceDA");
                dtValue.Columns.Add("StdDevDA");
                dtValue.Columns.Add("MedianDA");
                dtValue.Columns.Add("CpDA");
                dtValue.Columns.Add("CpkDA");
                dtValue.Columns.Add("CplDA");
                dtValue.Columns.Add("CpuDA");
                dtValue.Columns.Add("CountDR");
                dtValue.Columns.Add("SumDR");
                dtValue.Columns.Add("MeanDR");
                dtValue.Columns.Add("MinDR");
                dtValue.Columns.Add("MaxDR");
                dtValue.Columns.Add("RangeDR");
                dtValue.Columns.Add("VarianceDR");
                dtValue.Columns.Add("StdDevDR");
                dtValue.Columns.Add("MedianDR");
                dtValue.Columns.Add("CpDR");
                dtValue.Columns.Add("CpkDR");
                dtValue.Columns.Add("CplDR");
                dtValue.Columns.Add("CpuDR");
                dtValue.Columns.Add("USLUpperCount");
                dtValue.Columns.Add("LSLLowerCount");
                dtValue.Columns.Add("UpwardRunCount");
                dtValue.Columns.Add("DownwardRunCount");
                dtValue.Columns.Add("UpwardTrendCount");
                dtValue.Columns.Add("DownwardTrendCount");
                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["Package"] = strPackage;
                drValue["StackSeq"] = strStackSeq;
                drValue["Generation"] = strGeneration;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["ProcessCode"] = strProcessCode;
                drValue["StdNumber"] = strStdNumber;
                drValue["StdSeq"] = strStdSeq;
                drValue["VersionNum"] = numVersionNum;
                drValue["UpperSpec"] = numUpperSpec;
                drValue["LowerSpec"] = numLowerSpec;
                drValue["SpecRange"] = strSpecRange;

                drValue["CountDA"] = clsSum.CountDA;
                drValue["SumDA"] = clsSum.SumDA;
                drValue["MeanDA"] = clsSum.MeanDA;
                drValue["MinDA"] = clsSum.MinDA;
                drValue["MaxDA"] = clsSum.MaxDA;
                drValue["RangeDA"] = clsSum.RangeDA;
                drValue["VarianceDA"] = clsSum.VarianceDA;
                drValue["StdDevDA"] = clsSum.StdDevDA;
                drValue["MedianDA"] = clsSum.MedianDA;
                drValue["CpDA"] = clsSum.CpDA;
                drValue["CpkDA"] = clsSum.CpkDA;
                drValue["CplDA"] = clsSum.CplDA;
                drValue["CpuDA"] = clsSum.CpuDA;
                drValue["CountDR"] = clsSum.CountDR;
                drValue["SumDR"] = clsSum.SumDR;
                drValue["MeanDR"] = clsSum.MeanDR;
                drValue["MinDR"] = clsSum.MinDR;
                drValue["MaxDR"] = clsSum.MaxDR;
                drValue["RangeDR"] = clsSum.RangeDR;
                drValue["VarianceDR"] = clsSum.VarianceDR;
                drValue["StdDevDR"] = clsSum.StdDevDR;
                drValue["MedianDR"] = clsSum.MedianDR;
                drValue["CpDR"] = clsSum.CpDR;
                drValue["CpkDR"] = clsSum.CpkDR;
                drValue["CplDR"] = clsSum.CplDR;
                drValue["CpuDR"] = clsSum.CpuDR;
                drValue["USLUpperCount"] = clsSum.USLUpperCount;
                drValue["LSLLowerCount"] = clsSum.LSLLowerCount;
                drValue["UpwardRunCount"] = clsSum.UpwardRunCount;
                drValue["DownwardRunCount"] = clsSum.DownwardRunCount;
                drValue["UpwardTrendCount"] = clsSum.UpwardTrendCount;
                drValue["DownwardTrendCount"] = clsSum.DownwardTrendCount;
                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                return dtValue;
                throw ex;
            }
            finally
            {

            }
        }

        private static DataTable mfMakeDataTable_ImportInspetJob_CL(string strPlantCode, string strMaterialCode, string strVendorCode,
                                            string strInspectItemCode, string strAYear, int intAQuarter, STAControlLimit clsLimit)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("MaterialCode");
                dtValue.Columns.Add("VendorCode");
                dtValue.Columns.Add("InspectItemCode");
                dtValue.Columns.Add("AYear");
                dtValue.Columns.Add("AQuarter");

                dtValue.Columns.Add("XLCL");
                dtValue.Columns.Add("XCL");
                dtValue.Columns.Add("XUCL");
                dtValue.Columns.Add("XBRLCL");
                dtValue.Columns.Add("XBCL");
                dtValue.Columns.Add("XBRUCL");
                dtValue.Columns.Add("XBSLCL");
                dtValue.Columns.Add("XBSUCL");
                
                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["MaterialCode"] = strMaterialCode;
                drValue["VendorCode"] = strVendorCode;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["AYear"] = strAYear;
                drValue["AQuarter"] = intAQuarter.ToString();

                drValue["XLCL"] = clsLimit.XLCL;
                drValue["XCL"] = clsLimit.XCL;
                drValue["XUCL"] = clsLimit.XUCL;
                drValue["XBRLCL"] = clsLimit.XBRLCL;
                drValue["XBCL"] = clsLimit.XBCL;
                drValue["XBRUCL"] = clsLimit.XBRUCL;
                drValue["XBSLCL"] = clsLimit.XBSLCL;
                drValue["XBSUCL"] = clsLimit.XBSUCL;
                                
                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                return dtValue;
                throw ex;
            }
            finally
            {

            }
        }

        private static DataTable mfMakeDataTable_ProcessInspectJob_CL(string strPlantCode, string strPackage, string strProductCode, string strStackSeq, string strGeneration,
                                                    string strProcessCode, string strInspectItemCode, string strAYear, int intAQuarter, STAControlLimit clsLimit)
        {
            DataTable dtValue = new DataTable();
            try
            {
                #region DataTable
                dtValue.Columns.Add("PlantCode");
                dtValue.Columns.Add("ProductCode");
                dtValue.Columns.Add("Package");
                dtValue.Columns.Add("StackSeq");
                dtValue.Columns.Add("Generation");
                dtValue.Columns.Add("ProcessCode");
                dtValue.Columns.Add("InspectItemCode");

                dtValue.Columns.Add("XLCL");
                dtValue.Columns.Add("XCL");
                dtValue.Columns.Add("XUCL");
                dtValue.Columns.Add("XBRLCL");
                dtValue.Columns.Add("XBCL");
                dtValue.Columns.Add("XBRUCL");
                dtValue.Columns.Add("XBSLCL");
                dtValue.Columns.Add("XBSUCL");

                #endregion

                #region DataRow
                DataRow drValue = dtValue.NewRow();

                drValue["PlantCode"] = strPlantCode;
                drValue["ProductCode"] = strProductCode;
                drValue["Package"] = strPackage;
                drValue["StackSeq"] = strStackSeq;
                drValue["Generation"] = strGeneration;
                drValue["ProcessCode"] = strProcessCode;
                drValue["InspectItemCode"] = strInspectItemCode;
                drValue["AYear"] = strAYear;
                drValue["AQuarter"] = intAQuarter.ToString();

                drValue["XLCL"] = clsLimit.XLCL;
                drValue["XCL"] = clsLimit.XCL;
                drValue["XUCL"] = clsLimit.XUCL;
                drValue["XBRLCL"] = clsLimit.XBRLCL;
                drValue["XBCL"] = clsLimit.XBCL;
                drValue["XBRUCL"] = clsLimit.XBRUCL;
                drValue["XBSLCL"] = clsLimit.XBSLCL;
                drValue["XBSUCL"] = clsLimit.XBSUCL;

                dtValue.Rows.Add(drValue);
                #endregion
                return dtValue;
            }
            catch (Exception ex)
            {
                return dtValue;
                throw ex;
            }
            finally
            {

            }
        }
        #endregion
    }
}
