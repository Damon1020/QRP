﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.InteropServices;
using System.Xml;
using QRPDB;


[assembly: ApplicationName("QRPSTS")]
[assembly: ApplicationActivation(ActivationOption.Server)]
[assembly: ApplicationAccessControl(true,
                                    AccessChecksLevel = AccessChecksLevelOption.ApplicationComponent,
                                    Authentication = AuthenticationOption.None,
                                    ImpersonationLevel = ImpersonationLevelOption.Impersonate)]

namespace QRPJOB.BL.BATJOB
{

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("BatchJob")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class BatchJob : ServicedComponent
    {
        public DataTable mfDataSetInfo()
        {
            DataTable dtJobMaster = new DataTable();

            try
            {
                dtJobMaster.Columns.Add("TimerCode", typeof(string));
                dtJobMaster.Columns.Add("TimerGubun", typeof(string));
                dtJobMaster.Columns.Add("TickTime", typeof(string));
                dtJobMaster.Columns.Add("UnitTime", typeof(string));
                dtJobMaster.Columns.Add("UseFlag", typeof(string));

                return dtJobMaster;
            }
            catch (Exception ex)
            {
                return dtJobMaster;
                throw (ex);
            }
            finally
            {
            }
        }

        [AutoComplete]
        public DataTable mfReadJOBMaster()
        {
            SQLS sql = new SQLS();
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSJOBMaster", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public string mfSaveJOBMaster(DataTable dtJobMaster, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                for (int i = 0; i < dtJobMaster.Rows.Count; i++)
                { 
                    //파라미터 저장
                    DataTable dtParameter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParameter, "@i_strTimerCode", ParameterDirection.Input, SqlDbType.VarChar, dtJobMaster.Rows[i]["TimerCode"].ToString(), 10);
                    sql.mfAddParamDataRow(dtParameter, "@i_strTimerGubun", ParameterDirection.Input, SqlDbType.VarChar, dtJobMaster.Rows[i]["TimerGubun"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParameter, "@i_strTickTime", ParameterDirection.Input, SqlDbType.VarChar, dtJobMaster.Rows[i]["TickTime"].ToString(), 8);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUnitTime", ParameterDirection.Input, SqlDbType.VarChar, dtJobMaster.Rows[i]["UnitTime"].ToString(), 8);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUseFlag", ParameterDirection.Input, SqlDbType.VarChar, dtJobMaster.Rows[i]["UseFlag"].ToString(), 1);
                     
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                    

                    sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSJOBMaster", dtParameter);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 값 처리
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                trans.Commit();
                
                return strErrRtn;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        public DataTable mfDataSetInfoMDM()
        {
            DataTable dtMDM = new DataTable();

            try
            {
                dtMDM.Columns.Add("InterfaceID", typeof(string));
                dtMDM.Columns.Add("UseFlag", typeof(string));
                dtMDM.Columns.Add("InterfaceName", typeof(string));
                dtMDM.Columns.Add("SPName", typeof(string));
                dtMDM.Columns.Add("IF_VIEW", typeof(string));
                dtMDM.Columns.Add("EtcDesc", typeof(string));
                dtMDM.Columns.Add("DeleteFlag", typeof(string));

                return dtMDM;
            }
            catch (Exception ex)
            {
                return dtMDM;
                throw (ex);
            }
            finally
            {
            }
        }

        [AutoComplete]
        public DataTable mfReadMDMIFMaster()
        {
            SQLS sql = new SQLS();
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMDMIFMaster", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public string mfSaveMDMIFMaster(DataTable dtMDM, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                for (int i = 0; i < dtMDM.Rows.Count; i++)
                {
                    //파라미터 저장
                    DataTable dtParameter = sql.mfSetParamDataTable();
                    sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParameter, "@i_strInterfaceID", ParameterDirection.Input, SqlDbType.VarChar, dtMDM.Rows[i]["InterfaceID"].ToString(), 50);
                    sql.mfAddParamDataRow(dtParameter, "@I_strUseFlag", ParameterDirection.Input, SqlDbType.VarChar, dtMDM.Rows[i]["UseFlag"].ToString(), 1);
                    sql.mfAddParamDataRow(dtParameter, "@I_strInterfaceName", ParameterDirection.Input, SqlDbType.VarChar, dtMDM.Rows[i]["InterFaceName"].ToString(), 8000);
                    sql.mfAddParamDataRow(dtParameter, "@i_strSPName", ParameterDirection.Input, SqlDbType.VarChar, dtMDM.Rows[i]["SPName"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParameter, "@i_strIF_VIEW", ParameterDirection.Input, SqlDbType.VarChar, dtMDM.Rows[i]["IF_VIEW"].ToString(), 1000);
                    sql.mfAddParamDataRow(dtParameter, "@i_strEtcDesc", ParameterDirection.Input, SqlDbType.NVarChar, dtMDM.Rows[i]["EtcDesc"].ToString(), 2000);
                    sql.mfAddParamDataRow(dtParameter, "@i_strDeleteFlag", ParameterDirection.Input, SqlDbType.NVarChar, dtMDM.Rows[i]["DeleteFlag"].ToString(), 1);

                    sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);


                    sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                    //프로시저 실행
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_SYSMDMIFMaster", dtParameter);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    //처리결과 값 처리
                    if (ErrRtn.ErrNum != 0)
                    {
                        trans.Rollback();
                        return strErrRtn;
                    }
                }

                trans.Commit();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("ImportInspectJob")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ImportInspectJob : ServicedComponent
    {
        //private SqlConnection m_SqlConnDebug;
        //public ImportInspectJob()
        //{
        //    m_SqlConnDebug = new SqlConnection("data source=10.60.24.173;user id=QRPUSR;password=jinjujin; Initial Catalog=QRP_STS_DEV;persist security info=true");
        //    m_SqlConnDebug.Open();
        //}

        [AutoComplete]
        public DataTable mfReadImportInspectJob(string strFromDate, string strToDate, ref DataTable dtINS)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 30);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 30);
                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSImportInspectJob_Value", dtParame);
                dtINS = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSImportInspectJob", dtParame);

                //dtEquip = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSImportInspectJob_Value", dtParame);
                //dtINS = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSImportInspectJob", dtParame);
                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
            }
        }

        [AutoComplete]
        public DataTable mfReadImportInspectJob_CL(string strFromDate, string strToDate)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 30);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 30);
                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSImportInspectJob_CL", dtParame);
                
                //dtEquip = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSImportInspectJob_Value", dtParame);
                //dtINS = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_INSImportInspectJob", dtParame);
                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveImportInspectJob_Day(string strADate, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["MaterialCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["VendorCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strADate", ParameterDirection.Input, SqlDbType.VarChar, strADate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSImportStaticD", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch(Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveImportInspectJob_Week(string strAYear, string strAMonth, string strAWeek, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["MaterialCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["VendorCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAMonth", ParameterDirection.Input, SqlDbType.VarChar, strAMonth, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAWeek", ParameterDirection.Input, SqlDbType.VarChar, strAWeek, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSImportStaticD", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveImportInspectJob_Month(string strAMonth, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["MaterialCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["VendorCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strAMonth", ParameterDirection.Input, SqlDbType.VarChar, strAMonth, 7);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSImportStaticM", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveImportInspectJob_Quarter(string strAYear, int numQuarter, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["MaterialCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["VendorCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intAQuarter", ParameterDirection.Input, SqlDbType.Int, numQuarter.ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSImportStaticQ", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveImportInspectJob_Quarter_CL(DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["MaterialCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["VendorCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["AYear"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intAQuarter", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["AQuarter"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_numXLCL", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["XLCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXUCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XUCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBRLCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBRLCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBRUCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBRUCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBSLCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBSLCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBSUCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBSUCL"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSImportStaticCL", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveImportInspectJob_Year(string strAYear, DataTable dtSum, string strUserIP, string strUserID)
        {
            string strErrRtn = "";

            //strErrRtn = "0";
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString.ToString());    //디버깅용

            TransErrRtn ErrRtn = new TransErrRtn();
            

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();
            //SqlTransaction trans = m_SqlConnDebug.BeginTransaction();
            try
            {
                //strErrRtn = "1";
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strMaterialCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["MaterialCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strVendorCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["VendorCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 4);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion
                //strErrRtn = "2";
                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSImportStaticY", dtParameter);
                //strErrRtn = sql.mfExecTransStoredProc(m_SqlConnDebug, trans, "up_Update_INSImportStaticY", dtParameter); //up_Update_INSImportStaticY
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                //return strErrRtn;
                //throw (ex);
                //trans.Commit();
                //ErrRtn.SystemInnerException = ex.InnerException.ToString();
                //ErrRtn.SystemMessage = ex.Message;
                //ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("ProcessInspectJob")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class ProcessInspectJob : ServicedComponent
    {
        [AutoComplete]
        public DataTable mfReadProcessInspectJob(string strFromDate, string strToDate, ref DataTable dtINS, ref DataTable dtPackage, ref DataTable dtPackageValue)
        {
            SQLS sql = new SQLS();
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 30);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 30);
                //프로시저 실행
                
                dtINS = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessInspectJob", dtParame);
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessInspectJob_Value", dtParame);

                dtPackage = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessInspectJob_Package", dtParame);
                dtPackageValue = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessInspectJob_PackageValue", dtParame); 

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
            }
        }

        [AutoComplete]
        public DataTable mfReadProcessInspectJobCL(string strFromDate, string strToDate, string strPackageFlag)
        {
            SQLS sql = new SQLS();
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParame, "@i_strFromDate", ParameterDirection.Input, SqlDbType.VarChar, strFromDate, 30);
                sql.mfAddParamDataRow(dtParame, "@i_strToDate", ParameterDirection.Input, SqlDbType.VarChar, strToDate, 30);
                sql.mfAddParamDataRow(dtParame, "@i_strPackageFlag", ParameterDirection.Input, SqlDbType.VarChar, strPackageFlag, 1);
                //프로시저 실행

                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_INSProcessInspectJobCL", dtParame);
                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
            }
        }

        #region ProcessInspect
        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Day(string strADate, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProductCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strADate", ParameterDirection.Input, SqlDbType.VarChar, strADate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticD", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Week(string strAYear, string strAMonth, string strAWeek, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProductCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 4);
                sql.mfAddParamDataRow(dtParameter, "@i_strAMonth", ParameterDirection.Input, SqlDbType.VarChar, strAMonth, 2);
                sql.mfAddParamDataRow(dtParameter, "@i_strAWeek", ParameterDirection.Input, SqlDbType.Int, strAWeek);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticW", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Month(string strAMonth, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProductCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAMonth", ParameterDirection.Input, SqlDbType.VarChar, strAMonth, 7);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticM", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Quarter(string strAYear, int numQuarter, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProductCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StatckSeq"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@I_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intAQuarter", ParameterDirection.Input, SqlDbType.Int, numQuarter.ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticQ", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Year(string strAYear, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProductCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 4);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticY", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
        #endregion

        #region ProcessInspectPackage
        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Day_Package(string strADate, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Package"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strADate", ParameterDirection.Input, SqlDbType.VarChar, strADate, 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticDPackage", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Week_Package(string strAYear, string strAMonth, string strAWeek, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Package"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 4);
                sql.mfAddParamDataRow(dtParameter, "@i_strAMonth", ParameterDirection.Input, SqlDbType.VarChar, strAMonth, 2);
                sql.mfAddParamDataRow(dtParameter, "@i_strAWeek", ParameterDirection.Input, SqlDbType.Int, strAWeek);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticWPackage", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Month_Package(string strAMonth, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Package"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAMonth", ParameterDirection.Input, SqlDbType.VarChar, strAMonth, 7);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticMPackage", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Quarter_Package(string strAYear, int numQuarter, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Package"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intAQuarter", ParameterDirection.Input, SqlDbType.Int, numQuarter.ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticQPackage", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public void mfSaveProcessInspectJob_Year_Package(string strAYear, DataTable dtSum, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Package"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, strAYear, 4);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdNumber", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdNumber"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strStdSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StdSeq"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intVersionNum", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["VersionNum"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numUpperSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["UpperSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numLowerSpec", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["LowerSpec"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_strSpecRange", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["SpecRange"].ToString(), 50);

                sql.mfAddParamDataRow(dtParameter, "@i_intCountDA", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDA", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDA"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intCountDR", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["CountDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numSumDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["SumDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMeanDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MeanDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMinDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MinDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMaxDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MaxDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numRangeDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["RangeDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numVarianceDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["VarianceDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numStdDevDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["StdDevDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numMedianDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["MedianDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpkDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpkDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCplDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CplDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numCpuDR", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["CpuDR"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUSLUpperCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["USLUpperCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intLSLLowerCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["LSLLowerCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardRunCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardRunCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intUpwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["UpwardTrendCount"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_intDownwardTrendCount", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["DownwardTrendCount"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticYPackage", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
        #endregion

        #region ProcessInspect CL
        [AutoComplete(false)]
        public void mfSaveProcessInspectCL(DataTable dtSum, string strUserIP, string strUserID, string strPackageFlag)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                #region Parameter
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strPlantCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["PlantCode"].ToString(), 10);

                if(strPackageFlag.Equals("F"))
                    sql.mfAddParamDataRow(dtParameter, "@i_strProductCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProductCode"].ToString(), 20);
                else
                    sql.mfAddParamDataRow(dtParameter, "@i_strPackage", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Package"].ToString(), 20);

                sql.mfAddParamDataRow(dtParameter, "@i_strStackSeq", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["StackSeq"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strGeneration", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["Generation"].ToString(), 40);
                sql.mfAddParamDataRow(dtParameter, "@i_strInspectItemCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["InspectItemCode"].ToString(), 20);
                sql.mfAddParamDataRow(dtParameter, "@i_strProcessCode", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["ProcessCode"].ToString(), 10);
                sql.mfAddParamDataRow(dtParameter, "@i_strAYear", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["AYear"].ToString(), 4);
                sql.mfAddParamDataRow(dtParameter, "@i_intAQuarter", ParameterDirection.Input, SqlDbType.VarChar, dtSum.Rows[0]["AQuarter"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_numXLCL", ParameterDirection.Input, SqlDbType.Int, dtSum.Rows[0]["XLCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXUCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XUCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBRLCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBRLCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBRUCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBRUCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBSLCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBSLCL"].ToString());
                sql.mfAddParamDataRow(dtParameter, "@i_numXBSUCL", ParameterDirection.Input, SqlDbType.Decimal, dtSum.Rows[0]["XBSUCL"].ToString());

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                #endregion

                //프로시저 실행
                if(strPackageFlag.Equals("F"))
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticCL", dtParameter);
                else
                    strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, trans, "up_Update_INSProcessStaticPackageCL", dtParameter);

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //처리결과 값 처리
                trans.Commit();

                //return strErrRtn;
            }

            catch (Exception ex)
            {
                trans.Commit();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
            }
            finally
            {
                sql.mfDisConnect();
            }
        }
        #endregion

    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("MDM")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MDM : ServicedComponent
    {
        private SqlConnection m_SqlConnDebug;

        public MDM()
        {
            //sql = new SQLS();
            //sql.mfConnect();
        }
        public MDM(string strDBConn)
        {
            m_SqlConnDebug = new SqlConnection(strDBConn);

            m_SqlConnDebug.Open();
        }

        [AutoComplete(false)]
        public string mfSaveMDMIF(string strSPName, string strIF_ID, string strIF_VIEW, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString);

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();

                sql.mfAddParamDataRow(dtParameter, "@i_strIF_ID", ParameterDirection.Input, SqlDbType.VarChar, strIF_ID, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strIF_VIEW", ParameterDirection.Input, SqlDbType.VarChar, strIF_VIEW, 100);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, strSPName, dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                sql.mfDisConnect();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                //trans.Rollback();
                sql.mfDisConnect();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.InterfaceResultMessage = ex.Source.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public string mfSaveMDMIF_Total(DataTable dtVar, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString);

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            try
            {
                string strIF_ID = string.Empty;
                string strIF_VIEW = string.Empty;
                string strSPName = string.Empty;

                for (int i = 0; i < dtVar.Rows.Count; i++)
                {
                    if (!sql.mfConnect())
                        sql.mfConnect();

                    strIF_ID = dtVar.Rows[i]["IF_ID"].ToString();
                    strIF_VIEW = dtVar.Rows[i]["IF_VIEW"].ToString();
                    strSPName = dtVar.Rows[i]["SPName"].ToString();

                    DataTable dtParameter = sql.mfSetParamDataTable();

                    sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                    sql.mfAddParamDataRow(dtParameter, "@i_strIF_ID", ParameterDirection.Input, SqlDbType.VarChar, strIF_ID, 100);
                    sql.mfAddParamDataRow(dtParameter, "@i_strIF_VIEW", ParameterDirection.Input, SqlDbType.VarChar, strIF_VIEW, 100);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                    sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                    sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                    //프로시저 실행
                    strErrRtn = mfExecTransStoredProc(sql.SqlCon, strSPName, dtParameter);
                    //strErrRtn = mfExecTransStoredProc(m_SqlConnDebug, strSPName, dtParameter);
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    if (!ErrRtn.ErrNum.Equals(0))
                    {
                        break;
                    }
                }
                //파라미터 저장

                //sql.mfDisConnect();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                //trans.Rollback();
                //sql.mfDisConnect();
                //ErrRtn.SystemInnerException = ex.InnerException.ToString();
                //ErrRtn.InterfaceResultMessage = ex.Source.ToString();
                //ErrRtn.SystemMessage = ex.Message;
                //ErrRtn.SystemStackTrace = ex.StackTrace;
                //return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                sql.mfDisConnect();
            }            
        }
        
        [AutoComplete]
        public DataTable mfReadMDMIF()
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString);
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMDMIF", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_SYSMDMIF", dtParame);

                //정보리턴
                return dtEquip;
            }
            catch (Exception ex)
            {
                return dtEquip;
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
            }
        }

        [AutoComplete(false)]
        public string mfReadMDIF_Total(string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();
            //SQLS sql = new SQLS(m_SqlConnDebug.ConnectionString);
            DataTable dtEquip = new DataTable();
            try
            {
                //디비연결
                sql.mfConnect();

                //파라미터 저장
                DataTable dtParame = sql.mfSetParamDataTable();

                //프로시저 실행
                dtEquip = sql.mfExecReadStoredProc(sql.SqlCon, "up_Select_SYSMDMIF", dtParame);
                //dtEquip = sql.mfExecReadStoredProc(m_SqlConnDebug, "up_Select_SYSMDMIF", dtParame);

                if (!dtEquip.Rows.Count.Equals(0))
                {
                    TransErrRtn ErrRtn = new TransErrRtn();
                    string strErrRtn = "";
                    string strIF_ID = string.Empty;
                    string strIF_VIEW = string.Empty;
                    string strSPName = string.Empty;

                    for (int i = 0; i < dtEquip.Rows.Count; i++)
                    {
                        //if (!sql.mfConnect())
                        //    sql.mfConnect();

                        strIF_ID = dtEquip.Rows[i]["IF_ID"].ToString();
                        strIF_VIEW = dtEquip.Rows[i]["IF_VIEW"].ToString();
                        strSPName = dtEquip.Rows[i]["SPName"].ToString();

                        DataTable dtParameter = sql.mfSetParamDataTable();

                        sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                        sql.mfAddParamDataRow(dtParameter, "@i_strIF_ID", ParameterDirection.Input, SqlDbType.VarChar, strIF_ID, 100);
                        sql.mfAddParamDataRow(dtParameter, "@i_strIF_VIEW", ParameterDirection.Input, SqlDbType.VarChar, strIF_VIEW, 100);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                        sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);

                        sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);
                        //프로시저 실행
                        strErrRtn = mfExecTransStoredProc(sql.SqlCon, strSPName, dtParameter);
                        //strErrRtn = mfExecTransStoredProc(m_SqlConnDebug, strSPName, dtParameter);
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                        if (!ErrRtn.ErrNum.Equals(0))
                        {
                            break;
                        }
                    }
                    return strErrRtn;
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                
                throw (ex);
            }
            finally
            {
                //디비종료
                sql.mfDisConnect();
            }
        }

        public string mfExecTransStoredProc(SqlConnection Conn, string strSPName, DataTable dtSPParameter)
        {
            TransErrRtn Result = new TransErrRtn();
            string strErrRtn = "";
            try
            {
                if (Conn.State != ConnectionState.Open)
                {
                    Conn.Close();
                    Conn.Open();
                }
                //if (mfConnect() == true)
                //if (Conn.State == ConnectionState.Open)
                //{
                    SqlCommand m_SqlCmd = new SqlCommand();
                    m_SqlCmd.Connection = Conn;
                    //m_SqlCmd.Transaction = Trans;
                    m_SqlCmd.CommandType = CommandType.StoredProcedure;
                    m_SqlCmd.CommandText = strSPName;
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        SqlParameter param = new SqlParameter();
                        param.ParameterName = dr["ParamName"].ToString();
                        param.Direction = (ParameterDirection)dr["ParamDirect"];
                        param.SqlDbType = (SqlDbType)dr["DBType"];
                        param.IsNullable = true;

                        //if (dr["Value"].ToString() != "")
                        param.Value = dr["Value"].ToString();

                        if (dr["Length"].ToString() != "")
                        {
                            if (Convert.ToInt32(dr["Length"].ToString()) > 0)
                                param.Size = Convert.ToInt32(dr["Length"].ToString());
                        }

                        m_SqlCmd.Parameters.Add(param);
                    }

                    string strReturn = Convert.ToString(m_SqlCmd.ExecuteScalar());
                    //m_SqlCmd.ExecuteNonQuery();

                    //처리 결과를 구조체 변수에 저장시킴
                    Result.ErrNum = Convert.ToInt32(m_SqlCmd.Parameters["@Rtn"].Value);
                    Result.ErrMessage = m_SqlCmd.Parameters["@ErrorMessage"].Value.ToString();
                    //Output Param이 있는 경우 ArrayList에 저장시킴.
                    Result.mfInitReturnValue();
                    foreach (DataRow dr in dtSPParameter.Rows)
                    {
                        if (dr["ParamName"].ToString() != "@ErrorMessage" && (ParameterDirection)dr["ParamDirect"] == ParameterDirection.Output)
                        {
                            Result.mfAddReturnValue(m_SqlCmd.Parameters[dr["ParamName"].ToString()].Value.ToString());
                        }
                    }
                    strErrRtn = Result.mfEncodingErrMessage(Result);
                    return strErrRtn;
                //}
                //else
                //{
                //    Result.ErrNum = 99;
                //    Result.ErrMessage = "DataBase 연결되지 않았습니다.";
                //    strErrRtn = Result.mfEncodingErrMessage(Result);
                //    return strErrRtn;
                //}

            }
            catch (Exception ex)
            {
                Result.ErrNum = 99;
                Result.ErrMessage = ex.Message;
                Result.SystemMessage = ex.Message;
                Result.SystemStackTrace = ex.StackTrace;
                Result.SystemInnerException = ex.InnerException.ToString();
                strErrRtn = Result.mfEncodingErrMessage(Result);
                return strErrRtn;
            }
            finally
            {
            }
        }        
    }

    [EventTrackingEnabled(true)]
    [JustInTimeActivation(true)]
    [ConstructionEnabled(true, Default = "None")]
    [Transaction(TransactionOption.Supported)]
    [ObjectPooling(true)]
    [Serializable]
    [System.EnterpriseServices.Description("MESIF")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComponentAccessControl(true), SecurityRole("QRPService", true)]
    public class MESIF : ServicedComponent
    {
        [AutoComplete(false)]
        public string mfSaveMESIFErrorLog(string strFormName, string xmlSendMessage, string xmlReplyMessage, string strReturnCode, string strReturnMessage, string strUserIP, string strUserID)
        {
            SQLS sql = new SQLS();

            TransErrRtn ErrRtn = new TransErrRtn();
            string strErrRtn = "";

            sql.mfConnect();
            //Transaction시작
            //SqlTransaction trans = sql.SqlCon.BeginTransaction();

            try
            {
                //파라미터 저장
                DataTable dtParameter = sql.mfSetParamDataTable();
                sql.mfAddParamDataRow(dtParameter, "@Rtn", ParameterDirection.ReturnValue, SqlDbType.VarChar, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_strFormName", ParameterDirection.Input, SqlDbType.VarChar, strFormName, 100);

                sql.mfAddParamDataRow(dtParameter, "@i_xmlSendMessage", ParameterDirection.Input, SqlDbType.VarChar, xmlSendMessage, 8000);
                sql.mfAddParamDataRow(dtParameter, "@i_xmlReplyMessage", ParameterDirection.Input, SqlDbType.VarChar, xmlReplyMessage, 8000);

                sql.mfAddParamDataRow(dtParameter, "@i_strReturnCode", ParameterDirection.Input, SqlDbType.VarChar, strReturnCode, 50);
                sql.mfAddParamDataRow(dtParameter, "@i_strReturnMessage", ParameterDirection.Input, SqlDbType.VarChar, strReturnMessage, 50);

                sql.mfAddParamDataRow(dtParameter, "@i_strUserIP", ParameterDirection.Input, SqlDbType.VarChar, strUserIP, 15);
                sql.mfAddParamDataRow(dtParameter, "@i_strUserID", ParameterDirection.Input, SqlDbType.VarChar, strUserID, 20);
                sql.mfAddParamDataRow(dtParameter, "@ErrorMessage", ParameterDirection.Output, SqlDbType.VarChar, 8000);

                //프로시저 실행
                strErrRtn = sql.mfExecTransStoredProc(sql.SqlCon, "up_Update_SYSMESIFErrorLog", dtParameter);
                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //처리결과 값 처리
                //if (ErrRtn.ErrNum != 0)
                //{
                //    trans.Rollback();
                //    return strErrRtn;
                //}
                //trans.Commit();

                return strErrRtn;

            }
            catch (Exception ex)
            {
                //trans.Commit();

                //trans.Rollback();
                ErrRtn.SystemInnerException = ex.InnerException.ToString();
                ErrRtn.SystemMessage = ex.Message;
                ErrRtn.SystemStackTrace = ex.StackTrace;
                return ErrRtn.mfEncodingErrMessage(ErrRtn);
                throw (ex);
            }
            finally
            {
                //sql.mfDisConnect();
            }
        }
    }
}


