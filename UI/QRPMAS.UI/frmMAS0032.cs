﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0032.cs                                         */
/* 프로그램명   : 거래처정보                                            */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMAS0032 : Form,IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();


        public frmMAS0032()
        {
            InitializeComponent();
        }

        private void frmMAS0032_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0032_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            this.titleArea.mfSetLabelText("거래처정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤초기화
            SetToolAuth();
            InitButton();
            InitLabel();
            InitGrid();
            InitGroupBox();
            uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화 및 타이틀설정
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //버튼설정
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonDeleteRow.Click += new EventHandler(uButtonDeleteRow_Click);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchVendorCode, "거래처코드", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchVendorName, "거래처명", m_resSys.GetString("SYS_FONFNEMAE"), true, false);

                wLabel.mfSetLabel(this.uLabelVendorCode, "거래처코드", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelVendorName, "거래처명", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelVendorNameCh, "거래처명_중문", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelVendorNameEn, "거래처명_영문", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelRegNo, "사업자등록번호", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelBossName, "대표자명", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelAddress, "사업장주소", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelTel, "사업장전화번호", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelFax, "사업장Fax번호", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                // 거래처정보
                // 일반설정
                wGrid.mfInitGeneralGrid(uGridVendor, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "VendorCode", "거래처코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "VendorName", "거래처명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "PersonName", "품질담당자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "PTel", "담당자 전화번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "EMail", "E-Mail", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "VendorNameCh", "거래처명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "VendorNameEn", "거래처명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "BossName", "대표자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "Tel", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendor, 0, "Address", "주소", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 거래처담당자정보
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridVendorP, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0,
                     Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "DeptName", "업체부서명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "PersonName", "품질담당자명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "Position", "직위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "Tel", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "Hp", "핸드폰", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "Email", "E-mail", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridVendorP, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridVendor.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridVendor.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridVendorP.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridVendorP.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridVendorP, 0);

                this.uGridVendor.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridVendor_DoubleClickRow);
                this.uGridVendorP.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridVendorP_CellChange);
                this.uGridVendorP.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridVendorP_AfterCellUpdate);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();
                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "거래처담당자정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바기능
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //거래처 정보 저장
                string strVendorCode = this.uTextSearchVendorCode.Text;
                string strVendorName = this.uTextSearchVendorName.Text;

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                WinMessageBox msg = new WinMessageBox();
                // PrograssBar 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                DataTable dtVendor = clsVendor.mfReadVendor(strVendorCode,strVendorName,m_resSys.GetString("SYS_LANG"));
                this.uGridVendor.DataSource = dtVendor;
                this.uGridVendor.DataBind();

                // PrograssBar 종료
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // ContentGroupBox 접은상태로
                this.uGroupBoxContentsArea.Expanded = false;

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
               
                if (dtVendor.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridVendor, 0);

                    //for (int i = 0; i < this.uGridVendor.Rows.Count - 1; i++)
                    //{
                    //    if (this.uGridVendor.Rows[i].Cells["VendorCode"].Value.ToString().Equals(this.uGridVendor.Rows[i + 1].Cells["VendorCode"].Value.ToString())
                    //        && this.uGridVendor.Rows[i].Cells["VendorName"].Value.ToString().Equals(this.uGridVendor.Rows[i + 1].Cells["VendorName"].Value.ToString()))
                    //    {
                    //        this.uGridVendor.DisplayLayout.Bands[0].Columns["VendorName"].MergedCellStyle = Infragistics.Win.UltraWinGrid.MergedCellStyle.OnlyWhenSorted;
                    //    }
                    //    else
                    //    {
                    //        this.uGridVendor.DisplayLayout.Bands[0].Columns["VendorName"].MergedCellStyle = Infragistics.Win.UltraWinGrid.MergedCellStyle.Never;
                    //    }
                    //}


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001027", "M001028", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                    QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                    brwChannel.mfCredentials(clsVendor);

                    String strVendorCode = this.uTextVendorCode.Text;
                    DataTable dtSaveVenP = clsVendor.mfSetDataInfo();
                    DataRow row;

                    this.uGridVendorP.ActiveCell = this.uGridVendorP.Rows[0].Cells[0];

                    // 필수 입력사항 확인
                    for (int i = 0; i < this.uGridVendorP.Rows.Count; i++)
                    {
                        if (this.uGridVendorP.Rows[i].Hidden == false)
                        {
                            ////if (this.uGridVendorP.Rows[i].RowSelectorAppearance.Image != null)
                            ////{
                            if (this.uGridVendorP.Rows[i].Cells["PersonName"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", this.uGridVendorP.Rows[i].RowSelectorNumber + "M000574", Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridVendorP.ActiveCell = this.uGridVendorP.Rows[i].Cells["PersonName"];
                                this.uGridVendorP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else
                            {
                                row = dtSaveVenP.NewRow();
                                row["VendorCode"] = this.uTextVendorCode.Text;
                                row["Seq"] = this.uGridVendorP.Rows[i].RowSelectorNumber;
                                row["DeptName"] = this.uGridVendorP.Rows[i].Cells["DeptName"].Value.ToString();
                                row["PersonName"] = this.uGridVendorP.Rows[i].Cells["PersonName"].Value.ToString();
                                row["Position"] = this.uGridVendorP.Rows[i].Cells["Position"].Value.ToString();
                                row["Tel"] = this.uGridVendorP.Rows[i].Cells["Tel"].Value.ToString();
                                row["Hp"] = this.uGridVendorP.Rows[i].Cells["Hp"].Value.ToString();
                                row["EMail"] = this.uGridVendorP.Rows[i].Cells["EMail"].Value.ToString();
                                row["EtcDesc"] = this.uGridVendorP.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtSaveVenP.Rows.Add(row);
                            }
                            ////}
                        }
                    }

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                                , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 처리 로직 //
                        // Method 호출
                        String rtMSG = clsVendor.mfDeleteMASVendorP(strVendorCode, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveVenP);

                        // Decoding //
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        // 처리로직 끝//

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 처리결과에 따른 메세지 박스
                        System.Windows.Forms.DialogResult result;
                        if (ErrRtn.ErrNum == 0)
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                            this.uGroupBoxContentsArea.Expanded = false;
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {

            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우

                ////////if (this.uGroupBoxContentsArea.Expanded == false)
                ////////{
                ////////    this.uGroupBoxContentsArea.Expanded = true;
                ////////}
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
            }
            catch (Exception ex)
            {

            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            if (this.uGridVendor.Rows.Count > 0)
                grd.mfDownLoadGridToExcel(this.uGridVendor);
        }
        #endregion

        #region 이벤트
        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGridVendor.Height = 80;
                    Point point = new Point(0, 160);
                    this.uGroupBoxContentsArea.Location = point;
                }
                else
                {
                    Point point = new Point(0, 815);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridVendor.Height = 751;

                    this.uGridVendor.Rows.FixedRows.Clear();

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        void uGridVendorP_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridVendorP, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }   
        }

        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
        }

        // 셀수정시 RowSelector 이미지 변경 이벤트
        private void uGridVendorP_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제 버튼 클릭 이벤트
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridVendorP.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridVendorP.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridVendorP.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 더블클릭시
        private void uGridVendor_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // ContentsArea가 접혀있을경우 펼쳐주고 현재 클릭된 행을 고정
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Row.Fixed = true;
                }

                // 검색조건
                String strVendorCode = e.Row.Cells["VendorCode"].Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Vendor), "Vendor");
                QRPMAS.BL.MASGEN.Vendor clsVendor = new QRPMAS.BL.MASGEN.Vendor();
                brwChannel.mfCredentials(clsVendor);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinMessageBox msg = new WinMessageBox();
                // PrograssBar 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", strLang));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtVendor = clsVendor.mfReadVendorDetail(strVendorCode, strLang);

                this.uTextVendorCode.Text = dtVendor.Rows[0]["VendorCode"].ToString();
                this.uTextVendorName.Text = dtVendor.Rows[0]["VendorName"].ToString();
                this.uTextVendorNameCh.Text = dtVendor.Rows[0]["VendorNameCh"].ToString();
                this.uTextVendorNameEn.Text = dtVendor.Rows[0]["VendorNameEn"].ToString();
                this.uTextAddress.Text = dtVendor.Rows[0]["Address"].ToString();
                this.uTextBossName.Text = dtVendor.Rows[0]["BossName"].ToString();
                this.uTextFax.Text = dtVendor.Rows[0]["Fax"].ToString();
                this.uTextRegNo.Text = dtVendor.Rows[0]["RegNo"].ToString();
                this.uTextTel.Text = dtVendor.Rows[0]["Tel"].ToString();
                this.uTextUseFlag.Text = dtVendor.Rows[0]["UseFlag"].ToString();

                // 거래처 담당자 정보 조회
                DataTable dtVendorP = clsVendor.mfReadVendorP(strVendorCode);

                this.uGridVendorP.DataSource = dtVendorP;
                this.uGridVendorP.DataBind();
  

                // 담당자명 편집 불가 상태로
                for (int i = 0; i < this.uGridVendorP.Rows.Count; i++)
                {
                    this.uGridVendorP.Rows[i].Cells["PersonName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridVendorP.Rows[i].Cells["PersonName"].Appearance.BackColor = Color.Gainsboro;
                }

                WinGrid grd = new WinGrid();
                grd.mfSetAutoResizeColWidth(this.uGridVendorP, 0);

                // PrograssBar 종료
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void Clear()
        {
            try
            {
                this.uTextAddress.Text = "";
                this.uTextBossName.Text = "";
                this.uTextFax.Text = "";
                this.uTextRegNo.Text = "";
                this.uTextTel.Text = "";
                this.uTextUseFlag.Text = "";
                this.uTextVendorCode.Text = "";
                this.uTextVendorName.Text = "";
                this.uTextVendorNameCh.Text = "";
                this.uTextVendorNameEn.Text = "";

                while (this.uGridVendorP.Rows.Count > 0)
                {
                    this.uGridVendorP.Rows[0].Delete(false);
                }
                if (this.uGridVendor.Rows.Count > 0 && !this.uTextVendorCode.Text.Equals(""))
                {
                    this.uGridVendorP.DisplayLayout.Bands[0].Columns["PersonName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMAS0032_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmMAS0032_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}