﻿namespace QRPMAS.UI
{
    partial class frmMAS0028
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0028));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchProcess = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uComboArea = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelArea = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupHistory = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridHistory = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonFileDownload = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridFileList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextEquipLevel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMakerYear = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSTS = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendor = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipGroup = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSerialNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextModel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipProcGubun = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLocation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStation = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextArea = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSuperEquip = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEquipName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSuperEquip = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelArea1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMakerYear = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLevel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSTS = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendor = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLocation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSerialNum = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipProcGubun = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelModel = new Infragistics.Win.Misc.UltraLabel();
            this.uPictureEquip = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.titleArea = new QRPUserControl.TitleArea();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupHistory)).BeginInit();
            this.uGroupHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridFileList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipProcGubun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuperEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchProcess);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelArea);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 12;
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(660, 36);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(145, 21);
            this.uComboSearchEquipGroup.TabIndex = 8;
            // 
            // uComboSearchLargeType
            // 
            this.uComboSearchLargeType.Location = new System.Drawing.Point(388, 36);
            this.uComboSearchLargeType.MaxLength = 50;
            this.uComboSearchLargeType.Name = "uComboSearchLargeType";
            this.uComboSearchLargeType.Size = new System.Drawing.Size(145, 21);
            this.uComboSearchLargeType.TabIndex = 8;
            this.uComboSearchLargeType.ValueChanged += new System.EventHandler(this.uComboSearchLargeType_ValueChanged);
            // 
            // uComboSearchProcess
            // 
            this.uComboSearchProcess.Location = new System.Drawing.Point(116, 36);
            this.uComboSearchProcess.MaxLength = 50;
            this.uComboSearchProcess.Name = "uComboSearchProcess";
            this.uComboSearchProcess.Size = new System.Drawing.Size(145, 21);
            this.uComboSearchProcess.TabIndex = 8;
            this.uComboSearchProcess.ValueChanged += new System.EventHandler(this.uComboSearchProcess_ValueChanged);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(660, 12);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(145, 21);
            this.uComboSearchEquipLoc.TabIndex = 8;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboStation
            // 
            this.uComboStation.Location = new System.Drawing.Point(388, 12);
            this.uComboStation.MaxLength = 50;
            this.uComboStation.Name = "uComboStation";
            this.uComboStation.Size = new System.Drawing.Size(145, 21);
            this.uComboStation.TabIndex = 7;
            this.uComboStation.Text = "uCombo";
            this.uComboStation.ValueChanged += new System.EventHandler(this.uComboStation_ValueChanged);
            // 
            // uLabelSearchEquipGroup
            // 
            this.uLabelSearchEquipGroup.Location = new System.Drawing.Point(556, 36);
            this.uLabelSearchEquipGroup.Name = "uLabelSearchEquipGroup";
            this.uLabelSearchEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipGroup.TabIndex = 6;
            // 
            // uLabelSearchEquipLargeType
            // 
            this.uLabelSearchEquipLargeType.Location = new System.Drawing.Point(284, 36);
            this.uLabelSearchEquipLargeType.Name = "uLabelSearchEquipLargeType";
            this.uLabelSearchEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLargeType.TabIndex = 6;
            // 
            // uLabelSearchProcessGroup
            // 
            this.uLabelSearchProcessGroup.Location = new System.Drawing.Point(12, 36);
            this.uLabelSearchProcessGroup.Name = "uLabelSearchProcessGroup";
            this.uLabelSearchProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchProcessGroup.TabIndex = 6;
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(556, 12);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 6;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(284, 12);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 6;
            // 
            // uComboArea
            // 
            this.uComboArea.Location = new System.Drawing.Point(1032, 12);
            this.uComboArea.MaxLength = 50;
            this.uComboArea.Name = "uComboArea";
            this.uComboArea.Size = new System.Drawing.Size(28, 21);
            this.uComboArea.TabIndex = 5;
            this.uComboArea.Text = "uCombo";
            this.uComboArea.Visible = false;
            // 
            // uLabelArea
            // 
            this.uLabelArea.Location = new System.Drawing.Point(1016, 12);
            this.uLabelArea.Name = "uLabelArea";
            this.uLabelArea.Size = new System.Drawing.Size(12, 20);
            this.uLabelArea.TabIndex = 4;
            this.uLabelArea.Text = "ultraLabel1";
            this.uLabelArea.Visible = false;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(145, 21);
            this.uComboPlant.TabIndex = 3;
            this.uComboPlant.Text = "전체";
            this.uComboPlant.ValueMember = " ";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 2;
            // 
            // uGridEquipList
            // 
            this.uGridEquipList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipList.DisplayLayout.Appearance = appearance5;
            this.uGridEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipList.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridEquipList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipList.Location = new System.Drawing.Point(0, 100);
            this.uGridEquipList.Name = "uGridEquipList";
            this.uGridEquipList.Size = new System.Drawing.Size(1060, 710);
            this.uGridEquipList.TabIndex = 13;
            this.uGridEquipList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipList_AfterCellUpdate);
            this.uGridEquipList.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipList_CellChange);
            this.uGridEquipList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGrid1_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1060, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1060, 715);
            this.uGroupBoxContentsArea.TabIndex = 14;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupHistory);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlantCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipLevel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMakerYear);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSTS);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVendor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSerialNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextModel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipProcGubun);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextLocation);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStation);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextArea);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSuperEquip);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSuperEquip);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelArea1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMakerYear);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipLevel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSTS);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVendor);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStation1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelLocation);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSerialNum);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipProcGubun);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelModel);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uPictureEquip);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1054, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupHistory
            // 
            this.uGroupHistory.Controls.Add(this.uGridHistory);
            this.uGroupHistory.Location = new System.Drawing.Point(12, 484);
            this.uGroupHistory.Name = "uGroupHistory";
            this.uGroupHistory.Size = new System.Drawing.Size(1030, 184);
            this.uGroupHistory.TabIndex = 248;
            // 
            // uGridHistory
            // 
            this.uGridHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHistory.DisplayLayout.Appearance = appearance46;
            this.uGridHistory.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHistory.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance43.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHistory.DisplayLayout.GroupByBox.Appearance = appearance43;
            appearance44.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHistory.DisplayLayout.GroupByBox.BandLabelAppearance = appearance44;
            this.uGridHistory.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance45.BackColor2 = System.Drawing.SystemColors.Control;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHistory.DisplayLayout.GroupByBox.PromptAppearance = appearance45;
            this.uGridHistory.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHistory.DisplayLayout.MaxRowScrollRegions = 1;
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            appearance54.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHistory.DisplayLayout.Override.ActiveCellAppearance = appearance54;
            appearance49.BackColor = System.Drawing.SystemColors.Highlight;
            appearance49.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHistory.DisplayLayout.Override.ActiveRowAppearance = appearance49;
            this.uGridHistory.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHistory.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance48.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHistory.DisplayLayout.Override.CardAreaAppearance = appearance48;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHistory.DisplayLayout.Override.CellAppearance = appearance47;
            this.uGridHistory.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHistory.DisplayLayout.Override.CellPadding = 0;
            appearance51.BackColor = System.Drawing.SystemColors.Control;
            appearance51.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance51.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance51.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance51.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHistory.DisplayLayout.Override.GroupByRowAppearance = appearance51;
            appearance53.TextHAlignAsString = "Left";
            this.uGridHistory.DisplayLayout.Override.HeaderAppearance = appearance53;
            this.uGridHistory.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHistory.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            appearance52.BorderColor = System.Drawing.Color.Silver;
            this.uGridHistory.DisplayLayout.Override.RowAppearance = appearance52;
            this.uGridHistory.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHistory.DisplayLayout.Override.TemplateAddRowAppearance = appearance50;
            this.uGridHistory.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHistory.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHistory.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHistory.Location = new System.Drawing.Point(12, 12);
            this.uGridHistory.Name = "uGridHistory";
            this.uGridHistory.Size = new System.Drawing.Size(1004, 160);
            this.uGridHistory.TabIndex = 0;
            this.uGridHistory.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridHistory_DoubleClickCell);
            // 
            // uTextPlantCode
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance40;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(948, 8);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(100, 21);
            this.uTextPlantCode.TabIndex = 247;
            this.uTextPlantCode.Visible = false;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBox1.Controls.Add(this.uButtonFileDownload);
            this.uGroupBox1.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBox1.Controls.Add(this.uGridFileList);
            this.uGroupBox1.Location = new System.Drawing.Point(12, 240);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1030, 236);
            this.uGroupBox1.TabIndex = 246;
            // 
            // uButtonFileDownload
            // 
            this.uButtonFileDownload.Location = new System.Drawing.Point(104, 28);
            this.uButtonFileDownload.Name = "uButtonFileDownload";
            this.uButtonFileDownload.Size = new System.Drawing.Size(88, 28);
            this.uButtonFileDownload.TabIndex = 3;
            this.uButtonFileDownload.Text = "ultraButton1";
            this.uButtonFileDownload.Click += new System.EventHandler(this.uButtonFileDownload_Click);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 28);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 2;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGridFileList
            // 
            this.uGridFileList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridFileList.DisplayLayout.Appearance = appearance19;
            this.uGridFileList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridFileList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFileList.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFileList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.uGridFileList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridFileList.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridFileList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridFileList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            appearance27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridFileList.DisplayLayout.Override.ActiveCellAppearance = appearance27;
            appearance22.BackColor = System.Drawing.SystemColors.Highlight;
            appearance22.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridFileList.DisplayLayout.Override.ActiveRowAppearance = appearance22;
            this.uGridFileList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridFileList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGridFileList.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridFileList.DisplayLayout.Override.CellAppearance = appearance20;
            this.uGridFileList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridFileList.DisplayLayout.Override.CellPadding = 0;
            appearance24.BackColor = System.Drawing.SystemColors.Control;
            appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance24.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridFileList.DisplayLayout.Override.GroupByRowAppearance = appearance24;
            appearance26.TextHAlignAsString = "Left";
            this.uGridFileList.DisplayLayout.Override.HeaderAppearance = appearance26;
            this.uGridFileList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridFileList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGridFileList.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGridFileList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridFileList.DisplayLayout.Override.TemplateAddRowAppearance = appearance23;
            this.uGridFileList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridFileList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridFileList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridFileList.Location = new System.Drawing.Point(12, 40);
            this.uGridFileList.Name = "uGridFileList";
            this.uGridFileList.Size = new System.Drawing.Size(1006, 180);
            this.uGridFileList.TabIndex = 1;
            this.uGridFileList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFileList_AfterCellUpdate);
            this.uGridFileList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFileList_ClickCellButton);
            this.uGridFileList.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridFileList_CellChange);
            // 
            // uTextEquipLevel
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLevel.Appearance = appearance15;
            this.uTextEquipLevel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipLevel.Location = new System.Drawing.Point(732, 180);
            this.uTextEquipLevel.Name = "uTextEquipLevel";
            this.uTextEquipLevel.ReadOnly = true;
            this.uTextEquipLevel.Size = new System.Drawing.Size(135, 21);
            this.uTextEquipLevel.TabIndex = 244;
            // 
            // uTextMakerYear
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakerYear.Appearance = appearance28;
            this.uTextMakerYear.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMakerYear.Location = new System.Drawing.Point(436, 180);
            this.uTextMakerYear.Name = "uTextMakerYear";
            this.uTextMakerYear.ReadOnly = true;
            this.uTextMakerYear.Size = new System.Drawing.Size(135, 21);
            this.uTextMakerYear.TabIndex = 244;
            // 
            // uTextSTS
            // 
            appearance29.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTS.Appearance = appearance29;
            this.uTextSTS.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSTS.Location = new System.Drawing.Point(732, 156);
            this.uTextSTS.Name = "uTextSTS";
            this.uTextSTS.ReadOnly = true;
            this.uTextSTS.Size = new System.Drawing.Size(135, 21);
            this.uTextSTS.TabIndex = 244;
            // 
            // uTextVendor
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Appearance = appearance30;
            this.uTextVendor.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVendor.Location = new System.Drawing.Point(436, 156);
            this.uTextVendor.Name = "uTextVendor";
            this.uTextVendor.ReadOnly = true;
            this.uTextVendor.Size = new System.Drawing.Size(135, 21);
            this.uTextVendor.TabIndex = 244;
            // 
            // uTextEquipGroup
            // 
            appearance31.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroup.Appearance = appearance31;
            this.uTextEquipGroup.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroup.Location = new System.Drawing.Point(732, 132);
            this.uTextEquipGroup.Name = "uTextEquipGroup";
            this.uTextEquipGroup.ReadOnly = true;
            this.uTextEquipGroup.Size = new System.Drawing.Size(135, 21);
            this.uTextEquipGroup.TabIndex = 244;
            // 
            // uTextEquipType
            // 
            appearance32.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Appearance = appearance32;
            this.uTextEquipType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipType.Location = new System.Drawing.Point(436, 132);
            this.uTextEquipType.Name = "uTextEquipType";
            this.uTextEquipType.ReadOnly = true;
            this.uTextEquipType.Size = new System.Drawing.Size(135, 21);
            this.uTextEquipType.TabIndex = 244;
            // 
            // uTextSerialNo
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Appearance = appearance33;
            this.uTextSerialNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSerialNo.Location = new System.Drawing.Point(732, 108);
            this.uTextSerialNo.Name = "uTextSerialNo";
            this.uTextSerialNo.ReadOnly = true;
            this.uTextSerialNo.Size = new System.Drawing.Size(135, 21);
            this.uTextSerialNo.TabIndex = 244;
            // 
            // uTextModel
            // 
            appearance34.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Appearance = appearance34;
            this.uTextModel.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextModel.Location = new System.Drawing.Point(436, 108);
            this.uTextModel.Name = "uTextModel";
            this.uTextModel.ReadOnly = true;
            this.uTextModel.Size = new System.Drawing.Size(135, 21);
            this.uTextModel.TabIndex = 244;
            // 
            // uTextEquipProcGubun
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipProcGubun.Appearance = appearance35;
            this.uTextEquipProcGubun.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipProcGubun.Location = new System.Drawing.Point(436, 84);
            this.uTextEquipProcGubun.Name = "uTextEquipProcGubun";
            this.uTextEquipProcGubun.ReadOnly = true;
            this.uTextEquipProcGubun.Size = new System.Drawing.Size(135, 21);
            this.uTextEquipProcGubun.TabIndex = 244;
            // 
            // uTextLocation
            // 
            appearance36.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLocation.Appearance = appearance36;
            this.uTextLocation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLocation.Location = new System.Drawing.Point(732, 84);
            this.uTextLocation.Name = "uTextLocation";
            this.uTextLocation.ReadOnly = true;
            this.uTextLocation.Size = new System.Drawing.Size(224, 21);
            this.uTextLocation.TabIndex = 244;
            // 
            // uTextStation
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Appearance = appearance37;
            this.uTextStation.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStation.Location = new System.Drawing.Point(732, 60);
            this.uTextStation.Name = "uTextStation";
            this.uTextStation.ReadOnly = true;
            this.uTextStation.Size = new System.Drawing.Size(135, 21);
            this.uTextStation.TabIndex = 244;
            // 
            // uTextArea
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Appearance = appearance38;
            this.uTextArea.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextArea.Location = new System.Drawing.Point(436, 60);
            this.uTextArea.Name = "uTextArea";
            this.uTextArea.ReadOnly = true;
            this.uTextArea.Size = new System.Drawing.Size(135, 21);
            this.uTextArea.TabIndex = 244;
            // 
            // uTextEquipCode
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Appearance = appearance39;
            this.uTextEquipCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipCode.Location = new System.Drawing.Point(732, 12);
            this.uTextEquipCode.Name = "uTextEquipCode";
            this.uTextEquipCode.ReadOnly = true;
            this.uTextEquipCode.Size = new System.Drawing.Size(135, 21);
            this.uTextEquipCode.TabIndex = 244;
            // 
            // uTextSuperEquip
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuperEquip.Appearance = appearance41;
            this.uTextSuperEquip.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSuperEquip.Location = new System.Drawing.Point(732, 36);
            this.uTextSuperEquip.Name = "uTextSuperEquip";
            this.uTextSuperEquip.ReadOnly = true;
            this.uTextSuperEquip.Size = new System.Drawing.Size(135, 21);
            this.uTextSuperEquip.TabIndex = 244;
            // 
            // uTextPlant
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance14;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(436, 12);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(135, 21);
            this.uTextPlant.TabIndex = 244;
            // 
            // uTextEquipName
            // 
            appearance42.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Appearance = appearance42;
            this.uTextEquipName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipName.Location = new System.Drawing.Point(436, 36);
            this.uTextEquipName.Name = "uTextEquipName";
            this.uTextEquipName.ReadOnly = true;
            this.uTextEquipName.Size = new System.Drawing.Size(135, 21);
            this.uTextEquipName.TabIndex = 244;
            // 
            // uLabelPlant1
            // 
            this.uLabelPlant1.Location = new System.Drawing.Point(320, 12);
            this.uLabelPlant1.Name = "uLabelPlant1";
            this.uLabelPlant1.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant1.TabIndex = 238;
            this.uLabelPlant1.Text = "1";
            // 
            // uLabelEquipCode
            // 
            this.uLabelEquipCode.Location = new System.Drawing.Point(616, 12);
            this.uLabelEquipCode.Name = "uLabelEquipCode";
            this.uLabelEquipCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipCode.TabIndex = 239;
            this.uLabelEquipCode.Text = "2";
            // 
            // uLabelEquipName
            // 
            this.uLabelEquipName.Location = new System.Drawing.Point(320, 36);
            this.uLabelEquipName.Name = "uLabelEquipName";
            this.uLabelEquipName.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipName.TabIndex = 240;
            this.uLabelEquipName.Text = "3";
            // 
            // uLabelSuperEquip
            // 
            this.uLabelSuperEquip.Location = new System.Drawing.Point(616, 36);
            this.uLabelSuperEquip.Name = "uLabelSuperEquip";
            this.uLabelSuperEquip.Size = new System.Drawing.Size(110, 20);
            this.uLabelSuperEquip.TabIndex = 241;
            this.uLabelSuperEquip.Text = "4";
            // 
            // uLabelArea1
            // 
            this.uLabelArea1.Location = new System.Drawing.Point(320, 60);
            this.uLabelArea1.Name = "uLabelArea1";
            this.uLabelArea1.Size = new System.Drawing.Size(110, 20);
            this.uLabelArea1.TabIndex = 242;
            this.uLabelArea1.Text = "5";
            // 
            // uLabelMakerYear
            // 
            this.uLabelMakerYear.Location = new System.Drawing.Point(320, 180);
            this.uLabelMakerYear.Name = "uLabelMakerYear";
            this.uLabelMakerYear.Size = new System.Drawing.Size(110, 20);
            this.uLabelMakerYear.TabIndex = 227;
            this.uLabelMakerYear.Text = "15";
            // 
            // uLabelEquipLevel
            // 
            this.uLabelEquipLevel.Location = new System.Drawing.Point(616, 180);
            this.uLabelEquipLevel.Name = "uLabelEquipLevel";
            this.uLabelEquipLevel.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipLevel.TabIndex = 237;
            this.uLabelEquipLevel.Text = "16";
            // 
            // uLabelSTS
            // 
            this.uLabelSTS.Location = new System.Drawing.Point(616, 156);
            this.uLabelSTS.Name = "uLabelSTS";
            this.uLabelSTS.Size = new System.Drawing.Size(110, 20);
            this.uLabelSTS.TabIndex = 230;
            this.uLabelSTS.Text = "14";
            // 
            // uLabelVendor
            // 
            this.uLabelVendor.Location = new System.Drawing.Point(320, 156);
            this.uLabelVendor.Name = "uLabelVendor";
            this.uLabelVendor.Size = new System.Drawing.Size(110, 20);
            this.uLabelVendor.TabIndex = 233;
            this.uLabelVendor.Text = "13";
            // 
            // uLabelStation1
            // 
            this.uLabelStation1.Location = new System.Drawing.Point(616, 60);
            this.uLabelStation1.Name = "uLabelStation1";
            this.uLabelStation1.Size = new System.Drawing.Size(110, 20);
            this.uLabelStation1.TabIndex = 228;
            this.uLabelStation1.Text = "6";
            // 
            // uLabelLocation
            // 
            this.uLabelLocation.Location = new System.Drawing.Point(616, 84);
            this.uLabelLocation.Name = "uLabelLocation";
            this.uLabelLocation.Size = new System.Drawing.Size(110, 20);
            this.uLabelLocation.TabIndex = 229;
            this.uLabelLocation.Text = "7";
            // 
            // uLabelSerialNum
            // 
            this.uLabelSerialNum.Location = new System.Drawing.Point(616, 108);
            this.uLabelSerialNum.Name = "uLabelSerialNum";
            this.uLabelSerialNum.Size = new System.Drawing.Size(110, 20);
            this.uLabelSerialNum.TabIndex = 236;
            this.uLabelSerialNum.Text = "10";
            // 
            // uLabelEquipProcGubun
            // 
            this.uLabelEquipProcGubun.Location = new System.Drawing.Point(320, 84);
            this.uLabelEquipProcGubun.Name = "uLabelEquipProcGubun";
            this.uLabelEquipProcGubun.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipProcGubun.TabIndex = 231;
            this.uLabelEquipProcGubun.Text = "8";
            // 
            // uLabelEquipGroup
            // 
            this.uLabelEquipGroup.Location = new System.Drawing.Point(616, 132);
            this.uLabelEquipGroup.Name = "uLabelEquipGroup";
            this.uLabelEquipGroup.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipGroup.TabIndex = 232;
            this.uLabelEquipGroup.Text = "12";
            // 
            // uLabelEquipType
            // 
            this.uLabelEquipType.Location = new System.Drawing.Point(320, 132);
            this.uLabelEquipType.Name = "uLabelEquipType";
            this.uLabelEquipType.Size = new System.Drawing.Size(110, 20);
            this.uLabelEquipType.TabIndex = 234;
            this.uLabelEquipType.Text = "11";
            // 
            // uLabelModel
            // 
            this.uLabelModel.Location = new System.Drawing.Point(320, 108);
            this.uLabelModel.Name = "uLabelModel";
            this.uLabelModel.Size = new System.Drawing.Size(110, 20);
            this.uLabelModel.TabIndex = 235;
            this.uLabelModel.Text = "9";
            // 
            // uPictureEquip
            // 
            this.uPictureEquip.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPictureEquip.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uPictureEquip.Location = new System.Drawing.Point(12, 12);
            this.uPictureEquip.Name = "uPictureEquip";
            this.uPictureEquip.Size = new System.Drawing.Size(304, 220);
            this.uPictureEquip.TabIndex = 21;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 11;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmMAS0028
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridEquipList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0028";
            this.Load += new System.EventHandler(this.frmMAS0028_Load);
            this.Activated += new System.EventHandler(this.frmMAS0028_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0028_FormClosing);
            this.Resize += new System.EventHandler(this.frmMAS0028_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupHistory)).EndInit();
            this.uGroupHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridFileList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMakerYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSTS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSerialNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipProcGubun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSuperEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStation;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboArea;
        private Infragistics.Win.Misc.UltraLabel uLabelArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPictureEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant1;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipName;
        private Infragistics.Win.Misc.UltraLabel uLabelSuperEquip;
        private Infragistics.Win.Misc.UltraLabel uLabelArea1;
        private Infragistics.Win.Misc.UltraLabel uLabelMakerYear;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLevel;
        private Infragistics.Win.Misc.UltraLabel uLabelSTS;
        private Infragistics.Win.Misc.UltraLabel uLabelVendor;
        private Infragistics.Win.Misc.UltraLabel uLabelStation1;
        private Infragistics.Win.Misc.UltraLabel uLabelLocation;
        private Infragistics.Win.Misc.UltraLabel uLabelSerialNum;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipProcGubun;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelModel;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridFileList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipLevel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMakerYear;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSTS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendor;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSerialNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextModel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipProcGubun;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLocation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStation;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSuperEquip;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.Misc.UltraButton uButtonFileDownload;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchProcess;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchProcessGroup;
        private Infragistics.Win.Misc.UltraGroupBox uGroupHistory;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHistory;
    }
}