﻿namespace QRPMAS.UI
{
    partial class frmMASZ0025
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0025));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton3 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton4 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraTextEditor11 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialS = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelClientS = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDataName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDataName = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterial = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelClient = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uComboUesFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uTextStandardNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStandardNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStandardName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStandardCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStandardNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStandardNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStandardName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStandardCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDataName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUesFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor11);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor10);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelMaterialS);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelClientS);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor9);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextDataName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelDataName);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // ultraTextEditor11
            // 
            appearance33.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Appearance = appearance33;
            this.ultraTextEditor11.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor11.Location = new System.Drawing.Point(548, 12);
            this.ultraTextEditor11.Name = "ultraTextEditor11";
            this.ultraTextEditor11.ReadOnly = true;
            this.ultraTextEditor11.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor11.TabIndex = 15;
            // 
            // ultraTextEditor10
            // 
            appearance34.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance34.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance34;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor10.ButtonsRight.Add(editorButton1);
            this.ultraTextEditor10.Location = new System.Drawing.Point(444, 12);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor10.TabIndex = 16;
            // 
            // uLabelMaterialS
            // 
            this.uLabelMaterialS.Location = new System.Drawing.Point(340, 12);
            this.uLabelMaterialS.Name = "uLabelMaterialS";
            this.uLabelMaterialS.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterialS.TabIndex = 14;
            this.uLabelMaterialS.Text = "ultraLabel1";
            // 
            // ultraTextEditor2
            // 
            appearance35.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Appearance = appearance35;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Location = new System.Drawing.Point(220, 12);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor2.TabIndex = 13;
            // 
            // uLabelClientS
            // 
            this.uLabelClientS.Location = new System.Drawing.Point(12, 12);
            this.uLabelClientS.Name = "uLabelClientS";
            this.uLabelClientS.Size = new System.Drawing.Size(100, 20);
            this.uLabelClientS.TabIndex = 11;
            this.uLabelClientS.Text = "ultraLabel1";
            // 
            // ultraTextEditor9
            // 
            appearance36.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance36.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance36;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor9.ButtonsRight.Add(editorButton2);
            this.ultraTextEditor9.Location = new System.Drawing.Point(116, 12);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor9.TabIndex = 12;
            // 
            // uTextDataName
            // 
            this.uTextDataName.Location = new System.Drawing.Point(796, 12);
            this.uTextDataName.Name = "uTextDataName";
            this.uTextDataName.Size = new System.Drawing.Size(144, 21);
            this.uTextDataName.TabIndex = 1;
            // 
            // uLabelDataName
            // 
            this.uLabelDataName.Location = new System.Drawing.Point(672, 12);
            this.uLabelDataName.Name = "uLabelDataName";
            this.uLabelDataName.Size = new System.Drawing.Size(120, 20);
            this.uLabelDataName.TabIndex = 0;
            this.uLabelDataName.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance5;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 760);
            this.uGrid1.TabIndex = 2;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterial);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelClient);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGrid2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUesFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStandardNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStandardNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStandardName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStandardCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // ultraTextEditor3
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Appearance = appearance4;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Location = new System.Drawing.Point(940, 12);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.ReadOnly = true;
            this.ultraTextEditor3.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor3.TabIndex = 90;
            // 
            // ultraTextEditor4
            // 
            appearance41.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor4.Appearance = appearance41;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.PowderBlue;
            appearance44.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance44.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton3.Appearance = appearance44;
            editorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor4.ButtonsRight.Add(editorButton3);
            this.ultraTextEditor4.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor4.Location = new System.Drawing.Point(839, 12);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor4.TabIndex = 89;
            // 
            // uLabelMaterial
            // 
            this.uLabelMaterial.Location = new System.Drawing.Point(695, 12);
            this.uLabelMaterial.Name = "uLabelMaterial";
            this.uLabelMaterial.Size = new System.Drawing.Size(140, 20);
            this.uLabelMaterial.TabIndex = 88;
            this.uLabelMaterial.Text = "ultraLabel1";
            // 
            // ultraTextEditor1
            // 
            appearance30.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor1.Appearance = appearance30;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor1.Location = new System.Drawing.Point(576, 12);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.ReadOnly = true;
            this.ultraTextEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor1.TabIndex = 87;
            // 
            // uTextCreateUserID
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCreateUserID.Appearance = appearance31;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance32.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance32.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton4.Appearance = appearance32;
            editorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCreateUserID.ButtonsRight.Add(editorButton4);
            this.uTextCreateUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCreateUserID.Location = new System.Drawing.Point(475, 12);
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserID.TabIndex = 86;
            // 
            // uLabelClient
            // 
            this.uLabelClient.Location = new System.Drawing.Point(331, 12);
            this.uLabelClient.Name = "uLabelClient";
            this.uLabelClient.Size = new System.Drawing.Size(140, 20);
            this.uLabelClient.TabIndex = 85;
            this.uLabelClient.Text = "ultraLabel1";
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance15;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance22;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance25;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 88);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1040, 596);
            this.uGrid2.TabIndex = 3;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            // 
            // uComboUesFlag
            // 
            this.uComboUesFlag.Location = new System.Drawing.Point(840, 64);
            this.uComboUesFlag.Name = "uComboUesFlag";
            this.uComboUesFlag.Size = new System.Drawing.Size(150, 21);
            this.uComboUesFlag.TabIndex = 2;
            this.uComboUesFlag.Text = "ultraComboEditor1";
            // 
            // uTextStandardNameCh
            // 
            this.uTextStandardNameCh.Location = new System.Drawing.Point(156, 64);
            this.uTextStandardNameCh.Name = "uTextStandardNameCh";
            this.uTextStandardNameCh.Size = new System.Drawing.Size(150, 21);
            this.uTextStandardNameCh.TabIndex = 1;
            // 
            // uTextStandardNameEn
            // 
            this.uTextStandardNameEn.Location = new System.Drawing.Point(475, 64);
            this.uTextStandardNameEn.Name = "uTextStandardNameEn";
            this.uTextStandardNameEn.Size = new System.Drawing.Size(150, 21);
            this.uTextStandardNameEn.TabIndex = 1;
            // 
            // uTextStandardName
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStandardName.Appearance = appearance28;
            this.uTextStandardName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStandardName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextStandardName.Location = new System.Drawing.Point(475, 40);
            this.uTextStandardName.Name = "uTextStandardName";
            this.uTextStandardName.Size = new System.Drawing.Size(150, 21);
            this.uTextStandardName.TabIndex = 1;
            // 
            // uTextStandardCode
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStandardCode.Appearance = appearance27;
            this.uTextStandardCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStandardCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextStandardCode.Location = new System.Drawing.Point(156, 40);
            this.uTextStandardCode.Name = "uTextStandardCode";
            this.uTextStandardCode.Size = new System.Drawing.Size(150, 21);
            this.uTextStandardCode.TabIndex = 1;
            // 
            // uComboPlant
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance14;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboPlant.Location = new System.Drawing.Point(156, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(696, 64);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(140, 20);
            this.uLabelUseFlag.TabIndex = 0;
            this.uLabelUseFlag.Text = "ultraLabel2";
            // 
            // uLabelStandardNameCh
            // 
            this.uLabelStandardNameCh.Location = new System.Drawing.Point(12, 64);
            this.uLabelStandardNameCh.Name = "uLabelStandardNameCh";
            this.uLabelStandardNameCh.Size = new System.Drawing.Size(140, 20);
            this.uLabelStandardNameCh.TabIndex = 0;
            this.uLabelStandardNameCh.Text = "ultraLabel2";
            // 
            // uLabelStandardNameEn
            // 
            this.uLabelStandardNameEn.Location = new System.Drawing.Point(331, 64);
            this.uLabelStandardNameEn.Name = "uLabelStandardNameEn";
            this.uLabelStandardNameEn.Size = new System.Drawing.Size(140, 20);
            this.uLabelStandardNameEn.TabIndex = 0;
            this.uLabelStandardNameEn.Text = "ultraLabel2";
            // 
            // uLabelStandardName
            // 
            this.uLabelStandardName.Location = new System.Drawing.Point(331, 40);
            this.uLabelStandardName.Name = "uLabelStandardName";
            this.uLabelStandardName.Size = new System.Drawing.Size(140, 20);
            this.uLabelStandardName.TabIndex = 0;
            this.uLabelStandardName.Text = "ultraLabel2";
            // 
            // uLabelStandardCode
            // 
            this.uLabelStandardCode.Location = new System.Drawing.Point(12, 40);
            this.uLabelStandardCode.Name = "uLabelStandardCode";
            this.uLabelStandardCode.Size = new System.Drawing.Size(140, 20);
            this.uLabelStandardCode.TabIndex = 0;
            this.uLabelStandardCode.Text = "ultraLabel2";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(140, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel2";
            // 
            // frmMASZ0025
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0025";
            this.Load += new System.EventHandler(this.frmMASZ0025_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0025_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDataName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUesFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDataName;
        private Infragistics.Win.Misc.UltraLabel uLabelDataName;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardName;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardCode;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUesFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.Misc.UltraLabel uLabelClientS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelClient;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterial;
    }
}