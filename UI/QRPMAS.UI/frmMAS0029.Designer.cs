﻿namespace QRPMAS.UI
{
    partial class frmMAS0029
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0029));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonFileDown = new Infragistics.Win.Misc.UltraButton();
            this.uButtonCopy = new Infragistics.Win.Misc.UltraButton();
            this.uGridEquipPMD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uTab = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTextRejectReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRevisionReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRejectReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRevisionReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAcceptState = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAcceptState = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAcceptUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAcceptUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAcceptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateCreateDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVersionNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStandardNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMD)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).BeginInit();
            this.uGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).BeginInit();
            this.uTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNo)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonFileDown);
            this.ultraTabPageControl1.Controls.Add(this.uButtonCopy);
            this.ultraTabPageControl1.Controls.Add(this.uGridEquipPMD);
            this.ultraTabPageControl1.Controls.Add(this.uButtonDeleteRow1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1046, 550);
            // 
            // uButtonFileDown
            // 
            this.uButtonFileDown.Location = new System.Drawing.Point(104, 12);
            this.uButtonFileDown.Name = "uButtonFileDown";
            this.uButtonFileDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonFileDown.TabIndex = 4;
            this.uButtonFileDown.Click += new System.EventHandler(this.uButtonFileDown_Click);
            // 
            // uButtonCopy
            // 
            this.uButtonCopy.Location = new System.Drawing.Point(196, 12);
            this.uButtonCopy.Name = "uButtonCopy";
            this.uButtonCopy.Size = new System.Drawing.Size(88, 28);
            this.uButtonCopy.TabIndex = 3;
            this.uButtonCopy.Click += new System.EventHandler(this.uButtonCopy_Click);
            // 
            // uGridEquipPMD
            // 
            this.uGridEquipPMD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipPMD.DisplayLayout.Appearance = appearance16;
            this.uGridEquipPMD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipPMD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.uGridEquipPMD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipPMD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipPMD.DisplayLayout.Override.ActiveCellAppearance = appearance24;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipPMD.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridEquipPMD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipPMD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance17.BorderColor = System.Drawing.Color.Silver;
            appearance17.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipPMD.DisplayLayout.Override.CellAppearance = appearance17;
            this.uGridEquipPMD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipPMD.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance23.TextHAlignAsString = "Left";
            this.uGridEquipPMD.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridEquipPMD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipPMD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipPMD.DisplayLayout.Override.RowAppearance = appearance22;
            this.uGridEquipPMD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipPMD.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.uGridEquipPMD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipPMD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipPMD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipPMD.Location = new System.Drawing.Point(12, 44);
            this.uGridEquipPMD.Name = "uGridEquipPMD";
            this.uGridEquipPMD.Size = new System.Drawing.Size(1026, 500);
            this.uGridEquipPMD.TabIndex = 2;
            this.uGridEquipPMD.Text = "ultraGrid1";
            this.uGridEquipPMD.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGridEquipPMD.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipPMD_ClickCellButton);
            this.uGridEquipPMD.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipPMD_CellChange);
            // 
            // uButtonDeleteRow1
            // 
            this.uButtonDeleteRow1.Location = new System.Drawing.Point(12, 12);
            this.uButtonDeleteRow1.Name = "uButtonDeleteRow1";
            this.uButtonDeleteRow1.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow1.TabIndex = 1;
            this.uButtonDeleteRow1.Click += new System.EventHandler(this.uButtonDeleteRow1_Click);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridEquipList);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1046, 550);
            // 
            // uGridEquipList
            // 
            this.uGridEquipList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipList.DisplayLayout.Appearance = appearance28;
            this.uGridEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.uGridEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.uGridEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance36;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipList.DisplayLayout.Override.CellAppearance = appearance29;
            this.uGridEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance35.TextHAlignAsString = "Left";
            this.uGridEquipList.DisplayLayout.Override.HeaderAppearance = appearance35;
            this.uGridEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipList.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGridEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipList.Location = new System.Drawing.Point(12, 12);
            this.uGridEquipList.Name = "uGridEquipList";
            this.uGridEquipList.Size = new System.Drawing.Size(1016, 528);
            this.uGridEquipList.TabIndex = 2;
            this.uGridEquipList.Text = "ultraGrid1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 36);
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(150, 21);
            this.uComboProcessGroup.TabIndex = 4;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 36);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 3;
            // 
            // uLabelSearchEquipType
            // 
            this.uLabelSearchEquipType.Location = new System.Drawing.Point(316, 36);
            this.uLabelSearchEquipType.Name = "uLabelSearchEquipType";
            this.uLabelSearchEquipType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipType.TabIndex = 3;
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(632, 12);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 3;
            // 
            // uComboSearchEquipType
            // 
            this.uComboSearchEquipType.Location = new System.Drawing.Point(420, 36);
            this.uComboSearchEquipType.MaxLength = 50;
            this.uComboSearchEquipType.Name = "uComboSearchEquipType";
            this.uComboSearchEquipType.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchEquipType.TabIndex = 2;
            this.uComboSearchEquipType.ValueChanged += new System.EventHandler(this.uComboSearchEquipType_ValueChanged);
            this.uComboSearchEquipType.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboSearchEquipGroup_BeforeDropDown);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(736, 12);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchEquipLoc.TabIndex = 2;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            this.uComboSearchEquipLoc.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboSearchEquipGroup_BeforeDropDown);
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(420, 12);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchStation.TabIndex = 2;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            this.uComboSearchStation.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboSearchEquipGroup_BeforeDropDown);
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(736, 36);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchEquipGroup.TabIndex = 2;
            this.uComboSearchEquipGroup.ValueChanged += new System.EventHandler(this.uComboSearchEquipGroup_ValueChanged);
            this.uComboSearchEquipGroup.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboSearchEquipGroup_BeforeDropDown);
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(316, 12);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 2;
            // 
            // uLabelSearchEquipGroup
            // 
            this.uLabelSearchEquipGroup.Location = new System.Drawing.Point(632, 36);
            this.uLabelSearchEquipGroup.Name = "uLabelSearchEquipGroup";
            this.uLabelSearchEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipGroup.TabIndex = 2;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            this.uComboSearchPlant.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboSearchPlant_BeforeDropDown);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uGroupBox
            // 
            this.uGroupBox.Controls.Add(this.ultraLabel2);
            this.uGroupBox.Controls.Add(this.uTab);
            this.uGroupBox.Controls.Add(this.uTextRejectReason);
            this.uGroupBox.Controls.Add(this.uTextRevisionReason);
            this.uGroupBox.Controls.Add(this.uTextEtc);
            this.uGroupBox.Controls.Add(this.uLabelRejectReason);
            this.uGroupBox.Controls.Add(this.uLabelRevisionReason);
            this.uGroupBox.Controls.Add(this.uLabelEtc);
            this.uGroupBox.Controls.Add(this.uTextAcceptState);
            this.uGroupBox.Controls.Add(this.uLabelAcceptState);
            this.uGroupBox.Controls.Add(this.uTextAcceptUserName);
            this.uGroupBox.Controls.Add(this.uTextAcceptUserID);
            this.uGroupBox.Controls.Add(this.uLabelAcceptUser);
            this.uGroupBox.Controls.Add(this.uDateCreateDate);
            this.uGroupBox.Controls.Add(this.uLabelCreateDate);
            this.uGroupBox.Controls.Add(this.uTextCreateUserName);
            this.uGroupBox.Controls.Add(this.uTextCreateUserID);
            this.uGroupBox.Controls.Add(this.uLabelCreateUser);
            this.uGroupBox.Controls.Add(this.uTextVersionNo);
            this.uGroupBox.Controls.Add(this.uTextStandardNo);
            this.uGroupBox.Controls.Add(this.uLabelStandardNo);
            this.uGroupBox.Location = new System.Drawing.Point(0, 100);
            this.uGroupBox.Name = "uGroupBox";
            this.uGroupBox.Size = new System.Drawing.Size(1070, 720);
            this.uGroupBox.TabIndex = 3;
            // 
            // ultraLabel2
            // 
            this.ultraLabel2.Location = new System.Drawing.Point(964, 52);
            this.ultraLabel2.Name = "ultraLabel2";
            this.ultraLabel2.Size = new System.Drawing.Size(12, 8);
            this.ultraLabel2.TabIndex = 21;
            this.ultraLabel2.Text = "-";
            this.ultraLabel2.Visible = false;
            // 
            // uTab
            // 
            this.uTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTab.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTab.Controls.Add(this.ultraTabPageControl1);
            this.uTab.Controls.Add(this.ultraTabPageControl2);
            this.uTab.Location = new System.Drawing.Point(12, 136);
            this.uTab.Name = "uTab";
            this.uTab.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTab.Size = new System.Drawing.Size(1050, 576);
            this.uTab.TabIndex = 20;
            ultraTab1.Key = "Item";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "점검항목상세";
            ultraTab2.Key = "Equip";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "점검그룹설비리스트";
            this.uTab.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1046, 550);
            // 
            // uTextRejectReason
            // 
            appearance38.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRejectReason.Appearance = appearance38;
            this.uTextRejectReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRejectReason.Location = new System.Drawing.Point(148, 108);
            this.uTextRejectReason.Name = "uTextRejectReason";
            this.uTextRejectReason.ReadOnly = true;
            this.uTextRejectReason.Size = new System.Drawing.Size(628, 21);
            this.uTextRejectReason.TabIndex = 19;
            // 
            // uTextRevisionReason
            // 
            this.uTextRevisionReason.Location = new System.Drawing.Point(148, 84);
            this.uTextRevisionReason.MaxLength = 100;
            this.uTextRevisionReason.Name = "uTextRevisionReason";
            this.uTextRevisionReason.ReadOnly = true;
            this.uTextRevisionReason.Size = new System.Drawing.Size(628, 21);
            this.uTextRevisionReason.TabIndex = 7;
            this.uTextRevisionReason.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextRevisionReason_KeyDown);
            // 
            // uTextEtc
            // 
            this.uTextEtc.Location = new System.Drawing.Point(148, 60);
            this.uTextEtc.MaxLength = 100;
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.Size = new System.Drawing.Size(628, 21);
            this.uTextEtc.TabIndex = 6;
            this.uTextEtc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextEtc_KeyDown);
            // 
            // uLabelRejectReason
            // 
            this.uLabelRejectReason.Location = new System.Drawing.Point(12, 108);
            this.uLabelRejectReason.Name = "uLabelRejectReason";
            this.uLabelRejectReason.Size = new System.Drawing.Size(130, 20);
            this.uLabelRejectReason.TabIndex = 18;
            this.uLabelRejectReason.Text = "ultraLabel1";
            // 
            // uLabelRevisionReason
            // 
            this.uLabelRevisionReason.Location = new System.Drawing.Point(12, 84);
            this.uLabelRevisionReason.Name = "uLabelRevisionReason";
            this.uLabelRevisionReason.Size = new System.Drawing.Size(130, 20);
            this.uLabelRevisionReason.TabIndex = 18;
            this.uLabelRevisionReason.Text = "ultraLabel1";
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 60);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(130, 20);
            this.uLabelEtc.TabIndex = 18;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uTextAcceptState
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptState.Appearance = appearance6;
            this.uTextAcceptState.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptState.Location = new System.Drawing.Point(552, 36);
            this.uTextAcceptState.MaxLength = 10;
            this.uTextAcceptState.Name = "uTextAcceptState";
            this.uTextAcceptState.ReadOnly = true;
            this.uTextAcceptState.Size = new System.Drawing.Size(100, 21);
            this.uTextAcceptState.TabIndex = 17;
            // 
            // uLabelAcceptState
            // 
            this.uLabelAcceptState.Location = new System.Drawing.Point(416, 36);
            this.uLabelAcceptState.Name = "uLabelAcceptState";
            this.uLabelAcceptState.Size = new System.Drawing.Size(130, 20);
            this.uLabelAcceptState.TabIndex = 16;
            this.uLabelAcceptState.Text = "ultraLabel2";
            // 
            // uTextAcceptUserName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptUserName.Appearance = appearance2;
            this.uTextAcceptUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAcceptUserName.Location = new System.Drawing.Point(252, 36);
            this.uTextAcceptUserName.MaxLength = 20;
            this.uTextAcceptUserName.Name = "uTextAcceptUserName";
            this.uTextAcceptUserName.ReadOnly = true;
            this.uTextAcceptUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextAcceptUserName.TabIndex = 15;
            // 
            // uTextAcceptUserID
            // 
            appearance3.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextAcceptUserID.Appearance = appearance3;
            this.uTextAcceptUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance4.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance4.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance4;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextAcceptUserID.ButtonsRight.Add(editorButton1);
            this.uTextAcceptUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAcceptUserID.Location = new System.Drawing.Point(148, 36);
            this.uTextAcceptUserID.MaxLength = 20;
            this.uTextAcceptUserID.Name = "uTextAcceptUserID";
            this.uTextAcceptUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextAcceptUserID.TabIndex = 5;
            this.uTextAcceptUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextAcceptUserID_KeyDown);
            this.uTextAcceptUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextAcceptUserID_EditorButtonClick);
            // 
            // uLabelAcceptUser
            // 
            this.uLabelAcceptUser.Location = new System.Drawing.Point(12, 36);
            this.uLabelAcceptUser.Name = "uLabelAcceptUser";
            this.uLabelAcceptUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelAcceptUser.TabIndex = 13;
            this.uLabelAcceptUser.Text = "ultraLabel2";
            // 
            // uDateCreateDate
            // 
            appearance5.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCreateDate.Appearance = appearance5;
            this.uDateCreateDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateCreateDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateCreateDate.Location = new System.Drawing.Point(552, 12);
            this.uDateCreateDate.MaskInput = "{date}";
            this.uDateCreateDate.Name = "uDateCreateDate";
            this.uDateCreateDate.Size = new System.Drawing.Size(100, 21);
            this.uDateCreateDate.TabIndex = 4;
            this.uDateCreateDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uDateCreateDate_KeyDown);
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(416, 12);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelCreateDate.TabIndex = 11;
            this.uLabelCreateDate.Text = "ultraLabel2";
            // 
            // uTextCreateUserName
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Appearance = appearance9;
            this.uTextCreateUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Location = new System.Drawing.Point(252, 12);
            this.uTextCreateUserName.Name = "uTextCreateUserName";
            this.uTextCreateUserName.ReadOnly = true;
            this.uTextCreateUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserName.TabIndex = 10;
            // 
            // uTextCreateUserID
            // 
            appearance10.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextCreateUserID.Appearance = appearance10;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance11.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance11;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCreateUserID.ButtonsRight.Add(editorButton2);
            this.uTextCreateUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextCreateUserID.Location = new System.Drawing.Point(148, 12);
            this.uTextCreateUserID.MaxLength = 20;
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserID.TabIndex = 3;
            this.uTextCreateUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCreateUserID_KeyDown);
            this.uTextCreateUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCreateUserID_EditorButtonClick);
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(12, 12);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelCreateUser.TabIndex = 8;
            this.uLabelCreateUser.Text = "ultraLabel2";
            // 
            // uTextVersionNo
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNo.Appearance = appearance8;
            this.uTextVersionNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNo.Location = new System.Drawing.Point(976, 24);
            this.uTextVersionNo.MaxLength = 10;
            this.uTextVersionNo.Name = "uTextVersionNo";
            this.uTextVersionNo.ReadOnly = true;
            this.uTextVersionNo.Size = new System.Drawing.Size(44, 21);
            this.uTextVersionNo.TabIndex = 3;
            this.uTextVersionNo.Visible = false;
            // 
            // uTextStandardNo
            // 
            appearance37.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardNo.Appearance = appearance37;
            this.uTextStandardNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardNo.Location = new System.Drawing.Point(864, 24);
            this.uTextStandardNo.MaxLength = 10;
            this.uTextStandardNo.Name = "uTextStandardNo";
            this.uTextStandardNo.ReadOnly = true;
            this.uTextStandardNo.Size = new System.Drawing.Size(100, 21);
            this.uTextStandardNo.TabIndex = 1;
            this.uTextStandardNo.Visible = false;
            // 
            // uLabelStandardNo
            // 
            this.uLabelStandardNo.Location = new System.Drawing.Point(728, 24);
            this.uLabelStandardNo.Name = "uLabelStandardNo";
            this.uLabelStandardNo.Size = new System.Drawing.Size(130, 20);
            this.uLabelStandardNo.TabIndex = 0;
            this.uLabelStandardNo.Text = "ultraLabel1";
            this.uLabelStandardNo.Visible = false;
            // 
            // frmMAS0029
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBox);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0029";
            this.Load += new System.EventHandler(this.frmMAS0029_Load);
            this.Activated += new System.EventHandler(this.frmMAS0029_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0029_FormClosing);
            this.Resize += new System.EventHandler(this.frmMAS0029_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMD)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox)).EndInit();
            this.uGroupBox.ResumeLayout(false);
            this.uGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTab)).EndInit();
            this.uTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVersionNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardNo;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNo;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateCreateDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTab;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcceptState;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptState;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcceptUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcceptUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptUser;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipPMD;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipList;
        private Infragistics.Win.Misc.UltraLabel ultraLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRejectReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRejectReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevisionReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRevisionReason;
        private Infragistics.Win.Misc.UltraButton uButtonCopy;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
    }
}