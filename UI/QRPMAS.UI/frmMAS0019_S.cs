﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0019.cs                                         */
/* 프로그램명   : 검사항목정보등록                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-31 : 선택항목ComboBox 추가 (이종호)           */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;
using System.Threading;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMAS0019_S : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();
        private string m_strDBConn = "";
        private bool m_bolDebugMode = false;

        public frmMAS0019_S()
        {
            InitializeComponent();
        }

        private void frmMAS0019_Load(object sender, EventArgs e)
        {
            // SystemInfo ResourceSet
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Label설정
            this.titleArea.mfSetLabelText("검사항목정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            //SetRunMode();
            SetToolAuth();
            // 초기화 Method
            // 텍스트 박스 초기화
            InitTextBox();
            // 그리드 초기화 함수 호출
            InitGrid();
            //Label 초기화 함수
            InitLabel();
            // 콤보박스 초기화 함수
            InitComboBox();

            // ExtandableGroupBox 설정
            this.uGroupBoxContentsArea.Expanded = false;

            //this.uGridInspectItemList.Height = 700;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        // 툴바 활성화 이벤트
        private void frmMAS0019_Activated(object sender, EventArgs e)
        {
            // 툴바상태 지정
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        #region 컨트롤초기화

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    //MessageBox.Show(this.Tag.ToString());
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        MessageBox.Show(this.Tag.ToString());
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void InitTextBox()
        {
            try
            {
                this.uTextInspectItemCode.MaxLength = 20;
                this.uTextInspectItemName.MaxLength = 50;
                this.uTextInspectItemNameCh.MaxLength = 50;
                this.uTextInspectItemNameEn.MaxLength = 50;
                this.uTextInspectCondition.MaxLength = 100;
                this.uTextInspectMethod.MaxLength = 100;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리도 초기화
        /// </summary>
        public void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // 그리드 일반속성
                grd.mfInitGeneralGrid(this.uGridInspectItemList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼추가
                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "InspectGroupName", "검사분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "InspectTypeName", "검사유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "InspectItemCode", "검사항목코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false
                    , 20, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "InspectItemName", "검사항목명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "InspectItemNameCh", "검사항목명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "InspectItemNameEn", "검사항목명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "DataType", "데이타유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 2, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "UseDecimalPoint", "허용소수점자리수", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 10, Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "UnitCode", "단위명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "InspectCondition", "검사조건", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 100, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "InspectMethod", "검사방법", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false
                    , 100, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false
                    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridInspectItemList, 0, "MDMTFlag", "MDM전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true
                    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                //폰트설정
                uGridInspectItemList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridInspectItemList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                ////////// DropDown 컬럼의 DropDownBox설정
                ////////// Plant
                ////////// BL 연결
                ////////QRPBrowser brwChannel = new QRPBrowser();
                ////////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                ////////QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                ////////brwChannel.mfCredentials(clsPlant);

                ////////DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                ////////grd.mfSetGridColumnValueList(this.uGridInspectItemList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                ////////// InspectGroup
                ////////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                ////////QRPMAS.BL.MASQUA.InspectGroup iGroup = new QRPMAS.BL.MASQUA.InspectGroup();
                ////////brwChannel.mfCredentials(iGroup);

                ////////dt = iGroup.mfReadMASInspectGroupCombo("", m_resSys.GetString("SYS_LANG"));

                ////////grd.mfSetGridColumnValueList(this.uGridInspectItemList, 0, "InspectGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                //////////InspectType
                ////////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                ////////QRPMAS.BL.MASQUA.InspectType iType = new QRPMAS.BL.MASQUA.InspectType();
                ////////brwChannel.mfCredentials(iType);

                ////////dt = iType.mfReadMASInspectTypeForCombo("", "", m_resSys.GetString("SYS_LANG"));

                ////////grd.mfSetGridColumnValueList(this.uGridInspectItemList, 0, "InspectTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                ////////// Unit
                ////////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                ////////QRPMAS.BL.MASGEN.Unit unit = new QRPMAS.BL.MASGEN.Unit();
                ////////brwChannel.mfCredentials(unit);

                ////////dt = unit.mfReadMASUnitForCombo();

                ////////grd.mfSetGridColumnValueList(this.uGridInspectItemList, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                ////////// UseFlag
                ////////dt = null;
                ////////dt = new DataTable();
                ////////dt.Columns.Add("Key", typeof(String));
                ////////dt.Columns.Add("Value", typeof(String));

                ////////DataRow row = dt.NewRow();
                ////////DataRow row1 = dt.NewRow();

                ////////row[0] = "T";
                ////////row[1] = "사용함";
                ////////dt.Rows.Add(row);

                ////////row1[0] = "F";
                ////////row1[1] = "사용안함";
                ////////dt.Rows.Add(row1);

                //////////그리드 컬럼에 콤보박스 추가
                ////////grd.mfSetGridColumnValueList(this.uGridInspectItemList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                // 공백줄 추가
                //grd.mfAddRowGrid(this.uGridInspectItemList, 0);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        public void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // label Instance
                WinLabel lb = new WinLabel();

                lb.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(this.uLabelSearchInspectGroup, "검사분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(this.uLabelSearchInspectType, "검사유형", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lb.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelInspectGroup, "검사분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelInspectType, "검사유형", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelInspectItemCode, "검사항목코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelInspectItemName, "검사항목명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelInspectItemNameCh, "검사항목명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(this.uLabelInspectItemNameEn, "검사항목명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(this.uLabelInspectCondition, "검사조건", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(this.uLabelInspectMethod, "검사방법", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(this.uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelDataType, "데이터유형", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelUnit, "단위", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelUseDecimalPoint, "허용소수점자리수", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lb.mfSetLabel(this.uLabelSelectItem, "선택항목", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lb.mfSetLabel(this.uLabelSPCNFlag, "SPCN여부", m_resSys.GetString("SYS_FONTNAME"), true, false);    //2012-12-07 추가
                // 선택항목 Hidden 처리
                this.uLabelSelectItem.Visible = false;
            }
            catch (Exception ex) 
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // WinComboEditor Instance
                WinComboEditor combo = new WinComboEditor();

                string strLang = m_resSys.GetString("SYS_LANG");
        

                // Plant ComboBox
                // BL연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 검색조건 공장 콤보박스 설정
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName"
                    , dt);
                // 입력용 공장 콤보박스 설정
                combo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName"
                    , dt);

                // 단위정보 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit unit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(unit);

                dt = unit.mfReadMASUnitCombo();

                combo.mfSetComboEditor(this.uComboUnit, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", "", "선택", "UnitCode", "UnitName"
                    , dt);

                // 사용여부 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                dt = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboUseFlag, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "T", "", "선탯"
                    , "ComCode", "ComCodeName", dt);

                //데이터 유형 콤보박스
                dt = clsComCode.mfReadCommonCode("C0002", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboDataType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComCode", "ComCodeName", dt);

                // 선택항목 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUserComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUserComCode);

                dt = clsUserComCode.mfReadUserCommonGubunCode("QUA", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboSelectItem, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_LANG")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, "", "", "선택"
                    , "ComGubunCode", "ComGubunName", dt);

                this.uComboSelectItem.Visible = false;

            }
            catch (Exception ex) 
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            {
            }
        }
        #endregion

        #region 툴바
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 조회를 위한 Plant/Routing 변수
                string strSearchPlantCode = this.uComboSearchPlant.Value.ToString();
                string strSearchInspectGroupCode = this.uComboSearchInspectGroup.Value.ToString();
                string strSearchInspectTypeCode = this.uComboSearchInspectType.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 호출 //
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                QRPMAS.BL.MASQUA.InspectItem iItem = new QRPMAS.BL.MASQUA.InspectItem();
                brwChannel.mfCredentials(iItem);
                DataTable dtInspectItem = new DataTable();

                // Method 호출 //
                dtInspectItem = iItem.mfReadMASInspectItem(strSearchPlantCode, strSearchInspectGroupCode, strSearchInspectTypeCode, m_resSys.GetString("SYS_LANG"));
                this.uGridInspectItemList.DataSource = dtInspectItem;
                this.uGridInspectItemList.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // ContentGroupBox 접은상태로
                this.uGroupBoxContentsArea.Expanded = false;

                DialogResult DResult = new DialogResult();
                
                if (dtInspectItem.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridInspectItemList, 0);

                    for (int i = 0; i < this.uGridInspectItemList.Rows.Count; i++)
                    {
                        if (!this.uGridInspectItemList.Rows[i].Cells["MDMTFlag"].Value.ToString().Equals("T"))
                            this.uGridInspectItemList.Rows[i].Appearance.BackColor = Color.Salmon;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001027", "M001028", Infragistics.Win.HAlign.Right);

                    //this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    // BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem iItem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(iItem);

                    // 매개변수용 DataTable
                    DataTable dtInspectItem = iItem.mfSetDataInfo();

                    // 필수입력사항 확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uComboInspectGroup.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000178", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboInspectGroup.DropDown();
                        return;
                    }
                    else if (this.uComboInspectType.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000184", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboInspectType.DropDown();
                        return;
                    }
                    else if (this.uTextInspectItemCode.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000197", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextInspectItemCode.Focus();
                        return;
                    }
                    else if (this.uTextInspectItemName.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000191", Infragistics.Win.HAlign.Center);

                        // 포커스
                        this.uTextInspectItemName.Focus();
                        return;
                    }
                    ////else if (this.uTextInspectItemNameCh.Text == "")
                    ////{
                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                    , "확인창", "필수입력사항 확인", "검사항목명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

                    ////    // Focus
                    ////    this.uTextInspectItemNameCh.Focus();
                    ////    return;
                    ////}
                    ////else if (this.uTextInspectItemNameEn.Text == "")
                    ////{
                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                    , "확인창", "필수입력사항 확인", "검사항목명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

                    ////    // Focus
                    ////    this.uTextInspectItemNameEn.Focus();
                    ////    return;
                    ////}
                    else if (this.uComboUseFlag.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000619", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboUseFlag.DropDown();
                        return;
                    }
                    else if (this.uComboDataType.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000373", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboDataType.DropDown();
                        return;
                    }
                    else if (this.uComboUnit.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000361", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboUnit.DropDown();
                        return;
                    }
                    else
                    {
                        //검사항목 신규등록시 기존에 검사항목코드가 있는지 체크
                        if (uTextInspectItemCode.ReadOnly == false)
                        {
                            DataTable dtItem = iItem.mfReadMASInspectItemDetail(this.uComboPlant.Value.ToString(), this.uTextInspectItemCode.Text, m_resSys.GetString("SYS_LANG"));
                            if (dtItem.Rows.Count > 0)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000881", "M000846", Infragistics.Win.HAlign.Center);
                                this.uTextInspectItemCode.Text = "";
                                this.uTextInspectItemCode.Focus();
                                return;
                            }
                        }

                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////

                        string strServer = m_resSys.GetString("SYS_SERVERPATH") == null ? "" : m_resSys.GetString("SYS_SERVERPATH");

                        DataRow row = dtInspectItem.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["InspectGroupCode"] = this.uComboInspectGroup.Value.ToString();
                        row["InspectTypeCode"] = this.uComboInspectType.Value.ToString();
                        row["InspectItemCode"] = this.uTextInspectItemCode.Text;
                        row["InspectItemName"] = this.uTextInspectItemName.Text;
                        row["InspectItemNameCh"] = this.uTextInspectItemNameCh.Text;
                        row["InspectItemNameEn"] = this.uTextInspectItemNameEn.Text;
                        row["InspectCondition"] = this.uTextInspectCondition.Text;
                        row["InspectMethod"] = this.uTextInspectMethod.Text;
                        row["DataTypeCode"] = this.uComboDataType.Value.ToString();
                        if (this.uComboDataType.Value.ToString() == "5")
                            row["ComGubunCode"] = this.uComboSelectItem.Value.ToString();
                        row["UnitCode"] = this.uComboUnit.Value.ToString();
                        row["UseDecimalPoint"] = this.uNumUseDecimalPoint.Value.ToString();
                        row["UseFlag"] = this.uComboUseFlag.Value.ToString();

                        if (strServer.Contains("10.61.61.71") || strServer.Contains("10.61.61.73")) // 라이브적용시 변경요망 2012-07-05
                            row["SendMDM"] = "T";

                        row["SPCNFlag"] = this.uCheckSPCNFlag.Checked == true ? "T":"F";   //2012-12-07 SPCN여부 추가
                        
                        dtInspectItem.Rows.Add(row);
                    }

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", m_resSys.GetString("SYS_LANG")));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // Method 호출
                        string rtMSG = iItem.mfSaveMASInspectItem(dtInspectItem, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // Decoding //
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        // 처리로직 끝//

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        DialogResult DResult = new DialogResult();
                        // 처리결과에 따른 메세지 박스
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M000632", "M000633", Infragistics.Win.HAlign.Right);

                    //this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    // BL 호출
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                    QRPMAS.BL.MASQUA.InspectItem iItem = new QRPMAS.BL.MASQUA.InspectItem();
                    brwChannel.mfCredentials(iItem);

                    // 매개변수용 DataTable
                    DataTable dtInspectItem = iItem.mfSetDataInfo();

                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uComboInspectGroup.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000178", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboInspectGroup.DropDown();
                        return;
                    }
                    else if (this.uComboInspectType.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000184", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboInspectType.DropDown();
                        return;
                    }
                    else if (this.uTextInspectItemCode.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000197", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextInspectItemCode.Focus();
                        return;
                    }
                    else
                    {
                        DataRow row = dtInspectItem.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["InspectGroupCode"] = this.uComboInspectGroup.Value.ToString();
                        row["InspectTypeCode"] = this.uComboInspectType.Value.ToString();
                        row["InspectItemCode"] = this.uTextInspectItemCode.Text;
                        dtInspectItem.Rows.Add(row);
                    }

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M000650", "M000675",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", m_resSys.GetString("SYS_LANG")));
                        this.MdiParent.Cursor = Cursors.WaitCursor;  

                        // 처리 로직 //
                        // 함수호출
                        string rtMSG = iItem.mfDeleteMASInspectItem(dtInspectItem);

                        // Decoding //
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        // 처리로직 끝 //

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        DialogResult DResult = new DialogResult();
                        // 삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000926",
                                                Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000923",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
                else
                {
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }

        /// <summary>
        /// 엑셀다운
        /// </summary>
        public void mfExcel()
        {
            try
            {
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                grd.mfDownLoadGridToExcel(this.uGridInspectItemList);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트
        // 검색용 공장 콤보박스값에 따라 검사분류 콤보박스 설정
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 변수 설정
                string strSearchPlantCode = this.uComboSearchPlant.Value.ToString();
                WinComboEditor combo = new WinComboEditor();
                DataTable dtInspectGroup = new DataTable();
                // 콤보박스 Clear
                this.uComboSearchInspectGroup.Items.Clear();

                if (!strSearchPlantCode.Equals(string.Empty))
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                    QRPMAS.BL.MASQUA.InspectGroup iGroup = new QRPMAS.BL.MASQUA.InspectGroup();
                    brwChannel.mfCredentials(iGroup);

                    dtInspectGroup = iGroup.mfReadMASInspectGroupCombo(strSearchPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                string strLang = m_resSys.GetString("SYS_LANG");
                string strAll = "";
                if (strLang.Equals("KOR"))
                    strAll = "전체";
                else if (strLang.Equals("CHN"))
                    strAll = "全部";
                else if (strLang.Equals("ENG"))
                    strAll = "All";

                combo.mfSetComboEditor(this.uComboSearchInspectGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", strAll, "InspectGroupCode", "InspectGroupName"
                    , dtInspectGroup);
            }
            catch (Exception ex) 
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }
        // 입력용 공장콤보박스값에 따라 검사분류 콤보박스 설정
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 변수 설정
                string strPlantCode = this.uComboPlant.Value.ToString();
                WinComboEditor combo = new WinComboEditor();
                DataTable dtInspectGroup = new DataTable();
                // 콤보박스 Clear
                this.uComboInspectGroup.Items.Clear();

                if (strPlantCode != "")
                {
                    QRPMAS.BL.MASQUA.InspectGroup iGroup;
                    if (m_bolDebugMode == false)
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                        iGroup = new QRPMAS.BL.MASQUA.InspectGroup();
                        brwChannel.mfCredentials(iGroup);
                    }
                    else
                        iGroup = new QRPMAS.BL.MASQUA.InspectGroup(m_strDBConn);

                    dtInspectGroup = iGroup.mfReadMASInspectGroupCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                string strLang = m_resSys.GetString("SYS_LANG");
                string strChoice = "";
                if (strLang.Equals("KOR"))
                    strChoice = "선택";
                else if (strLang.Equals("CHN"))
                    strChoice = "选择";
                else if (strLang.Equals("ENG"))
                    strChoice = "Choice";

                combo.mfSetComboEditor(this.uComboInspectGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", strChoice, "InspectGroupCode", "InspectGroupName"
                    , dtInspectGroup);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }

        // 검색용 검사분류 콤보박스값에 따라 검사유형 콤보박스 설정
        private void uComboSearchInspectGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strInspectGroupCode = this.uComboSearchInspectGroup.Value.ToString();
                WinComboEditor combo = new WinComboEditor();
                DataTable dtInspectType = new DataTable();
                // 콤보박스 Clear
                this.uComboSearchInspectType.Items.Clear();

                if (strInspectGroupCode != "")
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType iType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(iType);

                    dtInspectType = iType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));
                }

                string strLang = m_resSys.GetString("SYS_LANG");
                string strAll = "";
                if (strLang.Equals("KOR"))
                    strAll = "전체";
                else if (strLang.Equals("CHN"))
                    strAll = "全部";
                else if (strLang.Equals("ENG"))
                    strAll = "All";

                combo.mfSetComboEditor(this.uComboSearchInspectType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", strAll, "InspectTypeCode", "InspectTypeName"
                    , dtInspectType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

        // 입력용 검사분류 콤보박스값에 따라 검사유형 콤보박스 설정
        private void uComboInspectGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 변수 설정
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strInspectGroupCode = this.uComboInspectGroup.Value.ToString();
                WinComboEditor combo = new WinComboEditor();
                DataTable dtInspectType = new DataTable();
                // 콤보박스 Clear
                this.uComboInspectType.Items.Clear();

                if (strInspectGroupCode != "")
                {
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                    QRPMAS.BL.MASQUA.InspectType iType = new QRPMAS.BL.MASQUA.InspectType();
                    brwChannel.mfCredentials(iType);

                    dtInspectType = iType.mfReadMASInspectTypeForCombo(strPlantCode, strInspectGroupCode, m_resSys.GetString("SYS_LANG"));
                }

                string strLang = m_resSys.GetString("SYS_LANG");
                string strChoice = "";
                if (strLang.Equals("KOR"))
                    strChoice = "선택";
                else if (strLang.Equals("CHN"))
                    strChoice = "选择";
                else if (strLang.Equals("ENG"))
                    strChoice = "Choice";

                combo.mfSetComboEditor(this.uComboInspectType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, "", "", strChoice, "InspectTypeCode", "InspectTypeName"
                    , dtInspectType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            {
            }
        }

        // 검사유형 선택시 BomCheckFlag 가져오는 구문
        private void uComboInspectType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 변수 설정
                string strPlantCode = this.uComboPlant.Value.ToString();
                string strInspectGroupCode = this.uComboInspectGroup.Value.ToString();
                string strInspectTypeCode = this.uComboInspectType.Value.ToString();
                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                QRPMAS.BL.MASQUA.InspectType clsType = new QRPMAS.BL.MASQUA.InspectType();
                brwChannel.mfCredentials(clsType);

                DataTable dtBomCheck = clsType.mfReadMASInspectType_BomChecFlag(strPlantCode, strInspectGroupCode, strInspectTypeCode);

                if (dtBomCheck.Rows.Count > 0)
                {
                    if (dtBomCheck.Rows[0]["BomCheckFlag"].ToString().Equals("T"))
                    {
                        this.uComboDataType.Value = "5";
                    }
                    else
                    {
                        this.uComboDataType.Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // uGroupBoxContentsArea 펼침상태 변화 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInspectItemList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInspectItemList.Height = 730;

                    for (int i = 0; i < uGridInspectItemList.Rows.Count; i++)
                    {
                        uGridInspectItemList.Rows[i].Fixed = false;
                    }
                    Clear();
                }
            }
            catch (Exception ex) 
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }

        // 그리드 더블클릭시 이벤트
        private void uGridInspectItemList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }
                ////// 그룹박스의 각 컴포넌트들의 값에 그리드의 값 대입
                ////this.uComboPlant.Value = e.Cell.Row.Cells["PlantCode"].Value;
                ////this.uComboInspectGroup.Value = e.Cell.Row.Cells["InspectGroupName"].Value;
                ////this.uComboInspectType.Value = e.Cell.Row.Cells["InspectTypeName"].Value;
                ////this.uTextInspectItemCode.Text = e.Cell.Row.Cells["InspectItemCode"].Value.ToString();
                ////this.uTextInspectItemName.Text = e.Cell.Row.Cells["InspectItemName"].Value.ToString();
                ////this.uTextInspectItemNameCh.Text = e.Cell.Row.Cells["InspectItemNameCh"].Value.ToString();
                ////this.uTextInspectItemNameEn.Text = e.Cell.Row.Cells["InspectItemNameEn"].Value.ToString();
                ////this.uTextInspectCondition.Text = e.Cell.Row.Cells["InspectCondition"].Value.ToString();
                ////this.uTextInspectMethod.Text = e.Cell.Row.Cells["InspectMethod"].Value.ToString();
                ////this.uComboUseFlag.Value = e.Cell.Row.Cells["UseFlag"].Value;
                ////this.uComboUnit.Value = e.Cell.Row.Cells["UnitCode"].Value;
                ////this.uComboDataType.Value = e.Cell.Row.Cells["DataType"].Value;
                ////this.uComboSelectItem.Value = e.Cell.
                ////this.uNumUseDecimalPoint.Value = e.Cell.Row.Cells["UseDecimalPoint"].Value;

                // 검색조건용 변수 설정
                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strInspectItemCode = e.Cell.Row.Cells["InspectItemCode"].Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectItem), "InspectItem");
                QRPMAS.BL.MASQUA.InspectItem clsItem = new QRPMAS.BL.MASQUA.InspectItem();
                brwChannel.mfCredentials(clsItem);

                DataTable dtItem = clsItem.mfReadMASInspectItemDetail(strPlantCode, strInspectItemCode, m_resSys.GetString("SYS_LANG"));

                this.uComboPlant.Value = dtItem.Rows[0]["PlantCode"].ToString();
                this.uComboInspectGroup.Value = dtItem.Rows[0]["InspectGroupCode"].ToString();
                this.uComboInspectType.Value = dtItem.Rows[0]["InspectTypeCode"].ToString();
                this.uTextInspectItemCode.Text = dtItem.Rows[0]["InspectItemCode"].ToString();
                this.uTextInspectItemName.Text = dtItem.Rows[0]["InspectItemName"].ToString();
                this.uTextInspectItemNameCh.Text = dtItem.Rows[0]["InspectItemNameCh"].ToString();
                this.uTextInspectItemNameEn.Text = dtItem.Rows[0]["InspectItemNameEn"].ToString();
                this.uTextInspectCondition.Text = dtItem.Rows[0]["InspectCondition"].ToString();
                this.uTextInspectMethod.Text = dtItem.Rows[0]["InspectMethod"].ToString();
                this.uComboUseFlag.Value = dtItem.Rows[0]["UseFlag"].ToString();
                this.uComboUnit.Value = dtItem.Rows[0]["UnitCode"].ToString();
                this.uComboDataType.Value = dtItem.Rows[0]["DataType"].ToString();
                this.uComboSelectItem.Value = dtItem.Rows[0]["ComGubunCode"].ToString();
                int Value;
                bool result = Int32.TryParse(dtItem.Rows[0]["UseDecimalPoint"].ToString(), out Value);
                this.uNumUseDecimalPoint.Value = Value;

                this.uCheckSPCNFlag.Checked = dtItem.Rows[0]["SPCNFlag"].ToString() == "T" ? true : false;

                // PK 편집불가로 설정
                this.uComboPlant.Enabled = false;
                this.uTextInspectItemCode.Appearance.BackColor = Color.Gainsboro;
                this.uTextInspectItemCode.ReadOnly = true;
            }
            catch (Exception ex) 
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            {
            }
        }

        // 허용소수점자리 SpinButton 클릭 이벤트
        private void uNumUseDecimalPoint_EditorSpinButtonClick(object sender, Infragistics.Win.UltraWinEditors.SpinButtonClickEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 현재 NumericEditor 의 값을 int형 변수에 저장
                int intTemp = (int)ed.Value;

                // 증가버튼 클릭시 변수의 값을 1 증가시킨후 Editor에 값을 대입
                if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.NextItem)
                {
                    intTemp += 1;
                    ed.Value = intTemp;
                }
                // 감소버튼 클릭시 변수의 값을 1 감소시킨후 Editor에 값을 대입
                else if (e.ButtonType == Infragistics.Win.UltraWinEditors.SpinButtonItem.PreviousItem && intTemp > 0)
                {
                    intTemp -= 1;
                    ed.Value = intTemp;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // NumericEditor 왼쪽 버튼 클릭 이벤트
        private void uNumUseDecimalPoint_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                Infragistics.Win.UltraWinEditors.UltraNumericEditor ed = sender as Infragistics.Win.UltraWinEditors.UltraNumericEditor;

                // 값을 0으로 초기화
                ed.Value = 0;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// Control 초기화 Method
        /// </summary>
        private void Clear()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE"); //"";
                this.uTextInspectItemCode.Clear();
                this.uTextInspectItemName.Clear();
                this.uTextInspectItemNameCh.Clear();
                this.uTextInspectItemNameEn.Clear();
                this.uTextInspectCondition.Clear();
                this.uTextInspectMethod.Clear();
                this.uComboDataType.Value = "";
                this.uComboUnit.Value = "";
                this.uComboUseFlag.Value = "T";
                this.uNumUseDecimalPoint.Value = 0;

                this.uCheckSPCNFlag.Checked = false;    // SPCN여부 2012-12-07추가

                // PK 편집가능 상태로
                this.uComboPlant.Enabled = true;
                this.uTextInspectItemCode.Appearance.BackColor = Color.PowderBlue;
                this.uTextInspectItemCode.ReadOnly = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMAS0019_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        // 데이터 유형콤보박스 선택 선택시 선택항목 콤보박스 보이도록
        private void uComboDataType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uComboDataType.Value.ToString() == "5")
                {
                    this.uComboSelectItem.Visible = true;
                    this.uLabelSelectItem.Visible = true;
                }
                else
                {
                    this.uComboSelectItem.Visible = false;
                    this.uLabelSelectItem.Visible = false;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMAS0019_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
