﻿namespace QRPMAS.UI
{
    partial class frmMAS0019_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0019_S));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.UltraWinEditors.SpinEditorButton spinEditorButton1 = new Infragistics.Win.UltraWinEditors.SpinEditorButton();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelSearchInspectType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchInspectGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchInspectGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridInspectItemList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uLabelSelectItem = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSelectItem = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uNumUseDecimalPoint = new Infragistics.Win.UltraWinEditors.UltraNumericEditor();
            this.uTextInspectItemNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectItemNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectMethod = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectCondition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectItemName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInspectItemCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUseDecimalPoint = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUnit = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectItemNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectMethod = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectItemNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDataType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectCondition = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectItemName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInspectItemCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboUnit = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboDataType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboUseFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboInspectType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboInspectGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSPCNFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uCheckSPCNFlag = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInspectItemList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSelectItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumUseDecimalPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDataType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSPCNFlag)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspectGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchInspectGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uLabelSearchInspectType
            // 
            this.uLabelSearchInspectType.Location = new System.Drawing.Point(270, 12);
            this.uLabelSearchInspectType.Name = "uLabelSearchInspectType";
            this.uLabelSearchInspectType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectType.TabIndex = 5;
            this.uLabelSearchInspectType.Text = "ultraLabel3";
            // 
            // uLabelSearchInspectGroup
            // 
            this.uLabelSearchInspectGroup.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchInspectGroup.Name = "uLabelSearchInspectGroup";
            this.uLabelSearchInspectGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspectGroup.TabIndex = 4;
            this.uLabelSearchInspectGroup.Text = "ultraLabel2";
            // 
            // uComboSearchInspectType
            // 
            this.uComboSearchInspectType.Location = new System.Drawing.Point(374, 12);
            this.uComboSearchInspectType.Name = "uComboSearchInspectType";
            this.uComboSearchInspectType.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchInspectType.TabIndex = 3;
            // 
            // uComboSearchInspectGroup
            // 
            this.uComboSearchInspectGroup.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchInspectGroup.Name = "uComboSearchInspectGroup";
            this.uComboSearchInspectGroup.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchInspectGroup.TabIndex = 2;
            this.uComboSearchInspectGroup.ValueChanged += new System.EventHandler(this.uComboSearchInspectGroup_ValueChanged);
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(1028, 8);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(24, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(1004, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(20, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGridInspectItemList
            // 
            this.uGridInspectItemList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridInspectItemList.DisplayLayout.Appearance = appearance2;
            this.uGridInspectItemList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridInspectItemList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInspectItemList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInspectItemList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridInspectItemList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInspectItemList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridInspectItemList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridInspectItemList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridInspectItemList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridInspectItemList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridInspectItemList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridInspectItemList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridInspectItemList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridInspectItemList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridInspectItemList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridInspectItemList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInspectItemList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridInspectItemList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridInspectItemList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridInspectItemList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridInspectItemList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridInspectItemList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridInspectItemList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridInspectItemList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridInspectItemList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridInspectItemList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridInspectItemList.Location = new System.Drawing.Point(0, 80);
            this.uGridInspectItemList.Name = "uGridInspectItemList";
            this.uGridInspectItemList.Size = new System.Drawing.Size(1064, 760);
            this.uGridInspectItemList.TabIndex = 2;
            this.uGridInspectItemList.Text = "ultraGrid1";
            this.uGridInspectItemList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridInspectItemList_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 700);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 145);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 700);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uCheckSPCNFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSelectItem);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboSelectItem);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uNumUseDecimalPoint);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectItemNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectItemNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectMethod);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectCondition);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectItemName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextInspectItemCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseDecimalPoint);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSPCNFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUnit);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectItemNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectMethod);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectItemNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelDataType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectCondition);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectItemName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInspectItemCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUnit);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboDataType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboInspectType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboInspectGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 680);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uLabelSelectItem
            // 
            this.uLabelSelectItem.Location = new System.Drawing.Point(300, 204);
            this.uLabelSelectItem.Name = "uLabelSelectItem";
            this.uLabelSelectItem.Size = new System.Drawing.Size(110, 20);
            this.uLabelSelectItem.TabIndex = 29;
            this.uLabelSelectItem.Text = "ultraLabel5";
            // 
            // uComboSelectItem
            // 
            this.uComboSelectItem.Location = new System.Drawing.Point(416, 204);
            this.uComboSelectItem.Name = "uComboSelectItem";
            this.uComboSelectItem.Size = new System.Drawing.Size(144, 21);
            this.uComboSelectItem.TabIndex = 28;
            // 
            // uNumUseDecimalPoint
            // 
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            editorButton1.Text = "0";
            this.uNumUseDecimalPoint.ButtonsLeft.Add(editorButton1);
            spinEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uNumUseDecimalPoint.ButtonsRight.Add(spinEditorButton1);
            this.uNumUseDecimalPoint.Location = new System.Drawing.Point(128, 180);
            this.uNumUseDecimalPoint.Name = "uNumUseDecimalPoint";
            this.uNumUseDecimalPoint.Size = new System.Drawing.Size(144, 21);
            this.uNumUseDecimalPoint.TabIndex = 27;
            this.uNumUseDecimalPoint.EditorSpinButtonClick += new Infragistics.Win.UltraWinEditors.SpinButtonClickEventHandler(this.uNumUseDecimalPoint_EditorSpinButtonClick);
            this.uNumUseDecimalPoint.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uNumUseDecimalPoint_EditorButtonClick);
            // 
            // uTextInspectItemNameEn
            // 
            this.uTextInspectItemNameEn.Location = new System.Drawing.Point(128, 108);
            this.uTextInspectItemNameEn.Name = "uTextInspectItemNameEn";
            this.uTextInspectItemNameEn.Size = new System.Drawing.Size(300, 21);
            this.uTextInspectItemNameEn.TabIndex = 26;
            // 
            // uTextInspectItemNameCh
            // 
            this.uTextInspectItemNameCh.Location = new System.Drawing.Point(128, 84);
            this.uTextInspectItemNameCh.Name = "uTextInspectItemNameCh";
            this.uTextInspectItemNameCh.Size = new System.Drawing.Size(300, 21);
            this.uTextInspectItemNameCh.TabIndex = 25;
            // 
            // uTextInspectMethod
            // 
            this.uTextInspectMethod.Location = new System.Drawing.Point(128, 156);
            this.uTextInspectMethod.Name = "uTextInspectMethod";
            this.uTextInspectMethod.Size = new System.Drawing.Size(524, 21);
            this.uTextInspectMethod.TabIndex = 23;
            // 
            // uTextInspectCondition
            // 
            this.uTextInspectCondition.Location = new System.Drawing.Point(128, 132);
            this.uTextInspectCondition.Name = "uTextInspectCondition";
            this.uTextInspectCondition.Size = new System.Drawing.Size(524, 21);
            this.uTextInspectCondition.TabIndex = 22;
            // 
            // uTextInspectItemName
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextInspectItemName.Appearance = appearance15;
            this.uTextInspectItemName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextInspectItemName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectItemName.Location = new System.Drawing.Point(128, 60);
            this.uTextInspectItemName.Name = "uTextInspectItemName";
            this.uTextInspectItemName.Size = new System.Drawing.Size(300, 21);
            this.uTextInspectItemName.TabIndex = 21;
            // 
            // uTextInspectItemCode
            // 
            appearance14.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextInspectItemCode.Appearance = appearance14;
            this.uTextInspectItemCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextInspectItemCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextInspectItemCode.Location = new System.Drawing.Point(128, 36);
            this.uTextInspectItemCode.Name = "uTextInspectItemCode";
            this.uTextInspectItemCode.Size = new System.Drawing.Size(300, 21);
            this.uTextInspectItemCode.TabIndex = 20;
            // 
            // uLabelUseDecimalPoint
            // 
            this.uLabelUseDecimalPoint.Location = new System.Drawing.Point(12, 180);
            this.uLabelUseDecimalPoint.Name = "uLabelUseDecimalPoint";
            this.uLabelUseDecimalPoint.Size = new System.Drawing.Size(110, 20);
            this.uLabelUseDecimalPoint.TabIndex = 19;
            this.uLabelUseDecimalPoint.Text = "ultraLabel12";
            // 
            // uLabelUnit
            // 
            this.uLabelUnit.Location = new System.Drawing.Point(584, 180);
            this.uLabelUnit.Name = "uLabelUnit";
            this.uLabelUnit.Size = new System.Drawing.Size(110, 20);
            this.uLabelUnit.TabIndex = 18;
            this.uLabelUnit.Text = "ultraLabel11";
            // 
            // uLabelInspectType
            // 
            this.uLabelInspectType.Location = new System.Drawing.Point(588, 12);
            this.uLabelInspectType.Name = "uLabelInspectType";
            this.uLabelInspectType.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectType.TabIndex = 17;
            this.uLabelInspectType.Text = "ultraLabel10";
            // 
            // uLabelInspectItemNameEn
            // 
            this.uLabelInspectItemNameEn.Location = new System.Drawing.Point(12, 108);
            this.uLabelInspectItemNameEn.Name = "uLabelInspectItemNameEn";
            this.uLabelInspectItemNameEn.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectItemNameEn.TabIndex = 16;
            this.uLabelInspectItemNameEn.Text = "ultraLabel9";
            // 
            // uLabelInspectGroup
            // 
            this.uLabelInspectGroup.Location = new System.Drawing.Point(300, 12);
            this.uLabelInspectGroup.Name = "uLabelInspectGroup";
            this.uLabelInspectGroup.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectGroup.TabIndex = 15;
            this.uLabelInspectGroup.Text = "ultraLabel8";
            // 
            // uLabelInspectMethod
            // 
            this.uLabelInspectMethod.Location = new System.Drawing.Point(12, 156);
            this.uLabelInspectMethod.Name = "uLabelInspectMethod";
            this.uLabelInspectMethod.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectMethod.TabIndex = 14;
            this.uLabelInspectMethod.Text = "ultraLabel7";
            // 
            // uLabelInspectItemNameCh
            // 
            this.uLabelInspectItemNameCh.Location = new System.Drawing.Point(12, 84);
            this.uLabelInspectItemNameCh.Name = "uLabelInspectItemNameCh";
            this.uLabelInspectItemNameCh.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectItemNameCh.TabIndex = 13;
            this.uLabelInspectItemNameCh.Text = "ultraLabel6";
            // 
            // uLabelDataType
            // 
            this.uLabelDataType.Location = new System.Drawing.Point(300, 180);
            this.uLabelDataType.Name = "uLabelDataType";
            this.uLabelDataType.Size = new System.Drawing.Size(110, 20);
            this.uLabelDataType.TabIndex = 12;
            this.uLabelDataType.Text = "ultraLabel5";
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(12, 204);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(110, 20);
            this.uLabelUseFlag.TabIndex = 11;
            this.uLabelUseFlag.Text = "ultraLabel4";
            // 
            // uLabelInspectCondition
            // 
            this.uLabelInspectCondition.Location = new System.Drawing.Point(12, 132);
            this.uLabelInspectCondition.Name = "uLabelInspectCondition";
            this.uLabelInspectCondition.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectCondition.TabIndex = 10;
            this.uLabelInspectCondition.Text = "ultraLabel3";
            // 
            // uLabelInspectItemName
            // 
            this.uLabelInspectItemName.Location = new System.Drawing.Point(12, 60);
            this.uLabelInspectItemName.Name = "uLabelInspectItemName";
            this.uLabelInspectItemName.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectItemName.TabIndex = 9;
            this.uLabelInspectItemName.Text = "ultraLabel2";
            // 
            // uLabelInspectItemCode
            // 
            this.uLabelInspectItemCode.Location = new System.Drawing.Point(12, 36);
            this.uLabelInspectItemCode.Name = "uLabelInspectItemCode";
            this.uLabelInspectItemCode.Size = new System.Drawing.Size(110, 20);
            this.uLabelInspectItemCode.TabIndex = 8;
            this.uLabelInspectItemCode.Text = "ultraLabel1";
            // 
            // uComboUnit
            // 
            this.uComboUnit.Location = new System.Drawing.Point(700, 180);
            this.uComboUnit.Name = "uComboUnit";
            this.uComboUnit.Size = new System.Drawing.Size(144, 21);
            this.uComboUnit.TabIndex = 6;
            // 
            // uComboDataType
            // 
            this.uComboDataType.Location = new System.Drawing.Point(416, 180);
            this.uComboDataType.Name = "uComboDataType";
            this.uComboDataType.Size = new System.Drawing.Size(144, 21);
            this.uComboDataType.TabIndex = 5;
            this.uComboDataType.ValueChanged += new System.EventHandler(this.uComboDataType_ValueChanged);
            // 
            // uComboUseFlag
            // 
            this.uComboUseFlag.Location = new System.Drawing.Point(128, 204);
            this.uComboUseFlag.Name = "uComboUseFlag";
            this.uComboUseFlag.Size = new System.Drawing.Size(144, 21);
            this.uComboUseFlag.TabIndex = 4;
            // 
            // uComboInspectType
            // 
            this.uComboInspectType.Location = new System.Drawing.Point(704, 12);
            this.uComboInspectType.Name = "uComboInspectType";
            this.uComboInspectType.Size = new System.Drawing.Size(144, 21);
            this.uComboInspectType.TabIndex = 3;
            this.uComboInspectType.ValueChanged += new System.EventHandler(this.uComboInspectType_ValueChanged);
            // 
            // uComboInspectGroup
            // 
            this.uComboInspectGroup.Location = new System.Drawing.Point(416, 12);
            this.uComboInspectGroup.Name = "uComboInspectGroup";
            this.uComboInspectGroup.Size = new System.Drawing.Size(144, 21);
            this.uComboInspectGroup.TabIndex = 2;
            this.uComboInspectGroup.ValueChanged += new System.EventHandler(this.uComboInspectGroup_ValueChanged);
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(128, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(110, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uLabelSPCNFlag
            // 
            this.uLabelSPCNFlag.Location = new System.Drawing.Point(584, 204);
            this.uLabelSPCNFlag.Name = "uLabelSPCNFlag";
            this.uLabelSPCNFlag.Size = new System.Drawing.Size(110, 20);
            this.uLabelSPCNFlag.TabIndex = 18;
            this.uLabelSPCNFlag.Text = "ultraLabel11";
            // 
            // uCheckSPCNFlag
            // 
            this.uCheckSPCNFlag.Location = new System.Drawing.Point(700, 204);
            this.uCheckSPCNFlag.Name = "uCheckSPCNFlag";
            this.uCheckSPCNFlag.Size = new System.Drawing.Size(16, 16);
            this.uCheckSPCNFlag.TabIndex = 30;
            // 
            // frmMAS0019_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridInspectItemList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0019_S";
            this.Load += new System.EventHandler(this.frmMAS0019_Load);
            this.Activated += new System.EventHandler(this.frmMAS0019_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0019_FormClosing);
            this.Resize += new System.EventHandler(this.frmMAS0019_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchInspectGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInspectItemList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSelectItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNumUseDecimalPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInspectItemCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDataType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSPCNFlag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspectGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchInspectGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridInspectItemList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectCondition;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectItemName;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectItemCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUnit;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDataType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectMethod;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectCondition;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInspectItemCode;
        private Infragistics.Win.Misc.UltraLabel uLabelUseDecimalPoint;
        private Infragistics.Win.Misc.UltraLabel uLabelUnit;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectType;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectItemNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectMethod;
        private Infragistics.Win.Misc.UltraLabel uLabelInspectItemNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelDataType;
        private Infragistics.Win.UltraWinEditors.UltraNumericEditor uNumUseDecimalPoint;
        private Infragistics.Win.Misc.UltraLabel uLabelSelectItem;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSelectItem;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSPCNFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelSPCNFlag;
    }
}