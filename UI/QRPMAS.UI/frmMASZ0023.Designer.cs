﻿namespace QRPMAS.UI
{
    partial class frmMASZ0023
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0023));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchWorkName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchWorkName = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextWorkNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWorkNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWorkNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboUseFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWorkName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWorkCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWorkCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchWorkName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchWorkName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchWorkName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uTextSearchWorkName
            // 
            this.uTextSearchWorkName.Location = new System.Drawing.Point(384, 12);
            this.uTextSearchWorkName.Name = "uTextSearchWorkName";
            this.uTextSearchWorkName.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchWorkName.TabIndex = 5;
            // 
            // uLabelSearchWorkName
            // 
            this.uLabelSearchWorkName.Location = new System.Drawing.Point(280, 12);
            this.uLabelSearchWorkName.Name = "uLabelSearchWorkName";
            this.uLabelSearchWorkName.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchWorkName.TabIndex = 4;
            this.uLabelSearchWorkName.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance2;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 760);
            this.uGrid1.TabIndex = 2;
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWorkNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWorkNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWorkNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWorkNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWorkName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGrid2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonDeleteRow);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWorkName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWorkCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWorkCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextWorkNameEn
            // 
            appearance26.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkNameEn.Appearance = appearance26;
            this.uTextWorkNameEn.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkNameEn.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkNameEn.Location = new System.Drawing.Point(644, 36);
            this.uTextWorkNameEn.Name = "uTextWorkNameEn";
            this.uTextWorkNameEn.Size = new System.Drawing.Size(150, 21);
            this.uTextWorkNameEn.TabIndex = 20;
            // 
            // uLabelWorkNameEn
            // 
            this.uLabelWorkNameEn.Location = new System.Drawing.Point(540, 36);
            this.uLabelWorkNameEn.Name = "uLabelWorkNameEn";
            this.uLabelWorkNameEn.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkNameEn.TabIndex = 19;
            this.uLabelWorkNameEn.Text = "ultraLabel1";
            // 
            // uTextWorkNameCh
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkNameCh.Appearance = appearance28;
            this.uTextWorkNameCh.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkNameCh.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkNameCh.Location = new System.Drawing.Point(380, 36);
            this.uTextWorkNameCh.Name = "uTextWorkNameCh";
            this.uTextWorkNameCh.Size = new System.Drawing.Size(150, 21);
            this.uTextWorkNameCh.TabIndex = 18;
            // 
            // uLabelWorkNameCh
            // 
            this.uLabelWorkNameCh.Location = new System.Drawing.Point(276, 36);
            this.uLabelWorkNameCh.Name = "uLabelWorkNameCh";
            this.uLabelWorkNameCh.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkNameCh.TabIndex = 17;
            this.uLabelWorkNameCh.Text = "ultraLabel1";
            // 
            // uTextWorkName
            // 
            appearance27.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkName.Appearance = appearance27;
            this.uTextWorkName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkName.Location = new System.Drawing.Point(116, 36);
            this.uTextWorkName.Name = "uTextWorkName";
            this.uTextWorkName.Size = new System.Drawing.Size(150, 21);
            this.uTextWorkName.TabIndex = 16;
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance17;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance18.BorderColor = System.Drawing.Color.Silver;
            appearance18.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance18;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance24.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance23;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance21.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance21;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 116);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1044, 572);
            this.uGrid2.TabIndex = 15;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 84);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 14;
            this.uButtonDeleteRow.Text = "ultraButton1";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uTextEtc
            // 
            this.uTextEtc.Location = new System.Drawing.Point(116, 60);
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.Size = new System.Drawing.Size(680, 21);
            this.uTextEtc.TabIndex = 13;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 60);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEtc.TabIndex = 12;
            this.uLabelEtc.Text = "ultraLabel4";
            // 
            // uComboUseFlag
            // 
            this.uComboUseFlag.Location = new System.Drawing.Point(908, 36);
            this.uComboUseFlag.Name = "uComboUseFlag";
            this.uComboUseFlag.Size = new System.Drawing.Size(150, 21);
            this.uComboUseFlag.TabIndex = 11;
            this.uComboUseFlag.Text = "ultraComboEditor2";
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(804, 36);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelUseFlag.TabIndex = 10;
            this.uLabelUseFlag.Text = "ultraLabel1";
            // 
            // uLabelWorkName
            // 
            this.uLabelWorkName.Location = new System.Drawing.Point(12, 36);
            this.uLabelWorkName.Name = "uLabelWorkName";
            this.uLabelWorkName.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkName.TabIndex = 8;
            this.uLabelWorkName.Text = "ultraLabel1";
            // 
            // uTextWorkCode
            // 
            appearance29.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkCode.Appearance = appearance29;
            this.uTextWorkCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWorkCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWorkCode.Location = new System.Drawing.Point(380, 12);
            this.uTextWorkCode.Name = "uTextWorkCode";
            this.uTextWorkCode.Size = new System.Drawing.Size(150, 21);
            this.uTextWorkCode.TabIndex = 7;
            // 
            // uLabelWorkCode
            // 
            this.uLabelWorkCode.Location = new System.Drawing.Point(276, 12);
            this.uLabelWorkCode.Name = "uLabelWorkCode";
            this.uLabelWorkCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelWorkCode.TabIndex = 6;
            this.uLabelWorkCode.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 2;
            this.uComboPlant.Text = "ultraComboEditor1";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 1;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // frmMASZ0023
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0023";
            this.Load += new System.EventHandler(this.frmMASZ0023_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0023_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchWorkName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWorkCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchWorkName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchWorkName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkCode;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelWorkNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWorkName;
    }
}