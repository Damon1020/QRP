﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통기준정보                                          */
/* 모듈(분류)명 : 자재관리 기준정보                                     */
/* 프로그램ID   : frmMAS0007.cs                                         */
/* 프로그램명   : 자재정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0007 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0007()
        {
            InitializeComponent();
        }

        private void frmMAS0007_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);

        }

        private void frmMAS0007_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            titleArea.mfSetLabelText("자재정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method 호출
            SetToolAuth();
            InitGrid();
            InitCombo();
            InitLabel();
            InitValue();

            // 그룹박스 접힌 상태로 만듬
            this.uGroupBoxContentsArea.Expanded = false;

            this.Resize += new EventHandler(frmMAS0007_Resize);
        }

        void frmMAS0007_Resize(object sender, EventArgs e)
        {
            if (this.Width > 1070)
            {
                uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
            }
            else
            {
                uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            }
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 컴포넌트 초기화
        /// </summary>
        private void InitValue()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uComboPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                this.uComboMaterialGroup.Value = "";
                this.uComboMaterialType.Value = "";
                this.uComboConsumableType.Value = "";
                this.uComboInspectTypeFlag.Value = "";
                this.uComboUseFlag.Value = "";
                this.uTextSpec.Value = "";

                this.uTextMaterialCode.Text = "";
                this.uTextMaterialCode.MaxLength = 20;

                this.uTextMaterialName.Text = "";
                this.uTextMaterialName.MaxLength = 300;

                this.uTextMaterialNameCh.Text = "";
                this.uTextMaterialNameCh.MaxLength = 300;

                this.uTextMaterialNameEn.Text = "";
                this.uTextMaterialNameEn.MaxLength = 300;

                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;
                this.uTextMaterialCode.Appearance.BackColor = Color.PowderBlue;
                this.uTextMaterialName.Appearance.BackColor = Color.PowderBlue;

                
                this.uComboPlant.ReadOnly = false;
                this.uTextMaterialCode.ReadOnly = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridMateriallist, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit
                    , 100, false, true, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "PlantName", "공장", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialCode", "자재코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialName", "자재설명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialNameEn", "자재명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialNameCh", "자재명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "ConsumeUnitCode", "자재단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 150, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "Spec", "Spec", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 150, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialTypeCode", "자재유형코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialTypeName", "자재유형명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "ConsumableTypeCode", "자재종류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "ConsumableTypeName", "자재종류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialGroupCode", "자재그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialGorupName", "자재그룹명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridMateriallist, 0, "MaterialTypeName", "자재유형명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                ////    , 100, false, true, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "DRAWING_NO", "도면No", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                        , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "InspectFlag", "검사대상여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", " ");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                        , 100, false, false, 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridMateriallist, 0, "CreateType", "CreateType", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                        , 100, false, true, 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridMateriallist.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridMateriallist.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 검사대상여부 드랍다운 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtCom = clsCom.mfReadCommonCode("C0046", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridMateriallist, 0, "InspectFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, " ", "선택", dtCom);

                //////// 그리드에 DropDown 추가
                //////// 그리드 컬럼에 공장 콤보박스 추가
                //////DataTable dt = new DataTable();

                //////// BL 호출
                //////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                //////QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                //////brwChannel.mfCredentials(clsPlant);

                //////// Plant정보를 얻어오는 함수를 호출하여 DataTable에 저장
                //////dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(uGridMateriallist, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                //////// 그리드 컬럼에 자재그룹 Dropdown 추가
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialGroup), "MaterialGroup");
                //////QRPMAS.BL.MASMAT.MaterialGroup mg = new MaterialGroup();
                //////brwChannel.mfCredentials(mg);

                //////dt = mg.mfReadMASMaterialGroup("", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(uGridMateriallist, 0, "MaterialGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                //////// 자재유형
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialType), "MaterialType");
                //////QRPMAS.BL.MASMAT.MaterialType mt = new MaterialType();
                //////brwChannel.mfCredentials(mt);

                //////dt = mt.mfReadMASMaterialTypeCombo("", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(uGridMateriallist, 0, "MaterialTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                //////// 사용여부
                //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //////QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                //////brwChannel.mfCredentials(clsCommonCode);

                //////DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(this.uGridMateriallist, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                // 그리드 편집불가 상태로
                this.uGridMateriallist.DisplayLayout.Bands[0].Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                string strLang = m_resSys.GetString("SYS_LANG");
                string strChoice = "";
                string strAll = "";
                if (strLang.Equals("KOR"))
                { strChoice = "선택"; strAll = "전체"; }
                else if (strLang.Equals("CHN"))
                { strChoice = "选择"; strAll = "全部"; }
                else if (strLang.Equals("ENG"))
                { strChoice = "Choice"; strAll = "All"; }

                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", strAll, "PlantCode", "PlantName", dt);

                combo.mfSetComboEditor(uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", strAll, "PlantCode", "PlantName", dt);


                // 검사여부 콤보박스
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtInspectFlag = clsCommonCode.mfReadCommonCode("C0046", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboInspectFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", strAll
                    , "ComCode", "ComCodeName", dtInspectFlag);

                combo.mfSetComboEditor(this.uComboInspectTypeFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", strChoice
                    , "ComCode", "ComCodeName", dtInspectFlag);

                DataTable dtMaterialType = clsCommonCode.mfReadCommonCode("MaterialClass", m_resSys.GetString("SYS_LANG"));

                //자재유형콤보박스
                combo.mfSetComboEditor(this.uComboSearchMaterialType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", strAll
                    , "ComCode", "ComCodeName", dtMaterialType);

                combo.mfSetComboEditor(this.uComboMaterialType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", strChoice
                    , "ComCode", "ComCodeName", dtMaterialType);


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.ConsumableType), "ConsumableType");
                QRPMAS.BL.MASMAT.ConsumableType clsConsumableType = new QRPMAS.BL.MASMAT.ConsumableType();
                brwChannel.mfCredentials(clsConsumableType);

                DataTable dtCt = clsConsumableType.mfReadMASConsumableTypeCombo(m_resSys.GetString("SYS_LANG"));

                // 자재종류 콤보박스
                combo.mfSetComboEditor(this.uComboSearchConsumableType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", strAll, "ConsumableTypeCode", "ConsumableTypeName", dtCt);

                combo.mfSetComboEditor(this.uComboConsumableType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", strChoice, "ConsumableTypeCode", "ConsumableTypeName", dtCt);

                //사용여부
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboUseFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", strChoice, "ComCode", "ComCodeName", dtUseFlag);
                
                //자재그룹
                DataTable dtMG = clsComCode.mfReadCommonCode("MaterialGroup", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboMaterialGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", strChoice, "ComCode", "ComCodeName", dtMG);

                clsComCode.Dispose();
                clsCommonCode.Dispose();
                clsConsumableType.Dispose();
                clsPlant.Dispose();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchInspect, "검사대상", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchConsumableType, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                //label.mfSetLabel(uLabelSearchMaterialGroup, "자재그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchMaterialType, "자재유형", m_resSys.GetString("SYS_FONTNAME"), true, false);


                label.mfSetLabel(uLabelMaterialCode, "자재코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelMaterialName, "자재설명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                label.mfSetLabel(uLabelMaterialNameCh, "자재설명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMaterialNameEn, "자재설명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPlant, "공장명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelConsumableType, "자재종류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMaterialGroup, "자재그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMaterialType, "자재유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSpec, "Spec", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelUseflag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelInstpectTypeFlag, "검사대상여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }

        #endregion

        #region 툴바
        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                ////이전 저장 메소드
                ////SystemInfo 리소스
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                //DialogResult DResult = new DialogResult();

                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                //QRPMAS.BL.MASMAT.Material clsMat = new QRPMAS.BL.MASMAT.Material();
                //brwChannel.mfCredentials(clsMat);

                //DataTable dtSave = clsMat.mfSetDataInfo();
                //DataRow drRow;

                //// 수정된것만 저장
                //if (this.uGridMateriallist.Rows.Count > 0)
                //{
                //    this.uGridMateriallist.ActiveCell = this.uGridMateriallist.Rows[0].Cells[0];
                //    for (int i = 0; i < this.uGridMateriallist.Rows.Count; i++)
                //    {
                //        if (this.uGridMateriallist.Rows[i].RowSelectorAppearance.Image != null)
                //        {
                //            drRow = dtSave.NewRow();
                //            drRow["PlantCode"] = this.uGridMateriallist.Rows[i].Cells["PlantCode"].Value.ToString();
                //            drRow["MaterialCode"] = this.uGridMateriallist.Rows[i].Cells["MaterialCode"].Value.ToString();
                //            drRow["InspectFlag"] = this.uGridMateriallist.Rows[i].Cells["InspectFlag"].Value.ToString();
                //            dtSave.Rows.Add(drRow);
                //        }
                //    }
                //}

                //// 저장할 정보가 있으면 저장
                //if (dtSave.Rows.Count > 0)
                //{
                //    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                            "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                //                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                //    {
                //        // 프로그래스 팝업창 Open
                //        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //        Thread thread = m_ProgressPopup.mfStartThread();
                //        m_ProgressPopup.mfOpenProgressPopup(this, "저장중 입니다.");
                //        this.MdiParent.Cursor = Cursors.WaitCursor;

                //        string strErrRtn = clsMat.mfSaveMASMaterial(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                //        // 프로그래스 팝업창 Close
                //        this.MdiParent.Cursor = Cursors.Default;
                //        m_ProgressPopup.mfCloseProgressPopup(this);

                //        // 저장결과
                //        TransErrRtn ErrRtn = new TransErrRtn();
                //        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //        if (ErrRtn.ErrNum == 0)
                //        {
                //            // WMS 검사대상여부전송
                //            strErrRtn = clsMat.mfSaveMASmaterial_WMS_Oracle(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                //            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                //            if (ErrRtn.ErrMessage.Equals("00") && ErrRtn.ErrNum.Equals(0))
                //            {
                //                // WMSTFlag 저장
                //                strErrRtn = clsMat.mfSaveMASmaterial_WMSTFlag(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                //                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                //                if (ErrRtn.ErrNum == 0)
                //                {
                //                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.", Infragistics.Win.HAlign.Right);
                //                }
                //                else
                //                {
                //                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                            "처리결과", "WMS I/F 결과", "WMS I/F 는 성공하였으나, WMSFlag 저장중 오류가 발생하였습니다.", Infragistics.Win.HAlign.Right);
                //                }
                //            }
                //            else
                //            {
                //                DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                            "처리결과", "WMS I/F 결과", "WMS I/F 중 에러가 발생하였습니다.", Infragistics.Win.HAlign.Right);
                //            }
                //        }
                //        else
                //        {
                //            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                                 "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.", Infragistics.Win.HAlign.Right);
                //            return;
                //        }
                //    }
                //}
                //else
                //{
                //    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                                 "저장확인", "저장정보 조회결과", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                //    return;
                //}
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001135", "M001225", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboPlant.DropDown();
                    return;
                }
                else if (this.uTextMaterialCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                             , "M001135", "M001225", "M000975", Infragistics.Win.HAlign.Right);

                    this.uTextMaterialCode.Focus();
                    return;
                }
                else if (this.uTextMaterialName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001135", "M001225", "M000968", Infragistics.Win.HAlign.Right);

                    this.uTextMaterialName.Focus();
                    return;
                }
                else
                {
                    QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                    if (!check.mfCheckValidValueBeforSave(this)) return;

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                    QRPMAS.BL.MASMAT.Material clsMat = new QRPMAS.BL.MASMAT.Material();
                    brwChannel.mfCredentials(clsMat);

                    DataTable dtCheckPK = clsMat.mfReadMASMaterialDetail(this.uComboPlant.Value.ToString(), this.uTextMaterialCode.Text, m_resSys.GetString("SYSLANG"));

                    if (dtCheckPK.Rows.Count != 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                            , "M001135", "M000974", "M000850", Infragistics.Win.HAlign.Right);

                        this.uTextMaterialCode.Focus();
                        return;
                    }
                    else
                    {
                        DataTable dtSaveMat = clsMat.mfSetDataInfo();
                        DataRow dr = dtSaveMat.NewRow();

                        dr["PlantCode"] = this.uComboPlant.Value.ToString();
                        dr["MaterialCode"] = this.uTextMaterialCode.Text;
                        dr["MaterialName"] = this.uTextMaterialName.Text;
                        dr["MaterialNameCh"] = this.uTextMaterialNameCh.Text;
                        dr["MaterialNameEn"] = this.uTextMaterialNameEn.Text;
                        dr["MaterialGroupCode"] = this.uComboMaterialGroup.Value.ToString();
                        dr["MaterialTypeCode"] = this.uComboMaterialType.Value.ToString();
                        dr["ConsumableTypeCode"] = this.uComboConsumableType.Value.ToString();
                        dr["InspectFlag"] = this.uComboInspectTypeFlag.Value.ToString();
                        dr["Spec"] = this.uTextSpec.Text;
                        dr["UseFlag"] = this.uComboUseFlag.Value.ToString();
                        dr["CreateType"] = "QRP";

                        dtSaveMat.Rows.Add(dr);

                        DResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                            , "M001264", "M001053", "M000943", Infragistics.Win.HAlign.Right);
                        if (DResult == DialogResult.Yes)
                        {
                            // 프로그래스 팝업창 Open
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread thread = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, "저장중 입니다.");
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string strErrRtn = clsMat.mfSaveMASMaterialDetail(dtSaveMat, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                            // 프로그래스 팝업창 Close
                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);
                            // 저장결과
                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            if (ErrRtn.ErrNum == 0)
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000930", Infragistics.Win.HAlign.Right);

                                mfSearch();
                            }
                            else
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                     "M001135", "M001037", "M000953", Infragistics.Win.HAlign.Right);
                                return;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }

        public void mfDelete()
        {
            try
            {
            }
            catch 
            { 
            }
            finally 
            { 
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                InitValue();

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                QRPMAS.BL.MASMAT.Material material = new QRPMAS.BL.MASMAT.Material();
                brwChannel.mfCredentials(material);

                //검색조건정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //string strMaterialCode = "";
                //if(!this.uTextSearchMaterialCode.Text.Equals(string.Empty) && !this.uTextSearchMaterialName.Text.Equals(string.Empty))
                //{
                //    strMaterialCode = this.uTextSearchMaterialCode.Text;  
                //}
                string strMaterialCode = this.uTextSearchMaterialCode.Text;  
                string strMaterialTypeCode = this.uComboSearchMaterialType.Value.ToString();
                string strConsumableTypeCode = this.uComboSearchConsumableType.Value.ToString();
                string strInspectFlag = this.uComboInspectFlag.Value.ToString();

                DataTable dtMaterial = material.mfReadMASMaterial(strPlantCode, strMaterialCode, strMaterialTypeCode, strConsumableTypeCode, strInspectFlag,m_resSys.GetString("SYS_LANG"));

                this.uGridMateriallist.DataSource = dtMaterial;
                this.uGridMateriallist.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
               
                if (dtMaterial.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                         , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridMateriallist, 0);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                InitValue();
                this.uComboUseFlag.Value = "T";
            }
            catch 
            { 
            }
            finally 
            { 
            }
        }

        public void mfExcel()
        {
            //처리 로직//
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            grd.mfDownLoadGridToExcel(this.uGridMateriallist);
        }

        public void mfPrint()
        {
        }
        #endregion

        #region 이벤트
        //어떤공장을 선택여부에 따른 Value 바뀜
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //WinComboEditor combo = new WinComboEditor();

                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialGroup), "MaterialGroup");
                //QRPMAS.BL.MASMAT.MaterialGroup mg = new QRPMAS.BL.MASMAT.MaterialGroup();
                //brwChannel.mfCredentials(mg);

                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialType), "MaterialType");
                //QRPMAS.BL.MASMAT.MaterialType mt = new QRPMAS.BL.MASMAT.MaterialType();
                //brwChannel.mfCredentials(mt);

                //String strPlantCode = this.uComboSearchPlant.Value.ToString();
                //DataTable dtMaterialGroup = new DataTable();
                //DataTable dtMaterialType = new DataTable();
                //// 콤보박스 아이템 전체 삭제
                //this.uComboSearchMaterialGroup.Items.Clear();
                //this.uComboSearchMaterialType.Items.Clear();

                //// 공장콤보박스에서 값이 선택되었을경우
                //if (strPlantCode != "")
                //{
                //    dtMaterialGroup = mg.mfReadMASMaterialGroupCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                //    dtMaterialType = mt.mfReadMASMaterialTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                //}

                //combo.mfSetComboEditor(uComboSearchMaterialGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                //    , "", "", "전체", "MaterialGroupCode", "MaterialGroupName", dtMaterialGroup);

                //combo.mfSetComboEditor(uComboSearchMaterialType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                //    , "", "", "전체", "MaterialTypeCode", "MaterialTypeName", dtMaterialType);

            
            }
            catch { }
            finally { }
        }

        // 그룹박스 펼침 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGridMateriallist.Height = 80;
                    Point point = new Point(0, 180);
                    this.uGroupBoxContentsArea.Location = point;
                }
                else
                {
                    Point point = new Point(0, 815);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridMateriallist.Height = 731;

                    this.uGridMateriallist.Rows.FixedRows.Clear();
                }
            }
            catch
            {
            }
            finally
            {
            }
        }

        // 그리드 수정시 RowSelector 이미지 변화
        private void uGridMateriallist_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        #endregion

        private void frmMAS0007_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void uTextSearchMaterialCode_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                frmPOP0001 frmMaterial = new frmPOP0001();
                frmMaterial.PlantCode = strPlantCode;
                frmMaterial.ShowDialog();

                this.uTextSearchMaterialCode.Text = frmMaterial.MaterialCode;
                this.uTextSearchMaterialName.Text = frmMaterial.MaterialName;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextSearchMaterialCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (!this.uTextSearchMaterialCode.Text.Equals(string.Empty) && e.KeyData.Equals(Keys.Enter))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    //조회정보 저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();
                    string strMaterialCode = this.uTextSearchMaterialCode.Text;

                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                        return;
                    }


                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                    QRPMAS.BL.MASMAT.Material clsMaterial = new QRPMAS.BL.MASMAT.Material();
                    brwChannel.mfCredentials(clsMaterial);

                    DataTable dtMaterial = clsMaterial.mfReadMASMaterialDetail(strPlantCode, strMaterialCode, m_resSys.GetString("SYS_LANG"));

                    if (dtMaterial.Rows.Count > 0)
                    {
                        this.uTextSearchMaterialName.Text = dtMaterial.Rows[0]["MaterialName"].ToString();
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001104", "M000904", Infragistics.Win.HAlign.Right);
                        if (this.uTextSearchMaterialName.Text != "")
                        {
                            this.uTextSearchMaterialName.Clear();
                        }
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextSearchMaterialCode_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //코드정보가 변경시 코드명은 공백처리
                if (!this.uTextSearchMaterialName.Text.Equals(string.Empty))
                {
                    this.uTextSearchMaterialName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridMateriallist_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //DialogResult DResult = new DialogResult();
                //WinMessageBox msg = new WinMessageBox();
                //if (e.Row.Cells["CreateType"].Value.ToString().Equals("QRP"))
                //{
                    string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                    string strMaterialCode = e.Row.Cells["MaterialCode"].Value.ToString();

                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Material), "Material");
                    QRPMAS.BL.MASMAT.Material clsMat = new QRPMAS.BL.MASMAT.Material();
                    brwChannel.mfCredentials(clsMat);

                    DataTable dtDetail = clsMat.mfReadMASMaterialDetailInfo(strPlantCode, strMaterialCode);

                    this.uComboPlant.Value =  dtDetail.Rows[0]["PlantCode"].ToString();
                    this.uTextMaterialCode.Text = dtDetail.Rows[0]["MaterialCode"].ToString();
                    this.uTextMaterialName.Text = dtDetail.Rows[0]["MaterialName"].ToString();
                    this.uTextMaterialNameCh.Text = dtDetail.Rows[0]["MaterialNameCh"].ToString();
                    this.uTextMaterialNameEn.Text = dtDetail.Rows[0]["MaterialNameEn"].ToString();
                    this.uTextSpec.Text = dtDetail.Rows[0]["Spec"].ToString();
                    this.uComboUseFlag.Value = dtDetail.Rows[0]["UseFlag"].ToString();
                    this.uComboMaterialGroup.Value = dtDetail.Rows[0]["MaterialGroupCode"].ToString();
                    this.uComboMaterialType.Value = dtDetail.Rows[0]["MaterialTypeCode"].ToString();
                    this.uComboInspectTypeFlag.Value = dtDetail.Rows[0]["InspectFlag"].ToString();
                    this.uComboConsumableType.Value = dtDetail.Rows[0]["ConsumableTypeCode"].ToString();

                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Row.Fixed = true;

                    //검색후 PK 수정불가처리
                    this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                    this.uComboPlant.ReadOnly = true;
                    this.uTextMaterialCode.Appearance.BackColor = Color.Gainsboro;
                    this.uTextMaterialCode.ReadOnly = true;
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
    }
}