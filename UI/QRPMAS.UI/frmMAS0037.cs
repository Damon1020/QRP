﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMAS0037.cs                                         */
/* 프로그램명   : 통문사유정보                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-01                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0037 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmMAS0037()
        {
            InitializeComponent();
        }

        private void frmMAS0037_Activated(object sender, EventArgs e)
        {
            //툴바활성
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //사용여부설정
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0037_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            titleArea.mfSetLabelText("반입구분정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            //각컨트롤 초기화
            SetToolAuth();
            InitComboBox();

            InitGrid();
            InitLabel();
        }

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 컨트롤초기화 , 사용권한설정

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //레이블초기화
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //그리드초기화
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGrid, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                grd.mfSetGridColumn(this.uGrid, 0, "EquipCarryInGubunCode", "반입구분코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid, 0, "EquipCarryInGubunName", "반입구분명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid, 0, "EquipCarryInGubunNameCh", "반입구분명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid, 0, "EquipCarryInGubunNameEn", "반입구분명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드에 DropDown 추가
                // 그리드 컬럼에 공장 콤보박스 추가

                // BL 공장호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Plant정보를 얻어오는 함수를 호출하여 DataTable에 저장
                DataTable PlantList = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));


                grd.mfSetGridColumnValueList(uGrid, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", PlantList);
                //사용여부
                DataTable dtUseFlag = new DataTable();
                DataRow drUse;

                // QRPBrowser brwChannel = new QRPBrowser(); // 1
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode"); // 2
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode(); // 3
                brwChannel.mfCredentials(clsComCode); // 4

                dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG")); // 5

                //그리드 컬럼 전체에 적용하여 넣기(mfSetGridColumnValueList)
                grd.mfSetGridColumnValueList(this.uGrid, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag); 


                //한줄생성
                grd.mfAddRowGrid(this.uGrid, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region ToolBar

        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCarryInGubun), "EquipCarryInGubun");
                QRPMAS.BL.MASEQU.EquipCarryInGubun rslt = new QRPMAS.BL.MASEQU.EquipCarryInGubun();
                brwChannel.mfCredentials(rslt);

                String strPlantCode = this.uComboPlant.Value.ToString();

                DataTable dt = rslt.mfReadMASEquipCarryInGubun(strPlantCode, m_resSys.GetString("SYS_LANG"));

                this.uGrid.DataSource = dt;
                this.uGrid.DataBind();

                // 디비로 가져온 정보의 PK 편집 불가 상태로
                for (int i = 0; i < this.uGrid.Rows.Count; i++)
                {
                    this.uGrid.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid.Rows[i].Cells["EquipCarryInGubunCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                    this.uGrid.Rows[i].Cells["EquipCarryInGubunCode"].Appearance.BackColor = Color.Gainsboro;
                }

                WinGrid grd = new WinGrid();

                // 바인딩후 체크박스 상태를 모두 Uncheck로 만든다
                grd.mfSetAllUnCheckedGridColumn(this.uGrid, 0, "Check");

                // RowSelector Clear
                grd.mfClearRowSeletorGrid(this.uGrid);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                // BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCarryInGubun), "EquipCarryInGubun");
                QRPMAS.BL.MASEQU.EquipCarryInGubun rslt = new QRPMAS.BL.MASEQU.EquipCarryInGubun();
                brwChannel.mfCredentials(rslt);

                // 저장 함수호출 매개변수 DataTable
                DataTable dtEquipCarryInGubun = rslt.mfSetDataInfo();

                DialogResult DResult = new DialogResult();

                for (int i = 0; i < this.uGrid.Rows.Count; i++)
                {
                    this.uGrid.ActiveCell = this.uGrid.Rows[0].Cells[0];

                    //그리드가 수정되었을때 저장
                    if (this.uGrid.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        //필수 입력사항 확인
                        if (this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME")
                                , 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창"
                                , "필수입력사항", (i + 1) + "번째 열의 공장코드를 입력해주세요", Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["PlantCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["EquipCarryInGubunCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                                , (i + 1) + "번째 열의 반입구분코드를 입력해주세요", Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["EquipCarryInGubunCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["EquipCarryInGubunName"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                                , (i + 1) + "번째 열의 반입구분명을 입력해주세요", Infragistics.Win.HAlign.Center);

                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["EquipCarryInGubunName"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        ////else if (this.uGrid.Rows[i].Cells["EquipCarryInGubunNameCh"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                        ////        , (i + 1) + "번째 열의 반입구분명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["EquipCarryInGubunNameCh"];
                        ////    this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}
                        ////else if (this.uGrid.Rows[i].Cells["EquipCarryInGubunNameEn"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                        ////        , (i + 1) + "번째의 열의 반입구분명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["EquipCarryInGubunNameEn"];
                        ////    this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}
                        else
                        {
                            DataRow drEquipCarryInGubun = dtEquipCarryInGubun.NewRow();
                            drEquipCarryInGubun["PlantCode"] = this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString();
                            drEquipCarryInGubun["EquipCarryInGubunCode"] = this.uGrid.Rows[i].Cells["EquipCarryInGubunCode"].Value.ToString();
                            drEquipCarryInGubun["EquipCarryInGubunName"] = this.uGrid.Rows[i].Cells["EquipCarryInGubunName"].Value.ToString();
                            drEquipCarryInGubun["EquipCarryInGubunNameCh"] = this.uGrid.Rows[i].Cells["EquipCarryInGubunNameCh"].Value.ToString();
                            drEquipCarryInGubun["EquipCarryInGubunNameEn"] = this.uGrid.Rows[i].Cells["EquipCarryInGubunNameEn"].Value.ToString();
                            drEquipCarryInGubun["UseFlag"] = this.uGrid.Rows[i].Cells["UseFlag"].Value.ToString();
                            dtEquipCarryInGubun.Rows.Add(drEquipCarryInGubun);
                        }
                    }
                }


                if (dtEquipCarryInGubun.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        QRPProgressBar uProgressPopup = new QRPProgressBar();
                        Thread uTh = uProgressPopup.mfStartThread();
                        uProgressPopup.mfOpenProgressPopup(this, "저장중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //저장함수 호출
                        String strEquipCarryInGubun = rslt.mfSaveMASEquipCarryInGubun(dtEquipCarryInGubun, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        //Decoding
                        TransErrRtn ErrEtn = new TransErrRtn();
                        ErrEtn = ErrEtn.mfDecodingErrMessage(strEquipCarryInGubun);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        uProgressPopup.mfCloseProgressPopup(this);

                        //처리결과에 따른 메세지 박스
                        if (ErrEtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다."
                            , Infragistics.Win.HAlign.Right);
                            mfSearch();

                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다"
                            , Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {
                //SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCarryInGubun), "EquipCarryInGubun");
                QRPMAS.BL.MASEQU.EquipCarryInGubun rslt = new QRPMAS.BL.MASEQU.EquipCarryInGubun();
                brwChannel.mfCredentials(rslt);

                //함수호출 매개변수 DataTable
                DataTable dtEquipCarryInGubun = rslt.mfSetDataInfo();

                DialogResult DResult = new DialogResult();


                //chek된것 삭제
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    // 활성셀을 첫번째줄 첫번째 셀로 이동
                    this.uGrid.ActiveCell = this.uGrid.Rows[0].Cells[0];

                    if (Convert.ToBoolean(this.uGrid.Rows[i].Cells["Check"].Value) == true)
                    {
                        //필수입력사항 확인
                        if (this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", (i + 1) + "번째 열의 공장코드를 입력해주세요", Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["PlantCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["EquipCarryInGubunCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "확인창", "필수입력사항 확인", (i + 1) + "번째 열의 반입구분코드를 입력해주세요", Infragistics.Win.HAlign.Center);

                            //FocusCell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["EquipCarryInGubunCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            DataRow drEquipCarryInGubun = dtEquipCarryInGubun.NewRow();
                            drEquipCarryInGubun["PlantCode"] = this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString();
                            drEquipCarryInGubun["EquipCarryInGubunCode"] = this.uGrid.Rows[i].Cells["EquipCarryInGubunCode"].Value.ToString();
                            dtEquipCarryInGubun.Rows.Add(drEquipCarryInGubun);
                        }
                    }
                }

                if (dtEquipCarryInGubun.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?"
                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //함수호출
                        string strSG = rslt.mfDeleteMASEquipCarryInGubun(dtEquipCarryInGubun);

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strSG);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        //삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                 Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "처리결과", "삭제처리결과", "입력한 정보를 삭제하지 못했습니다.",
                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        public void mfCreate()
        {

        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "출력정보 확인", "엑셀 출력정보를 확인해주세요.", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                grd.mfDownLoadGridToExcel(this.uGrid);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        public void mfPrint()
        {
        }

        #endregion
    }
}
