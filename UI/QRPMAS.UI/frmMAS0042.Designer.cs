﻿namespace QRPMAS.UI
{
    partial class frmMAS0042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0042));
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboMold = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMold = new Infragistics.Win.Misc.UltraLabel();
            this.uComboDurableMatName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDurableMat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uTextDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDurableMatCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlantName = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uLabelLotNoInfo = new Infragistics.Win.Misc.UltraLabel();
            this.uGridModelList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridDurableLot = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupDurable = new Infragistics.Win.Misc.UltraGroupBox();
            this.uText = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupDetail = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboDeleteType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uButtonDeleteLotInfo = new Infragistics.Win.Misc.UltraButton();
            this.uButtonDeleteLot = new Infragistics.Win.Misc.UltraButton();
            this.uButton = new Infragistics.Win.Misc.UltraButton();
            this.uCheckSerial = new Infragistics.Win.UltraWinEditors.UltraCheckEditor();
            this.uComboDurableInven = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGroupBoxPackage = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboCustomer = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGridPackage = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridPackageList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonSearchP = new Infragistics.Win.Misc.UltraButton();
            this.uButtonPackageOK = new Infragistics.Win.Misc.UltraButton();
            this.uLabelCoustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonRowDel = new Infragistics.Win.Misc.UltraButton();
            this.uGroupBoxModel = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uGridModel = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonSearchM = new Infragistics.Win.Misc.UltraButton();
            this.uButtonModelOK = new Infragistics.Win.Misc.UltraButton();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelDurableInventory = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridModelList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDurable)).BeginInit();
            this.uGroupDurable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDetail)).BeginInit();
            this.uGroupDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDeleteType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSerial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDurableInven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxPackage)).BeginInit();
            this.uGroupBoxPackage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPackageList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxModel)).BeginInit();
            this.uGroupBoxModel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridModel)).BeginInit();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboMold);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMold);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(424, 12);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPackage.TabIndex = 3;
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(296, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchPackage.TabIndex = 2;
            // 
            // uComboMold
            // 
            this.uComboMold.Location = new System.Drawing.Point(1000, 12);
            this.uComboMold.MaxLength = 50;
            this.uComboMold.Name = "uComboMold";
            this.uComboMold.Size = new System.Drawing.Size(36, 21);
            this.uComboMold.TabIndex = 3;
            this.uComboMold.Visible = false;
            // 
            // uLabelSearchMold
            // 
            this.uLabelSearchMold.Location = new System.Drawing.Point(964, 12);
            this.uLabelSearchMold.Name = "uLabelSearchMold";
            this.uLabelSearchMold.Size = new System.Drawing.Size(32, 20);
            this.uLabelSearchMold.TabIndex = 2;
            this.uLabelSearchMold.Visible = false;
            // 
            // uComboDurableMatName
            // 
            this.uComboDurableMatName.Location = new System.Drawing.Point(140, 12);
            this.uComboDurableMatName.MaxLength = 50;
            this.uComboDurableMatName.Name = "uComboDurableMatName";
            this.uComboDurableMatName.Size = new System.Drawing.Size(144, 21);
            this.uComboDurableMatName.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(920, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(36, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelDurableMatName
            // 
            this.uLabelDurableMatName.Location = new System.Drawing.Point(12, 12);
            this.uLabelDurableMatName.Name = "uLabelDurableMatName";
            this.uLabelDurableMatName.Size = new System.Drawing.Size(124, 20);
            this.uLabelDurableMatName.TabIndex = 0;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(900, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGridDurableMat
            // 
            this.uGridDurableMat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableMat.DisplayLayout.Appearance = appearance2;
            this.uGridDurableMat.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableMat.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMat.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridDurableMat.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMat.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridDurableMat.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableMat.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableMat.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableMat.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridDurableMat.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableMat.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableMat.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridDurableMat.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableMat.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridDurableMat.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridDurableMat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableMat.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableMat.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridDurableMat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableMat.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridDurableMat.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableMat.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableMat.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableMat.Location = new System.Drawing.Point(12, 32);
            this.uGridDurableMat.Name = "uGridDurableMat";
            this.uGridDurableMat.Size = new System.Drawing.Size(436, 720);
            this.uGridDurableMat.TabIndex = 3;
            this.uGridDurableMat.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMat_AfterCellUpdate);
            this.uGridDurableMat.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridDurableMat_AfterRowInsert);
            this.uGridDurableMat.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMat_CellChange);
            this.uGridDurableMat.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDurableMat_DoubleClickCell);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uTextDurableMatName
            // 
            appearance40.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Appearance = appearance40;
            this.uTextDurableMatName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatName.Location = new System.Drawing.Point(244, 28);
            this.uTextDurableMatName.Name = "uTextDurableMatName";
            this.uTextDurableMatName.ReadOnly = true;
            this.uTextDurableMatName.Size = new System.Drawing.Size(100, 21);
            this.uTextDurableMatName.TabIndex = 113;
            // 
            // uTextDurableMatCode
            // 
            appearance41.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Appearance = appearance41;
            this.uTextDurableMatCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDurableMatCode.Location = new System.Drawing.Point(124, 28);
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.ReadOnly = true;
            this.uTextDurableMatCode.Size = new System.Drawing.Size(116, 21);
            this.uTextDurableMatCode.TabIndex = 113;
            // 
            // uTextPlantName
            // 
            appearance39.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Appearance = appearance39;
            this.uTextPlantName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantName.Location = new System.Drawing.Point(592, 8);
            this.uTextPlantName.Name = "uTextPlantName";
            this.uTextPlantName.ReadOnly = true;
            this.uTextPlantName.Size = new System.Drawing.Size(8, 21);
            this.uTextPlantName.TabIndex = 113;
            this.uTextPlantName.Visible = false;
            // 
            // uLabelDurableMatCode
            // 
            this.uLabelDurableMatCode.Location = new System.Drawing.Point(12, 28);
            this.uLabelDurableMatCode.Name = "uLabelDurableMatCode";
            this.uLabelDurableMatCode.Size = new System.Drawing.Size(108, 20);
            this.uLabelDurableMatCode.TabIndex = 112;
            // 
            // uLabelPlantName
            // 
            this.uLabelPlantName.Location = new System.Drawing.Point(580, 8);
            this.uLabelPlantName.Name = "uLabelPlantName";
            this.uLabelPlantName.Size = new System.Drawing.Size(8, 20);
            this.uLabelPlantName.TabIndex = 112;
            this.uLabelPlantName.Visible = false;
            // 
            // uButtonDeleteRow
            // 
            appearance66.FontData.BoldAsString = "True";
            this.uButtonDeleteRow.Appearance = appearance66;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(500, 24);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(70, 28);
            this.uButtonDeleteRow.TabIndex = 111;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uLabelLotNoInfo
            // 
            this.uLabelLotNoInfo.Location = new System.Drawing.Point(12, 52);
            this.uLabelLotNoInfo.Name = "uLabelLotNoInfo";
            this.uLabelLotNoInfo.Size = new System.Drawing.Size(108, 20);
            this.uLabelLotNoInfo.TabIndex = 108;
            // 
            // uGridModelList
            // 
            this.uGridModelList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            appearance67.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridModelList.DisplayLayout.Appearance = appearance67;
            this.uGridModelList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridModelList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance68.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance68.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance68.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance68.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridModelList.DisplayLayout.GroupByBox.Appearance = appearance68;
            appearance69.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridModelList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance69;
            this.uGridModelList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance70.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance70.BackColor2 = System.Drawing.SystemColors.Control;
            appearance70.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance70.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridModelList.DisplayLayout.GroupByBox.PromptAppearance = appearance70;
            this.uGridModelList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridModelList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridModelList.DisplayLayout.Override.ActiveCellAppearance = appearance71;
            appearance72.BackColor = System.Drawing.SystemColors.Highlight;
            appearance72.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridModelList.DisplayLayout.Override.ActiveRowAppearance = appearance72;
            this.uGridModelList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridModelList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            this.uGridModelList.DisplayLayout.Override.CardAreaAppearance = appearance73;
            appearance74.BorderColor = System.Drawing.Color.Silver;
            appearance74.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridModelList.DisplayLayout.Override.CellAppearance = appearance74;
            this.uGridModelList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridModelList.DisplayLayout.Override.CellPadding = 0;
            appearance75.BackColor = System.Drawing.SystemColors.Control;
            appearance75.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance75.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance75.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridModelList.DisplayLayout.Override.GroupByRowAppearance = appearance75;
            appearance76.TextHAlignAsString = "Left";
            this.uGridModelList.DisplayLayout.Override.HeaderAppearance = appearance76;
            this.uGridModelList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridModelList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.BorderColor = System.Drawing.Color.Silver;
            this.uGridModelList.DisplayLayout.Override.RowAppearance = appearance77;
            this.uGridModelList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance78.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridModelList.DisplayLayout.Override.TemplateAddRowAppearance = appearance78;
            this.uGridModelList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridModelList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridModelList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridModelList.Location = new System.Drawing.Point(11, 32);
            this.uGridModelList.Name = "uGridModelList";
            this.uGridModelList.Size = new System.Drawing.Size(261, 208);
            this.uGridModelList.TabIndex = 16;
            this.uGridModelList.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGrid_ClickCell);
            // 
            // uGridDurableLot
            // 
            this.uGridDurableLot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableLot.DisplayLayout.Appearance = appearance15;
            this.uGridDurableLot.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableLot.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLot.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableLot.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.uGridDurableLot.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableLot.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridDurableLot.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableLot.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableLot.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableLot.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.uGridDurableLot.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableLot.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLot.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            appearance34.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableLot.DisplayLayout.Override.CellAppearance = appearance34;
            this.uGridDurableLot.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableLot.DisplayLayout.Override.CellPadding = 0;
            appearance35.BackColor = System.Drawing.SystemColors.Control;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableLot.DisplayLayout.Override.GroupByRowAppearance = appearance35;
            appearance36.TextHAlignAsString = "Left";
            this.uGridDurableLot.DisplayLayout.Override.HeaderAppearance = appearance36;
            this.uGridDurableLot.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableLot.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableLot.DisplayLayout.Override.RowAppearance = appearance37;
            this.uGridDurableLot.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableLot.DisplayLayout.Override.TemplateAddRowAppearance = appearance38;
            this.uGridDurableLot.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableLot.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableLot.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableLot.Location = new System.Drawing.Point(12, 56);
            this.uGridDurableLot.Name = "uGridDurableLot";
            this.uGridDurableLot.Size = new System.Drawing.Size(584, 184);
            this.uGridDurableLot.TabIndex = 16;
            this.uGridDurableLot.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableLot_AfterCellUpdate);
            this.uGridDurableLot.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableLot_CellChange);
            // 
            // uGroupDurable
            // 
            this.uGroupDurable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.uGroupDurable.Controls.Add(this.uText);
            this.uGroupDurable.Controls.Add(this.uGridDurableMat);
            this.uGroupDurable.Controls.Add(this.uLabel);
            this.uGroupDurable.Location = new System.Drawing.Point(0, 80);
            this.uGroupDurable.Name = "uGroupDurable";
            this.uGroupDurable.Size = new System.Drawing.Size(460, 760);
            this.uGroupDurable.TabIndex = 6;
            // 
            // uText
            // 
            appearance42.BackColor = System.Drawing.Color.Salmon;
            this.uText.Appearance = appearance42;
            this.uText.BackColor = System.Drawing.Color.Salmon;
            this.uText.Location = new System.Drawing.Point(424, 32);
            this.uText.Multiline = true;
            this.uText.Name = "uText";
            this.uText.Size = new System.Drawing.Size(20, 12);
            this.uText.TabIndex = 5;
            // 
            // uLabel
            // 
            this.uLabel.Location = new System.Drawing.Point(308, 28);
            this.uLabel.Name = "uLabel";
            this.uLabel.Size = new System.Drawing.Size(112, 20);
            this.uLabel.TabIndex = 4;
            // 
            // uGroupDetail
            // 
            this.uGroupDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupDetail.Controls.Add(this.uComboDeleteType);
            this.uGroupDetail.Controls.Add(this.uButtonDeleteLotInfo);
            this.uGroupDetail.Controls.Add(this.uButtonDeleteLot);
            this.uGroupDetail.Controls.Add(this.uButton);
            this.uGroupDetail.Controls.Add(this.uCheckSerial);
            this.uGroupDetail.Controls.Add(this.uComboDurableInven);
            this.uGroupDetail.Controls.Add(this.uGroupBoxPackage);
            this.uGroupDetail.Controls.Add(this.uGroupBoxModel);
            this.uGroupDetail.Controls.Add(this.uTextDurableMatName);
            this.uGroupDetail.Controls.Add(this.uLabelLotNoInfo);
            this.uGroupDetail.Controls.Add(this.uTextDurableMatCode);
            this.uGroupDetail.Controls.Add(this.uGridDurableLot);
            this.uGroupDetail.Controls.Add(this.uLabelDurableInventory);
            this.uGroupDetail.Controls.Add(this.uLabelPlantName);
            this.uGroupDetail.Controls.Add(this.uTextPlantName);
            this.uGroupDetail.Controls.Add(this.uLabelDurableMatCode);
            this.uGroupDetail.Location = new System.Drawing.Point(460, 80);
            this.uGroupDetail.Name = "uGroupDetail";
            this.uGroupDetail.Size = new System.Drawing.Size(608, 760);
            this.uGroupDetail.TabIndex = 7;
            // 
            // uComboDeleteType
            // 
            this.uComboDeleteType.Location = new System.Drawing.Point(208, 52);
            this.uComboDeleteType.Name = "uComboDeleteType";
            this.uComboDeleteType.Size = new System.Drawing.Size(116, 21);
            this.uComboDeleteType.TabIndex = 122;
            this.uComboDeleteType.Visible = false;
            // 
            // uButtonDeleteLotInfo
            // 
            this.uButtonDeleteLotInfo.Location = new System.Drawing.Point(124, 52);
            this.uButtonDeleteLotInfo.Name = "uButtonDeleteLotInfo";
            this.uButtonDeleteLotInfo.Size = new System.Drawing.Size(80, 25);
            this.uButtonDeleteLotInfo.TabIndex = 121;
            this.uButtonDeleteLotInfo.Visible = false;
            this.uButtonDeleteLotInfo.Click += new System.EventHandler(this.uButtonDeleteLot_Click);
            // 
            // uButtonDeleteLot
            // 
            this.uButtonDeleteLot.Location = new System.Drawing.Point(124, 52);
            this.uButtonDeleteLot.Name = "uButtonDeleteLot";
            this.uButtonDeleteLot.Size = new System.Drawing.Size(80, 25);
            this.uButtonDeleteLot.TabIndex = 121;
            this.uButtonDeleteLot.Click += new System.EventHandler(this.uButtonDeleteLot_Click);
            // 
            // uButton
            // 
            this.uButton.Location = new System.Drawing.Point(492, 24);
            this.uButton.Name = "uButton";
            this.uButton.Size = new System.Drawing.Size(92, 24);
            this.uButton.TabIndex = 120;
            this.uButton.Click += new System.EventHandler(this.uButton_Click);
            // 
            // uCheckSerial
            // 
            this.uCheckSerial.Enabled = false;
            this.uCheckSerial.GlyphInfo = Infragistics.Win.UIElementDrawParams.Office2007CheckBoxGlyphInfo;
            this.uCheckSerial.Location = new System.Drawing.Point(352, 28);
            this.uCheckSerial.Name = "uCheckSerial";
            this.uCheckSerial.Size = new System.Drawing.Size(56, 20);
            this.uCheckSerial.TabIndex = 119;
            this.uCheckSerial.Text = "Serial";
            this.uCheckSerial.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // uComboDurableInven
            // 
            this.uComboDurableInven.Location = new System.Drawing.Point(464, 52);
            this.uComboDurableInven.MaxLength = 50;
            this.uComboDurableInven.Name = "uComboDurableInven";
            this.uComboDurableInven.Size = new System.Drawing.Size(120, 21);
            this.uComboDurableInven.TabIndex = 118;
            // 
            // uGroupBoxPackage
            // 
            this.uGroupBoxPackage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxPackage.Controls.Add(this.uComboCustomer);
            this.uGroupBoxPackage.Controls.Add(this.uGridPackage);
            this.uGroupBoxPackage.Controls.Add(this.uGridPackageList);
            this.uGroupBoxPackage.Controls.Add(this.uButtonSearchP);
            this.uGroupBoxPackage.Controls.Add(this.uButtonPackageOK);
            this.uGroupBoxPackage.Controls.Add(this.uLabelCoustomer);
            this.uGroupBoxPackage.Controls.Add(this.uButtonRowDel);
            this.uGroupBoxPackage.Location = new System.Drawing.Point(12, 496);
            this.uGroupBoxPackage.Name = "uGroupBoxPackage";
            this.uGroupBoxPackage.Size = new System.Drawing.Size(584, 244);
            this.uGroupBoxPackage.TabIndex = 117;
            // 
            // uComboCustomer
            // 
            this.uComboCustomer.Location = new System.Drawing.Point(84, 28);
            this.uComboCustomer.Name = "uComboCustomer";
            this.uComboCustomer.Size = new System.Drawing.Size(100, 21);
            this.uComboCustomer.TabIndex = 115;
            // 
            // uGridPackage
            // 
            this.uGridPackage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            appearance21.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPackage.DisplayLayout.Appearance = appearance21;
            this.uGridPackage.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPackage.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance22.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPackage.DisplayLayout.GroupByBox.Appearance = appearance22;
            appearance23.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPackage.DisplayLayout.GroupByBox.BandLabelAppearance = appearance23;
            this.uGridPackage.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance24.BackColor2 = System.Drawing.SystemColors.Control;
            appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPackage.DisplayLayout.GroupByBox.PromptAppearance = appearance24;
            this.uGridPackage.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPackage.DisplayLayout.MaxRowScrollRegions = 1;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPackage.DisplayLayout.Override.ActiveCellAppearance = appearance25;
            appearance26.BackColor = System.Drawing.SystemColors.Highlight;
            appearance26.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPackage.DisplayLayout.Override.ActiveRowAppearance = appearance26;
            this.uGridPackage.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPackage.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance27.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPackage.DisplayLayout.Override.CardAreaAppearance = appearance27;
            appearance28.BorderColor = System.Drawing.Color.Silver;
            appearance28.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPackage.DisplayLayout.Override.CellAppearance = appearance28;
            this.uGridPackage.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPackage.DisplayLayout.Override.CellPadding = 0;
            appearance29.BackColor = System.Drawing.SystemColors.Control;
            appearance29.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance29.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPackage.DisplayLayout.Override.GroupByRowAppearance = appearance29;
            appearance30.TextHAlignAsString = "Left";
            this.uGridPackage.DisplayLayout.Override.HeaderAppearance = appearance30;
            this.uGridPackage.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPackage.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            this.uGridPackage.DisplayLayout.Override.RowAppearance = appearance33;
            this.uGridPackage.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPackage.DisplayLayout.Override.TemplateAddRowAppearance = appearance32;
            this.uGridPackage.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPackage.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPackage.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPackage.Location = new System.Drawing.Point(308, 32);
            this.uGridPackage.Name = "uGridPackage";
            this.uGridPackage.Size = new System.Drawing.Size(260, 208);
            this.uGridPackage.TabIndex = 16;
            this.uGridPackage.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridPackage_AfterCellUpdate);
            this.uGridPackage.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMat_CellChange);
            // 
            // uGridPackageList
            // 
            this.uGridPackageList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            appearance54.BackColor = System.Drawing.SystemColors.Window;
            appearance54.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridPackageList.DisplayLayout.Appearance = appearance54;
            this.uGridPackageList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridPackageList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance55.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance55.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance55.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance55.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.GroupByBox.Appearance = appearance55;
            appearance56.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPackageList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance56;
            this.uGridPackageList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance57.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance57.BackColor2 = System.Drawing.SystemColors.Control;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridPackageList.DisplayLayout.GroupByBox.PromptAppearance = appearance57;
            this.uGridPackageList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridPackageList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            appearance58.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridPackageList.DisplayLayout.Override.ActiveCellAppearance = appearance58;
            appearance59.BackColor = System.Drawing.SystemColors.Highlight;
            appearance59.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridPackageList.DisplayLayout.Override.ActiveRowAppearance = appearance59;
            this.uGridPackageList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridPackageList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance60.BackColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.Override.CardAreaAppearance = appearance60;
            appearance61.BorderColor = System.Drawing.Color.Silver;
            appearance61.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridPackageList.DisplayLayout.Override.CellAppearance = appearance61;
            this.uGridPackageList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridPackageList.DisplayLayout.Override.CellPadding = 0;
            appearance62.BackColor = System.Drawing.SystemColors.Control;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridPackageList.DisplayLayout.Override.GroupByRowAppearance = appearance62;
            appearance63.TextHAlignAsString = "Left";
            this.uGridPackageList.DisplayLayout.Override.HeaderAppearance = appearance63;
            this.uGridPackageList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridPackageList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            appearance64.BorderColor = System.Drawing.Color.Silver;
            this.uGridPackageList.DisplayLayout.Override.RowAppearance = appearance64;
            this.uGridPackageList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance65.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridPackageList.DisplayLayout.Override.TemplateAddRowAppearance = appearance65;
            this.uGridPackageList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridPackageList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridPackageList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridPackageList.Location = new System.Drawing.Point(12, 32);
            this.uGridPackageList.Name = "uGridPackageList";
            this.uGridPackageList.Size = new System.Drawing.Size(261, 208);
            this.uGridPackageList.TabIndex = 16;
            this.uGridPackageList.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGrid_ClickCell);
            // 
            // uButtonSearchP
            // 
            this.uButtonSearchP.Location = new System.Drawing.Point(200, 24);
            this.uButtonSearchP.Name = "uButtonSearchP";
            this.uButtonSearchP.Size = new System.Drawing.Size(70, 28);
            this.uButtonSearchP.TabIndex = 114;
            this.uButtonSearchP.Click += new System.EventHandler(this.uButtonSearchP_Click);
            // 
            // uButtonPackageOK
            // 
            this.uButtonPackageOK.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uButtonPackageOK.Location = new System.Drawing.Point(276, 124);
            this.uButtonPackageOK.Name = "uButtonPackageOK";
            this.uButtonPackageOK.Size = new System.Drawing.Size(28, 36);
            this.uButtonPackageOK.TabIndex = 114;
            this.uButtonPackageOK.Text = "→";
            this.uButtonPackageOK.Click += new System.EventHandler(this.uButtonPackageOK_Click);
            // 
            // uLabelCoustomer
            // 
            this.uLabelCoustomer.Location = new System.Drawing.Point(12, 28);
            this.uLabelCoustomer.Name = "uLabelCoustomer";
            this.uLabelCoustomer.Size = new System.Drawing.Size(68, 20);
            this.uLabelCoustomer.TabIndex = 112;
            // 
            // uButtonRowDel
            // 
            appearance3.FontData.BoldAsString = "True";
            this.uButtonRowDel.Appearance = appearance3;
            this.uButtonRowDel.Location = new System.Drawing.Point(500, 24);
            this.uButtonRowDel.Name = "uButtonRowDel";
            this.uButtonRowDel.Size = new System.Drawing.Size(70, 28);
            this.uButtonRowDel.TabIndex = 111;
            this.uButtonRowDel.Text = "행삭제";
            this.uButtonRowDel.Click += new System.EventHandler(this.uButtonRowDel_Click);
            // 
            // uGroupBoxModel
            // 
            this.uGroupBoxModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxModel.Controls.Add(this.uComboStation);
            this.uGroupBoxModel.Controls.Add(this.uGridModel);
            this.uGroupBoxModel.Controls.Add(this.uButtonSearchM);
            this.uGroupBoxModel.Controls.Add(this.uButtonModelOK);
            this.uGroupBoxModel.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBoxModel.Controls.Add(this.uLabelStation);
            this.uGroupBoxModel.Controls.Add(this.uGridModelList);
            this.uGroupBoxModel.Location = new System.Drawing.Point(12, 248);
            this.uGroupBoxModel.Name = "uGroupBoxModel";
            this.uGroupBoxModel.Size = new System.Drawing.Size(584, 244);
            this.uGroupBoxModel.TabIndex = 116;
            // 
            // uComboStation
            // 
            this.uComboStation.Location = new System.Drawing.Point(84, 28);
            this.uComboStation.Name = "uComboStation";
            this.uComboStation.Size = new System.Drawing.Size(100, 21);
            this.uComboStation.TabIndex = 115;
            // 
            // uGridModel
            // 
            this.uGridModel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            appearance79.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridModel.DisplayLayout.Appearance = appearance79;
            this.uGridModel.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridModel.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance80.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance80.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance80.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridModel.DisplayLayout.GroupByBox.Appearance = appearance80;
            appearance81.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridModel.DisplayLayout.GroupByBox.BandLabelAppearance = appearance81;
            this.uGridModel.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance82.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance82.BackColor2 = System.Drawing.SystemColors.Control;
            appearance82.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance82.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridModel.DisplayLayout.GroupByBox.PromptAppearance = appearance82;
            this.uGridModel.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridModel.DisplayLayout.MaxRowScrollRegions = 1;
            appearance83.BackColor = System.Drawing.SystemColors.Window;
            appearance83.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridModel.DisplayLayout.Override.ActiveCellAppearance = appearance83;
            appearance84.BackColor = System.Drawing.SystemColors.Highlight;
            appearance84.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridModel.DisplayLayout.Override.ActiveRowAppearance = appearance84;
            this.uGridModel.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridModel.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            this.uGridModel.DisplayLayout.Override.CardAreaAppearance = appearance85;
            appearance86.BorderColor = System.Drawing.Color.Silver;
            appearance86.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridModel.DisplayLayout.Override.CellAppearance = appearance86;
            this.uGridModel.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridModel.DisplayLayout.Override.CellPadding = 0;
            appearance87.BackColor = System.Drawing.SystemColors.Control;
            appearance87.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance87.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance87.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance87.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridModel.DisplayLayout.Override.GroupByRowAppearance = appearance87;
            appearance88.TextHAlignAsString = "Left";
            this.uGridModel.DisplayLayout.Override.HeaderAppearance = appearance88;
            this.uGridModel.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridModel.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance89.BackColor = System.Drawing.SystemColors.Window;
            appearance89.BorderColor = System.Drawing.Color.Silver;
            this.uGridModel.DisplayLayout.Override.RowAppearance = appearance89;
            this.uGridModel.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridModel.DisplayLayout.Override.TemplateAddRowAppearance = appearance90;
            this.uGridModel.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridModel.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridModel.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridModel.Location = new System.Drawing.Point(308, 32);
            this.uGridModel.Name = "uGridModel";
            this.uGridModel.Size = new System.Drawing.Size(260, 208);
            this.uGridModel.TabIndex = 16;
            this.uGridModel.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridModel_AfterCellUpdate);
            this.uGridModel.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDurableMat_CellChange);
            // 
            // uButtonSearchM
            // 
            this.uButtonSearchM.Location = new System.Drawing.Point(200, 24);
            this.uButtonSearchM.Name = "uButtonSearchM";
            this.uButtonSearchM.Size = new System.Drawing.Size(70, 28);
            this.uButtonSearchM.TabIndex = 114;
            this.uButtonSearchM.Click += new System.EventHandler(this.uButtonSearchM_Click);
            // 
            // uButtonModelOK
            // 
            this.uButtonModelOK.Font = new System.Drawing.Font("굴림", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.uButtonModelOK.Location = new System.Drawing.Point(276, 116);
            this.uButtonModelOK.Name = "uButtonModelOK";
            this.uButtonModelOK.Size = new System.Drawing.Size(28, 36);
            this.uButtonModelOK.TabIndex = 114;
            this.uButtonModelOK.Text = "→";
            this.uButtonModelOK.Click += new System.EventHandler(this.uButtonModelOK_Click);
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(12, 28);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(68, 20);
            this.uLabelStation.TabIndex = 112;
            // 
            // uLabelDurableInventory
            // 
            this.uLabelDurableInventory.Location = new System.Drawing.Point(352, 52);
            this.uLabelDurableInventory.Name = "uLabelDurableInventory";
            this.uLabelDurableInventory.Size = new System.Drawing.Size(108, 20);
            this.uLabelDurableInventory.TabIndex = 112;
            // 
            // frmMAS0042
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupDetail);
            this.Controls.Add(this.uGroupDurable);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0042";
            this.Load += new System.EventHandler(this.frmMAS0042_Load);
            this.Activated += new System.EventHandler(this.frmMAS0042_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0042_FormClosing);
            this.Resize += new System.EventHandler(this.frmMAS0042_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridModelList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDurable)).EndInit();
            this.uGroupDurable.ResumeLayout(false);
            this.uGroupDurable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDetail)).EndInit();
            this.uGroupDetail.ResumeLayout(false);
            this.uGroupDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDeleteType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uCheckSerial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboDurableInven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxPackage)).EndInit();
            this.uGroupBoxPackage.ResumeLayout(false);
            this.uGroupBoxPackage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridPackageList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxModel)).EndInit();
            this.uGroupBoxModel.ResumeLayout(false);
            this.uGroupBoxModel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridModel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMold;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMold;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableMat;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.Misc.UltraLabel uLabelLotNoInfo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableLot;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridModelList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantName;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatCode;
        private Infragistics.Win.Misc.UltraLabel uLabelPlantName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.Misc.UltraGroupBox uGroupDurable;
        private Infragistics.Win.Misc.UltraGroupBox uGroupDetail;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridModel;
        private Infragistics.Win.Misc.UltraButton uButtonRowDel;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPackage;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridPackageList;
        private Infragistics.Win.Misc.UltraButton uButtonPackageOK;
        private Infragistics.Win.Misc.UltraButton uButtonModelOK;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxModel;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxPackage;
        private Infragistics.Win.Misc.UltraButton uButtonSearchP;
        private Infragistics.Win.Misc.UltraButton uButtonSearchM;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStation;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelCoustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDurableInven;
        private Infragistics.Win.Misc.UltraLabel uLabelDurableInventory;
        private Infragistics.Win.UltraWinEditors.UltraCheckEditor uCheckSerial;
        private Infragistics.Win.Misc.UltraButton uButton;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteLot;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText;
        private Infragistics.Win.Misc.UltraLabel uLabel;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboDeleteType;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteLotInfo;
    }
}