﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 금형치공구관리기준정보                                */
/* 프로그램ID   : frmMASZ0032_S.cs                                        */
/* 프로그램명   : 금형치공구수명관리                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-02-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMASZ0032_S : Form,IToolbar
    {
        //리소스를 호출하기위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0032_S()
        {
            InitializeComponent();
        }

        private void frmMASZ0032_Activated(object sender, EventArgs e)
        {
            //Toolbar활성화
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfActiveToolBar(this.MdiParent, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0032_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            //grd.mfSaveGridColumnProperty
        }

        private void frmMASZ0032_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정
            titleArea.mfSetLabelText("금형치공구별 Spec", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitEvent();
            InitButton();
            InitLabel();
            InitGroupbox();
            InitCombo();
            InitGrid();

            this.uTextPlantCode.Hide();
            this.uTextDurableMatCode.Hide();
            this.uTextDurableMatName.Hide();
            this.uTextLotNo.Hide();
        }

        #region 초기화 메소드

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonMove, "↓", m_resSys.GetString("SYS_FONTNAME"), null);
                this.uButtonMove.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uButtonMove.Appearance.FontData.SizeInPoints = 12;
                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                wButton.mfSetButton(this.uButtonSearchEquip, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                wButton.mfSetButton(this.uButtonDeleteEquip, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchDurableMatName, "금형치공구유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                this.uLabelSearchPlant.Hide();

                wLabel.mfSetLabel(this.uLabelEquipStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupbox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupDurable, GroupBoxType.LIST, "금형치공구 리스트", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                this.uGroupDurable.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupDurable.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBoxDurableMatLot, GroupBoxType.LIST, "수명관리 Spec 기준정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                this.uGroupBoxDurableMatLot.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxDurableMatLot.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                // 설비정보 그룹박스 추가
                wGroupBox.mfSetGroupBox(this.uGroupBoxEquip, GroupBoxType.LIST, "설비정보List", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // 설비정보 그룹박스 폰트설정
                this.uGroupBoxEquip.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxEquip.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitEvent()
        {
            this.uComboEquipStation.ValueChanged += new System.EventHandler(uComboStation_ValueChanged);
            this.uComboEquipLoc.ValueChanged += new System.EventHandler(uComboEquipLoc_ValueChanged);
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(uComboProcessGroup_ValueChanged);
            this.uComboEquipType.ValueChanged += new System.EventHandler(uComboEquipType_ValueChanged);
            this.uButtonEquipOK.Click += new System.EventHandler(uButtonEquipOK_Click);
            this.uButtonMove.Click += new System.EventHandler(uButtonMove_Click);
            this.uButtonSearchEquip.Click += new System.EventHandler(uButtonSearchEquip_Click);
            this.uButtonDeleteEquip.Click += new System.EventHandler(uButtonDeleteEquip_Click);
        }

        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

                // Package
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "Package", "ComboName", dtPackage);
                
                // 금형치공구코드
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDMat);
                DataTable dtDurableMat = clsDMat.mfReadDurableMat_TypeCombo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchDurableMatName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "DurableMatTypeCode", "DurableMatTypeName", dtDurableMat);

                this.uComboSearchPlant.Hide();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 금형치공구 리스트

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableMat, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                ////wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "EMC", "EMC", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridDurableMat, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                //폰트설정
                this.uGridDurableMat.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableMat.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region SpecInfo List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableSpec, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDurableSpec, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurableSpec, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridDurableSpec, 0, "SpecCode", "Spec코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 30
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableSpec, 0, "SpecName", "Spec명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumnValueList(this.uGridDurableSpec, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                //폰트설정
                this.uGridDurableSpec.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableSpec.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region LotNoSpecInfo List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridLotNoSpecInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "SpecCode", "Spec코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 30
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "SpecName", "Spec명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "UsageLimitLower", "Limit-1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.0}", "0");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "UsageLimit", "Limit", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.0}", "0");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "UsageLimitUpper", "Limit+1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.0}", "0");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "CurUsage", "CurUsage", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:15.5}", "0");

                wGrid.mfSetGridColumnValueList(this.uGridLotNoSpecInfo, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                this.uGridLotNoSpecInfo.DisplayLayout.Bands[0].Columns["UsageLimitLower"].PromptChar = ' ';
                this.uGridLotNoSpecInfo.DisplayLayout.Bands[0].Columns["UsageLimit"].PromptChar = ' ';
                this.uGridLotNoSpecInfo.DisplayLayout.Bands[0].Columns["UsageLimitUpper"].PromptChar = ' ';

                //폰트설정
                this.uGridLotNoSpecInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridLotNoSpecInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region Equip Info

                // tj
                wGrid.mfInitGeneralGrid(this.uGridEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquip, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridEquip.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquip.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                
                #endregion

                #region EquipList

                // 설비정보를 담을 그리드 선언
                wGrid.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region IToolbar 멤버

        // 신규
        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 삭제
        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 엑셀
        public void mfExcel()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 출력
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 저장
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (!(this.uGridLotNoSpecInfo.Rows.Count > 0) && !(this.uGridEquip.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001034", "M001041", Infragistics.Win.HAlign.Right);

                    return;
                }
                string strLang = m_resSys.GetString("SYS_LANG");
                // 필수 입력값 확인
                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                for (int i = 0; i < this.uGridLotNoSpecInfo.Rows.Count; i++)
                {
                    if (!CheckInvalidValue(this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimitLower"]))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang), (i + 1) + msg.GetMessge_Text("M001287", strLang), Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else if (!CheckInvalidValue(this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimit"]))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang), (i + 1) + msg.GetMessge_Text("M001288", strLang), Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else if (!CheckInvalidValue(this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimitUpper"]))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang), (i + 1) + msg.GetMessge_Text("M001286", strLang), Infragistics.Win.HAlign.Right);

                        return;
                    }
                }

                // 필수 입력값 확인
                this.uGridEquip.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // 설비정보 그리드에서 설비코드가 공백인경우 메세지 출력
                for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                {
                    if (this.uGridEquip.Rows[i].Hidden)
                        continue;

                    if (this.uGridEquip.Rows[i].GetCellValue("EquipCode").ToString().Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001235", "M001234", "M000701", Infragistics.Win.HAlign.Right);

                        this.uGridEquip.ActiveCell = this.uGridEquip.Rows[i].Cells["EquipCode"];
                        this.uGridEquip.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }
                    
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 데이터 테이블 반환 메소드 호출
                    DataTable dtSaveLotSpecInfo = Rtn_Save_LotSpecInfoDate();
                    DataTable dtDelLotSpecInfo = Rtn_Delete_LotSpecInfoDate();
                    DataTable dtEquipInfo = Rtn_Save_EquipInfoData();
                    

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                    QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                    brwChannel.mfCredentials(clsLotSpecInfo);

                    string strErrRtn = clsLotSpecInfo.mfSaveLotNoSpecInfo_frmMASZ0032(dtSaveLotSpecInfo, dtDelLotSpecInfo, dtEquipInfo,
                                                                                        m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M001037", "M000930",
                                                            Infragistics.Win.HAlign.Right);

                        Init();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001023", "M000953", Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001023", ErrRtn.ErrMessage, Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strDurableMatName = this.uComboSearchDurableMatName.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDLot);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtDLot = clsDLot.mfReadMASDurableLot_PSTS_frmMASZ0032_S(strPlantCode, strDurableMatName, m_resSys.GetString("SYS_LANG"));
                this.uGridDurableMat.SetDataBinding(dtDLot, string.Empty);

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtDLot.Rows.Count <= 0)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }


                while (this.uGridDurableSpec.Rows.Count > 0)
                    this.uGridDurableSpec.Rows[0].Delete(false);

                while (this.uGridLotNoSpecInfo.Rows.Count > 0)
                    this.uGridLotNoSpecInfo.Rows[0].Delete(false);

                this.uTextPlantCode.Clear();
                this.uTextDurableMatCode.Clear();
                this.uTextLotNo.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Events

        // 공장콤보값 변경시 이벤트
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                WinComboEditor wCombo = new WinComboEditor();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                DataTable dtPackage = new DataTable();
                DataTable dtDurableMat = new DataTable();

                this.uComboSearchPackage.Items.Clear();
                this.uComboSearchDurableMatName.Items.Clear();

                if (!strPlantCode.Equals(string.Empty))
                {
                    // Package
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);
                    dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // 금형치공구코드
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsDMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsDMat);
                    dtDurableMat = clsDMat.mfReadDurableMatNameCombo(strPlantCode, string.Empty, m_resSys.GetString("SYS_LANG"));
                }

                // Package
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                        , "", "", "전체", "Package", "ComboName", dtPackage);

                // 금형치공구코드
                wCombo.mfSetComboEditor(this.uComboSearchDurableMatName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                        , "", "", "전체", "DurableMat", "DurableMatName", dtDurableMat);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Station콤보 선택시 설비위치변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboEquipStation.Value.ToString();
                string strStation = this.uComboEquipStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboEquipStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboEquipStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboEquipStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (strChk.Equals(string.Empty))
                    return;

                //검색조건저장
                string strPlantCode = this.uTextPlantCode.Text;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                //설비위치정보 BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                brwChannel.mfCredentials(clsEquipLocation);

                //설비위치정보콤보조회 매서드 실행
                DataTable dtLoc = clsEquipLocation.mfReadLocation_Combo(strPlantCode, strStationCode, m_resSys.GetString("SYS_LANG"));

                WinComboEditor wCombo = new WinComboEditor();

                //설비위치정보콤보 이전 정보 클리어
                this.uComboEquipLoc.Items.Clear();

                wCombo.mfSetComboEditor(this.uComboEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Left, "", "", "전체", "EquipLocCode", "EquipLocName", dtLoc);

                clsEquipLocation.Dispose();
                dtLoc.Dispose();

                mfSearchCombo(strPlantCode, strStationCode, "", "", "", 3);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비위치 변경시 자동조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLoc.Value.ToString();
                string strPlantCode = this.uTextPlantCode.Text;
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (strChk.Equals(string.Empty))
                        return;

                    mfSearchCombo(strPlantCode, this.uComboEquipStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), "", "", 3);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비대분류콤보 선택시 자동조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                string strPlantCode = this.uTextPlantCode.Text;
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (strChk.Equals(string.Empty))
                        return;

                    mfSearchCombo(strPlantCode, this.uComboEquipStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비중분류 변경시 자동조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboEquipType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipType.Value.ToString();
                string strPlantCode = this.uTextPlantCode.Text;
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboEquipType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(strPlantCode, this.uComboEquipStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboEquipType.Value.ToString(), 1);

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        // 이동버튼 클릭 이벤트
        private void uButtonMove_Click(object sender, EventArgs e)
        {
            try
            {
                // 키 입력여부 확인
                if (!this.uTextPlantCode.Text.Equals(string.Empty) &&
                    !this.uTextDurableMatCode.Text.Equals(string.Empty) &&
                    !this.uTextLotNo.Text.Equals(string.Empty))
                {
                    // 테이블 복제
                    DataTable dtLotNoSpecInfo = (DataTable)this.uGridLotNoSpecInfo.DataSource;
                    // 기본키 값 설정
                    dtLotNoSpecInfo.Columns["PlantCode"].DefaultValue = this.uTextPlantCode.Text;
                    dtLotNoSpecInfo.Columns["DurableMatCode"].DefaultValue = this.uTextDurableMatCode.Text;
                    dtLotNoSpecInfo.Columns["LotNo"].DefaultValue = this.uTextLotNo.Text;

                    for (int i = 0; i < this.uGridDurableSpec.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridDurableSpec.Rows[i].Cells["Check"].Value))
                        {
                            // Check 된 행이 기존에 저장된 값일경우
                            DataRow[] _drs = dtLotNoSpecInfo.Select("SpecCode = '" + this.uGridDurableSpec.Rows[i].Cells["SpecCode"].Value.ToString() + "'");
                            if (_drs.Length > 0)
                            {
                                //foreach (DataRow dr in _drs)
                                //{
                                //    dtLotNoSpecInfo.Rows.Add(dr);
                                //}
                            }
                            else
                            {
                                // Check 된 행이 기존에 저장된 값이 아닐경우
                                DataRow dr = dtLotNoSpecInfo.NewRow();
                                dr["SpecCode"] = this.uGridDurableSpec.Rows[i].Cells["SpecCode"].Value.ToString();
                                dr["SpecName"] = this.uGridDurableSpec.Rows[i].Cells["SpecName"].Value.ToString();
                                dr["UsageLimitLower"] = DBNull.Value;
                                dr["UsageLimit"] = DBNull.Value;
                                dr["UsageLimitUpper"] = DBNull.Value;
                                dr["CurUsage"] = 0.0m;
                                dtLotNoSpecInfo.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            //// Check 안된 행이 기존에 저장된 값일경우
                            //DataRow[] _drs = dtLotNoSpecInfo.Select("SpecCode = '" + this.uGridDurableSpec.Rows[i].Cells["SpecCode"].Value.ToString() + "'");
                            //if (_drs.Length > 0)
                            //{
                            //    foreach (DataRow dr in _drs)
                            //    {
                            //        dtLotNoSpecInfo.Rows.Remove(dr);
                            //    }
                            //}
                        }
                    }

                    this.uGridLotNoSpecInfo.SetDataBinding(dtLotNoSpecInfo, string.Empty);
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 금형치공구 리스트 더블클릭 이벤트
        /// </summary>
        private void uGridDurableMat_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 변수
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strDurableMatCode = e.Row.Cells["DurableMatCode"].Value.ToString();
                string strDurableMatName = e.Row.Cells["DurableMatName"].Value.ToString();
                string strLotNo = e.Row.Cells["LotNo"].Value.ToString();
                string strPackage = e.Row.Cells["Package"].Value.ToString();
                string strEMC = e.Row.Cells["EMC"].Value.ToString();


                Init();

                this.uTextPlantCode.Text = strPlantCode;
                this.uTextDurableMatCode.Text = strDurableMatCode;
                this.uTextDurableMatName.Text = strDurableMatName;
                this.uTextLotNo.Text = strLotNo;
                this.uTextPackage.Text = strPackage;
                this.uTextEMC.Text = strEMC;

                // BL 연결
                // DurableSpec
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableSpec), "DurableSpec");
                QRPMAS.BL.MASDMM.DurableSpec clsDSpec = new QRPMAS.BL.MASDMM.DurableSpec();
                brwChannel.mfCredentials(clsDSpec);

                // LotNoSpecInfo
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                brwChannel.mfCredentials(clsLotSpecInfo);

                DataTable dtDurableSpecList = clsDSpec.mfReadMASDurableSpec(strPlantCode);
                this.uGridDurableSpec.SetDataBinding(dtDurableSpecList, string.Empty);

                // Data조회
                DataTable dtLotSpecInfo = clsLotSpecInfo.mfReadLotNoSpecInfo_Detail(strPlantCode, strDurableMatCode, strLotNo,strPackage,strEMC);
                // Data Binding
                this.uGridLotNoSpecInfo.SetDataBinding(dtLotSpecInfo, string.Empty);

                //금형치공구BOM_Equip정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMatBOM_Equip), "DurableMatBOM_Equip");
                QRPMAS.BL.MASDMM.DurableMatBOM_Equip clsBOM_Equip = new QRPMAS.BL.MASDMM.DurableMatBOM_Equip();
                brwChannel.mfCredentials(clsBOM_Equip);

                //금형치공구BOM_Equip정보 조회 매서드 실행
                DataTable dtSource = clsBOM_Equip.mfReadDurableMatBOM_Equip(strPlantCode, strDurableMatCode, strLotNo, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                // Data Binding
                this.uGridEquip.SetDataBinding(dtSource, string.Empty);
                //this.uGridEquip.DataSource = dtSource;
                //this.uGridEquip.DataBind();

                ////// DurableSpec Grid Check 해제
                ////WinGrid wGrid = new WinGrid();
                ////wGrid.mfSetAllUnCheckedGridColumn(this.uGridDurableSpec, 0, "Check");
                ////wGrid.mfClearRowSeletorGrid(this.uGridDurableSpec);

                //Station정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                brwChannel.mfCredentials(clsStation);

                //Station 콤보정보 조회매서드 실행
                DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                WinComboEditor wCom = new WinComboEditor();
                wCom.mfSetComboEditor(this.uComboEquipStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", "전체", "StationCode", "StationName", dtStation);

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                m_resSys.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// LotNoSpecInfo List 업데이트 이벤트
        /// </summary>
        private void uGridLotNoSpecInfo_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == null || e.Cell.Value == DBNull.Value)
                    return;

                if (e.Cell.Column.Key.Equals("UsageLimitLower"))
                {
                    if(!(ReturnDecimalValue(e.Cell.Value.ToString()) > 0))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001307", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        e.Cell.Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }
                    else
                    {
                        //CheckLimitValue(e);

                        if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimit"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) 
                                >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001335", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimitLower"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimitLower"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                    }
                }
                else if (e.Cell.Column.Key.Equals("UsageLimit"))
                {
                    if (!(ReturnDecimalValue(e.Cell.Value.ToString()) > 0))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001308", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        e.Cell.Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }
                    else
                    {
                        //CheckLimitValue(e);
                        if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimitLower"]) && CheckInvalidValue(e.Cell.Row.Cells["UsageLimitUpper"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) &&
                                ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001337", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                            //else if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) &&
                            //        ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                            //{
                            //    // SystemInfo ResourceSet
                            //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                            //    WinMessageBox msg = new WinMessageBox();
                            //    DialogResult Result = new DialogResult();

                            //    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //    , "M001264", "M001228", "M001336", Infragistics.Win.HAlign.Right);

                            //    e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                            //    e.Cell.Row.Cells["UsageLimit"].Activate();
                            //    this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            //}
                            else if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) &&
                                    ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001338", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                        else if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimitLower"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) 
                                >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001337", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                        else if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimitUpper"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001336", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                        
                    }
                }
                //else if (e.Cell.Column.Key.Equals("UsageLimitUpper"))
                //{
                //    if (!(ReturnDecimalValue(e.Cell.Value.ToString()) > 0))
                //    {
                //        // SystemInfo ResourceSet
                //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //        WinMessageBox msg = new WinMessageBox();
                //        DialogResult Result = new DialogResult();

                //        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //        , "M001264", "M001228", "M001306", Infragistics.Win.HAlign.Right);

                //        e.Cell.Value = e.Cell.OriginalValue;
                //        e.Cell.Activate();
                //        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                //        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                //        return;
                //    }
                //    else
                //    {
                //        //CheckLimitValue(e);
                //        if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimit"]))
                //        {
                //            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                //            {
                //                // SystemInfo ResourceSet
                //                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //                WinMessageBox msg = new WinMessageBox();
                //                DialogResult Result = new DialogResult();

                //                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                , "M001264", "M001228", "M001334", Infragistics.Win.HAlign.Right);

                //                e.Cell.Row.Cells["UsageLimitUpper"].Value = DBNull.Value;
                //                e.Cell.Row.Cells["UsageLimitUpper"].Activate();
                //                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridLotNoSpecInfo.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridLotNoSpecInfo.Rows[i].Cells["Check"].Value))
                        this.uGridLotNoSpecInfo.Rows[i].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 선택된 Equip행 정보를 숨김처리 Check Equip Hidden 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDeleteEquip_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridEquip.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridEquip.Rows[i].Cells["Check"].Value))
                        {
                            this.uGridEquip.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        /// <summary>
        /// 선택한 설비를 이동
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonEquipOK_Click(object sender, EventArgs e)
        {
            try
            {
                // 설비리스트가 없으면 리턴
                if (this.uGridEquipList.Rows.Count == 0)
                    return;

                if (this.uTextPlantCode.Text.Equals(string.Empty))
                    return;

                // 공장,치공구코드 정보저장
                string strPlantCode = this.uTextPlantCode.Text;
                string strDurableMatCode = this.uTextDurableMatCode.Text;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                DataTable dtSource = new DataTable();
                dtSource.Columns.Add("DurableMatCode", typeof(string));
                dtSource.Columns.Add("PlantCode", typeof(string));
                dtSource.Columns.Add("EquipCode", typeof(string));
                dtSource.Columns.Add("EquipName", typeof(string));

                // 설비정보를 담았는지 체크하는 변수
                string strSave = "";

                // For문을 돌며 체크박스에 선택되어있는 설비정보를 이동시킨다.
                // 단 Model그리드 정보를 가지고 있으면 이동하지 않는다.
                for (int i = 0; i < this.uGridEquipList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridEquipList.Rows[i].Cells["Check"].Value))
                    {
                        string strChk = "";
                        // 설비정보가 이미 있으면 중복확인
                        if (this.uGridEquip.Rows.Count > 0)
                        {
                            // 설비정보 그리드를 돌며 중복확인
                            for (int j = 0; j < this.uGridEquip.Rows.Count; j++)
                            {
                                if (this.uGridEquip.Rows[j].Hidden.Equals(false))
                                {
                                    // 설비코드 중복확인
                                    if (this.uGridEquip.Rows[j].Cells["EquipCode"].Value.Equals(this.uGridEquipList.Rows[i].Cells["EquipCode"].Value))
                                    {
                                        if (strChk.Equals(string.Empty))
                                            strChk = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value.ToString();
                                    }
                                    if (strSave.Equals(string.Empty))
                                    {
                                        DataRow drSource = dtSource.NewRow();

                                        drSource["DurableMatCode"] = strDurableMatCode;
                                        drSource["PlantCode"] = strPlantCode;
                                        drSource["EquipCode"] = this.uGridEquip.Rows[j].Cells["EquipCode"].Value;
                                        drSource["EquipName"] = this.uGridEquip.Rows[j].Cells["EquipName"].Value;

                                        dtSource.Rows.Add(drSource);
                                    }
                                }
                            }

                            if (strSave.Equals(string.Empty))
                                strSave = "ok";
                        }

                        if (strChk.Equals(string.Empty))
                        {

                            int intCnt = this.uGridEquip.Rows.Count;

                            //데이터테이블에 선택한 설비정보를 추가한다.
                            DataRow drSource = dtSource.NewRow();

                            drSource["DurableMatCode"] = strDurableMatCode;
                            drSource["PlantCode"] = strPlantCode;
                            drSource["EquipCode"] = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value;
                            drSource["EquipName"] = this.uGridEquipList.Rows[i].Cells["EquipName"].Value;

                            dtSource.Rows.Add(drSource);

                        }

                        // 이동한 설비정보를 숨긴다.
                        if (this.uGridEquipList.Rows[i].Hidden.Equals(false))
                        {
                            this.uGridEquipList.Rows[i].Hidden = true;
                            this.uGridEquipList.Rows[i].Cells["Check"].Value = false;
                        }
                    }
                }
                //그리드에 바인드

                this.uGridEquip.DataSource = dtSource;
                this.uGridEquip.DataBind();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비정보 조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonSearchEquip_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uTextPlantCode.Text.Equals(string.Empty)
                   || this.uTextDurableMatCode.Text.Equals(string.Empty))
                    return;
                WinMessageBox msg = new WinMessageBox();

                string strPlantCode = this.uTextPlantCode.Text;
                string strStationCode = this.uComboEquipStation.Value.ToString();
                string strEquipLoc = this.uComboEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipType = this.uComboEquipType.Value.ToString();
                string strEquipGroupCode = this.uComboEquipGroup.Value == null ? string.Empty : this.uComboEquipGroup.Value.ToString();

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // ProgressBar보이기
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                // 커서Change
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 설비정보BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                // 설비정보조회 매서드 호출
                DataTable dtEquip = clsEquip.mfReadEquip_Info(strPlantCode, strStationCode, strEquipLoc, strProcessGroup, strEquipType, strEquipGroupCode, m_resSys.GetString("SYS_LANG"));

                // 그리드에 바인드
                this.uGridEquipList.DataSource = dtEquip;
                this.uGridEquipList.DataBind();

                //PrograssBar 종료
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtEquip.Rows.Count == 0)
                {
                    DialogResult DResult = new DialogResult();

                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        #endregion

        #region Methods

        /// <summary>
        /// 값 유효성 체크
        /// </summary>
        /// <param name="uCell">값 체크할 GridCell</param>
        /// <returns></returns>
        private bool CheckInvalidValue(Infragistics.Win.UltraWinGrid.UltraGridCell uCell)
        {
            try
            {
                if (string.IsNullOrEmpty(uCell.Value.ToString()) || uCell.Value == DBNull.Value)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m; ;
        }

        /// <summary>
        /// Litmit 값의 크기 비교함수
        /// </summary>
        /// <param name="e"></param>
        private void CheckLimitValue(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimitLower"]) &&
                    CheckInvalidValue(e.Cell.Row.Cells["UsageLimit"]) &&
                    CheckInvalidValue(e.Cell.Row.Cells["UsageLimitUpper"]))
                {
                    if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001334", Infragistics.Win.HAlign.Right);

                        //this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        e.Cell.Row.Cells["UsageLimitUpper"].Value = DBNull.Value;
                        e.Cell.Row.Cells["UsageLimitUpper"].Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                    if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001337", Infragistics.Win.HAlign.Right);

                        //this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                        e.Cell.Row.Cells["UsageLimit"].Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
                else
                    return;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장용 데이터 테이블 반환 메소드
        /// </summary>
        private DataTable Rtn_Save_LotSpecInfoDate()
        {
            DataTable dtLotSpecInfo = new DataTable();
            try
            {
                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                brwChannel.mfCredentials(clsLotSpecInfo);

                dtLotSpecInfo = clsLotSpecInfo.mfSetDataInfo_frmMASZ0032();

                for (int i = 0; i < this.uGridLotNoSpecInfo.Rows.Count; i++)
                {
                    if (!Convert.ToBoolean(this.uGridLotNoSpecInfo.Rows[i].Cells["Check"].Value))
                    {
                        DataRow drRow = dtLotSpecInfo.NewRow();
                        drRow["PlantCode"] = this.uTextPlantCode.Text;
                        drRow["DurableMatCode"] = this.uTextDurableMatCode.Text;
                        drRow["DurableMatName"] = this.uTextDurableMatName.Text;
                        drRow["LotNo"] = this.uTextLotNo.Text;

                        drRow["Package"] = this.uTextPackage.Text;
                        drRow["EMC"] = this.uTextEMC.Text;

                        drRow["SpecCode"] = this.uGridLotNoSpecInfo.Rows[i].Cells["SpecCode"].Value.ToString();
                        drRow["UsageLimitLower"] = this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimitLower"].Value;
                        drRow["UsageLimit"] = this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimit"].Value;
                        drRow["UsageLimitUpper"] = this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimitUpper"].Value;
                        dtLotSpecInfo.Rows.Add(drRow);
                    }
                }

                return dtLotSpecInfo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtLotSpecInfo;
            }
            finally
            {
                dtLotSpecInfo.Dispose();
            }
        }

        /// <summary>
        /// 삭제용 데이터 테이블 반환 메소드
        /// </summary>
        private DataTable Rtn_Delete_LotSpecInfoDate()
        {
            DataTable dtLotSpecInfo = new DataTable();
            try
            {
                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                brwChannel.mfCredentials(clsLotSpecInfo);

                dtLotSpecInfo = clsLotSpecInfo.mfSetDataInfo_frmMASZ0032();

                for (int i = 0; i < this.uGridLotNoSpecInfo.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridLotNoSpecInfo.Rows[i].Cells["Check"].Value))
                    {
                        DataRow drRow = dtLotSpecInfo.NewRow();
                        drRow["PlantCode"] = this.uTextPlantCode.Text;
                        drRow["DurableMatCode"] = this.uTextDurableMatCode.Text;
                        drRow["LotNo"] = this.uTextLotNo.Text;

                        drRow["Package"] = this.uTextPackage.Text;
                        drRow["EMC"] = this.uTextEMC.Text;

                        drRow["SpecCode"] = this.uGridLotNoSpecInfo.Rows[i].Cells["SpecCode"].Value.ToString();
                        dtLotSpecInfo.Rows.Add(drRow);
                    }
                }

                return dtLotSpecInfo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtLotSpecInfo;
            }
            finally
            {
                dtLotSpecInfo.Dispose();
            }
        }

        /// <summary>
        /// Equip 그리드 정보저장
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_Save_EquipInfoData()
        {
            DataTable dtDurableMatBOM_Equip = new DataTable();
            try
            {

                //금형치공구BOM_Equip정보 BL호출  2013-01-10
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMatBOM_Equip), "DurableMatBOM_Equip");
                QRPMAS.BL.MASDMM.DurableMatBOM_Equip clsDurableMatBOM_Equip = new QRPMAS.BL.MASDMM.DurableMatBOM_Equip();
                brwChannel.mfCredentials(clsDurableMatBOM_Equip);

                // 금형치공구 BOM_Equip
                dtDurableMatBOM_Equip = clsDurableMatBOM_Equip.mfSetDataInfo();

                //정보 저장(편집이미지가 있고 숨김상태가아닌것은 저장 숨김상태인것은 행삭제 저장
                if (this.uGridEquip.Rows.Count > 0 
                    && !this.uTextDurableMatCode.Text.Equals(string.Empty))
                {
                    for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                    {
                        if (this.uGridEquip.Rows[i].Hidden)
                            continue;

                        DataRow drSave;
                        drSave = dtDurableMatBOM_Equip.NewRow();

                        // 공장코드
                        if (this.uGridEquip.Rows[i].GetCellValue("PlantCode").ToString().Equals(string.Empty))
                            drSave["PlantCode"] = this.uTextPlantCode.Text;
                        else
                            drSave["PlantCode"] = this.uGridEquip.Rows[i].GetCellValue("PlantCode");

                        // 치공구코드
                        if (this.uGridEquip.Rows[i].GetCellValue("DurableMatCode").ToString().Equals(string.Empty))
                            drSave["DurableMatCode"] = this.uTextDurableMatCode.Text;
                        else
                            drSave["DurableMatCode"] = this.uGridEquip.Rows[i].GetCellValue("DurableMatCode");

                        // LotNo
                        drSave["LotNo"] = this.uTextLotNo.Text;

                        // 설비코드
                        drSave["EquipCode"] = this.uGridEquip.Rows[i].GetCellValue("EquipCode");


                        dtDurableMatBOM_Equip.Rows.Add(drSave);

                        
                    }
                }

                return dtDurableMatBOM_Equip;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtDurableMatBOM_Equip;
            }
            finally
            {
                dtDurableMatBOM_Equip.Dispose();
            }
        }

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();
                string strDefault = "전체";

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboEquipType.Items.Clear();
                    this.uComboEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboEquipType.Items.Clear();
                    this.uComboEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 정보 Clear
        /// </summary>
        private void Init()
        {
            this.uTextPlantCode.Clear();
            this.uTextDurableMatCode.Clear();
            this.uTextDurableMatName.Clear();
            this.uTextLotNo.Clear();

            if (this.uComboEquipStation.Items.Count > 0)
                this.uComboEquipStation.Items.Clear();

            //if (this.uComboEquipLoc.Items.Count > 0)
            //    this.uComboEquipLoc.Items.Clear();

            //if (this.uComboProcessGroup.Items.Count > 0)
            //    this.uComboProcessGroup.Items.Clear();

            //if (this.uComboEquipType.Items.Count > 0)
            //    this.uComboEquipType.Items.Clear();

            //if (this.uComboEquipGroup.Items.Count > 0)
            //    this.uComboEquipGroup.Items.Clear();

            while (this.uGridDurableSpec.Rows.Count > 0)
                this.uGridDurableSpec.Rows[0].Delete(false);

            while (this.uGridLotNoSpecInfo.Rows.Count > 0)
                this.uGridLotNoSpecInfo.Rows[0].Delete(false);

            //설비List그리드 전체행을 선택하여 삭제
            if (this.uGridEquipList.Rows.Count > 0)
            {
                this.uGridEquipList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipList.Rows.All);
                this.uGridEquipList.DeleteSelectedRows(false);
            }

            //설비그리드 전체행을 선택하여 삭제
            if (this.uGridEquip.Rows.Count > 0)
            {
                this.uGridEquip.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquip.Rows.All);
                this.uGridEquip.DeleteSelectedRows(false);
            }
        }

        #endregion



    }
}
