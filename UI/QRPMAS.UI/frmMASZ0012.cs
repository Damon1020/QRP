﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMASZ0012.cs                                        */
/* 프로그램명   : 품종교체점검Point정보                                 */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-05 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
namespace QRPMAS.UI
{
    public partial class frmMASZ0012 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmMASZ0012()
        {
            InitializeComponent();
        }
        private void frmMASZ0012_Activated(object sender, EventArgs e)
        {
            //툴바활성
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //사용여부설정
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0012_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            titleArea.mfSetLabelText("품종교체점검Point정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤 초기화
            SetToolAuth();
            InitGrid();
            InitLabel();
            InitComboBox();
        }
       
        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGridDeviceChgPoint, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridDeviceChgPoint, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDeviceChgPoint, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                grd.mfSetGridColumn(this.uGridDeviceChgPoint, 0, "DeviceChgPointCode", "품종교체점검Point코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 5, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPoint, 0, "DeviceChgPointName", "품종교체점검Point명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPoint, 0, "DeviceChgPointNameCh", "품종교체점검Point명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPoint, 0, "DeviceChgPointNameEn", "품종교체점검Point명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPoint, 0, "CCSFlag", "CCS의뢰대상여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDeviceChgPoint, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //폰트설정
                this.uGridDeviceChgPoint.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDeviceChgPoint.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //사용여부
                DataTable dtUseFlag = new DataTable();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                //공장리스트
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //그리드에넣기
                grd.mfSetGridColumnValueList(this.uGridDeviceChgPoint, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUseFlag);

                grd.mfSetGridColumnValueList(this.uGridDeviceChgPoint, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);
                //한줄생성
                grd.mfAddRowGrid(this.uGridDeviceChgPoint, 0);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion


        #region 툴바관련
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //ProgressBar호출
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리로직 -----------------------------------------------------
                //Bl호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPoint), "DeviceChgPoint");
                QRPMAS.BL.MASEQU.DeviceChgPoint clsDeviceChgPoint = new QRPMAS.BL.MASEQU.DeviceChgPoint();
                brwChannel.mfCredentials(clsDeviceChgPoint);
                //공장코드
                string strPlantCode = uComboPlant.Value.ToString();
                //매서드호출
                DataTable dtDeviceChgPoint = clsDeviceChgPoint.mfReadDeviceChgPoint(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //데이터바인드
                this.uGridDeviceChgPoint.DataSource = dtDeviceChgPoint;
                this.uGridDeviceChgPoint.DataBind();

               
                //---------------------------------------------------------------
                //커서변경
                this.MdiParent.Cursor = Cursors.Default;
                //ProgressBar닫기
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtDeviceChgPoint.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    WinMessageBox msg = new WinMessageBox();
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //PK 처리
                    for (int i = 0; i < dtDeviceChgPoint.Rows.Count; i++)
                    {
                        this.uGridDeviceChgPoint.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDeviceChgPoint.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                        this.uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointCode"].Appearance.BackColor = Color.Gainsboro;

                    }

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDeviceChgPoint, 0);
                }
            }

            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGridDeviceChgPoint.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인",  "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 정보저장

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPoint), "DeviceChgPoint");
                QRPMAS.BL.MASEQU.DeviceChgPoint clsDeviceChgPoint = new QRPMAS.BL.MASEQU.DeviceChgPoint();
                brwChannel.mfCredentials(clsDeviceChgPoint);

                //-----------------------------------------처리 로직--------------------------------------------------------//

                DataTable dtDevicechgPoint = clsDeviceChgPoint.mfDataSet();

                if(this.uGridDeviceChgPoint.Rows.Count > 0)
                    this.uGridDeviceChgPoint.ActiveCell = this.uGridDeviceChgPoint.Rows[0].Cells[0];  //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.

                for (int i = 0; i < uGridDeviceChgPoint.Rows.Count; i++)
                {
                    if (this.uGridDeviceChgPoint.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        #region 필수입력사항

                        string strRowNum = this.uGridDeviceChgPoint.Rows[i].RowSelectorNumber.ToString();
                        
                        //콤보박스에 지정되어있지않은 코드 값인지 체크
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;

                        if (this.uGridDeviceChgPoint.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", strRowNum + "번째 열의 공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                            // Focus Cell
                            this.uGridDeviceChgPoint.ActiveCell = this.uGridDeviceChgPoint.Rows[i].Cells["PlantCode"];
                            this.uGridDeviceChgPoint.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;

                        }
                        if (this.uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointCode"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", strRowNum + "번째 열의 품종교체점검Point코드를 입력해주세요.", Infragistics.Win.HAlign.Right);

                            // Focus Cell
                            this.uGridDeviceChgPoint.ActiveCell = this.uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointCode"];
                            this.uGridDeviceChgPoint.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (this.uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointName"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", strRowNum + "번째 열의 품종교체점검Point명을 입력해주세요.", Infragistics.Win.HAlign.Right);

                            // Focus Cell
                            this.uGridDeviceChgPoint.ActiveCell = this.uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointName"];
                            this.uGridDeviceChgPoint.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        if (uGridDeviceChgPoint.Rows[i].Cells["CCSFlag"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", strRowNum + "번째 열의 CCS의뢰대상여부를 선택해주세요.", Infragistics.Win.HAlign.Right);

                            return;
                        }
                        if (uGridDeviceChgPoint.Rows[i].Cells["UseFlag"].Value.ToString().Equals(string.Empty))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", strRowNum + "번째 열의 사용여부를 선택해주세요.", Infragistics.Win.HAlign.Right);

                            // Focus Cell
                            this.uGridDeviceChgPoint.ActiveCell = this.uGridDeviceChgPoint.Rows[i].Cells["UseFlag"];
                            this.uGridDeviceChgPoint.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }

                        #endregion
                        
                        DataRow drDeviceChgPoint;
                        drDeviceChgPoint = dtDevicechgPoint.NewRow();
                        drDeviceChgPoint["PlantCode"] = uGridDeviceChgPoint.Rows[i].Cells["PlantCode"].Value.ToString();
                        drDeviceChgPoint["DeviceChgPointCode"] = uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointCode"].Value.ToString();
                        drDeviceChgPoint["DeviceChgPointName"] = uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointName"].Value.ToString();
                        drDeviceChgPoint["DeviceChgPointNameCh"] = uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointNameCh"].Value.ToString();
                        drDeviceChgPoint["DeviceChgPointNameEn"] = uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointNameEn"].Value.ToString();
                        drDeviceChgPoint["CCSFlag"] = uGridDeviceChgPoint.Rows[i].Cells["CCSFlag"].Value.ToString();
                        drDeviceChgPoint["UseFlag"] = uGridDeviceChgPoint.Rows[i].Cells["UseFlag"].Value.ToString();
                        dtDevicechgPoint.Rows.Add(drDeviceChgPoint);

                        
                    }
                }

                if (dtDevicechgPoint.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "저장할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                //---------------------------------------------------------------------------------------------------------//
                //저장여부 박스

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //Progressbar 시작
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPopup = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    //커서변경
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    string rtMSG = clsDeviceChgPoint.mfSaveDeviceChgPoint(dtDevicechgPoint, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                    //커서변경
                    this.MdiParent.Cursor = Cursors.Default;
                    //Progressbar닫기
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

               
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                if (this.uGridDeviceChgPoint.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "입력사항확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 데이터 저장

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPoint), "DeviceChgPoint");
                QRPMAS.BL.MASEQU.DeviceChgPoint clsDeviceChgPoint = new QRPMAS.BL.MASEQU.DeviceChgPoint();
                brwChannel.mfCredentials(clsDeviceChgPoint);
                //데이터컬럼설정
                DataTable dtDevicechgPoint = clsDeviceChgPoint.mfDataSet();

                //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                this.uGridDeviceChgPoint.ActiveCell = this.uGridDeviceChgPoint.Rows[0].Cells[0];

                for (int i = 0; i < uGridDeviceChgPoint.Rows.Count; i++)
                {

                    if (Convert.ToBoolean(this.uGridDeviceChgPoint.Rows[i].Cells["Check"].Value) == true)
                    {
                        string strRowNum = this.uGridDeviceChgPoint.Rows[i].RowSelectorNumber.ToString();

                        if (this.uGridDeviceChgPoint.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", strRowNum + "번째 열의 공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                            // Focus Cell
                            this.uGridDeviceChgPoint.ActiveCell = this.uGridDeviceChgPoint.Rows[i].Cells["PlantCode"];
                            this.uGridDeviceChgPoint.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;

                        }
                        if (this.uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                              "확인창", "필수입력사항확인", strRowNum + "번째 열의 품종교체점검Point코드를 입력해주세요.", Infragistics.Win.HAlign.Right);

                            // Focus Cell
                            this.uGridDeviceChgPoint.ActiveCell = this.uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointCode"];
                            this.uGridDeviceChgPoint.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                            return;
                        }
                        
                        DataRow drDeviceChgPoint;
                        drDeviceChgPoint = dtDevicechgPoint.NewRow();
                        drDeviceChgPoint["PlantCode"] = uGridDeviceChgPoint.Rows[i].Cells["PlantCode"].Value.ToString();
                        drDeviceChgPoint["DeviceChgPointCode"] = uGridDeviceChgPoint.Rows[i].Cells["DeviceChgPointCode"].Value.ToString();
                        dtDevicechgPoint.Rows.Add(drDeviceChgPoint);

                        
                    }
                }

                if (dtDevicechgPoint.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "확인창", "입력사항확인", "삭제할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }


                #endregion

                //---------------------------------------------확인 끝 ----------------------------------------------//

                //삭제 확인
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;
                    //처리 로직//
                    //매서드호출
                    string strRtn = clsDeviceChgPoint.mfDeleteDeviceChgPoint(dtDevicechgPoint);
                    
                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    /////////////
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfCreate()
        {
            //try
            //{
            //    while (this.uGridDeviceChgPoint.Rows.Count > 0)
            //    {
            //        this.uGridDeviceChgPoint.Rows[0].Delete(false);
            //    }
            //    this.uGridDeviceChgPoint.Rows[0].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            //    this.uGridDeviceChgPoint.Rows[0].Cells["DeviceChgPointCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            //}
            //catch (Exception ex)
            //{ }
            //finally
            //{ }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridDeviceChgPoint.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "출력정보 확인", "엑셀 출력정보가 없습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                WinGrid grd = new WinGrid();

                grd.mfDownLoadGridToExcel(this.uGridDeviceChgPoint);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
        }
#endregion

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGridDeviceChgPoint_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
              
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDeviceChgPoint, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDeviceChgPoint_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            //RowSelector란에 편집이미지를 나타나게함
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

        }
    }
}
