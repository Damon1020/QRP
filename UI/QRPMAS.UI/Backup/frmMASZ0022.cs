﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMASZ0022.cs                                        */
/* 프로그램명   : CCS 의뢰사유 정보                                     */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : 2011-08-04 : 삭제, 조회, 저장 수정 (정  결)           */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMASZ0022 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0022()
        {
            InitializeComponent();
        }

        private void frmMASZ0022_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0022_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("CCS 의뢰사유 정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        #region Init
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchProcessGroup, "공정그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchPlant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left,
                    m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue("A", m_resSys.GetString("SYS_LANG")), "PlantCode", "PlantName", dtPlant);                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                     , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGrid, 0, "ProcessGroup", "공정그룹", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CCSReqTypeCode", "의뢰사유코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CCSReqTypeName", "의뢰사유명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CCSReqTypeNameCh", "의뢰사유명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "CCSReqTypeNameEn", "의뢰사유명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "MESReqTypeCode", "MES 의뢰사유코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                // DropDown 설정
                // Plant
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", ComboDefaultValue("S", m_resSys.GetString("SYS_LANG")), dt);

                // 공정그룹
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcessGroup = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcessGroup);

                DataTable dtProcessGroup = clsProcessGroup.mfReadMASProcessGroup(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGrid, 0, "ProcessGroup", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", ComboDefaultValue("S", m_resSys.GetString("SYS_LANG")), dtProcessGroup);
                                           
                // 사용여부
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                dt = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                // AddRow
                wGrid.mfAddRowGrid(this.uGrid, 0);

                // Set FontSize
                this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">S : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun, string strLang)
        {
            try
            {
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                strLang = strLang.ToUpper();
                if (strGubun.Equals("S"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        #endregion

        #region ToolBar
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqType), "CCSReqType");
                QRPMAS.BL.MASQUA.CCSReqType ccsReqType = new QRPMAS.BL.MASQUA.CCSReqType();
                brwChannel.mfCredentials(ccsReqType);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();
                String strProcessGroup = this.uComboSearchProcessGroup.Value.ToString();


                DataTable dt = ccsReqType.mfReadCCSReqType(strPlantCode, strProcessGroup);
                this.uGrid.DataSource = dt;
                this.uGrid.DataBind();                

                // Bl 연결
                brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                brwChannel.mfCredentials(clsProcess);

                WinGrid grid = new WinGrid();

                DataTable dtProcessGroup = clsProcess.mfReadMASProcessGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < uGrid.Rows.Count; i++)
                {                    
                    //String strPlantCode1 = this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString();
                    
                    grid.mfSetGridCellValueList(this.uGrid, i, "ProcessGroup", "", "", dtProcessGroup);

                    this.uGrid.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                    this.uGrid.Rows[i].Cells["ProcessGroup"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid.Rows[i].Cells["ProcessGroup"].Appearance.BackColor = Color.Gainsboro;
                    this.uGrid.Rows[i].Cells["CCSReqTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGrid.Rows[i].Cells["CCSReqTypeCode"].Appearance.BackColor = Color.Gainsboro;
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid, 0);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>        
        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                // BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqType), "CCSReqType");
                QRPMAS.BL.MASQUA.CCSReqType ccsReqType = new QRPMAS.BL.MASQUA.CCSReqType();
                brwChannel.mfCredentials(ccsReqType);

                // 저장 함수호출 매개변수 DataTable
                DataTable dtCCSReqType = ccsReqType.mfSetDataInfo();

                DialogResult DResult = new DialogResult();

                if(this.uGrid.Rows.Count > 0)
                    this.uGrid.ActiveCell = this.uGrid.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");

                for (int i = 0; i < this.uGrid.Rows.Count; i++)
                {
                    //그리드가 수정되었을때 저장
                    if (this.uGrid.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        //필수 입력사항 확인
                        if (this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGrid.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000560",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["PlantCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["ProcessGroup"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGrid.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000561",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["ProcessGroup"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["CCSReqTypeCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGrid.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000569",strLang)
                                , Infragistics.Win.HAlign.Center);

                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["CCSReqTypeCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else
                        {
                            DataRow drCCSReqType = dtCCSReqType.NewRow();
                            drCCSReqType["PlantCode"] = this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString();
                            drCCSReqType["ProcessGroup"] = this.uGrid.Rows[i].Cells["ProcessGroup"].Value.ToString();
                            drCCSReqType["CCSReqTypeCode"] = this.uGrid.Rows[i].Cells["CCSReqTypeCode"].Value.ToString();
                            drCCSReqType["CCSReqTypeName"] = this.uGrid.Rows[i].Cells["CCSReqTypeName"].Value.ToString();
                            drCCSReqType["CCSReqTypeNameCh"] = this.uGrid.Rows[i].Cells["CCSReqTypeNameCh"].Value.ToString();
                            drCCSReqType["CCSReqTypeNameEn"] = this.uGrid.Rows[i].Cells["CCSReqTypeNameEn"].Value.ToString();
                            drCCSReqType["MESReqTypeCode"] = this.uGrid.Rows[i].Cells["MESReqTypeCode"].Value.ToString();
                            drCCSReqType["UseFlag"] = this.uGrid.Rows[i].Cells["UseFlag"].Value.ToString();
                            dtCCSReqType.Rows.Add(drCCSReqType);
                        }
                    }
                }
                if (dtCCSReqType.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //저장함수 호출
                        String strCCSReqType = ccsReqType.mfSaveCCSReqType(dtCCSReqType, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        //Decoding
                        TransErrRtn ErrEtn = new TransErrRtn();
                        ErrEtn = ErrEtn.mfDecodingErrMessage(strCCSReqType);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        //처리결과에 따른 메세지 박스
                        if (ErrEtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000930"
                            , Infragistics.Win.HAlign.Right);

                            mfSearch();
                        }
                        else
                        {
                            string strMes = "";
                            if (ErrEtn.ErrMessage.Equals(string.Empty))
                                strMes = msg.GetMessge_Text("M000953", strLang);
                            else
                                strMes = ErrEtn.ErrMessage;


                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMes
                                , Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제 
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqType), "CCSReqType");
                QRPMAS.BL.MASQUA.CCSReqType ccsReqType = new QRPMAS.BL.MASQUA.CCSReqType();
                brwChannel.mfCredentials(ccsReqType);

                // 매개변수로 넘겨줄 DataTable 설정
                DataTable dtCCSReqType = ccsReqType.mfSetDataInfo();

                string strLang = m_resSys.GetString("SYS_LANG");

                // 활성셀을 첫번쨰줄 첫번쨰 셀로 이동
                if(uGrid.Rows.Count > 0)
                    this.uGrid.ActiveCell = this.uGrid.Rows[0].Cells[0];

                // check 삭제
                for(int i = 0; i < uGrid.Rows.Count; i++)
                {
                    if(Convert.ToBoolean(this.uGrid.Rows[i].Cells["Check"].Value) == true)
                    {
                        // 필수입력사항 확인
                        if (this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString() =="")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGrid.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000560",strLang)
                                , Infragistics.Win.HAlign.Center);

                             //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["PlantCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["ProcessGroup"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGrid.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000482",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //FocusCell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["ProcessGroup"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["CCSReqTypeCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGrid.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000569",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //FocusCell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["CCSReqTypeCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            DataRow drCCSReqType = dtCCSReqType.NewRow();
                            drCCSReqType["PlantCode"] = this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString();
                            drCCSReqType["ProcessGroup"] = this.uGrid.Rows[i].Cells["ProcessGroup"].Value.ToString();
                            drCCSReqType["CCSReqTypeCode"] = this.uGrid.Rows[i].Cells["CCSReqTypeCode"].Value.ToString();
                            dtCCSReqType.Rows.Add(drCCSReqType);
                        }
                    }
                }

                if (dtCCSReqType.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;
                        // 처리 로직 //


                        // Call Delete Method
                        string rtMSG = ccsReqType.mfDeleteCCSReqType(dtCCSReqType);

                        // Decoding //
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        // 처리로직 끝 //

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        DialogResult DResult = new DialogResult();
                        // 삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000926",
                                                Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M000638", "M000923",
                                                Infragistics.Win.HAlign.Right);
                        }
                        String strPlantCode = this.uComboSearchPlant.Value.ToString();
                        String strProcessGroup = this.uComboSearchProcessGroup.Value.ToString();

                        DataTable dt = ccsReqType.mfReadCCSReqType(strPlantCode, strProcessGroup);
                        this.uGrid.DataSource = dt;
                        this.uGrid.DataBind();

                        WinGrid grd = new WinGrid();

                        // 바인딩후 체크박스 상태를 모두 Uncheck로 만든다
                        grd.mfSetAllUnCheckedGridColumn(this.uGrid, 0, "Check");

                        // RowSelector Clear
                        grd.mfClearRowSeletorGrid(this.uGrid);

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                    }
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M000648", Infragistics.Win.HAlign.Center);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                //mfSearch();
            }
        }

        public void mfCreate()
        {
            try
            {
                this.uComboSearchPlant.Value = "";
                this.uComboSearchProcessGroup.Value = "";
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            try
            {
                //처리로직
                WinGrid grd = new WinGrid();
                //엑셀 저장 함수 호출
                grd.mfDownLoadGridToExcel(this.uGrid);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 일반 Event
        private void uGrid_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
                // 자동행삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                if (e.Cell.Column.Key == "PlantCode")
                {
                    // 공정 DropDown 설정
                    WinGrid wGrid = new WinGrid();

                    this.uGrid.ActiveRow.Cells["ProcessGroup"].Value = "";

                    //string strPlantCode = this.uGrid.ActiveRow.Cells["PlantCode"].Value.ToString();
                    //string strPlantCode = this.uGrid.Rows[i].Cells["Plantcode"].Value.ToString();
                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcessGroup = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcessGroup);

                    DataTable dtProcessGroup = clsProcessGroup.mfReadMASProcessGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    //wGrid.mfSetGridColumnValueList(this.uGrid, e.Cell.Row.Index, "ProcessGroup", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    //                                , "", "선택", dtProcessGroup);
                    wGrid.mfSetGridCellValueList(this.uGrid, e.Cell.Row.Index, "ProcessGroup", "", ComboDefaultValue("S", m_resSys.GetString("SYS_LANG")), dtProcessGroup);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtProcessGroup = new DataTable();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchProcessGroup.Items.Clear();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcessGroup = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcessGroup);

                    dtProcessGroup = clsProcessGroup.mfReadMASProcessGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue("A", m_resSys.GetString("SYS_LANG"))
                    , "ProcessGroup", "ComboName", dtProcessGroup);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMASZ0022_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }
        #endregion
    }
}
