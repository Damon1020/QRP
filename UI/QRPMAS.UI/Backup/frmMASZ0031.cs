﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMASZ0031.cs                                        */
/* 프로그램명   : 정비사현황                                            */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-12-01                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMASZ0031 : Form, IToolbar
    {
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0031()
        {
            InitializeComponent();
        }

        #region Fomr Events

        private void frmMASZ0031_Activated(object sender, EventArgs e)
        {
            //Toolbar활성화
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfActiveToolBar(this.MdiParent, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0031_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정
            titleArea.mfSetLabelText("수명관리 Spec 기준정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitGrid();
        }

        private void frmMASZ0031_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        #endregion

        #region IToolbar 멤버

        public void mfCreate()
        {
            
        }

        public void mfDelete()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableSpec), "DurableSpec");
                QRPMAS.BL.MASDMM.DurableSpec clsDSpec = new QRPMAS.BL.MASDMM.DurableSpec();
                brwChannel.mfCredentials(clsDSpec);

                DataTable dtDelete = clsDSpec.mfSetDataInfo();

                this.uGridDurableSpecLIst.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                string strLang = m_resSys.GetString("SYS_LANG");
                // 필수입력사항 확인
                for (int i = 0; i < this.uGridDurableSpecLIst.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDurableSpecLIst.Rows[i].Cells["Check"].Value))
                    {
                        if (this.uGridDurableSpecLIst.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M000881",strLang)
                                                        , msg.GetMessge_Text("M001228",strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000481",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                            this.uGridDurableSpecLIst.Rows[i].Cells["PlantCode"].Activate();
                            this.uGridDurableSpecLIst.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridDurableSpecLIst.Rows[i].Cells["SpecCode"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M000881",strLang)
                                                        , msg.GetMessge_Text("M001228",strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M001290",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                            this.uGridDurableSpecLIst.Rows[i].Cells["SpecCode"].Activate();
                            this.uGridDurableSpecLIst.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else
                        {
                            DataRow drRow = dtDelete.NewRow();
                            drRow["PlantCode"] = this.uGridDurableSpecLIst.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["SpecCode"] = this.uGridDurableSpecLIst.Rows[i].Cells["SpecCode"].Value.ToString();
                            dtDelete.Rows.Add(drRow);
                        }
                    }
                }

                if (dtDelete.Rows.Count > 0)
                {
                    // 삭제여부를 묻는다
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000922", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // ProgressBar 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 메소드 호출
                        string strErrRtn = clsDSpec.mfDeleteMASDurableSpec(dtDelete);

                        // ProgressBar Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 결과 검사
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M000638", "M000677",
                                                        Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", "M000923",
                                                            Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M000638", ErrRtn.ErrMessage,
                                                            Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M000634", "M000634", "M000644", Infragistics.Win.HAlign.Right);

                    return;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                if (this.uGridDurableSpecLIst.Rows.Count > 0)
                {
                    WinGrid wGrid = new WinGrid();
                    wGrid.mfDownLoadGridToExcel(this.uGridDurableSpecLIst);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            
        }

        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableSpec), "DurableSpec");
                QRPMAS.BL.MASDMM.DurableSpec clsDSpec = new QRPMAS.BL.MASDMM.DurableSpec();
                brwChannel.mfCredentials(clsDSpec);

                DataTable dtSave = clsDSpec.mfSetDataInfo();

                this.uGridDurableSpecLIst.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                string strLang = m_resSys.GetString("SYS_LANG");

                // 필수입력사항 확인
                for (int i = 0; i < this.uGridDurableSpecLIst.Rows.Count; i++)
                {
                    if (this.uGridDurableSpecLIst.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        if (this.uGridDurableSpecLIst.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M000881",strLang)
                                                        , msg.GetMessge_Text("M001228",strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M000481",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                            this.uGridDurableSpecLIst.Rows[i].Cells["PlantCode"].Activate();
                            this.uGridDurableSpecLIst.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridDurableSpecLIst.Rows[i].Cells["SpecCode"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M000881",strLang)
                                                        , msg.GetMessge_Text("M001228",strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M001290",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                            this.uGridDurableSpecLIst.Rows[i].Cells["SpecCode"].Activate();
                            this.uGridDurableSpecLIst.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGridDurableSpecLIst.Rows[i].Cells["SpecName"].Value.ToString().Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                        , msg.GetMessge_Text("M000881",strLang)
                                                        , msg.GetMessge_Text("M001228",strLang)
                                                        , (i + 1).ToString() + msg.GetMessge_Text("M001289",strLang)
                                                        , Infragistics.Win.HAlign.Right);

                            this.uGridDurableSpecLIst.Rows[i].Cells["SpecName"].Activate();
                            this.uGridDurableSpecLIst.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else
                        {
                            DataRow drRow = dtSave.NewRow();
                            drRow["PlantCode"] = this.uGridDurableSpecLIst.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["SpecCode"] = this.uGridDurableSpecLIst.Rows[i].Cells["SpecCode"].Value.ToString();
                            drRow["SpecName"] = this.uGridDurableSpecLIst.Rows[i].Cells["SpecName"].Value.ToString();
                            dtSave.Rows.Add(drRow);
                        }
                    }
                }

                if (dtSave.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 프로그래스 팝업창 생성
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        string strErrRtn = clsDSpec.mfSaveMASDurableSpec(dtSave, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        // 팦업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum.Equals(0))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M001037", "M000930",
                                                            Infragistics.Win.HAlign.Right);

                            // List 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001023", "M000916", Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001264", "M001023", ErrRtn.ErrMessage, Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001032", "M001032", "M001049", Infragistics.Win.HAlign.Right);

                    return;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableSpec), "DurableSpec");
                QRPMAS.BL.MASDMM.DurableSpec clsDSpec = new QRPMAS.BL.MASDMM.DurableSpec();
                brwChannel.mfCredentials(clsDSpec);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtDurableSpecList = clsDSpec.mfReadMASDurableSpec(m_resSys.GetString("SYS_PLANTCODE"));

                this.uGridDurableSpecLIst.SetDataBinding(dtDurableSpecList, string.Empty);

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (this.uGridDurableSpecLIst.Rows.Count <= 0)
                {
                    
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDurableSpecLIst, 0);

                    for (int i = 0; i < this.uGridDurableSpecLIst.Rows.Count; i++)
                    {
                        this.uGridDurableSpecLIst.Rows[i].Cells["SpecCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Init Control

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableSpecLIst, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정

                wGrid.mfSetGridColumn(this.uGridDurableSpecLIst, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurableSpecLIst, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridDurableSpecLIst, 0, "SpecCode", "Spec코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 30
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableSpecLIst, 0, "SpecName", "Spec명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridDurableSpecLIst, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                //폰트설정
                this.uGridDurableSpecLIst.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableSpecLIst.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                wGrid.mfAddRowGrid(this.uGridDurableSpecLIst, 0);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Grid Event

        private void uGridDurableSpecLIst_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableSpecLIst_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGridDurableSpecLIst, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// 저장정보 반환 메소드
        /// </summary>
        /// <returns></returns>
        private DataTable Rtn_DataTable()
        {
            DataTable dtRtn = new DataTable();
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableSpec), "DurableSpec");
                QRPMAS.BL.MASDMM.DurableSpec clsDSpec = new QRPMAS.BL.MASDMM.DurableSpec();
                brwChannel.mfCredentials(clsDSpec);

                dtRtn = clsDSpec.mfSetDataInfo();

                for (int i = 0; i < this.uGridDurableSpecLIst.Rows.Count; i++)
                {
                    if (this.uGridDurableSpecLIst.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        DataRow drRow = dtRtn.NewRow();
                        drRow["PlantCode"] = this.uGridDurableSpecLIst.Rows[i].Cells["PlantCode"].Value.ToString();
                        drRow["SpecCode"] = this.uGridDurableSpecLIst.Rows[i].Cells["SpecCode"].Value.ToString();
                        drRow["SpecName"] = this.uGridDurableSpecLIst.Rows[i].Cells["SpecName"].Value.ToString();
                        dtRtn.Rows.Add(drRow);
                    }
                }
                return dtRtn;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtRtn;
            }
            finally
            {
                dtRtn.Dispose();
            }
        }

        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">C : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();
                if (strGubun.Equals("C"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }
    }
}
