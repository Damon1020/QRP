﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0018.cs                                         */
/* 프로그램명   : 검사분류정보등록                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가참조
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMAS0018_S : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0018_S()
        {
            InitializeComponent();
        }

        private void frmMAS0018_Activated(object sender, EventArgs e)
        {
            // ToolBar 활성화
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0018_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("검사분류정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitTextBox();
            InitGroupBox();
            InitGrid();
            InitLabel();
            InitComboBox();
            InitButton();

            // ExtandableGroupBox 설정
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitTextBox()
        {
            try
            {
                this.uTextInspectGroupCode.MaxLength = 10;
                this.uTextInspectGroupName.MaxLength = 50;
                this.uTextInspectGroupNameCh.MaxLength = 50;
                this.uTextInspectGroupNameEn.MaxLength = 50;
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }
        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonDelRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                WinGroupBox gb = new WinGroupBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 검사분류정보 GroupBox
                gb.mfSetGroupBox(this.uGroupBoxInspectGroup, GroupBoxType.INFO, "검사분류정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // 검사유형정보 GroupBox
                gb.mfSetGroupBox(this.uGroupBoxInspectType, GroupBoxType.LIST, "검사유형정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBoxInspectGroup.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxInspectGroup.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxInspectType.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxInspectType.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 그리드 일반속성 정의
                // InspectGroup Grid
                grd.mfInitGeneralGrid(this.uGridInspectGroup, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // InspectType Grid
                grd.mfInitGeneralGrid(this.uGridInspectType, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼추가
                // InspectGroup Grid
                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "InspectGroupCode", "검사분류코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "InspectGroupName", "검사분류명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "InspectGroupNameCh", "검사분류명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "InspectGroupNameEn", "검사분류명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectGroup, 0, "MDMTFlag", "MDM전송여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //InspectType Grid
                grd.mfSetGridColumn(this.uGridInspectType, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false
                    , 0, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridInspectType, 0, "InspectTypeCode", "검사유형코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false
                    , 10, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectType, 0, "InspectTypeName", "검사유형명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectType, 0, "InspectTypeNameCh", "검사유형명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectType, 0, "InspectTypeNameEn", "검사유형명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                    , 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInspectType, 0, "BomCheckFlag", "BomCheck", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                     , 0, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridInspectType, 0, "MonitoringFlag", "모니터링 시작여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true
                     , 0, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridInspectType, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false
                    , 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //폰트설정
                uGridInspectGroup.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridInspectGroup.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridInspectType.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridInspectType.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 검사유형 그리드에 DropDown 추가
                // 사용여부
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                
                grd.mfSetGridColumnValueList(this.uGridInspectType, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                // 공백줄 추가
                grd.mfAddRowGrid(this.uGridInspectType, 0);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                WinLabel lb = new WinLabel();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                lb.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lb.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelInspectGroupCode, "검사분류코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelInspectGroupName, "검사분류명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lb.mfSetLabel(this.uLabelInspectGroupNameCh, "검사분류명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(this.uLabelInspectGroupNameEn, "검사분류명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(this.uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                WinComboEditor combo = new WinComboEditor();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                string strLang = m_resSys.GetString("SYS_LANG");
                string strChoice = "";
                string strAll = "";
                if (strLang.Equals("KOR"))
                { strChoice = "선택"; strAll = "전체"; }
                else if (strLang.Equals("CHN"))
                { strChoice = "选择"; strAll = "全部"; }
                else if (strLang.Equals("ENG"))
                { strChoice = "Choice"; strAll = "All"; }

                // 함수호출
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                clsPlant.Dispose();
                // SearchPlant ComboBox
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", strAll
                    , "PlantCode", "PlantName", dtPlant);

                // PlantComboBox
                combo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", strChoice
                    , "PlantCode", "PlantName", dtPlant);

                // 입력용 사용여부 ComboBox
                // 사용유무 콤보박스 초기화
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                clsComCode.Dispose();

                combo.mfSetComboEditor(this.uComboUseFlag, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "T", "", strChoice
                    , "ComCode", "ComCodeName", dtUseFlag);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                QRPMAS.BL.MASQUA.InspectGroup inspectgroup = new QRPMAS.BL.MASQUA.InspectGroup();
                brwChannel.mfCredentials(inspectgroup);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                // 함수호출
                DataTable dt = inspectgroup.mfReadMASInspectGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));
                // DataBinding
                this.uGridInspectGroup.DataSource = dt;
                this.uGridInspectGroup.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                         , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridInspectGroup, 0);

                    for (int i = 0; i < this.uGridInspectGroup.Rows.Count; i++)
                    {
                        if (!this.uGridInspectGroup.Rows[i].Cells["MDMTFlag"].Value.ToString().Equals("T"))
                            this.uGridInspectGroup.Rows[i].Appearance.BackColor = Color.Salmon;
                    }
                }
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DialogResult DResult = new DialogResult();
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001027", "M001028", Infragistics.Win.HAlign.Right);

                    //this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    DataTable dtGroup = new DataTable();
                    DataTable dtSaveType = new DataTable();
                    DataTable dtDelType = new DataTable();
                    DataRow row;

                    // 필수입력사항 확인
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uTextInspectGroupCode.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000180", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextInspectGroupCode.Focus();
                        return;
                    }
                    else if (this.uTextInspectGroupName.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000179", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uTextInspectGroupName.Focus();
                        return;
                    }
                    ////else if (this.uTextInspectGroupNameCh.Text == "")
                    ////{
                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                    , "확인창", "필수입력사항 확인", "검사분류명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

                    ////    // Focus
                    ////    this.uTextInspectGroupNameCh.Focus();
                    ////    return;
                    ////}
                    ////else if (this.uTextInspectGroupNameEn.Text == "")
                    ////{
                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    ////                    , "확인창", "필수입력사항 확인", "검사분류명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

                    ////    // Focus
                    ////    this.uTextInspectGroupNameEn.Focus();
                    ////    return;
                    ////}
                    else if (this.uComboUseFlag.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M000619", Infragistics.Win.HAlign.Center);

                        // Focus
                        this.uComboUseFlag.DropDown();
                        return;
                    }
                    else
                    {
                        //콤보박스 선택값 Validation Check//////////
                        QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                        if (!check.mfCheckValidValueBeforSave(this)) return;
                        ///////////////////////////////////////////

                        // BL 호출
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                        QRPMAS.BL.MASQUA.InspectGroup iGroup = new QRPMAS.BL.MASQUA.InspectGroup();
                        brwChannel.mfCredentials(iGroup);

                        string strServer = m_resSys.GetString("SYS_SERVERPATH") == null ? "" : m_resSys.GetString("SYS_SERVERPATH");

                        dtGroup = iGroup.mfSetDataInfo();

                        row = dtGroup.NewRow();
                        row["PlantCode"] = this.uComboPlant.Value.ToString();
                        row["InspectGroupCode"] = this.uTextInspectGroupCode.Text;
                        row["InspectGroupName"] = this.uTextInspectGroupName.Text;
                        row["InspectGroupNameCh"] = this.uTextInspectGroupNameCh.Text;
                        row["InspectGroupNameEn"] = this.uTextInspectGroupNameEn.Text;
                        row["UseFlag"] = this.uComboUseFlag.Value.ToString();

                        if (strServer.Contains("10.61.61.71") || strServer.Contains("10.61.61.73")) //라이브적용시 주소적고 주석풀기
                            row["SendMDM"] = "T";


                        dtGroup.Rows.Add(row);


                        /////////////////////////////
                        ////////// 검사유형//////////
                        /////////////////////////////
                        // BL 호출
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                        QRPMAS.BL.MASQUA.InspectType iType = new QRPMAS.BL.MASQUA.InspectType();
                        brwChannel.mfCredentials(iType);

                        dtSaveType = iType.mfSetDataInfo();
                        dtDelType = iType.mfSetDataInfo();

                        if (this.uGridInspectType.Rows.Count > 0)
                            this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[0].Cells[0];

                        string strLang = m_resSys.GetString("SYS_LANG");

                        for (int i = 0; i < this.uGridInspectType.Rows.Count; i++)
                        {
                            if (this.uGridInspectType.Rows[i].Hidden == false)
                            {
                                if (this.uGridInspectType.Rows[i].Cells["InspectTypeCode"].Value.ToString() == "")
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang)
                                            , this.uGridInspectType.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000555", strLang)
                                            , Infragistics.Win.HAlign.Center);

                                    // Focus Cell
                                    this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[i].Cells["InspectTypeCode"];
                                    this.uGridInspectType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                else if (this.uGridInspectType.Rows[i].Cells["InspectTypeName"].Value.ToString() == "")
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang)
                                            , this.uGridInspectType.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000554", strLang)
                                            , Infragistics.Win.HAlign.Center);

                                    // Focus Cell
                                    this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[i].Cells["InspectTypeName"];
                                    this.uGridInspectType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                                ////else if (this.uGridInspectType.Rows[i].Cells["InspectTypeNameCh"].Value.ToString() == "")
                                ////{
                                ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                ////            , "확인창", "필수입력사항 확인", this.uGridInspectType.Rows[i].RowSelectorNumber + "번째 열의 검사유형명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

                                ////    // Focus Cell
                                ////    this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[i].Cells["InspectTypeNameCh"];
                                ////    this.uGridInspectType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                ////    return;
                                ////}
                                ////else if (this.uGridInspectType.Rows[i].Cells["InspectTypeNameEn"].Value.ToString() == "")
                                ////{
                                ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                ////            , "확인창", "필수입력사항 확인", this.uGridInspectType.Rows[i].RowSelectorNumber + "번째 열의 검사유형명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

                                ////    // Focus Cell
                                ////    this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[i].Cells["InspectTypeNameEn"];
                                ////    this.uGridInspectType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                ////    return;
                                ////}
                                else
                                {
                                    row = dtSaveType.NewRow();
                                    row["PlantCode"] = this.uComboPlant.Value.ToString();
                                    row["InspectGroupCode"] = this.uTextInspectGroupCode.Text;
                                    row["InspectTypeCode"] = this.uGridInspectType.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                                    row["InspectTypeName"] = this.uGridInspectType.Rows[i].Cells["InspectTypeName"].Value.ToString();
                                    row["InspectTypeNameCh"] = this.uGridInspectType.Rows[i].Cells["InspectTypeNameCh"].Value.ToString();
                                    row["InspectTypeNameEn"] = this.uGridInspectType.Rows[i].Cells["InspectTypeNameEn"].Value.ToString();
                                    if (this.uGridInspectType.Rows[i].Cells["BomCheckFlag"].Value.ToString() == "True")
                                    {
                                        row["BomCheckFlag"] = "T";
                                    }
                                    else
                                    {
                                        row["BomCheckFlag"] = "F";
                                    }
                                    row["Seq"] = this.uGridInspectType.Rows[i].RowSelectorNumber;
                                    if (this.uGridInspectType.Rows[i].Cells["MonitoringFlag"].Value.ToString() == "True")
                                    {
                                        row["MonitoringFlag"] = "T";
                                    }
                                    else
                                    {
                                        row["MonitoringFlag"] = "F";
                                    }
                                    row["UseFlag"] = this.uGridInspectType.Rows[i].Cells["UseFlag"].Value.ToString();
                                    dtSaveType.Rows.Add(row);
                                }
                            }
                            else
                            {
                                row = dtDelType.NewRow();
                                row["PlantCode"] = this.uComboPlant.Value.ToString();
                                row["InspectGroupCode"] = this.uTextInspectGroupCode.Text;
                                row["InspectTypeCode"] = this.uGridInspectType.Rows[i].Cells["InspectTypeCode"].Value.ToString();
                                dtDelType.Rows.Add(row);
                            }
                        }

                        if (dtGroup.Rows.Count > 0)
                        {
                            if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                    , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                                    , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                            {

                                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                                Thread t1 = m_ProgressPopup.mfStartThread();
                                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                                this.MdiParent.Cursor = Cursors.WaitCursor;

                                // 처리 로직 //
                                // 저장함수 호출

                                string rtMSG = iGroup.mfSaveMASInspectGroup(dtGroup, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveType, dtDelType);

                                // Decoding //
                                TransErrRtn ErrRtn = new TransErrRtn();
                                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                                // 처리로직 끝//

                                this.MdiParent.Cursor = Cursors.Default;
                                m_ProgressPopup.mfCloseProgressPopup(this);

                                // 처리결과에 따른 메세지 박스
                                System.Windows.Forms.DialogResult result;
                                if (ErrRtn.ErrNum == 0)
                                {
                                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001037", "M000930",
                                                        Infragistics.Win.HAlign.Right);
                                    ////SaveInspectType();

                                    // 리스트 갱신
                                    mfSearch();
                                }
                                else
                                {
                                    string strMes = "";
                                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                                        strMes = msg.GetMessge_Text("M000953", strLang);
                                    else
                                        strMes = ErrRtn.ErrMessage;

                                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang), strMes,
                                                        Infragistics.Win.HAlign.Right);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                #region 기존소스
                //// SystemInfo 리소스
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //WinMessageBox msg = new WinMessageBox();
                //if (this.uGroupBoxContentsArea.Expanded == false)
                //{
                //    DialogResult DResult = new DialogResult();
                //    DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //        , "확인창", "삭제시 입력사항 확인", "삭제시 필수 입력사항을 입력해주세요", Infragistics.Win.HAlign.Right);

                //    //this.uGroupBoxContentsArea.Expanded = true;
                //}
                //else
                //{
                //    // BL 호출
                //    QRPBrowser brwChannel = new QRPBrowser();
                //    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                //    QRPMAS.BL.MASQUA.InspectGroup iGroup = new QRPMAS.BL.MASQUA.InspectGroup();
                //    brwChannel.mfCredentials(iGroup);

                //    // DataTaable 컬럼설정
                //    DataTable dt = iGroup.mfSetDataInfo();

                //    #region BL/DataCreate
                //    // 필수입력사항 확인
                //    if (this.uComboPlant.Value.ToString() == "")
                //    {
                //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                        , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Center);

                //        // Focus
                //        this.uComboPlant.DropDown();
                //        return;
                //    }
                //    else if (this.uTextInspectGroupCode.Text == "")
                //    {
                //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //                        , "확인창", "필수입력사항 확인", "검사분류코드 입력해주세요", Infragistics.Win.HAlign.Center);

                //        // Focus
                //        this.uTextInspectGroupCode.Focus();
                //        return;
                //    }
                //    else
                //    {
                //        DataRow row = dt.NewRow();
                //        row["PlantCode"] = this.uComboPlant.Value.ToString();
                //        row["InspectGroupCode"] = this.uTextInspectGroupCode.Text;
                //        dt.Rows.Add(row);
                //    }
                //    #endregion

                //    if (dt.Rows.Count > 0)
                //    {
                //        if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500,
                //                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                //                                Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                //        {

                //            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //            Thread t1 = m_ProgressPopup.mfStartThread();
                //            m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                //            this.MdiParent.Cursor = Cursors.WaitCursor;

                //            #region BL
                //            // 처리 로직 //
                //            //// 삭제 함수호출
                //            ////string rtMSG = IType.mfDeleteMASInspectType(dtType);

                //            ////// Decoding //
                //            ////TransErrRtn ErrRtn = new TransErrRtn();
                //            ////ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                //            ////// 처리로직 끝 //

                //            ////// 삭제성공여부
                //            ////if (ErrRtn.ErrNum != 0)
                //            ////{
                //            ////    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //            ////                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //            ////                        "처리결과", "삭제처리결과", "입력한 정보를 삭제하지 못했습니다.",
                //            ////                        Infragistics.Win.HAlign.Right);
                //            ////    return;
                //            ////}

                //            // InspectGroup 삭제 함수호출
                //            string rtMSG = iGroup.mfDeleteMASInspectGroup(dt);

                //            // Decoding //
                //            TransErrRtn ErrRtn = new TransErrRtn();
                //            ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                //            // 처리로직 끝 //
                //            #endregion

                //            this.MdiParent.Cursor = Cursors.Default;
                //            m_ProgressPopup.mfCloseProgressPopup(this);

                //            DialogResult DResult = new DialogResult();
                //            // 삭제성공여부
                //            if (ErrRtn.ErrNum == 0)
                //            {
                //                DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                    "처리결과", "삭제처리결과", "입력한 정보를 성공적으로 삭제했습니다.",
                //                                    Infragistics.Win.HAlign.Right);

                //                // 리스트 갱신
                //                mfSearch();
                //            }
                //            else
                //            {
                //                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                //                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                    "처리결과", "삭제처리결과", ErrRtn.ErrMessage,
                //                                    Infragistics.Win.HAlign.Right);
                //            }
                //        }
                //    }
                //}
                #endregion
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridInspectGroup.Rows.Count > 0)
                {
                    this.uGridInspectGroup.ActiveCell = this.uGridInspectGroup.Rows[0].Cells[0];

                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectGroup), "InspectGroup");
                    QRPMAS.BL.MASQUA.InspectGroup clsGroup = new QRPMAS.BL.MASQUA.InspectGroup();
                    brwChannel.mfCredentials(clsGroup);

                    DataTable dtGroup = clsGroup.mfSetDataInfo();
                    DataRow drRow;

                    for (int i = 0; i < this.uGridInspectGroup.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridInspectGroup.Rows[i].Cells["Check"].Value).Equals(true))
                        {
                            drRow = dtGroup.NewRow();
                            drRow["PlantCode"] = this.uGridInspectGroup.Rows[i].Cells["PlantCode"].Value.ToString();
                            drRow["InspectGroupCode"] = this.uGridInspectGroup.Rows[i].Cells["InspectGroupCode"].Value.ToString();
                            dtGroup.Rows.Add(drRow);
                        }
                    }

                    string strLang = m_resSys.GetString("SYS_LANG");
                    if (dtGroup.Rows.Count > 0)
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000650", "M000675",
                                                Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread t1 = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637",strLang));
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string strErrRtn = clsGroup.mfDeleteMASInspectGroup(dtGroup);


                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                            if (ErrRtn.ErrNum.Equals(0))
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000638", "M000926",
                                                    Infragistics.Win.HAlign.Right);

                                // 리스트 갱신
                                mfSearch();
                            }
                            else
                            {
                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000638", ErrRtn.ErrMessage,
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000636", "M000647",
                                                    Infragistics.Win.HAlign.Right);

                    // 리스트 갱신
                    mfSearch();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
                else
                {
                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {

        }

        public void mfExcel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (this.uGridInspectGroup.Rows.Count > 0)
                {
                    wGrid.mfDownLoadGridToExcel(this.uGridInspectGroup);

                    if (this.uGridInspectType.Rows.Count > 0)
                    {
                        wGrid.mfDownLoadGridToExcel(this.uGridInspectType);
                    }
                }
                else
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M000803", "M000809", "M000181",
                                                    Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트
        // 행삭제 버튼 이벤트
        private void uButtonDelRow_Click(object sender, EventArgs e)
        {
            DeleteRow();
        }

        // 셀 업데이트 이벤트
        private void uGridInspectType_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //// 셀 수정시 RowSelector 이미지 변화
                //QRPGlobal grdImg = new QRPGlobal();
                //e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 자동 행삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridInspectType, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                ////// 순번
                ////if (e.Cell.Row.Cells["Seq"].Value.ToString() == "")
                ////    e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // RowSelector 이미지 변화와 순번처리를 위한 이벤트
        private void uGridInspectType_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            // 셀 수정시 RowSelector 이미지 변화
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            ////// 순번처리
            ////e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
        }

        // 그룹박스 펼침상태 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                QRPBrowser toolButton = new QRPBrowser();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInspectGroup.Height = 55;

                    // ToolBar 활성화
                    toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInspectGroup.Height = 715;

                    for (int i = 0; i < uGridInspectGroup.Rows.Count; i++)
                    {
                        uGridInspectGroup.Rows[i].Fixed = false;
                    }

                    Clear();

                    // ToolBar 활성화
                    toolButton.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridInspectGroup_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }

                ////// 초기화 함수 호출
                Clear();
                ////mfCreate();

                this.uComboPlant.Value = e.Cell.Row.Cells["PlantCode"].Value;
                this.uTextInspectGroupCode.Text = e.Cell.Row.Cells["InspectGroupCode"].Value.ToString();
                this.uTextInspectGroupName.Text = e.Cell.Row.Cells["InspectGroupName"].Value.ToString();
                this.uTextInspectGroupNameCh.Text = e.Cell.Row.Cells["InspectGroupNameCh"].Value.ToString();
                this.uTextInspectGroupNameEn.Text = e.Cell.Row.Cells["InspectGroupNameEn"].Value.ToString();
                this.uComboUseFlag.Value = e.Cell.Row.Cells["UseFlag"].Value;

                // PK 편집불가로 설정
                this.uComboPlant.ReadOnly = true;
                this.uTextInspectGroupCode.Appearance.BackColor = Color.Gainsboro;
                this.uTextInspectGroupCode.ReadOnly = true;

                // InspectType 조회함수 호출
                SearchInspectType();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        /// <summary>
        /// 검사유형 조회 메소드
        /// </summary>
        private void SearchInspectType()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strPlantCode = this.uComboPlant.Value.ToString();
                String strInspectGroupCode = uTextInspectGroupCode.Text;

                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
                QRPMAS.BL.MASQUA.InspectType IType = new QRPMAS.BL.MASQUA.InspectType();
                brwChannel.mfCredentials(IType);

                DataTable dt = IType.mfReadMASInspectType(strPlantCode, strInspectGroupCode);
                this.uGridInspectType.DataSource = dt;
                this.uGridInspectType.DataBind();

                // 바인딩된 그리드 PK 값 편집 불가 상태로
                for (int i = 0; i < this.uGridInspectType.Rows.Count; i++)
                {
                    this.uGridInspectType.Rows[i].Cells["InspectTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridInspectType.Rows[i].Cells["InspectTypeCode"].Appearance.BackColor = Color.Gainsboro;
                }

                WinGrid grd = new WinGrid();
                grd.mfSetAutoResizeColWidth(this.uGridInspectType, 0);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                
                ////if (dt.Rows.Count == 0)
                ////    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 검사유형 저장 메소드
        /// </summary>
        ////private void SaveInspectType()
        ////{
        ////    try
        ////    {
        ////        WinMessageBox msg = new WinMessageBox();
        ////        // SystemInfo 리소스
        ////        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
        ////        // BL 호출
        ////        QRPBrowser brwChannel = new QRPBrowser();
        ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectType), "InspectType");
        ////        QRPMAS.BL.MASQUA.InspectType iType = new QRPMAS.BL.MASQUA.InspectType();
        ////        brwChannel.mfCredentials(iType);

        ////        DataTable Savedt = iType.mfSetDataInfo();
        ////        DataTable Deldt = iType.mfSetDataInfo();

        ////        for (int i = 0; i < this.uGridInspectType.Rows.Count; i++)
        ////        {
        ////            if (this.uGridInspectType.Rows[i].Hidden == false)
        ////            {
        ////                //if (this.uGridInspectType.Rows[i].RowSelectorAppearance.Image != null)
        ////                //{
        ////                    if (this.uGridInspectType.Rows[i].Cells["InspectTypeCode"].Value.ToString() == "")
        ////                    {
        ////                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////                                , "확인창", "필수입력사항 확인", this.uGridInspectType.Rows[i].RowSelectorNumber + "번째 열의 검사유형코드를 입력해주세요", Infragistics.Win.HAlign.Center);

        ////                        // Focus Cell
        ////                        this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[i].Cells["InspectTypeCode"];
        ////                        this.uGridInspectType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
        ////                        return;
        ////                    }
        ////                    else if(this.uGridInspectType.Rows[i].Cells["InspectTypeName"].Value.ToString() == "")
        ////                    {
        ////                        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////                                , "확인창", "필수입력사항 확인", this.uGridInspectType.Rows[i].RowSelectorNumber + "번째 열의 검사유형명을 입력해주세요", Infragistics.Win.HAlign.Center);

        ////                        // Focus Cell
        ////                        this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[i].Cells["InspectTypeName"];
        ////                        this.uGridInspectType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
        ////                        return;
        ////                    }
        ////                    ////else if (this.uGridInspectType.Rows[i].Cells["InspectTypeNameCh"].Value.ToString() == "")
        ////                    ////{
        ////                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////                    ////            , "확인창", "필수입력사항 확인", this.uGridInspectType.Rows[i].RowSelectorNumber + "번째 열의 검사유형명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

        ////                    ////    // Focus Cell
        ////                    ////    this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[i].Cells["InspectTypeNameCh"];
        ////                    ////    this.uGridInspectType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
        ////                    ////    return;
        ////                    ////}
        ////                    ////else if (this.uGridInspectType.Rows[i].Cells["InspectTypeNameEn"].Value.ToString() == "")
        ////                    ////{
        ////                    ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
        ////                    ////            , "확인창", "필수입력사항 확인", this.uGridInspectType.Rows[i].RowSelectorNumber + "번째 열의 검사유형명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

        ////                    ////    // Focus Cell
        ////                    ////    this.uGridInspectType.ActiveCell = this.uGridInspectType.Rows[i].Cells["InspectTypeNameEn"];
        ////                    ////    this.uGridInspectType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
        ////                    ////    return;
        ////                    ////}
        ////                    else
        ////                    {
        ////                        DataRow row = Savedt.NewRow();
        ////                        row["PlantCode"] = this.uComboPlant.Value.ToString();
        ////                        row["InspectGroupCode"] = this.uTextInspectGroupCode.Text;
        ////                        row["InspectTypeCode"] = this.uGridInspectType.Rows[i].Cells["InspectTypeCode"].Value.ToString();
        ////                        row["InspectTypeName"] = this.uGridInspectType.Rows[i].Cells["InspectTypeName"].Value.ToString();
        ////                        row["InspectTypeNameCh"] = this.uGridInspectType.Rows[i].Cells["InspectTypeNameCh"].Value.ToString();
        ////                        row["InspectTypeNameEn"] = this.uGridInspectType.Rows[i].Cells["InspectTypeNameEn"].Value.ToString();
        ////                        row["Seq"] = this.uGridInspectType.Rows[i].RowSelectorNumber;
        ////                        row["UseFlag"] = this.uGridInspectType.Rows[i].Cells["UseFlag"].Value.ToString();
        ////                        Savedt.Rows.Add(row);
        ////                    }
        ////                //}
        ////            }
        ////            else
        ////            {
        ////                DataRow row = Deldt.NewRow();
        ////                row["PlantCode"] = this.uComboPlant.Value.ToString();
        ////                row["InspectGroupCode"] = this.uTextInspectGroupCode.Text;
        ////                row["InspectTypeCode"] = this.uGridInspectType.Rows[i].Cells["InspectTypeCode"].Value.ToString();
        ////                Deldt.Rows.Add(row);
        ////            }
        ////        }

        ////        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
        ////        Thread t1 = m_ProgressPopup.mfStartThread();
        ////        m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
        ////        this.MdiParent.Cursor = Cursors.WaitCursor;

        ////        // 처리 로직 //
        ////        string SavertMSG = "";
        ////        string DelrtMSG = "";
        ////        // 저장함수 호출
        ////        if(Savedt.Rows.Count>0)
        ////            SavertMSG = iType.mfSaveMASInspectType(Savedt, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
        ////        if (Deldt.Rows.Count > 0)
        ////            DelrtMSG = iType.mfDeleteMASInspectType(Deldt);


        ////        // Decoding //
        ////        TransErrRtn SaveErrRtn = new TransErrRtn();
        ////        SaveErrRtn = SaveErrRtn.mfDecodingErrMessage(SavertMSG);
        ////        TransErrRtn DelErrRtn = new TransErrRtn();
        ////        DelErrRtn = DelErrRtn.mfDecodingErrMessage(DelrtMSG);
        ////        // 처리로직 끝//

        ////        this.MdiParent.Cursor = Cursors.Default;
        ////        m_ProgressPopup.mfCloseProgressPopup(this);

        ////        // 처리결과에 따른 메세지 박스
        ////        if ((SaveErrRtn.ErrNum == 0) && (DelErrRtn.ErrNum == 0))
        ////        {
        ////            msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////                                "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
        ////                                Infragistics.Win.HAlign.Right);
        ////        }
        ////        else
        ////        {
        ////            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        ////                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        ////                                "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
        ////                                Infragistics.Win.HAlign.Right);
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        /// <summary>
        /// 행삭제 메소드
        /// </summary>
        private void DeleteRow()
        {
            try
            {
                for (int i = 0; i < this.uGridInspectType.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridInspectType.Rows[i].Cells["Check"].Value) == true)
                        this.uGridInspectType.Rows[i].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 상세정보 컨트롤 초기화 메소드
        /// </summary>
        private void Clear()
        {
            try
            {
                this.uComboPlant.Value = "";
                this.uTextInspectGroupCode.Text = "";
                this.uTextInspectGroupName.Text = "";
                this.uTextInspectGroupNameCh.Text = "";
                this.uTextInspectGroupNameEn.Text = "";
                this.uComboUseFlag.Value = "T";

                while (this.uGridInspectType.Rows.Count > 0)
                {
                    this.uGridInspectType.Rows[0].Delete(false);
                }

                // PK 편집불가상태를 편집 가능상태로
                this.uComboPlant.ReadOnly = false;
                this.uTextInspectGroupCode.Appearance.BackColor = Color.PowderBlue;
                this.uTextInspectGroupCode.ReadOnly = false;
                this.uGridInspectType.DisplayLayout.Bands[0].Columns["InspectTypeCode"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMAS0018_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmMAS0018_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
