﻿namespace QRPMAS.UI
{
    partial class frmPOP0021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPOP0021));
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchEquipType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonSearch = new Infragistics.Win.Misc.UltraButton();
            this.uGridHeader = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonOK = new Infragistics.Win.Misc.UltraButton();
            this.uButtonClose = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uButtonSearch);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 0);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(969, 60);
            this.uGroupBoxSearchArea.TabIndex = 6;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 36);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(150, 21);
            this.uComboProcessGroup.TabIndex = 13;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 36);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 12;
            // 
            // uLabelSearchEquipType
            // 
            this.uLabelSearchEquipType.Location = new System.Drawing.Point(304, 36);
            this.uLabelSearchEquipType.Name = "uLabelSearchEquipType";
            this.uLabelSearchEquipType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipType.TabIndex = 10;
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(596, 12);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 11;
            // 
            // uComboSearchEquipType
            // 
            this.uComboSearchEquipType.Location = new System.Drawing.Point(408, 36);
            this.uComboSearchEquipType.MaxLength = 50;
            this.uComboSearchEquipType.Name = "uComboSearchEquipType";
            this.uComboSearchEquipType.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchEquipType.TabIndex = 9;
            this.uComboSearchEquipType.ValueChanged += new System.EventHandler(this.uComboSearchEquipType_ValueChanged);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(700, 12);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchEquipLoc.TabIndex = 8;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(408, 12);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchStation.TabIndex = 4;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(700, 36);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchEquipGroup.TabIndex = 5;
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(304, 12);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 6;
            // 
            // uLabelSearchEquipGroup
            // 
            this.uLabelSearchEquipGroup.Location = new System.Drawing.Point(596, 36);
            this.uLabelSearchEquipGroup.Name = "uLabelSearchEquipGroup";
            this.uLabelSearchEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipGroup.TabIndex = 7;
            // 
            // uButtonSearch
            // 
            this.uButtonSearch.Location = new System.Drawing.Point(876, 8);
            this.uButtonSearch.Name = "uButtonSearch";
            this.uButtonSearch.Size = new System.Drawing.Size(88, 28);
            this.uButtonSearch.TabIndex = 3;
            this.uButtonSearch.Text = "검색";
            this.uButtonSearch.Click += new System.EventHandler(this.uButtonSearch_Click);
            // 
            // uGridHeader
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridHeader.DisplayLayout.Appearance = appearance5;
            this.uGridHeader.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridHeader.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridHeader.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridHeader.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridHeader.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridHeader.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridHeader.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridHeader.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridHeader.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridHeader.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridHeader.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridHeader.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridHeader.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridHeader.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridHeader.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridHeader.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridHeader.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridHeader.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridHeader.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridHeader.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridHeader.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridHeader.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridHeader.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridHeader.Location = new System.Drawing.Point(0, 60);
            this.uGridHeader.Name = "uGridHeader";
            this.uGridHeader.Size = new System.Drawing.Size(390, 372);
            this.uGridHeader.TabIndex = 7;
            this.uGridHeader.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.uGridHeader_ClickCell);
            // 
            // uGridDetail
            // 
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDetail.DisplayLayout.Appearance = appearance14;
            this.uGridDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.uGridDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDetail.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDetail.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDetail.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDetail.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDetail.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.uGridDetail.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridDetail.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.uGridDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDetail.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDetail.Location = new System.Drawing.Point(396, 60);
            this.uGridDetail.Name = "uGridDetail";
            this.uGridDetail.Size = new System.Drawing.Size(572, 372);
            this.uGridDetail.TabIndex = 8;
            // 
            // uButtonOK
            // 
            this.uButtonOK.Location = new System.Drawing.Point(768, 444);
            this.uButtonOK.Name = "uButtonOK";
            this.uButtonOK.Size = new System.Drawing.Size(88, 28);
            this.uButtonOK.TabIndex = 9;
            this.uButtonOK.Click += new System.EventHandler(this.uButtonOK_Click);
            // 
            // uButtonClose
            // 
            this.uButtonClose.Location = new System.Drawing.Point(868, 444);
            this.uButtonClose.Name = "uButtonClose";
            this.uButtonClose.Size = new System.Drawing.Size(88, 28);
            this.uButtonClose.TabIndex = 10;
            this.uButtonClose.Click += new System.EventHandler(this.uButtonClose_Click);
            // 
            // frmPOP0021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(969, 486);
            this.Controls.Add(this.uButtonClose);
            this.Controls.Add(this.uButtonOK);
            this.Controls.Add(this.uGridDetail);
            this.Controls.Add(this.uGridHeader);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPOP0021";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "설비점검 정보";
            this.Load += new System.EventHandler(this.frmPOP0021_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraButton uButtonSearch;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridHeader;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDetail;
        private Infragistics.Win.Misc.UltraButton uButtonOK;
        private Infragistics.Win.Misc.UltraButton uButtonClose;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
    }
}