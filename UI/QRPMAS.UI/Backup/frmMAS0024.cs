﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMAS0024.cs                                         */
/* 프로그램명   : 설비유형정보                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-01                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0024 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmMAS0024()
        {
            InitializeComponent();
        }

        private void frmMAS0024_Activated(object sender, EventArgs e)
        {
            //툴바활성
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //사용여부설정
            ToolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0024_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            titleArea.mfSetLabelText("설비유형정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            //각컨트롤 초기화
            SetToolAuth();
            InitGrid();
            InitLabel();
            InitComBox();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }


        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGridEquipType, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridEquipType, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipType, 0, "EquipTypeCode", "설비유형코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipType, 0, "EquipTypeName", "설비유형명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipType, 0, "EquipTypeNameCh", "설비유형명_중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipType, 0, "EquipTypeNameEn", "설비유형명_영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipType, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridEquipType.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipType.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                ////사용여부
                //DataTable dtUseFlag = new DataTable();
                //QRPBrowser brwChnnel = new QRPBrowser();
                //brwChnnel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "Commin");
                //QRPSYS.BL.SYSPGM.CommonCode CommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                //brwChnnel.mfCredentials(CommonCode);

                //dtUseFlag = CommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));


                ////그리드에넣기
                //grd.mfSetGridColumnValueList(this.uGridEquipType, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                ////한줄생성
                //grd.mfAddRowGrid(this.uGridEquipType, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChnnel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                WinComboEditor com = new WinComboEditor();
                com.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), ""
                    , "전체", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 툴바
        public void mfSearch()
        {
            try
            {
                //공장콤보Key값
                string strPlantCode = uComboPlant.Value.ToString();

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //ProgressBar open
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPopup = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서 Change
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //--------------------------------------------------------------------처리로직----------------------------------------------------------//
                //BL호출
                QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipType), "EquipType");
                QRPMAS.BL.MASEQU.EquipType clsEquipType = new QRPMAS.BL.MASEQU.EquipType();
                brwChnnel.mfCredentials(clsEquipType);

                //조회매서드 호출
                DataTable dtEquipType = clsEquipType.mfReadEquipType(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //데이터 바인드
                this.uGridEquipType.DataSource = dtEquipType;
                this.uGridEquipType.DataBind();
                //--------------------------------------------------------------------------------------------------------------------------------------//
               
                //커서 기본값으로
                this.MdiParent.Cursor = Cursors.Default;

                //Progressbar 닫기
                m_ProgressPopup.mfCloseProgressPopup(this);

                //데이터가 없을시
                if (dtEquipType.Rows.Count == 0)
                {
                    WinMessageBox msg = new WinMessageBox();
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipType, 0);
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
        }

        public void mfDelete()
        {
        }

        public void mfCreate()
        {

        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipType.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000708", "M000707", Infragistics.Win.HAlign.Right);
                    return;
                }
                //처리 로직//
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                grd.mfDownLoadGridToExcel(this.uGridEquipType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }

        }

        public void mfPrint()
        {
        }
        #endregion

        private void frmMAS0024_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
    }
}
