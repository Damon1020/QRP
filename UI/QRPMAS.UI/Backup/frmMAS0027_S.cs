﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMAS0027_S.cs                                         */
/* 프로그램명   : SparePart정보                                         */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPMAS.UI
{
    public partial class frmMAS0027_S : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0027_S()
        {
            InitializeComponent();
        }
         private void frmMAS0027_Activated(object sender, EventArgs e)
        {
            //툴바활성
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //사용여부설정
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0027_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            titleArea.mfSetLabelText("SparePart정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤 초기화
            SetToolAuth();
            InitGrid();
            InitLabel();
            InitComboBox();
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        //레이블초기화
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //그리드초기화
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                grd.mfInitGeneralGrid(this.uGridSPList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Show, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridSPList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSPList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                grd.mfSetGridColumn(this.uGridSPList, 0, "SparePartCode", "SparePart코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPList, 0, "SparePartName", "SparePart명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 170, true, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPList, 0, "SparePartNameCh", "SparePart명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 170, false, true, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPList, 0, "SparePartNameEn", "SparePart명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 170, false, true, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPList, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPList, 0, "Maker", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSPList, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd.mfSetGridColumn(this.uGridSPList, 0, "SafeQty", "안전재고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGridSPList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                // 입력을 위한 공백줄 추가
                grd.mfAddRowGrid(this.uGridSPList, 0);

                ////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "SparePart");
                ////QRPMAS.BL.MASEQU.SparePart clsSP = new QRPMAS.BL.MASEQU.SparePart();
                ////brwChannel.mfCredentials(clsSP);
                ////DataSet dsInit = clsSP.mfSetDatainfo_PSTS();

                DataSet dsInit = mfSetDatainfo_PSTS();

                this.uGridSPList.SetDataBinding(dsInit, "SparePart");

                #region DropDown

                // DropDown 설정
                // Plant
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                mfSetGridColumnValueList(this.uGridSPList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                // UseFlag
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                mfSetGridColumnValueList(this.uGridSPList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUseFlag);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadMASUnitCombo();
                mfSetGridColumnValueList(this.uGridSPList, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUnit);

                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                string strLang = m_resSys.GetString("SYS_LANG");
                string strAll = "";
                if (strLang.Equals("KOR"))
                    strAll = "전체";
                else if (strLang.Equals("CHN"))
                    strAll = "全部";
                else if (strLang.Equals("ENG"))
                    strAll = "All";

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", strAll, "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바기능

        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "SparePart");
                QRPMAS.BL.MASEQU.SparePart spResult = new QRPMAS.BL.MASEQU.SparePart();
                brwChannel.mfCredentials(spResult);

                String strPlantCode = this.uComboPlant.Value.ToString();
                string strDSSP = spResult.mfReadSparePart_PSTS(strPlantCode, m_resSys.GetString("SYS_LANG"));

                DialogResult DResult = new DialogResult();
                

                if (strDSSP.Equals(string.Empty))
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    DataSet dsSP = DecompressDataSet(strDSSP);

                    this.uGridSPList.SetDataBinding(dsSP, string.Empty);

                    for (int i = 0; i < uGridSPList.Rows.Count; i++)
                    {
                        this.uGridSPList.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridSPList.Rows[i].Cells["SparePartCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridSPList.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                        this.uGridSPList.Rows[i].Cells["SparePartCode"].Appearance.BackColor = Color.Gainsboro;
                    }

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridSPList, 0);
                }                

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult DResult = new DialogResult();

                #region 필수입력사항 확인

                string strLang = m_resSys.GetString("SYS_LANG");

                this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                for (int i = 0; i < this.uGridSPList.Rows.Count; i++)
                {
                    //그리드가 수정되었을때 저장
                    if (this.uGridSPList.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        int intIndex = this.uGridSPList.Rows[i].RowSelectorNumber;
                        //필수 입력사항 확인
                        if (this.uGridSPList.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error,m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , intIndex + msg.GetMessge_Text("M000481",strLang), Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridSPList.ActiveCell = this.uGridSPList.Rows[i].Cells["PlantCode"];
                            this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridSPList.Rows[i].Cells["SparePartCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , intIndex + "M000476", Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridSPList.ActiveCell = this.uGridSPList.Rows[i].Cells["SparePartCode"];
                            this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGridSPList.Rows[i].Cells["SparePartName"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , intIndex + msg.GetMessge_Text("M000474",strLang), Infragistics.Win.HAlign.Center);

                            this.uGridSPList.ActiveCell = this.uGridSPList.Rows[i].Cells["SparePartName"];
                            this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGridSPList.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , intIndex + msg.GetMessge_Text("M000473",strLang), Infragistics.Win.HAlign.Center);

                            this.uGridSPList.ActiveCell = this.uGridSPList.Rows[i].Cells["UnitCode"];
                            this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridSPList.Rows[i].Cells["SafeQty"].Value == null)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000879",strLang)
                                , intIndex + msg.GetMessge_Text("M000519",strLang), Infragistics.Win.HAlign.Center);

                            this.uGridSPList.ActiveCell = this.uGridSPList.Rows[i].Cells["SafeQty"];
                            this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                    }
                }

                #endregion

                // 데이터 입력사항 반영
                this.uGridSPList.UpdateData();

                // 변경사항이 있는지 체크
                if (!((DataSet)this.uGridSPList.DataSource).HasChanges())
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001009", "M001018"
                                            , Infragistics.Win.HAlign.Right);

                    return;
                }

                // 추가되거나 수정된행만 DataSet으로 저장
                DataSet dsSparePart = ((DataSet)this.uGridSPList.DataSource).GetChanges(DataRowState.Added | DataRowState.Modified);

                if (dsSparePart.Tables["SparePart"].Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        // BL 호출
                        QRPMAS.BL.MASEQU.SparePart sp;
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "SparePart");
                        sp = new QRPMAS.BL.MASEQU.SparePart();
                        brwChannel.mfCredentials(sp);

                        QRPProgressBar uProgressPopup = new QRPProgressBar();
                        Thread uTh = uProgressPopup.mfStartThread();
                        uProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //저장함수 호출
                        string strErrRtn = sp.mfSaveSparePart_PSTS(CompressDataSet(dsSparePart), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        //Decoding
                        TransErrRtn ErrEtn = new TransErrRtn();
                        ErrEtn = ErrEtn.mfDecodingErrMessage(strErrRtn);

                        //POPUP Close
                        this.MdiParent.Cursor = Cursors.Default;
                        uProgressPopup.mfCloseProgressPopup(this);

                        //처리결과에 따른 메세지 박스
                        if (ErrEtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000930"
                            , Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (ErrEtn.ErrMessage.Equals(string.Empty))
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000953"
                                , Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang), ErrEtn.ErrMessage
                                , Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {
                //SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                DialogResult DResult = new DialogResult();

                this.uGridSPList.UpdateData();
                this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // 변경사항이 있는지 체크
                if (!((DataSet)this.uGridSPList.DataSource).HasChanges())
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000622", "M000639"
                                            , Infragistics.Win.HAlign.Right);

                    return;
                }

                // 삭제정보는 기존 DB에 저장되어 있는 정보만... => 수정된 행만 DataSet으로 저장
                DataSet dsSparePart = ((DataSet)this.uGridSPList.DataSource).GetChanges(DataRowState.Modified);
                // 추가되거나 수정된행만 DataSet으로 저장
                //DataSet dsSparePart = ((DataSet)this.uGridSPList.DataSource).GetChanges(DataRowState.Added | DataRowState.Modified);

                // Check 되지 않거나 필수입력사항 없는 행 삭제
                foreach (DataRow _dr in dsSparePart.Tables["SparePart"].Rows)
                {
                    if (Convert.ToBoolean(_dr["Check"]).Equals(false) || _dr["PlantCode"].ToString().Equals(string.Empty) || _dr["SparePartCode"].ToString().Equals(string.Empty))
                        _dr.Delete();
                }

                // 삭제된것 반영
                dsSparePart.AcceptChanges();

                if (dsSparePart.Tables["SparePart"].Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        //BL호출
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "SparePart");
                        QRPMAS.BL.MASEQU.SparePart sp = new QRPMAS.BL.MASEQU.SparePart();
                        brwChannel.mfCredentials(sp);

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", m_resSys.GetString("SYS_LANG")));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //함수호출
                        string strSG = sp.mfDeleteMASSparePart_PSTS(CompressDataSet(dsSparePart));

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strSG);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        //삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001135", "M000638", "M000677",
                                 Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (strSG.Equals(string.Empty))
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001135", "M000638", "M000923",
                                    Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000638", strLang), ErrRtn.ErrMessage,
                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                else
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000622", "M000639"
                                            , Infragistics.Win.HAlign.Right);

                    return;
                }

                #region 기존소스 주석처리
                /*
                //함수호출 매개변수 DataTable
                DataTable dtSp = sp.mfSetDatainfo();

                //chek된것 삭제
                for (int i = 0; i < uGridSPList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridSPList.Rows[i].Cells["Check"].Value) == true)
                    {
                        //필수입력사항 확인
                        if (this.uGridSPList.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", (i + 1) + "M000481", Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridSPList.ActiveCell = this.uGridSPList.Rows[i].Cells["PlantCode"];
                            this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridSPList.Rows[i].Cells["SparePartCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", (i + 1) + "M000476", Infragistics.Win.HAlign.Center);

                            //FocusCell
                            this.uGridSPList.ActiveCell = this.uGridSPList.Rows[i].Cells["SparePartCode"];
                            this.uGridSPList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            DataRow drSp = dtSp.NewRow();
                            drSp["PlantCode"] = this.uGridSPList.Rows[i].Cells["PlantCode"].Value.ToString();
                            drSp["SparePartCode"] = this.uGridSPList.Rows[i].Cells["SparePartCode"].Value.ToString();
                            dtSp.Rows.Add(drSp);
                        }
                    }
                }

                if (dtSp.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //함수호출
                        DataSet dsSparePart = new DataSet();
                        dtSp.TableName = "SparePart";
                        dsSparePart.Tables.Add(dtSp);
                        string strSG = sp.mfDeleteMASSparePart_PSTS(CompressDataSet(dsSparePart));

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strSG);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        //삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001135", "M000638", "M000677",
                                 Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            if (strSG.Equals(string.Empty))
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001135", "M000638", "M000923",
                                    Infragistics.Win.HAlign.Right);
                            }
                            else
                            {
                                string strLang = m_resSys.GetString("SYS_LANG");
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M000638", strLang), ErrRtn.ErrMessage,
                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                    }
                }
                 * */
                #endregion
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {

        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridSPList.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000811", "M000806", Infragistics.Win.HAlign.Right);

                    return;
                }
                //처리 로직//
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                grd.mfDownLoadGridToExcel(this.uGridSPList);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        public void mfPrint()
        {
        }

        #endregion        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문

        #region Events

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridSPList, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        #endregion

        /// <summary>
        /// 그리드 컬럼에 콤보박스로 설정하는 함수
        /// </summary>
        /// <param name="Grid">그리드 Object</param>
        /// <param name="intBandIndex"></param>
        /// <param name="strColKey"></param>
        /// <param name="ValueListStyle"></param>
        /// <param name="strTopKey"></param>
        /// <param name="strTopValue"></param>
        /// <param name="dtValueList"></param>
        public void mfSetGridColumnValueList(Infragistics.Win.UltraWinGrid.UltraGrid Grid,
                                             int intBandIndex,
                                             string strColKey,
                                             Infragistics.Win.ValueListDisplayStyle ValueListStyle,
                                             string strTopKey,
                                             string strTopValue,
                                             System.Data.DataTable dtValueList)
        {
            try
            {
                Infragistics.Win.ValueList uValueList = new Infragistics.Win.ValueList();

                ////ValueList의 Style 지정
                uValueList.DisplayStyle = ValueListStyle;

                // DropDown 리스트 각Item의 높이 설정
                uValueList.ItemHeight = 15;
                // Dropdown 리스트에 한번에 보여지는 최대 Item 갯수;
                uValueList.MaxDropDownItems = 5;

                //Value List에 상단값 설정
                if (strTopKey != "" || strTopValue != "")
                {
                    // TopValue 설정
                    QRPCOM.QRPGLO.QRPGlobal SysRes = new QRPCOM.QRPGLO.QRPGlobal();
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    string strChgTopVal = string.Empty;
                    switch (strTopValue)
                    {
                        case "전체":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "全部";
                                    break;
                                case "ENG":
                                    strChgTopVal = "ALL";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        case "선택":
                            switch (m_resSys.GetString("SYS_LANG"))
                            {
                                case "CHN":
                                    strChgTopVal = "选择";
                                    break;
                                case "ENG":
                                    strChgTopVal = "Select";
                                    break;
                                default:
                                    strChgTopVal = strTopValue;
                                    break;
                            }
                            break;
                        default:
                            strChgTopVal = strTopValue;
                            break;
                    }
                    uValueList.ValueListItems.Add(strTopKey, strChgTopVal);
                }

                //추가적인 Value List 값 설정
                for (int i = 0; i < dtValueList.Rows.Count; i++)
                {
                    uValueList.ValueListItems.Add(dtValueList.Rows[i][0].ToString(), dtValueList.Rows[i][1].ToString());
                }
                uValueList.DropDownResizeHandleStyle = Infragistics.Win.DropDownResizeHandleStyle.Default;

                //그리드 콤보에서 Like검색 속성지정
                Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AutoSuggestFilterMode = Infragistics.Win.AutoSuggestFilterMode.Contains;
                Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].AutoCompleteMode = Infragistics.Win.AutoCompleteMode.SuggestAppend;

                //그리드 컬럼에 ValueList 지정 
                Grid.DisplayLayout.Bands[intBandIndex].Columns[strColKey].ValueList = uValueList;
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private string CompressDataSet(DataSet dataSet)
        {
            if (dataSet == null)
                throw new ArgumentNullException("dataSet");

            dataSet.RemotingFormat = SerializationFormat.Binary;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            byte[] result = null;

            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, dataSet);

                using (System.IO.MemoryStream resultStream = new System.IO.MemoryStream())
                {
                    using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(resultStream, System.IO.Compression.CompressionMode.Compress))
                    {
                        result = memoryStream.ToArray();
                        deflateStream.Write(result, 0, result.Length);
                    }

                    result = resultStream.ToArray();
                }
            }

            return Convert.ToBase64String(result, 0, result.Length);
        }

        private DataSet DecompressDataSet(string base64String)
        {
            if (String.IsNullOrEmpty(base64String))
                throw new ArgumentNullException("base64String");

            byte[] deflatedData = Convert.FromBase64String(base64String);

            using (System.IO.MemoryStream deflatedStream = new System.IO.MemoryStream(deflatedData, false))
            {
                using (System.IO.Compression.DeflateStream deflateStream = new System.IO.Compression.DeflateStream(deflatedStream, System.IO.Compression.CompressionMode.Decompress))
                {
                    using (System.IO.MemoryStream uncompressedStream = new System.IO.MemoryStream())
                    {
                        int read = 0;
                        byte[] readBuffer = new byte[64000];

                        while ((read = deflateStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
                            uncompressedStream.Write(readBuffer, 0, read);

                        uncompressedStream.Seek(0L, System.IO.SeekOrigin.Begin);
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        return binaryFormatter.Deserialize(uncompressedStream) as DataSet;
                    }
                }
            }
        }

        ///<summary>
        ///컬럼 설정
        ///</summary>
        ///<return></return>
        public DataSet mfSetDatainfo_PSTS()
        {
            DataSet dsSparePart = new DataSet();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                DataTable dtSPSparePart = new DataTable();
                dtSPSparePart.Columns.Add("Check", typeof(Boolean));
                dtSPSparePart.Columns.Add("PlantCode", typeof(String));
                dtSPSparePart.Columns.Add("SparePartCode", typeof(String));
                dtSPSparePart.Columns.Add("SparePartName", typeof(String));
                dtSPSparePart.Columns.Add("SparePartNameCh", typeof(String));
                dtSPSparePart.Columns.Add("SparePartNameEn", typeof(String));
                dtSPSparePart.Columns.Add("Spec", typeof(String));
                dtSPSparePart.Columns.Add("Maker", typeof(String));
                dtSPSparePart.Columns.Add("UnitCode", typeof(String));
                dtSPSparePart.Columns.Add("SafeQty", typeof(Int32));
                dtSPSparePart.Columns.Add("UseFlag", typeof(String));

                dtSPSparePart.TableName = "SparePart";
                dsSparePart.Tables.Add(dtSPSparePart);
                return dsSparePart;
            }
            catch (Exception ex)
            {
                return dsSparePart;
                throw (ex);
            }
            finally
            {
                dsSparePart.Dispose();
            }
        }
    }
}

        