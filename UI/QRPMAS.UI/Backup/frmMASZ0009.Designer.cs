﻿namespace QRPMAS.UI
{
    partial class frmMASZ0009
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0009));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridDeviceChgPMD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridDeviceChgPMH = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.utextAdmitDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRevisionReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevisionReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEtcDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAdmitID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage3 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTextRejectReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRejectReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelWriteDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelWriteID = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelVersionNum = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStdNumber = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgPMD)).BeginInit();
            this.ultraTabPageControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgPMH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utextAdmitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl1)).BeginInit();
            this.uTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.uGridDeviceChgPMD);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(1040, 502);
            // 
            // uGridDeviceChgPMD
            // 
            this.uGridDeviceChgPMD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDeviceChgPMD.DisplayLayout.Appearance = appearance24;
            this.uGridDeviceChgPMD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDeviceChgPMD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMD.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgPMD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.uGridDeviceChgPMD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgPMD.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.uGridDeviceChgPMD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDeviceChgPMD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDeviceChgPMD.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDeviceChgPMD.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.uGridDeviceChgPMD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDeviceChgPMD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMD.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDeviceChgPMD.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGridDeviceChgPMD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDeviceChgPMD.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMD.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.uGridDeviceChgPMD.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.uGridDeviceChgPMD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDeviceChgPMD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGridDeviceChgPMD.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGridDeviceChgPMD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDeviceChgPMD.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridDeviceChgPMD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDeviceChgPMD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDeviceChgPMD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDeviceChgPMD.Location = new System.Drawing.Point(12, 8);
            this.uGridDeviceChgPMD.Name = "uGridDeviceChgPMD";
            this.uGridDeviceChgPMD.Size = new System.Drawing.Size(1024, 444);
            this.uGridDeviceChgPMD.TabIndex = 1;
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.uGrid2);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(1040, 502);
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance12;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance17.BackColor = System.Drawing.SystemColors.Highlight;
            appearance17.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance17;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            appearance19.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance19;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance20.BackColor = System.Drawing.SystemColors.Control;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance20;
            appearance21.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance21;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance22;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance23;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 12);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1040, 504);
            this.uGrid2.TabIndex = 3;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 9;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 10;
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 4;
            this.uComboPlant.Text = "uCombo";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 2;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // uGridDeviceChgPMH
            // 
            this.uGridDeviceChgPMH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDeviceChgPMH.DisplayLayout.Appearance = appearance36;
            this.uGridDeviceChgPMH.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDeviceChgPMH.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMH.DisplayLayout.GroupByBox.Appearance = appearance37;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgPMH.DisplayLayout.GroupByBox.BandLabelAppearance = appearance38;
            this.uGridDeviceChgPMH.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance39.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance39.BackColor2 = System.Drawing.SystemColors.Control;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgPMH.DisplayLayout.GroupByBox.PromptAppearance = appearance39;
            this.uGridDeviceChgPMH.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDeviceChgPMH.DisplayLayout.MaxRowScrollRegions = 1;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDeviceChgPMH.DisplayLayout.Override.ActiveCellAppearance = appearance40;
            appearance41.BackColor = System.Drawing.SystemColors.Highlight;
            appearance41.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDeviceChgPMH.DisplayLayout.Override.ActiveRowAppearance = appearance41;
            this.uGridDeviceChgPMH.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDeviceChgPMH.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMH.DisplayLayout.Override.CardAreaAppearance = appearance42;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            appearance43.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDeviceChgPMH.DisplayLayout.Override.CellAppearance = appearance43;
            this.uGridDeviceChgPMH.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDeviceChgPMH.DisplayLayout.Override.CellPadding = 0;
            appearance44.BackColor = System.Drawing.SystemColors.Control;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMH.DisplayLayout.Override.GroupByRowAppearance = appearance44;
            appearance45.TextHAlignAsString = "Left";
            this.uGridDeviceChgPMH.DisplayLayout.Override.HeaderAppearance = appearance45;
            this.uGridDeviceChgPMH.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDeviceChgPMH.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            this.uGridDeviceChgPMH.DisplayLayout.Override.RowAppearance = appearance46;
            this.uGridDeviceChgPMH.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance47.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDeviceChgPMH.DisplayLayout.Override.TemplateAddRowAppearance = appearance47;
            this.uGridDeviceChgPMH.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDeviceChgPMH.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDeviceChgPMH.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDeviceChgPMH.Location = new System.Drawing.Point(0, 80);
            this.uGridDeviceChgPMH.Name = "uGridDeviceChgPMH";
            this.uGridDeviceChgPMH.Size = new System.Drawing.Size(1070, 760);
            this.uGridDeviceChgPMH.TabIndex = 11;
            this.uGridDeviceChgPMH.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridDeviceChgPMH_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 12;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.utextAdmitDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevisionReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevisionReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtcDesc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAdmitID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStdNumber);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabControl1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRejectReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRejectReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel8);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAdmitName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel7);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextWriteName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelWriteID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVersionNum);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelVersionNum);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStdNumber);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // utextAdmitDate
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.utextAdmitDate.Appearance = appearance6;
            this.utextAdmitDate.BackColor = System.Drawing.Color.Gainsboro;
            this.utextAdmitDate.Location = new System.Drawing.Point(144, 60);
            this.utextAdmitDate.Name = "utextAdmitDate";
            this.utextAdmitDate.ReadOnly = true;
            this.utextAdmitDate.Size = new System.Drawing.Size(100, 21);
            this.utextAdmitDate.TabIndex = 63;
            // 
            // uTextRevisionReason
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionReason.Appearance = appearance3;
            this.uTextRevisionReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionReason.Location = new System.Drawing.Point(144, 132);
            this.uTextRevisionReason.Name = "uTextRevisionReason";
            this.uTextRevisionReason.ReadOnly = true;
            this.uTextRevisionReason.Size = new System.Drawing.Size(788, 21);
            this.uTextRevisionReason.TabIndex = 62;
            // 
            // uLabelRevisionReason
            // 
            this.uLabelRevisionReason.Location = new System.Drawing.Point(12, 132);
            this.uLabelRevisionReason.Name = "uLabelRevisionReason";
            this.uLabelRevisionReason.Size = new System.Drawing.Size(125, 20);
            this.uLabelRevisionReason.TabIndex = 61;
            this.uLabelRevisionReason.Text = "ultraLabel1";
            // 
            // uTextEtcDesc
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtcDesc.Appearance = appearance2;
            this.uTextEtcDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtcDesc.Location = new System.Drawing.Point(144, 108);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.ReadOnly = true;
            this.uTextEtcDesc.Size = new System.Drawing.Size(788, 21);
            this.uTextEtcDesc.TabIndex = 60;
            // 
            // uLabelEtcDesc
            // 
            this.uLabelEtcDesc.Location = new System.Drawing.Point(12, 108);
            this.uLabelEtcDesc.Name = "uLabelEtcDesc";
            this.uLabelEtcDesc.Size = new System.Drawing.Size(125, 20);
            this.uLabelEtcDesc.TabIndex = 59;
            this.uLabelEtcDesc.Text = "ultraLabel1";
            // 
            // uTextWriteDate
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteDate.Appearance = appearance5;
            this.uTextWriteDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteDate.Location = new System.Drawing.Point(144, 35);
            this.uTextWriteDate.Name = "uTextWriteDate";
            this.uTextWriteDate.ReadOnly = true;
            this.uTextWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteDate.TabIndex = 58;
            // 
            // uTextAdmitID
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitID.Appearance = appearance10;
            this.uTextAdmitID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitID.Location = new System.Drawing.Point(476, 60);
            this.uTextAdmitID.Name = "uTextAdmitID";
            this.uTextAdmitID.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitID.TabIndex = 57;
            // 
            // uTextWriteID
            // 
            appearance49.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteID.Appearance = appearance49;
            this.uTextWriteID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteID.Location = new System.Drawing.Point(476, 36);
            this.uTextWriteID.Name = "uTextWriteID";
            this.uTextWriteID.ReadOnly = true;
            this.uTextWriteID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteID.TabIndex = 52;
            // 
            // uTextStdNumber
            // 
            appearance98.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Appearance = appearance98;
            this.uTextStdNumber.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStdNumber.Location = new System.Drawing.Point(144, 12);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.ReadOnly = true;
            this.uTextStdNumber.Size = new System.Drawing.Size(100, 21);
            this.uTextStdNumber.TabIndex = 50;
            // 
            // uTabControl1
            // 
            this.uTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabControl1.Controls.Add(this.ultraTabSharedControlsPage3);
            this.uTabControl1.Controls.Add(this.ultraTabPageControl5);
            this.uTabControl1.Controls.Add(this.ultraTabPageControl6);
            this.uTabControl1.Location = new System.Drawing.Point(12, 160);
            this.uTabControl1.Name = "uTabControl1";
            this.uTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage3;
            this.uTabControl1.Size = new System.Drawing.Size(1044, 528);
            this.uTabControl1.TabIndex = 48;
            ultraTab1.TabPage = this.ultraTabPageControl5;
            ultraTab1.Text = "점검항목상세";
            ultraTab2.TabPage = this.ultraTabPageControl6;
            ultraTab2.Text = "점검그룹설비리스트";
            ultraTab2.Visible = false;
            this.uTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage3
            // 
            this.ultraTabSharedControlsPage3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage3.Name = "ultraTabSharedControlsPage3";
            this.ultraTabSharedControlsPage3.Size = new System.Drawing.Size(1040, 502);
            // 
            // uTextRejectReason
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRejectReason.Appearance = appearance8;
            this.uTextRejectReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRejectReason.Location = new System.Drawing.Point(144, 84);
            this.uTextRejectReason.Name = "uTextRejectReason";
            this.uTextRejectReason.Size = new System.Drawing.Size(788, 21);
            this.uTextRejectReason.TabIndex = 47;
            // 
            // uLabelRejectReason
            // 
            this.uLabelRejectReason.Location = new System.Drawing.Point(12, 84);
            this.uLabelRejectReason.Name = "uLabelRejectReason";
            this.uLabelRejectReason.Size = new System.Drawing.Size(125, 20);
            this.uLabelRejectReason.TabIndex = 46;
            this.uLabelRejectReason.Text = "ultraLabel7";
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(12, 60);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(125, 20);
            this.uLabel8.TabIndex = 44;
            this.uLabel8.Text = "ultraLabel6";
            // 
            // uTextAdmitName
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitName.Appearance = appearance4;
            this.uTextAdmitName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitName.Location = new System.Drawing.Point(580, 60);
            this.uTextAdmitName.Name = "uTextAdmitName";
            this.uTextAdmitName.ReadOnly = true;
            this.uTextAdmitName.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitName.TabIndex = 43;
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(344, 60);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(125, 20);
            this.uLabel7.TabIndex = 41;
            this.uLabel7.Text = "ultraLabel2";
            // 
            // uLabelWriteDate
            // 
            this.uLabelWriteDate.Location = new System.Drawing.Point(12, 36);
            this.uLabelWriteDate.Name = "uLabelWriteDate";
            this.uLabelWriteDate.Size = new System.Drawing.Size(125, 20);
            this.uLabelWriteDate.TabIndex = 40;
            this.uLabelWriteDate.Text = "ultraLabel2";
            // 
            // uTextWriteName
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance9;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(580, 36);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteName.TabIndex = 38;
            // 
            // uLabelWriteID
            // 
            this.uLabelWriteID.Location = new System.Drawing.Point(344, 36);
            this.uLabelWriteID.Name = "uLabelWriteID";
            this.uLabelWriteID.Size = new System.Drawing.Size(125, 20);
            this.uLabelWriteID.TabIndex = 36;
            this.uLabelWriteID.Text = "ultraLabel2";
            // 
            // uTextVersionNum
            // 
            appearance97.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Appearance = appearance97;
            this.uTextVersionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Location = new System.Drawing.Point(248, 12);
            this.uTextVersionNum.Name = "uTextVersionNum";
            this.uTextVersionNum.ReadOnly = true;
            this.uTextVersionNum.Size = new System.Drawing.Size(24, 21);
            this.uTextVersionNum.TabIndex = 31;
            // 
            // uLabelVersionNum
            // 
            this.uLabelVersionNum.Location = new System.Drawing.Point(344, 12);
            this.uLabelVersionNum.Name = "uLabelVersionNum";
            this.uLabelVersionNum.Size = new System.Drawing.Size(125, 20);
            this.uLabelVersionNum.TabIndex = 30;
            this.uLabelVersionNum.Text = "ultraLabel2";
            // 
            // uLabelStdNumber
            // 
            this.uLabelStdNumber.Location = new System.Drawing.Point(12, 12);
            this.uLabelStdNumber.Name = "uLabelStdNumber";
            this.uLabelStdNumber.Size = new System.Drawing.Size(125, 20);
            this.uLabelStdNumber.TabIndex = 28;
            this.uLabelStdNumber.Text = "ultraLabel2";
            // 
            // frmMASZ0009
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridDeviceChgPMH);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0009";
            this.Load += new System.EventHandler(this.frmMASZ0009_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0009_Activated);
            this.Resize += new System.EventHandler(this.frmMASZ0009_Resize);
            this.ultraTabPageControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgPMD)).EndInit();
            this.ultraTabPageControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgPMH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.utextAdmitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl1)).EndInit();
            this.uTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDeviceChgPMH;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevisionReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRevisionReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtcDesc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDeviceChgPMD;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRejectReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRejectReason;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitName;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.Misc.UltraLabel uLabelWriteID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVersionNum;
        private Infragistics.Win.Misc.UltraLabel uLabelVersionNum;
        private Infragistics.Win.Misc.UltraLabel uLabelStdNumber;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor utextAdmitDate;

    }
}