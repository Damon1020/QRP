﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMASZ0005.cs                                        */
/* 프로그램명   : 설비점검정보승인                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : 2011-07-04 : ~~~~~ 추가 (권종구)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMASZ0005 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

        public frmMASZ0005()
        {
            InitializeComponent();
        }

        private void frmMASZ0005_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0005_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("설비점검정보승인", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitButton();

             //ContentGroupBox 닫힘상태로
            this.uGroupBoxContentsArea.Expanded = false;

            //그리드 설정 
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipGroupCode, "설비점검그룹코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipGroupName, "설비점검그룹명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "생성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateDate, "생성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAcceptUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAcceptDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevisionReason, "개정사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRejectReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);


                //////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                //////QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                //////brwChannel.mfCredentials(clsUser);

                //////DataTable dtUser = clsUser.mfReadSYSUser(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));

                this.uTextCreateUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextCreateUserName.Text = m_resSys.GetString("SYS_USERNAME");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonFileDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 설비점검정보 승인리스트
                //--------------------------------------------- 설비점검정보 승인 리스트
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipPMAdmit, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    ,false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "PlantCode", "공장코", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "StdNumVersion", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "EquipGroupCode", "설비점검그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "EquipGroupName", "설비점검그룹명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "WriteName", "생성자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "WriteDate", "생성일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "AdmitName", "승인자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "RevisionReason", "개정사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGridEquipPMAdmit, 0);

                // 폰트 설정
                this.uGridEquipPMAdmit.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipPMAdmit.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 점검항목상세
                // ------------------------------점검항목상세 Grid------------------------------------------------------------------
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipPMD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMPeriodCode", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 3
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");


                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "ImageFile", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "MeasureValueFlag", "수치입력여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "UnitDesc", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "LevelCode", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                this.uGridEquipPMD.DisplayLayout.Bands[0].Columns["MeasureValueFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                

                #region DropDown
                //--점검주기 콤보
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtPeriod = clsCommonCode.mfReadCommonCode("C0007", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridEquipPMD, 0, "PMPeriodCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPeriod);

                //--난이도 콤보

                DataTable dtLevel = clsCommonCode.mfReadCommonCode("C0008", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridEquipPMD, 0, "LevelCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtLevel);

                #endregion


               
                // 폰트 설정
                this.uGridEquipPMD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipPMD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 점검그룹설비리스트
                //점검그룹설비리스트---------------------------------------------------------------------------------------------------
                //--기본설정
                wGrid.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    ,false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

               
                //--컬럼설정
                //wGrid.mfSetGridColumn(this.uGrid3, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10, Infragistics.Win.HAlign.Right,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",0,0,1,2,null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",0,0,1,2,null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",1,0,1,2,null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",2,0,1,2,null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",3,0,1,2,null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",4,0,1,2,null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",5,0,1,2,null);

                //---헤더설정
                this.uGridEquipList.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupWeek", "주간점검", 6, 0, 1, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupMonth", "월간점검",7,0,2,2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group3 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupAll", "분기,반기,년간",9,0,3,2, false);

                //헤더안컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMWeekDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 0, 0, 1, 1, group1);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 0, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 1, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 0, 0, 1, 1, group3);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 1, 0, 1, 1, group3);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 2, 0, 1, 1, group3);

               

                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGridEquipList, 0);
                
                //--Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //---------------------------------------------------------------------------------------------------------------------------------------------------
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
        #endregion

        #region 툴바
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                string strPlantCode = uComboSearchPlant.Value.ToString();
                
                //ExpandGroupBox True면 숨김
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                    //Value초기화
                    ValueClear();
                }

                //Popup창 실행
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;
                //처리 로직//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);

                DataTable dtAdmit = clsEquipPMH.mfReadEquipPMAdmit(m_resSys.GetString("SYS_USERID"),strPlantCode, m_resSys.GetString("SYS_LANG"));
                
                
                this.uGridEquipPMAdmit.DataSource = dtAdmit;
                this.uGridEquipPMAdmit.DataBind();
                //데이터 바인드

                this.MdiParent.Cursor = Cursors.Default;

                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtAdmit.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipPMAdmit, 0);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "확인창", "입력사항확인", "저장 할 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    
                    return;
                }
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);

                DataTable dtAdmit = clsEquipPMH.mfSetEquipPMHData();

                //---------------필수입력사항확인------------------//
                if (this.uTextAdmitUserID.Text == "" || this.uTextAdmitUserName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "확인창", "필수입력사항확인", "승인자를 입력해주세요.", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uTextAdmitUserID.Focus();
                    return;
                }
                else if (this.uDateAdmitDate.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "확인창", "필수입력사항확인", "승인일을 입력해주세요.", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uDateAdmitDate.DropDown();
                    return;
                }
                else
                {
                    //----값 저장----//
                    DataRow drAdmit;
                    drAdmit = dtAdmit.NewRow();
                    drAdmit["StdNumber"] = this.uTextStandardNo.Text.Substring(0,7);
                    drAdmit["VersionNum"] = this.uTextVersionNum.Text;
                    drAdmit["AdmitID"] = this.uTextAdmitUserID.Text;
                    drAdmit["AdmitDate"] = this.uDateAdmitDate.Value;
                    drAdmit["RejectReason"] = this.uTextRejectReason.Text;
                    drAdmit["EtcDesc"] = this.uTextEtc.Text;
                    dtAdmit.Rows.Add(drAdmit);

                }
                //--------------------승인 여부 메시지 박스 ------------------------------//
                DialogResult diResult = msg.mfSetMessageBox(MessageBoxType.AgreeReject, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "저장확인", "입력한 정보를 저장하시겠습니까?",
                                    Infragistics.Win.HAlign.Right);

                if (diResult == DialogResult.Yes)
                {
                    dtAdmit.Rows[0]["AdmitStatusCode"] = "FN";
                }
                else if (diResult == DialogResult.No)
                {
                    dtAdmit.Rows[0]["AdmitStatusCode"] = "RE";
                }
                else
                {
                    return;
                }
                //-------------------------저장 팝업 창 ---------------------------//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //--------------처리로직-----------------//
                string strRtn = clsEquipPMH.mfSaveEquipPMAdmit(dtAdmit, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                //Decoding//
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                /////////////
                //---------------------------------------//
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                //처리결과값에 대한 메세지박스//
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
                else
                {
                    string strMessage = "";
                    if (!ErrRtn.ErrMessage.Equals(string.Empty))
                        strMessage = ErrRtn.ErrMessage;
                    else
                        strMessage = "입력한 정보를 저장하지 못했습니다.";


                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "처리결과", "저장처리결과", strMessage,
                                                 Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
           
        }

        public void mfCreate()
        {
            
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipPMAdmit.Rows.Count == 0 && 
                    ((this.uGridEquipList.Rows.Count == 0 && this.uGridEquipPMD.Rows.Count == 0) || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "출력정보 확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                ////처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridEquipPMAdmit.Rows.Count != 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquipPMAdmit);

                if (this.uGridEquipPMD.Rows.Count != 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridEquipPMD);

                if (this.uGridEquipList.Rows.Count != 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridEquipList);

                ///////////////


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion 

        #region 이벤트

        //ExpandGroupBox펼침과 닫힘에 대한 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipPMAdmit.Height = 60;
                    
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipPMAdmit.Height = 760;
                    for (int i = 0; i < this.uGridEquipPMAdmit.Rows.Count; i++)
                    {
                        this.uGridEquipPMAdmit.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        //승인리스트 더블 클릭 시 발생하는 이벤트
        private void uGridEquipPMAdmit_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                if (e.Cell.Row.Cells["AdmitID"].Value.ToString() != m_resSys.GetString("SYS_USERID"))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "확인창", "정보확인", "승인자를 확인해 주세요.", Infragistics.Win.HAlign.Right);
                    return;
                }


                //공장코드 설비그룹코드 표준번호 개정번호 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEuqipGroupCode = e.Cell.Row.Cells["EquipgroupCode"].Value.ToString();
                string strStdNumber = e.Cell.Row.Cells["StdNumber"].Value.ToString();
                string strVersion = e.Cell.Row.Cells["VersionNum"].Value.ToString();


                
                
                //--설비점검정보헤더 조회 --//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);

                //DataTable dtEquipPMH = clsEquipPMH.mfReadEquipPMH(strPlantCode, strEuqipGroupCode, m_resSys.GetString("SYS_LANG"));
                DataTable dtEquipPMH = clsEquipPMH.mfReadEquipPMAdmitH(strPlantCode,strStdNumber,strVersion, m_resSys.GetString("SYS_LANG"));

                if (dtEquipPMH.Rows.Count > 0 && dtEquipPMH.Rows[0]["AdmitStatusCode"].ToString().Equals("AR"))
                {
                    if (this.uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;

                    }

                    e.Cell.Row.Fixed = true;

                    //탭컨트롤 인덱스 기본값으로
                    if (this.uTabEquipPM.SelectedTab.Index != 0)
                    {
                        this.uTabEquipPM.Tabs[0].Selected = true;
                    }

                    //그리드에 있는 정보 텍스트에 넣기
                    this.uTextEquipGroupCode.Text = strEuqipGroupCode;
                    this.uTextEquipGroupName.Text = e.Cell.Row.Cells["EquipGroupName"].Value.ToString();
                    this.uTextAdmitUserID.Text = e.Cell.Row.Cells["AdmitID"].Value.ToString();
                    this.uTextCreateDate.Text = e.Cell.Row.Cells["WriteDate"].Value.ToString();

                    this.uTextVersionNum.Text = strVersion;
                    this.uTextStandardNo.Text = strStdNumber;

                    //데이터바인드
                    this.uTextCreateUserID.Text = dtEquipPMH.Rows[0]["WriteID"].ToString();
                    this.uTextEtc.Text = dtEquipPMH.Rows[0]["EtcDesc"].ToString();
                    this.uTextRejectReason.Text = dtEquipPMH.Rows[0]["RejectReason"].ToString();
                    this.uTextRevisionReason.Text = dtEquipPMH.Rows[0]["RevisionReason"].ToString();
                    this.uTextCreateUserName.Text = dtEquipPMH.Rows[0]["WriteName"].ToString();
                    this.uTextAdmitUserName.Text = dtEquipPMH.Rows[0]["AdmitName"].ToString();


                    //---설비점검정보상세 조회--//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMD), "EquipPMD");
                    QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                    brwChannel.mfCredentials(clsEquipPMD);

                    DataTable dtPMD = clsEquipPMD.mfReadEquipPMD(strStdNumber, strVersion);

                    //데이터바인드
                    this.uGridEquipPMD.DataSource = dtPMD;
                    this.uGridEquipPMD.DataBind();

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipPMD, 0);

                    //--------설비리스트조회---------//
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    brwChannel.mfCredentials(clsEquipGroup);

                    DataTable dtEquip = clsEquipGroup.mfReadEquipGroupList(strPlantCode, strEuqipGroupCode, m_resSys.GetString("SYS_LANG"));

                    //데이터바인드
                    this.uGridEquipList.DataSource = dtEquip;
                    this.uGridEquipList.DataBind();

                    grd.mfSetAutoResizeColWidth(this.uGridEquipList, 0);
                    //------------------------------//
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "승인정보 확인", "승인되었거나 반려되어 조회할 수 없습니다.", Infragistics.Win.HAlign.Right);
                    if (this.uGroupBoxContentsArea.Expanded == true)
                    {
                        this.uGroupBoxContentsArea.Expanded = false;

                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //엔터누를시 발생하는 이벤트 설정
        private void uTextAdmitUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    //승인자 공장코드 저장
                    string strAdmitID = this.uTextAdmitUserID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strAdmitID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "ID확인", "ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextCreateUserID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "경고창", "공장확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //매서드호출
                    DataTable dtCreat = clsUser.mfReadSYSUser(strPlantCode, strAdmitID, m_resSys.GetString("SYS_LANG"));

                    //정보가 없을 시
                    if (dtCreat.Rows.Count == 0)
                    {
                        /* 검색결과 Record수 = 0이면 메시지 띄움 */
                        System.Windows.Forms.DialogResult result;
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.", Infragistics.Win.HAlign.Right);

                        this.uTextAdmitUserID.Text = "";
                        this.uTextAdmitUserName.Text = "";


                        return;
                    }

                    //정보가 확인 시 이름 추가
                    string strUserName = dtCreat.Rows[0]["UserName"].ToString();


                    //정보가 확인 시 승인자 이름 추가
                    this.uTextAdmitUserName.Text = strUserName;


                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //승인자 검색버튼 클릭시 생김
        private void uTextAdmitUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                QRPMAS.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장보냄
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                if (frmUser.PlantCode !="" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "확인창", "입력사항확인", "공장이 맞지 않습니다.", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextAdmitUserID.Text = frmUser.UserID;
                this.uTextAdmitUserName.Text = frmUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //폼을닫거나 닫을때마다 발생되는이벤트
        private void frmMASZ0005_FormClosing(object sender, FormClosingEventArgs e)
        {
            //그리드설정
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }
                ValueClear();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        /// <summary>
        /// Value 와 그리드 지우기
        /// </summary>
        private void ValueClear()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextAdmitUserName.Text = "";
                this.uTextAdmitUserID.Text = "";
                this.uTextAdmitUserName.Text = "";
                this.uTextCreateDate.Text = "";
                this.uTextCreateUserID.Text = "";
                this.uTextCreateUserName.Text = "";
                this.uTextEquipGroupCode.Text = "";
                this.uTextEquipGroupName.Text = "";
                this.uTextEtc.Text = "";
                this.uTextVersionNum.Text = "";
                this.uTextStandardNo.Text = "";
                this.uTextRevisionReason.Text = "";
                this.uTextRejectReason.Text = "";
                
                this.uDateAdmitDate.DateTime = DateTime.Today;

                if (this.uGridEquipList.Rows.Count > 0)
                {
                    this.uGridEquipList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipList.Rows.All);
                    this.uGridEquipList.DeleteSelectedRows(false);
                }

                if (this.uGridEquipPMD.Rows.Count > 0)
                {
                    this.uGridEquipPMD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipPMD.Rows.All);
                    this.uGridEquipPMD.DeleteSelectedRows(false);
                }


                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = clsUser.mfReadSYSUser(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));

                this.uTextCreateUserName.Text = dtUser.Rows[0]["UserName"].ToString();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMASZ0005_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                int intFileNum = 0;
                for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                {
                    //경로가 없는 행이 있는지 체크
                    if (this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["Check"].Value) == true)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "처리결과", "처리결과", "다운받을 첨부화일이 없습니다.",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비점검정보 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0023");


                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                    {
                        if (this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["Check"].Value) == true)
                            arrFile.Add(this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


    }
}
