﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리 기준정보                                     */
/* 모듈(분류)명 : 설비점검 정보등록                                     */
/* 프로그램ID   : frmPOP21.cs                                           */
/* 프로그램명   : 설비정보                                              */
/* 작성자       : 정결                                                  */
/* 작성일자     : 2011-11-10                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Resources;
using System.Threading;

namespace QRPMAS.UI
{
    public partial class frmPOP0021 : Form
    {
        //Resource
        QRPGlobal SysRes = new QRPGlobal();

        //BL 호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();


        //데이터테이블을 넘기기 위한 전역변수

        private DataTable dtEquip = new DataTable();

        public DataTable dtRtnEquip
        {
            get { return dtEquip; }
            set { dtEquip = value; }
        }

        public frmPOP0021()
        {
            InitializeComponent();
        }

        // 폼 로드
        private void frmPOP0021_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");

            InitButton();
            InitComboBox();
            InitGrid(); 
            InitLabel();

            QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        #region 컨트롤 초기화

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonSearch, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                wButton.mfSetButton(this.uButtonClose, "닫기", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Stop);
                wButton.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Label초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelSearchEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue_Choice(), "PlantCode", "PlantName", dtPlant);

                this.uComboSearchPlant.Enabled = false;

                this.uComboSearchPlant.Appearance.BackColor = Color.White;
                this.uComboSearchStation.Appearance.BackColor = Color.White;
                this.uComboSearchEquipLoc.Appearance.BackColor = Color.White;
                this.uComboProcessGroup.Appearance.BackColor = Color.White;
                this.uComboSearchEquipType.Appearance.BackColor = Color.White;
                this.uComboSearchEquipGroup.Appearance.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //Header Grid
                grd.mfInitGeneralGrid(this.uGridHeader, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridHeader, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHeader, 0, "StdNumber", "StdNumber", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHeader, 0, "VersionNum", "VersionNum", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHeader, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHeader, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHeader, 0, "ProcessGroupCode", "설비대분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHeader, 0, "EquipLargeTypeCode", "설비중분류", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHeader, 0, "EquipGroupCode", "설비 그룹 코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHeader, 0, "EquipGroupName", "설비 그룹명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                //Detail 그리드
                grd.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridDetail, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "PMPeriodCode", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "ImageFile", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "MeasureValueFlag", "수치입력여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDetail, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnn", "0");

                grd.mfSetGridColumn(this.uGridDetail, 0, "UnitDesc", "단위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "LevelCode", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "VersionNum", "VersionNum", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "StdNumber", "StdNumber", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridDetail.DisplayLayout.Bands[0].Columns["MeasureValueFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //--점검주기 콤보
                QRPBrowser brwChannel = new QRPBrowser();
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtPeriod = clsCommonCode.mfReadCommonCode("C0007", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridDetail, 0, "PMPeriodCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtPeriod);
                //--난이도 콤보

                DataTable dtLevel = clsCommonCode.mfReadCommonCode("C0008", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridDetail, 0, "LevelCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtLevel);

                // Set Font
                this.uGridDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridHeader.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridHeader.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Event

        /// <summary>
        /// 검색버튼 클릭 이벤트(HeaderTable 검색)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                Search(strPlantCode);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Header 그리드 클릭시, Detail 검색
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridHeader_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {
                QRPBrowser brwChannel = new QRPBrowser();
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMD), "EquipPMD");
                QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                brwChannel.mfCredentials(clsEquipPMD);

                String strStdNumber = e.Cell.Row.Cells["StdNumber"].Value.ToString();
                String strVersionNum = e.Cell.Row.Cells["VersionNum"].Value.ToString();

                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.Cursor = Cursors.WaitCursor;

                DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMD(strStdNumber, strVersionNum);

                m_ProgressPopup.mfCloseProgressPopup(this);
                this.Cursor = Cursors.Default;

                if (dtEquipPMD.Rows.Count > 0)
                {
                    this.uGridDetail.DataSource = dtEquipPMD;
                    this.uGridDetail.DataBind();
                }
                else
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                       Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001135", "M001115", "M001102",
                                                       Infragistics.Win.HAlign.Right);
                    while (this.uGridDetail.Rows.Count > 0)
                    {
                        this.uGridDetail.Rows[0].Delete(false);
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 닫기버튼 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 확인버튼 클릭시, 오른쪽 Detail그리드의 값을 데이터테이블에 담아서 리턴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMD), "EquipPMD");
                QRPMAS.BL.MASEQU.EquipPMD clsPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                brwChannel.mfCredentials(clsPMD);

                dtEquip = clsPMD.mfSetEquipPMDData();


                DataRow drEquipD;

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    drEquipD = dtEquip.NewRow();

                    drEquipD["PMInspectRegion"] = this.uGridDetail.Rows[i].Cells["PMInspectRegion"].Value.ToString();
                    drEquipD["PMInspectName"] = this.uGridDetail.Rows[i].Cells["PMInspectName"].Value.ToString();
                    drEquipD["PMInspectCriteria"] = this.uGridDetail.Rows[i].Cells["PMInspectCriteria"].Value.ToString();
                    drEquipD["PMPeriodCode"] = this.uGridDetail.Rows[i].Cells["PMPeriodCode"].Value.ToString();
                    drEquipD["PMMethod"] = this.uGridDetail.Rows[i].Cells["PMMethod"].Value.ToString();
                    drEquipD["FaultFixMethod"] = this.uGridDetail.Rows[i].Cells["FaultFixMethod"].Value.ToString();
                    drEquipD["StandardManCount"] = this.uGridDetail.Rows[i].Cells["StandardManCount"].Value.ToString();
                    drEquipD["StandardTime"] = this.uGridDetail.Rows[i].Cells["StandardTime"].Value.ToString();
                    drEquipD["LevelCode"] = this.uGridDetail.Rows[i].Cells["LevelCode"].Value.ToString();
                    //drEquipD["StdNumber"] = this.uGridHeader.ActiveRow.Cells["StdNumber"].Value.ToString();
                    //drEquipD["VersionNum"] = this.uGridHeader.ActiveRow.Cells["VersionNum"].Value.ToString();
                    drEquipD["StdNumber"] = this.uGridDetail.Rows[i].Cells["StdNumber"].Value.ToString();
                    drEquipD["VersionNum"] = this.uGridDetail.Rows[i].Cells["VersionNum"].Value.ToString();
                    drEquipD["Seq"] = this.uGridDetail.Rows[i].Cells["Seq"].Value.ToString();
                    drEquipD["MeasureValueFlag"] = this.uGridDetail.Rows[i].Cells["MeasureValueFlag"].Value.ToString();
                    drEquipD["UnitDesc"] = this.uGridDetail.Rows[i].Cells["UnitDesc"].Value.ToString();
                    //drEquipD["ImageFile"] = this.uGridDetail.Rows[i].Cells["ImageFile"].Value.ToString();

                    dtEquip.Rows.Add(drEquipD);
                }

                dtRtnEquip = dtEquip.Copy();
                this.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 공장코드 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //콤보박스 클리어
                this.uComboSearchStation.Items.Clear();

                //공장코드 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                /////////////////////////////////////////////////////////////

                //Station정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                brwChannel.mfCredentials(clsStation);

                //Station콤보조회 BL호출
                DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                wCombo.mfSetComboEditor(this.uComboSearchStation, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue_Choice(), "StationCode", "StationName", dtStation);

                ////////////////////////////////////////////////////////////

                mfSearchCombo(this.uComboSearchPlant.Value.ToString(), "", "", "", "", 3);
                InitSearch();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// Statioin 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //검색조건저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //설비위치정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsEquipLocation);

                    //설비위치정보콤보조회 매서드 실행
                    DataTable dtLoc = clsEquipLocation.mfReadLocation_Combo(strPlantCode, strStationCode, m_resSys.GetString("SYS_LANG"));

                    WinComboEditor wCombo = new WinComboEditor();

                    //설비위치정보콤보 이전 정보 클리어
                    this.uComboSearchEquipLoc.Items.Clear();

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLoc, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue_All(), "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(strPlantCode, strStationCode, "", "", "", 3);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비 위치 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();

                //ProcessGroup(설비대분류)
                DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                wCombo.mfSetComboEditor(this.uComboProcessGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                ////////////////////////////////////////////////////////////
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();

                //설비중분류조회 매서드 실행
                DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                wCombo.mfSetComboEditor(this.uComboSearchEquipType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                   , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                   , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                string strEquipLargeTypeCode = this.uComboSearchEquipType.Value.ToString();
                /////////////----- 설비소분류 콤보박스 ----/////////////////

                DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                this.uComboSearchEquipType.Items.Clear();
                this.uComboSearchEquipGroup.Items.Clear();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();


                //설비중분류조회 매서드 실행
                DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                wCombo.mfSetComboEditor(this.uComboSearchEquipType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                   , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                   , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                /////////////----- 설비소분류 콤보박스 ----/////////////////
                string strEquipLargeTypeCode = this.uComboSearchEquipType.Value.ToString();

                DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        private void uComboSearchEquipType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipLargeTypeCode = this.uComboSearchEquipType.Value.ToString();

                /////////////----- 설비소분류 콤보박스 ----/////////////////

                DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipGroupCode", "EquipGroupName", dtEquipGroup);
            }
            catch (Exception ex)
            { }
            finally
            { }
        }

        #endregion

        #region Method

        //검색 메소드
        public void Search(String strPlantCode)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();

                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroupCode = this.uComboProcessGroup.Value.ToString();
                string strEquipLargeTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strEquipGroupCode = this.uComboSearchEquipGroup.Value.ToString();


                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsPHM = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsPHM);

                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.Cursor = Cursors.WaitCursor;

                DataTable dtPHM = clsPHM.mfReadEquipPMHPopup(strPlantCode, strStationCode, strEquipLocCode, strProcessGroupCode
                                                                                        , strEquipLargeTypeCode, strEquipGroupCode, m_resSys.GetString("SYS_LANG"));

                m_ProgressPopup.mfCloseProgressPopup(this);
                this.Cursor = Cursors.Default;

                if (dtPHM.Rows.Count > 0)
                {
                    this.uGridHeader.DataSource = dtPHM;
                    this.uGridHeader.DataBind();
                }
                else
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitSearch()
        {
            try
            {
                while (this.uGridHeader.Rows.Count > 0)
                {
                    this.uGridHeader.Rows[0].Delete(false);
                }
                while (this.uGridDetail.Rows.Count > 0)
                {
                    this.uGridDetail.Rows[0].Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();
                string strAll = ComboDefaultValue_All();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strAll, "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strAll, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strAll, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strAll, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strAll, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strAll, "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보 기본값(선택)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue_Choice()
        {
            try
            {
                string strRtnValue = "";
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();

                if (strLang.Equals("KOR"))
                    strRtnValue = "선택";
                else if (strLang.Equals("CHN"))
                    strRtnValue = "选择";
                else if (strLang.Equals("ENG"))
                    strRtnValue = "Choice";

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보 기본값(전체)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue_All()
        {
            try
            {
                string strRtnValue = "";
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();

                if (strLang.Equals("KOR"))
                    strRtnValue = "전체";
                else if (strLang.Equals("CHN"))
                    strRtnValue = "全部";
                else if (strLang.Equals("ENG"))
                    strRtnValue = "All";


                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        #endregion

    }
}
