﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMASZ0008.cs                                        */
/* 프로그램명   : 설비품종교체점검정보승인                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMASZ0008 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0008()
        {
            InitializeComponent();
        }
        private void frmMASZ0008_Activated(object sender, EventArgs e)
        {
            //툴바활성화
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0008_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀설정
            titleArea.mfSetLabelText("설비품종교체점검정보승인", m_resSys.GetString("SYS_FONTNAME"), 12);
            //컨트롤초기화
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitButton();
            InitGroupBox();
            InitText();
            // ContentGroupBox 닫힘상태로
            //this.uGroupBoxContentsArea.Expanded = false;
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                //레이블설정
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStdNumber, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelVersionNum, "개정번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                lbl.mfSetLabel(this.uLabelWriteID, "생성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelWriteDate, "생성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel7, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel8, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabelRejectReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEtcDesc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRevisionReason, "개정사유", m_resSys.GetString("SYS_FONTNAME"), true, false);

                this.uLabelVersionNum.Visible = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본정보
                //--그리드기본설정
                grd.mfInitGeneralGrid(this.uGridDeviceChgPMH, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //--컬럼설정
                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "Standard", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                //grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "EquipGroupCode", "설비점검그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 170, false, false, 10, Infragistics.Win.HAlign.Center,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                //grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "EquipGroupName", "설비점검그룹명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                //    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "EquipTypeCode", "설비유형코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 170, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "EquipTypeName", "설비유형명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                
                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "WriteName", "생성자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "WriteDate","생성일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", "");
                
                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "AdmitName", "승인자", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 15, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 15, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "RevisionReason", "개정사유", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 0, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridDeviceChgPMH.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDeviceChgPMH.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                
                //빈줄추가
                grd.mfAddRowGrid(this.uGridDeviceChgPMH, 0);

                //점검항목상세
                //--그리드기본설정
                grd.mfInitGeneralGrid(this.uGridDeviceChgPMD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, false,
                    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //--컬럼설정
                grd.mfSetGridColumn(this.uGridDeviceChgPMD, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, true, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDeviceChgPMD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 60, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMD, 0, "PMInspectGubun", "점검항목구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMD, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMD, 0, "PMInspectCriteria", "점검기준", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMD, 0, "LevelCode", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "키");

                //--Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridDeviceChgPMD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDeviceChgPMD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                grd.mfAddRowGrid(this.uGridDeviceChgPMD, 0);


                //////////점검설비리스트
                //////////--그리드기본설정
                ////////grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, false,
                ////////    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                ////////    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                ////////    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //////////--헤더설정
                ////////this.uGrid2.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                ////////Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = grd.mfSetGridGroup(this.uGrid2, 0, "주간점검", "주간점검", true);
                ////////Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = grd.mfSetGridGroup(this.uGrid2, 0, "월간점검", "월간점검", true);
                ////////Infragistics.Win.UltraWinGrid.UltraGridGroup group3 = grd.mfSetGridGroup(this.uGrid2, 0, "분기,반기,년간", "분기,반기,년간", true);

                //////////--컬럼설정
                ////////grd.mfSetGridColumn(this.uGrid2, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10
                ////////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 2, null);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 50
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "PMWeekDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, group1);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "PMMonthWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, group2);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "PMMonthDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, group2);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "PMYearMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 10, 0, 1, 1, group3);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "PMYearWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 11, 0, 1, 1, group3);

                ////////grd.mfSetGridColumn(this.uGrid2, 0, "PMYearDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 12, 0, 1, 1, group3);

                //////////--Grid초기화 후 Font크기를 아래와 같이 적용
                ////////this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                ////////this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //////////빈줄추가
                ////////grd.mfAddRowGrid(this.uGrid2, 0);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                this.uComboPlant.Items.Clear();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        //버튼초기화
        private void InitButton()
        {
            try
            {
                
            }
            catch
            { }
            finally
            { }
        }

        private void InitGroupBox()
        {
            try
            {
                WinGroupBox gb = new WinGroupBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //  GroupBox
                gb.mfSetGroupBox(this.uGroupBoxContentsArea, GroupBoxType.INFO, "상세정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                //폰트설정
                uGroupBoxContentsArea.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxContentsArea.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

 

                // ExtandableGroupBox 설정
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }
            /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextStdNumber.Enabled = false;
                this.uTextRejectReason.Enabled = true;
                this.uTextRevisionReason.Enabled = false;

                this.uTextWriteID.Text = "";
                this.uTextWriteName.Text = "";
                this.uTextAdmitID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextAdmitName.Text = m_resSys.GetString("SYS_USERNAME");

                

                this.uTextStdNumber.Text = "";
                this.uTextVersionNum.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextRejectReason.Text = "";
                this.uTextRevisionReason.Text = "";

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        

#endregion

        #region 툴바관련

        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMH), "DeviceChgPMH");
                QRPMAS.BL.MASEQU.DeviceChgPMH clsDeviceChgPMH = new QRPMAS.BL.MASEQU.DeviceChgPMH();
                brwChannel.mfCredentials(clsDeviceChgPMH);

                string strPlantCode = this.uComboPlant.Value.ToString();
                // Call Method
                DataTable dtDeviceChgPMH = clsDeviceChgPMH.mfReadDeviceChgPMHList(strPlantCode, "AR", m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                this.uGridDeviceChgPMH.DataSource = dtDeviceChgPMH;
                this.uGridDeviceChgPMH.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                if (dtDeviceChgPMH.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                              Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDeviceChgPMH, 0);
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtGroup = new DataTable();
                DataTable dtGroupD = new DataTable();
                DataTable dtSaveType = new DataTable();
                DataTable dtDelType = new DataTable();
                DataRow row;
                DialogResult DResult = new DialogResult();

             
                // 필수입력사항 확인
                if (this.uTextStdNumber.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "저장 할 정보를 리스트에서 선택하세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                if (this.uTextAdmitID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "승인자를 입력하세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }
                
                DialogResult dir = new DialogResult();

     
                //dir = msg.mfSetMessageBox(MessageBoxType.YesNoCancel, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                //                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "승인하시겠습니까? \n Y:승인      N:반려      C:취소"
                //                            , Infragistics.Win.HAlign.Right);

                dir = msg.mfSetMessageBox(MessageBoxType.AgreeReject, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "승인하시겠습니까?"
                                            , Infragistics.Win.HAlign.Right);

                if (dir != DialogResult.Cancel)
                {
                    if (dir == DialogResult.Yes)
                    {
                        if (this.uTextAdmitID.Text == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "승인자를 입력해주세요", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uTextAdmitID.Focus();
                            return;

                        }
                    }
                    // BL 호출(헤더)
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMH), "DeviceChgPMH");
                    QRPMAS.BL.MASEQU.DeviceChgPMH iGroup = new QRPMAS.BL.MASEQU.DeviceChgPMH();
                    brwChannel.mfCredentials(iGroup);

                    dtGroup = iGroup.mfSetDatainfo();

                    row = dtGroup.NewRow();
                    row["StdNumber"] = this.uTextStdNumber.Text;

                    if (this.uTextVersionNum.Text == "")
                    {
                        row["VersionNum"] = 0;
                    }
                    else
                    {
                        row["VersionNum"] = this.uTextVersionNum.Text;
                    }

                    row["WriteID"] = this.uTextWriteID.Text;
                    row["WriteDate"] = this.uTextWriteDate.Text;
                    row["AdmitID"] = this.uTextAdmitID.Text;
                    row["AdmitDate"] = this.uDateAdmitDate.Value.ToString();
 
                    if (dir == DialogResult.Yes)
                    {
                        row["AdmitStatusCode"] = "FN";   // 승인할 경우 FN    
                    }
                    else if (dir == DialogResult.No)
                    {
                        row["AdmitStatusCode"] = "RE";   // 반려할 경우 RE    
                    }

                    row["RevisionType"] = "";
                    row["EtcDesc"] = this.uTextEtcDesc.Text;
                    row["RevisionReason"] = this.uTextRevisionReason.Text;
                    row["RejectReason"] = this.uTextRejectReason.Text;

                    dtGroup.Rows.Add(row);


                    // BL 호출(아이템)
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMD), "DeviceChgPMD");
                    QRPMAS.BL.MASEQU.DeviceChgPMD iGroupD = new QRPMAS.BL.MASEQU.DeviceChgPMD();
                    brwChannel.mfCredentials(iGroupD);

                    dtGroupD = iGroupD.mfSetDatainfo();
                    ////for (int i = 0; i < this.uGridDeviceChgPMH.Rows.Count; i++)
                    ////{
                    ////    this.uGridDeviceChgPMH.ActiveCell = this.uGridDeviceChgPMH.Rows[0].Cells[0];

                    ////    if (this.uGridDeviceChgPMH.Rows[i].Hidden == false)
                    ////    {
                    ////        row = dtGroupD.NewRow();
                    ////        row["StdNumber"] = this.uTextStdNumber.Text;
                    ////        row["VersionNum"] = 0;
                    ////        row["Seq"] = i + 1;
                    ////        row["PMInspectGubun"] = this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectGubun"].Value.ToString();
                    ////        row["PMInspectName"] = this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectName"].Value.ToString();
                    ////        row["PMInspectCriteria"] = this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectCriteria"].Value.ToString();
                    ////        row["LevelCode"] = this.uGridDeviceChgPMH.Rows[i].Cells["LevelCode"].Value.ToString();
                    ////        dtGroupD.Rows.Add(row);
                    ////    }
                    ////}



                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 처리 로직 //
                    // 저장함수 호출
                    string rtMSG = iGroup.mfSaveDeviceChgPMH(dtGroup, dtGroupD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                    // 처리로직 끝//

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 처리결과에 따른 메세지 박스
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                            Infragistics.Win.HAlign.Right);

                        mfSearch();
                        ////SaveInspectType();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                            Infragistics.Win.HAlign.Right);
                    }

                    mfCreate();
                    mfSearch();
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
        }
        public void mfCreate()
        {
            InitGroupBox();
            InitComboBox();
            InitText();
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                if (this.uGridDeviceChgPMH.Rows.Count == 0 && (this.uGridDeviceChgPMD.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))// && this.uGridEquipList.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "출력정보 확인", "엑셀출력 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();

                if(this.uGridDeviceChgPMH.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridDeviceChgPMH);

                if (this.uGridDeviceChgPMD.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridDeviceChgPMD);

                //if (this.uGridEquipList.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                //    grd.mfDownLoadGridToExcel(this.uGridEquipList);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        public void mfPrint()
        {
        }
        #endregion

        #region 이벤트관련
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDeviceChgPMH, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void ultraGrid4_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDeviceChgPMD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void ultraGrid5_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridEquipList, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        //펼치거나 닫을 때 발생하는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridDeviceChgPMH.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridDeviceChgPMH.Height = 740;
                    for (int i = 0; i < uGridDeviceChgPMH.Rows.Count; i++)
                    {
                        uGridDeviceChgPMH.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion


        private void uGridDeviceChgPMH_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //// 초기화 함수 호출
                mfCreate();


                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }


                string strStdNumber = e.Cell.Row.Cells["StdNumber"].Value.ToString();
                string strVersionNum = e.Cell.Row.Cells["VersionNum"].Value.ToString();
                string strPlantCode = "";
                string strEquipTypeCode = "";

                //채널 연결
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();

                //-----------헤더 기본정보 --------//
                //BL호출
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMH), "DeviceChgPMH");
                QRPMAS.BL.MASEQU.DeviceChgPMH clsDeviceChgPMH = new QRPMAS.BL.MASEQU.DeviceChgPMH();
                brwChnnel.mfCredentials(clsDeviceChgPMH);

                DataTable dtDeviceChgPMH = clsDeviceChgPMH.mfReadDeviceChgPMHDetail(strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < dtDeviceChgPMH.Rows.Count; i++)
                {
                    this.uTextStdNumber.Text = dtDeviceChgPMH.Rows[i]["StdNumber"].ToString();
                    this.uTextVersionNum.Text = dtDeviceChgPMH.Rows[i]["VersionNum"].ToString();
                    this.uTextWriteID.Text = dtDeviceChgPMH.Rows[i]["WriteID"].ToString();
                    this.uTextWriteName.Text = dtDeviceChgPMH.Rows[i]["WriteName"].ToString();
                    this.uTextWriteDate.Value = dtDeviceChgPMH.Rows[i]["WriteDate"].ToString();
                    this.uTextAdmitID.Text = dtDeviceChgPMH.Rows[i]["AdmitID"].ToString();
                    this.uTextAdmitName.Text = dtDeviceChgPMH.Rows[i]["AdmitName"].ToString();

                    this.uTextEtcDesc.Text = dtDeviceChgPMH.Rows[i]["EtcDesc"].ToString();
                    this.uTextRejectReason.Text = dtDeviceChgPMH.Rows[i]["RejectReason"].ToString();
                    this.uTextRevisionReason.Text = dtDeviceChgPMH.Rows[i]["RevisionReason"].ToString();

                    strPlantCode = dtDeviceChgPMH.Rows[i]["PlantCode"].ToString();
                    strEquipTypeCode = dtDeviceChgPMH.Rows[i]["EquipTypeCode"].ToString();
                }




                //-----------점검항목 상세 그리드 바인딩--------------//
                //BL호출

                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMD), "DeviceChgPMD");
                QRPMAS.BL.MASEQU.DeviceChgPMD clsDeviceChgPMD = new QRPMAS.BL.MASEQU.DeviceChgPMD();
                brwChnnel.mfCredentials(clsDeviceChgPMD);

                //매서드호출
                DataTable dtDeviceChgPMD = clsDeviceChgPMD.mfReadDeviceChgPMD(strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));


                //테이터바인드
                uGridDeviceChgPMD.DataSource = dtDeviceChgPMD;
                uGridDeviceChgPMD.DataBind();

                if (dtDeviceChgPMD.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDeviceChgPMD, 0);
                }

                //-----------점검그룹 설비 리스트 그리드 바인딩--------------//
                //BL호출
                ////////brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                ////////QRPMAS.BL.MASEQU.EquipGroup EquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                ////////brwChnnel.mfCredentials(EquipGroup);

                //////////매서드호출
                ////////DataTable dtEquipList = EquipGroup.mfReadEquipGroupList(strPlantCode, strEquipTypeCode, m_resSys.GetString("SYS_LANG"));


                //////////테이터바인드
                ////////uGrid2.DataSource = dtEquipList;
                ////////uGrid2.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextAdmitID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUserID = this.uTextAdmitID.Text;
                    WinMessageBox msg = new WinMessageBox();

                    // 공장콤보박스 미선택시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, "굴림", 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "공장을 선택해주세요.",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            this.uTextAdmitName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, "굴림", 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "사번을 찾을수 없습니다",
                                            Infragistics.Win.HAlign.Right);

                            this.uTextAdmitName.Text = "";
                            this.uTextAdmitID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextAdmitID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.ShowDialog();

                this.uTextAdmitID.Text = frmPOP.UserID;
                this.uTextAdmitName.Text = frmPOP.UserName;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmMASZ0008_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
