﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMAS0030.cs                                         */
/* 프로그램명   : 점검결과정보                                          */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-01                                            */
/* 수정이력     : 2011-08-19 : 공통코드 사용하도록 수정 (서정현)        */
/*                xxxx-xx-xx : ~~~~~ 추가 ()                            */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0030 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        public frmMAS0030()
        {
            InitializeComponent();
        }

        private void frmMAS0030_Activated(object sender, EventArgs e)
        {
            //툴바활성
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //사용여부설정
            // 버튼활성화 여부                          검색, 저장, 삭제,  신규,  출력, 엑셀 
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0030_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            titleArea.mfSetLabelText("점검결과정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            //각컨트롤 초기화
            SetToolAuth();
            InitGrid();
            InitLabel();
            InitComboBox();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }
        
        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGridPMResult, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridPMResult, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridPMResult, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                grd.mfSetGridColumn(this.uGridPMResult, 0, "PMResultCode", "점검결과코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 5, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMResult, 0, "PMResultName", "점검결과명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMResult, 0, "PMResultNameCh", "점검결과명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMResult, 0, "PMResultNameEn", "점검결과명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridPMResult, 0, "PMUseFlag", "기생산Pass여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "F");

                grd.mfSetGridColumn(this.uGridPMResult, 0, "ChangeFlag", "교체여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "F");

                grd.mfSetGridColumn(this.uGridPMResult, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "T");

                this.uGridPMResult.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPMResult.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드에 DropDown 추가
                // 그리드 컬럼에 공장 콤보박스 추가
                 

                // BL 공장호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Plant정보를 얻어오는 함수를 호출하여 DataTable에 저장
               DataTable PlantList = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(uGridPMResult, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", PlantList);

                // 1.브라우저 객체 생성 -> 2.채널등록 -> 3.공통코드객체 선언 및 생성 -> 4.권한설정 -> 5.데이터테이블 형태로 반환받아 옴. -> 6. 그리드 컬럼에 매칭
 
                // QRPBrowser brwChannel = new QRPBrowser(); // 1
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode"); // 2
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode(); // 3
                brwChannel.mfCredentials(clsComCode); // 4

                DataTable dtPMUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG")); // 5
                DataTable dtChangeFlag = clsComCode.mfReadCommonCode("C0053", m_resSys.GetString("SYS_LANG")); // 5
                
                //그리드 컬럼 전체에 적용하여 넣기(mfSetGridColumnValueList)
                grd.mfSetGridColumnValueList(this.uGridPMResult, 0, "PMUseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtPMUseFlag);
                grd.mfSetGridColumnValueList(this.uGridPMResult, 0, "ChangeFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtChangeFlag);
                grd.mfSetGridColumnValueList(this.uGridPMResult, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                //한줄생성
                grd.mfAddRowGrid(this.uGridPMResult, 0);
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                                
                string strLang = m_resSys.GetString("SYS_LANG");
                string strChoice = "";
                if (strLang.Equals("KOR"))
                    strChoice = "선택";
                else if (strLang.Equals("CHN"))
                    strChoice = "选择";
                else if (strLang.Equals("ENG"))
                    strChoice = "Choice";

                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left ,"","",strChoice
                    ,"PlantCode", "PlantName",dtPlant);
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

#endregion

        #region 툴바관련

        /// <summary>
        /// 검색 버튼 기능 
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType pmResult = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(pmResult);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                DataTable dt = pmResult.mfReadMASPMResultType(strPlantCode, m_resSys.GetString("SYS_LANG"));
                
                this.uGridPMResult.DataSource = dt;
                this.uGridPMResult.DataBind();

                // 디비로 가져온 정보의 PK 편집 불가 상태로
                for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                {
                    this.uGridPMResult.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridPMResult.Rows[i].Cells["PMResultCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridPMResult.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                    this.uGridPMResult.Rows[i].Cells["PMResultCode"].Appearance.BackColor = Color.Gainsboro;
                }

                WinGrid grd = new WinGrid();

                // 바인딩후 체크박스 상태를 모두 Uncheck로 만든다
                grd.mfSetAllUnCheckedGridColumn(this.uGridPMResult, 0, "Check");

                // RowSelector Clear
               // grd.mfClearRowSeletorGrid(this.uGridPMResult);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    grd.mfSetAutoResizeColWidth(this.uGridPMResult, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제기능 구현
        /// </summary>   
        public void mfDelete()
        {
            try
            {
                //SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResultType);

                //함수호출 매개변수 DataTable
                DataTable dtPMResult = clsPMResultType.mfSetDataInfo();

                DialogResult DResult = new DialogResult();

                string strLang = m_resSys.GetString("SYS_LANG");

                if(this.uGridPMResult.Rows.Count > 0)
                    this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[0].Cells[0];  // 활성셀을 첫번째줄 첫번째 셀로 이동

                //chek된것 삭제
                for (int i = 0; i < uGridPMResult.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridPMResult.Rows[i].Cells["Check"].Value) == true)
                    {
                        string strRowNum = this.uGridPMResult.Rows[i].RowSelectorNumber.ToString();

                        //필수입력사항 확인
                        if (this.uGridPMResult.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000481",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PlantCode"];
                            this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000521",strLang), Infragistics.Win.HAlign.Center);

                            //FocusCell
                            this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PMResultCode"];
                            this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            DataRow drPMResult = dtPMResult.NewRow();
                            drPMResult["PlantCode"] = this.uGridPMResult.Rows[i].Cells["PlantCode"].Value.ToString();
                            drPMResult["PMResultCode"] = this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString();
                            dtPMResult.Rows.Add(drPMResult);
                        }
                    }
                }

                if (dtPMResult.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //함수호출
                        string strSG = clsPMResultType.mfDeletePMResultType(dtPMResult);

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strSG);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        //삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001135", "M000638", "M000677",
                                 Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001135", "M000638", "M000923",
                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                // BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.PMResultType), "PMResultType");
                QRPMAS.BL.MASEQU.PMResultType clsPMResultType = new QRPMAS.BL.MASEQU.PMResultType();
                brwChannel.mfCredentials(clsPMResultType);

                // 저장 함수호출 매개변수 DataTable
                DataTable dtPMResult = clsPMResultType.mfSetDataInfo();

                string strLang = m_resSys.GetString("SYS_LANG");
                DialogResult DResult = new DialogResult();

                if(this.uGridPMResult.Rows.Count > 0 )
                    this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[0].Cells[0];

                for (int i = 0; i < this.uGridPMResult.Rows.Count; i++)
                {

                    //그리드가 수정되었을때 저장
                    if (this.uGridPMResult.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        string strRowNum = this.uGridPMResult.Rows[i].RowSelectorNumber.ToString();

                        //필수 입력사항 확인
                        if (this.uGridPMResult.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error,m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000481",strLang), Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PlantCode"];
                            this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000521",strLang), Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PMResultCode"];
                            this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGridPMResult.Rows[i].Cells["PMResultName"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000520",strLang), Infragistics.Win.HAlign.Center);

                            this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PMResultName"];
                            this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        ////else if (this.uGridPMResult.Rows[i].Cells["PMResultNameCh"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                        ////        , (i + 1) + "번째 열의 점검결과명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PMResultNameCh"];
                        ////    this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}
                        ////else if (this.uGridPMResult.Rows[i].Cells["PMResultNameEn"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                        ////        , (i + 1) + "번째의 열의 점검결과명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    this.uGridPMResult.ActiveCell = this.uGridPMResult.Rows[i].Cells["PMResultNameEn"];
                        ////    this.uGridPMResult.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}
                        else
                        {
                            DataRow drPMResult = dtPMResult.NewRow();
                            drPMResult["PlantCode"] = this.uGridPMResult.Rows[i].Cells["PlantCode"].Value.ToString();
                            drPMResult["PMResultCode"] = this.uGridPMResult.Rows[i].Cells["PMResultCode"].Value.ToString();
                            drPMResult["PMResultName"] = this.uGridPMResult.Rows[i].Cells["PMResultName"].Value.ToString();
                            drPMResult["PMResultNameCh"] = this.uGridPMResult.Rows[i].Cells["PMResultNameCh"].Value.ToString();
                            drPMResult["PMResultNameEn"] = this.uGridPMResult.Rows[i].Cells["PMResultNameEn"].Value.ToString();
                            drPMResult["PMUseFlag"] = this.uGridPMResult.Rows[i].Cells["PMUseFlag"].Value;
                            drPMResult["ChangeFlag"] = this.uGridPMResult.Rows[i].Cells["ChangeFlag"].Value.ToString();
                            drPMResult["UseFlag"] = this.uGridPMResult.Rows[i].Cells["UseFlag"].Value.ToString();
                            
                            dtPMResult.Rows.Add(drPMResult);
                        }
                    }
                }
                if (dtPMResult.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        QRPProgressBar uProgressPopup = new QRPProgressBar();
                        Thread uTh = uProgressPopup.mfStartThread();
                        uProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //저장함수 호출
                        String strPMResult = clsPMResultType.mfSavePMResultType(dtPMResult, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        //Decoding
                        TransErrRtn ErrEtn = new TransErrRtn();
                        ErrEtn = ErrEtn.mfDecodingErrMessage(strPMResult);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        uProgressPopup.mfCloseProgressPopup(this);

                        //처리결과에 따른 메세지 박스
                        if (ErrEtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000930"
                            , Infragistics.Win.HAlign.Right);
                            mfSearch();

                        }
                        else
                        {
                            string strMes = "";
                            if (ErrEtn.ErrMessage.Equals(string.Empty))
                                strMes = msg.GetMessge_Text("M000953", strLang);
                            else
                                strMes = ErrEtn.ErrMessage;

                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMes, Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
                
        }

        /// <summary>
        /// 신규 생성
        /// </summary>
        public void mfCreate()
        {
           // 해당 없음 
        }

        /// <summary>
        /// 엑셀 Export 
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //Systme ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridPMResult.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000811", "M000805", Infragistics.Win.HAlign.Right);
                    return;
                }
                //처리 로직//
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                grd.mfDownLoadGridToExcel(this.uGridPMResult);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 인쇄
        public void mfPrint()
        {
            // 해당 없음
        }
        #endregion

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPMResult, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridPMResult_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        private void frmMAS0030_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }


    }
}
