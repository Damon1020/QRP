﻿namespace QRPMAS.UI
{
    partial class frmMAS0004
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0004));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridLineList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxProcessOrder = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridLineProcList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxLineInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUseFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextLineDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLineDesc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLineNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLineNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLineCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLineCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLineNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLineNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextLineName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelLineName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLineList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxProcessOrder)).BeginInit();
            this.uGroupBoxProcessOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLineProcList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLineInfo)).BeginInit();
            this.uGroupBoxLineInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineName)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1078, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uComboSearchPlant.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 19);
            this.uComboSearchPlant.TabIndex = 0;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            this.uLabelSearchPlant.Appearance = appearance17;
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uGridLineList
            // 
            this.uGridLineList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridLineList.DisplayLayout.Appearance = appearance2;
            this.uGridLineList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridLineList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLineList.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLineList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridLineList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLineList.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridLineList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridLineList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridLineList.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridLineList.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridLineList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridLineList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridLineList.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridLineList.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridLineList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridLineList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLineList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridLineList.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridLineList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridLineList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridLineList.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridLineList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridLineList.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridLineList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridLineList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridLineList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridLineList.Location = new System.Drawing.Point(0, 80);
            this.uGridLineList.Name = "uGridLineList";
            this.uGridLineList.Size = new System.Drawing.Size(1068, 760);
            this.uGridLineList.TabIndex = 0;
            this.uGridLineList.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridLineList_DoubleClickRow);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxProcessOrder);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBoxLineInfo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBoxProcessOrder
            // 
            this.uGroupBoxProcessOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxProcessOrder.Controls.Add(this.uGridLineProcList);
            this.uGroupBoxProcessOrder.Location = new System.Drawing.Point(4, 116);
            this.uGroupBoxProcessOrder.Name = "uGroupBoxProcessOrder";
            this.uGroupBoxProcessOrder.Size = new System.Drawing.Size(1044, 572);
            this.uGroupBoxProcessOrder.TabIndex = 0;
            // 
            // uGridLineProcList
            // 
            this.uGridLineProcList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridLineProcList.DisplayLayout.Appearance = appearance29;
            this.uGridLineProcList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridLineProcList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLineProcList.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLineProcList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance31;
            this.uGridLineProcList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance32.BackColor2 = System.Drawing.SystemColors.Control;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLineProcList.DisplayLayout.GroupByBox.PromptAppearance = appearance32;
            this.uGridLineProcList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridLineProcList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridLineProcList.DisplayLayout.Override.ActiveCellAppearance = appearance33;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridLineProcList.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.uGridLineProcList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridLineProcList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            this.uGridLineProcList.DisplayLayout.Override.CardAreaAppearance = appearance35;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            appearance36.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridLineProcList.DisplayLayout.Override.CellAppearance = appearance36;
            this.uGridLineProcList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridLineProcList.DisplayLayout.Override.CellPadding = 0;
            appearance37.BackColor = System.Drawing.SystemColors.Control;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLineProcList.DisplayLayout.Override.GroupByRowAppearance = appearance37;
            appearance38.TextHAlignAsString = "Left";
            this.uGridLineProcList.DisplayLayout.Override.HeaderAppearance = appearance38;
            this.uGridLineProcList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridLineProcList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            this.uGridLineProcList.DisplayLayout.Override.RowAppearance = appearance39;
            this.uGridLineProcList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridLineProcList.DisplayLayout.Override.TemplateAddRowAppearance = appearance40;
            this.uGridLineProcList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridLineProcList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridLineProcList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridLineProcList.Location = new System.Drawing.Point(12, 28);
            this.uGridLineProcList.Name = "uGridLineProcList";
            this.uGridLineProcList.Size = new System.Drawing.Size(1020, 536);
            this.uGridLineProcList.TabIndex = 0;
            // 
            // uGroupBoxLineInfo
            // 
            this.uGroupBoxLineInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxLineInfo.Controls.Add(this.uTextUseFlag);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextPlant);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextLineDesc);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelLineDesc);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelUseFlag);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextLineNameEn);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelLineNameEn);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextLineCode);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelLineCode);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextLineNameCh);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelLineNameCh);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextLineName);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelLineName);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxLineInfo.Location = new System.Drawing.Point(5, 3);
            this.uGroupBoxLineInfo.Name = "uGroupBoxLineInfo";
            this.uGroupBoxLineInfo.Size = new System.Drawing.Size(1047, 109);
            this.uGroupBoxLineInfo.TabIndex = 0;
            // 
            // uTextUseFlag
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseFlag.Appearance = appearance25;
            this.uTextUseFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseFlag.Location = new System.Drawing.Point(860, 76);
            this.uTextUseFlag.Name = "uTextUseFlag";
            this.uTextUseFlag.ReadOnly = true;
            this.uTextUseFlag.Size = new System.Drawing.Size(133, 21);
            this.uTextUseFlag.TabIndex = 9;
            // 
            // uTextPlant
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance26;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(116, 28);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(135, 21);
            this.uTextPlant.TabIndex = 8;
            // 
            // uTextLineDesc
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineDesc.Appearance = appearance18;
            this.uTextLineDesc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineDesc.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uTextLineDesc.Location = new System.Drawing.Point(116, 76);
            this.uTextLineDesc.Name = "uTextLineDesc";
            this.uTextLineDesc.ReadOnly = true;
            this.uTextLineDesc.Size = new System.Drawing.Size(629, 19);
            this.uTextLineDesc.TabIndex = 7;
            // 
            // uLabelLineDesc
            // 
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            this.uLabelLineDesc.Appearance = appearance15;
            this.uLabelLineDesc.Location = new System.Drawing.Point(12, 76);
            this.uLabelLineDesc.Name = "uLabelLineDesc";
            this.uLabelLineDesc.Size = new System.Drawing.Size(100, 20);
            this.uLabelLineDesc.TabIndex = 0;
            // 
            // uLabelUseFlag
            // 
            appearance16.TextHAlignAsString = "Left";
            appearance16.TextVAlignAsString = "Middle";
            this.uLabelUseFlag.Appearance = appearance16;
            this.uLabelUseFlag.Location = new System.Drawing.Point(756, 76);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelUseFlag.TabIndex = 0;
            // 
            // uTextLineNameEn
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineNameEn.Appearance = appearance27;
            this.uTextLineNameEn.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineNameEn.Location = new System.Drawing.Point(612, 52);
            this.uTextLineNameEn.Name = "uTextLineNameEn";
            this.uTextLineNameEn.ReadOnly = true;
            this.uTextLineNameEn.Size = new System.Drawing.Size(133, 21);
            this.uTextLineNameEn.TabIndex = 6;
            // 
            // uLabelLineNameEn
            // 
            appearance22.TextHAlignAsString = "Left";
            appearance22.TextVAlignAsString = "Middle";
            this.uLabelLineNameEn.Appearance = appearance22;
            this.uLabelLineNameEn.Location = new System.Drawing.Point(508, 52);
            this.uLabelLineNameEn.Name = "uLabelLineNameEn";
            this.uLabelLineNameEn.Size = new System.Drawing.Size(100, 20);
            this.uLabelLineNameEn.TabIndex = 0;
            // 
            // uTextLineCode
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineCode.Appearance = appearance24;
            this.uTextLineCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineCode.Location = new System.Drawing.Point(364, 28);
            this.uTextLineCode.Name = "uTextLineCode";
            this.uTextLineCode.ReadOnly = true;
            this.uTextLineCode.Size = new System.Drawing.Size(133, 21);
            this.uTextLineCode.TabIndex = 3;
            // 
            // uLabelLineCode
            // 
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            this.uLabelLineCode.Appearance = appearance46;
            this.uLabelLineCode.Location = new System.Drawing.Point(260, 28);
            this.uLabelLineCode.Name = "uLabelLineCode";
            this.uLabelLineCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelLineCode.TabIndex = 0;
            // 
            // uTextLineNameCh
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineNameCh.Appearance = appearance14;
            this.uTextLineNameCh.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineNameCh.Location = new System.Drawing.Point(364, 52);
            this.uTextLineNameCh.Name = "uTextLineNameCh";
            this.uTextLineNameCh.ReadOnly = true;
            this.uTextLineNameCh.Size = new System.Drawing.Size(133, 21);
            this.uTextLineNameCh.TabIndex = 5;
            // 
            // uLabelLineNameCh
            // 
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.uLabelLineNameCh.Appearance = appearance19;
            this.uLabelLineNameCh.Location = new System.Drawing.Point(260, 52);
            this.uLabelLineNameCh.Name = "uLabelLineNameCh";
            this.uLabelLineNameCh.Size = new System.Drawing.Size(100, 20);
            this.uLabelLineNameCh.TabIndex = 0;
            // 
            // uTextLineName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineName.Appearance = appearance23;
            this.uTextLineName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextLineName.Location = new System.Drawing.Point(116, 52);
            this.uTextLineName.Name = "uTextLineName";
            this.uTextLineName.ReadOnly = true;
            this.uTextLineName.Size = new System.Drawing.Size(135, 21);
            this.uTextLineName.TabIndex = 4;
            // 
            // uLabelLineName
            // 
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this.uLabelLineName.Appearance = appearance20;
            this.uLabelLineName.Location = new System.Drawing.Point(12, 52);
            this.uLabelLineName.Name = "uLabelLineName";
            this.uLabelLineName.Size = new System.Drawing.Size(100, 20);
            this.uLabelLineName.TabIndex = 0;
            // 
            // uLabelPlant
            // 
            appearance21.TextHAlignAsString = "Left";
            appearance21.TextVAlignAsString = "Middle";
            this.uLabelPlant.Appearance = appearance21;
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            // 
            // frmMAS0004
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridLineList);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0004";
            this.Load += new System.EventHandler(this.frmMAS0004_Load);
            this.Activated += new System.EventHandler(this.frmMAS0004_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0004_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLineList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxProcessOrder)).EndInit();
            this.uGroupBoxProcessOrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridLineProcList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLineInfo)).EndInit();
            this.uGroupBoxLineInfo.ResumeLayout(false);
            this.uGroupBoxLineInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLineName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridLineList;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxLineInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLineNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelLineNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLineCode;
        private Infragistics.Win.Misc.UltraLabel uLabelLineCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLineNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelLineNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLineName;
        private Infragistics.Win.Misc.UltraLabel uLabelLineName;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLineDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelLineDesc;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxProcessOrder;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridLineProcList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
    }
}