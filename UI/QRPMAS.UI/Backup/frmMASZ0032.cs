﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 금형치공구관리기준정보                                */
/* 프로그램ID   : frmMASZ0032.cs                                        */
/* 프로그램명   : 금형치공구수명관리                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-02-28                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMASZ0032 : Form,IToolbar
    {
        //리소스를 호출하기위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0032()
        {
            InitializeComponent();
        }

        private void frmMASZ0032_Activated(object sender, EventArgs e)
        {
            //Toolbar활성화
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            QRPBrowser brwChannel = new QRPBrowser();
            brwChannel.mfActiveToolBar(this.MdiParent, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0032_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            //grd.mfSaveGridColumnProperty
        }

        private void frmMASZ0032_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀지정
            titleArea.mfSetLabelText("금형치공구별 Spec", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitButton();
            InitLabel();
            InitGroupbox();
            InitCombo();
            InitGrid();

            this.uTextPlantCode.Hide();
            this.uTextDurableMatCode.Hide();
            this.uTextLotNo.Hide();
        }

        #region 초기화 메소드

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonMove, "↓", m_resSys.GetString("SYS_FONTNAME"), null);
                this.uButtonMove.Appearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                this.uButtonMove.Appearance.FontData.SizeInPoints = 12;
                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                this.uLabelSearchPlant.Hide();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// GroupBox 초기화
        /// </summary>
        private void InitGroupbox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGroupBox wGroupBox = new WinGroupBox();

                wGroupBox.mfSetGroupBox(this.uGroupDurable, GroupBoxType.LIST, "금형치공구 리스트", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                this.uGroupDurable.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupDurable.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                wGroupBox.mfSetGroupBox(this.uGroupBoxDurableMatLot, GroupBoxType.LIST, "수명관리 Spec 기준정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default
                    , Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                this.uGroupBoxDurableMatLot.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxDurableMatLot.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitCombo()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // 공장콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

                // Package
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);
                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "Package", "ComboName", dtPackage);
                
                // 금형치공구코드
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDMat);
                DataTable dtDurableMat = clsDMat.mfReadDurableMatNameCombo(m_resSys.GetString("SYS_PLANTCODE"), string.Empty, m_resSys.GetString("SYS_LANG"));
                wCombo.mfSetComboEditor(this.uComboSearchDurableMatName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "DurableMat", "DurableMatName", dtDurableMat);

                this.uComboSearchPlant.Hide();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 금형치공구 리스트

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableMat, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                ////wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "EMC", "EMC", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridDurableMat, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                //폰트설정
                this.uGridDurableMat.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableMat.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region SpecInfo List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableSpec, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDurableSpec, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurableSpec, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridDurableSpec, 0, "SpecCode", "Spec코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 30
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableSpec, 0, "SpecName", "Spec명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumnValueList(this.uGridDurableSpec, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                //폰트설정
                this.uGridDurableSpec.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableSpec.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region LotNoSpecInfo List

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridLotNoSpecInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "SpecCode", "Spec코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 30
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "SpecName", "Spec명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "UsageLimitLower", "Limit-1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "UsageLimit", "Limit", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "UsageLimitUpper", "Limit+1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0");

                wGrid.mfSetGridColumn(this.uGridLotNoSpecInfo, 0, "CurUsage", "CurUsage", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "{double:10.5}", "0");

                wGrid.mfSetGridColumnValueList(this.uGridLotNoSpecInfo, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                this.uGridLotNoSpecInfo.DisplayLayout.Bands[0].Columns["UsageLimitLower"].PromptChar = ' ';
                this.uGridLotNoSpecInfo.DisplayLayout.Bands[0].Columns["UsageLimit"].PromptChar = ' ';
                this.uGridLotNoSpecInfo.DisplayLayout.Bands[0].Columns["UsageLimitUpper"].PromptChar = ' ';

                //폰트설정
                this.uGridLotNoSpecInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridLotNoSpecInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region IToolbar 멤버

        // 신규
        public void mfCreate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 삭제
        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 엑셀
        public void mfExcel()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 출력
        public void mfPrint()
        {
            try
            {

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 저장
        public void mfSave()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();

                if (!(this.uGridLotNoSpecInfo.Rows.Count > 0))
                {
                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001034", "M001041", Infragistics.Win.HAlign.Right);

                    return;
                }
                string strLang = m_resSys.GetString("SYS_LANG");
                // 필수 입력값 확인
                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);
                for (int i = 0; i < this.uGridLotNoSpecInfo.Rows.Count; i++)
                {
                    if (!CheckInvalidValue(this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimitLower"]))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang), (i + 1) + msg.GetMessge_Text("M001287", strLang), Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else if (!CheckInvalidValue(this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimit"]))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang), (i + 1) + msg.GetMessge_Text("M001288", strLang), Infragistics.Win.HAlign.Right);

                        return;
                    }
                    else if (!CheckInvalidValue(this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimitUpper"]))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang), (i + 1) + msg.GetMessge_Text("M001286", strLang), Infragistics.Win.HAlign.Right);

                        return;
                    }
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 데이터 테이블 반환 메소드 호출
                    DataTable dtSaveLotSpecInfo = Rtn_Save_LotSpecInfoDate();
                    DataTable dtDelLotSpecInfo = Rtn_Delete_LotSpecInfoDate();
                    
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                    QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                    brwChannel.mfCredentials(clsLotSpecInfo);

                    string strErrRtn = clsLotSpecInfo.mfSaveLotNoSpecInfo_frmMASZ0032(dtSaveLotSpecInfo, dtDelLotSpecInfo, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (ErrRtn.ErrNum.Equals(0))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001135", "M001037", "M000930",
                                                            Infragistics.Win.HAlign.Right);

                        while (this.uGridDurableSpec.Rows.Count > 0)
                            this.uGridDurableSpec.Rows[0].Delete(false);

                        while (this.uGridLotNoSpecInfo.Rows.Count > 0)
                            this.uGridLotNoSpecInfo.Rows[0].Delete(false);

                        this.uTextPlantCode.Clear();
                        this.uTextDurableMatCode.Clear();
                        this.uTextLotNo.Clear();
                        this.uTextDurableMatName.Clear();
                    }
                    else
                    {
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001023", "M000953", Infragistics.Win.HAlign.Right);
                        }
                        else
                        {
                            Result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001023", ErrRtn.ErrMessage, Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 검색
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 변수 설정
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strDurableMatName = this.uComboSearchDurableMatName.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDLot);

                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                DataTable dtDLot = clsDLot.mfReadMASDurableLot_WithPackage(strPlantCode, strDurableMatName, strPackage, m_resSys.GetString("SYS_LANG"));
                this.uGridDurableMat.SetDataBinding(dtDLot, string.Empty);

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtDLot.Rows.Count <= 0)
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult Result = new DialogResult();

                    Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }


                while (this.uGridDurableSpec.Rows.Count > 0)
                    this.uGridDurableSpec.Rows[0].Delete(false);

                while (this.uGridLotNoSpecInfo.Rows.Count > 0)
                    this.uGridLotNoSpecInfo.Rows[0].Delete(false);

                this.uTextPlantCode.Clear();
                this.uTextDurableMatCode.Clear();
                this.uTextLotNo.Clear();
                this.uTextDurableMatName.Clear();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Events

        // 공장콤보값 변경시 이벤트
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPBrowser brwChannel = new QRPBrowser();
                WinComboEditor wCombo = new WinComboEditor();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                DataTable dtPackage = new DataTable();
                DataTable dtDurableMat = new DataTable();

                this.uComboSearchPackage.Items.Clear();
                this.uComboSearchDurableMatName.Items.Clear();

                if (!strPlantCode.Equals(string.Empty))
                {
                    // Package
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                    QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                    brwChannel.mfCredentials(clsProduct);
                    dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    // 금형치공구코드
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsDMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsDMat);
                    dtDurableMat = clsDMat.mfReadDurableMatNameCombo(strPlantCode, string.Empty, m_resSys.GetString("SYS_LANG"));
                }

                // Package
                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                        , "", "", "전체", "Package", "ComboName", dtPackage);

                // 금형치공구코드
                wCombo.mfSetComboEditor(this.uComboSearchDurableMatName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                        , "", "", "전체", "DurableMat", "DurableMatName", dtDurableMat);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 이동버튼 클릭 이벤트
        private void uButtonMove_Click(object sender, EventArgs e)
        {
            try
            {
                // 키 입력여부 확인
                if (!this.uTextPlantCode.Text.Equals(string.Empty) &&
                    !this.uTextDurableMatCode.Text.Equals(string.Empty) &&
                    !this.uTextLotNo.Text.Equals(string.Empty))
                {
                    // 테이블 복제
                    DataTable dtLotNoSpecInfo = (DataTable)this.uGridLotNoSpecInfo.DataSource;
                    // 기본키 값 설정
                    //dtLotNoSpecInfo.Columns["PlantCode"].DefaultValue = this.uTextPlantCode.Text;
                    dtLotNoSpecInfo.Columns["DurableMatCode"].DefaultValue = this.uTextDurableMatCode.Text;
                    dtLotNoSpecInfo.Columns["LotNo"].DefaultValue = this.uTextLotNo.Text;

                    for (int i = 0; i < this.uGridDurableSpec.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridDurableSpec.Rows[i].Cells["Check"].Value))
                        {
                            // Check 된 행이 기존에 저장된 값일경우
                            DataRow[] _drs = dtLotNoSpecInfo.Select("SpecCode = '" + this.uGridDurableSpec.Rows[i].Cells["SpecCode"].Value.ToString() + "'");
                            if (_drs.Length > 0)
                            {
                                //foreach (DataRow dr in _drs)
                                //{
                                //    dtLotNoSpecInfo.Rows.Add(dr);
                                //}
                            }
                            else
                            {
                                // Check 된 행이 기존에 저장된 값이 아닐경우
                                DataRow dr = dtLotNoSpecInfo.NewRow();
                                dr["SpecCode"] = this.uGridDurableSpec.Rows[i].Cells["SpecCode"].Value.ToString();
                                dr["SpecName"] = this.uGridDurableSpec.Rows[i].Cells["SpecName"].Value.ToString();
                                dr["UsageLimitLower"] = DBNull.Value;
                                dr["UsageLimit"] = DBNull.Value;
                                dr["UsageLimitUpper"] = DBNull.Value;
                                dr["CurUsage"] = 0.0m;
                                dtLotNoSpecInfo.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            // Check 안된 행이 기존에 저장된 값일경우
                            DataRow[] _drs = dtLotNoSpecInfo.Select("SpecCode = '" + this.uGridDurableSpec.Rows[i].Cells["SpecCode"].Value.ToString() + "'");
                            if (_drs.Length > 0)
                            {
                                DataTable dtRow = _drs.CopyToDataTable();

                                if (dtRow.Rows[0]["PlantCode"].ToString().Equals(string.Empty))
                                {
                                    foreach (DataRow dr in _drs)
                                    {

                                        dtLotNoSpecInfo.Rows.Remove(dr);
                                    }
                                }
                            }
                        }
                    }

                    this.uGridLotNoSpecInfo.SetDataBinding(dtLotNoSpecInfo, string.Empty);
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 금형치공구 리스트 더블클릭 이벤트
        private void uGridDurableMat_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // 프로그래스바 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 변수
                string strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                string strDurableMatCode = e.Row.Cells["DurableMatCode"].Value.ToString();
                string strLotNo = e.Row.Cells["LotNo"].Value.ToString();

                string strPackage = e.Row.Cells["Package"].Value.ToString();
                string strEMC = e.Row.Cells["EMC"].Value.ToString();

                this.uTextPlantCode.Clear();
                this.uTextDurableMatCode.Clear();
                this.uTextLotNo.Clear();
                this.uTextDurableMatName.Clear();

                this.uTextPlantCode.Text = strPlantCode;
                this.uTextDurableMatCode.Text = strDurableMatCode;
                this.uTextLotNo.Text = strLotNo;
                this.uTextDurableMatName.Text = e.Row.Cells["DurableMatName"].Value.ToString();

                // BL 연결
                // DurableSpec
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableSpec), "DurableSpec");
                QRPMAS.BL.MASDMM.DurableSpec clsDSpec = new QRPMAS.BL.MASDMM.DurableSpec();
                brwChannel.mfCredentials(clsDSpec);

                // LotNoSpecInfo
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                brwChannel.mfCredentials(clsLotSpecInfo);

                DataTable dtDurableSpecList = clsDSpec.mfReadMASDurableSpec(strPlantCode);
                this.uGridDurableSpec.SetDataBinding(dtDurableSpecList, string.Empty);

                // Data조회
                DataTable dtLotSpecInfo = clsLotSpecInfo.mfReadLotNoSpecInfo_Detail(strPlantCode, strDurableMatCode, strLotNo, strPackage, strEMC);
                // Data Binding
                this.uGridLotNoSpecInfo.SetDataBinding(dtLotSpecInfo, string.Empty);

                ////// DurableSpec Grid Check 해제
                ////WinGrid wGrid = new WinGrid();
                ////wGrid.mfSetAllUnCheckedGridColumn(this.uGridDurableSpec, 0, "Check");
                ////wGrid.mfClearRowSeletorGrid(this.uGridDurableSpec);

                // POPUP창 Close
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // LotNoSpecInfo List 업데이트 이벤트
        private void uGridLotNoSpecInfo_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Value == null || e.Cell.Value == DBNull.Value)
                    return;

                if (e.Cell.Column.Key.Equals("UsageLimitLower"))
                {
                    if(!(ReturnDecimalValue(e.Cell.Value.ToString()) > 0))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001307", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        e.Cell.Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }
                    else
                    {
                        //CheckLimitValue(e);

                        if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimit"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001335", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimitLower"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimitLower"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                    }
                }
                else if (e.Cell.Column.Key.Equals("UsageLimit"))
                {
                    if (!(ReturnDecimalValue(e.Cell.Value.ToString()) > 0))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001308", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        e.Cell.Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }
                    else
                    {
                        //CheckLimitValue(e);
                        if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimitLower"]) && CheckInvalidValue(e.Cell.Row.Cells["UsageLimitUpper"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) &&
                                ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001337", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                            else if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) &&
                                    ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001336", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                            else if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) &&
                                    ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001338", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                        else if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimitLower"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001337", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                        else if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimitUpper"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001336", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimit"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                        
                    }
                }
                else if (e.Cell.Column.Key.Equals("UsageLimitUpper"))
                {
                    if (!(ReturnDecimalValue(e.Cell.Value.ToString()) > 0))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001306", Infragistics.Win.HAlign.Right);

                        e.Cell.Value = e.Cell.OriginalValue;
                        e.Cell.Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        return;
                    }
                    else
                    {
                        //CheckLimitValue(e);
                        if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimit"]))
                        {
                            if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                            {
                                // SystemInfo ResourceSet
                                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                                WinMessageBox msg = new WinMessageBox();
                                DialogResult Result = new DialogResult();

                                Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001228", "M001334", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Cells["UsageLimitUpper"].Value = DBNull.Value;
                                e.Cell.Row.Cells["UsageLimitUpper"].Activate();
                                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// 값 유효성 체크
        /// </summary>
        /// <param name="uCell">값 체크할 GridCell</param>
        /// <returns></returns>
        private bool CheckInvalidValue(Infragistics.Win.UltraWinGrid.UltraGridCell uCell)
        {
            try
            {
                if (string.IsNullOrEmpty(uCell.Value.ToString()) || uCell.Value == DBNull.Value)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Decimal 반환 메소드(실패시 0반환)
        /// </summary>
        /// <param name="value">decimal로 반환받을 값</param>
        /// <returns></returns>
        private decimal ReturnDecimalValue(string value)
        {
            decimal result = 0.0m;

            if (decimal.TryParse(value, out result))
                return result;
            else
                return 0.0m; ;
        }

        /// <summary>
        /// Litmit 값의 크기 비교함수
        /// </summary>
        /// <param name="e"></param>
        private void CheckLimitValue(Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (CheckInvalidValue(e.Cell.Row.Cells["UsageLimitLower"]) &&
                    CheckInvalidValue(e.Cell.Row.Cells["UsageLimit"]) &&
                    CheckInvalidValue(e.Cell.Row.Cells["UsageLimitUpper"]))
                {
                    if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitUpper"].Value.ToString()))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001334", Infragistics.Win.HAlign.Right);

                        //this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        e.Cell.Row.Cells["UsageLimitUpper"].Value = DBNull.Value;
                        e.Cell.Row.Cells["UsageLimitUpper"].Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                    if (ReturnDecimalValue(e.Cell.Row.Cells["UsageLimitLower"].Value.ToString()) >= ReturnDecimalValue(e.Cell.Row.Cells["UsageLimit"].Value.ToString()))
                    {
                        // SystemInfo ResourceSet
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();
                        DialogResult Result = new DialogResult();

                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001228", "M001337", Infragistics.Win.HAlign.Right);

                        //this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.PrevCell);
                        e.Cell.Row.Cells["UsageLimit"].Value = DBNull.Value;
                        e.Cell.Row.Cells["UsageLimit"].Activate();
                        this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    }
                }
                else
                    return;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장용 데이터 테이블 반환 메소드
        /// </summary>
        private DataTable Rtn_Save_LotSpecInfoDate()
        {
            DataTable dtLotSpecInfo = new DataTable();
            try
            {
                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                brwChannel.mfCredentials(clsLotSpecInfo);

                dtLotSpecInfo = clsLotSpecInfo.mfSetDataInfo_frmMASZ0032();

                for (int i = 0; i < this.uGridLotNoSpecInfo.Rows.Count; i++)
                {
                    if (!Convert.ToBoolean(this.uGridLotNoSpecInfo.Rows[i].Cells["Check"].Value))
                    {
                        DataRow drRow = dtLotSpecInfo.NewRow();
                        drRow["PlantCode"] = this.uTextPlantCode.Text;
                        drRow["DurableMatCode"] = this.uTextDurableMatCode.Text;
                        drRow["DurableMatName"] = this.uTextDurableMatName.Text;
                        drRow["LotNo"] = this.uTextLotNo.Text;
                        drRow["SpecCode"] = this.uGridLotNoSpecInfo.Rows[i].Cells["SpecCode"].Value.ToString();
                        drRow["UsageLimitLower"] = this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimitLower"].Value;
                        drRow["UsageLimit"] = this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimit"].Value;
                        drRow["UsageLimitUpper"] = this.uGridLotNoSpecInfo.Rows[i].Cells["UsageLimitUpper"].Value;
                        dtLotSpecInfo.Rows.Add(drRow);
                    }
                }

                return dtLotSpecInfo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtLotSpecInfo;
            }
            finally
            {
                dtLotSpecInfo.Dispose();
            }
        }

        /// <summary>
        /// 삭제용 데이터 테이블 반환 메소드
        /// </summary>
        private DataTable Rtn_Delete_LotSpecInfoDate()
        {
            DataTable dtLotSpecInfo = new DataTable();
            try
            {
                this.uGridLotNoSpecInfo.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.ExitEditMode);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.LotNoSpecInfo), "LotNoSpecInfo");
                QRPMAS.BL.MASDMM.LotNoSpecInfo clsLotSpecInfo = new QRPMAS.BL.MASDMM.LotNoSpecInfo();
                brwChannel.mfCredentials(clsLotSpecInfo);

                dtLotSpecInfo = clsLotSpecInfo.mfSetDataInfo_frmMASZ0032();

                for (int i = 0; i < this.uGridLotNoSpecInfo.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridLotNoSpecInfo.Rows[i].Cells["Check"].Value))
                    {
                        DataRow drRow = dtLotSpecInfo.NewRow();
                        drRow["PlantCode"] = this.uTextPlantCode.Text;
                        drRow["DurableMatCode"] = this.uTextDurableMatCode.Text;
                        drRow["LotNo"] = this.uTextLotNo.Text;
                        drRow["SpecCode"] = this.uGridLotNoSpecInfo.Rows[i].Cells["SpecCode"].Value.ToString();
                        dtLotSpecInfo.Rows.Add(drRow);
                    }
                }

                return dtLotSpecInfo;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return dtLotSpecInfo;
            }
            finally
            {
                dtLotSpecInfo.Dispose();
            }
        }

        #endregion

        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridLotNoSpecInfo.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridLotNoSpecInfo.Rows[i].Cells["Check"].Value))
                        this.uGridLotNoSpecInfo.Rows[i].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
