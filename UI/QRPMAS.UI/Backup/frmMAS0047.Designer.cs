﻿namespace QRPMAS.UI
{
    partial class frmMAS0047
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0047));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProductActionType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProductActionType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchICustomerProductSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomer = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchICustomerProductSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPacakge = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchCustomer = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPACKAGEGROUP_5 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPACKAGEGROUP_4 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPACKAGEGROUP_3 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPACKAGEGROUP_2 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboPACKAGEGROUP_1 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPACKAGEGROUP_5 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPACKAGEGROUP_4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPACKAGEGROUP_3 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPACKAGEGROUP_2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPACKAGEGROUP_1 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMaterialType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMaterialType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMaterialGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMaterialGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uGridProduct = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextFamily = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFamily = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPackageType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPackageGroup = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPackageType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPackageGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uTextUseFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialType = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextMaterialGroup = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUseflag = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMaterialType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelMaterialGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextProductCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelProductCode = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProductActionType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchICustomerProductSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPacakge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFamily)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProductActionType);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchICustomerProductSpec);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchICustomerProductSpec);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPacakge);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchCustomer);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPACKAGEGROUP_5);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPACKAGEGROUP_4);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPACKAGEGROUP_3);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPACKAGEGROUP_2);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPACKAGEGROUP_1);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPACKAGEGROUP_5);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPACKAGEGROUP_4);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPACKAGEGROUP_3);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPACKAGEGROUP_2);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPACKAGEGROUP_1);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboProductActionType
            // 
            this.uComboProductActionType.Location = new System.Drawing.Point(116, 36);
            this.uComboProductActionType.MaxLength = 40;
            this.uComboProductActionType.Name = "uComboProductActionType";
            this.uComboProductActionType.Size = new System.Drawing.Size(132, 21);
            this.uComboProductActionType.TabIndex = 12;
            // 
            // uLabelProductActionType
            // 
            this.uLabelProductActionType.Location = new System.Drawing.Point(12, 36);
            this.uLabelProductActionType.Name = "uLabelProductActionType";
            this.uLabelProductActionType.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductActionType.TabIndex = 11;
            // 
            // uTextSearchICustomerProductSpec
            // 
            this.uTextSearchICustomerProductSpec.Location = new System.Drawing.Point(892, 12);
            this.uTextSearchICustomerProductSpec.MaxLength = 40;
            this.uTextSearchICustomerProductSpec.Name = "uTextSearchICustomerProductSpec";
            this.uTextSearchICustomerProductSpec.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchICustomerProductSpec.TabIndex = 4;
            // 
            // uTextCustomer
            // 
            appearance26.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance26.TextHAlignAsString = "Center";
            editorButton1.Appearance = appearance26;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextCustomer.ButtonsRight.Add(editorButton1);
            this.uTextCustomer.Location = new System.Drawing.Point(379, 11);
            this.uTextCustomer.MaxLength = 20;
            this.uTextCustomer.Name = "uTextCustomer";
            this.uTextCustomer.Size = new System.Drawing.Size(108, 21);
            this.uTextCustomer.TabIndex = 2;
            this.uTextCustomer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextCustomer_KeyDown);
            this.uTextCustomer.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextCustomer_EditorButtonClick);
            // 
            // uLabelSearchICustomerProductSpec
            // 
            this.uLabelSearchICustomerProductSpec.Location = new System.Drawing.Point(776, 12);
            this.uLabelSearchICustomerProductSpec.Name = "uLabelSearchICustomerProductSpec";
            this.uLabelSearchICustomerProductSpec.Size = new System.Drawing.Size(112, 20);
            this.uLabelSearchICustomerProductSpec.TabIndex = 10;
            // 
            // uComboSearchPacakge
            // 
            this.uComboSearchPacakge.Location = new System.Drawing.Point(612, 12);
            this.uComboSearchPacakge.MaxLength = 40;
            this.uComboSearchPacakge.Name = "uComboSearchPacakge";
            this.uComboSearchPacakge.Size = new System.Drawing.Size(148, 21);
            this.uComboSearchPacakge.TabIndex = 3;
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(508, 12);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPackage.TabIndex = 8;
            // 
            // uLabelSearchCustomer
            // 
            this.uLabelSearchCustomer.Location = new System.Drawing.Point(275, 11);
            this.uLabelSearchCustomer.Name = "uLabelSearchCustomer";
            this.uLabelSearchCustomer.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchCustomer.TabIndex = 10;
            // 
            // uComboPACKAGEGROUP_5
            // 
            this.uComboPACKAGEGROUP_5.Location = new System.Drawing.Point(1040, 36);
            this.uComboPACKAGEGROUP_5.MaxLength = 40;
            this.uComboPACKAGEGROUP_5.Name = "uComboPACKAGEGROUP_5";
            this.uComboPACKAGEGROUP_5.Size = new System.Drawing.Size(16, 21);
            this.uComboPACKAGEGROUP_5.TabIndex = 9;
            this.uComboPACKAGEGROUP_5.Visible = false;
            // 
            // uComboPACKAGEGROUP_4
            // 
            this.uComboPACKAGEGROUP_4.Location = new System.Drawing.Point(1040, 8);
            this.uComboPACKAGEGROUP_4.MaxLength = 40;
            this.uComboPACKAGEGROUP_4.Name = "uComboPACKAGEGROUP_4";
            this.uComboPACKAGEGROUP_4.Size = new System.Drawing.Size(12, 21);
            this.uComboPACKAGEGROUP_4.TabIndex = 8;
            this.uComboPACKAGEGROUP_4.Visible = false;
            // 
            // uComboPACKAGEGROUP_3
            // 
            this.uComboPACKAGEGROUP_3.Location = new System.Drawing.Point(892, 36);
            this.uComboPACKAGEGROUP_3.MaxLength = 40;
            this.uComboPACKAGEGROUP_3.Name = "uComboPACKAGEGROUP_3";
            this.uComboPACKAGEGROUP_3.Size = new System.Drawing.Size(100, 21);
            this.uComboPACKAGEGROUP_3.TabIndex = 7;
            // 
            // uComboPACKAGEGROUP_2
            // 
            this.uComboPACKAGEGROUP_2.Location = new System.Drawing.Point(612, 36);
            this.uComboPACKAGEGROUP_2.MaxLength = 40;
            this.uComboPACKAGEGROUP_2.Name = "uComboPACKAGEGROUP_2";
            this.uComboPACKAGEGROUP_2.Size = new System.Drawing.Size(148, 21);
            this.uComboPACKAGEGROUP_2.TabIndex = 6;
            // 
            // uComboPACKAGEGROUP_1
            // 
            this.uComboPACKAGEGROUP_1.Location = new System.Drawing.Point(380, 36);
            this.uComboPACKAGEGROUP_1.MaxLength = 40;
            this.uComboPACKAGEGROUP_1.Name = "uComboPACKAGEGROUP_1";
            this.uComboPACKAGEGROUP_1.Size = new System.Drawing.Size(104, 21);
            this.uComboPACKAGEGROUP_1.TabIndex = 5;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(132, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelPACKAGEGROUP_5
            // 
            this.uLabelPACKAGEGROUP_5.Location = new System.Drawing.Point(1024, 36);
            this.uLabelPACKAGEGROUP_5.Name = "uLabelPACKAGEGROUP_5";
            this.uLabelPACKAGEGROUP_5.Size = new System.Drawing.Size(16, 20);
            this.uLabelPACKAGEGROUP_5.TabIndex = 4;
            this.uLabelPACKAGEGROUP_5.Visible = false;
            // 
            // uLabelPACKAGEGROUP_4
            // 
            this.uLabelPACKAGEGROUP_4.Location = new System.Drawing.Point(1020, 8);
            this.uLabelPACKAGEGROUP_4.Name = "uLabelPACKAGEGROUP_4";
            this.uLabelPACKAGEGROUP_4.Size = new System.Drawing.Size(12, 20);
            this.uLabelPACKAGEGROUP_4.TabIndex = 4;
            this.uLabelPACKAGEGROUP_4.Visible = false;
            // 
            // uLabelPACKAGEGROUP_3
            // 
            this.uLabelPACKAGEGROUP_3.Location = new System.Drawing.Point(776, 36);
            this.uLabelPACKAGEGROUP_3.Name = "uLabelPACKAGEGROUP_3";
            this.uLabelPACKAGEGROUP_3.Size = new System.Drawing.Size(112, 20);
            this.uLabelPACKAGEGROUP_3.TabIndex = 4;
            // 
            // uLabelPACKAGEGROUP_2
            // 
            this.uLabelPACKAGEGROUP_2.Location = new System.Drawing.Point(508, 36);
            this.uLabelPACKAGEGROUP_2.Name = "uLabelPACKAGEGROUP_2";
            this.uLabelPACKAGEGROUP_2.Size = new System.Drawing.Size(100, 20);
            this.uLabelPACKAGEGROUP_2.TabIndex = 4;
            // 
            // uLabelPACKAGEGROUP_1
            // 
            this.uLabelPACKAGEGROUP_1.Location = new System.Drawing.Point(276, 36);
            this.uLabelPACKAGEGROUP_1.Name = "uLabelPACKAGEGROUP_1";
            this.uLabelPACKAGEGROUP_1.Size = new System.Drawing.Size(100, 20);
            this.uLabelPACKAGEGROUP_1.TabIndex = 4;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 4;
            // 
            // uComboSearchMaterialType
            // 
            this.uComboSearchMaterialType.Location = new System.Drawing.Point(880, 620);
            this.uComboSearchMaterialType.Name = "uComboSearchMaterialType";
            this.uComboSearchMaterialType.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchMaterialType.TabIndex = 3;
            this.uComboSearchMaterialType.Text = "ultraComboEditor2";
            this.uComboSearchMaterialType.Visible = false;
            // 
            // uLabelSearchMaterialType
            // 
            this.uLabelSearchMaterialType.Location = new System.Drawing.Point(776, 620);
            this.uLabelSearchMaterialType.Name = "uLabelSearchMaterialType";
            this.uLabelSearchMaterialType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterialType.TabIndex = 2;
            this.uLabelSearchMaterialType.Text = "ultraLabel2";
            this.uLabelSearchMaterialType.Visible = false;
            // 
            // uComboSearchMaterialGroup
            // 
            this.uComboSearchMaterialGroup.Location = new System.Drawing.Point(624, 620);
            this.uComboSearchMaterialGroup.Name = "uComboSearchMaterialGroup";
            this.uComboSearchMaterialGroup.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchMaterialGroup.TabIndex = 1;
            this.uComboSearchMaterialGroup.Text = "ultraComboEditor1";
            this.uComboSearchMaterialGroup.Visible = false;
            // 
            // uLabelSearchMaterialGroup
            // 
            this.uLabelSearchMaterialGroup.Location = new System.Drawing.Point(520, 620);
            this.uLabelSearchMaterialGroup.Name = "uLabelSearchMaterialGroup";
            this.uLabelSearchMaterialGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterialGroup.TabIndex = 0;
            this.uLabelSearchMaterialGroup.Text = "ultraLabel1";
            this.uLabelSearchMaterialGroup.Visible = false;
            // 
            // uGridProduct
            // 
            this.uGridProduct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridProduct.DisplayLayout.Appearance = appearance2;
            this.uGridProduct.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridProduct.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProduct.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProduct.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridProduct.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridProduct.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridProduct.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridProduct.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridProduct.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridProduct.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridProduct.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridProduct.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridProduct.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridProduct.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridProduct.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridProduct.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridProduct.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridProduct.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridProduct.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridProduct.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridProduct.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridProduct.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridProduct.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridProduct.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridProduct.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridProduct.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridProduct.Location = new System.Drawing.Point(0, 100);
            this.uGridProduct.Name = "uGridProduct";
            this.uGridProduct.Size = new System.Drawing.Size(1070, 720);
            this.uGridProduct.TabIndex = 3;
            this.uGridProduct.Text = "ultraGrid1";
            this.uGridProduct.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridproduct_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 745);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 745);
            this.uGroupBoxContentsArea.TabIndex = 4;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextFamily);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelFamily);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPackageType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPackageGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboSearchMaterialType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPackageType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSearchMaterialType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPackageGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboSearchMaterialGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSpec);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSearchMaterialGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSpec);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseflag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProductNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProductNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProductName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProductName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProductNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProductNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextProductCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelProductCode);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 725);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextFamily
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFamily.Appearance = appearance23;
            this.uTextFamily.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextFamily.Location = new System.Drawing.Point(116, 60);
            this.uTextFamily.Name = "uTextFamily";
            this.uTextFamily.ReadOnly = true;
            this.uTextFamily.Size = new System.Drawing.Size(372, 21);
            this.uTextFamily.TabIndex = 28;
            // 
            // uLabelFamily
            // 
            this.uLabelFamily.Location = new System.Drawing.Point(12, 60);
            this.uLabelFamily.Name = "uLabelFamily";
            this.uLabelFamily.Size = new System.Drawing.Size(100, 20);
            this.uLabelFamily.TabIndex = 27;
            // 
            // uTextPackageType
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageType.Appearance = appearance24;
            this.uTextPackageType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageType.Location = new System.Drawing.Point(620, 36);
            this.uTextPackageType.Name = "uTextPackageType";
            this.uTextPackageType.ReadOnly = true;
            this.uTextPackageType.Size = new System.Drawing.Size(372, 21);
            this.uTextPackageType.TabIndex = 26;
            // 
            // uTextPackageGroup
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageGroup.Appearance = appearance19;
            this.uTextPackageGroup.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPackageGroup.Location = new System.Drawing.Point(116, 36);
            this.uTextPackageGroup.Name = "uTextPackageGroup";
            this.uTextPackageGroup.ReadOnly = true;
            this.uTextPackageGroup.Size = new System.Drawing.Size(372, 21);
            this.uTextPackageGroup.TabIndex = 25;
            // 
            // uLabelPackageType
            // 
            this.uLabelPackageType.Location = new System.Drawing.Point(516, 36);
            this.uLabelPackageType.Name = "uLabelPackageType";
            this.uLabelPackageType.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackageType.TabIndex = 24;
            // 
            // uLabelPackageGroup
            // 
            this.uLabelPackageGroup.Location = new System.Drawing.Point(12, 36);
            this.uLabelPackageGroup.Name = "uLabelPackageGroup";
            this.uLabelPackageGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelPackageGroup.TabIndex = 23;
            // 
            // uTextSpec
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Appearance = appearance16;
            this.uTextSpec.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSpec.Location = new System.Drawing.Point(116, 132);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.ReadOnly = true;
            this.uTextSpec.Size = new System.Drawing.Size(372, 21);
            this.uTextSpec.TabIndex = 22;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(12, 132);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(100, 20);
            this.uLabelSpec.TabIndex = 21;
            // 
            // uTextUseFlag
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseFlag.Appearance = appearance14;
            this.uTextUseFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseFlag.Location = new System.Drawing.Point(620, 132);
            this.uTextUseFlag.Name = "uTextUseFlag";
            this.uTextUseFlag.ReadOnly = true;
            this.uTextUseFlag.Size = new System.Drawing.Size(140, 21);
            this.uTextUseFlag.TabIndex = 20;
            // 
            // uTextMaterialType
            // 
            appearance20.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialType.Appearance = appearance20;
            this.uTextMaterialType.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialType.Location = new System.Drawing.Point(620, 208);
            this.uTextMaterialType.Name = "uTextMaterialType";
            this.uTextMaterialType.ReadOnly = true;
            this.uTextMaterialType.Size = new System.Drawing.Size(372, 21);
            this.uTextMaterialType.TabIndex = 19;
            this.uTextMaterialType.Visible = false;
            // 
            // uTextMaterialGroup
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialGroup.Appearance = appearance21;
            this.uTextMaterialGroup.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextMaterialGroup.Location = new System.Drawing.Point(116, 208);
            this.uTextMaterialGroup.Name = "uTextMaterialGroup";
            this.uTextMaterialGroup.ReadOnly = true;
            this.uTextMaterialGroup.Size = new System.Drawing.Size(372, 21);
            this.uTextMaterialGroup.TabIndex = 18;
            this.uTextMaterialGroup.Visible = false;
            // 
            // uTextPlant
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance22;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(116, 12);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(140, 21);
            this.uTextPlant.TabIndex = 17;
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 16;
            // 
            // uLabelUseflag
            // 
            this.uLabelUseflag.Location = new System.Drawing.Point(516, 132);
            this.uLabelUseflag.Name = "uLabelUseflag";
            this.uLabelUseflag.Size = new System.Drawing.Size(100, 20);
            this.uLabelUseflag.TabIndex = 10;
            // 
            // uLabelMaterialType
            // 
            this.uLabelMaterialType.Location = new System.Drawing.Point(516, 208);
            this.uLabelMaterialType.Name = "uLabelMaterialType";
            this.uLabelMaterialType.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterialType.TabIndex = 9;
            this.uLabelMaterialType.Visible = false;
            // 
            // uLabelMaterialGroup
            // 
            this.uLabelMaterialGroup.Location = new System.Drawing.Point(12, 208);
            this.uLabelMaterialGroup.Name = "uLabelMaterialGroup";
            this.uLabelMaterialGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelMaterialGroup.TabIndex = 8;
            this.uLabelMaterialGroup.Visible = false;
            // 
            // uTextProductNameEn
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductNameEn.Appearance = appearance17;
            this.uTextProductNameEn.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductNameEn.Location = new System.Drawing.Point(620, 108);
            this.uTextProductNameEn.Name = "uTextProductNameEn";
            this.uTextProductNameEn.ReadOnly = true;
            this.uTextProductNameEn.Size = new System.Drawing.Size(372, 21);
            this.uTextProductNameEn.TabIndex = 7;
            // 
            // uLabelProductNameEn
            // 
            this.uLabelProductNameEn.Location = new System.Drawing.Point(516, 108);
            this.uLabelProductNameEn.Name = "uLabelProductNameEn";
            this.uLabelProductNameEn.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductNameEn.TabIndex = 6;
            // 
            // uTextProductName
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Appearance = appearance15;
            this.uTextProductName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductName.Location = new System.Drawing.Point(620, 84);
            this.uTextProductName.Name = "uTextProductName";
            this.uTextProductName.ReadOnly = true;
            this.uTextProductName.Size = new System.Drawing.Size(372, 21);
            this.uTextProductName.TabIndex = 5;
            // 
            // uLabelProductName
            // 
            this.uLabelProductName.Location = new System.Drawing.Point(516, 84);
            this.uLabelProductName.Name = "uLabelProductName";
            this.uLabelProductName.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductName.TabIndex = 4;
            // 
            // uTextProductNameCh
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductNameCh.Appearance = appearance18;
            this.uTextProductNameCh.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductNameCh.Location = new System.Drawing.Point(116, 108);
            this.uTextProductNameCh.Name = "uTextProductNameCh";
            this.uTextProductNameCh.ReadOnly = true;
            this.uTextProductNameCh.Size = new System.Drawing.Size(372, 21);
            this.uTextProductNameCh.TabIndex = 3;
            // 
            // uLabelProductNameCh
            // 
            this.uLabelProductNameCh.Location = new System.Drawing.Point(12, 108);
            this.uLabelProductNameCh.Name = "uLabelProductNameCh";
            this.uLabelProductNameCh.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductNameCh.TabIndex = 2;
            // 
            // uTextProductCode
            // 
            appearance25.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Appearance = appearance25;
            this.uTextProductCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextProductCode.Location = new System.Drawing.Point(116, 84);
            this.uTextProductCode.Name = "uTextProductCode";
            this.uTextProductCode.ReadOnly = true;
            this.uTextProductCode.Size = new System.Drawing.Size(372, 21);
            this.uTextProductCode.TabIndex = 1;
            // 
            // uLabelProductCode
            // 
            this.uLabelProductCode.Location = new System.Drawing.Point(12, 84);
            this.uLabelProductCode.Name = "uLabelProductCode";
            this.uLabelProductCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelProductCode.TabIndex = 0;
            // 
            // frmMAS0047
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridProduct);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0047";
            this.Load += new System.EventHandler(this.frmMAS0047_Load);
            this.Activated += new System.EventHandler(this.frmMAS0047_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProductActionType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchICustomerProductSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPacakge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPACKAGEGROUP_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFamily)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPackageGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextProductCode)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialGroup;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridProduct;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialGroup;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelUseflag;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialType;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialGroup;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelProductNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductName;
        private Infragistics.Win.Misc.UltraLabel uLabelProductName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelProductNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextProductCode;
        private Infragistics.Win.Misc.UltraLabel uLabelProductCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPacakge;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFamily;
        private Infragistics.Win.Misc.UltraLabel uLabelFamily;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackageType;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPackageGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelPackageType;
        private Infragistics.Win.Misc.UltraLabel uLabelPackageGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchCustomer;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchICustomerProductSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchICustomerProductSpec;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomer;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPACKAGEGROUP_2;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPACKAGEGROUP_1;
        private Infragistics.Win.Misc.UltraLabel uLabelPACKAGEGROUP_5;
        private Infragistics.Win.Misc.UltraLabel uLabelPACKAGEGROUP_4;
        private Infragistics.Win.Misc.UltraLabel uLabelPACKAGEGROUP_3;
        private Infragistics.Win.Misc.UltraLabel uLabelPACKAGEGROUP_2;
        private Infragistics.Win.Misc.UltraLabel uLabelPACKAGEGROUP_1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPACKAGEGROUP_5;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPACKAGEGROUP_4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPACKAGEGROUP_3;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProductActionType;
        private Infragistics.Win.Misc.UltraLabel uLabelProductActionType;
    }
}