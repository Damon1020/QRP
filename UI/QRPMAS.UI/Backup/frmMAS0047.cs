﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통기준정보                                          */
/* 모듈(분류)명 : 자재관리 기준정보                                     */
/* 프로그램ID   : frmMAS0047.cs                                         */
/* 프로그램명   : 제품정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-25                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0047 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0047()
        {
            InitializeComponent();
        }

        private void frmMAS0047_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드

            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            titleArea.mfSetLabelText("제품정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method 호출
            SetToolAuth();
            InitGrid();
            InitCombo();
            InitLabel();

            // 그룹박스 접힌 상태로 만듬
            this.uGroupBoxContentsArea.Expanded = false;
            this.uGroupBoxContentsArea.Visible = false;
        }

        private void frmMAS0047_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                // 서버 연결 체크 //
                if (brwChannel.mfCheckQRPServer() == false) return;

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                WinGrid grd = new WinGrid();
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                grd.mfInitGeneralGrid(this.uGridProduct, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(this.uGridProduct, 0, "PlantName", "공장", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "ProductCode", "제품코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PRODUCTACTIONTYPE", "제품구분", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "CustomerCode", "고객사", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "CUSTOMERPRODUCTSPEC", "고객제품코드", true, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "ProductName", "제품설명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "ProductNameEn", "제품설명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "ProductNameCh", "제품설명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, true, 50, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PackageGroupCode", "패키지그룹1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PACKAGEGROUP_1", "패키지그룹2", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PACKAGEGROUP_2", "패키지그룹3", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PACKAGEGROUP_3", "패키지그룹4", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PACKAGEGROUP_4", "패키지그룹5", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "Package", "패키지", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "EndCustomerCode", "최종고객사", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "FAMILY_1", "패밀리1", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "FAMILY_2", "패밀리2", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "FAMILY_3", "패밀리3", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "FAMILY_4", "패밀리4", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "FAMILY_5", "패밀리5", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "STACKKIND", "스택구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "STACKSEQ", "스택Seq", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "GENERATION", "세대", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 150, false, false, 50, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PRODUCTIONTYPE", "생산유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "SUBPRODUCTTYPE", "하위제품유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "ORGANIZATION", "BIT정보", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "Density", "메모리", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "BD_DIAGRAM", "BD", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "MK_LAYOUT", "MK", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "PCB_LAYOUT", "BOX", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "LF_LAYOUT", "PCB", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "WORKAREA", "WORKAREA", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                    , 100, false, false, 10, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridProduct, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly
                        , 100, false, false, 1, Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                        , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridProduct.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridProduct.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //////// 그리드에 DropDown 추가
                //////// 그리드 컬럼에 공장 콤보박스 추가
                //////DataTable dt = new DataTable();

                //////// BL 호출
                //////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                //////QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                //////brwChannel.mfCredentials(clsPlant);

                //////// Plant정보를 얻어오는 함수를 호출하여 DataTable에 저장

                //////dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(uGridMateriallist, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                //////// 그리드 컬럼에 자재그룹 Dropdown 추가
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialGroup), "MaterialGroup");
                //////QRPMAS.BL.MASMAT.MaterialGroup mg = new MaterialGroup();
                //////brwChannel.mfCredentials(mg);

                //////dt = mg.mfReadMASMaterialGroup("", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(uGridMateriallist, 0, "MaterialGroupCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                //////// 자재유형
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialType), "MaterialType");
                //////QRPMAS.BL.MASMAT.MaterialType mt = new MaterialType();
                //////brwChannel.mfCredentials(mt);

                //////dt = mt.mfReadMASMaterialTypeCombo("", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(uGridMateriallist, 0, "MaterialTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dt);

                //////// 사용여부
                //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //////QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                //////brwChannel.mfCredentials(clsCommonCode);

                //////DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(this.uGridMateriallist, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }
        /// <summary>
        /// 콤보박스초기화

        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor combo = new WinComboEditor();

                string strLang = m_resSys.GetString("SYS_LANG");

                string strAll = "";
                if (strLang.Equals("KOR"))
                    strAll = "전체";
                else if (strLang.Equals("CHN"))
                    strAll = "全部";
                else if (strLang.Equals("ENG"))
                    strAll = "All";

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dt = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                clsPlant.Dispose();
                // 공장 콤보박스
                combo.mfSetComboEditor(uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , m_resSys.GetString("SYS_PLANTCODE"), "", strAll, "PlantCode", "PlantName", dt);


                QRPBrowser brwChannelCt = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);

                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                clsPackage.Dispose();
                // 패키지그룹 콤보박스
                combo.mfSetComboEditor(uComboSearchPacakge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", strAll, "Package", "ComboName", dtPackage);


                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.PackageType), "PackageType");
                //QRPMAS.BL.MASMAT.PackageType clsPT = new QRPMAS.BL.MASMAT.PackageType();
                //brwChannel.mfCredentials(clsPT);

                //DataTable dtPT = clsPT.mfReadMASPackageTypeCombo(m_resSys.GetString("SYS_LANG"));

                //// 패키지유형 콤보박스
                //combo.mfSetComboEditor(uComboSearchPacakgeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                //    , "", "", "전체", "PackageTypeCode", "PackageTypeName", dtPT);


                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Family), "Family");
                //QRPMAS.BL.MASMAT.Family clsFamily = new QRPMAS.BL.MASMAT.Family();
                //brwChannel.mfCredentials(clsFamily);

                //DataTable dtF = clsFamily.mfReadMASFamilyCombo(m_resSys.GetString("SYS_LANG"));

                //// 패키지유형 콤보박스
                //combo.mfSetComboEditor(uComboSearchFamily, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                //    , "", "", "전체", "FamilyCode", "FamilyName", dtF);


                ////// 입력용 공장 콤보박스(삭제???)
                ////combo.mfSetComboEditor(uComboplant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                ////        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                ////        , "", "", "선택", "PlantCode", "PlantName", dt);

                ////// 사용유무 콤보박스
                ////QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                ////brwChannel.mfCredentials(clsCommonCode);

                ////DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                ////combo.mfSetComboEditor(uComboUseFlag, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                ////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", ""
                ////    , "ComCode", "ComCodeName", dtUseFlag);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel label = new WinLabel();

                label.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchCustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchICustomerProductSpec, "고객사제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPACKAGEGROUP_1, "패키지그룹_1", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPACKAGEGROUP_2, "패키지그룹_2", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPACKAGEGROUP_3, "패키지그룹_3", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPACKAGEGROUP_4, "패키지그룹_4", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPACKAGEGROUP_5, "패키지그룹_5", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelProductActionType, "제품구분", m_resSys.GetString("SYS_FONTNAME"), true, false);

                label.mfSetLabel(uLabelSearchMaterialGroup, "자재그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelSearchMaterialType, "자재유형", m_resSys.GetString("SYS_FONTNAME"), true, false);

                label.mfSetLabel(uLabelProductCode, "제품코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelProductName, "제품명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelProductNameCh, "제품명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelProductNameEn, "제품명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPlant, "공장명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMaterialGroup, "자재그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelMaterialType, "자재유형", m_resSys.GetString("SYS_FONTNAME"), true, false);

                label.mfSetLabel(uLabelPackageGroup, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelPackageType, "Package유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelFamily, "Family", m_resSys.GetString("SYS_FONTNAME"), true, false);

                label.mfSetLabel(uLabelSpec, "Spec", m_resSys.GetString("SYS_FONTNAME"), true, false);
                label.mfSetLabel(uLabelUseflag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바
        public void mfSave()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPBrowser brwChannel = new QRPBrowser();
                // 서버 연결 체크 //
                if (brwChannel.mfCheckQRPServer() == false) return;

                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220",m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                //검색정보 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strCustomer = this.uTextCustomer.Text;
                string strPackage = this.uComboSearchPacakge.Value.ToString();
                string strCustomerSpec = this.uTextSearchICustomerProductSpec.Text;
                string strPackageGroup1 = this.uComboPACKAGEGROUP_1.Value.ToString();
                string strPackageGroup2 = this.uComboPACKAGEGROUP_2.Value.ToString();
                string strPackageGroup3 = this.uComboPACKAGEGROUP_3.Value.ToString();
                string strPackageGroup4 = this.uComboPACKAGEGROUP_4.Value.ToString();
                string strPackageGroup5 = this.uComboPACKAGEGROUP_5.Value.ToString();
                string strProductActionType = this.uComboProductActionType.Value.ToString();


                //string strMaterialGroupCode = this.uComboSearchMaterialGroup.Value.ToString();
                //string strMaterialTypeCode = this.uComboSearchMaterialType.Value.ToString();
                //string strPackageTypeCode = this.uComboSearchPacakgeType.Value.ToString();
                //string strFamilyCode = this.uComboSearchFamily.Value.ToString();


                //DataTable dtProduct = clsProduct.mfReadMASProduct(strPlantCode, strMaterialGroupCode, strMaterialTypeCode, strPackageGroupCode, strPackageTypeCode, strFamilyCode, m_resSys.GetString("SYS_LANG"));
                DataTable dtProduct = clsProduct.mfReadMASProduct(strPlantCode, strPackage,strCustomer,strCustomerSpec,
                                                                    strPackageGroup1,strPackageGroup2,strPackageGroup3,strPackageGroup4,strPackageGroup5,
                                                                    strProductActionType, m_resSys.GetString("SYS_LANG"));

                this.uGridProduct.DataSource = dtProduct;
                this.uGridProduct.DataBind();
                  
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                
                if (dtProduct.Rows.Count == 0) 
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridProduct, 0);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        public void mfCreate()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            //처리 로직//
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            grd.mfDownLoadGridToExcel(this.uGridProduct);
        }

        public void mfPrint()
        {
        }
        #endregion

        #region 이벤트

        //어떤공장을 선택여부에 따른 Value 바뀜

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor combo = new WinComboEditor();
                QRPBrowser brwChannel = new QRPBrowser();

                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialGroup), "MaterialGroup");
                //////QRPMAS.BL.MASMAT.MaterialGroup mg = new QRPMAS.BL.MASMAT.MaterialGroup();
                //////brwChannel.mfCredentials(mg);

                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialType), "MaterialType");
                //////QRPMAS.BL.MASMAT.MaterialType mt = new QRPMAS.BL.MASMAT.MaterialType();
                //////brwChannel.mfCredentials(mt);
                this.uComboSearchPacakge.Items.Clear();
                

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                string strLang = m_resSys.GetString("SYS_LANG");
                string strAll = "";
                if (strLang.Equals("KOR"))
                    strAll = "전체";
                else if (strLang.Equals("CHN"))
                    strAll = "全部";
                else if (strLang.Equals("ENG"))
                    strAll = "All";


                QRPBrowser brwChannelCt = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsPackage = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsPackage);

                DataTable dtPackage = clsPackage.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                // 패키지 콤보박스
                combo.mfSetComboEditor(uComboSearchPacakge, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Left
                    , "", "", strAll, "Package", "ComboName", dtPackage);

                #region 패키지 그룹 콤보박스

                //패키지그룹 콤보박스
                this.uComboPACKAGEGROUP_1.Items.Clear();
                this.uComboPACKAGEGROUP_2.Items.Clear();
                this.uComboPACKAGEGROUP_3.Items.Clear();
                this.uComboPACKAGEGROUP_4.Items.Clear();
                this.uComboPACKAGEGROUP_5.Items.Clear();                

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                DataTable dtPackageGroup1 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode, "1", m_resSys.GetString("SYS_LANG"));
                DataTable dtPackageGroup2 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode, "2", m_resSys.GetString("SYS_LANG"));
                DataTable dtPackageGroup3 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode, "3", m_resSys.GetString("SYS_LANG"));
                DataTable dtPackageGroup4 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode, "4", m_resSys.GetString("SYS_LANG"));
                DataTable dtPackageGroup5 = clsProduct.mfReadMASProduct_PackageGroup(strPlantCode, "5", m_resSys.GetString("SYS_LANG"));

                combo.mfSetComboEditor(this.uComboPACKAGEGROUP_1, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", strAll, "PackageGroupCode", "PackageGroupName", dtPackageGroup1);

                combo.mfSetComboEditor(this.uComboPACKAGEGROUP_2, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", strAll, "PackageGroupCode", "PackageGroupName", dtPackageGroup2);

                combo.mfSetComboEditor(this.uComboPACKAGEGROUP_3, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", strAll, "PackageGroupCode", "PackageGroupName", dtPackageGroup3);

                combo.mfSetComboEditor(this.uComboPACKAGEGROUP_4, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", strAll, "PackageGroupCode", "PackageGroupName", dtPackageGroup4);

                combo.mfSetComboEditor(this.uComboPACKAGEGROUP_5, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", strAll, "PackageGroupCode", "PackageGroupName", dtPackageGroup5);

                //제품구분 콤보박스 추가
                this.uComboProductActionType.Items.Clear();

                DataTable dtProductActionType = clsProduct.mfReadMASProduct_ActionType(strPlantCode, "", m_resSys.GetString("SYS_LANG"));
                combo.mfSetComboEditor(this.uComboProductActionType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                                    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                                    , "", "", strAll, "ActionTypeCode", "ActionTypeName", dtProductActionType);

                #endregion

                #region 주석
                //////DataTable dtMaterialGroup = new DataTable();
                //////DataTable dtMaterialType = new DataTable();
                //////// 콤보박스 아이템 전체 삭제
                //////this.uComboSearchMaterialGroup.Items.Clear();
                //////this.uComboSearchMaterialType.Items.Clear();

                //////// 공장콤보박스에서 값이 선택되었을경우

                //////if (strPlantCode != "")
                //////{
                //////    dtMaterialGroup = mg.mfReadMASMaterialGroupCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                //////    dtMaterialType = mt.mfReadMASMaterialTypeCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                //////}

                //////combo.mfSetComboEditor(uComboSearchMaterialGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                //////    , "", "", "전체", "MaterialGroupCode", "MaterialGroupName", dtMaterialGroup);

                //////combo.mfSetComboEditor(uComboSearchMaterialType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //////    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center
                //////    , "", "", "전체", "MaterialTypeCode", "MaterialTypeName", dtMaterialType);
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        // 그룹박스 펼침 이벤트

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridProduct.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridProduct.Height = 720;

                    for (int i = 0; i < uGridProduct.Rows.Count; i++)
                    {
                        uGridProduct.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드더블클릭시 발생하는 이벤트

        private void uGridproduct_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //리스트에서 조회하도록 한다.
                //////if (uGroupBoxContentsArea.Expanded == false)
                //////{
                //////    this.uGroupBoxContentsArea.Expanded = true;
                //////    e.Cell.Row.Fixed = true;
                //////}
                //////this.uTextPlant.Text = e.Cell.Row.Cells["PlantName"].Value.ToString();
                //////this.uTextMaterialGroup.Text = e.Cell.Row.Cells["MaterialGroupName"].Value.ToString();
                //////this.uTextMaterialType.Text = e.Cell.Row.Cells["MaterialTypeName"].Value.ToString();
                //////this.uTextProductCode.Text = e.Cell.Row.Cells["ProductCode"].Value.ToString();
                //////this.uTextProductName.Text = e.Cell.Row.Cells["ProductName"].Value.ToString();
                //////this.uTextProductNameCh.Text = e.Cell.Row.Cells["ProductNameCh"].Value.ToString();
                //////this.uTextProductNameEn.Text = e.Cell.Row.Cells["ProductNameEn"].Value.ToString();
                //////this.uTextSpec.Text = e.Cell.Row.Cells["Spec"].Value.ToString();
                //////this.uTextUseFlag.Text = e.Cell.Row.Cells["UseFlag"].Value.ToString();

                //////this.uTextPackageGroup.Text = e.Cell.Row.Cells["PackageGroupName"].Value.ToString();
                //////this.uTextPackageType.Text = e.Cell.Row.Cells["PackageTypeName"].Value.ToString();
                //////this.uTextFamily.Text = e.Cell.Row.Cells["FamilyName"].Value.ToString();

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion

        private void uTextCustomer_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

               

                frmPOP0003 frmCustom = new frmPOP0003();
                frmCustom.ShowDialog();

                this.uTextCustomer.Text = frmCustom.CustomerCode;

            }
            catch (Exception ex)
            {
                 QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextCustomer_KeyDown(object sender, KeyEventArgs e)
        {
            //try
            //{
            //    if (!this.uTextCustomer.Text.Equals(string.Empty) && e.KeyData.Equals(Keys.Enter))
            //    {

            //    }
            //}
            //catch (Exception ex)
            //{
            //    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            //    frmErr.ShowDialog();
            //}
            //finally
            //{ }
        }
    }
}
