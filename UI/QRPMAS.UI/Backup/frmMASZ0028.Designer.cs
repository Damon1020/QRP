﻿namespace QRPMAS.UI
{
    partial class frmMASZ0028
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0028));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonFileDown = new Infragistics.Win.Misc.UltraButton();
            this.uGridEquipPMD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchEquipType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchLargeEquipType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchStation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquipPMAdmitFN = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextRejectReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRejectReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTabEquipPM = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTextAdmitDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevisionReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevisionReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAcceptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAcceptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevisionNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStandardNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMD)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).BeginInit();
            this.uGroupSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLargeEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMAdmitFN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabEquipPM)).BeginInit();
            this.uTabEquipPM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNo)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonFileDown);
            this.ultraTabPageControl1.Controls.Add(this.uGridEquipPMD);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1040, 466);
            // 
            // uButtonFileDown
            // 
            this.uButtonFileDown.Location = new System.Drawing.Point(12, 12);
            this.uButtonFileDown.Name = "uButtonFileDown";
            this.uButtonFileDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonFileDown.TabIndex = 6;
            this.uButtonFileDown.Click += new System.EventHandler(this.uButtonFileDown_Click);
            // 
            // uGridEquipPMD
            // 
            this.uGridEquipPMD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipPMD.DisplayLayout.Appearance = appearance40;
            this.uGridEquipPMD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipPMD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.Appearance = appearance41;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance43.BackColor2 = System.Drawing.SystemColors.Control;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.PromptAppearance = appearance43;
            this.uGridEquipPMD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipPMD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipPMD.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipPMD.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.uGridEquipPMD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipPMD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.Override.CardAreaAppearance = appearance46;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipPMD.DisplayLayout.Override.CellAppearance = appearance47;
            this.uGridEquipPMD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipPMD.DisplayLayout.Override.CellPadding = 0;
            appearance48.BackColor = System.Drawing.SystemColors.Control;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.Override.GroupByRowAppearance = appearance48;
            appearance49.TextHAlignAsString = "Left";
            this.uGridEquipPMD.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.uGridEquipPMD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipPMD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipPMD.DisplayLayout.Override.RowAppearance = appearance50;
            this.uGridEquipPMD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipPMD.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.uGridEquipPMD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipPMD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipPMD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipPMD.Location = new System.Drawing.Point(12, 44);
            this.uGridEquipPMD.Name = "uGridEquipPMD";
            this.uGridEquipPMD.Size = new System.Drawing.Size(1020, 412);
            this.uGridEquipPMD.TabIndex = 0;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridEquipList);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1040, 502);
            // 
            // uGridEquipList
            // 
            this.uGridEquipList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipList.DisplayLayout.Appearance = appearance55;
            this.uGridEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance52.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance52.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance52.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.GroupByBox.Appearance = appearance52;
            appearance53.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance53;
            this.uGridEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance54.BackColor2 = System.Drawing.SystemColors.Control;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance54;
            this.uGridEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance63;
            appearance58.BackColor = System.Drawing.SystemColors.Highlight;
            appearance58.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance58;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.CardAreaAppearance = appearance57;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipList.DisplayLayout.Override.CellAppearance = appearance56;
            this.uGridEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance60.BackColor = System.Drawing.SystemColors.Control;
            appearance60.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance60.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance60.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance60.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance60;
            appearance62.TextHAlignAsString = "Left";
            this.uGridEquipList.DisplayLayout.Override.HeaderAppearance = appearance62;
            this.uGridEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipList.DisplayLayout.Override.RowAppearance = appearance61;
            this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance59.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance59;
            this.uGridEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipList.Location = new System.Drawing.Point(12, 12);
            this.uGridEquipList.Name = "uGridEquipList";
            this.uGridEquipList.Size = new System.Drawing.Size(1020, 484);
            this.uGridEquipList.TabIndex = 0;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupSearchArea
            // 
            this.uGroupSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupSearchArea.Appearance = appearance1;
            this.uGroupSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchEquipType);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchEquipLoc);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchLargeEquipType);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchStation);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchEquipGroup);
            this.uGroupSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupSearchArea.Name = "uGroupSearchArea";
            this.uGroupSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupSearchArea.TabIndex = 1;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 36);
            this.uComboProcessGroup.MaxLength = 40;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(144, 21);
            this.uComboProcessGroup.TabIndex = 20;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uLabelSearchEquipType
            // 
            this.uLabelSearchEquipType.Location = new System.Drawing.Point(320, 36);
            this.uLabelSearchEquipType.Name = "uLabelSearchEquipType";
            this.uLabelSearchEquipType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipType.TabIndex = 18;
            // 
            // uLabelSearchEquipLoc
            // 
            this.uLabelSearchEquipLoc.Location = new System.Drawing.Point(628, 12);
            this.uLabelSearchEquipLoc.Name = "uLabelSearchEquipLoc";
            this.uLabelSearchEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipLoc.TabIndex = 19;
            // 
            // uComboSearchLargeEquipType
            // 
            this.uComboSearchLargeEquipType.Location = new System.Drawing.Point(424, 36);
            this.uComboSearchLargeEquipType.MaxLength = 50;
            this.uComboSearchLargeEquipType.Name = "uComboSearchLargeEquipType";
            this.uComboSearchLargeEquipType.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchLargeEquipType.TabIndex = 17;
            this.uComboSearchLargeEquipType.ValueChanged += new System.EventHandler(this.uComboSearchLargeEquipType_ValueChanged);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(732, 12);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchEquipLoc.TabIndex = 16;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(424, 12);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchStation.TabIndex = 12;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(732, 36);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchEquipGroup.TabIndex = 13;
            // 
            // uLabelSearchStation
            // 
            this.uLabelSearchStation.Location = new System.Drawing.Point(320, 12);
            this.uLabelSearchStation.Name = "uLabelSearchStation";
            this.uLabelSearchStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchStation.TabIndex = 14;
            // 
            // uLabelSearchEquipGroup
            // 
            this.uLabelSearchEquipGroup.Location = new System.Drawing.Point(628, 36);
            this.uLabelSearchEquipGroup.Name = "uLabelSearchEquipGroup";
            this.uLabelSearchEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchEquipGroup.TabIndex = 15;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 36);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 0;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uGridEquipPMAdmitFN
            // 
            this.uGridEquipPMAdmitFN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipPMAdmitFN.DisplayLayout.Appearance = appearance36;
            this.uGridEquipPMAdmitFN.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipPMAdmitFN.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMAdmitFN.DisplayLayout.GroupByBox.Appearance = appearance37;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMAdmitFN.DisplayLayout.GroupByBox.BandLabelAppearance = appearance38;
            this.uGridEquipPMAdmitFN.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance39.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance39.BackColor2 = System.Drawing.SystemColors.Control;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMAdmitFN.DisplayLayout.GroupByBox.PromptAppearance = appearance39;
            this.uGridEquipPMAdmitFN.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipPMAdmitFN.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance13.BackColor = System.Drawing.SystemColors.Highlight;
            appearance13.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.ActiveRowAppearance = appearance13;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.CardAreaAppearance = appearance15;
            appearance16.BorderColor = System.Drawing.Color.Silver;
            appearance16.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.CellAppearance = appearance16;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.CellPadding = 0;
            appearance17.BackColor = System.Drawing.SystemColors.Control;
            appearance17.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance17.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.GroupByRowAppearance = appearance17;
            appearance18.TextHAlignAsString = "Left";
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.HeaderAppearance = appearance18;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.RowAppearance = appearance19;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance20.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipPMAdmitFN.DisplayLayout.Override.TemplateAddRowAppearance = appearance20;
            this.uGridEquipPMAdmitFN.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipPMAdmitFN.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipPMAdmitFN.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipPMAdmitFN.Location = new System.Drawing.Point(0, 100);
            this.uGridEquipPMAdmitFN.Name = "uGridEquipPMAdmitFN";
            this.uGridEquipPMAdmitFN.Size = new System.Drawing.Size(1070, 720);
            this.uGridEquipPMAdmitFN.TabIndex = 2;
            this.uGridEquipPMAdmitFN.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridEquipPMAdmit_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 674);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 674);
            this.uGroupBoxContentsArea.TabIndex = 5;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRejectReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRejectReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAdmitUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabEquipPM);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAdmitDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevisionReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevisionReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAdmitUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAcceptUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAcceptDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroupName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroupName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroupCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroupCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVersionNum);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevisionNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStandardNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardNo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 654);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextRejectReason
            // 
            appearance22.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRejectReason.Appearance = appearance22;
            this.uTextRejectReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRejectReason.Location = new System.Drawing.Point(148, 132);
            this.uTextRejectReason.MaxLength = 100;
            this.uTextRejectReason.Name = "uTextRejectReason";
            this.uTextRejectReason.ReadOnly = true;
            this.uTextRejectReason.Size = new System.Drawing.Size(784, 21);
            this.uTextRejectReason.TabIndex = 45;
            // 
            // uLabelRejectReason
            // 
            this.uLabelRejectReason.Location = new System.Drawing.Point(12, 132);
            this.uLabelRejectReason.Name = "uLabelRejectReason";
            this.uLabelRejectReason.Size = new System.Drawing.Size(130, 20);
            this.uLabelRejectReason.TabIndex = 44;
            // 
            // uTextAdmitUserID
            // 
            appearance10.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitUserID.Appearance = appearance10;
            this.uTextAdmitUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitUserID.Location = new System.Drawing.Point(148, 60);
            this.uTextAdmitUserID.MaxLength = 20;
            this.uTextAdmitUserID.Name = "uTextAdmitUserID";
            this.uTextAdmitUserID.ReadOnly = true;
            this.uTextAdmitUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitUserID.TabIndex = 43;
            // 
            // uTabEquipPM
            // 
            this.uTabEquipPM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabEquipPM.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabEquipPM.Controls.Add(this.ultraTabPageControl1);
            this.uTabEquipPM.Controls.Add(this.ultraTabPageControl2);
            this.uTabEquipPM.Location = new System.Drawing.Point(12, 156);
            this.uTabEquipPM.Name = "uTabEquipPM";
            this.uTabEquipPM.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabEquipPM.Size = new System.Drawing.Size(1044, 492);
            this.uTabEquipPM.TabIndex = 42;
            ultraTab1.Key = "Item";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "점검항목상세";
            ultraTab2.Key = "Equip";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "점검그룹 설비리스트";
            this.uTabEquipPM.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1040, 466);
            // 
            // uTextAdmitDate
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitDate.Appearance = appearance9;
            this.uTextAdmitDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitDate.Location = new System.Drawing.Point(588, 60);
            this.uTextAdmitDate.Name = "uTextAdmitDate";
            this.uTextAdmitDate.ReadOnly = true;
            this.uTextAdmitDate.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitDate.TabIndex = 41;
            // 
            // uTextCreateDate
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateDate.Appearance = appearance23;
            this.uTextCreateDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateDate.Location = new System.Drawing.Point(588, 36);
            this.uTextCreateDate.Name = "uTextCreateDate";
            this.uTextCreateDate.ReadOnly = true;
            this.uTextCreateDate.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateDate.TabIndex = 41;
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(452, 36);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelCreateDate.TabIndex = 40;
            // 
            // uTextRevisionReason
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionReason.Appearance = appearance3;
            this.uTextRevisionReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionReason.Location = new System.Drawing.Point(148, 84);
            this.uTextRevisionReason.Name = "uTextRevisionReason";
            this.uTextRevisionReason.ReadOnly = true;
            this.uTextRevisionReason.Size = new System.Drawing.Size(784, 21);
            this.uTextRevisionReason.TabIndex = 39;
            // 
            // uTextEtc
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtc.Appearance = appearance21;
            this.uTextEtc.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEtc.Location = new System.Drawing.Point(148, 108);
            this.uTextEtc.MaxLength = 100;
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.ReadOnly = true;
            this.uTextEtc.Size = new System.Drawing.Size(784, 21);
            this.uTextEtc.TabIndex = 39;
            // 
            // uLabelRevisionReason
            // 
            this.uLabelRevisionReason.Location = new System.Drawing.Point(12, 84);
            this.uLabelRevisionReason.Name = "uLabelRevisionReason";
            this.uLabelRevisionReason.Size = new System.Drawing.Size(130, 20);
            this.uLabelRevisionReason.TabIndex = 38;
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 108);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(130, 20);
            this.uLabelEtc.TabIndex = 38;
            // 
            // uTextAdmitUserName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitUserName.Appearance = appearance2;
            this.uTextAdmitUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitUserName.Location = new System.Drawing.Point(252, 60);
            this.uTextAdmitUserName.Name = "uTextAdmitUserName";
            this.uTextAdmitUserName.ReadOnly = true;
            this.uTextAdmitUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitUserName.TabIndex = 35;
            // 
            // uLabelAcceptUser
            // 
            this.uLabelAcceptUser.Location = new System.Drawing.Point(12, 60);
            this.uLabelAcceptUser.Name = "uLabelAcceptUser";
            this.uLabelAcceptUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelAcceptUser.TabIndex = 33;
            // 
            // uLabelAcceptDate
            // 
            this.uLabelAcceptDate.Location = new System.Drawing.Point(452, 60);
            this.uLabelAcceptDate.Name = "uLabelAcceptDate";
            this.uLabelAcceptDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelAcceptDate.TabIndex = 31;
            // 
            // uTextCreateUserName
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Appearance = appearance27;
            this.uTextCreateUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Location = new System.Drawing.Point(252, 36);
            this.uTextCreateUserName.Name = "uTextCreateUserName";
            this.uTextCreateUserName.ReadOnly = true;
            this.uTextCreateUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserName.TabIndex = 30;
            // 
            // uTextCreateUserID
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserID.Appearance = appearance5;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserID.Location = new System.Drawing.Point(148, 36);
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.ReadOnly = true;
            this.uTextCreateUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserID.TabIndex = 29;
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(12, 36);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelCreateUser.TabIndex = 28;
            // 
            // uTextEquipGroupName
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Appearance = appearance12;
            this.uTextEquipGroupName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Location = new System.Drawing.Point(588, 12);
            this.uTextEquipGroupName.Name = "uTextEquipGroupName";
            this.uTextEquipGroupName.ReadOnly = true;
            this.uTextEquipGroupName.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipGroupName.TabIndex = 27;
            // 
            // uLabelEquipGroupName
            // 
            this.uLabelEquipGroupName.Location = new System.Drawing.Point(452, 12);
            this.uLabelEquipGroupName.Name = "uLabelEquipGroupName";
            this.uLabelEquipGroupName.Size = new System.Drawing.Size(130, 20);
            this.uLabelEquipGroupName.TabIndex = 26;
            // 
            // uTextEquipGroupCode
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupCode.Appearance = appearance7;
            this.uTextEquipGroupCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupCode.Location = new System.Drawing.Point(148, 12);
            this.uTextEquipGroupCode.Name = "uTextEquipGroupCode";
            this.uTextEquipGroupCode.ReadOnly = true;
            this.uTextEquipGroupCode.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipGroupCode.TabIndex = 25;
            // 
            // uLabelEquipGroupCode
            // 
            this.uLabelEquipGroupCode.Location = new System.Drawing.Point(12, 12);
            this.uLabelEquipGroupCode.Name = "uLabelEquipGroupCode";
            this.uLabelEquipGroupCode.Size = new System.Drawing.Size(130, 20);
            this.uLabelEquipGroupCode.TabIndex = 24;
            // 
            // uTextVersionNum
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Appearance = appearance8;
            this.uTextVersionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Location = new System.Drawing.Point(996, 60);
            this.uTextVersionNum.Name = "uTextVersionNum";
            this.uTextVersionNum.ReadOnly = true;
            this.uTextVersionNum.Size = new System.Drawing.Size(36, 21);
            this.uTextVersionNum.TabIndex = 23;
            this.uTextVersionNum.Visible = false;
            // 
            // uLabelRevisionNo
            // 
            this.uLabelRevisionNo.Location = new System.Drawing.Point(984, 64);
            this.uLabelRevisionNo.Name = "uLabelRevisionNo";
            this.uLabelRevisionNo.Size = new System.Drawing.Size(12, 8);
            this.uLabelRevisionNo.TabIndex = 22;
            this.uLabelRevisionNo.Text = "-";
            this.uLabelRevisionNo.Visible = false;
            // 
            // uTextStandardNo
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardNo.Appearance = appearance14;
            this.uTextStandardNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardNo.Location = new System.Drawing.Point(880, 60);
            this.uTextStandardNo.Name = "uTextStandardNo";
            this.uTextStandardNo.ReadOnly = true;
            this.uTextStandardNo.Size = new System.Drawing.Size(104, 21);
            this.uTextStandardNo.TabIndex = 21;
            this.uTextStandardNo.Visible = false;
            // 
            // uLabelStandardNo
            // 
            this.uLabelStandardNo.Location = new System.Drawing.Point(744, 60);
            this.uLabelStandardNo.Name = "uLabelStandardNo";
            this.uLabelStandardNo.Size = new System.Drawing.Size(130, 20);
            this.uLabelStandardNo.TabIndex = 20;
            this.uLabelStandardNo.Visible = false;
            // 
            // frmMASZ0028
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridEquipPMAdmitFN);
            this.Controls.Add(this.uGroupSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0028";
            this.Load += new System.EventHandler(this.frmMASZ0028_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0028_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMASZ0028_FormClosing);
            this.Resize += new System.EventHandler(this.frmMASZ0028_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMD)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchArea)).EndInit();
            this.uGroupSearchArea.ResumeLayout(false);
            this.uGroupSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchLargeEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMAdmitFN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabEquipPM)).EndInit();
            this.uTabEquipPM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipPMAdmitFN;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRejectReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRejectReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitUserID;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabEquipPM;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipPMD;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateDate;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevisionReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelRevisionReason;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptUser;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVersionNum;
        private Infragistics.Win.Misc.UltraLabel uLabelRevisionNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardNo;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNo;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitDate;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchLargeEquipType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
    }
}