﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0046.cs                                         */
/* 프로그램명   : 정비유형정보                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0046 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0046()
        {
            InitializeComponent();
        }

        private void frmMAS0046_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0046_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("정비유형정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
        }

        #region 초기화 함수
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchRepairGroup, "정비분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurablePMType, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDurablePMType, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurablePMType, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "",m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridDurablePMType, 0, "DurablePMGubunCode", "정비분류", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurablePMType, 0, "DurablePMTypeCode", "정비유형코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurablePMType, 0, "DurablePMTypeName", "정비유형명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurablePMType, 0, "DurablePMTypeNameCh", "정비유형명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurablePMType, 0, "DurablePMTypeNameEn", "정비유형명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurablePMType, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                // 공장컬럼 DropDonw 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridDurablePMType, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);
                DataTable dtCommonCode = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridDurablePMType, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtCommonCode);


                


                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGridDurablePMType, 0);

                // FontSize
                this.uGridDurablePMType.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurablePMType.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar Method

        public void mfSearch()
        {
            try
            {
                string strPlantCode = uComboSearchPlant.Value.ToString();
                //string strDurableMatTypeCode = uComboMold.Value.ToString();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPGlobal grdImg = new QRPGlobal();
                WinGrid wGrid = new WinGrid();
                DataRow row;
                string strRowPlantCode = "";

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중…");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //--------------------------------------------------------------------처리로직----------------------------------------------------------//
                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurablePMType), "DurablePMType");
                QRPMAS.BL.MASDMM.DurablePMType clsDurablePMType = new QRPMAS.BL.MASDMM.DurablePMType();
                brwChnnel.mfCredentials(clsDurablePMType);


                DataTable dtDurablePMType = new DataTable();
                //조회함수호출
                dtDurablePMType = clsDurablePMType.mfReadDurablePMType(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인딩
                this.uGridDurablePMType.DataSource = dtDurablePMType;
                this.uGridDurablePMType.DataBind();

                //--------------------------------------------------------------------------------------------------------------------------------------//
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtDurablePMType.Rows.Count == 0)
                {/* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                   //PK 수정불가
                    for (int i = 0; i < this.uGridDurablePMType.Rows.Count; i++)
                    {
                        this.uGridDurablePMType.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurablePMType.Rows[i].Cells["DurablePMGubunCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurablePMType.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                        this.uGridDurablePMType.Rows[i].Cells["DurablePMGubunCode"].Appearance.BackColor = Color.Gainsboro;
                        this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeCode"].Appearance.BackColor = Color.Gainsboro;

                    }

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDurablePMType, 0);
                }

                

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataRow row;
                DialogResult DResult = new DialogResult();

                int intRowCheck = 0;

                if(this.uGridDurablePMType.Rows.Count > 0)
                    this.uGridDurablePMType.ActiveCell = this.uGridDurablePMType.Rows[0].Cells[0];

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGridDurablePMType.Rows.Count; i++)
                {
                    
                    if (this.uGridDurablePMType.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;
                        if (this.uGridDurablePMType.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", this.uGridDurablePMType.Rows[i].RowSelectorNumber + "번째 열의 공장을 선택해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridDurablePMType.ActiveCell = this.uGridDurablePMType.Rows[i].Cells["PlantCode"];
                            this.uGridDurablePMType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridDurablePMType.Rows[i].Cells["DurablePMGubunCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", this.uGridDurablePMType.Rows[i].RowSelectorNumber + "번째 열의 정비분류코드를 선택해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridDurablePMType.ActiveCell = this.uGridDurablePMType.Rows[i].Cells["DurablePMGubunCode"];
                            this.uGridDurablePMType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", this.uGridDurablePMType.Rows[i].RowSelectorNumber + "번째 열의 정비유형코드를 입력해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridDurablePMType.ActiveCell = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeCode"];
                            this.uGridDurablePMType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", "저장할 행을 한줄 이상 입력해주세요", Infragistics.Win.HAlign.Center);
                    return;
                }


                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                           , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "입력 한 정보를 저장하시겠습니까?"
                                           , Infragistics.Win.HAlign.Right);


                if (dir == DialogResult.No)
                    return;


                // 채널 연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 호출 : 금형/치공구 저장을 위한 데이터테이블 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurablePMType), "DurablePMType");
                QRPMAS.BL.MASDMM.DurablePMType clsDurablePMType = new QRPMAS.BL.MASDMM.DurablePMType();
                brwChannel.mfCredentials(clsDurablePMType);

                DataTable dtDurablePMType = clsDurablePMType.mfSetDatainfo();

                for (int i = 0; i < this.uGridDurablePMType.Rows.Count; i++)
                {
                    this.uGridDurablePMType.ActiveCell = this.uGridDurablePMType.Rows[0].Cells[0];

                    if (this.uGridDurablePMType.Rows[i].Hidden == false
                        && this.uGridDurablePMType.Rows[i].RowSelectorAppearance.Image != null)
                    {

                        row = dtDurablePMType.NewRow();
                        row["PlantCode"] = this.uGridDurablePMType.Rows[i].Cells["PlantCode"].Value.ToString();
                        row["DurablePMGubunCode"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMGubunCode"].Value.ToString();
                        row["DurablePMTypeCode"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeCode"].Value.ToString();
                        row["DurablePMTypeName"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeName"].Value.ToString();
                        row["DurablePMTypeNameCh"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeNameCh"].Value.ToString();
                        row["DurablePMTypeNameEn"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeNameEn"].Value.ToString();
                        row["UseFlag"] = this.uGridDurablePMType.Rows[i].Cells["UseFlag"].Value.ToString();
                        dtDurablePMType.Rows.Add(row);

                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 처리 로직 //
                // 저장함수 호출
                string rtMSG = clsDurablePMType.mfSaveMASDurablePMType(dtDurablePMType, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                        Infragistics.Win.HAlign.Right);


                    mfCreate();
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                        Infragistics.Win.HAlign.Right);
                }




            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataRow row;
                DialogResult DResult = new DialogResult();

                int intRowCheck = 0;



                // 필수 입력사항 확인
                for (int i = 0; i < this.uGridDurablePMType.Rows.Count; i++)
                {
                    this.uGridDurablePMType.ActiveCell = this.uGridDurablePMType.Rows[0].Cells[0];

                    if (this.uGridDurablePMType.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridDurablePMType.Rows[i].Cells["Check"].Value) == true)
                        {
                            intRowCheck = intRowCheck + 1;
                        }
                    }
                }
                if (intRowCheck == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", "삭제할 행을 하나이상 선택해주세요", Infragistics.Win.HAlign.Center);
                    return;
                }



                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                           , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 삭제확인", "입력한 정보를 삭제하시겠습니까?"
                                           , Infragistics.Win.HAlign.Right);


                if (dir == DialogResult.No)
                    return;


                // 채널 연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 호출 : 금형/치공구 저장을 위한 데이터테이블 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurablePMType), "DurablePMType");
                QRPMAS.BL.MASDMM.DurablePMType clsDurablePMType = new QRPMAS.BL.MASDMM.DurablePMType();
                brwChannel.mfCredentials(clsDurablePMType);

                DataTable dtDurablePMType = clsDurablePMType.mfSetDatainfo();

                for (int i = 0; i < this.uGridDurablePMType.Rows.Count; i++)
                {
                    this.uGridDurablePMType.ActiveCell = this.uGridDurablePMType.Rows[0].Cells[0];

                    if (this.uGridDurablePMType.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGridDurablePMType.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurablePMType.NewRow();
                            row["PlantCode"] = this.uGridDurablePMType.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DurablePMGubunCode"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMGubunCode"].Value.ToString();
                            row["DurablePMTypeCode"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeCode"].Value.ToString();
                            row["DurablePMTypeName"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeName"].Value.ToString();
                            row["DurablePMTypeNameCh"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeNameCh"].Value.ToString();
                            row["DurablePMTypeNameEn"] = this.uGridDurablePMType.Rows[i].Cells["DurablePMTypeNameEn"].Value.ToString();
                            row["UseFlag"] = this.uGridDurablePMType.Rows[i].Cells["UseFlag"].Value.ToString();
                            dtDurablePMType.Rows.Add(row);
                        }
                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;



                // 처리 로직 //
                // 삭제함수 호출
                string rtMSG = clsDurablePMType.mfDeleteMASDurablePMType(dtDurablePMType, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "처리결과", "삭제처리결과", "입력한 행을 성공적으로 삭제했습니다.",
                                        Infragistics.Win.HAlign.Right);


                    mfCreate();
                    mfSearch();
                }
                else
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "처리결과", "삭제처리결과", "선택한 행을 삭제하지 못하였습니다.",
                                        Infragistics.Win.HAlign.Right);
                }




            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        #endregion

        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 수정시 이미지 변경

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 자동행삭제

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurablePMType, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                this.uComboDurableGubun.Items.Clear();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (strPlantCode == "")
                    return;


                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurablePMGubun), "DurablePMGubun");
                QRPMAS.BL.MASDMM.DurablePMGubun clsDurablePMGubun = new QRPMAS.BL.MASDMM.DurablePMGubun();
                brwChannel.mfCredentials(clsDurablePMGubun);


                DataTable dtSPStock = clsDurablePMGubun.mfReadDurablePMGubun_Combo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboDurableGubun, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , "", "", "선택", "DurablePMGubunCode", "DurablePMGubunName", dtSPStock);
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurablePMType_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurablePMGubun), "DurablePMGubun");
                QRPMAS.BL.MASDMM.DurablePMGubun clsDurablePMGubun = new QRPMAS.BL.MASDMM.DurablePMGubun();
                brwChannel.mfCredentials(clsDurablePMGubun);

                DataTable dtSPStock = clsDurablePMGubun.mfReadDurablePMGubun_Combo(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridCellValueList(this.uGridDurablePMType,e.Row.Index, "DurablePMGubunCode", "", "선택", dtSPStock);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurablePMType_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //컬럼
                String strColumn = e.Cell.Column.Key;
                int intIndex = e.Cell.Row.Index;



                if (strColumn == "PlantCode")
                {

                    string strPlantCode = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();

                    if (strPlantCode == "")
                        return;

                    WinGrid wGrid = new WinGrid();
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurablePMGubun), "DurablePMGubun");
                    QRPMAS.BL.MASDMM.DurablePMGubun clsDurablePMGubun = new QRPMAS.BL.MASDMM.DurablePMGubun();
                    brwChannel.mfCredentials(clsDurablePMGubun);

                    DataTable dtSPStock = clsDurablePMGubun.mfReadDurablePMGubun_Combo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    wGrid.mfSetGridCellValueList(this.uGridDurablePMType, intIndex, "DurablePMGubunCode", "", "", dtSPStock);
                    Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
