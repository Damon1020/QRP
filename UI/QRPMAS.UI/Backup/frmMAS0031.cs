﻿
/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0031.cs                                         */
/* 프로그램명   : 고객정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMAS0031 : Form,IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();


        public frmMAS0031()
        {
            InitializeComponent();
        }

        private void frmMAS0031_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0031_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("고객정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitButton();
            InitGrid();
            InitGroupBox();
            uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                this.uButtonDeleteRow.Click += new EventHandler(uButtonDeleteRow_Click);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel wLabel = new WinLabel();
                wLabel.mfSetLabel(this.uLabelCustomerCode, "고객코드", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerName, "고객명", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerNameCh, "고객명_중문", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelCustomerNameEn, "고객명_영문", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelRegNo, "사업자등록번호", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelBossName, "대표자명", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelAddress, "사업장주소", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelTel, "사업장전화번호", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelFax, "사업장Fax번호", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                wLabel.mfSetLabel(this.uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                //기본설정
                //--고객정보
                grd.mfInitGeneralGrid(this.uGridCustomer, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.No
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--고객담당자정보
                grd.mfInitGeneralGrid(this.uGridCustomerP, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //--고객정보
                grd.mfSetGridColumn(this.uGridCustomer, 0, "CustomerCode", "고객코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomer, 0, "CustomerName", "고객명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomer, 0, "CustomerNameCh", "고객명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomer, 0, "CustomerNameEn", "고객명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--고객담당자정보
                grd.mfSetGridColumn(this.uGridCustomerP, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0,
                     Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                     , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridCustomerP, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                grd.mfSetGridColumn(this.uGridCustomerP, 0, "DeptName", "업체부서명",false,Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomerP, 0, "PersonName", "담당자명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomerP, 0, "Position", "직위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomerP, 0, "Tel", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomerP, 0, "Hp", "핸드폰", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 90, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomerP, 0, "Email", "E-mail", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridCustomerP, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //폰트설정
                uGridCustomer.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridCustomer.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridCustomerP.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridCustomerP.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //빈줄추가
                grd.mfAddRowGrid(this.uGridCustomerP, 0); 

                this.uGridCustomer.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(uGridCustomer_DoubleClickRow);
                this.uGridCustomerP.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridCustomerP_CellChange);
                this.uGridCustomerP.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(uGridCustomerP_AfterCellUpdate);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }
        
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();
                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "고객담당자정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                this.uGroupBoxContentsArea.ExpandedStateChanging += new CancelEventHandler(uGroupBoxContentsArea_ExpandedStateChanging);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바기능
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clscustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clscustomer);
                WinMessageBox msg = new WinMessageBox();
                // PrograssBar 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 조회 Method 호출
                DataTable dtCustomer = clscustomer.mfReadCustomer();
                this.uGridCustomer.DataSource = dtCustomer;
                this.uGridCustomer.DataBind();

                // PrograssBar 종료
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // ContentGroupBox 접은상태로
                this.uGroupBoxContentsArea.Expanded = false;

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움
                
                if (dtCustomer.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridCustomer, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();

                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001264", "M001027", "M001028", Infragistics.Win.HAlign.Right);

                    this.uGroupBoxContentsArea.Expanded = true;
                }
                else
                {
                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                    QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                    brwChannel.mfCredentials(clsCustomer);

                    String strCustomerCode = this.uTextCustomerCode.Text;
                    DataTable dtSaveCusP = clsCustomer.mfSetDataInfo();
                    DataRow row;

                    string strLang = m_resSys.GetString("SYS_LANG");
                    // 필수 입력사항 확인
                    for (int i = 0; i < this.uGridCustomerP.Rows.Count; i++)
                    {
                        this.uGridCustomerP.ActiveCell = this.uGridCustomerP.Rows[0].Cells[0];

                        if (this.uGridCustomerP.Rows[i].Hidden == false)
                        {
                            //if (this.uGridCustomerP.Rows[i].RowSelectorAppearance.Image != null)
                            //{
                            if (this.uGridCustomerP.Rows[i].Cells["PersonName"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                                , this.uGridCustomerP.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000564",strLang)
                                                , Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridCustomerP.ActiveCell = this.uGridCustomerP.Rows[i].Cells["PersonName"];
                                this.uGridCustomerP.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else
                            {
                                row = dtSaveCusP.NewRow();
                                row["CustomerCode"] = this.uTextCustomerCode.Text;
                                row["Seq"] = this.uGridCustomerP.Rows[i].RowSelectorNumber;
                                row["DeptName"] = this.uGridCustomerP.Rows[i].Cells["DeptName"].Value.ToString();
                                row["PersonName"] = this.uGridCustomerP.Rows[i].Cells["PersonName"].Value.ToString();
                                row["Position"] = this.uGridCustomerP.Rows[i].Cells["Position"].Value.ToString();
                                row["Tel"] = this.uGridCustomerP.Rows[i].Cells["Tel"].Value.ToString();
                                row["Hp"] = this.uGridCustomerP.Rows[i].Cells["Hp"].Value.ToString();
                                row["EMail"] = this.uGridCustomerP.Rows[i].Cells["EMail"].Value.ToString();
                                row["EtcDesc"] = this.uGridCustomerP.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtSaveCusP.Rows.Add(row);
                            }
                            //}
                        }
                    }

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                                , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 처리 로직 //
                        // Method 호출
                        String rtMSG = clsCustomer.mfDeleteCustomerP(strCustomerCode, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"), dtSaveCusP);

                        // Decoding //
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        // 처리로직 끝//

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 처리결과에 따른 메세지 박스
                        System.Windows.Forms.DialogResult result;
                        if (ErrRtn.ErrNum == 0)
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                            this.uGroupBoxContentsArea.Expanded = false;
                        }
                        else
                        {
                            result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000953",
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {
                ////// 펼침상태가 false 인경우

                ////if (this.uGroupBoxContentsArea.Expanded == false)
                ////{
                ////    this.uGroupBoxContentsArea.Expanded = true;
                ////}
                ////// 이미 펼쳐진 상태이면 컴포넌트 초기화
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }

        public void mfExcel()
        {
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            if (this.uGridCustomer.Rows.Count > 0)
                grd.mfDownLoadGridToExcel(this.uGridCustomer);
        }
        #endregion

        #region 이벤트
        // ContensGroupBox ExpandeStateChanging
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGridCustomer.Height = 80;
                    Point point = new Point(0, 120);
                    this.uGroupBoxContentsArea.Location = point;
                }
                else
                {
                    Point point = new Point(0, 815);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridCustomer.Height = 791;

                    this.uGridCustomer.Rows.FixedRows.Clear();

                    Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        void uGridCustomerP_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridCustomerP, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // CellChange Event 셀 수정시 RowSelector 이미지 변경
        private void uGridCustomerP_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 고객 그리드 더블클릭시
        private void uGridCustomer_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // ContentsArea가 접혀있을경우 펼쳐주고 현재 클릭된 행을 고정
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Row.Fixed = true;
                }

                String strCustomerCode = e.Row.Cells["CustomerCode"].Value.ToString();

                // BL 연결
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                brwChannel.mfCredentials(clsCustomer);

                WinMessageBox msg = new WinMessageBox();
                // PrograssBar 생성
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 상새고객 정보
                DataTable dtCustomer = clsCustomer.mfReadCustomerDetail(strCustomerCode, m_resSys.GetString("SYS_LANG"));

                this.uTextAddress.Text = dtCustomer.Rows[0]["Address"].ToString();
                this.uTextBossName.Text = dtCustomer.Rows[0]["BossName"].ToString();
                this.uTextCustomerCode.Text = dtCustomer.Rows[0]["CustomerCode"].ToString();
                this.uTextCustomerName.Text = dtCustomer.Rows[0]["CustomerName"].ToString();
                this.uTextCustomerNameCh.Text = dtCustomer.Rows[0]["CustomerNameCh"].ToString();
                this.uTextCustomerNameEn.Text = dtCustomer.Rows[0]["CustomerNameEn"].ToString();
                this.uTextFax.Text = dtCustomer.Rows[0]["Fax"].ToString();
                this.uTextRegNo.Text = dtCustomer.Rows[0]["RegNo"].ToString();
                this.uTextTel.Text = dtCustomer.Rows[0]["Tel"].ToString();
                this.uTextUseFlag.Text = dtCustomer.Rows[0]["UseFlag"].ToString();

                // 고객담당자 정보 조회
                DataTable dtCustomerP = clsCustomer.mfReadCustomerP(strCustomerCode);

                this.uGridCustomerP.DataSource = dtCustomerP;
                this.uGridCustomerP.DataBind();
                
                // 담당자명 편집 불가 상태로
                for (int i = 0; i < this.uGridCustomerP.Rows.Count; i++)
                {
                    this.uGridCustomerP.Rows[i].Cells["PersonName"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridCustomerP.Rows[i].Cells["PersonName"].Appearance.BackColor = Color.Gainsboro;
                }

                WinGrid grd = new WinGrid();
                grd.mfSetAutoResizeColWidth(this.uGridCustomerP, 0);

                // PrograssBar 종료
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 행삭제 이벤트
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridCustomerP.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridCustomerP.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridCustomerP.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void Clear()
        {
            try
            {
                this.uTextAddress.Clear();
                this.uTextBossName.Clear();
                this.uTextCustomerCode.Clear();
                this.uTextCustomerName.Clear();
                this.uTextCustomerNameCh.Clear();
                this.uTextCustomerNameEn.Clear();
                this.uTextFax.Clear();
                this.uTextRegNo.Clear();
                this.uTextTel.Clear();
                this.uTextUseFlag.Clear();

                while (this.uGridCustomerP.Rows.Count > 0)
                {
                    this.uGridCustomerP.Rows[0].Delete(false);
                }

                this.uGridCustomerP.DisplayLayout.Bands[0].Columns["PersonName"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMAS0031_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmMAS0031_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

                ////if (this.Width > 1070 && this.Height > 850)
                ////{
                ////    ////this.uGridCustomer.Dock = DockStyle.Fill;
                ////    ////this.uGroupBoxContentsArea.Dock = DockStyle.Bottom;

                ////    this.uGridCustomer.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                ////    this.uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                ////    this.uGroupBoxContentsArea.Height = this.Height - titleArea.Height - 95 - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////    this.uGridCustomer.Height = this.Height - titleArea.Height - this.uGroupBoxContentsArea.Height - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////}
                ////else
                ////{
                ////    this.uGridCustomer.Dock = DockStyle.None;
                ////    this.uGroupBoxContentsArea.Dock = DockStyle.None;

                ////    if (this.Width > 1070 && this.Height < 850)
                ////    {
                ////        this.uGridCustomer.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                ////        this.uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                ////        this.uGridCustomer.Height = 850 - titleArea.Height - this.uGroupBoxContentsArea.Height - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////        //this.uGroupBoxContentsArea.Height = 850 - titleArea.Height - this.uGridCustomer.Height - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////        this.uGroupBoxContentsArea.Height = 755;
                ////    }
                ////    else if (this.Width < 1070 && this.Height > 850)
                ////    {
                ////        this.uGridCustomer.Height = this.Height - titleArea.Height - this.uGroupBoxContentsArea.Height - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////        this.uGroupBoxContentsArea.Height = this.Height - titleArea.Height - this.uGridCustomer.Height - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////        this.uGridCustomer.Width = 1070 - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                ////        this.uGroupBoxContentsArea.Width = 1070 - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                ////    }
                ////    else
                ////    {
                ////        this.uGroupBoxContentsArea.Height = this.Height - titleArea.Height - 95 - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////        this.uGridCustomer.Height = 850 - titleArea.Height - this.uGroupBoxContentsArea.Height - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////        //this.uGroupBoxContentsArea.Height = 850 - titleArea.Height - this.uGridCustomer.Height - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight;
                ////        this.uGroupBoxContentsArea.Height = 755;
                ////        this.uGridCustomer.Width = 1070;
                ////        this.uGroupBoxContentsArea.Width = 1070;
                ////    }
                ////}

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
