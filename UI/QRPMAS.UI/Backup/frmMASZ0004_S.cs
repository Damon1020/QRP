﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMASZ0004.cs                                        */
/* 프로그램명   : 설비점검그룹정보등록                                  */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-05 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

using System.IO;

using QRPMAS.BL;

namespace QRPMAS.UI
{
    public partial class frmMASZ0004_S : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0004_S()
        {
            InitializeComponent();
        }

        private void frmMASZ0004_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0004_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("설비점검그룹정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitButton();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitTab();
            InitValue();

            //비활성화처리 : 설비그룹은 MDM에서 가지고 오므로 편집불가
            uComboPlant.Enabled = false;
            uTextEquipGroupCode.Enabled = false;
            uTextEquipGroupName.Enabled = false;
            uTextEquipGroupNameCh.Enabled = false;
            uTextEquipGroupNameEn.Enabled = false;
            uComboUseFlag.Enabled = false;

            // ContentGroupBox 닫힘상태로
            this.uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }


        #region 컨트롤초기화

        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                // 서버 연결 체크 //
                if (brwChannel.mfCheckQRPServer() == false) return;

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitValue()
        {
            try
            {
                this.uTextEquipGroupCode.Text = "";
                this.uTextEquipGroupName.Text = "";
                this.uTextEquipGroupNameCh.Text = "";
                this.uTextEquipGroupNameEn.Text = "";

                this.uComboPlant.Value = "";
                this.uComboUseFlag.Value = "T";
                
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEquipGroupCode, "설비점검그룹코드", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEquipGroupName, "설비점검그룹명", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEquipGroupNameCh, "설비점검그룹명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEquipGroupNameEn, "설비점검그룹명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, true);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonUpLoad, "UpLoad", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Fileupload);
                wButton.mfSetButton(this.uButtonDeleteRow1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonDeleteRow2, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

                uButtonDeleteRow1.Visible = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                // 사용여부
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                // 서버 연결 체크 //
                if (brwChannel.mfCheckQRPServer() == false) return;

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                

                wCombo.mfSetComboEditor(this.uComboUseFlag, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                    , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", ""
                    , ComboDefaultValue("S", m_resSys.GetString("SYS_LANG")), "ComCode", "ComCodeName", dtUseFlag);

                clsCommonCode.Dispose();
                dtUseFlag.Dispose();
                // PlantComboBox
                // Call BL
                
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                clsPlant.Dispose();
                // SearchPlant ComboBox
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), ""
                    , ComboDefaultValue("A", m_resSys.GetString("SYS_LANG"))
                    , "PlantCode", "PlantName", dtPlant);
                // Plant ComboBox
                wCombo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), ""
                    , ComboDefaultValue("S", m_resSys.GetString("SYS_LANG"))
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 설비점검그룹Grid
                // 설비점검그룹 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquip, 0, "PlantCode", "공장", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipGroupCode", "설비그룹코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipGroupName", "설비그룹명", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipGroupNameCh", "설비그룹명_중문", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipGroupNameEn", "설비그룹명_영문", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "MACHINEGROUPACTIONTYPE", "설비그룹구분코드", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "MACHINEGROUPACTIONTYPEName", "설비그룹구분명", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "MODEL", "설비모델", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "MAKER", "메이커", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipImageFile", "설비이미지화일명", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "UseFlag", "사용여부", true, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "T");

                // 폰트설정
                this.uGridEquip.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquip.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 점검그룹 설비리스트
                //2012-08-24 분기,반기,년간 -> 분기점검 | 반기점검 | 년기점검 변경
                // 점검그룹 설비리스트 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                string strLang = m_resSys.GetString("SYS_LANG");
                string strName = "";

                if (strLang.Equals("KOR"))
                    strName = "주간점검,월간점검,분기점검,반기점검,년간점검";
                else if (strLang.Equals("CHN"))
                    strName = "每周点检,每月点检,季度点检,半年点检,年间点检";
                else if (strLang.Equals("ENG"))
                    strName = "주간점검,월간점검,분기점검,반기점검,년간점검";


                string[] strGroups = strName.Split(',');
                string strDefault = ComboDefaultValue("S", strLang);

                // Set GridGroup
                this.uGridEquipList.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "group1", strGroups[0], 7, 0, 1, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "group2", strGroups[1], 8, 0, 2, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQUA = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "group3", strGroups[2], 10, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHAF = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupHAF", strGroups[3], 13, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupYear = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupYEA", strGroups[4], 16, 0, 3, 2, false);

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false", 0, 0, 1, 2, null);

                //wGrid.mfSetGridColumn(this.uGrid2, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                //-->설비에 대한 설비그룹은 MDM에서 정하기 때문에 설비편집 불가 처리
                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "AreaCode", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "StationCode", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocCode", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunCode", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 4, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "", 5, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMWeekDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "", 0, 0, 1, 1, group1);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "0", 0, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "", 1, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "0", 0, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "0", 1, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "", 2, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "0", 0, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "0", 1, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "", 2, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "0", 0, 0, 1, 1, uGroupYear);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "0", 1, 0, 1, 1, uGroupYear);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "", 2, 0, 1, 1, uGroupYear);


                #region DropDown
                //-------------------------------------------------------------------------------------------------------------------------------------------------
                //공장 콤보박스
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //사용여부 콤보박스
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(uGridEquip, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtPlant);
                wGrid.mfSetGridColumnValueList(uGridEquip, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                GridCombo();


                //점검요일 콤보박스
                //BL호출

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode CommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(CommonCode);
                //매서드호출
                DataTable dtWeekDay = CommonCode.mfReadCommonCode("C0006", m_resSys.GetString("SYS_LANG"));

                //정보 그리드에 넣기
                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMWeekDay", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefault, dtWeekDay);

                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMMonthDay", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefault, dtWeekDay);

                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMQuarterDay", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefault, dtWeekDay);

                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMHalfDay", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefault, dtWeekDay);

                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMYearDay", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefault, dtWeekDay);

                //---------------------------------------------------------------------------------------------------------------------------------------------------
                //점검주간 콤보박스
                DataTable dtPMWeek = new DataTable();
                dtPMWeek.Columns.Add("Key", typeof(int));
                dtPMWeek.Columns.Add("Value", typeof(string));
                DataRow drPMWeek;
                //1~5주차 
                int intCnt = 1;
                for (int i = 0; i < 5; i++)
                {

                    drPMWeek = dtPMWeek.NewRow();
                    drPMWeek["Key"] = intCnt;
                    drPMWeek["Value"] = intCnt.ToString();
                    dtPMWeek.Rows.Add(drPMWeek);

                    intCnt++;
                }
                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMMonthWeek", Infragistics.Win.ValueListDisplayStyle.DisplayText, "0", strDefault, dtPMWeek);
                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMYearWeek", Infragistics.Win.ValueListDisplayStyle.DisplayText, "0", strDefault, dtPMWeek);
                //2012-08-24 추가
                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMQuarterWeek", Infragistics.Win.ValueListDisplayStyle.DisplayText, "0", strDefault, dtPMWeek);
                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMHalfWeek", Infragistics.Win.ValueListDisplayStyle.DisplayText, "0", strDefault, dtPMWeek);


                //점검월
                DataTable dtPMMonth = new DataTable();
                dtPMMonth.Columns.Add("Key", typeof(int));
                dtPMMonth.Columns.Add("Value", typeof(string));
                //1월~12월
                intCnt = 1;
                for (int i = 0; i < 12; i++)
                {

                    drPMWeek = dtPMMonth.NewRow();
                    drPMWeek["Key"] = intCnt;
                    drPMWeek["Value"] = intCnt.ToString();
                    dtPMMonth.Rows.Add(drPMWeek);

                    intCnt++;
                }

                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMYearMonth", Infragistics.Win.ValueListDisplayStyle.DisplayText, "0", strDefault, dtPMMonth);

                //2012-08-24 추가
                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMQuarterMonth", Infragistics.Win.ValueListDisplayStyle.DisplayText, "0", strDefault, dtPMMonth);
                wGrid.mfSetGridColumnValueList(uGridEquipList, 0, "PMHalfMonth", Infragistics.Win.ValueListDisplayStyle.DisplayText, "0", strDefault, dtPMMonth);

                group1.CellAppearance.ForeColor = Color.Black;
                group2.CellAppearance.ForeColor = Color.Black;
                uGroupQUA.CellAppearance.ForeColor = Color.Black;
                uGroupHAF.CellAppearance.ForeColor = Color.Black;
                uGroupYear.CellAppearance.ForeColor = Color.Black;
                
                //---------------------------------------------------------------------------------------------------------------------------------------------------
                #endregion

                // 공백추가
                wGrid.mfAddRowGrid(this.uGridEquipList, 0);

                // 폰트설정
                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 점검그룹정비사리스트

                // 점검그룹 정비사 리스트 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridTechnicianList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridTechnicianList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                //wGrid.mfSetGridColumn(this.uGrid3, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 10
                //    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTechnicianList, 0, "UserID", "정비사ID", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTechnicianList, 0, "UserName", "정비사명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTechnicianList, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTechnicianList, 0, "Position", "직위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTechnicianList, 0, "TelNum", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridTechnicianList, 0, "HpNum", "핸드폰번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 빈줄생성
                wGrid.mfAddRowGrid(this.uGridTechnicianList, 0);

                // 폰트지정
                this.uGridTechnicianList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridTechnicianList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                #endregion
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Tab컨트롤초기화
        /// </summary>
        private void InitTab()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinTabControl tab = new WinTabControl();

                tab.mfInitGeneralTabControl(this.uTabEquipGroup, Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.PropertyPage,
                                                Infragistics.Win.UltraWinTabs.TabCloseButtonVisibility.Never, Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.Default
                                                , m_resSys.GetString("SYS_FONTNAME"));

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">S : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun, string strLang)
        {
            try
            {
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                strLang = strLang.ToUpper();
                if (strGubun.Equals("S"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        #endregion

        #region 툴바
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //공장코드
                string strPlnatCode = uComboSearchPlant.Value.ToString();
                WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();

                //ProgessBar 창띄우기
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL호출
                QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup EquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChnnel.mfCredentials(EquipGroup);
                //매서드호출
                DataTable dtEquipGroup = EquipGroup.mfReadEquipGroup(strPlnatCode, m_resSys.GetString("SYS_LANG"));

                //데이터바인드
                this.uGridEquip.DataSource = dtEquipGroup;
                this.uGridEquip.DataBind();

                //커서변경
                this.MdiParent.Cursor = Cursors.Default;

                //ProgressBar닫기
                m_ProgressPopup.mfCloseProgressPopup(this);

                //grd.mfClearRowSeletorGrid(this.uGridEquip);
             
                //데이터가없을경우
                if (dtEquipGroup.Rows.Count == 0)
                {
                    
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M000221", "M000222", Infragistics.Win.HAlign.Right);

                }
                else
                {
                    grd.mfSetAutoResizeColWidth(this.uGridEquip, 0);
                }

                // ContentGroupBox 접은상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG");
                #region 컬럼정보 DataTabel저장
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                //설비그룹 컬럼Set
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChannel.mfCredentials(clsEquipGroup);

                DataTable dtEquipGroup = clsEquipGroup.mfSetDateInfo();

                //설비 컬럼Set
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtEquip = clsEquip.mfEquipDataSet();

                DataTable dtDeleteEquip = clsEquip.mfEquipDataSet();

                //설비그룹정비사 컬럼Set
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroupP), "EquipGroupP");
                QRPMAS.BL.MASEQU.EquipGroupP clsEquipGroupP = new QRPMAS.BL.MASEQU.EquipGroupP();
                brwChannel.mfCredentials(clsEquipGroupP);

                DataTable dtEquipGroupP = clsEquipGroupP.mfSetDataInfo();

                DataTable dtDeleteGroupP = clsEquipGroupP.mfSetDataInfo();

                #endregion

                #region 설비점검그룹정보등록 정보저장

                if (this.uGroupBoxContentsArea.Expanded !=false)
                {
                    #region 필수입력사항확인

                    if (uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                        if (this.uGroupBoxContentsArea.Expanded == false)
                        {
                            this.uGroupBoxContentsArea.Expanded = true;
                            while (this.uGridEquip.Rows.Count > 0)
                            {
                                this.uGridEquip.Rows[0].Delete(false);
                            }
                        }
                        // Focus Cell
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (uTextEquipGroupCode.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000713", Infragistics.Win.HAlign.Right);

                        this.uTextEquipGroupCode.Focus();
                        return;
                    }
                    else if (uTextEquipGroupName.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001230", "M000711", Infragistics.Win.HAlign.Right);

                        // Focus Cell
                        uTextEquipGroupName.Focus();
                        return;
                    }
                    else if (uComboUseFlag.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001230", "M000619", Infragistics.Win.HAlign.Right);

                        // Focus Cell
                        uComboUseFlag.DropDown();
                        return;
                    }
                    #endregion
                    else
                    {
                        DataRow drEquipGroup;
                        drEquipGroup = dtEquipGroup.NewRow();
                        drEquipGroup["PlantCode"] = this.uComboPlant.Value.ToString();
                        drEquipGroup["EquipGroupCode"] = this.uTextEquipGroupCode.Text;
                        drEquipGroup["EquipGroupName"] = this.uTextEquipGroupName.Text;
                        drEquipGroup["EquipGroupNameCh"] = this.uTextEquipGroupNameCh.Text;
                        drEquipGroup["EquipGroupNameEn"] = this.uTextEquipGroupNameEn.Text;

                        //경로명이 있으면 화일명을 변경하여 DB에 저장
                        string strDBImageFile = "";
                        if (@uTextEquipImageFile.Text.Contains(":\\"))
                        {
                            FileInfo fileImg = new FileInfo(uTextEquipImageFile.Text);
                            strDBImageFile = uComboPlant.Value.ToString() + "-" + uTextEquipGroupCode.Text + "-" + fileImg.Name;
                        }
                        else
                            strDBImageFile = uTextEquipImageFile.Text;

                        drEquipGroup["EquipImageFile"] = strDBImageFile;
                        drEquipGroup["UseFlag"] = this.uComboUseFlag.Value.ToString();
                        dtEquipGroup.Rows.Add(drEquipGroup);

                    }

                    if (this.uGridEquipList.Rows.Count > 0)
                    {
                        //--Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[0].Cells[0];
                    }

                    //점검그룹 설비리스트 필수입력사항체크                    
                    for (int i = 0; i < this.uGridEquipList.Rows.Count; i++)
                    {
                        if (this.uGridEquipList.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (this.uGridEquipList.Rows[i].Hidden == false)
                            {
                                #region 필수입력사항확인

                                string strNumber = this.uGridEquipList.Rows[i].RowSelectorNumber.ToString();
                                if (this.uGridEquipList.Rows[i].Cells["EquipCode"].Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                                         , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                         , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001230",strLang)
                                                         , strNumber + msg.GetMessge_Text("M000458",strLang)
                                                         , Infragistics.Win.HAlign.Right);

                                    //TabControl 이동
                                    if (uTabEquipGroup.SelectedTab.Index != 0)
                                    {
                                        this.uTabEquipGroup.Tabs[0].Selected = true;
                                    }
                                    // Focus Cell
                                    this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[i].Cells["EquipCode"];
                                    this.uGridEquipList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }
                                #endregion
                                
                                DataRow drEquip;
                                drEquip = dtEquip.NewRow();
                                drEquip["EquipGroupCode"] = this.uTextEquipGroupCode.Text;
                                drEquip["PlantCode"] = this.uComboPlant.Value.ToString();
                                drEquip["EquipCode"] = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value;
                                
                                drEquip["PMWeekDay"] = this.uGridEquipList.Rows[i].Cells["PMWeekDay"].Value;

                                drEquip["PMMonthWeek"] = this.uGridEquipList.Rows[i].Cells["PMMonthWeek"].Value.ToString().Equals(string.Empty) ? 0 : this.uGridEquipList.Rows[i].Cells["PMMonthWeek"].Value;//
                                drEquip["PMMonthDay"] = this.uGridEquipList.Rows[i].Cells["PMMonthDay"].Value;

                                drEquip["PMYearMonth"] = this.uGridEquipList.Rows[i].Cells["PMYearMonth"].Value.ToString().Equals(string.Empty) ? 0 : this.uGridEquipList.Rows[i].Cells["PMYearMonth"].Value;//
                                drEquip["PMYearWeek"] = this.uGridEquipList.Rows[i].Cells["PMYearWeek"].Value.ToString().Equals(string.Empty) ? 0 : this.uGridEquipList.Rows[i].Cells["PMYearWeek"].Value;//
                                drEquip["PMYearDay"] = this.uGridEquipList.Rows[i].Cells["PMYearDay"].Value;

                                // 2012-08-24 분기점검,반기점검 정보 저장 (권종구)//
                                drEquip["PMQuarterDay"] = this.uGridEquipList.Rows[i].Cells["PMQuarterDay"].Value;
                                drEquip["PMQuarterWeek"] = this.uGridEquipList.Rows[i].Cells["PMQuarterWeek"].Value.ToString().Equals(string.Empty) ? 0 : this.uGridEquipList.Rows[i].Cells["PMQuarterWeek"].Value;
                                drEquip["PMQuarterMonth"] = this.uGridEquipList.Rows[i].Cells["PMQuarterMonth"].Value.ToString().Equals(string.Empty) ? 0 : this.uGridEquipList.Rows[i].Cells["PMQuarterMonth"].Value;

                                drEquip["PMHalfDay"] = this.uGridEquipList.Rows[i].Cells["PMHalfDay"].Value;
                                drEquip["PMHalfWeek"] = this.uGridEquipList.Rows[i].Cells["PMHalfWeek"].Value.ToString().Equals(string.Empty) ? 0 : this.uGridEquipList.Rows[i].Cells["PMHalfWeek"].Value;
                                drEquip["PMHalfMonth"] = this.uGridEquipList.Rows[i].Cells["PMHalfMonth"].Value.ToString().Equals(string.Empty) ? 0 : this.uGridEquipList.Rows[i].Cells["PMHalfMonth"].Value;
                                // --------------------------------------------- //
                                
                                dtEquip.Rows.Add(drEquip);
                                
                            }
                            else
                            {
                                DataRow drDeleteEquip;
                                drDeleteEquip = dtDeleteEquip.NewRow();
                                drDeleteEquip["PlantCode"] = uComboPlant.Value.ToString();
                                drDeleteEquip["EquipCode"] = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value.ToString();
                                drDeleteEquip["EquipGroupCode"] = this.uTextEquipGroupCode.Text;
                                dtDeleteEquip.Rows.Add(drDeleteEquip);
                            }
                        }




                    }

                    //////    //--Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    //////    this.uGridTechnicianList.ActiveCell = this.uGridTechnicianList.Rows[0].Cells[0];
                    //점검그룹 정비사리스트 필수입력사항체크 ==> 정비사별 설비호기 지정으로 변경
                    //////for (int i = 0; i < this.uGridTechnicianList.Rows.Count; i++)
                    //////{
                    //////    if (this.uGridTechnicianList.Rows[i].RowSelectorAppearance.Image != null)
                    //////    {
                    //////        if (this.uGridTechnicianList.Rows[i].Hidden == false)
                    //////        {

                    //////            if (this.uGridTechnicianList.Rows[i].Cells["UserID"].Value.ToString() == "")
                    //////            {
                    //////                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                    //////                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //////                 "확인창", "필수입력사항확인", (i + 1) + "번째 정비사ID를 입력해주세요.", Infragistics.Win.HAlign.Right);

                    //////                //TabControl 이동
                    //////                if (uTabEquipGroup.SelectedTab.Index != 1)
                    //////                {
                    //////                    this.uTabEquipGroup.Tabs[1].Selected = true;
                    //////                }

                    //////                // Focus Cell
                    //////                this.uGridTechnicianList.ActiveCell = this.uGridTechnicianList.Rows[i].Cells["UserID"];
                    //////                this.uGridTechnicianList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                    //////                return;
                    //////            }
                    //////            else
                    //////            {
                    //////                DataRow drGroupP;
                    //////                drGroupP = dtEquipGroupP.NewRow();
                    //////                drGroupP["PlantCode"] = this.uComboPlant.Value.ToString();
                    //////                drGroupP["EquipGroupCode"] = this.uTextEquipGroupCode.Text;
                    //////                drGroupP["UserID"] = this.uGridTechnicianList.Rows[i].Cells["UserID"].Value.ToString();
                    //////                dtEquipGroupP.Rows.Add(drGroupP);
                    //////            }
                    //////        }
                    //////        else
                    //////        {
                    //////            DataRow drDeleteEquipP;
                    //////            drDeleteEquipP = dtDeleteGroupP.NewRow();
                    //////            drDeleteEquipP["PlantCode"] = this.uComboPlant.Value.ToString();
                    //////            drDeleteEquipP["EquipGroupCode"] = this.uTextEquipGroupCode.Text;
                    //////            drDeleteEquipP["UserID"] = this.uGridTechnicianList.Rows[i].Cells["UserID"].Value.ToString();
                    //////            dtDeleteGroupP.Rows.Add(drDeleteEquipP);
                    //////        }
                    //////    }
                    //////}

                }
                #endregion
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M001024", "M001073",
                                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                //-----------------------------------------------필수 입력사항 체크등 끝------------------------------------------------//

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", m_resSys.GetString("SYS_LANG")));
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    string strRtn = clsEquipGroup.mfSaveEquipGroup_PSTS
                        (dtEquipGroup, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtEquip,dtDeleteEquip,dtEquipGroupP,dtDeleteGroupP);

                    //Decoding//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    //----//
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                        //이미지저장하기
                        UploadEquipImage();

                        #region ReBind
                        
                        this.uComboSearchPlant.Value = "";
                        if (this.uGroupBoxContentsArea.Expanded == true)
                        {
                            this.uGroupBoxContentsArea.Expanded = false;

                            //while (this.uGridEquip.Rows.Count > 0)
                            //{
                            //    this.uGridEquip.Rows[0].Delete(false);
                            //}
                        }

                        string strPlantCode = this.uComboSearchPlant.Value.ToString();

                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                        QRPMAS.BL.MASEQU.EquipGroup EquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                        brwChannel.mfCredentials(EquipGroup);
                        //매서드호출
                        DataTable dtReadEquipGroup = EquipGroup.mfReadEquipGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));

                        //데이터바인드
                        this.uGridEquip.DataSource = dtReadEquipGroup;
                        this.uGridEquip.DataBind();

                        #endregion

                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000953",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                //설비그룹 컬럼Set
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChannel.mfCredentials(clsEquipGroup);

                DataTable dtEquipGroup = clsEquipGroup.mfSetDateInfo();

                //설비 컬럼Set
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                DataTable dtDeleteEquip = clsEquip.mfEquipDataSet();

                //설비그룹정비사 컬럼Set
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroupP), "EquipGroupP");
                QRPMAS.BL.MASEQU.EquipGroupP clsEquipGroupP = new QRPMAS.BL.MASEQU.EquipGroupP();
                brwChannel.mfCredentials(clsEquipGroupP);

                DataTable dtDeleteGroupP = clsEquipGroupP.mfSetDataInfo();


                #region 설비점검그룸정보등록 삭제정보저장

                if (this.uGroupBoxContentsArea.Expanded != false)
                { 
                    #region 필수입력사항확인
                    if (uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);

                        // Focus Cell
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (uTextEquipGroupCode.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000713", Infragistics.Win.HAlign.Right);

                        this.uTextEquipGroupCode.Focus();
                        return;
                    }
                    else if (uTextEquipGroupName.Text == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001230", "M000711", Infragistics.Win.HAlign.Right);

                        // Focus Cell
                        uTextEquipGroupName.Focus();
                        return;
                    }
                    else if (uComboUseFlag.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001230", "M000619", Infragistics.Win.HAlign.Right);

                        // Focus Cell
                        uComboUseFlag.DropDown();
                        return;
                    }
                    #endregion
                    else
                    {
                        DataRow drEquipGroup;
                        drEquipGroup = dtEquipGroup.NewRow();
                        drEquipGroup["PlantCode"] = this.uComboPlant.Value.ToString();
                        drEquipGroup["EquipGroupCode"] = this.uTextEquipGroupCode.Text;
                        dtEquipGroup.Rows.Add(drEquipGroup);

                    }
                    //--Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[0].Cells[0];

                    //점검그룹 설비리스트 필수입력사항체크
                    for (int i = 0; i < this.uGridEquipList.Rows.Count; i++)
                    {


                        #region 필수입력사항확인

                        if (this.uGridEquipList.Rows[i].Cells["AreaCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000030", Infragistics.Win.HAlign.Right);

                            //TabControl 이동
                            if (uTabEquipGroup.SelectedTab.Index != 0)
                            {
                                this.uTabEquipGroup.Tabs[0].Selected = true;
                            }

                            // Focus Cell
                            this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[i].Cells["AreaCode"];
                            this.uGridEquipList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridEquipList.Rows[i].Cells["StationCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000128", Infragistics.Win.HAlign.Right);

                            //TabControl 이동
                            if (uTabEquipGroup.SelectedTab.Index != 0)
                            {
                                this.uTabEquipGroup.Tabs[0].Selected = true;
                            }

                            // Focus Cell
                            this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[i].Cells["StationCode"];
                            this.uGridEquipList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridEquipList.Rows[i].Cells["EquipLocCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000831", Infragistics.Win.HAlign.Right);

                            //TabControl 이동
                            if (uTabEquipGroup.SelectedTab.Index != 0)
                            {
                                this.uTabEquipGroup.Tabs[0].Selected = true;
                            }

                            // Focus Cell
                            this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[i].Cells["EquipLocCode"];
                            this.uGridEquipList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridEquipList.Rows[i].Cells["EquipProcGubunCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000691", Infragistics.Win.HAlign.Right);

                            //TabControl 이동
                            if (uTabEquipGroup.SelectedTab.Index != 0)
                            {
                                this.uTabEquipGroup.Tabs[0].Selected = true;
                            }

                            // Focus Cell
                            this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[i].Cells["EquipProcGubunCode"];
                            this.uGridEquipList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridEquipList.Rows[i].Cells["EquipCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000721", Infragistics.Win.HAlign.Right);

                            //TabControl 이동
                            if (uTabEquipGroup.SelectedTab.Index != 0)
                            {
                                this.uTabEquipGroup.Tabs[0].Selected = true;
                            }

                            // Focus Cell
                            this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[i].Cells["EquipCode"];
                            this.uGridEquipList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        #endregion

                        else
                        {
                            DataRow drDeleteEquip;
                            drDeleteEquip = dtDeleteEquip.NewRow();
                            drDeleteEquip["PlantCode"] = uComboPlant.Value.ToString();
                            drDeleteEquip["EquipCode"] = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value.ToString();
                            drDeleteEquip["EquipGroupCode"] = this.uTextEquipGroupCode.Text;
                            dtDeleteEquip.Rows.Add(drDeleteEquip);
                        }


                    }

                    if (this.uGridTechnicianList.Rows.Count > 0)
                    {
                        //--Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                        this.uGridTechnicianList.ActiveCell = this.uGridTechnicianList.Rows[0].Cells[0];

                        //점검그룹 정비사리스트 필수입력사항체크
                        for (int i = 0; i < this.uGridTechnicianList.Rows.Count; i++)
                        {

                            if (this.uGridTechnicianList.Rows[i].Cells["UserID"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001230", "M001077", Infragistics.Win.HAlign.Right);

                                //TabControl 이동
                                if (uTabEquipGroup.SelectedTab.Index != 1)
                                {
                                    this.uTabEquipGroup.Tabs[1].Selected = true;
                                }

                                // Focus Cell
                                this.uGridTechnicianList.ActiveCell = this.uGridTechnicianList.Rows[i].Cells["UserID"];
                                this.uGridTechnicianList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else
                            {
                                DataRow drDeleteEquipP;
                                drDeleteEquipP = dtDeleteGroupP.NewRow();
                                drDeleteEquipP["PlantCode"] = this.uComboPlant.Value.ToString();
                                drDeleteEquipP["EquipGroupCode"] = this.uTextEquipGroupCode.Text;
                                drDeleteEquipP["UserID"] = this.uGridTechnicianList.Rows[i].Cells["UserID"].Value.ToString();
                                dtDeleteGroupP.Rows.Add(drDeleteEquipP);
                            }

                        }
                    }
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001264", "M000650", "M000646",
                                                    Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", m_resSys.GetString("SYS_LANG")));
                    this.MdiParent.Cursor = Cursors.WaitCursor;  

                    //처리 로직//
                    
                    string strRtn = clsEquipGroup.mfDeleteEquipGroup(dtEquipGroup, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"), dtDeleteEquip, dtDeleteGroupP);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    /////////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);

                        //ReBind
                        if (this.uGroupBoxContentsArea.Expanded == true)
                        {
                            this.uGroupBoxContentsArea.Expanded = false;

                            //while (this.uGridEquip.Rows.Count > 0)
                            //{
                            //    this.uGridEquip.Rows[0].Delete(false);
                            //}
                        }

                        string strPlantCode = this.uComboPlant.Value.ToString();

                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                        QRPMAS.BL.MASEQU.EquipGroup EquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                        brwChannel.mfCredentials(EquipGroup);
                        //매서드호출
                        DataTable dtReadEquipGroup = EquipGroup.mfReadEquipGroup(strPlantCode, m_resSys.GetString("SYS_LANG"));

                        //데이터바인드
                        this.uGridEquip.DataSource = dtReadEquipGroup;
                        this.uGridEquip.DataBind();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    while (this.uGridEquip.Rows.Count > 0)
                    {
                        this.uGridEquip.Rows[0].Delete(false);
                    }
                    InitValue();
                    //탭컨트롤 인덱스 기본값으로
                    if (uTabEquipGroup.SelectedTab.Index != 0)
                    {
                        this.uTabEquipGroup.Tabs[0].Selected = true;
                    }
                    uComboPlant.ReadOnly =false;
                    uTextEquipGroupCode.ReadOnly = false;
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
                else
                {
                    //탭컨트롤 인덱스 기본값으로
                    if (uTabEquipGroup.SelectedTab.Index != 0)
                    {
                        this.uTabEquipGroup.Tabs[0].Selected = true;
                    }
                    
                    while (this.uGridEquip.Rows.Count > 0)
                    {
                        this.uGridEquip.Rows[0].Delete(false);
                    }
                    while (this.uGridEquipList.Rows.Count > 0)
                    {
                        this.uGridEquipList.Rows[0].Delete(false);
                    }
                    while (this.uGridTechnicianList.Rows.Count > 0)
                    {
                        this.uGridTechnicianList.Rows[0].Delete(false);
                    }
                    InitValue();
                    uComboPlant.ReadOnly = false;
                    uTextEquipGroupCode.ReadOnly = false;
                }

                //상세 컨트롤 초기화
                uPicEquipImage.Image = null;
                uTextEquipImageFile.Text = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }
        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquip.Rows.Count == 0 && (this.uGridEquipList.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000811", "M000712", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                if(this.uGridEquip.Rows.Count > 0)
                grd.mfDownLoadGridToExcel(this.uGridEquip);

                if(this.uGridEquipList.Rows.Count >0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                grd.mfDownLoadGridToExcel(this.uGridEquipList);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        // Contents GroupBox 
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquip.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquip.Height = 740;

                    for (int i = 0; i < uGridEquip.Rows.Count; i++)
                    {
                        uGridEquip.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally { }
        }

        //행삭제 버튼
        private void uButtonDeleteRow2_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridTechnicianList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridTechnicianList.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridTechnicianList.Rows[i].Hidden = true;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        //행삭제 버튼
        private void uButtonDeleteRow1_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridEquipList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridEquipList.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridEquipList.Rows[i].Hidden = true;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGridEquipList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

        }

        //RowSelector란에 편집이미지 를 나타나게 한다
        private void uGridTechnicianList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

        }

        //점검그룹정비사리스트에 관한 AfterCellUpdate이벤트
        private void uGridTechnicianList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                
                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridTechnicianList, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //점검그룹설비리스트에 관한 AfterCellUpdate 이벤트
        private void uGridEquipList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                
                WinMessageBox msg = new WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
               
                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridEquipList, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                //컬럼
                String strColumn = e.Cell.Column.Key;
                //줄번호
                int intIndex = e.Cell.Row.Index;
                //공장코드
                string strPlantCode = uComboPlant.Value.ToString();

                if (strColumn == "AreaCode" || strColumn == "StationCode" || strColumn == "EquipLocCode" || strColumn == "EquipProcGubunCode")
                {
                    //콤보값들
                    
                    string strArea = e.Cell.Row.Cells["AreaCode"].Value.ToString();
                    string strStation = e.Cell.Row.Cells["StationCode"].Value.ToString();
                    string strLocation = e.Cell.Row.Cells["EquipLocCode"].Value.ToString();
                    string strGubun = e.Cell.Row.Cells["EquipProcGubunCode"].Value.ToString();
                    
                    GridComboList(strPlantCode, strArea, strStation, strLocation, strGubun, intIndex);
                    
                }
                if (strColumn == "EquipCode")
                {
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();
                    //BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    DataTable dtEquipName = clsEquip.mfReadEquipName(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    e.Cell.Row.Cells["EquipName"].Value = dtEquipName.Rows[0]["EquipName"];
                }

                

                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //더블클릭한 행의 정보보기
        private void uGridEquip_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //ProgessBar 창띄우기
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                
                //내용이 있는경우 실행
                if (!grd.mfCheckCellDataInRow(this.uGridTechnicianList, 0, e.Cell.Row.Index))
                {
                    //Contents 펼침상태에따라
                    if (uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;
                        
                    }

                    e.Cell.Row.Fixed = true;

                    //탭컨트롤 인덱스 기본값으로
                    if (uTabEquipGroup.SelectedTab.Index != 0)
                    {
                        this.uTabEquipGroup.Tabs[0].Selected = true;
                    }

                    uComboPlant.Value = e.Cell.Row.Cells["PlantCode"].Value;
                    uTextEquipGroupCode.Text = e.Cell.Row.Cells["EquipGroupCode"].Value.ToString();
                    uTextEquipGroupName.Text = e.Cell.Row.Cells["EquipGroupName"].Value.ToString();
                    uTextEquipGroupNameCh.Text = e.Cell.Row.Cells["EquipGroupNameCh"].Value.ToString();
                    uTextEquipGroupNameEn.Text = e.Cell.Row.Cells["EquipGroupNameEn"].Value.ToString();
                    uTextEquipImageFile.Text = e.Cell.Row.Cells["EquipImageFile"].Value.ToString();
                    uComboUseFlag.Value = e.Cell.Row.Cells["UseFlag"].Value;

                    //PK수정불가처리
                    uComboPlant.ReadOnly = true;
                    uTextEquipGroupCode.ReadOnly = true;

                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipGroupCode = e.Cell.Row.Cells["EquipGroupCode"].Value.ToString();

                    

                    //BL호출
                    QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                    brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    QRPMAS.BL.MASEQU.EquipGroup EquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    brwChnnel.mfCredentials(EquipGroup);

                    //매서드호출
                    DataTable dtEquipList = EquipGroup.mfReadEquipGroupList_PSTS(strPlantCode, strEquipGroupCode, m_resSys.GetString("SYS_LANG"));


                    //테이터바인드
                    uGridEquipList.DataSource = dtEquipList;
                    uGridEquipList.DataBind();

                    for (int i = 0; i < this.uGridEquipList.Rows.Count; i++)
                    {
                        string strArea = this.uGridEquipList.Rows[i].Cells["AreaCode"].Value.ToString();
                        string strStation = this.uGridEquipList.Rows[i].Cells["StationCode"].Value.ToString();
                        string strLoc = this.uGridEquipList.Rows[i].Cells["EquipLocCode"].Value.ToString();
                        string strGubun = this.uGridEquipList.Rows[i].Cells["EquipProcGubunCode"].Value.ToString();
                        GridComboList(strPlantCode, strArea, strStation, strLoc, strGubun, i);
                    }
                    
                    grd.mfSetAutoResizeColWidth(this.uGridEquipList, 0);

                    ////brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroupP), "EquipGroupP");
                    ////QRPMAS.BL.MASEQU.EquipGroupP EquipGroupP = new QRPMAS.BL.MASEQU.EquipGroupP();
                    ////brwChnnel.mfCredentials(EquipGroupP);

                    //////매서드호출
                    ////DataTable dtEquipGroupP = EquipGroupP.mfReadEquipGroupP(strPlantCode, strEquipGroupCode, m_resSys.GetString("SYS_LANG"));

                    //////데이터바인드
                    ////this.uGridTechnicianList.DataSource = dtEquipGroupP;
                    ////this.uGridTechnicianList.DataBind();

                    //////////////////////////////////////////////////////////////////////////////////////////////////////
                    //설비이미지 Download받아 보여주기
                    uPicEquipImage.Image = null;
                    if (uTextEquipImageFile.Text != "")
                    {
                        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                        // 서버 연결 체크 //
                        if (brwChannel.mfCheckQRPServer() == false) return;

                        //화일서버 연결정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAccess);
                        DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");

                        //설비이미지 저장경로정보 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);
                        DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0001");

                        //설비이미지 Upload하기
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        ArrayList arrFile = new ArrayList();
                        arrFile.Add(uTextEquipImageFile.Text);

                        //Download정보 설정
                        string strExePath = Application.ExecutablePath;
                        int intPos = strExePath.LastIndexOf(@"\");
                        strExePath = strExePath.Substring(0, intPos + 1);

                        fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                               dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();
                        
                        //Download받은 화일을 이미지에 보여줌
                        string strImageFile = strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + uTextEquipImageFile.Text;
                        Bitmap img = (Bitmap)Bitmap.FromFile(strImageFile);
                        uPicEquipImage.AutoSize = false;
                        uPicEquipImage.Image = img;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                }

                //커서변경
                this.MdiParent.Cursor = Cursors.Default;

                //ProgressBar닫기
                m_ProgressPopup.mfCloseProgressPopup(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //공장이 바뀔 때 마다 발생되는 이벤트
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                while (this.uGridEquipList.Rows.Count > 0)
                {
                    this.uGridEquipList.Rows[0].Delete(false);
                }
                while (this.uGridTechnicianList.Rows.Count > 0)
                {
                    this.uGridTechnicianList.Rows[0].Delete(false);
                }

                GridCombo();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //정비사 아이디 입력후 Enter키 누를시 발생되는 이벤트
        private void uGridTechnicianList_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Enter)
                {
                    WinMessageBox msg = new WinMessageBox();
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    int intRow = uGridTechnicianList.ActiveRow.Index;

                    string strValue = uGridTechnicianList.ActiveCell.Text.ToString();

                    //공백 확인
                    if (strValue == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);
                        return;
                    }
                    else if (uComboPlant.Value.ToString() == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000280", "M000266", Infragistics.Win.HAlign.Right);
                        this.uComboPlant.DropDown();
                        return;
                    }
                    string strPlantCode = uComboPlant.Value.ToString();



                    SearchUser(strPlantCode, strValue, intRow);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        //Area,Station,위치,공정구분 콤보박스 누르는 순간 발생되는 이벤트
        private void uGridEquipList_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                String strColumn = e.Cell.Column.Key;
                if (strColumn == "AreaCode" || strColumn == "StationCode" || strColumn == "EquipLocCode" || strColumn == "EquipProcGubunCode" || strColumn == "EquipCode")
                {
                    if (uComboPlant.Value.ToString() == "")
                    {
                        WinMessageBox meg = new WinMessageBox();
                        meg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "M001224", "M001234", "M000266", Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                        return;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //정비사 그리드버튼 클릭시 자동조회
        private void uGridTechnicianList_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {


                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPMAS.UI.frmPOP0011 frmUser = new frmPOP0011();
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                if (frmUser.PlantCode != "" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000268", Infragistics.Win.HAlign.Right);

                    return;
                }


                uGridTechnicianList.Rows[e.Cell.Row.Index].Cells["UserID"].Value = frmUser.UserID;
                uGridTechnicianList.Rows[e.Cell.Row.Index].Cells["UserName"].Value = frmUser.UserName;
                uGridTechnicianList.Rows[e.Cell.Row.Index].Cells["DeptName"].Value = frmUser.DeptName;
                uGridTechnicianList.Rows[e.Cell.Row.Index].Cells["Position"].Value = frmUser.Position;
                uGridTechnicianList.Rows[e.Cell.Row.Index].Cells["TelNum"].Value = frmUser.TelNum;
                uGridTechnicianList.Rows[e.Cell.Row.Index].Cells["HpNum"].Value = frmUser.HpNum;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 그리드 콤보
        /// <summary>
        /// 공장선택시 Area , Station ,위치,설비공정구분 콤보박스
        /// </summary>
        private void GridCombo()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();
                //----Area , Station ,위치,설비공정구분 콤보박스---

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                // 서버 연결 체크 //
                if (brwChannel.mfCheckQRPServer() == false) return;

                string strPlantCode = uComboPlant.Value.ToString();
                
                //uGridEquip.Rows[].Cells[].ValueList.DeActivate
                //uGridEquip.Rows
                ////Area
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                brwChannel.mfCredentials(clsArea);
                DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode ,m_resSys.GetString("SYS_LANG"));

                //Station
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                brwChannel.mfCredentials(clsStation);

                DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //위치
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                brwChannel.mfCredentials(clsEquipLocation);
                DataTable dtLocation = clsEquipLocation.mfReadLocationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //공정구분
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipProcGubun), "EquipProcGubun");
                QRPMAS.BL.MASEQU.EquipProcGubun clsEquipProcGubun = new QRPMAS.BL.MASEQU.EquipProcGubun();
                brwChannel.mfCredentials(clsEquipProcGubun);
                DataTable dtGubun = clsEquipProcGubun.mfReadProGubunCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                string strDefalut = ComboDefaultValue("S", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridEquipList, 0, "AreaCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefalut, dtArea);
                wGrid.mfSetGridColumnValueList(this.uGridEquipList, 0, "StationCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefalut, dtStation);
                wGrid.mfSetGridColumnValueList(this.uGridEquipList, 0, "EquipLocCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefalut, dtLocation);
                wGrid.mfSetGridColumnValueList(this.uGridEquipList, 0, "EquipProcGubunCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefalut, dtGubun);



                //if (dtArea.Rows.Count == 0 || || )
                //{
                //    WinMessageBox msg = new WinMessageBox();
                //    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                //    System.Windows.Forms.DialogResult result;
                //    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                                "처리결과", "처리결과", "Area결과가 없습니다.", Infragistics.Win.HAlign.Right);
                //}



            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비코드 리스트
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strArea">Area코드</param>
        /// <param name="strStation">Station코드</param>
        /// <param name="strLoc">위치코드</param>
        /// <param name="strGubun">구분코드</param>
        /// <param name="intIndex">RowIndex</param>
        private void GridComboList(string strPlantCode,string strArea,string strStation,string strLoc,string strGubun,int intIndex)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChnnel.mfCredentials(clsEquip);

                //매서드호출
                DataTable dtEquipList = clsEquip.mfReadEquipListCombo(strPlantCode, strArea, strStation, strLoc, strGubun, m_resSys.GetString("SYS_LANG"));



                if (dtEquipList.Rows.Count != 0)
                {
                    //this.uGridEquipList.Rows[intIndex].Cells["EquipCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    
                    WinGrid grd = new WinGrid();
                    string strDropDownGridHKey = "EquipCode,EquipName,SuperEquipCode,ModelName,SerialNo,EquipTypeName,EquipGroupName,VendorName";
                    string strDropDownGridHName = "설비코드,설비명,Super설비,모델,SerialNo,설비유형,설비그룹명,Vendor";
                    grd.mfSetGridCellValueGridList(this.uGridEquipList,0, intIndex,"EquipCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, strDropDownGridHKey, strDropDownGridHName, "EquipCode", "EquipCode", dtEquipList);

                }
               

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion


        #region 화일 관련
        /// <summary>
        /// 설비이미지화일을 선택하여 PictureBox에 보여주고, 이미 저장된 정보면 화일서버에 Upload시킨다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonUpLoad_Click(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (uComboPlant.Value.ToString() == "")
                {
                    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboPlant.DropDown();
                    return;
                }

                //폴더 및 화일선택
                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "Image files (*.bmp, *.jpg, *.gif)|*.bmp;*.jpg;*gif"; //excel files (*.xlsx)|*.xlsx";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strImageFile = openFile.FileName;
                    uTextEquipImageFile.Text = strImageFile;

                    Bitmap img = (Bitmap)Bitmap.FromFile(strImageFile);
                    uPicEquipImage.AutoSize = false;
                    uPicEquipImage.Image = img;
                }

                //PK코드를 수정할 수 없는 경우 바로 화일서버로 Upload함 (아닌 경우 저장할 때 화일서버에 Upload함)//
                if (uTextEquipImageFile.Text != "" && uTextEquipGroupCode.ReadOnly == true)
                    UploadEquipImage();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 화일서버에 Upload시킨다.
        /// </summary>
        private void UploadEquipImage()
        {
            frmCOMFileAttach fileAtt = new frmCOMFileAttach();
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //Drive 및 Path가 있는 경우에 한해서 Upload시킨다.
                if (@uTextEquipImageFile.Text.Contains(":\\"))
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    // 서버 연결 체크 //
                    if (brwChannel.mfCheckQRPServer() == false) return;

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uComboPlant.Value.ToString(), "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uComboPlant.Value.ToString(), "D0001");

                    //설비이미지 Upload하기
                    ArrayList arrFile = new ArrayList();
                    
                    //화일이름변경(공장코드+설비점검그룹코드+화일명)하여 복사하기//
                    FileInfo fileImg = new FileInfo(uTextEquipImageFile.Text);
                    string strUploadImageFile = fileImg.DirectoryName + "\\" + 
                                                uComboPlant.Value.ToString() + "-" + uTextEquipGroupCode.Text + "-" + fileImg.Name;
                    //변경한 화일이 있으면 삭제하기
                    if (File.Exists(strUploadImageFile))
                        File.Delete(strUploadImageFile);
                    //변경한 화일이름으로 복사하기
                    File.Copy(uTextEquipImageFile.Text, strUploadImageFile);
                    arrFile.Add(strUploadImageFile);

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                               dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                               dtFilePath.Rows[0]["FolderName"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                               dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.mfFileUploadNoProgView();

                    //변경한 이름 삭제하기 //
                    //if (File.Exists(strUploadImageFile))
                    //    File.Delete(strUploadImageFile);

                    //테이블에 이미지화일명 저장
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                    QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                    brwChannel.mfCredentials(clsEquipGroup);
                    string strRes = clsEquipGroup.mfSaveEquipGroupImageFile(uComboPlant.Value.ToString(),
                                                                               uTextEquipGroupCode.Text,
                                                                               uComboPlant.Value.ToString() + "-" + uTextEquipGroupCode.Text + "-" + fileImg.Name,
                                                                               m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
                fileAtt.Dispose();
            }
        }

        #endregion

        /// <summary>
        /// 정비사 조회
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strUserID">입력받은아이디</param>
        private void SearchUser(string strPlantCode, string strUserID,int intRow)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User User = new QRPSYS.BL.SYSUSR.User();
                brwChnnel.mfCredentials(User);

                //매서드호출
                DataTable dtEquipP = User.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                //정보가 없을 시
                if (dtEquipP.Rows.Count == 0)
                {
                    WinMessageBox msg = new WinMessageBox();
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    this.uGridTechnicianList.Rows[intRow].Cells["UserID"].Value = "";
                    return;
                }


                uGridTechnicianList.Rows[intRow].Cells["UserID"].Value = dtEquipP.Rows[0]["UserID"];
                uGridTechnicianList.Rows[intRow].Cells["UserName"].Value = dtEquipP.Rows[0]["UserName"];
                uGridTechnicianList.Rows[intRow].Cells["DeptName"].Value = dtEquipP.Rows[0]["DeptName"];
                uGridTechnicianList.Rows[intRow].Cells["Position"].Value = dtEquipP.Rows[0]["Position"];
                uGridTechnicianList.Rows[intRow].Cells["TelNum"].Value = dtEquipP.Rows[0]["TelNum"];
                uGridTechnicianList.Rows[intRow].Cells["HpNum"].Value = dtEquipP.Rows[0]["HpNum"];
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMASZ0004_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmMASZ0004_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
