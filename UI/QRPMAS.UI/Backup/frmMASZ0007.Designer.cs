﻿namespace QRPMAS.UI
{
    partial class frmMASZ0007
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0007));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridDeviceChgPMH = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextRevisionReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevisionReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRejectReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRejectReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextWriteID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTextEtcDesc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitStatus = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.uDateWriteDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTextWriteName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStdNumber = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboEquipType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSysInspect = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgPMH)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl1)).BeginInit();
            this.uTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonDeleteRow);
            this.ultraTabPageControl1.Controls.Add(this.uGridDeviceChgPMH);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1040, 574);
            // 
            // uButtonDeleteRow
            // 
            appearance72.FontData.BoldAsString = "False";
            appearance72.Image = global::QRPMAS.UI.Properties.Resources.btn_delTable;
            this.uButtonDeleteRow.Appearance = appearance72;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 12);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 58;
            this.uButtonDeleteRow.Text = "행삭제";
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGridDeviceChgPMH
            // 
            this.uGridDeviceChgPMH.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDeviceChgPMH.DisplayLayout.Appearance = appearance24;
            this.uGridDeviceChgPMH.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDeviceChgPMH.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMH.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgPMH.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.uGridDeviceChgPMH.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDeviceChgPMH.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.uGridDeviceChgPMH.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDeviceChgPMH.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDeviceChgPMH.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDeviceChgPMH.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.uGridDeviceChgPMH.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDeviceChgPMH.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMH.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDeviceChgPMH.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGridDeviceChgPMH.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDeviceChgPMH.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDeviceChgPMH.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.uGridDeviceChgPMH.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.uGridDeviceChgPMH.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDeviceChgPMH.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.uGridDeviceChgPMH.DisplayLayout.Override.RowAppearance = appearance34;
            this.uGridDeviceChgPMH.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDeviceChgPMH.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.uGridDeviceChgPMH.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDeviceChgPMH.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDeviceChgPMH.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDeviceChgPMH.Location = new System.Drawing.Point(12, 40);
            this.uGridDeviceChgPMH.Name = "uGridDeviceChgPMH";
            this.uGridDeviceChgPMH.Size = new System.Drawing.Size(1004, 528);
            this.uGridDeviceChgPMH.TabIndex = 1;
            this.uGridDeviceChgPMH.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            this.uGridDeviceChgPMH.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridDeviceChgPMH_CellChange);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGrid2);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1040, 574);
            // 
            // uGrid2
            // 
            this.uGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid2.DisplayLayout.Appearance = appearance12;
            this.uGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.uGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.uGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance17.BackColor = System.Drawing.SystemColors.Highlight;
            appearance17.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance17;
            this.uGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            appearance19.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid2.DisplayLayout.Override.CellAppearance = appearance19;
            this.uGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance20.BackColor = System.Drawing.SystemColors.Control;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance20;
            appearance21.TextHAlignAsString = "Left";
            this.uGrid2.DisplayLayout.Override.HeaderAppearance = appearance21;
            this.uGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.uGrid2.DisplayLayout.Override.RowAppearance = appearance22;
            this.uGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance23;
            this.uGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid2.Location = new System.Drawing.Point(12, 12);
            this.uGrid2.Name = "uGrid2";
            this.uGrid2.Size = new System.Drawing.Size(1024, 552);
            this.uGrid2.TabIndex = 3;
            this.uGrid2.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid2_AfterCellUpdate);
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uTextRevisionReason);
            this.uGroupBox1.Controls.Add(this.uLabelRevisionReason);
            this.uGroupBox1.Controls.Add(this.uTextRejectReason);
            this.uGroupBox1.Controls.Add(this.uLabelRejectReason);
            this.uGroupBox1.Controls.Add(this.uTextAdmitID);
            this.uGroupBox1.Controls.Add(this.uTextWriteID);
            this.uGroupBox1.Controls.Add(this.uTabControl1);
            this.uGroupBox1.Controls.Add(this.uTextEtcDesc);
            this.uGroupBox1.Controls.Add(this.uLabel9);
            this.uGroupBox1.Controls.Add(this.uTextAdmitStatus);
            this.uGroupBox1.Controls.Add(this.uLabel8);
            this.uGroupBox1.Controls.Add(this.uTextAdmitName);
            this.uGroupBox1.Controls.Add(this.uLabel7);
            this.uGroupBox1.Controls.Add(this.uLabel6);
            this.uGroupBox1.Controls.Add(this.uDateWriteDate);
            this.uGroupBox1.Controls.Add(this.uTextWriteName);
            this.uGroupBox1.Controls.Add(this.uLabel5);
            this.uGroupBox1.Controls.Add(this.uTextVersionNum);
            this.uGroupBox1.Controls.Add(this.uLabel2);
            this.uGroupBox1.Controls.Add(this.uTextStdNumber);
            this.uGroupBox1.Controls.Add(this.uLabel1);
            this.uGroupBox1.Location = new System.Drawing.Point(0, 80);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1068, 768);
            this.uGroupBox1.TabIndex = 8;
            // 
            // uTextRevisionReason
            // 
            appearance3.BackColor = System.Drawing.Color.Transparent;
            this.uTextRevisionReason.Appearance = appearance3;
            this.uTextRevisionReason.BackColor = System.Drawing.Color.Transparent;
            this.uTextRevisionReason.Location = new System.Drawing.Point(144, 132);
            this.uTextRevisionReason.Name = "uTextRevisionReason";
            this.uTextRevisionReason.ReadOnly = true;
            this.uTextRevisionReason.Size = new System.Drawing.Size(788, 21);
            this.uTextRevisionReason.TabIndex = 64;
            // 
            // uLabelRevisionReason
            // 
            this.uLabelRevisionReason.Location = new System.Drawing.Point(12, 132);
            this.uLabelRevisionReason.Name = "uLabelRevisionReason";
            this.uLabelRevisionReason.Size = new System.Drawing.Size(125, 20);
            this.uLabelRevisionReason.TabIndex = 63;
            this.uLabelRevisionReason.Text = "ultraLabel1";
            // 
            // uTextRejectReason
            // 
            this.uTextRejectReason.Location = new System.Drawing.Point(144, 108);
            this.uTextRejectReason.Name = "uTextRejectReason";
            this.uTextRejectReason.Size = new System.Drawing.Size(784, 21);
            this.uTextRejectReason.TabIndex = 59;
            // 
            // uLabelRejectReason
            // 
            this.uLabelRejectReason.Location = new System.Drawing.Point(12, 108);
            this.uLabelRejectReason.Name = "uLabelRejectReason";
            this.uLabelRejectReason.Size = new System.Drawing.Size(125, 20);
            this.uLabelRejectReason.TabIndex = 58;
            this.uLabelRejectReason.Text = "ultraLabel7";
            // 
            // uTextAdmitID
            // 
            appearance10.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextAdmitID.Appearance = appearance10;
            this.uTextAdmitID.BackColor = System.Drawing.Color.PowderBlue;
            appearance11.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance11;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextAdmitID.ButtonsRight.Add(editorButton1);
            this.uTextAdmitID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAdmitID.Location = new System.Drawing.Point(144, 60);
            this.uTextAdmitID.Name = "uTextAdmitID";
            this.uTextAdmitID.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitID.TabIndex = 57;
            this.uTextAdmitID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextAdmitID_KeyDown);
            this.uTextAdmitID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextAdmitID_EditorButtonClick);
            // 
            // uTextWriteID
            // 
            appearance36.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextWriteID.Appearance = appearance36;
            this.uTextWriteID.BackColor = System.Drawing.Color.PowderBlue;
            appearance37.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance37.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance37;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextWriteID.ButtonsRight.Add(editorButton2);
            this.uTextWriteID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextWriteID.Location = new System.Drawing.Point(144, 36);
            this.uTextWriteID.Name = "uTextWriteID";
            this.uTextWriteID.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteID.TabIndex = 56;
            this.uTextWriteID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextWriteID_KeyDown);
            this.uTextWriteID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextWriteID_EditorButtonClick);
            // 
            // uTabControl1
            // 
            this.uTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabControl1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabControl1.Controls.Add(this.ultraTabPageControl1);
            this.uTabControl1.Controls.Add(this.ultraTabPageControl2);
            this.uTabControl1.Location = new System.Drawing.Point(12, 156);
            this.uTabControl1.Name = "uTabControl1";
            this.uTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabControl1.Size = new System.Drawing.Size(1044, 600);
            this.uTabControl1.TabIndex = 27;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "점검항목상세";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "점검그룹설비리스트";
            ultraTab2.Visible = false;
            this.uTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1040, 574);
            // 
            // uTextEtcDesc
            // 
            this.uTextEtcDesc.Location = new System.Drawing.Point(144, 84);
            this.uTextEtcDesc.Name = "uTextEtcDesc";
            this.uTextEtcDesc.Size = new System.Drawing.Size(784, 21);
            this.uTextEtcDesc.TabIndex = 26;
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(12, 84);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(125, 20);
            this.uLabel9.TabIndex = 25;
            this.uLabel9.Text = "ultraLabel7";
            // 
            // uTextAdmitStatus
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitStatus.Appearance = appearance8;
            this.uTextAdmitStatus.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitStatus.Location = new System.Drawing.Point(520, 60);
            this.uTextAdmitStatus.Name = "uTextAdmitStatus";
            this.uTextAdmitStatus.ReadOnly = true;
            this.uTextAdmitStatus.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitStatus.TabIndex = 24;
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(388, 60);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(125, 20);
            this.uLabel8.TabIndex = 23;
            this.uLabel8.Text = "ultraLabel6";
            // 
            // uTextAdmitName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitName.Appearance = appearance2;
            this.uTextAdmitName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitName.Location = new System.Drawing.Point(248, 60);
            this.uTextAdmitName.Name = "uTextAdmitName";
            this.uTextAdmitName.ReadOnly = true;
            this.uTextAdmitName.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitName.TabIndex = 22;
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 60);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(125, 20);
            this.uLabel7.TabIndex = 20;
            this.uLabel7.Text = "ultraLabel2";
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(388, 36);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(125, 20);
            this.uLabel6.TabIndex = 19;
            this.uLabel6.Text = "ultraLabel2";
            // 
            // uDateWriteDate
            // 
            appearance7.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.Appearance = appearance7;
            this.uDateWriteDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateWriteDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateWriteDate.Location = new System.Drawing.Point(520, 36);
            this.uDateWriteDate.Name = "uDateWriteDate";
            this.uDateWriteDate.Size = new System.Drawing.Size(100, 21);
            this.uDateWriteDate.TabIndex = 18;
            // 
            // uTextWriteName
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Appearance = appearance9;
            this.uTextWriteName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextWriteName.Location = new System.Drawing.Point(248, 36);
            this.uTextWriteName.Name = "uTextWriteName";
            this.uTextWriteName.ReadOnly = true;
            this.uTextWriteName.Size = new System.Drawing.Size(100, 21);
            this.uTextWriteName.TabIndex = 17;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(12, 36);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(125, 20);
            this.uLabel5.TabIndex = 15;
            this.uLabel5.Text = "ultraLabel2";
            // 
            // uTextVersionNum
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Appearance = appearance5;
            this.uTextVersionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Location = new System.Drawing.Point(248, 12);
            this.uTextVersionNum.Name = "uTextVersionNum";
            this.uTextVersionNum.ReadOnly = true;
            this.uTextVersionNum.Size = new System.Drawing.Size(28, 21);
            this.uTextVersionNum.TabIndex = 10;
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(388, 12);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(125, 20);
            this.uLabel2.TabIndex = 9;
            this.uLabel2.Text = "ultraLabel2";
            this.uLabel2.Visible = false;
            // 
            // uTextStdNumber
            // 
            appearance4.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStdNumber.Appearance = appearance4;
            this.uTextStdNumber.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextStdNumber.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextStdNumber.Location = new System.Drawing.Point(144, 12);
            this.uTextStdNumber.Name = "uTextStdNumber";
            this.uTextStdNumber.Size = new System.Drawing.Size(100, 21);
            this.uTextStdNumber.TabIndex = 8;
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(12, 12);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(125, 20);
            this.uLabel1.TabIndex = 7;
            this.uLabel1.Text = "ultraLabel2";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboEquipType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSysInspect);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 7;
            // 
            // uComboEquipType
            // 
            this.uComboEquipType.Location = new System.Drawing.Point(384, 12);
            this.uComboEquipType.Name = "uComboEquipType";
            this.uComboEquipType.Size = new System.Drawing.Size(240, 21);
            this.uComboEquipType.TabIndex = 6;
            this.uComboEquipType.Text = "uCombo";
            this.uComboEquipType.AfterCloseUp += new System.EventHandler(this.uComboEquipType_AfterCloseUp);
            this.uComboEquipType.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.uComboEquipGroup_BeforeDropDown);
            // 
            // uLabelSysInspect
            // 
            this.uLabelSysInspect.Location = new System.Drawing.Point(280, 12);
            this.uLabelSysInspect.Name = "uLabelSysInspect";
            this.uLabelSysInspect.Size = new System.Drawing.Size(100, 20);
            this.uLabelSysInspect.TabIndex = 5;
            this.uLabelSysInspect.Text = "ultraLabel2";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 4;
            this.uComboPlant.Text = "uCombo";
            this.uComboPlant.AfterCloseUp += new System.EventHandler(this.uComboPlant_AfterCloseUp);
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 2;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 6;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmMASZ0007
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.Controls.Add(this.uGroupBox1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0007";
            this.Load += new System.EventHandler(this.frmMASZ0007_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0007_Activated);
            this.Resize += new System.EventHandler(this.frmMASZ0007_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDeviceChgPMH)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            this.uGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl1)).EndInit();
            this.uTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtcDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateWriteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextWriteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStdNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDeviceChgPMH;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtcDesc;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitStatus;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitName;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateWriteDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteName;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVersionNum;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStdNumber;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipType;
        private Infragistics.Win.Misc.UltraLabel uLabelSysInspect;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextWriteID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRejectReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRejectReason;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevisionReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRevisionReason;

    }
}