﻿namespace QRPMAS.UI
{
    partial class frmMASZ0004_S
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        
        
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0004));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridTechnicianList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow2 = new Infragistics.Win.Misc.UltraButton();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquip = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextEquipImageFile = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonUpLoad = new Infragistics.Win.Misc.UltraButton();
            this.uPicEquipImage = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.uTabEquipGroup = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uComboUseFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridTechnicianList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipImageFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabEquipGroup)).BeginInit();
            this.uTabEquipGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uGridEquipList);
            this.ultraTabPageControl1.Controls.Add(this.uButtonDeleteRow1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1044, 430);
            // 
            // uGridEquipList
            // 
            this.uGridEquipList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipList.DisplayLayout.Appearance = appearance26;
            this.uGridEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.uGridEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.uGridEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            appearance33.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipList.DisplayLayout.Override.CellAppearance = appearance33;
            this.uGridEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance34.BackColor = System.Drawing.SystemColors.Control;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance34;
            appearance35.TextHAlignAsString = "Left";
            this.uGridEquipList.DisplayLayout.Override.HeaderAppearance = appearance35;
            this.uGridEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipList.DisplayLayout.Override.RowAppearance = appearance36;
            this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance37;
            this.uGridEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipList.Location = new System.Drawing.Point(12, 20);
            this.uGridEquipList.Name = "uGridEquipList";
            this.uGridEquipList.Size = new System.Drawing.Size(1028, 400);
            this.uGridEquipList.TabIndex = 1;
            this.uGridEquipList.Text = "ultraGrid1";
            this.uGridEquipList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipList_AfterCellUpdate);
            this.uGridEquipList.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.uGridEquipList_BeforeCellListDropDown);
            this.uGridEquipList.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquipList_CellChange);
            // 
            // uButtonDeleteRow1
            // 
            this.uButtonDeleteRow1.Location = new System.Drawing.Point(12, 12);
            this.uButtonDeleteRow1.Name = "uButtonDeleteRow1";
            this.uButtonDeleteRow1.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow1.TabIndex = 0;
            this.uButtonDeleteRow1.Text = "ultraButton1";
            this.uButtonDeleteRow1.Click += new System.EventHandler(this.uButtonDeleteRow1_Click);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridTechnicianList);
            this.ultraTabPageControl2.Controls.Add(this.uButtonDeleteRow2);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1044, 430);
            // 
            // uGridTechnicianList
            // 
            this.uGridTechnicianList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance14.BackColor = System.Drawing.SystemColors.Window;
            appearance14.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridTechnicianList.DisplayLayout.Appearance = appearance14;
            this.uGridTechnicianList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridTechnicianList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTechnicianList.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTechnicianList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridTechnicianList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridTechnicianList.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.uGridTechnicianList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridTechnicianList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridTechnicianList.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridTechnicianList.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridTechnicianList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridTechnicianList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridTechnicianList.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridTechnicianList.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridTechnicianList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridTechnicianList.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridTechnicianList.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.uGridTechnicianList.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridTechnicianList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridTechnicianList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridTechnicianList.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridTechnicianList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridTechnicianList.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.uGridTechnicianList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridTechnicianList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridTechnicianList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridTechnicianList.Location = new System.Drawing.Point(12, 44);
            this.uGridTechnicianList.Name = "uGridTechnicianList";
            this.uGridTechnicianList.Size = new System.Drawing.Size(1028, 548);
            this.uGridTechnicianList.TabIndex = 2;
            this.uGridTechnicianList.Text = "ultraGrid1";
            this.uGridTechnicianList.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridTechnicianList_AfterCellUpdate);
            this.uGridTechnicianList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uGridTechnicianList_KeyDown);
            this.uGridTechnicianList.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridTechnicianList_ClickCellButton);
            this.uGridTechnicianList.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridTechnicianList_CellChange);
            // 
            // uButtonDeleteRow2
            // 
            this.uButtonDeleteRow2.Location = new System.Drawing.Point(12, 12);
            this.uButtonDeleteRow2.Name = "uButtonDeleteRow2";
            this.uButtonDeleteRow2.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow2.TabIndex = 1;
            this.uButtonDeleteRow2.Text = "ultraButton1";
            this.uButtonDeleteRow2.Click += new System.EventHandler(this.uButtonDeleteRow2_Click);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridEquip
            // 
            this.uGridEquip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquip.DisplayLayout.Appearance = appearance5;
            this.uGridEquip.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquip.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridEquip.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridEquip.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquip.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquip.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquip.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridEquip.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquip.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquip.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridEquip.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquip.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridEquip.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridEquip.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquip.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquip.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquip.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridEquip.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquip.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquip.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquip.Location = new System.Drawing.Point(0, 80);
            this.uGridEquip.Name = "uGridEquip";
            this.uGridEquip.Size = new System.Drawing.Size(1070, 740);
            this.uGridEquip.TabIndex = 2;
            this.uGridEquip.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridEquip_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 120);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipImageFile);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uButtonUpLoad);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uPicEquipImage);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabEquipGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroupNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroupNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroupNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroupNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroupName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroupName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroupCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroupCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextEquipImageFile
            // 
            this.uTextEquipImageFile.Location = new System.Drawing.Point(424, 212);
            this.uTextEquipImageFile.MaxLength = 50;
            this.uTextEquipImageFile.Name = "uTextEquipImageFile";
            this.uTextEquipImageFile.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipImageFile.TabIndex = 16;
            this.uTextEquipImageFile.Visible = false;
            // 
            // uButtonUpLoad
            // 
            this.uButtonUpLoad.Location = new System.Drawing.Point(328, 204);
            this.uButtonUpLoad.Name = "uButtonUpLoad";
            this.uButtonUpLoad.Size = new System.Drawing.Size(88, 28);
            this.uButtonUpLoad.TabIndex = 14;
            this.uButtonUpLoad.Text = "ultraButton1";
            this.uButtonUpLoad.Click += new System.EventHandler(this.uButtonUpLoad_Click);
            // 
            // uPicEquipImage
            // 
            this.uPicEquipImage.BorderShadowColor = System.Drawing.Color.Empty;
            this.uPicEquipImage.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uPicEquipImage.Location = new System.Drawing.Point(12, 12);
            this.uPicEquipImage.Name = "uPicEquipImage";
            this.uPicEquipImage.Size = new System.Drawing.Size(304, 220);
            this.uPicEquipImage.TabIndex = 13;
            // 
            // uTabEquipGroup
            // 
            this.uTabEquipGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabEquipGroup.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabEquipGroup.Controls.Add(this.ultraTabPageControl1);
            this.uTabEquipGroup.Controls.Add(this.ultraTabPageControl2);
            this.uTabEquipGroup.Location = new System.Drawing.Point(12, 236);
            this.uTabEquipGroup.Name = "uTabEquipGroup";
            this.uTabEquipGroup.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabEquipGroup.Size = new System.Drawing.Size(1048, 456);
            this.uTabEquipGroup.TabIndex = 12;
            ultraTab1.Key = "0";
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "점검그룹설비리스트";
            ultraTab2.Key = "1";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "점검그룹 정비사리스트";
            ultraTab2.Visible = false;
            this.uTabEquipGroup.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1044, 430);
            // 
            // uComboUseFlag
            // 
            this.uComboUseFlag.Location = new System.Drawing.Point(808, 84);
            this.uComboUseFlag.MaxLength = 10;
            this.uComboUseFlag.Name = "uComboUseFlag";
            this.uComboUseFlag.Size = new System.Drawing.Size(150, 21);
            this.uComboUseFlag.TabIndex = 6;
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(664, 84);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(140, 20);
            this.uLabelUseFlag.TabIndex = 10;
            this.uLabelUseFlag.Text = "ultraLabel1";
            // 
            // uTextEquipGroupNameEn
            // 
            this.uTextEquipGroupNameEn.Location = new System.Drawing.Point(464, 84);
            this.uTextEquipGroupNameEn.MaxLength = 50;
            this.uTextEquipGroupNameEn.Name = "uTextEquipGroupNameEn";
            this.uTextEquipGroupNameEn.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipGroupNameEn.TabIndex = 5;
            // 
            // uLabelEquipGroupNameEn
            // 
            this.uLabelEquipGroupNameEn.Location = new System.Drawing.Point(320, 84);
            this.uLabelEquipGroupNameEn.Name = "uLabelEquipGroupNameEn";
            this.uLabelEquipGroupNameEn.Size = new System.Drawing.Size(140, 20);
            this.uLabelEquipGroupNameEn.TabIndex = 8;
            this.uLabelEquipGroupNameEn.Text = "ultraLabel3";
            // 
            // uTextEquipGroupNameCh
            // 
            this.uTextEquipGroupNameCh.Location = new System.Drawing.Point(464, 60);
            this.uTextEquipGroupNameCh.MaxLength = 50;
            this.uTextEquipGroupNameCh.Name = "uTextEquipGroupNameCh";
            this.uTextEquipGroupNameCh.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipGroupNameCh.TabIndex = 4;
            // 
            // uLabelEquipGroupNameCh
            // 
            this.uLabelEquipGroupNameCh.Location = new System.Drawing.Point(320, 60);
            this.uLabelEquipGroupNameCh.Name = "uLabelEquipGroupNameCh";
            this.uLabelEquipGroupNameCh.Size = new System.Drawing.Size(140, 20);
            this.uLabelEquipGroupNameCh.TabIndex = 6;
            this.uLabelEquipGroupNameCh.Text = "ultraLabel2";
            // 
            // uTextEquipGroupName
            // 
            appearance38.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipGroupName.Appearance = appearance38;
            this.uTextEquipGroupName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipGroupName.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipGroupName.Location = new System.Drawing.Point(464, 36);
            this.uTextEquipGroupName.MaxLength = 50;
            this.uTextEquipGroupName.Name = "uTextEquipGroupName";
            this.uTextEquipGroupName.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipGroupName.TabIndex = 3;
            // 
            // uLabelEquipGroupName
            // 
            this.uLabelEquipGroupName.Location = new System.Drawing.Point(320, 36);
            this.uLabelEquipGroupName.Name = "uLabelEquipGroupName";
            this.uLabelEquipGroupName.Size = new System.Drawing.Size(140, 20);
            this.uLabelEquipGroupName.TabIndex = 4;
            this.uLabelEquipGroupName.Text = "ultraLabel1";
            // 
            // uTextEquipGroupCode
            // 
            appearance39.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipGroupCode.Appearance = appearance39;
            this.uTextEquipGroupCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextEquipGroupCode.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextEquipGroupCode.Location = new System.Drawing.Point(808, 12);
            this.uTextEquipGroupCode.MaxLength = 10;
            this.uTextEquipGroupCode.Name = "uTextEquipGroupCode";
            this.uTextEquipGroupCode.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipGroupCode.TabIndex = 2;
            // 
            // uLabelEquipGroupCode
            // 
            this.uLabelEquipGroupCode.Location = new System.Drawing.Point(664, 12);
            this.uLabelEquipGroupCode.Name = "uLabelEquipGroupCode";
            this.uLabelEquipGroupCode.Size = new System.Drawing.Size(140, 20);
            this.uLabelEquipGroupCode.TabIndex = 2;
            this.uLabelEquipGroupCode.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(464, 12);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboPlant.TabIndex = 1;
            this.uComboPlant.Text = "전체";
            this.uComboPlant.ValueMember = " ";
            this.uComboPlant.ValueChanged += new System.EventHandler(this.uComboPlant_ValueChanged);
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(320, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(140, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // frmMASZ0004_S
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridEquip);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0004_S";
            this.Load += new System.EventHandler(this.frmMASZ0004_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0004_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMASZ0004_FormClosing);
            this.Resize += new System.EventHandler(this.frmMASZ0004_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridTechnicianList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipImageFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabEquipGroup)).EndInit();
            this.uTabEquipGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquip;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupNameCh;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabEquipGroup;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipList;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridTechnicianList;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow2;
        private Infragistics.Win.UltraWinEditors.UltraPictureBox uPicEquipImage;
        private Infragistics.Win.Misc.UltraButton uButtonUpLoad;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipImageFile;
    }
}