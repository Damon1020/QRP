﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMASZ0007.cs                                        */
/* 프로그램명   : 설비품종교체점검정보등록                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;


namespace QRPMAS.UI
{
    public partial class frmMASZ0007 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        string m_strAdmitStatusCode = "";
        string m_strSaveCheck = "";

        public frmMASZ0007()
        {
            InitializeComponent();
        }
        private void frmMASZ0007_Activated(object sender, EventArgs e)
        {
            //툴바활성화
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ToolButton.mfActiveToolBar(this.ParentForm, false, true, false, true, false, false, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0007_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀설정
            titleArea.mfSetLabelText("설비품종교체점검정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);
            //컨트롤초기화
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitButton();
            InitText();
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                //레이블설정
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                //lbl.mfSetLabel(this.uLabelSysInspect, "설비점검그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSysInspect, "설비유형", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabel1, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel2, "개정번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
                lbl.mfSetLabel(this.uLabel5, "생성자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel6, "생성일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel7, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                lbl.mfSetLabel(this.uLabel8, "승인상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel9, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRejectReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelRevisionReason, "개정사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 텍스트초기화
        /// </summary>
        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                this.uTextStdNumber.Enabled = false;
                this.uTextRejectReason.Enabled = false;
                this.uTextRevisionReason.Enabled = false;
                uTextWriteID.Text = m_resSys.GetString("SYS_USERID");
                uTextWriteName.Text = m_resSys.GetString("SYS_USERNAME");
                uTextAdmitID.Text = m_resSys.GetString("SYS_USERID");
                uTextAdmitName.Text = m_resSys.GetString("SYS_USERNAME");

                this.uTextStdNumber.Text = "";
                this.uTextVersionNum.Text = "";
                this.uTextAdmitStatus.Text = "";
                this.uTextEtcDesc.Text = "";
                this.uTextRejectReason.Text = "";
                this.uTextRevisionReason.Text = "";

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                
                //점검항목상세
                //--그리드기본설정
                grd.mfInitGeneralGrid(this.uGridDeviceChgPMH, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true,
                    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //--컬럼설정
                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 60, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "PMInspectGubun", "점검항목구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "PMInspectCriteria", "점검기준", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDeviceChgPMH, 0, "LevelCode", "난이도", false,  Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 170, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");
                
                //--Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridDeviceChgPMH.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDeviceChgPMH.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //한줄추가
                grd.mfAddRowGrid(this.uGridDeviceChgPMH, 0);


                ////////////점검설비리스트
                ////////////--그리드기본설정
                //////////grd.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, false,
                //////////    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                //////////    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                //////////    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                ////////////--헤더설정
                //////////this.uGrid2.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                //////////Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = grd.mfSetGridGroup(this.uGrid2, 0, "주간점검", "주간점검", true);
                //////////Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = grd.mfSetGridGroup(this.uGrid2, 0, "월간점검", "월간점검", true);
                //////////Infragistics.Win.UltraWinGrid.UltraGridGroup group3 = grd.mfSetGridGroup(this.uGrid2, 0, "분기,반기,년간", "분기,반기,년간", true);
                
                ////////////--컬럼설정
                //////////grd.mfSetGridColumn(this.uGrid2, 0, "순번", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10
                //////////    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",0,0,1,2,null);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",1,0,1,2,null);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",2,0,1,2,null);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 50
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",3,0,1,2,null);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",4,0,1,2,null);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //////////    , Infragistics.Win.HAlign.Center,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",5,0,1,2,null);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "",6,0,1,2,null);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "PMWeekDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 7, 0, 1, 1, group1);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "PMMonthWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 8, 0, 1, 1, group2);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "PMMonthDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 9, 0, 1, 1, group2);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "PMYearMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 10, 0, 1, 1, group3);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "PMYearWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 11, 0, 1, 1, group3);

                //////////grd.mfSetGridColumn(this.uGrid2, 0, "PMYearDay", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                //////////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //////////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 12, 0, 1, 1, group3);

                ////////////--Grid초기화 후 Font크기를 아래와 같이 적용
                //////////this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                //////////this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                ////////////빈줄추가
                //////////grd.mfAddRowGrid(this.uGrid2, 0);

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsSYS = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsSYS);

                DataTable dtLevel = clsSYS.mfReadCommonCode("C0008", m_resSys.GetString("SYS_LANG"));
                grd.mfSetGridColumnValueList(this.uGridDeviceChgPMH, 0, "LevelCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtLevel);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Call Method
                this.uComboPlant.Items.Clear();
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);

                //this.uComboEquipType.Items.Clear();
                this.uComboEquipType.Text = "";


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 툴바 관련
        public void mfSearch()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtGroup = new DataTable();
                DataTable dtGroupD = new DataTable();
                DataTable dtSaveType = new DataTable();
                DataTable dtDelType = new DataTable();
                DataRow row;
                DialogResult DResult = new DialogResult();

                #region 헤더필수입력사항

                // 필수입력사항 확인
                if (this.uComboPlant.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "공장을 선택해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (this.uComboEquipType.Value.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "설비유형을 선택해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboPlant.DropDown();
                    return;
                }

                // 필수입력사항 확인
                if (m_strAdmitStatusCode == "AR")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "승인요청 상태이므로 수정 할 수 없습니다", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }


                if (this.uTextWriteID.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "생성자ID를 입력해주세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextWriteID.Focus();
                    return;

                }

                if (this.uTextWriteName.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , "확인창", "필수입력사항 확인", "생성자ID를 정확히 입력 후 엔터키를 누르세요", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uTextWriteID.Focus();
                    return;

                }

                #endregion

                if(this.uGridDeviceChgPMH.Rows.Count > 0)
                    this.uGridDeviceChgPMH.ActiveCell = this.uGridDeviceChgPMH.Rows[0].Cells[0];  //그리드의 셀을 첫번째줄 첫번째 셀로 이동시킨다.

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGridDeviceChgPMH.Rows.Count; i++)
                {
                    if (this.uGridDeviceChgPMH.Rows[i].Hidden == false)
                    {
                        string strRowNum = this.uGridDeviceChgPMH.Rows[i].RowSelectorNumber.ToString();

                        if (this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectGubun"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", strRowNum + "번째 열의 점검항목구분을 입력해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridDeviceChgPMH.ActiveCell = this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectGubun"];
                            this.uGridDeviceChgPMH.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectName"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항 확인", strRowNum + "번째 열의 점검항목을 입력해주세요", Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridDeviceChgPMH.ActiveCell = this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectName"];
                            this.uGridDeviceChgPMH.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        //}
                    }
                }

                DialogResult dirRevision = new DialogResult();
                DialogResult dir = new DialogResult();

                string strRevisionType = "";

                //// 승인완료 상태일 경우는 개정여부를 물어본다 ////////////////////////////////////////////////
                if (m_strAdmitStatusCode == "FN")
                {
                    dirRevision = msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "저장확인", "개정하시겠습니까? \n" 
                                                , Infragistics.Win.HAlign.Right);
                }

                if (dirRevision == DialogResult.No)
                {
                    return;
                }
                else if (dirRevision == DialogResult.Yes)
                {
                    strRevisionType = "Revision";
                }
                
                //----------------------------------------------------------------------------------------------------
                
                


                //dir = msg.mfSetMessageBox(MessageBoxType.YesNoCancel, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                //                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "저장하시겠습니까? \n Y:승인요청      N:중간저장      C:취소"
                //                            , Infragistics.Win.HAlign.Right);


                dir = msg.mfSetMessageBox(MessageBoxType.ReqAgreeSave, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "저장하시겠습니까?"
                                            , Infragistics.Win.HAlign.Right);
               
                
                if  (dir != DialogResult.Cancel)
                {
                    if (dir == DialogResult.Yes) 
                    {
                        if (this.uTextAdmitID.Text == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "승인자를 입력해주세요", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uTextAdmitID.Focus();
                            return;

                        }

                        if (this.uTextAdmitName.Text == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "확인창", "필수입력사항 확인", "승인자ID를 정확히 입력 후 엔터키를 누르세요", Infragistics.Win.HAlign.Center);

                            // Focus
                            this.uTextAdmitID.Focus();
                            return;

                        }
                    }
                    // BL 호출(헤더)
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMH), "DeviceChgPMH");
                    QRPMAS.BL.MASEQU.DeviceChgPMH iGroup = new QRPMAS.BL.MASEQU.DeviceChgPMH();
                    brwChannel.mfCredentials(iGroup);

                    dtGroup = iGroup.mfSetDatainfo();

                    row = dtGroup.NewRow();
                    row["StdNumber"] = this.uTextStdNumber.Text;

                    if (this.uTextVersionNum.Text == "")
                    {
                        row["VersionNum"] = 0;
                    }
                    else
                    {
                        row["VersionNum"] = this.uTextVersionNum.Text;
                    }


                    row["PlantCode"] = this.uComboPlant.Value.ToString();
                    
                    //row["EquipGroupCode"] = this.uComboEquipGroup.Value.ToString();
                    row["EquipTypeCode"] = this.uComboEquipType.Value.ToString();

                    row["WriteID"] = this.uTextWriteID.Text;
                    row["WriteDate"] = this.uDateWriteDate.Value.ToString();
                    row["AdmitID"] = this.uTextAdmitID.Text;
                    row["AdmitDate"] = DateTime.Now;
                    row["AdmitStatusCode"] = m_strAdmitStatusCode;


                    // 개정저장여부에서 Yes인경우는 RevisionType = 'Revision'으로 넘겨준다
                    if (strRevisionType == "Revision")
                    {
                        if (dir == DialogResult.Yes)
                        {
                            row["AdmitStatusCode"] = "AR";   // 승인요청 상태일경우는 "AR"    
                        }
                        else if (dir == DialogResult.No)
                        {
                            row["AdmitStatusCode"] = "WR";   // 중간저장 상태일경우는 "WR"    
                        }

                        row["RevisionType"] = "Revision";
                    }

                    // 일반저장(개정이 아닌경우) RevisionType = ''으로 넘겨준다
                    else
                    {
                        if (dir == DialogResult.Yes)
                        {
                            row["AdmitStatusCode"] = "AR";   // 승인요청 상태일경우는 "AR"    
                        }
                        else if (dir == DialogResult.No)
                        {
                            row["AdmitStatusCode"] = "WR";   // 중간저장 상태일경우는 "WR"    
                        }
                        row["RevisionType"] = "";
                    }                        

                    row["EtcDesc"] = this.uTextEtcDesc.Text;
                    row["RevisionReason"] = this.uTextRevisionReason.Text;
                    row["RejectReason"] = this.uTextRejectReason.Text;

                    dtGroup.Rows.Add(row);


                    // BL 호출(아이템)
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMD), "DeviceChgPMD");
                    QRPMAS.BL.MASEQU.DeviceChgPMD iGroupD = new QRPMAS.BL.MASEQU.DeviceChgPMD();
                    brwChannel.mfCredentials(iGroupD);

                    dtGroupD = iGroupD.mfSetDatainfo();
                    for (int i = 0; i < this.uGridDeviceChgPMH.Rows.Count; i++)
                    {
                        this.uGridDeviceChgPMH.ActiveCell = this.uGridDeviceChgPMH.Rows[0].Cells[0];

                        if (this.uGridDeviceChgPMH.Rows[i].Hidden == false)
                        {
                            row = dtGroupD.NewRow();
                            row["StdNumber"] = this.uTextStdNumber.Text;
                            row["VersionNum"] = 0;
                            row["Seq"] = i + 1;
                            row["PMInspectGubun"] = this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectGubun"].Value.ToString();
                            row["PMInspectName"] = this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectName"].Value.ToString();
                            row["PMInspectCriteria"] = this.uGridDeviceChgPMH.Rows[i].Cells["PMInspectCriteria"].Value.ToString();
                            row["LevelCode"] = this.uGridDeviceChgPMH.Rows[i].Cells["LevelCode"].Value.ToString();
                            dtGroupD.Rows.Add(row);
                        }
                    }

                    
                    
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 처리 로직 //
                    // 저장함수 호출
                    string rtMSG = iGroup.mfSaveDeviceChgPMH(dtGroup, dtGroupD, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                    // 처리로직 끝//

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 처리결과에 따른 메세지 박스
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                            Infragistics.Win.HAlign.Right);

                        mfCreate();
                        ////SaveInspectType();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                            Infragistics.Win.HAlign.Right);
                    }
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }

        public void mfCreate()
        {
            try
            {
                InitGrid();
                InitComboBox();
                InitText();
                m_strAdmitStatusCode = "";
                m_strSaveCheck = "";
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridDeviceChgPMH.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "확인창", "출력정보 확인", "엑셀출력정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridDeviceChgPMH);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        #endregion

        #region Event

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            // 행삭제 이벤트
            try
            {
                for (int i = 0; i < this.uGridDeviceChgPMH.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDeviceChgPMH.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridDeviceChgPMH.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Combo

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //InitText();
                //InitGrid();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipType), "EquipType");
                QRPMAS.BL.MASEQU.EquipType clsEquipType = new QRPMAS.BL.MASEQU.EquipType();
                brwChannel.mfCredentials(clsEquipType);

                // Call Method
                DataTable dtEquipType = clsEquipType.mfReadEquipType(this.uComboPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));


                this.uComboEquipType.Items.Clear();
                wCombo.mfSetComboEditor(this.uComboEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "", "EquipTypeCode", "EquipTypeName", dtEquipType);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        private void uComboPlant_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                InitText();
                InitGrid();

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                //////QRPBrowser brwChannel = new QRPBrowser();
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                //////QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                //////brwChannel.mfCredentials(clsEquipGroup);

                //////// Call Method
                //////DataTable dtEquipGroup = clsEquipGroup.mfReadEquipGroup(this.uComboPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipType), "EquipType");
                QRPMAS.BL.MASEQU.EquipType clsEquipType = new QRPMAS.BL.MASEQU.EquipType();
                brwChannel.mfCredentials(clsEquipType);

                // Call Method
                DataTable dtEquipType = clsEquipType.mfReadEquipType(this.uComboPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));


                this.uComboEquipType.Items.Clear();
                wCombo.mfSetComboEditor(this.uComboEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, "", "", "", "EquipTypeCode", "EquipTypeName", dtEquipType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboEquipType_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult dir = new DialogResult();

                

                InitText();
                //this.uGridDeviceChgPMH.Enabled = false;

                

                string strPlantCode = uComboPlant.Value.ToString();
                string strEquipTypeCode = uComboEquipType.Value.ToString();

                string strStdNumber = "";
                string strVersionNum = "";

                m_strAdmitStatusCode = "";

                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();

                //-----------품종교체점검정보에 등록이 되어있는지 확인 --------//
                //BL호출
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMH), "DeviceChgPMH");
                QRPMAS.BL.MASEQU.DeviceChgPMH clsDeviceChgPMH = new QRPMAS.BL.MASEQU.DeviceChgPMH();
                brwChnnel.mfCredentials(clsDeviceChgPMH);

                DataTable dtDeviceChgPMH = clsDeviceChgPMH.mfReadDeviceChgPMHCheck(strPlantCode, strEquipTypeCode, m_resSys.GetString("SYS_LANG"));

                for (int i = 0; i < dtDeviceChgPMH.Rows.Count; i++)
                {
                    this.uTextStdNumber.Text = dtDeviceChgPMH.Rows[i]["StdNumber"].ToString();
                    this.uTextVersionNum.Text = dtDeviceChgPMH.Rows[i]["VersionNum"].ToString();
                    this.uTextWriteID.Text = dtDeviceChgPMH.Rows[i]["WriteID"].ToString();
                    this.uTextWriteName.Text = dtDeviceChgPMH.Rows[i]["WriteName"].ToString();
                    this.uDateWriteDate.Value = dtDeviceChgPMH.Rows[i]["WriteDate"].ToString();
                    this.uTextAdmitID.Text = dtDeviceChgPMH.Rows[i]["AdmitID"].ToString();
                    this.uTextAdmitName.Text = dtDeviceChgPMH.Rows[i]["AdmitName"].ToString();
                    
                    this.uTextAdmitStatus.Text = dtDeviceChgPMH.Rows[i]["AdmitStatusName"].ToString();
                    this.uTextEtcDesc.Text = dtDeviceChgPMH.Rows[i]["EtcDesc"].ToString();
                    this.uTextRejectReason.Text = dtDeviceChgPMH.Rows[i]["RejectReason"].ToString();
                    this.uTextRevisionReason.Text = dtDeviceChgPMH.Rows[i]["RevisionReason"].ToString();

                    m_strAdmitStatusCode = dtDeviceChgPMH.Rows[i]["AdmitStatusCode"].ToString();                   
                }

                if (m_strAdmitStatusCode == "AR")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "확인창", "필수입력사항 확인", "승인요청 상태이므로 수정 할 수 없습니다", Infragistics.Win.HAlign.Center);
                }

                else if (m_strAdmitStatusCode == "FN")
                {
                    this.uTextRevisionReason.Enabled = true;
                    this.uTextRevisionReason.ReadOnly = false;
                }


                strStdNumber = this.uTextStdNumber.Text;

                if (this.uTextVersionNum.Text == "")
                {
                    strVersionNum = "0";
                }
                else
                {
                    strVersionNum = this.uTextVersionNum.Text;
                }

                //-----------점검항목 상세 그리드 바인딩--------------//
                //BL호출

                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.DeviceChgPMD), "DeviceChgPMD");
                QRPMAS.BL.MASEQU.DeviceChgPMD clsDeviceChgPMD = new QRPMAS.BL.MASEQU.DeviceChgPMD();
                brwChnnel.mfCredentials(clsDeviceChgPMD);

                //매서드호출
                DataTable dtDeviceChgPMD = clsDeviceChgPMD.mfReadDeviceChgPMD(strStdNumber, strVersionNum, m_resSys.GetString("SYS_LANG"));


                //테이터바인드
                uGridDeviceChgPMH.DataSource = dtDeviceChgPMD;
                uGridDeviceChgPMH.DataBind();

                if (dtDeviceChgPMD.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDeviceChgPMH, 0);
                }


                //-----------점검그룹 설비 리스트 그리드 바인딩--------------//
                //BL호출                
                ////////brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                ////////QRPMAS.BL.MASEQU.EquipGroup EquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                ////////brwChnnel.mfCredentials(EquipGroup);

                //////////매서드호출
                ////////DataTable dtEquipList = EquipGroup.mfReadEquipGroupList(strPlantCode, strEquipTypeCode, m_resSys.GetString("SYS_LANG"));


                //////////테이터바인드
                ////////uGrid2.DataSource = dtEquipList;
                ////////uGrid2.DataBind();



                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uComboEquipGroup_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //try
            //{

            //    // SystemInfo ResourceSet
            //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //    WinComboEditor wCombo = new WinComboEditor();
            //    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
            //    DialogResult dir = new DialogResult();


            //    if (m_strSaveCheck == "SAVE")
            //    {
            //        dir = msg.mfSetMessageBox(MessageBoxType.YesNoCancel, m_resSys.GetString("SYS_FONTNAME"), 500, 500
            //                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", " 저장확인", "입력중이던 정보가 사라집니다. 저장하시겠습니까?"
            //                                , Infragistics.Win.HAlign.Right);

            //        if (dir == DialogResult.Yes)
            //        {
            //            mfSave();
            //        }
            //        else
            //            this.uComboEquipGroup.CloseUp();
            //    }
            //}
            //catch
            //{ }
            //finally
            //{ }
        }

        #endregion

        #region Grid

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDeviceChgPMH, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }        

        private void uGridDeviceChgPMH_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            
            try
            {
                if (Convert.ToInt32(e.Cell.Row.Cells["Seq"].Value) == 0)
                {
                    e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
                }

               

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Text
        private void uTextWriteID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //m_strSaveCheck = "SAVE";

                if (e.KeyCode == Keys.Enter)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUserID = this.uTextWriteID.Text;
                    WinMessageBox msg = new WinMessageBox();

                    // 공장콤보박스 미선택시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "공장을 선택해주세요.",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            this.uTextWriteName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "사번을 찾을수 없습니다",
                                            Infragistics.Win.HAlign.Right);

                            this.uTextWriteName.Text = "";
                            this.uTextWriteID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextAdmitID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    // SystemInfo ResourceSet
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    String strPlantCode = this.uComboPlant.Value.ToString();
                    String strUserID = this.uTextAdmitID.Text;
                    WinMessageBox msg = new WinMessageBox();

                    // 공장콤보박스 미선택시 종료
                    if (strPlantCode == "" && strUserID != "")
                    {

                        DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "공장을 선택해주세요.",
                                            Infragistics.Win.HAlign.Right);

                        this.uComboPlant.DropDown();
                    }
                    else if (strPlantCode != "" && strUserID != "")
                    {
                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                        QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                        brwChannel.mfCredentials(clsUser);

                        DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));
                        if (dtUser.Rows.Count > 0)
                        {
                            this.uTextAdmitName.Text = dtUser.Rows[0]["UserName"].ToString();
                        }
                        else
                        {
                            DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "확인창", "입력확인", "사번을 찾을수 없습니다",
                                            Infragistics.Win.HAlign.Right);

                            this.uTextAdmitName.Text = "";
                            this.uTextAdmitID.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextWriteID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.ShowDialog();

                this.uTextWriteID.Text = frmPOP.UserID;
                this.uTextWriteName.Text = frmPOP.UserName;
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uTextAdmitID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                frmPOP0011 frmPOP = new frmPOP0011();
                frmPOP.ShowDialog();

                this.uTextAdmitID.Text = frmPOP.UserID;
                this.uTextAdmitName.Text = frmPOP.UserName;

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        private void frmMASZ0007_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBox1.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion
    }
}
