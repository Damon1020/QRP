﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0042.cs                                         */
/* 프로그램명   : 금형치공구정보                                        */
/* 작성자       : 이용현                                                */
/* 작성일자     : 2011-09-05                                            */
/* 수정이력     : 2011-09-26 : Header 그리드 구성 수정/Detail(Lot) 추가 */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0042 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();
        //BL호출을위한 전역변수
        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

        public frmMAS0042()
        {
            InitializeComponent();
        }

        private void frmMAS0042_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0042_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("금형치공구정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();
            InitButton();
            InitGroupBox();
            ButtonAuth();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        #region 초기화 함수

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchMold, "금형치공구유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDurableMatName, "금형치공구명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchPackage, "Package", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabel, "MDM재전송여부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelPlantName, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDurableMatCode, "금형치공구코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelLotNoInfo, "금형치공구 LOT", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelDurableInventory, "금형치공구창고", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCoustomer, "고객사", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                WinButton btn = new WinButton();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                btn.mfSetButton(this.uButtonDeleteLot, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteLotInfo, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonRowDel, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);

               // btn.mfSetButton(this.uButtonModelOK, "▶", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                //btn.mfSetButton(this.uButtonPackageOK, "->", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonSearchM, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButtonSearchP, "검색", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
                btn.mfSetButton(this.uButton, "Lot입력 의뢰", m_resSys.GetString("SYS_FONTNAME"), null);
                this.uButton.Appearance.ForeColor = Color.Red;
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);


                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtCom = clsCommonCode.mfReadCommonCode("C0043",m_resSys.GetString("SYS_LANG"));

                if (dtCom.Select("ComCode ='M12' OR ComCode ='M13' OR ComCode ='M14'OR ComCode ='M15'").Count() != 0)
                {
                    DataRow[] drRow = dtCom.Select("ComCode ='M12' OR ComCode ='M13' OR ComCode ='M14' OR ComCode ='M15'");
                    dtCom = drRow.CopyToDataTable();
                }

                wCombo.mfSetComboEditor(this.uComboDeleteType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                   , "", "", "선택", "ComCode", "ComCodeName", dtCom);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grBox = new WinGroupBox();

                grBox.mfSetGroupBox(this.uGroupDurable, GroupBoxType.LIST, "금형치공구리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grBox.mfSetGroupBox(this.uGroupDetail, GroupBoxType.DETAIL, "금형치공구상세정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grBox.mfSetGroupBox(this.uGroupBoxModel, GroupBoxType.LIST, "Model정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grBox.mfSetGroupBox(this.uGroupBoxPackage, GroupBoxType.LIST, "Package정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                //폰트설정
                this.uGroupDurable.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupDurable.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //폰트설정
                this.uGroupDetail.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupDetail.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //폰트설정
                this.uGroupBoxModel.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxModel.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //폰트설정
                this.uGroupBoxPackage.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupBoxPackage.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region DurableMat
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridDurableMat, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Always
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatName", "금형치공구명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatNameCh", "금형치공구명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "DurableMatNameEn", "금형치공구명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //wGrid.mfSetGridColumn(this.uGrid1, 0, "DurableMatTypeCode", "금형치공구유형", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "Spec", "규격", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "UsageLimitLower", "Limit - 1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "UsageLimit", "Limit", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "UsageLimitUpper", "Limit + 1", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn.nnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "SerialFlag", "Serial", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                wGrid.mfSetGridColumn(this.uGridDurableMat, 0, "MDMIFFlag", "MDM전송여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #region DropDown Binding

                
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode"); // 2
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode(); // 3
                brwChannel.mfCredentials(clsComCode); // 4
                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG")); // 5
                //DataTable dtDiscardFlag = clsComCode.mfReadCommonCode("C0004", m_resSys.GetString("SYS_LANG")); // 5

                wGrid.mfSetGridColumnValueList(this.uGridDurableMat, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);
                wGrid.mfSetGridColumnValueList(this.uGridDurableMat, 0, "SerialFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);
                

                #endregion

                //헤더 체크박스 표시 비활성화
                this.uGridDurableMat.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                #endregion

                #region DurableLot
                wGrid.mfInitGeneralGrid(this.uGridDurableLot, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "Check", "", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");
                
                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "DurableMatCode", "금형치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "GRDate", "입고일", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Date, "", "yyyy-mm-dd", DateTime.Now.Date.ToString());
                
                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "LotNo", "LotNo", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
               
                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "CurUsage", "현재Usage", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn.nnnnn", "0");
                
                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "CumUsage", "누적Usage", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Double, "", "nnnnnnnnnn.nnnnn", "0");
                
                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "LastInspectDate", "최종점검일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
               
                ////wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "DiscardName", "폐기여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                ////wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "DiscardDate", "폐기일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                ////wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "DiscardChargeID", "폐기담당자ID", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                ////wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "DiscardChargeName", "폐기담당자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                ////wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "DiscardReason", "폐기사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 10
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "ModelName", "ModelName", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "RepairReqCode", "수리요청코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridDurableLot, 0, "WriteID", "수리출고요청자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, true, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                this.uGridDurableLot.DisplayLayout.Bands[0].Columns["Check"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                #endregion

                #region Model정보
                
                //기본설정
                wGrid.mfInitGeneralGrid(this.uGridModel, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                wGrid.mfSetGridColumn(this.uGridModel, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridModel, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridModel, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridModel, 0, "ModelName", "설비모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                
                #endregion

                #region ModelList

                //기본설정
                wGrid.mfInitGeneralGrid(this.uGridModelList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                wGrid.mfSetGridColumn(this.uGridModelList, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridModelList, 0, "ModelName", "설비모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");


                #endregion

                #region Package정보

                //기본설정
                wGrid.mfInitGeneralGrid(this.uGridPackage, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                wGrid.mfSetGridColumn(this.uGridPackage, 0, "DurableMatCode", "치공구코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackage, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridPackage, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPackage, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, true, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                #endregion

                #region PackageList

                //기본설정
                wGrid.mfInitGeneralGrid(this.uGridPackageList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                wGrid.mfSetGridColumn(this.uGridPackageList, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridPackageList, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 40
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                #endregion

                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGridDurableMat, 0);
                wGrid.mfAddRowGrid(this.uGridDurableLot, 0);

                #region FontSize

                // FontSize
                this.uGridDurableMat.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableMat.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridDurableLot.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridDurableLot.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridModel.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridModel.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridModelList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridModelList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridPackage.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPackage.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridPackageList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPackageList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region Grid RowSelector = False

                this.uGridModel.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                this.uGridModelList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                this.uGridPackage.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                this.uGridPackageList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                #endregion

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region ToolBar Method
        
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();

                string strPlantCode = uComboSearchPlant.Value.ToString();

                if (strPlantCode.Equals(string.Empty))
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                string strDurableMatTypeCode = uComboMold.Value.ToString();
                string strPackage = this.uComboSearchPackage.Value.ToString();
                string strDurableMatName = this.uComboDurableMatName.Value.ToString();
                
                //ProgressBar보이기
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서Change
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //--------------------------------------------------------------------처리로직----------------------------------------------------------//
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);


                DataTable dtDurableMat = new DataTable();
                //조회함수호출
                dtDurableMat = clsDurableMat.mfReadDurableMat_Package(strPlantCode, strDurableMatTypeCode, strPackage, strDurableMatName,m_resSys.GetString("SYS_LANG"));

                //그리드에 바인딩
                this.uGridDurableMat.DataSource = dtDurableMat;
                this.uGridDurableMat.DataBind();

                //--------------------------------------------------------------------------------------------------------------------------------------//
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                
                // 조회 결과가 없을시 메세지창 띄움
                if (dtDurableMat.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridDurableMat, 0);

                    for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                    {
                        this.uGridDurableMat.Rows[i].Cells["DurableMatCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurableMat.Rows[i].Cells["SerialFlag"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        
                        //MDM전송이 실패한 정보인경우 줄색을 변경하여 나타낸다.
                        if (this.uGridDurableMat.Rows[i].Cells["MDMIFFlag"].Value.ToString().Equals("F"))
                            this.uGridDurableMat.Rows[i].Appearance.BackColor = Color.Salmon;
                        else
                            this.uGridDurableMat.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit; //만약 삭제도 가능하다면 주석처리 
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }   

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //줄이 한줄일 경우
                if (this.uGridDurableMat.Rows.Count == 0 && this.uGridModel.Rows.Count == 0 && this.uGridPackage.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001032", "M001025", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region BL호출 및 컬럼정보 저장

                //금형치공구 정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);

                //금형치공구정보 컬럼정보매서드 호출
                DataTable dtDurableMat = clsDurableMat.mfSetDataInfoH();
                

                //금형치공구BOM_Model정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMatBOM_Model), "DurableMatBOM_Model");
                QRPMAS.BL.MASDMM.DurableMatBOM_Model clsDurableMatBOM_Model = new QRPMAS.BL.MASDMM.DurableMatBOM_Model();
                brwChannel.mfCredentials(clsDurableMatBOM_Model);

                //금형치공구BOM_Package 정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMatBOM_Package), "DurableMatBOM_Package");
                QRPMAS.BL.MASDMM.DurableMatBOM_Package clsDurableMatBOM_Package = new QRPMAS.BL.MASDMM.DurableMatBOM_Package();
                brwChannel.mfCredentials(clsDurableMatBOM_Package);

                //금형치공구BOM_Model 컬럼정보매서드 호출
                DataTable dtDurableMatBOM_Model = clsDurableMatBOM_Model.mfSetDataInfo();

                //금형치공구BOM_Package 컬럼정보매서드호출
                DataTable dtDurableMatBOM_Package = clsDurableMatBOM_Package.mfDataSet();

                #endregion

                #region 헤더 그리드

                string strPlantCode = "";

                if (this.uTextPlantName.Tag != null && this.uTextPlantName.Tag.ToString().Equals(string.Empty))
                    strPlantCode = this.uTextPlantName.Tag.ToString();
                else
                    strPlantCode = this.uComboSearchPlant.Value.ToString();

                string strDurableMatCode = this.uTextDurableMatCode.Text;

                if (this.uGridDurableMat.Rows.Count > 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                    {
                        if (this.uGridDurableMat.Rows[i].RowSelectorAppearance.Image != null)
                            //|| this.uGridDurableMat.Rows[i].Cells["MDMIFFlag"].Value.ToString().Equals("F"))
                        {

                            #region 필수입력사항

                            //if (this.uGridDurableMat.Rows[i].GetCellValue("PlantCode").ToString().Equals(string.Empty))
                            //{
                            //    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            //           , "필수입력확인창", "필수입력확인", "공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                            //    this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["PlantCode"];
                            //    this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            //    return;
                            //}

                            if (this.uGridDurableMat.Rows[i].GetCellValue("DurableMatCode").ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M001234", "M000341", Infragistics.Win.HAlign.Right);

                                this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["DurableMatCode"];
                                this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            if (this.uGridDurableMat.Rows[i].GetCellValue("DurableMatName").ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M001234", "M000340", Infragistics.Win.HAlign.Right);

                                this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[i].Cells["DurableMatName"];
                                this.uGridDurableMat.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }

                            #endregion

                            #region 정보저장
                            DataRow drDurable;
                            drDurable = dtDurableMat.NewRow();

                            if (!this.uGridDurableMat.Rows[i].GetCellValue("PlantCode").ToString().Equals(string.Empty))
                                drDurable["PlantCode"] = this.uGridDurableMat.Rows[i].GetCellValue("PlantCode");
                            else
                                drDurable["PlantCode"] = this.uComboSearchPlant.Value.ToString();

                            drDurable["DurableMatCode"] = this.uGridDurableMat.Rows[i].GetCellValue("DurableMatCode");
                            drDurable["DurableMatName"] = this.uGridDurableMat.Rows[i].GetCellValue("DurableMatName");
                            drDurable["DurableMatNameCh"] = this.uGridDurableMat.Rows[i].GetCellValue("DurableMatNameCh");
                            drDurable["DurableMatNameEn"] = this.uGridDurableMat.Rows[i].GetCellValue("DurableMatNameEn");
                            drDurable["Spec"] = this.uGridDurableMat.Rows[i].GetCellValue("Spec");
                            drDurable["SerialFlag"] = this.uGridDurableMat.Rows[i].GetCellValue("SerialFlag");
                            drDurable["UseFlag"] = this.uGridDurableMat.Rows[i].GetCellValue("UseFlag");

                            //각 사용한계치들 중에 공백이 있으면 0으로 넘긴다.
                            if (this.uGridDurableMat.Rows[i].GetCellValue("UsageLimitLower").ToString().Equals(string.Empty))
                            {
                                drDurable["UsageLimitLower"] = 0;
                            }
                            else
                            {
                                drDurable["UsageLimitLower"] = this.uGridDurableMat.Rows[i].GetCellValue("UsageLimitLower");
                            }
                            if (this.uGridDurableMat.Rows[i].GetCellValue("UsageLimit").ToString().Equals(string.Empty))
                            {
                                drDurable["UsageLimit"] = 0;
                            }
                            else
                            {
                                drDurable["UsageLimit"] = this.uGridDurableMat.Rows[i].GetCellValue("UsageLimit");
                            }
                            if (this.uGridDurableMat.Rows[i].GetCellValue("UsageLimitUpper").ToString().Equals(string.Empty))
                            {
                                drDurable["UsageLimitUpper"] = 0;
                            }
                            else
                            {
                                drDurable["UsageLimitUpper"] = this.uGridDurableMat.Rows[i].GetCellValue("UsageLimitUpper");
                            }
                            dtDurableMat.Rows.Add(drDurable);

                            #endregion
                        }
                    }

                }


                #endregion

                #region 금형치공구 Lot 그리드

                DataTable dtDurableLot = new DataTable();
                DataTable dtDurableStockMoveHist = new DataTable();
                DataTable dtDurableStock = new DataTable();
                DataTable dtDurableRepairH = new DataTable();
                DataTable dtDurableRepairD = new DataTable();
                DataTable dtRepairLotHistory = new DataTable();

                 #region Columns

                    //금형치공구Lot정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                    QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                    brwChannel.mfCredentials(clsDurableLot);

                    //금형치공구Lot컬럼정보 저장
                    dtDurableLot = clsDurableLot.mfSetDatainfo();

                    //금형치공구이력정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                    QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                    brwChannel.mfCredentials(clsDurableStockMoveHist);

                    //금형치공구이력 컬럼 정보 저장
                    dtDurableStockMoveHist = clsDurableStockMoveHist.mfSetDatainfo();

                    //금형치공구 재고정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    //금형치공구 재고 컬럼정보 저장
                    dtDurableStock = clsDurableStock.mfSetDatainfo();

                    //치공구교체정보BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairH), "DurableMatRepairH");
                    QRPEQU.BL.EQUMGM.DurableMatRepairH clsDurableMatRepairH = new QRPEQU.BL.EQUMGM.DurableMatRepairH();
                    brwChannel.mfCredentials(clsDurableMatRepairH);
                    //---헤더값을 넣을 데이터 테이블---//
                    dtDurableRepairH = clsDurableMatRepairH.mfDataSetInfo();

                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUMGM.DurableMatRepairD), "DurableMatRepairD");
                    QRPEQU.BL.EQUMGM.DurableMatRepairD clsDurableMatRepairD = new QRPEQU.BL.EQUMGM.DurableMatRepairD();
                    brwChannel.mfCredentials(clsDurableMatRepairD);

                    //--상세정보를 넣을 데이터 테이블--//
                    dtDurableRepairD = clsDurableMatRepairD.mfsetDataInfo();

                    dtDurableRepairD.Columns.Add("UnitName", typeof(string));
                    dtDurableRepairD.Columns.Add("Qty", typeof(string));
                    dtDurableRepairD.Columns.Add("ChgDurableMatName", typeof(string));
                    dtDurableRepairD.Columns.Add("ItemGubunCode", typeof(string));
                    dtDurableRepairD.Columns.Add("RepairReqCode", typeof(string));

                    //신규Lot정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairLotHistory), "RepairLotHistory");
                    QRPEQU.BL.EQUREP.RepairLotHistory clsRepairLotHistory = new QRPEQU.BL.EQUREP.RepairLotHistory();
                    brwChannel.mfCredentials(clsRepairLotHistory);

                    dtRepairLotHistory = new DataTable();
                    dtRepairLotHistory.Columns.Add("PlantCode", typeof(string));
                    dtRepairLotHistory.Columns.Add("RepairReqCode", typeof(string));
                #endregion

                if (this.uCheckSerial.Checked)
                {

                    if (this.uGridDurableLot.Rows.Count > 0)
                    {
                        this.uGridDurableLot.ActiveCell = this.uGridDurableLot.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                        {
                            string strRowNum = this.uGridDurableLot.Rows[i].RowSelectorNumber.ToString();
                            if (this.uGridDurableLot.Rows[i].Hidden.Equals(false) &&
                                this.uGridDurableLot.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                #region 필수입력사항
                                if (this.uComboDurableInven.Value.ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001235", "M001134", "M000907", Infragistics.Win.HAlign.Right);

                                    this.uComboDurableInven.DropDown();
                                    return;
                                }

                                if (this.uGridDurableLot.Rows[i].GetCellValue("GRDate").ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001235", "M000869", "M000867", Infragistics.Win.HAlign.Right);

                                    this.uGridDurableLot.ActiveCell = this.uGridDurableLot.Rows[i].Cells["GRDate"];
                                    this.uGridDurableLot.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;
                                }

                                if (this.uGridDurableLot.Rows[i].GetCellValue("LotNo").ToString().Equals(string.Empty))
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001235", "M000079", "M000078", Infragistics.Win.HAlign.Right);
                                    this.uGridDurableLot.ActiveCell = this.uGridDurableLot.Rows[i].Cells["GRDate"];
                                    this.uGridDurableLot.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                    return;

                                }



                                #endregion

                                #region 투입 여부확인


                                //BOM정보에서 팝업창에서 불러온 LotNo의 투입 여부를 판별한다.
                                if (!this.uGridDurableLot.Rows[i].Cells["RepairReqCode"].Value.ToString().Equals(string.Empty))
                                {
                                    DataTable dtFlag = clsRepairLotHistory.mfReadRepairLotHistory_WriteFlag(strPlantCode, this.uGridDurableLot.Rows[i].Cells["RepairReqCode"].Value.ToString(), m_resSys.GetString("SYS_LANG"));

                                    //신규Lot정보에서 WriteFlag 확인하여 투입되어있는 경우 메세지를 뿌린다.
                                    if (dtFlag.Rows.Count == 0 || dtFlag.Rows[0]["WriteFlag"].ToString().Equals("T"))
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "M001264", "M000310", strRowNum + "M000512", Infragistics.Win.HAlign.Right);

                                        this.uGridDurableLot.ActiveCell = this.uGridDurableLot.Rows[i].Cells["LotNo"];
                                        return;
                                    }
                                }

                                #endregion

                                #region Lot
                                DataRow drRow = dtDurableLot.NewRow();

                                drRow["PlantCode"] = strPlantCode;
                                drRow["DurableMatCode"] = strDurableMatCode;
                                drRow["LotNo"] = this.uGridDurableLot.Rows[i].GetCellValue("LotNo").ToString();
                                drRow["GRDate"] = this.uGridDurableLot.Rows[i].GetCellValue("GRDate").ToString();
                                drRow["DiscardFlag"] = "F";
                                drRow["DiscardDate"] = "";
                                drRow["DiscardChargeID"] = "";
                                drRow["DiscardReason"] = "";
                                drRow["ChangeUsage"] = "0";
                                drRow["CurUsage"] = "0";

                                drRow["CumUsage"] = "0";
                                drRow["LastInspectDate"] = "";
                                drRow["LimitArrivalDate"] = "";

                                dtDurableLot.Rows.Add(drRow);

                                #endregion

                                #region Stock
                                drRow = dtDurableStock.NewRow();

                                drRow["PlantCode"] = strPlantCode;
                                drRow["DurableInventoryCode"] = this.uComboDurableInven.Value.ToString();
                                drRow["DurableMatCode"] = strDurableMatCode;
                                drRow["LotNo"] = this.uGridDurableLot.Rows[i].GetCellValue("LotNo").ToString();
                                drRow["Qty"] = "1";
                                drRow["UnitCode"] = "EA";

                                dtDurableStock.Rows.Add(drRow);

                                #endregion

                                #region Hist

                                drRow = dtDurableStockMoveHist.NewRow();

                                drRow["MoveGubunCode"] = "M01";       //기초재고일 경우는 "M01"
                                drRow["DocCode"] = "";
                                drRow["MoveDate"] = this.uGridDurableLot.Rows[i].GetCellValue("GRDate").ToString();
                                drRow["MoveChargeID"] = m_resSys.GetString("SYS_USERID");
                                drRow["PlantCode"] = strPlantCode;
                                drRow["DurableInventoryCode"] = this.uComboDurableInven.Value.ToString();
                                drRow["EquipCode"] = "";
                                drRow["LotNo"] = this.uGridDurableLot.Rows[i].GetCellValue("LotNo").ToString();
                                drRow["DurableMatCode"] = strDurableMatCode;
                                drRow["MoveQty"] = "1";
                                drRow["UnitCode"] = "EA";

                                dtDurableStockMoveHist.Rows.Add(drRow);

                                #endregion

                                #region 신규LotNo
                                //팝업LotNo정보인 경우 수리출고구분정보에 담는다.
                                if (!this.uGridDurableLot.Rows[i].Cells["RepairReqCode"].Value.ToString().Equals(string.Empty))
                                {
                                    #region 상세저장
                                    DataRow drRepairD;
                                    drRepairD = dtDurableRepairD.NewRow();
                                    drRepairD["PlantCode"] = strPlantCode;          //공장코드
                                    drRepairD["Seq"] = 0;                           //순번
                                    drRepairD["ItemGubunCode"] = "TL";
                                    drRepairD["RepairGIGubunCode"] = "INN";//수리출고구분
                                    drRepairD["CurDurableMatCode"] = ""; //구성품
                                    drRepairD["CurQty"] = "0";                  //기존수량
                                    drRepairD["CurLotNo"] = "";                //기존 LotNo
                                    drRepairD["ChgDurableInventoryCode"] = this.uComboDurableInven.Value.ToString();//교체창고
                                    drRepairD["ChgDurableMatCode"] = this.uTextDurableMatCode.Text;//교체구성품
                                    drRepairD["ChgDurableMatName"] = this.uTextDurableMatName.Text;
                                    drRepairD["ChgQty"] = "1";                //교체수량
                                    drRepairD["Qty"] = "1";                      //가용수량
                                    drRepairD["ChgLotNo"] = this.uGridDurableLot.Rows[i].Cells["LotNo"].Value.ToString();            //변경된LotNo
                                    drRepairD["UnitCode"] = "EA";            //단위
                                    drRepairD["EtcDesc"] = "";              //특이사항
                                    drRepairD["CancelFlag"] = "F";
                                    drRepairD["RepairReqCode"] = this.uGridDurableLot.Rows[i].Cells["RepairReqCode"].Value.ToString();

                                    dtDurableRepairD.Rows.Add(drRepairD);

                                    #endregion

                                    //똑같은 수리요청번호가 있는 경우 헤더저장을 패스 한다.
                                    string strChk = "";
                                    if (dtDurableRepairH.Rows.Count > 0)
                                    {

                                        for (int j = 0; j < dtDurableRepairH.Rows.Count; j++)
                                        {
                                            if (dtDurableRepairH.Rows[j]["RepairReqCode"].ToString().Equals(this.uGridDurableLot.Rows[i].Cells["RepairReqCode"].Value.ToString()))
                                            {

                                                strChk = "O";
                                                break;
                                            }
                                        }
                                    }

                                    if (strChk.Equals(string.Empty) && dtDurableRepairD.Rows.Count > 0)
                                    {

                                        #region 치공구교체헤더저장

                                        DataRow drRepairH;

                                        drRepairH = dtDurableRepairH.NewRow();
                                        drRepairH["PlantCode"] = strPlantCode;
                                        drRepairH["EquipCode"] = this.uGridDurableLot.Rows[i].Cells["EquipCode"].Value.ToString();


                                        drRepairH["RepairReqCode"] = this.uGridDurableLot.Rows[i].Cells["RepairReqCode"].Value.ToString();
                                        drRepairH["GITypeCode"] = "RE";

                                        drRepairH["RepairGIReqDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                                        drRepairH["RepairGIReqID"] = m_resSys.GetString("SYS_USERID");
                                        drRepairH["EtcDesc"] = "";


                                        dtDurableRepairH.Rows.Add(drRepairH);

                                        #endregion

                                        //신규 LotNo정보 저장
                                        drRepairH = dtRepairLotHistory.NewRow();

                                        drRepairH["PlantCode"] = strPlantCode;
                                        drRepairH["RepairReqCode"] = this.uGridDurableLot.Rows[i].Cells["RepairReqCode"].Value.ToString();

                                        dtRepairLotHistory.Rows.Add(drRepairH);

                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }

                #endregion

                #region Model 그리드 정보저장
                //정보 저장(편집이미지가 있고 숨김상태가아닌것은 저장 숨김상태인것은 행삭제 저장
                if (this.uGridModel.Rows.Count > 0 && !this.uTextDurableMatCode.Text.Equals(string.Empty))
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridModel.ActiveCell = this.uGridModel.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridModel.Rows.Count; i++)
                    {

                        if (this.uGridModel.Rows[i].Hidden.Equals(false))
                        {
                            #region 필수입력사항

                            if (this.uGridModel.Rows[i].GetCellValue("ModelName").ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001235", "M001234", "M000701", Infragistics.Win.HAlign.Right);

                                this.uGridModelList.ActiveCell = this.uGridModelList.Rows[i].Cells["ModelName"];
                                this.uGridModelList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }

                            #endregion

                            #region 저장정보저장

                            DataRow drSave;
                            drSave = dtDurableMatBOM_Model.NewRow();

                            if (this.uGridModel.Rows[i].GetCellValue("PlantCode").ToString().Equals(string.Empty))
                            {
                                drSave["PlantCode"] = this.uTextPlantName.Tag.ToString();
                            }
                            else
                            {
                                drSave["PlantCode"] = this.uGridModel.Rows[i].GetCellValue("PlantCode").ToString();
                            }

                            if (this.uGridModel.Rows[i].GetCellValue("DurableMatCode").ToString().Equals(string.Empty))
                            {
                                drSave["DurableMatCode"] = this.uTextDurableMatCode.Text;
                            }
                            else
                            {
                                drSave["DurableMatCode"] = this.uGridModel.Rows[i].GetCellValue("DurableMatCode").ToString();
                            }
                            drSave["ModelName"] = this.uGridModel.Rows[i].GetCellValue("ModelName").ToString();


                            dtDurableMatBOM_Model.Rows.Add(drSave);

                            #endregion
                        }
                    }
                }

                #endregion

                #region Package 그리드 정보저장

                //치공구 코드텍스트 박스가 공백이아니고 Package그리드줄이 1줄이상인 경우
                if (this.uGridPackage.Rows.Count > 0 && !this.uTextDurableMatCode.Text.Equals(string.Empty))
                {
                    //그리드의 셀을 맨 처음으로 이동시킨다.
                    this.uGridPackage.ActiveCell = this.uGridPackage.Rows[0].Cells[0];

                    //for문을 돌며 줄이 숨김이아닌 줄을 저장한다.
                    for (int i = 0; i < this.uGridPackage.Rows.Count; i++)
                    {
                        if (this.uGridPackage.Rows[i].Hidden.Equals(false))
                        {
                            if (this.uGridPackage.Rows[i].GetCellValue("Package").ToString().Equals(string.Empty))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000099", "M000098", Infragistics.Win.HAlign.Right);
                                return;
                            }

                            #region Package정보저장

                            DataRow drSave = dtDurableMatBOM_Package.NewRow();

                            if (this.uGridPackage.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                                drSave["PlantCode"] = this.uTextPlantName.Tag;
                            else
                                drSave["PlantCode"] = this.uGridPackage.Rows[i].Cells["PlantCode"].Value.ToString();

                            if (this.uGridPackage.Rows[i].Cells["DurableMatCode"].Value.ToString().Equals(string.Empty))
                                drSave["DurableMatCode"] = uTextDurableMatCode.Text;
                            else
                                drSave["DurableMatCode"] = this.uGridPackage.Rows[i].Cells["DurableMatCode"].Value.ToString();

                            drSave["Package"] = this.uGridPackage.Rows[i].Cells["Package"].Value.ToString();

                            dtDurableMatBOM_Package.Rows.Add(drSave);

                            #endregion

                        }
                    }

                }

                #endregion

                //MDM 전송여부
                string strSendMDM = m_resSys.GetString("SYS_SERVERPATH") == null ? "" : m_resSys.GetString("SYS_SERVERPATH");

                if (strSendMDM.Contains("10.60.60.91") || strSendMDM.Contains("10.60.60.92"))
                    strSendMDM = "T";
                else
                    strSendMDM = "F";


                if (dtDurableMat.Rows.Count == 0 && dtDurableMatBOM_Model.Rows.Count == 0 && dtDurableMatBOM_Package.Rows.Count == 0 && dtDurableLot.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001032", "M001025", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    //금형치공구 BOM 저장매서드 호출
                    string strErrRtn = clsDurableMat.mfSaveDurableMat(dtDurableMat, dtDurableMatBOM_Model, dtDurableMatBOM_Package, dtDurableLot, dtDurableStock, dtDurableStockMoveHist,
                                                                        dtDurableRepairH,dtDurableRepairD,dtRepairLotHistory ,
                                                                        strPlantCode, strDurableMatCode, strSendMDM,m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMsg = "";
                        if(ErrRtn.ErrMessage.Equals(string.Empty))
                            strMsg = "M000953";
                        else
                            strMsg = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", strMsg,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();


                //if (this.uGroupBoxContentsArea.Expanded.Equals(false) || 
                //    this.uGridDurableMatBOM.Rows.Count == 0 
                //    || this.uGridDurableMatBOM.Rows[0].GetCellValue("PlantCode").ToString().Equals(string.Empty))
                //{
                //    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                //                        "확인창", "삭제정보확인", "삭제데이터가 없습니다.", Infragistics.Win.HAlign.Right);
                //    return;
                //}

                if (this.uGridDurableMat.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000636", "M000631", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 정보저장

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);

                DataTable dtDurableMatDel = clsDurableMat.mfSetDataInfoDel();

                if (this.uGridDurableMat.Rows.Count > 0)
                {
                    this.uGridDurableMat.ActiveCell = this.uGridDurableMat.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridDurableMat.Rows.Count; i++)
                    {
                        if (this.uGridDurableMat.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            if (Convert.ToBoolean(this.uGridDurableMat.Rows[i].Cells["Check"].Value)
                                && !this.uGridDurableMat.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                            {

                                DataRow drDurable;
                                drDurable = dtDurableMatDel.NewRow();

                                drDurable["PlantCode"] = this.uGridDurableMat.Rows[i].GetCellValue("PlantCode");
                                drDurable["DurableMatCode"] = this.uGridDurableMat.Rows[i].GetCellValue("DurableMatCode");

                                dtDurableMatDel.Rows.Add(drDurable);

                            }
                        }
                    }
                }

                if (dtDurableMatDel.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                          "M001264", "M000636", "M000631", Infragistics.Win.HAlign.Right);
                    return;

                }

                #endregion

                DialogResult drResult = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");
                //삭제여부 메세지를 띄운다
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000650",strLang), msg.GetMessge_Text("M000657",strLang) + " " + dtDurableMatDel.Rows.Count + msg.GetMessge_Text("M000160",strLang),
                                    Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    drResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000664",
                                    Infragistics.Win.HAlign.Right);
                }

                if (drResult == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    TransErrRtn ErrRtn = new TransErrRtn();

                    
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMatBOM), "DurableMatBOM");
                    //QRPMAS.BL.MASDMM.DurableMatBOM clsDurableMatBOM = new QRPMAS.BL.MASDMM.DurableMatBOM();
                    //brwChannel.mfCredentials(clsDurableMatBOM);

                    //금형치공구 삭제 매서드 호출
                    string strErrRtn = clsDurableMat.mfDeleteDurableMat(dtDurableMatDel,m_resSys.GetString("SYS_USERIP"),m_resSys.GetString("SYS_USERID"));

                    //Decoding
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);


                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    //처리결과에 따라서 메세지박스를 띄운다.
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        InitControl();
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);

                    }
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridDurableMat.Rows.Count == 0 && this.uGridDurableLot.Rows.Count == 0 && this.uGridModel.Rows.Count == 0 && this.uGridPackage.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000811", "M000805", Infragistics.Win.HAlign.Right);
                    return;
                }

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridDurableMat);

                if(this.uGridDurableLot.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridDurableLot);

                if (this.uGridModel.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridModel);

                if (this.uGridPackage.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridPackage);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }


        #endregion

        #region 이벤트

        //공장콤보의 정보가 바뀐시 금형치공구유형콤보의 정보가 해당 공장정보대로 바뀐다.
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //System ResourceInfo
               
               WinComboEditor wCombo = new WinComboEditor();
               string strPlantCode = uComboSearchPlant.Value.ToString();


                //--------------------------------------------------------------------처리로직----------------------------------------------------------//
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMatType), "DurableMatType");
                QRPMAS.BL.MASDMM.DurableMatType clsDurableMatType = new QRPMAS.BL.MASDMM.DurableMatType();
                brwChannel.mfCredentials(clsDurableMatType);


                //제품정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                //패키지콤보매서드 호출
                DataTable dtPackage = clsProduct.mfReadMASProduct_Package(strPlantCode, m_resSys.GetString("SYS_LANG"));

                DataTable dtDurableMat = new DataTable();
                //조회함수호출
                dtDurableMat = clsDurableMatType.mfReadDurableMatType_combo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //콤보아이템초기화
                if(this.uComboMold.Items.Count > 0)
                    this.uComboMold.Items.Clear();

                if(this.uComboSearchPackage.Items.Count > 0)
                    this.uComboSearchPackage.Items.Clear();

                wCombo.mfSetComboEditor(this.uComboMold, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "DurableMatTypeCode","DurableMatTypeName", dtDurableMat);

                wCombo.mfSetComboEditor(this.uComboSearchPackage, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "Package", "ComboName", dtPackage);

                //--------------------------------------------------------------------------------------------------------------------------------------//

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                brwChannel.mfCredentials(clsDurableMat);
                DataTable dtDurableMatName = clsDurableMat.mfReadDurableMatNameCombo(strPlantCode, "", m_resSys.GetString("SYS_LANG"));

                if(this.uComboDurableMatName.Items.Count > 0)
                    this.uComboDurableMatName.Items.Clear();

                wCombo.mfSetComboEditor(this.uComboDurableMatName, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                   , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                   , "", "", "전체", "DurableMat", "DurableMatName", dtDurableMatName);

                //금형치공구 리스트 전체선택하여 삭제
                if (this.uGridDurableMat.Rows.Count > 0)
                {
                    this.uGridDurableMat.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableMat.Rows.All);
                    this.uGridDurableMat.DeleteSelectedRows(false);
                }

                InitControl();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMAS0042_Resize(object sender, EventArgs e)
        {
            try
            {
                //if (this.Width > 1070)
                //{
                //    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                //}
                //else
                //{
                //    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                //}

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void frmMAS0042_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        #region Button

        /// <summary>
        /// Lot테이블의 선택된 행이 숨겨진다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonDeleteLot_Click(object sender, EventArgs e)
        {
            try
            {
                Infragistics.Win.Misc.UltraButton ubtn = (Infragistics.Win.Misc.UltraButton)sender;

                if (this.uGridDurableLot.Rows.Count > 0)
                {
                    if (ubtn.Name.Equals("uButtonDeleteLotInfo"))
                        DeleteDurableLot();
                    else
                    {
                        for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGridDurableLot.Rows[i].Cells["Check"].Value))
                            {
                                this.uGridDurableLot.Rows[i].Hidden = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //행 삭제 선택된 Model정보의 행이 숨겨진다.
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridModel.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridModel.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridModel.Rows[i].Cells["Check"].Value))
                        {
                            this.uGridModel.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //행 삭제 선택된 Package정보의 행이 숨겨진다.
        private void uButtonRowDel_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridPackage.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridPackage.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridPackage.Rows[i].Cells["Check"].Value))
                        {
                            this.uGridPackage.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //선택한 모델 이동
        private void uButtonModelOK_Click(object sender, EventArgs e)
        {
            try
            {
                //모델리스트가 없으면 리턴
                if (this.uGridModelList.Rows.Count == 0)
                    return;

                if (this.uTextPlantName.Tag == null)
                    return;

                //공장,치공구코드 정보저장
                string strPlantCode = this.uTextPlantName.Tag.ToString();
                string strDurableMatCode = this.uTextDurableMatCode.Text;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                DataTable dtSource = new DataTable();
                dtSource.Columns.Add("DurableMatCode", typeof(string));
                dtSource.Columns.Add("PlantCode", typeof(string));
                dtSource.Columns.Add("ModelName", typeof(string));

                //모델정보를 담았는지 체크하는 변수
                string strSave = "";

                //For문을 돌며 체크박스에 선택되어있는 설비정보를 이동시킨다.
                //단 Model그리드 정보를 가지고 있으면 이동하지 않는다.
                for (int i = 0; i < this.uGridModelList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridModelList.Rows[i].Cells["Check"].Value).Equals(true))
                    {
                        string strChk = "";
                        //모델정보가 이미 있으면 중복확인
                        if (this.uGridModel.Rows.Count > 0)
                        {

                            //모델정보 그리드를 돌며 중복확인
                            for (int j = 0; j < this.uGridModel.Rows.Count; j++)
                            {
                                if (this.uGridModel.Rows[j].Hidden.Equals(false))
                                {
                                    //ModelName중복확인
                                    if (this.uGridModel.Rows[j].Cells["ModelName"].Value.Equals(this.uGridModelList.Rows[i].Cells["ModelName"].Value))
                                    {
                                        if (strChk.Equals(string.Empty))
                                            strChk = this.uGridModelList.Rows[i].Cells["ModelName"].Value.ToString();
                                    }
                                    if (strSave.Equals(string.Empty))
                                    {
                                        DataRow drSource = dtSource.NewRow();

                                        drSource["DurableMatCode"] = strDurableMatCode;
                                        drSource["PlantCode"] = strPlantCode;
                                        drSource["ModelName"] = this.uGridModel.Rows[j].Cells["ModelName"].Value;

                                        dtSource.Rows.Add(drSource);
                                    }
                                }
                            }

                            if (strSave.Equals(string.Empty))
                                strSave = "ok";
                        }

                        if (strChk.Equals(string.Empty))
                        {

                            int intCnt = this.uGridModel.Rows.Count;

                            //데이터테이블에 선택한 모델정보를 추가한다.
                            DataRow drSource = dtSource.NewRow();

                            drSource["DurableMatCode"] = strDurableMatCode;
                            drSource["PlantCode"] = strPlantCode;
                            drSource["ModelName"] = this.uGridModelList.Rows[i].Cells["ModelName"].Value;

                            dtSource.Rows.Add(drSource);

                        }

                        // 이동한 모델정보를 숨긴다.
                        if (this.uGridModelList.Rows[i].Hidden.Equals(false))
                        {
                            this.uGridModelList.Rows[i].Hidden = true;
                            this.uGridModelList.Rows[i].Cells["Check"].Value = false;
                        }
                    }
                }
                //그리드에 바인드
                this.uGridModel.DataSource = dtSource;
                this.uGridModel.DataBind();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //선택한Package를 이동
        private void uButtonPackageOK_Click(object sender, EventArgs e)
        {
            try
            {
               
                //Package리스트가 없으면 리턴
                if (this.uGridPackageList.Rows.Count == 0)
                    return;

                if (this.uTextPlantName.Tag == null)
                    return;

                //공장,치공구코드 정보저장
                string strPlantCode = this.uTextPlantName.Tag.ToString();
                string strDurableMatCode = this.uTextDurableMatCode.Text;

                DataTable dtSource = new DataTable();
                dtSource.Columns.Add("DurableMatCode", typeof(string));
                dtSource.Columns.Add("PlantCode", typeof(string));
                dtSource.Columns.Add("Package", typeof(string));

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //Package정보를 담았는지 체크하는 변수
                string strSave = "";

                //For문을 돌며 체크박스에 선택되어있는 설비정보를 이동시킨다.
                //단 Package그리드에 이미 설비정보를 가지고 있으면 이동하지 않는다.
                for (int i = 0; i < this.uGridPackageList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridPackageList.Rows[i].Cells["Check"].Value).Equals(true))
                    {
                        string strChk = "";
                        if (this.uGridPackage.Rows.Count > 0)
                        {

                            //Package리스트 그리드를 돌며 중복확인
                            for (int j = 0; j < this.uGridPackage.Rows.Count; j++)
                            {
                                if (this.uGridPackage.Rows[j].Hidden.Equals(false))
                                {
                                    //Package중복확인
                                    if (this.uGridPackage.Rows[j].Cells["Package"].Value.Equals(this.uGridPackageList.Rows[i].Cells["Package"].Value))
                                    {
                                        if (strChk.Equals(string.Empty))
                                            strChk = this.uGridPackageList.Rows[i].Cells["Package"].Value.ToString();
                                    }
                                    if (strSave.Equals(string.Empty))
                                    {
                                        DataRow drSource = dtSource.NewRow();

                                        drSource["DurableMatCode"] = strDurableMatCode;
                                        drSource["PlantCode"] = strPlantCode;
                                        drSource["Package"] = this.uGridPackage.Rows[j].Cells["Package"].Value;
                                       

                                        dtSource.Rows.Add(drSource);
                                    }
                                }
                            }

                            if (strSave.Equals(string.Empty))
                                strSave = "ok";
                        }

                        if (strChk.Equals(string.Empty))
                        {

                            int intCnt = this.uGridPackage.Rows.Count;

                            //데이터테이블에 선택한 Package정보를 추가한다.
                            DataRow drSource = dtSource.NewRow();

                            drSource["DurableMatCode"] = strDurableMatCode;
                            drSource["PlantCode"] = strPlantCode;
                            drSource["Package"] = this.uGridPackageList.Rows[i].Cells["Package"].Value;

                            dtSource.Rows.Add(drSource);
                        }

                        // 이동한 Package정보를 숨긴다.
                        if (this.uGridPackageList.Rows[i].Hidden.Equals(false))
                        {
                            this.uGridPackageList.Rows[i].Hidden = true;
                            this.uGridPackageList.Rows[i].Cells["Check"].Value = false;
                        }
                    }
                }

                //Package그리드에 바인드
                this.uGridPackage.DataSource = dtSource;
                this.uGridPackage.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //Package조회
        private void uButtonSearchP_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uTextPlantName.Tag == null 
                    || this.uTextPlantName.Tag.ToString().Equals(string.Empty)
                    || this.uTextDurableMatCode.Text.Equals(string.Empty))
                    return;

                string strPlantCode = this.uTextPlantName.Tag.ToString();
                string strCustomer = this.uComboCustomer.Value.ToString();

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //ProgressBar보이기
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서Change
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //제품정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Product), "Product");
                QRPMAS.BL.MASMAT.Product clsProduct = new QRPMAS.BL.MASMAT.Product();
                brwChannel.mfCredentials(clsProduct);

                //패키지콤보매서드 호출
                DataTable dtPackage = clsProduct.mfReadMASProduct_Package_Customer(strPlantCode, strCustomer,m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridPackageList.DataSource = dtPackage;
                this.uGridPackageList.DataBind();

                //PrograssBar 종료
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtPackage.Rows.Count == 0)
                {
                    DialogResult DResult = new DialogResult();
                    // 조회 결과가 없을시 메세지창 띄움
                    WinMessageBox msg = new WinMessageBox();
                    
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //Model조회
        private void uButtonSearchM_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.uTextPlantName.Tag == null 
                    || this.uTextPlantName.Tag.ToString().Equals(string.Empty)
                    || this.uTextDurableMatCode.Text.Equals(string.Empty))
                    return;

                string strPlantCode = this.uTextPlantName.Tag.ToString();
                string strStationCode = this.uComboStation.Value.ToString();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //ProgressBar보이기
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서Change
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //설비정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                //모델명조회 매서드 호출
                DataTable dtModel = clsEquip.mfReadEquipModel_Station(strPlantCode,strStationCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인드
                this.uGridModelList.DataSource = dtModel;
                this.uGridModelList.DataBind();

                //PrograssBar 종료
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                if (dtModel.Rows.Count == 0)
                {
                    DialogResult DResult = new DialogResult();
                    // 조회 결과가 없을시 메세지창 띄움
                    WinMessageBox msg = new WinMessageBox();

                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Lot입력의뢰버튼 클릭 시 신규Lot정보 팝업창이 나타난다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButton_Click(object sender, EventArgs e)
        {
            try
            {
                //공장코드가 없는경우 리턴
                if (this.uTextPlantName.Tag == null
                    || this.uTextPlantName.Tag.ToString().Equals(string.Empty)
                    || this.uTextDurableMatCode.Text.Equals(string.Empty))
                    return;

                //신규 Lot정보 팝업창호출
                frmMAS0042_P1 frmP1 = new frmMAS0042_P1(this.uTextPlantName.Tag.ToString());
                frmP1.ShowDialog();

                if (frmP1.LotInfo.Rows.Count == 0)
                {
                    frmP1.LotInfo.Dispose();
                    return;
                }


                //그리드의 조건에 맞는 컬럼설정
                DataTable dtLotInfo = new DataTable();
                dtLotInfo.Columns.Add("PlantCode", typeof(string));
                dtLotInfo.Columns.Add("Package", typeof(string));
                dtLotInfo.Columns.Add("ModelName", typeof(string));
                dtLotInfo.Columns.Add("EquipCode", typeof(string));
                dtLotInfo.Columns.Add("LotNo", typeof(string));
                dtLotInfo.Columns.Add("RepairReqCode", typeof(string));
                dtLotInfo.Columns.Add("DurableMatCode", typeof(string));
                dtLotInfo.Columns.Add("GRDate", typeof(string));
                dtLotInfo.Columns.Add("CurUsage", typeof(string));
                dtLotInfo.Columns.Add("CumUsage", typeof(string));
                dtLotInfo.Columns.Add("LastInspectDate", typeof(string));
                dtLotInfo.Columns.Add("DiscardName", typeof(string));
                dtLotInfo.Columns.Add("DiscardDate", typeof(string));
                dtLotInfo.Columns.Add("DiscardChargeID", typeof(string));
                dtLotInfo.Columns.Add("DiscardChargeName", typeof(string));
                dtLotInfo.Columns.Add("DiscardReason", typeof(string));
                dtLotInfo.Columns.Add("WriteID", typeof(string));

                //기존Lot정보가 있는 경우 빈테이블에 넣어두고 받아온정보(팝업창정보)가 있는 경우 저장한다.
                if (this.uGridDurableLot.Rows.Count > 0)
                {
                    DataRow drLot;
                    for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                    {
                        // 
                        if (this.uGridDurableLot.Rows[i].Hidden.Equals(false) && !this.uGridDurableLot.Rows[i].GetCellValue("LotNo").ToString().Equals(string.Empty))
                        {
                            drLot = dtLotInfo.NewRow();

                            drLot["PlantCode"] = this.uGridDurableLot.Rows[i].GetCellValue("PlantCode");
                            drLot["DurableMatCode"] = this.uGridDurableLot.Rows[i].GetCellValue("DurableMatCode");
                            drLot["GRDate"] = this.uGridDurableLot.Rows[i].GetCellValue("GRDate");
                            drLot["LotNo"] = this.uGridDurableLot.Rows[i].GetCellValue("LotNo");
                            drLot["CurUsage"] = this.uGridDurableLot.Rows[i].GetCellValue("CurUsage");
                            drLot["CumUsage"] = this.uGridDurableLot.Rows[i].GetCellValue("CumUsage");
                            drLot["LastInspectDate"] = this.uGridDurableLot.Rows[i].GetCellValue("LastInspectDate");
                            drLot["DiscardName"] = this.uGridDurableLot.Rows[i].GetCellValue("DiscardName");
                            drLot["DiscardDate"] = this.uGridDurableLot.Rows[i].GetCellValue("DiscardDate");
                            drLot["DiscardChargeID"] = this.uGridDurableLot.Rows[i].GetCellValue("DiscardChargeID");
                            drLot["DiscardChargeName"] = this.uGridDurableLot.Rows[i].GetCellValue("DiscardChargeName");
                            drLot["DiscardReason"] = this.uGridDurableLot.Rows[i].GetCellValue("DiscardReason");
                            drLot["Package"] = this.uGridDurableLot.Rows[i].GetCellValue("Package");
                            drLot["ModelName"] = this.uGridDurableLot.Rows[i].GetCellValue("ModelName");
                            drLot["EquipCode"] = this.uGridDurableLot.Rows[i].GetCellValue("EquipCode");
                            drLot["RepairReqCode"] = this.uGridDurableLot.Rows[i].GetCellValue("RepairReqCode");

                            dtLotInfo.Rows.Add(drLot);
                        }

                    }
                    string strChk="";
                    for (int i = 0; i < frmP1.LotInfo.Rows.Count; i++)
                    {
                        for (int j = 0; j < this.uGridDurableLot.Rows.Count; j++)
                        {
                            if (frmP1.LotInfo.Rows[i]["LotNo"].ToString().Equals(this.uGridDurableLot.Rows[j].Cells["LotNo"].Value.ToString()))
                            {
                                strChk = "O";
                            }
                        }

                        //팝업창에서 가져온 LotNo가 기존 Lot정보그리드에 없는 경우 신규Lot정보를 담는다.
                        if (strChk.Equals(string.Empty))
                        {
                            drLot = dtLotInfo.NewRow();

                            drLot["PlantCode"] = frmP1.LotInfo.Rows[i]["PlantCode"];
                            drLot["DurableMatCode"] = frmP1.LotInfo.Rows[i]["DurableMatCode"];
                            drLot["GRDate"] = frmP1.LotInfo.Rows[i]["GRDate"];
                            drLot["LotNo"] = frmP1.LotInfo.Rows[i]["LotNo"];
                            drLot["CurUsage"] = frmP1.LotInfo.Rows[i]["CurUsage"];
                            drLot["CumUsage"] = frmP1.LotInfo.Rows[i]["CumUsage"];
                            drLot["LastInspectDate"] = frmP1.LotInfo.Rows[i]["LastInspectDate"];
                            drLot["DiscardName"] = frmP1.LotInfo.Rows[i]["DiscardName"];
                            drLot["DiscardDate"] = frmP1.LotInfo.Rows[i]["DiscardDate"];
                            drLot["DiscardChargeID"] = frmP1.LotInfo.Rows[i]["DiscardChargeID"];
                            drLot["DiscardChargeName"] = frmP1.LotInfo.Rows[i]["DiscardChargeName"];
                            drLot["DiscardReason"] = frmP1.LotInfo.Rows[i]["DiscardReason"];
                            drLot["Package"] = frmP1.LotInfo.Rows[i]["Package"];
                            drLot["ModelName"] = frmP1.LotInfo.Rows[i]["ModelName"];
                            drLot["EquipCode"] = frmP1.LotInfo.Rows[i]["EquipCode"];
                            drLot["RepairReqCode"] = frmP1.LotInfo.Rows[i]["RepairReqCode"];
                            drLot["WriteID"] = frmP1.LotInfo.Rows[i]["WriteID"];
                            dtLotInfo.Rows.Add(drLot);
                        }
                        strChk = string.Empty;
                    }

                    this.uGridDurableLot.DataSource = dtLotInfo;
                    this.uGridDurableLot.DataBind();
                }
                else
                {
                    //기존정보가 없는 경우 받아온 정보(팝업창정보)를 바인드 시킨다.
                    this.uGridDurableLot.DataSource = frmP1.LotInfo;
                    this.uGridDurableLot.DataBind();
                }

                frmP1.LotInfo.Dispose();

                //그리드 바인드 후 기존Lot정보,받아온Lot정보(팝업창정보)는 수정불가처리를 하고, 신규등록한 Lot정보와 받아온Lot정보(팝업창정보)에 편집이미지처리를 한다.
                if (this.uGridDurableLot.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                    {
                        if (!this.uGridDurableLot.Rows[i].GetCellValue("DurableMatCode").ToString().Equals(string.Empty))
                            this.uGridDurableLot.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        else
                            this.uGridDurableLot.Rows[i].RowSelectorAppearance.Image = SysRes.ModifyCellImage;

                        if (!this.uGridDurableLot.Rows[i].GetCellValue("RepairReqCode").ToString().Equals(string.Empty))
                        {
                            this.uGridDurableLot.Rows[i].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }

                    }
                }

                //frmP1.Dispose();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 그리드이벤트

        private void uGridDurableLot_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                    
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableLot, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                

                if (e.Cell.Column.Key.Equals("LotNo") )
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                        WinMessageBox msg = new WinMessageBox();

                        for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index)
                                && e.Cell.Value.ToString().ToUpper().Equals(this.uGridDurableLot.Rows[i].Cells["LotNo"].Value.ToString().ToUpper()))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000079", "M000852", Infragistics.Win.HAlign.Right);
                                e.Cell.Value = string.Empty;
                                return;
                            }
                        }
                    }
                    else
                    {
                        e.Cell.Row.Delete(false);
                    }

                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridDurableLot_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        private void uGridDurableMat_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableMat, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridDurableMat_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        }

        //셀업데이트시 편집이미지가 RowSelector에 박힌다
        private void uGridModel_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridModel, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //셀업데이트시 편집이미지가 RowSelector에 박힌다
        private void uGridPackage_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridPackage, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridDurableMatBOM_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                // 자동행삭제

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDurableMat, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //Model,Package리스트 그리드 셀클릭시 체크박스상태변화
        private void uGrid_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.Equals("ModelName"))
                {
                    if (Convert.ToBoolean(e.Cell.Row.Cells["Check"].Value))
                        e.Cell.Row.Cells["Check"].Value = false;
                    else
                        e.Cell.Row.Cells["Check"].Value = true;
                }
                if (e.Cell.Column.Key.Equals("Package"))
                {
                    if (Convert.ToBoolean(e.Cell.Row.Cells["Check"].Value))
                        e.Cell.Row.Cells["Check"].Value = false;
                    else
                        e.Cell.Row.Cells["Check"].Value = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //헤더그리드 정보더블클릭시 상세정보를 보여줌
        private void uGridDurableMat_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                string strPlantCode = "";
                string strPlantName = "";
                if (e.Cell.Row.Cells["PlantCode"].Value == null
                    || e.Cell.Row.Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                {
                    strPlantCode = this.uComboSearchPlant.Value.ToString();
                    strPlantName = this.uComboSearchPlant.Text;
                }
                else
                {
                    strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    strPlantName = e.Cell.Row.Cells["PlantName"].Value.ToString();
                }
                string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();

                InitControl();

                //공장과 치공구코드가 있을 경우
                if (!strPlantCode.Equals(string.Empty) && !strDurableMatCode.Equals(string.Empty))
                {

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    WinMessageBox msg = new WinMessageBox();
                    //ProgressBar보이기
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                    //커서Change
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //--------------------------------------------------------------------처리로직----------------------------------------------------------//
                    //해당정보 텍스트에 삽입
                    this.uTextPlantName.Tag = strPlantCode;
                    this.uTextPlantName.Text = strPlantName;
                    this.uTextDurableMatCode.Text = strDurableMatCode;
                    this.uTextDurableMatName.Text = e.Cell.Row.Cells["DurableMatName"].Value.ToString();
                    this.uCheckSerial.Checked = e.Cell.Row.Cells["SerialFlag"].Value.ToString() == "T" ? true : false;

                    //금형치공구BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsDurableMat);


                    DataTable dtDurableMat = new DataTable();
                    //조회함수호출
                    dtDurableMat = clsDurableMat.mfReadDurableMatLot(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                    //그리드에 바인딩
                    this.uGridDurableLot.DataSource = dtDurableMat;
                    this.uGridDurableLot.DataBind();

                    GridCheckAuth();
                    

                    #region Package,Model INFO

                    //금형치공구BOM_Model정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMatBOM_Model), "DurableMatBOM_Model");
                    QRPMAS.BL.MASDMM.DurableMatBOM_Model clsBOM_Model = new QRPMAS.BL.MASDMM.DurableMatBOM_Model();
                    brwChannel.mfCredentials(clsBOM_Model);

                    //금형치공구BOM_Model정보조회 매서드 실행
                    DataTable dtSource = clsBOM_Model.mfReadDurableMatBOM_Model(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                    //그리드에 바인드
                    this.uGridModel.DataSource = dtSource;
                    this.uGridModel.DataBind();

                    //금형치공구BOM_Package정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMatBOM_Package), "DurableMatBOM_Package");
                    QRPMAS.BL.MASDMM.DurableMatBOM_Package clsBOM_Package = new QRPMAS.BL.MASDMM.DurableMatBOM_Package();
                    brwChannel.mfCredentials(clsBOM_Package);

                    //금형치공구BOM_Package정보 조회 매서드 실행
                    dtSource = clsBOM_Package.mfReadDurableMatBOM_Package(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                    //그리드에 바인드
                    this.uGridPackage.DataSource = dtSource;
                    this.uGridPackage.DataBind();

                    #endregion

                    #region Station,Customer,Inventory Combo

                    //Station정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    //Station 콤보정보 조회매서드 실행
                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //고객사 정보 BL 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Customer), "Customer");
                    QRPMAS.BL.MASGEN.Customer clsCustomer = new QRPMAS.BL.MASGEN.Customer();
                    brwChannel.mfCredentials(clsCustomer);

                    //고객사정보콤보 매서드 실행
                    DataTable dtCustomer = clsCustomer.mfReadCustomer_Combo(m_resSys.GetString("SYS_LANG"));

                    //금형치공구창고정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                    QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                    brwChannel.mfCredentials(clsDurableInventory);

                    //금형치공구창고정보 콤보 조회 매서드 실행
                    DataTable dtInventory = clsDurableInventory.mfReadDurableInventoryCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));


                    //콤보에 바인드
                    WinComboEditor wCom = new WinComboEditor();

                    wCom.mfSetComboEditor(this.uComboStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "StationCode", "StationName", dtStation);

                    wCom.mfSetComboEditor(this.uComboCustomer, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "전체", "CustomerCode", "CustomerName", dtCustomer);

                    wCom.mfSetComboEditor(this.uComboDurableInven, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left
                    , "", "", "선택", "DurableInventoryCode", "DurableInventoryName", dtInventory);


                    #endregion

                    //PrograssBar 종료
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);


                    if (dtDurableMat.Rows.Count > 0)
                    {
                        WinGrid grd = new WinGrid();
                        grd.mfSetAutoResizeColWidth(this.uGridDurableLot, 0);

                    }

                    //Serial 사용여부에 따라 Lot정보 입력 설정
                    if (this.uCheckSerial.Checked)
                    {
                        this.uComboDurableInven.Enabled = true;
                        this.uGridDurableLot.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    }
                    else
                    {
                        this.uComboDurableInven.Enabled = false;
                        this.uGridDurableLot.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    }
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridDurableMat_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            e.Row.Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit; // 삭제가능시 주석.
        }

        #endregion

        #endregion

        #region Method

        /// <summary>
        /// 상세정보 컨트롤 초기화
        /// </summary>
        private void InitControl()
        {
            try
            {
                //Lot그리드 전체행을 선택하여 삭제
                if (this.uGridDurableLot.Rows.Count > 0)
                {
                    this.uGridDurableLot.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridDurableLot.Rows.All);
                    this.uGridDurableLot.DeleteSelectedRows(false);
                }
                //Model정보 그리드 전체행 선택하여 삭제
                if (this.uGridModel.Rows.Count > 0)
                {
                    this.uGridModel.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridModel.Rows.All);
                    this.uGridModel.DeleteSelectedRows(false);
                }

                //ModelList그리드 전체행을 선택하여 삭제
                if (this.uGridModelList.Rows.Count > 0)
                {
                    this.uGridModelList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridModelList.Rows.All);
                    this.uGridModelList.DeleteSelectedRows(false);
                }

                //PackageList그리드 전체행을 선택하여 삭제
                if (this.uGridPackageList.Rows.Count > 0)
                {
                    this.uGridPackageList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridPackageList.Rows.All);
                    this.uGridPackageList.DeleteSelectedRows(false);
                }

                //Package그리드 전체행을 선택하여 삭제
                if (this.uGridPackage.Rows.Count > 0)
                {
                    this.uGridPackage.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridPackage.Rows.All);
                    this.uGridPackage.DeleteSelectedRows(false);
                }


                //콤보 아이템 초기화
                if (this.uComboStation.Items.Count > 0)
                    this.uComboStation.Items.Clear();

                if (this.uComboCustomer.Items.Count > 0)
                    this.uComboCustomer.Items.Clear();

                this.uComboDeleteType.Value = "";


                //텍스트컨트롤 클리어
                this.uTextPlantName.Tag = string.Empty;
                this.uTextPlantName.Clear();
                this.uTextDurableMatCode.Clear();
                this.uTextDurableMatName.Clear();
                this.uCheckSerial.Checked = false;


                //if (this.uGridDurableLot.DisplayLayout.Override.AllowAddNew.Equals(Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom))
                //    this.uGridDurableLot.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 금형치공구 삭제
        /// </summary>
        private void DeleteDurableLot()
        {
            try
            {
                DataTable dtDurableLot = new DataTable();

                //금형치공구Lot정보 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                brwChannel.mfCredentials(clsDurableLot);

                //금형치공구Lot컬럼정보 저장
                dtDurableLot = clsDurableLot.mfSetDelDatainfo();


                string strPlantCode = "";

                if (this.uTextPlantName.Tag != null && this.uTextPlantName.Tag.ToString().Equals(string.Empty))
                    strPlantCode = this.uTextPlantName.Tag.ToString();
                else
                    strPlantCode = this.uComboSearchPlant.Value.ToString();

                string strDurableMatCode = this.uTextDurableMatCode.Text;

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                string strMoveType = this.uComboDeleteType.Value == null ? "" : this.uComboDeleteType.Value.ToString();

                if (strMoveType.Equals(string.Empty))
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "확인창", "필수입력사항확인", "삭제 유형을 선택해주세요.",
                                                    Infragistics.Win.HAlign.Right);
                    this.uComboDeleteType.DropDown();
                    return;
                }

                for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                {
                    if (this.uGridDurableLot.Rows[i].RowSelectorAppearance.Image == null)
                        continue;

                    if (!Convert.ToBoolean(this.uGridDurableLot.Rows[i].GetCellValue("Check")))
                        continue;

                    if (this.uGridDurableLot.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        continue;


                    DataRow drRow = dtDurableLot.NewRow();

                    drRow["PlantCode"] = strPlantCode;
                    drRow["DurableMatCode"] = strDurableMatCode;
                    drRow["LotNo"] = this.uGridDurableLot.Rows[i].GetCellValue("LotNo").ToString();
                    drRow["MoveGubunCode"] = strMoveType;

                    dtDurableLot.Rows.Add(drRow);


                }

                if (dtDurableLot.Rows.Count == 0)
                    return;

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M000650", "M000675",
                                    Infragistics.Win.HAlign.Right) == DialogResult.No)
                    return;



                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;


                TransErrRtn ErrRtn = new TransErrRtn();

                string strErrRtn = clsDurableLot.mfDeleteDurableLot(dtDurableLot, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                System.Windows.Forms.DialogResult result;

                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M000638", "M000677",
                                                Infragistics.Win.HAlign.Right);

                    //금형치공구BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                    QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                    brwChannel.mfCredentials(clsDurableMat);

                    DataTable dtDurableMat = new DataTable();
                    //조회함수호출
                    dtDurableMat = clsDurableMat.mfReadDurableMatLot(strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                    //그리드에 바인딩
                    this.uGridDurableLot.DataSource = dtDurableMat;
                    this.uGridDurableLot.DataBind();

                    GridCheckAuth();
                }
                else
                {
                    string strMes = "";
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMes = msg.GetMessge_Text("M000676",m_resSys.GetString("SYS_LANG"));
                    else
                        strMes = ErrRtn.ErrMessage;

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135",m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M000638",m_resSys.GetString("SYS_LANG")), strMes,
                                                 Infragistics.Win.HAlign.Right);

                }



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// LotNo 삭제권한
        /// </summary>
        private void ButtonAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strUserID = m_resSys.GetString("SYS_USERID").ToUpper();
                /*
                 * ST02675A 문성권 주임님 제조3팀
                 * ST01389A 유재현 주임님 제조2팀
                 * ST00746A 김국보 대리님 제조2팀
                 * ST00502A 김완교 대리님 제조3팀
                 * ST01184A 송영민 대리님 제조2팀
                 * ST01004A 염문중 대리님 제조2팀
                 * ST02974A 홍성현 대리님 제조1팀
                 * */

                if (strUserID.Equals("ST02675A")
                    || strUserID.Equals("ST01389A")
                    || strUserID.Equals("ST00746A")
                    || strUserID.Equals("ST00502A") 
                    || strUserID.Equals("ST01184A")
                    || strUserID.Equals("ST01004A")
                    || strUserID.Equals("ST02974A")
                    || strUserID.Equals("TESTUSR"))
                {
                    this.uComboDeleteType.Visible = true;
                    this.uButtonDeleteLotInfo.Visible = true;
                    this.uButtonDeleteLot.Visible = false;
                }
                else
                {
                    this.uButtonDeleteLot.Visible = true;
                    this.uComboDeleteType.Visible = false;
                    this.uButtonDeleteLotInfo.Visible = false;
                }



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Lot 정보 등록 그리드 등록된 LotNo 선택 권한
        /// </summary>
        private void GridCheckAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strUserID = m_resSys.GetString("SYS_USERID").ToUpper();
                /*
                 * ST02675A 문성권 주임님 제조3팀
                 * ST01389A 유재현 주임님 제조2팀
                 * ST00746A 김국보 대리님 제조2팀
                 * ST00502A 김완교 대리님 제조3팀
                 * ST01184A 송영민 대리님 제조2팀
                 * ST01004A 염문중 대리님 제조2팀
                 * ST02974A 홍성현 대리님 제조1팀
                 * */

                if (strUserID.Equals("ST02675A")
                    || strUserID.Equals("ST01389A")
                    || strUserID.Equals("ST00746A")
                    || strUserID.Equals("ST00502A")
                    || strUserID.Equals("ST01184A")
                    || strUserID.Equals("ST01004A")
                    || strUserID.Equals("ST02974A")
                    || strUserID.Equals("TESTUSR"))
                {
                    //바인드된 정보 행삭제 불가
                    for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                    {
                        this.uGridDurableLot.Rows[i].Cells["GRDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurableLot.Rows[i].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurableLot.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;

                    }
                }
                else
                {
                    //바인드된 정보 행삭제 불가
                    for (int i = 0; i < this.uGridDurableLot.Rows.Count; i++)
                    {
                        this.uGridDurableLot.Rows[i].Cells["GRDate"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurableLot.Rows[i].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridDurableLot.Rows[i].Cells["Check"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

    }
}
