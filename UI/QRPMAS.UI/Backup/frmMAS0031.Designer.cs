﻿namespace QRPMAS.UI
{
    partial class frmMAS0031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0031));
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGridCustomer = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridCustomerP = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUseFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCustomerCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextBossName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRegNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCustomerName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFax = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAddress = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelBossName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRegNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomerCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomerName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomerNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelCustomerNameCh = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridCustomerP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBossName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGridCustomer
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCustomer.DisplayLayout.Appearance = appearance5;
            this.uGridCustomer.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCustomer.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCustomer.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCustomer.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridCustomer.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCustomer.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridCustomer.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCustomer.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCustomer.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCustomer.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridCustomer.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCustomer.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCustomer.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCustomer.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridCustomer.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCustomer.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCustomer.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridCustomer.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridCustomer.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCustomer.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridCustomer.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridCustomer.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCustomer.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridCustomer.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCustomer.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCustomer.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCustomer.Location = new System.Drawing.Point(0, 40);
            this.uGridCustomer.Name = "uGridCustomer";
            this.uGridCustomer.Size = new System.Drawing.Size(1070, 800);
            this.uGridCustomer.TabIndex = 4;
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 755);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 95);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 755);
            this.uGroupBoxContentsArea.TabIndex = 5;
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 735);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uGridCustomerP);
            this.uGroupBox1.Controls.Add(this.ultraGroupBox1);
            this.uGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBox1.Location = new System.Drawing.Point(0, 140);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1064, 595);
            this.uGroupBox1.TabIndex = 93;
            // 
            // uGridCustomerP
            // 
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridCustomerP.DisplayLayout.Appearance = appearance30;
            this.uGridCustomerP.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridCustomerP.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCustomerP.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCustomerP.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.uGridCustomerP.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridCustomerP.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.uGridCustomerP.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridCustomerP.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridCustomerP.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance33.BackColor = System.Drawing.SystemColors.Highlight;
            appearance33.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridCustomerP.DisplayLayout.Override.ActiveRowAppearance = appearance33;
            this.uGridCustomerP.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridCustomerP.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.uGridCustomerP.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridCustomerP.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGridCustomerP.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridCustomerP.DisplayLayout.Override.CellPadding = 0;
            appearance35.BackColor = System.Drawing.SystemColors.Control;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridCustomerP.DisplayLayout.Override.GroupByRowAppearance = appearance35;
            appearance37.TextHAlignAsString = "Left";
            this.uGridCustomerP.DisplayLayout.Override.HeaderAppearance = appearance37;
            this.uGridCustomerP.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridCustomerP.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.uGridCustomerP.DisplayLayout.Override.RowAppearance = appearance36;
            this.uGridCustomerP.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridCustomerP.DisplayLayout.Override.TemplateAddRowAppearance = appearance34;
            this.uGridCustomerP.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridCustomerP.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridCustomerP.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridCustomerP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridCustomerP.Location = new System.Drawing.Point(3, 48);
            this.uGridCustomerP.Name = "uGridCustomerP";
            this.uGridCustomerP.Size = new System.Drawing.Size(1058, 544);
            this.uGridCustomerP.TabIndex = 3;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uButtonDeleteRow);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1058, 48);
            this.ultraGroupBox1.TabIndex = 2;
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(11, 11);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 1;
            this.uButtonDeleteRow.Text = "ultraButton1";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uTextUseFlag);
            this.ultraGroupBox2.Controls.Add(this.uLabelUseFlag);
            this.ultraGroupBox2.Controls.Add(this.uTextCustomerCode);
            this.ultraGroupBox2.Controls.Add(this.uTextFax);
            this.ultraGroupBox2.Controls.Add(this.uTextBossName);
            this.ultraGroupBox2.Controls.Add(this.uTextCustomerNameEn);
            this.ultraGroupBox2.Controls.Add(this.uTextTel);
            this.ultraGroupBox2.Controls.Add(this.uTextCustomerNameCh);
            this.ultraGroupBox2.Controls.Add(this.uTextAddress);
            this.ultraGroupBox2.Controls.Add(this.uTextRegNo);
            this.ultraGroupBox2.Controls.Add(this.uTextCustomerName);
            this.ultraGroupBox2.Controls.Add(this.uLabelFax);
            this.ultraGroupBox2.Controls.Add(this.uLabelTel);
            this.ultraGroupBox2.Controls.Add(this.uLabelAddress);
            this.ultraGroupBox2.Controls.Add(this.uLabelBossName);
            this.ultraGroupBox2.Controls.Add(this.uLabelRegNo);
            this.ultraGroupBox2.Controls.Add(this.uLabelCustomerCode);
            this.ultraGroupBox2.Controls.Add(this.uLabelCustomerName);
            this.ultraGroupBox2.Controls.Add(this.uLabelCustomerNameEn);
            this.ultraGroupBox2.Controls.Add(this.uLabelCustomerNameCh);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1064, 140);
            this.ultraGroupBox2.TabIndex = 92;
            // 
            // uTextUseFlag
            // 
            appearance20.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextUseFlag.Appearance = appearance20;
            this.uTextUseFlag.Location = new System.Drawing.Point(672, 108);
            this.uTextUseFlag.Name = "uTextUseFlag";
            this.uTextUseFlag.ReadOnly = true;
            this.uTextUseFlag.Size = new System.Drawing.Size(100, 21);
            this.uTextUseFlag.TabIndex = 114;
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(548, 108);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(120, 20);
            this.uLabelUseFlag.TabIndex = 113;
            // 
            // uTextCustomerCode
            // 
            appearance25.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextCustomerCode.Appearance = appearance25;
            this.uTextCustomerCode.Location = new System.Drawing.Point(136, 12);
            this.uTextCustomerCode.Name = "uTextCustomerCode";
            this.uTextCustomerCode.ReadOnly = true;
            this.uTextCustomerCode.Size = new System.Drawing.Size(150, 21);
            this.uTextCustomerCode.TabIndex = 112;
            // 
            // uTextFax
            // 
            appearance17.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextFax.Appearance = appearance17;
            this.uTextFax.Location = new System.Drawing.Point(672, 84);
            this.uTextFax.Name = "uTextFax";
            this.uTextFax.ReadOnly = true;
            this.uTextFax.Size = new System.Drawing.Size(200, 21);
            this.uTextFax.TabIndex = 107;
            // 
            // uTextBossName
            // 
            appearance18.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextBossName.Appearance = appearance18;
            this.uTextBossName.Location = new System.Drawing.Point(672, 60);
            this.uTextBossName.Name = "uTextBossName";
            this.uTextBossName.ReadOnly = true;
            this.uTextBossName.Size = new System.Drawing.Size(200, 21);
            this.uTextBossName.TabIndex = 106;
            // 
            // uTextCustomerNameEn
            // 
            appearance19.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextCustomerNameEn.Appearance = appearance19;
            this.uTextCustomerNameEn.Location = new System.Drawing.Point(672, 36);
            this.uTextCustomerNameEn.Name = "uTextCustomerNameEn";
            this.uTextCustomerNameEn.ReadOnly = true;
            this.uTextCustomerNameEn.Size = new System.Drawing.Size(300, 21);
            this.uTextCustomerNameEn.TabIndex = 105;
            // 
            // uTextTel
            // 
            appearance26.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextTel.Appearance = appearance26;
            this.uTextTel.Location = new System.Drawing.Point(136, 84);
            this.uTextTel.Name = "uTextTel";
            this.uTextTel.ReadOnly = true;
            this.uTextTel.Size = new System.Drawing.Size(200, 21);
            this.uTextTel.TabIndex = 108;
            // 
            // uTextCustomerNameCh
            // 
            appearance22.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextCustomerNameCh.Appearance = appearance22;
            this.uTextCustomerNameCh.Location = new System.Drawing.Point(136, 36);
            this.uTextCustomerNameCh.Name = "uTextCustomerNameCh";
            this.uTextCustomerNameCh.ReadOnly = true;
            this.uTextCustomerNameCh.Size = new System.Drawing.Size(300, 21);
            this.uTextCustomerNameCh.TabIndex = 111;
            // 
            // uTextAddress
            // 
            appearance23.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextAddress.Appearance = appearance23;
            this.uTextAddress.Location = new System.Drawing.Point(136, 108);
            this.uTextAddress.Name = "uTextAddress";
            this.uTextAddress.ReadOnly = true;
            this.uTextAddress.Size = new System.Drawing.Size(400, 21);
            this.uTextAddress.TabIndex = 110;
            // 
            // uTextRegNo
            // 
            appearance24.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextRegNo.Appearance = appearance24;
            this.uTextRegNo.Location = new System.Drawing.Point(136, 60);
            this.uTextRegNo.Name = "uTextRegNo";
            this.uTextRegNo.ReadOnly = true;
            this.uTextRegNo.Size = new System.Drawing.Size(200, 21);
            this.uTextRegNo.TabIndex = 109;
            // 
            // uTextCustomerName
            // 
            appearance14.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextCustomerName.Appearance = appearance14;
            this.uTextCustomerName.Location = new System.Drawing.Point(672, 12);
            this.uTextCustomerName.Name = "uTextCustomerName";
            this.uTextCustomerName.ReadOnly = true;
            this.uTextCustomerName.Size = new System.Drawing.Size(300, 21);
            this.uTextCustomerName.TabIndex = 104;
            // 
            // uLabelFax
            // 
            this.uLabelFax.Location = new System.Drawing.Point(548, 84);
            this.uLabelFax.Name = "uLabelFax";
            this.uLabelFax.Size = new System.Drawing.Size(120, 20);
            this.uLabelFax.TabIndex = 100;
            // 
            // uLabelTel
            // 
            this.uLabelTel.Location = new System.Drawing.Point(12, 84);
            this.uLabelTel.Name = "uLabelTel";
            this.uLabelTel.Size = new System.Drawing.Size(120, 20);
            this.uLabelTel.TabIndex = 99;
            // 
            // uLabelAddress
            // 
            this.uLabelAddress.Location = new System.Drawing.Point(12, 108);
            this.uLabelAddress.Name = "uLabelAddress";
            this.uLabelAddress.Size = new System.Drawing.Size(120, 20);
            this.uLabelAddress.TabIndex = 101;
            // 
            // uLabelBossName
            // 
            this.uLabelBossName.Location = new System.Drawing.Point(548, 60);
            this.uLabelBossName.Name = "uLabelBossName";
            this.uLabelBossName.Size = new System.Drawing.Size(120, 20);
            this.uLabelBossName.TabIndex = 103;
            // 
            // uLabelRegNo
            // 
            this.uLabelRegNo.Location = new System.Drawing.Point(12, 60);
            this.uLabelRegNo.Name = "uLabelRegNo";
            this.uLabelRegNo.Size = new System.Drawing.Size(120, 20);
            this.uLabelRegNo.TabIndex = 102;
            // 
            // uLabelCustomerCode
            // 
            this.uLabelCustomerCode.Location = new System.Drawing.Point(12, 12);
            this.uLabelCustomerCode.Name = "uLabelCustomerCode";
            this.uLabelCustomerCode.Size = new System.Drawing.Size(120, 20);
            this.uLabelCustomerCode.TabIndex = 95;
            this.uLabelCustomerCode.Text = "3";
            // 
            // uLabelCustomerName
            // 
            this.uLabelCustomerName.Location = new System.Drawing.Point(548, 12);
            this.uLabelCustomerName.Name = "uLabelCustomerName";
            this.uLabelCustomerName.Size = new System.Drawing.Size(120, 20);
            this.uLabelCustomerName.TabIndex = 96;
            this.uLabelCustomerName.Text = "4";
            // 
            // uLabelCustomerNameEn
            // 
            this.uLabelCustomerNameEn.Location = new System.Drawing.Point(548, 36);
            this.uLabelCustomerNameEn.Name = "uLabelCustomerNameEn";
            this.uLabelCustomerNameEn.Size = new System.Drawing.Size(120, 20);
            this.uLabelCustomerNameEn.TabIndex = 98;
            this.uLabelCustomerNameEn.Text = "6";
            // 
            // uLabelCustomerNameCh
            // 
            this.uLabelCustomerNameCh.Location = new System.Drawing.Point(12, 36);
            this.uLabelCustomerNameCh.Name = "uLabelCustomerNameCh";
            this.uLabelCustomerNameCh.Size = new System.Drawing.Size(120, 20);
            this.uLabelCustomerNameCh.TabIndex = 97;
            this.uLabelCustomerNameCh.Text = "5";
            // 
            // frmMAS0031
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridCustomer);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0031";
            this.Load += new System.EventHandler(this.frmMAS0031_Load);
            this.Activated += new System.EventHandler(this.frmMAS0031_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0031_FormClosing);
            this.Resize += new System.EventHandler(this.frmMAS0031_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridCustomerP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBossName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCustomerName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCustomer;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridCustomerP;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFax;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBossName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRegNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCustomerName;
        private Infragistics.Win.Misc.UltraLabel uLabelFax;
        private Infragistics.Win.Misc.UltraLabel uLabelTel;
        private Infragistics.Win.Misc.UltraLabel uLabelAddress;
        private Infragistics.Win.Misc.UltraLabel uLabelBossName;
        private Infragistics.Win.Misc.UltraLabel uLabelRegNo;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerCode;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerName;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelCustomerNameCh;
    }
}