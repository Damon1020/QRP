﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통기준정보                                          */
/* 모듈(분류)명 : 자재관리 기준정보                                     */
/* 프로그램ID   : frmMAS0005.cs                                         */
/* 프로그램명   : 자재그룹정보                                          */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가참조
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.Resources;
using System.Threading;
using System.EnterpriseServices;

namespace QRPMAS.UI
{
    public partial class frmMAS0005 : Form, IToolbar
    {
        // 다국어 지원을 위한 전역변수 //
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0005()
        {
            InitializeComponent();
        }

        private void frmMAS0005_Activated(object sender, EventArgs e)
        {
            // ToolBar 활성화
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0005_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // Title Text 설정
            titleArea.mfSetLabelText("자재그룹정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화 Method

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // 그리드 일반 설정 //
                grd.mfInitGeneralGrid(this.uGridMaterialGroupList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼 추가 //
                ////grd.mfSetGridColumn(this.uGridMaterialGroupList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox
                ////    , "", "", "false");

                grd.mfSetGridColumn(this.uGridMaterialGroupList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMaterialGroupList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMaterialGroupList, 0, "MaterialGroupCode", "자재그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMaterialGroupList, 0, "MaterialGroupName", "자재그룹명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMaterialGroupList, 0, "MaterialGroupNameCh", "자재그룹명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMaterialGroupList, 0, "MaterialGroupNameEn", "자재그룹명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridMaterialGroupList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                // Set FontSize
                this.uGridMaterialGroupList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridMaterialGroupList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //////// 그리드에 DropDown 추가
                //////// 그리드 컬럼에 공장 콤보박스 추가
                //////DataTable PlantList = new DataTable();

                //////// BL 호출
                //////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                //////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                //////QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                //////brwChannel.mfCredentials(clsPlant);

                //////// Plant정보를 얻어오는 함수를 호출하여 DataTable에 저장
                //////PlantList = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(uGridMaterialGroupList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", PlantList);

                ////////사용여부
                //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                //////QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                //////brwChannel.mfCredentials(clsCommonCode);

                //////DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                //////grd.mfSetGridColumnValueList(this.uGridMaterialGroupList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar...
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.MaterialGroup), "MaterialGroup");
                QRPMAS.BL.MASMAT.MaterialGroup materialgroup = new QRPMAS.BL.MASMAT.MaterialGroup();
                brwChannel.mfCredentials(materialgroup);

                String strPlant = this.uComboSearchPlant.Value.ToString();

                DataTable dt = materialgroup.mfReadMASMaterialGroup(strPlant, m_resSys.GetString("SYS_LANG"));
                this.uGridMaterialGroupList.DataSource = dt;
                this.uGridMaterialGroupList.DataBind();

                WinGrid grd = new WinGrid();

                ////// 바인딩후 체크박스 상태를 모두 Uncheck로 만든다
                ////grd.mfSetAllUnCheckedGridColumn(this.uGridMaterialGroupList, 0, "Check");

                ////// RowSelector Clear
                ////grd.mfClearRowSeletorGrid(this.uGridMaterialGroupList);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {

        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {

        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            //처리 로직//
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            grd.mfDownLoadGridToExcel(this.uGridMaterialGroupList);
        }
        #endregion

        private void frmMAS0005_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
    }
}
