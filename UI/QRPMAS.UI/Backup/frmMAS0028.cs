﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMAS0028.cs                                         */
/* 프로그램명   : 설비정보                                              */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-01                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

using System.Collections;
using System.IO;

namespace QRPMAS.UI
{
    public partial class frmMAS0028 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        public frmMAS0028()
        {
            InitializeComponent();
        }

        private void frmMAS0028_Activated(object sender, EventArgs e)
        {
            //툴바활성
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //사용여부설정
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0028_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            titleArea.mfSetLabelText("설비정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            //각컨트롤 초기화
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitButton();
            InitGroupBox();
            //InitTree();

            uGroupBoxContentsArea.Expanded = false;

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
            
        }
        
        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();
                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                btn.mfSetButton(this.uButtonFileDownload, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
                WinLabel lbl = new WinLabel();
                //레이블마다 텍스트명,폰트,이미지,필수 설정
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelArea, "Area", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipLargeType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);


                lbl.mfSetLabel(this.uLabelPlant1, "공장", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelSuperEquip, "Super설비", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelArea1, "Area", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelStation1, "Station", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelLocation, "위치", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelEquipProcGubun, "설비공정구분", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelModel, "모델", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelSerialNum, "SerialNo", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelEquipType, "설비유형", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelEquipGroup, "설비그룹명", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelVendor, "Vendor", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelSTS, "STS입고일", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelMakerYear, "제작년도", m_resSys.GetString("SYS_FONTNAM"), true, false);
                lbl.mfSetLabel(this.uLabelEquipLevel, "설비등급", m_resSys.GetString("SYS_FONTNAM"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                #region 기본정보
                //기본설정
                //--정보
                grd.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                //--정보
                grd.mfSetGridColumn(this.uGridEquipList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "PMCancelFlag", "PM제외여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "MACHINETYPE", "설비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "DETAILMACHINETYPE", "세부설비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipNameCh", "설비명_중문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipNameEn", "설비명_영문", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "SuperEquipCode", "Super설비", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipGroupCode", "설비그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipGroupName", "설비그룹명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "VendorCode", "Maker", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "ModelName", "모델", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 20
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "StationName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunName", "공정분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "GRDate", "STS입고일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "MakeYear", "제작년도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipTypeName", "설비Type", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "EquipLevelName", "설비등급", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "VendorName", "Vendor", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "ONLINEBEHAVIOR", "On-Line적용여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 170, false, false, 10
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipList, 0, "자산코드", "자산코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                #region 첨부화일

                //--정보2
                grd.mfInitGeneralGrid(this.uGridFileList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true
                    , Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Default, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--정보2
                grd.mfSetGridColumn(this.uGridFileList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridFileList, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0");

                grd.mfSetGridColumn(this.uGridFileList, 0, "FileName", "파일명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 250, true, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridFileList, 0, "FileDesc", "설명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 500, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // 빈줄생성
                grd.mfAddRowGrid(this.uGridFileList, 0);



                //PM제외여부 헤더체크박스 비활성화
                this.uGridEquipList.DisplayLayout.Bands[0].Columns["PMCancelFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                #endregion

                #region History

                // InitGeneralGrid
                grd.mfInitGeneralGrid(this.uGridHistory, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // Column
                grd.mfSetGridColumn(this.uGridHistory, 0, "Gubun", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "GubunName", "구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "MeasureToolCode", "계측기코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                   Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "CompleteDate", "일자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "UserID", "검증인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "UserName", "검증인", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "Result", "검,교정결과", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "FileName", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridHistory, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #endregion

                //폰트설정
                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridFileList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridFileList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridHistory.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridHistory.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        ///// <summary>
        ///// 트리초기화
        ///// </summary>
        //private void InitTree()
        //{
        //    try
        //    {
        //        //System ResourceInfo
        //        ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //        WinTree tree = new WinTree();
        //        tree.mfInitTree(this.uTreeSystemInfo, Infragistics.Win.UltraWinTree.UltraTreeDisplayStyle.WindowsVista, Infragistics.Win.UltraWinTree.ViewStyle.FreeForm,
        //            Infragistics.Win.UltraWinTree.ScrollBounds.Default, Infragistics.Win.DefaultableBoolean.False, m_resSys.GetString("SYS_FONTNAME"));
        //        //Infragistics.Win.UltraWinTree.UltraTreeNode Plant= tree.mfAddNodeToTree(this.uTreeSystemInfo, "Plant", "공장","",Infragistics.Win.UltraWinTree.NodeStyle.Default,false);
        //    }
        //    catch
        //    { }
        //    finally
        //    { }
        //}
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                
                // Call Method
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue("A", m_resSys.GetString("SYS_LANG"))
                    , "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grp = new WinGroupBox();

                grp.mfSetGroupBox(this.uGroupBox1, GroupBoxType.LIST, "첨부파일", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grp.mfSetGroupBox(this.uGroupHistory, GroupBoxType.LIST, "검,교정이력", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                this.uGroupHistory.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGroupHistory.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;


                //폰트설정
                uGroupBox1.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBox1.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


#endregion

        #region 툴바관련

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //검색조건
                string strPlantCode = uComboPlant.Value.ToString();
                string strAreaCode = uComboArea.Value.ToString();
                string strStation = uComboStation.Value.ToString();
                string strEquipLoc = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboSearchProcess.Value.ToString();
                string strEquipLargeType = this.uComboSearchLargeType.Value.ToString();
                string strEquipGroup = this.uComboSearchEquipGroup.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                //ProgressBar 시작
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //////////////////////////////////////////////////////////처리로직/////////////////////////////////////////////////////////
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip equip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(equip);

                //매서드호출
                DataTable dtEquip = equip.mfReadEquip_Info(strPlantCode, strStation, strEquipLoc, strProcessGroup, strEquipLargeType, strEquipGroup, m_resSys.GetString("SYS_LANG"));

                //데이터바인드
                uGridEquipList.DataSource = dtEquip;
                uGridEquipList.DataBind();

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // RowSelector Clear
                //grd.mfClearRowSeletorGrid(this.uGrid1);

                //커서복원
                this.MdiParent.Cursor = Cursors.Default;

                //ProgressBar닫기
                m_ProgressPopup.mfCloseProgressPopup(this);

                //데이터가 없을시
                if (dtEquip.Rows.Count == 0)
                {
                    
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                }
                else
                {
                    grd.mfSetAutoResizeColWidth(this.uGridEquipList, 0);
                }
                // ContentGroupBox 접은상태로
                this.uGroupBoxContentsArea.Expanded = false;
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        
        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {


                if (this.uGridEquipList.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001024", "M001038",
                               Infragistics.Win.HAlign.Right);
                    return;
                }

                //PM제외여부체크된 설비정보를 담을 변수선언
                DataTable dtEquip = new DataTable();

                //컬럼설정
                dtEquip.Columns.Add("PlantCode", typeof(string));
                dtEquip.Columns.Add("EquipCode", typeof(string));
                dtEquip.Columns.Add("PMCancelFlag", typeof(string));

                //활성화된 셀을 맨처음으로 이동시킨다.
                if (this.uGridEquipList.Rows.Count > 0)
                    this.uGridEquipList.ActiveCell = this.uGridEquipList.Rows[0].Cells[0];

                //PM제외를 체크한 설비정보를 변수에 담는다.
                for (int i = 0; i < this.uGridEquipList.Rows.Count; i++)
                {
                    if (this.uGridEquipList.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        DataRow drEquip = dtEquip.NewRow();

                        drEquip["PlantCode"] = this.uGridEquipList.Rows[i].Cells["PlantCode"].Value;
                        drEquip["EquipCode"] = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value;
                        drEquip["PMCancelFlag"] = Convert.ToBoolean(this.uGridEquipList.Rows[i].Cells["PMCancelFlag"].Value) == true ? "T" : "F";

                        dtEquip.Rows.Add(drEquip);
                    }
                }

                int intFileNum = 0;
                for (int i = 0; i < this.uGridFileList.Rows.Count; i++)
                {
                    //삭제가 안된 행에서 경로가 있는 행이 있는지 체크
                    if (this.uGridFileList.Rows[i].Hidden == false && this.uGridFileList.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                        intFileNum++;
                }
                //첨부파일,PM제외여부 정보가 없을 시 리턴
                if (intFileNum == 0 && dtEquip.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001024", "M001038",
                                Infragistics.Win.HAlign.Right);
                    return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001264", "M001053", "M000936",
                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {

                    #region 화일정보저장
                    //설비화일 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipFile), "EquipFile");
                    QRPMAS.BL.MASEQU.EquipFile clsEquipFile = new QRPMAS.BL.MASEQU.EquipFile();
                    brwChannel.mfCredentials(clsEquipFile);
                    DataTable dtEquipFile = clsEquipFile.mfSetEquipFile();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uTextPlantCode.Text, "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uTextPlantCode.Text, "D0002");

                    //설비이미지 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    if (!intFileNum.Equals(0))
                    {
                        this.uGridFileList.ActiveCell = this.uGridFileList.Rows[0].Cells[0];

                        for (int i = 0; i < this.uGridFileList.Rows.Count; i++)
                        {
                            if (this.uGridFileList.Rows[i].Hidden == false)
                            {
                                if (this.uGridFileList.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\"))
                                {
                                    //화일이름변경(공장코드+설비점검그룹코드+화일명)하여 복사하기//
                                    FileInfo fileDoc = new FileInfo(this.uGridFileList.Rows[i].Cells["FileName"].Value.ToString());
                                    string strUploadFile = fileDoc.DirectoryName + "\\" +
                                                           uTextPlantCode.Text + "-" + this.uTextEquipCode.Text + "-" + this.uGridFileList.Rows[i].Cells["Seq"].Value.ToString() + "-" + fileDoc.Name;
                                    //변경한 화일이 있으면 삭제하기
                                    if (File.Exists(strUploadFile))
                                        File.Delete(strUploadFile);
                                    //변경한 화일이름으로 복사하기
                                    File.Copy(this.uGridFileList.Rows[i].Cells["FileName"].Value.ToString(), strUploadFile);
                                    arrFile.Add(strUploadFile);

                                    //BL에 넘겨줄 DataTable구성
                                    DataRow drEquipFile;
                                    drEquipFile = dtEquipFile.NewRow();
                                    drEquipFile["PlantCode"] = this.uTextPlantCode.Text;
                                    drEquipFile["EquipCode"] = this.uTextEquipCode.Text;
                                    drEquipFile["Seq"] = this.uGridFileList.Rows[i].Cells["Seq"].Value.ToString();
                                    drEquipFile["FileName"] = uTextPlantCode.Text + "-" + this.uTextEquipCode.Text + "-" + this.uGridFileList.Rows[i].Cells["Seq"].Value.ToString() + "-" + fileDoc.Name;
                                    drEquipFile["FileDesc"] = this.uGridFileList.Rows[i].Cells["FileDesc"].Value.ToString();
                                    dtEquipFile.Rows.Add(drEquipFile);
                                }
                                //경로가 없는 경우
                                else
                                {
                                    //BL에 넘겨줄 DataTable구성
                                    DataRow drEquipFile;
                                    drEquipFile = dtEquipFile.NewRow();
                                    drEquipFile["PlantCode"] = this.uTextPlantCode.Text;
                                    drEquipFile["EquipCode"] = this.uTextEquipCode.Text;
                                    drEquipFile["Seq"] = this.uGridFileList.Rows[i].Cells["Seq"].Value.ToString();
                                    drEquipFile["FileName"] = this.uGridFileList.Rows[i].Cells["FileName"].Value.ToString();
                                    drEquipFile["FileDesc"] = this.uGridFileList.Rows[i].Cells["FileDesc"].Value.ToString();
                                    dtEquipFile.Rows.Add(drEquipFile);
                                }
                            }
                        }
                    }
                    #endregion

                    //DB에 저장한다.
                    string strRtn = clsEquipFile.mfSaveEquipFIle(uTextPlantCode.Text, uTextEquipCode.Text, dtEquipFile, dtEquip,m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                    if (ErrRtn.ErrNum == 0)
                    {
                        if (!intFileNum.Equals(0))
                        {
                            //Upload정보 설정
                            fileAtt.mfInitSetSystemFileInfo(arrFile, "", dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                       dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                       dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                       dtSysAccess.Rows[0]["AccessPassword"].ToString());
                            fileAtt.ShowDialog();
                        }

                        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001135", "M001037", "M000930",
                                              Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        DialogResult result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001037", "M000953",
                                             Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        public void mfDelete()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }
        
        public void mfCreate()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipList.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000807", "M000805", Infragistics.Win.HAlign.Right);

                    return;
                }
                WinGrid grd = new WinGrid();
                //엑셀출력
                grd.mfDownLoadGridToExcel(this.uGridEquipList);


                if (this.uGridHistory.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded)
                {
                    grd.mfDownLoadGridToExcel(this.uGridHistory);
                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
            try
            {
            }
            catch
            { }
            finally
            { }
        }

        #endregion

        #region 이벤트

        //접거나 펼칠때 발생되는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {

            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 160);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipList.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipList.Height = 720;
                    for (int i = 0; i < uGridEquipList.Rows.Count; i++)
                    {
                        uGridEquipList.Rows[i].Fixed = false;
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region Combo

        //공장선택시 Area Station콤보 변경
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strPlantCode = uComboPlant.Value.ToString();

                string strPlant = this.uComboPlant.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboPlant.Items.Count; i++)
                {
                    if (strPlantCode.Equals(this.uComboPlant.Items[i].DataValue.ToString()) && strPlant.Equals(this.uComboPlant.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }
                if (!strChk.Equals(string.Empty))
                {
                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinComboEditor com = new WinComboEditor();

                    //리스트클리어
                    uComboArea.Items.Clear();

                    uComboStation.Items.Clear();

                    //Area
                    //bl호출
                    //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Area), "Area");
                    //QRPMAS.BL.MASEQU.Area clsArea = new QRPMAS.BL.MASEQU.Area();
                    //brwChannel.mfCredentials(clsArea);
                    ////매서드호출
                    //DataTable dtArea = clsArea.mfReadAreaCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    //clsArea.Dispose();
                    ////ComboBox에 추가
                    //com.mfSetComboEditor(this.uComboArea, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                    //    true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체", "AreaCode", "AreaName", dtArea);

                    //Station
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                    QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                    brwChannel.mfCredentials(clsStation);

                    //ComboBox에 추가
                    DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                    clsStation.Dispose();
                    com.mfSetComboEditor(this.uComboStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"),
                        true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", 
                        ComboDefaultValue("A", m_resSys.GetString("SYS_LANG")), "StationCode", "StationName", dtStation);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //Station이 변경시 위치,설비대분류,설비중분류,설비그룹이바뀜
        private void uComboStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.uComboSearchEquipLoc.Items.Clear();

                //설비위치콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                brwChannel.mfCredentials(clsEquipLocation);
                
                ResourceSet m_resSys= new ResourceSet(SysRes.SystemInfoRes);

                DataTable dtEquipLoc = clsEquipLocation.mfReadLocation_Combo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                clsEquipLocation.Dispose();
                WinComboEditor com = new WinComboEditor();

                com.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true, false, "",
                                      true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", 
                                      ComboDefaultValue("A", m_resSys.GetString("SYS_LANG")), "EquipLocCode", "EquipLocName", dtEquipLoc);


                mfSearchCombo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), "", "", "", 3);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //위치가 변경시 설비대분류,설비중분류,설비그룹이변경
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if(!this.uComboSearchEquipLoc.Value.ToString().Equals(string.Empty))
                    mfSearchCombo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(),this.uComboSearchProcess.Value.ToString(), "", 3);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비대분류가 변경시 설비중분류,설비그룹이변경
        private void uComboSearchProcess_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                if (!this.uComboSearchProcess.Value.ToString().Equals(string.Empty))
                {
                    mfSearchCombo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboSearchProcess.Value.ToString(), "", 2);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //설비중분류가 변경시 설비그룹이변경됨
        private void uComboSearchLargeType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if(!this.uComboSearchLargeType.Value.ToString().Equals(string.Empty))
                    mfSearchCombo(this.uComboPlant.Value.ToString(), this.uComboStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboSearchProcess.Value.ToString(), this.uComboSearchLargeType.Value.ToString(), 1);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Grid

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGridFileList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridFileList, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // CellChange Event 셀 수정시 RowSelector 이미지 변경
        private void uGridFileList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //첨부화일 선택
        private void uGridFileList_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "All files (*.*)|*.*";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strImageFile = openFile.FileName;
                    e.Cell.Value = strImageFile;

                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                    e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //그리드 셀 더블클릭시 발생
        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                
                if (e.Cell.Value.ToString() != "")
                {
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                    this.MdiParent.Cursor = Cursors.WaitCursor;
                    

                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    string strEquipCode = e.Cell.Row.Cells["EquipCode"].Value.ToString();

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    if (dtEquip.Rows.Count <= 0)
                    {
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);
                        return;
                    }
                    if (uGroupBoxContentsArea.Expanded == false)
                    {
                        this.uGroupBoxContentsArea.Expanded = true;

                    }

                    e.Cell.Row.Fixed = true;

                    uTextPlantCode.Text = strPlantCode;
                    uTextPlant.Text = e.Cell.Row.Cells["PlantName"].Value.ToString();
                    uTextEquipGroup.Text = dtEquip.Rows[0]["EquipGroupName"].ToString();
                    //uTextEquipLevel.Text = e.Cell.Row.Cells["EquipLevelName"].Value.ToString();
                    uTextEquipName.Text = dtEquip.Rows[0]["EquipName"].ToString();
                    uTextEquipProcGubun.Text = dtEquip.Rows[0]["EquipProcGubunName"].ToString();
                    uTextEquipCode.Text = strEquipCode;
                    uTextEquipType.Text = dtEquip.Rows[0]["EquipTypeName"].ToString();
                    uTextLocation.Text = dtEquip.Rows[0]["EquipLocName"].ToString();
                    uTextMakerYear.Text = dtEquip.Rows[0]["MakeYear"].ToString();
                    uTextModel.Text = dtEquip.Rows[0]["ModelName"].ToString();
                    uTextSerialNo.Text = dtEquip.Rows[0]["SerialNo"].ToString();
                    uTextStation.Text = dtEquip.Rows[0]["StationName"].ToString();
                    uTextSTS.Text = dtEquip.Rows[0]["GRDate"].ToString();
                    uTextSuperEquip.Text = dtEquip.Rows[0]["SuperEquipCode"].ToString();
                    uTextVendor.Text = dtEquip.Rows[0]["VendorName"].ToString();
                    uTextArea.Text = dtEquip.Rows[0]["AreaName"].ToString();


                    //설비화일 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipFile), "EquipFile");
                    QRPMAS.BL.MASEQU.EquipFile clsEquipFile = new QRPMAS.BL.MASEQU.EquipFile();
                    brwChannel.mfCredentials(clsEquipFile);
                    DataTable dtEquipFile = clsEquipFile.mfReadEquipFile(strPlantCode, strEquipCode);

                    //데이터바인드
                    uGridFileList.DataSource = dtEquipFile;
                    uGridFileList.DataBind();


                    //설비이력정보조회
                    DataTable dtEquipMoveHist = clsEquip.mfReadEquip_VerityMoveHist(strPlantCode, strEquipCode, m_resSys.GetString("SYS_LANG"));

                    this.uGridHistory.DataSource = dtEquipMoveHist;
                    this.uGridHistory.DataBind();

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridEquipList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void uGridEquipList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 첨부파일 더블 클릭 시 다운로드 시작
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridHistory_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (!e.Cell.Column.Key.Equals("FileName"))
                    return;

                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strFileName = e.Cell.Value.ToString();

                // 파일서버에서 불러올수 있는 파일인지 체크
                if (strFileName.Equals(string.Empty) || strFileName.Contains(":\\"))
                {
                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(this.uComboPlant.Value.ToString(), "S02");

                    string strFileFath = "";
                    if (e.Cell.Row.Cells["Gubun"].Value.ToString().Equals("1")) //내부교정 파일경로
                        strFileFath = "D0017";
                    else if (e.Cell.Row.Cells["Gubun"].Value.ToString().Equals("2")) //외부교정 파일경로
                        strFileFath = "D0018";

                    //첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(this.uComboPlant.Value.ToString(), strFileFath);

                    //첨부파일 Download하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();
                    arrFile.Add(strFileName);

                    //Download정보 설정
                    string strExePath = Application.ExecutablePath;
                    int intPos = strExePath.LastIndexOf(@"\");
                    strExePath = strExePath.Substring(0, intPos + 1);

                    fileAtt.mfInitSetSystemFileInfo(arrFile, strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\",
                                                           dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();

                    // 파일 실행시키기
                    System.Diagnostics.Process.Start(strExePath + dtFilePath.Rows[0]["FolderName"].ToString() + "\\" + strFileName);


                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region Button

        private void uButtonFileDownload_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                int intFileNum = 0;
                for (int i = 0; i < this.uGridFileList.Rows.Count; i++)
                {
                    //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                    if (this.uGridFileList.Rows[i].Hidden == false && this.uGridFileList.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(uTextPlantCode.Text, "S02");

                    //설비이미지 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(uTextPlantCode.Text, "D0002");

                    //설비이미지 Upload하기
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridFileList.Rows.Count; i++)
                    {
                        if (this.uGridFileList.Rows[i].Hidden == false && this.uGridFileList.Rows[i].Cells["FileName"].Value.ToString().Contains(":\\") == false)
                            arrFile.Add(this.uGridFileList.Rows[i].Cells["FileName"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //행삭제 버튼
        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                //첨부파일 그리드에 정보가 있을 경우 체크박스에 체크가 되어있는 행을 삭제를 한다.
                if (this.uGridFileList.Rows.Count > 0)
                {
                    for (int i = 0; i < this.uGridFileList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridFileList.Rows[i].Cells["Check"].Value) == true)
                        {
                            this.uGridFileList.Rows[i].Hidden = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        private void frmMAS0028_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmMAS0028_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion



        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</param>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                string strDefault = ComboDefaultValue("A", strLang);
                WinComboEditor wCombo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboSearchProcess.Items.Clear();
                    this.uComboSearchLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_LargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_GroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchLargeType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_LargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_GroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_GroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }
                clsEquip.Dispose();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">S : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun, string strLang)
        {
            try
            {
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                strLang = strLang.ToUpper();
                if (strGubun.Equals("S"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }
    }
}
