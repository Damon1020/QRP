﻿namespace QRPMAS.UI
{
    partial class frmMASZ0030
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0030));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridEquipWorker = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUesr = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLoc = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipWorker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchStation);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelUesr);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipLoc);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipLargeType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelEquipGroup);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelStation);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 80);
            this.uGroupBoxSearchArea.TabIndex = 1;
            // 
            // uGridEquipWorker
            // 
            this.uGridEquipWorker.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipWorker.DisplayLayout.Appearance = appearance5;
            this.uGridEquipWorker.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipWorker.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipWorker.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipWorker.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.uGridEquipWorker.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipWorker.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridEquipWorker.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipWorker.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipWorker.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipWorker.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridEquipWorker.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipWorker.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipWorker.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipWorker.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridEquipWorker.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipWorker.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipWorker.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridEquipWorker.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridEquipWorker.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipWorker.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipWorker.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridEquipWorker.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipWorker.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridEquipWorker.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipWorker.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipWorker.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipWorker.Location = new System.Drawing.Point(0, 120);
            this.uGridEquipWorker.Name = "uGridEquipWorker";
            this.uGridEquipWorker.Size = new System.Drawing.Size(1070, 700);
            this.uGridEquipWorker.TabIndex = 2;
            // 
            // uTextUserName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance2;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(776, 56);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextUserName.TabIndex = 21;
            // 
            // uTextUserID
            // 
            appearance3.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance3.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance3;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextUserID.ButtonsRight.Add(editorButton1);
            this.uTextUserID.Location = new System.Drawing.Point(672, 56);
            this.uTextUserID.MaxLength = 20;
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextUserID.TabIndex = 27;
            this.uTextUserID.ValueChanged += new System.EventHandler(this.uTextUserID_ValueChanged);
            this.uTextUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextUserID_KeyDown);
            this.uTextUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextUserID_EditorButtonClick);
            // 
            // uComboSearchEquipGroup
            // 
            this.uComboSearchEquipGroup.Location = new System.Drawing.Point(384, 56);
            this.uComboSearchEquipGroup.MaxLength = 50;
            this.uComboSearchEquipGroup.Name = "uComboSearchEquipGroup";
            this.uComboSearchEquipGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipGroup.TabIndex = 26;
            // 
            // uComboSearchEquipLargeType
            // 
            this.uComboSearchEquipLargeType.Location = new System.Drawing.Point(120, 56);
            this.uComboSearchEquipLargeType.MaxLength = 50;
            this.uComboSearchEquipLargeType.Name = "uComboSearchEquipLargeType";
            this.uComboSearchEquipLargeType.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLargeType.TabIndex = 25;
            this.uComboSearchEquipLargeType.ValueChanged += new System.EventHandler(this.uComboSearchEquType_ValueChanged);
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(672, 32);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboProcessGroup.TabIndex = 24;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uComboSearchEquipLoc
            // 
            this.uComboSearchEquipLoc.Location = new System.Drawing.Point(384, 32);
            this.uComboSearchEquipLoc.MaxLength = 50;
            this.uComboSearchEquipLoc.Name = "uComboSearchEquipLoc";
            this.uComboSearchEquipLoc.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchEquipLoc.TabIndex = 23;
            this.uComboSearchEquipLoc.ValueChanged += new System.EventHandler(this.uComboSearchEquipLoc_ValueChanged);
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(120, 8);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchPlant.TabIndex = 20;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uComboSearchStation
            // 
            this.uComboSearchStation.Location = new System.Drawing.Point(120, 32);
            this.uComboSearchStation.MaxLength = 50;
            this.uComboSearchStation.Name = "uComboSearchStation";
            this.uComboSearchStation.Size = new System.Drawing.Size(120, 21);
            this.uComboSearchStation.TabIndex = 22;
            this.uComboSearchStation.ValueChanged += new System.EventHandler(this.uComboSearchStation_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(568, 32);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 15;
            // 
            // uLabelUesr
            // 
            this.uLabelUesr.Location = new System.Drawing.Point(568, 56);
            this.uLabelUesr.Name = "uLabelUesr";
            this.uLabelUesr.Size = new System.Drawing.Size(100, 20);
            this.uLabelUesr.TabIndex = 14;
            // 
            // uLabelEquipLoc
            // 
            this.uLabelEquipLoc.Location = new System.Drawing.Point(280, 32);
            this.uLabelEquipLoc.Name = "uLabelEquipLoc";
            this.uLabelEquipLoc.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLoc.TabIndex = 13;
            // 
            // uLabelEquipLargeType
            // 
            this.uLabelEquipLargeType.Location = new System.Drawing.Point(16, 56);
            this.uLabelEquipLargeType.Name = "uLabelEquipLargeType";
            this.uLabelEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLargeType.TabIndex = 16;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(16, 8);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 19;
            // 
            // uLabelEquipGroup
            // 
            this.uLabelEquipGroup.Location = new System.Drawing.Point(280, 56);
            this.uLabelEquipGroup.Name = "uLabelEquipGroup";
            this.uLabelEquipGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGroup.TabIndex = 18;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(16, 32);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 17;
            // 
            // frmMASZ0030
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGridEquipWorker);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0030";
            this.Load += new System.EventHandler(this.frmMASZ0030_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0030_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMASZ0030_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipWorker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchStation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchEquipLoc;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchStation;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelUesr;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipWorker;
    }
}