﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통기준정보                                          */
/* 모듈(분류)명 : 공정관리 기준정보                                     */
/* 프로그램ID   : frmMAS0004.cs                                         */
/* 프로그램명   : 라인정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가참조
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMAS0004 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();
        int aa = 0;
        public frmMAS0004()
        {
            InitializeComponent();
        }

        // ToolBar
        private void frmMAS0004_Activated(object sender, EventArgs e)
        {
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0004_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("라인정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 함수 호출
            SetToolAuth();
            InitGroupBox();
            InitLabel();
            InitGrid();
            InitCombo();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
            this.titleArea.Focus();
        }

        #region 컨트롤초기화

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스 초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                WinGroupBox gb = new WinGroupBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 라인정보 GroupBox
                gb.mfSetGroupBox(this.uGroupBoxLineInfo, GroupBoxType.INFO, "라인정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                // 공정순서정보 GroupBox
                gb.mfSetGroupBox(this.uGroupBoxProcessOrder, GroupBoxType.LIST, "공정순서정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default
                    , Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBoxLineInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxLineInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxProcessOrder.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxProcessOrder.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                // ExtandableGroupBox 설정
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                // 그리드 일반설정
                // LineList Grid
                grd.mfInitGeneralGrid(this.uGridLineList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // LineProc Grid
                grd.mfInitGeneralGrid(this.uGridLineProcList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼설정
                // LineList Grid
                grd.mfSetGridColumn(this.uGridLineList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLineList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineList, 0, "LineCode", "라인코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineList, 0, "LineName", "라인명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineList, 0, "LineNameCh", "라인명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineList, 0, "LineNameEn", "라인명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineList, 0, "LineDesc", "라인설명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", "");

                // LineProc Grid
                ////grd.mfSetGridColumn(this.uGridLineProcList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                ////    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox
                ////    , "", "", "false");

                grd.mfSetGridColumn(this.uGridLineProcList, 0, "ProcessSeq", "공정순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 50, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineProcList, 0, "ProcessCode", "공정", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineProcList, 0, "ProcessName", "공정명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineProcList, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                grd.mfSetGridColumn(this.uGridLineProcList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", "");

                //폰트설정
                uGridLineList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridLineList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                uGridLineProcList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridLineProcList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드에 DropDown 추가
                // LineList Grid
                // 사용여부
                ////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                ////QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                ////brwChannel.mfCredentials(clsCommonCode);

                ////DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                ////grd.mfSetGridColumnValueList(uGridLineList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);
                ////grd.mfSetGridColumnValueList(uGridLineProcList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                ////// LineList Grid 컬럼에 공장 DropDown 설정
                //////////
                
                ////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                ////QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                ////brwChannel.mfCredentials(clsPlant);
                ////// Plant정보를 얻어오는 함수를 호출하여 DataTable에 저장
                ////DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                ////// 그리드에 DropDown 만드는 함수 호출
                ////grd.mfSetGridColumnValueList(uGridLineList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                WinLabel lb = new WinLabel();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 라인정보 GroupBox 내 Label
                lb.mfSetLabel(uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(uLabelLineCode, "라인코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(uLabelLineName, "라인명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(uLabelLineNameCh, "라인명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(uLabelLineNameEn, "라인명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(uLabelLineDesc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lb.mfSetLabel(uLabelUseFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, false);

                // SearchArea GroupBox 내 Label
                lb.mfSetLabel(uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitCombo()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor combo = new WinComboEditor();

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // 함수호출
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 조회용 PlantComboBox
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName"
                    , dtPlant);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 이벤트
        ////// 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        ////private void uGridProcessOrderList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        ////{
        ////    try
        ////    {
        ////        QRPGlobal grdImg = new QRPGlobal();
        ////        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

        ////        QRPCOM.QRPUI.WinGrid grd = new WinGrid();
        ////        if (grd.mfCheckCellDataInRow(this.uGridLineProcList, 0, e.Cell.Row.Index))
        ////            e.Cell.Row.Delete(false);
        ////    }
        ////    catch
        ////    {
        ////    }
        ////    finally
        ////    {
        ////    }
        ////}

        // ExpandableGroupBox 펼침상태
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {                    
                    this.uGridLineList.Height = 45;
                    //Point point = new Point(0, 130);
                    Point point = new Point(0, this.uGridLineList.Top + this.uGridLineList.Height);
                    this.uGroupBoxContentsArea.Location = point;
                }
                else
                {                    
                    this.uGridLineList.Height = 760;
                    //Point point = new Point(0, 825);
                    Point point = new Point(0, this.uGridLineList.Top + this.uGridLineList.Height);
                    this.uGroupBoxContentsArea.Location = point;

                    for (int i = 0; i < uGridLineList.Rows.Count; i++)
                    {
                        uGridLineList.Rows[i].Fixed = false;
                    }
                    this.titleArea.Focus();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그리드 더블클릭시
        private void uGridLineList_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Row.Fixed = true;
                }
                
                // 라인정보 그룹박스에 그리드의 정보를 나타내기 위한 구문
                this.uTextPlant.Text = e.Row.Cells["PlantName"].Value.ToString();
                this.uTextLineCode.Text = e.Row.Cells["LineCode"].Value.ToString();
                this.uTextLineName.Text = e.Row.Cells["LineName"].Value.ToString();
                this.uTextLineNameCh.Text = e.Row.Cells["LineNameCh"].Value.ToString();
                this.uTextLineNameEn.Text = e.Row.Cells["LineNameEn"].Value.ToString();
                this.uTextLineDesc.Text = e.Row.Cells["LineDesc"].Value.ToString();
                this.uTextUseFlag.Text = e.Row.Cells["UseFlag"].Value.ToString();

                String strPlantCode = e.Row.Cells["PlantCode"].Value.ToString();
                String strLineCode = e.Row.Cells["LineCode"].Value.ToString();

                // ListProc Grid 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.LineProc), "LineProc");
                QRPMAS.BL.MASPRC.LineProc clsLineProc = new QRPMAS.BL.MASPRC.LineProc();
                brwChannel.mfCredentials(clsLineProc);

                DataTable dtLineProc = clsLineProc.mfReadMASLineProc(strPlantCode, strLineCode, m_resSys.GetString("SYS_LANG"));

                this.uGridLineProcList.DataSource = dtLineProc;
                this.uGridLineProcList.DataBind();
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 조회를 위한 Plant/Routing 변수
                string strSearchPlantCode = this.uComboSearchPlant.Value.ToString();

                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 호출 //
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Line), "Line");
                QRPMAS.BL.MASPRC.Line line = new QRPMAS.BL.MASPRC.Line();
                brwChannel.mfCredentials(line);
                DataTable dt = new DataTable();

                // 함수호출 //
                dt = line.mfReadMASLine(strSearchPlantCode, m_resSys.GetString("SYS_LANG"));
                this.uGridLineList.DataSource = dt;
                this.uGridLineList.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridLineList, 0);
                }

                // ContentGroupBox 접은상태로
                this.uGroupBoxContentsArea.Expanded = false;
                this.titleArea.Focus();
            }
            catch (Exception ex)
            {
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            grd.mfDownLoadGridToExcel(this.uGridLineList);
        }
        #endregion

        private void frmMAS0004_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
    }
}
