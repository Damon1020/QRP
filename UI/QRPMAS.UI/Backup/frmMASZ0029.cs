﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 치공구관리기준정보                                    */
/* 프로그램ID   : frmMASZ0029.cs                                        */
/* 프로그램명   : 정보보기                                  */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-12-01                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//API참조를 위해 추가
using System.Runtime.InteropServices;
using System.Resources;
using QRPCOM.QRPGLO;


namespace QRPMAS.UI
{
    public partial class frmMASZ0029 : Form
    {
        //리소스호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        private string m_strServer = "\\12.230.49.131\\비품 도면 번호 폴더\\STS 도면 관리";
        private string m_strSharedServer = @"\\12.230.49.131\비품 도면 번호 폴더\STS 도면 관리";
        //private string m_strAccessID = "";
        //private string m_strAccessPwd = "";

        //구조체 선언
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct NETRESOURCE
        {
            public uint dwScope;
            public uint dwType;
            public uint dwDisplayType;
            public uint dwUsage;
            public string lpLocalName;
            public string lpRemoteName;
            public string lpComment;
            public string lpProvider;
        };

        //API함수 선언(공유연결)
        [DllImport("mpr.dll", CharSet = CharSet.Auto)]
        public static extern int WNetUseConnection(
            IntPtr hwndOwner,
            [MarshalAs(UnmanagedType.Struct)] ref NETRESOURCE lpNetResource,
            string lpPassword,
            string lpUserID,
            uint dwFlags,
            StringBuilder lpAccessName,
            ref int lpBufferSize,
            out uint lpResult
            );

        //API함수 선언(공유해제)
        [DllImport("mpr.dll", EntryPoint = "WNetCancelConnection2", CharSet = CharSet.Auto)]
        public static extern int WNetCancelConnection2A(
            string lpName,
            int dwFlags,
            int fForce
            );

        public frmMASZ0029()
        {
            InitializeComponent();
        }

        private void frmMASZ0029_Load(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 타이틀 Text 설정함수 호출
                this.titleArea.mfSetLabelText("치공구도면관리", m_resSys.GetString("SYS_FONTNAME"), 12);

                InitView();
                //int aa = ConnectRemoteServer(m_strServer, m_strAccessID, m_strAccessPwd);

                //System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(@"\\10.60.24.173\공유폴더");
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(m_strSharedServer);
                foreach (System.IO.DirectoryInfo g in dir.GetDirectories())
                {
                    System.IO.DirectoryInfo directoryInfo = g;
                    Infragistics.Win.UltraWinListView.UltraListViewItem item = this.uListViewFolder.Items.Add(directoryInfo.FullName, directoryInfo.Name);
                    item.SubItems["FolderType"].Value = "File Folder";
                    item.SubItems["DateModified"].Value = directoryInfo.LastWriteTime;
                    item.Appearance = this.uListViewFolder.Appearances["folder"];
                }
                
                //CancelRemoteServer(m_strServer);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitView()
        {
            try
            {
                uListViewFolder.View = Infragistics.Win.UltraWinListView.UltraListViewStyle.Details;
                uListViewFolder.MainColumn.Text = "이름";

                //Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn colFileName = new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn();
                //colFileName.Key = "FolderName";
                //colFileName.Text = "이름";
                //colFileName.Width = 150;
                //this.uListViewFolder.SubItemColumns.Add(colFileName);

                Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn colFileType = new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn();
                colFileType.Key = "FolderType";
                colFileType.Text = "유형";
                colFileType.Width = 100;
                this.uListViewFolder.SubItemColumns.Add(colFileType);

                Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn colDateModified = new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn();
                colDateModified.Key = "DateModified";
                colDateModified.Text = "수정한 날짜";
                colDateModified.Width = 200;
                this.uListViewFolder.SubItemColumns.Add(colDateModified);

                Infragistics.Win.Appearance appearance = this.uListViewFolder.Appearances.Add("folder");
                appearance.Image = Properties.Resources.tree_nodeclose;

                
            }
            catch (System.Exception ex)
            {
            	QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 원격서버 공유폴더에 연결하기
        /// </summary>
        /// <param name="strServer"></param>
        /// <param name="strUserID"></param>
        /// <param name="strPassword"></param>
        /// <returns></returns>
        public int ConnectRemoteServer(string strServer, string strUserID, string strPassword)
        {
            int intResult = 0;
            try
            {
                int intCapacity = 64;
                uint intResultFlags = 0;
                uint intFlags = 0;
                System.Text.StringBuilder sb = new System.Text.StringBuilder(intCapacity);
                NETRESOURCE ns = new NETRESOURCE();
                ns.dwType = 1;         //공유 디스트
                ns.lpLocalName = null; //로컬 드라이브 지정하지 않음
                ns.lpRemoteName = strServer;
                ns.lpProvider = null;
                intResult = WNetUseConnection(IntPtr.Zero, ref ns, strPassword, strUserID, intFlags, sb, ref intCapacity, out intResultFlags);

                return intResult;
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return intResult;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 원격서버 공유폴더 연결 끊기
        /// </summary>
        /// <param name="strServer"></param>
        public void CancelRemoteServer(string strServer)
        {
            try
            {
                WNetCancelConnection2A(strServer, 1, 0);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uListViewFolder_ItemDoubleClick(object sender, Infragistics.Win.UltraWinListView.ItemDoubleClickEventArgs e)
        {
            try
            {
                int aa = ConnectRemoteServer("\\10.60.24.173\\공유폴더", "kchryu", "1");

                string strFolder = this.uListViewFolder.SelectedItems[0].Key;
                System.Diagnostics.Process.Start("explorer", strFolder);

                CancelRemoteServer("\\10.60.24.173\\공유폴더");
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
    }
}
