﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0020.cs                                         */
/* 프로그램명   : 계측기분류정보등록                                    */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-22                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//	추가참조
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;


namespace QRPMAS.UI
{
   
    public partial class frmMAS0020 : Form, IToolbar
    {
        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";

        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0020()
        {
            InitializeComponent();
        }
        private void frmMAS0020_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //검색,저장,삭제,새로만들기,프린트,엑셀 사용여부
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
            
        }

        private void frmMAS0020_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // Title Text 설정
            titleArea.mfSetLabelText("계측기분류정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);
            
            // 초기화 Method 호출
//            SetRunMode();
            SetToolAuth();
            InitGrid();
            InitLabel();
            InitComboBox();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화 Method

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        MessageBox.Show(this.Tag.ToString());
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();


                // BL 공장호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue("A",m_resSys.GetString("SYS_LANG"))
                    , "PlantCode", "PlantName", dtPlant);


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화
        /// </summary>
        public void InitGrid()
        {
            
            try 
            { 
                WinGrid gr = new WinGrid();
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //그리드일반설정 
                gr.mfInitGeneralGrid(this.uGridMttypeGroupList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom ,0, false, m_resSys.GetString("SYS_FONTNAME"));

            // 그리드 컬럼 추가 //
                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox
                    , "", "", "false");

                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", m_resSys.GetString("SYS_PLANTCODE"));


                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "MTLTypeCode", "계측기대분류코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", "");

                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "MTMTypeCode", "계측기중분류코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", "");

                
                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "MTTypeCode", "계측기분류코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "##", "");

                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "MTTypeName", "계측기분류명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "MTTypeNameCh", "계측기분류명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "MTTypeNameEn", "계측기분류명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit
                    , "", "", "");

                // 20111010 _ 검교정주기 , 단위 추가  begin///    
                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "InspectPeriod", "검교정주기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Bottom, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Integer
                    , "", "", "0");

                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "InspectPeriodUnitCode", "검교정단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", ComboDefaultValue("S", m_resSys.GetString("SYS_LANG")));

                gr.mfSetGridColumn(this.uGridMttypeGroupList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown
                    , "", "", "T");
                //폰트설정
                uGridMttypeGroupList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridMttypeGroupList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 그리드에 DropDown 추가
                // 그리드 컬럼에 공장 콤보박스 추가
                DataTable PlantList = new DataTable();

                // BL 공장호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Plant정보를 얻어오는 함수를 호출하여 DataTable에 저장
                PlantList = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                gr.mfSetGridColumnValueList(uGridMttypeGroupList, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", ComboDefaultValue("S",m_resSys.GetString("SYS_LANG")), PlantList);


                
                // 계측기 대분류 코드
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUComCode);

                DataTable dtMTLTypeCode = clsUComCode.mfReadUserCommonCode("QUA", "U0009", m_resSys.GetString("SYS_LANG"));
                gr.mfSetGridColumnValueList(this.uGridMttypeGroupList, 0, "MTLTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", ComboDefaultValue("S", m_resSys.GetString("SYS_LANG")), dtMTLTypeCode);

                // 계측기 중분류 코드                
                DataTable dtMTMTypeCode = clsUComCode.mfReadUserCommonCode("QUA", "U0010", m_resSys.GetString("SYS_LANG"));
                gr.mfSetGridColumnValueList(this.uGridMttypeGroupList, 0, "MTMTypeCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", ComboDefaultValue("S", m_resSys.GetString("SYS_LANG")), dtMTMTypeCode);

                // 사용여부
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                gr.mfSetGridColumnValueList(this.uGridMttypeGroupList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                // 검교정단위

                DataTable dtInspectPeriodUnitCode = clsComCode.mfReadCommonCode("C0003", m_resSys.GetString("SYS_LANG"));

                gr.mfSetGridColumnValueList(this.uGridMttypeGroupList, 0, "InspectPeriodUnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtInspectPeriodUnitCode);


                // 공백줄 추가
                gr.mfAddRowGrid(this.uGridMttypeGroupList, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            { 
            }
        }

        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">S : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun,string strLang)
        {
            try
            {
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                strLang = strLang.ToUpper();
                if (strGubun.Equals("S"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        #endregion

        #region Toolbar...
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MTType), "MTType");
                QRPMAS.BL.MASQUA.MTType mttype = new QRPMAS.BL.MASQUA.MTType();
                brwChannel.mfCredentials(mttype);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                DataTable dt = mttype.mfReadMASMTType(strPlantCode);
                this.uGridMttypeGroupList.DataSource = dt;
                this.uGridMttypeGroupList.DataBind();

                // 디비로 가져온 정보의 PK 편집 불가 상태로
                for (int i = 0; i < this.uGridMttypeGroupList.Rows.Count; i++)
                {
                    this.uGridMttypeGroupList.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridMttypeGroupList.Rows[i].Cells["MTLTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridMttypeGroupList.Rows[i].Cells["MTMTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                    this.uGridMttypeGroupList.Rows[i].Cells["PlantCOde"].Appearance.BackColor = Color.Gainsboro;
                    this.uGridMttypeGroupList.Rows[i].Cells["MTLTypeCode"].Appearance.BackColor = Color.Gainsboro;
                    this.uGridMttypeGroupList.Rows[i].Cells["MTMTypeCode"].Appearance.BackColor = Color.Gainsboro;
                    this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Appearance.BackColor = Color.Gainsboro;
                    //계측기 코드값중 뒤에서 2자리만 표현해준다
                    if (this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString() != "")
                    {
                        //3자리 이하인 경우도 있어서 예외경우 추가
                        if (this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString().Length < 3)
                            this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString();
                        else
                            this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString().Substring(3, 2);
                    }
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridMttypeGroupList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        ///  저장 
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                // BL 호출
                QRPMAS.BL.MASQUA.MTType mttype;
                ////if (m_bolDebugMode == false)
                /////{
                    QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MTType), "MTType");
                    mttype = new QRPMAS.BL.MASQUA.MTType();
                    brwChannel.mfCredentials(mttype);
                ///}
                ////////else
                ////////    mttype = new QRPMAS.BL.MASQUA.MTType(m_strDBConn);

                // 저장 함수호출 매개변수 DataTable
                DataTable dtMTType = mttype.mfSetMASMTType();

                DialogResult DResult = new DialogResult();

                if(this.uGridMttypeGroupList.Rows.Count > 0)
                    this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");
                
                for (int i = 0; i < this.uGridMttypeGroupList.Rows.Count; i++)
                {
                    //그리드가 수정되었을때 저장
                    if (this.uGridMttypeGroupList.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        //필수 입력사항 확인
                        if (this.uGridMttypeGroupList.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGridMttypeGroupList.Rows[i].RowSelectorNumber.ToString() + msg.GetMessge_Text("M000560",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["PlantCode"];
                            this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGridMttypeGroupList.Rows[i].RowSelectorNumber.ToString() + msg.GetMessge_Text("M000559",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"];
                            this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGridMttypeGroupList.Rows[i].Cells["MTTypeName"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGridMttypeGroupList.Rows[i].RowSelectorNumber.ToString() + msg.GetMessge_Text("M000558",strLang)
                                , Infragistics.Win.HAlign.Center);

                            this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeName"];
                            this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGridMttypeGroupList.Rows[i].Cells["InspectPeriod"].Value.ToString() == "0")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGridMttypeGroupList.Rows[i].RowSelectorNumber.ToString() + msg.GetMessge_Text("M000557",strLang)
                                , Infragistics.Win.HAlign.Center);

                            this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["InspectPeriod"];
                            this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGridMttypeGroupList.Rows[i].Cells["InspectPeriodUnitCode"].Value.ToString().Equals(string.Empty))
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGridMttypeGroupList.Rows[i].RowSelectorNumber.ToString() + msg.GetMessge_Text("M000556",strLang)
                                , Infragistics.Win.HAlign.Center);

                            this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["InspectPeriodUnitCode"];
                            this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        ////else if (this.uGridMttypeGroupList.Rows[i].Cells["MTTypeNameCh"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                        ////        , (i + 1) + "번째 열의 계측기분류명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeNameCh"];
                        ////    this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}
                        ////else if (this.uGridMttypeGroupList.Rows[i].Cells["MTTypeNameEn"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                        ////        , (i + 1) + "번째의 열의 계측기분류명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeNameEn"];
                        ////    this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}
                        else
                        {
                            DataRow drMTType = dtMTType.NewRow();
                            
                            
                            drMTType["PlantCode"] = this.uGridMttypeGroupList.Rows[i].Cells["PlantCode"].Value.ToString();
                            drMTType["MTLTypeCode"] = this.uGridMttypeGroupList.Rows[i].Cells["MTLTypeCode"].Value.ToString();
                            drMTType["MTMTypeCode"] = this.uGridMttypeGroupList.Rows[i].Cells["MTMTypeCode"].Value.ToString();
                            // 계측기 분류코드 5자리로 만들어준다
                            if (this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString().Length == 5)
                            {
                                drMTType["MTTypeCode"] = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString();
                            }
                            else
                            {
                                drMTType["MTTypeCode"] = string.Concat(this.uGridMttypeGroupList.Rows[i].Cells["MTMTypeCode"].Value.ToString(), this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString());
                            }
                            drMTType["MTTypeName"] = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeName"].Value.ToString();
                            drMTType["MTTypeNameCh"] = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeNameCh"].Value.ToString();
                            drMTType["MTTypeNameEn"] = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeNameEn"].Value.ToString();
                            drMTType["InspectPeriod"] = this.uGridMttypeGroupList.Rows[i].Cells["InspectPeriod"].Value.ToString();
                            drMTType["InspectPeriodUnitCode"] = this.uGridMttypeGroupList.Rows[i].Cells["InspectPeriodUnitCode"].Value.ToString();
                            drMTType["UseFlag"] = this.uGridMttypeGroupList.Rows[i].Cells["UseFlag"].Value.ToString();
                            dtMTType.Rows.Add(drMTType);
                        }
                    }
                }
                if (dtMTType.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //저장함수 호출
                        String strMMtype = mttype.mfSaveMASMTType(dtMTType, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        //Decoding
                        TransErrRtn ErrEtn = new TransErrRtn();
                        ErrEtn = ErrEtn.mfDecodingErrMessage(strMMtype);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        //처리결과에 따른 메세지 박스
                        if (ErrEtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001135", "M001037", "M000930"
                                , Infragistics.Win.HAlign.Right);

                        }
                        else
                        {
                            string strMes = "";
                            if (ErrEtn.ErrMessage.Equals(string.Empty))
                                strMes = msg.GetMessge_Text("M000953", strLang);
                            else
                                strMes = ErrEtn.ErrMessage;

                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMes
                                , Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 삭제 
        /// </summary>
        public void mfDelete()
        {
            try
            {
               //SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.MTType), "MTType");
                QRPMAS.BL.MASQUA.MTType mttype = new QRPMAS.BL.MASQUA.MTType();
                brwChannel.mfCredentials(mttype);
                
                //함수호출 매개변수 DataTable
                DataTable dtMTType = mttype.mfSetMASMTType();

                DialogResult DResult = new DialogResult();

                // 활성셀을 첫번째줄 첫번째 셀로 이동
                if(this.uGridMttypeGroupList.Rows.Count > 0)
                    this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");

                //chek된것 삭제
                for (int i = 0; i < uGridMttypeGroupList.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridMttypeGroupList.Rows[i].Cells["Check"].Value) == true)
                    {
                        //필수입력사항 확인
                        if (this.uGridMttypeGroupList.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGridMttypeGroupList.Rows[i].RowSelectorNumber.ToString() + msg.GetMessge_Text("M000560",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["PlantCode"];
                            this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , this.uGridMttypeGroupList.Rows[i].RowSelectorNumber.ToString() + msg.GetMessge_Text("M000559",strLang)
                                , Infragistics.Win.HAlign.Center);

                            //FocusCell
                            this.uGridMttypeGroupList.ActiveCell = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"];
                            this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            DataRow drMTType = dtMTType.NewRow();
                            drMTType["PlantCode"] = this.uGridMttypeGroupList.Rows[i].Cells["PlantCode"].Value.ToString();
                            //drMTType["MTTypeCode"] = this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString();
                            drMTType["MTTypeCode"] = string.Concat(this.uGridMttypeGroupList.Rows[i].Cells["MTMTypeCode"].Value.ToString(), this.uGridMttypeGroupList.Rows[i].Cells["MTTypeCode"].Value.ToString());
                            dtMTType.Rows.Add(drMTType);
                        }
                    }
                }

                if(dtMTType.Rows.Count>0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //함수호출
                        string strSG = mttype.mfDeleteMASMTType(dtMTType);

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strSG);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        //삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001135", "M000638", "M000677",
                                 Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            string strMes = "";
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                                strMes = msg.GetMessge_Text("M000923", strLang);
                            else
                                strMes = ErrRtn.ErrMessage;

                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M000638",strLang), strMes,
                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {

        }
        /// <summary>
        /// 프린트
        /// </summary>
        public void mfPrint()
        {

        }
        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            //처리로직
            WinGrid grd = new WinGrid();
            //엑셀저장함수호출
            grd.mfDownLoadGridToExcel(this.uGridMttypeGroupList);
        }
        #endregion

        #region Event...
        /// <summary>
        /// 셀 수정시 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridMttypeGroupList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();


                // 행이 빈칸이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridMttypeGroupList, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                // 계측기대분류코드 DropDown 설정
                if (e.Cell.Column.Key == "PlantCode")
                {
                    String strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    //String strMTLTypeCode = e.Cell.Row.Cells["MTLTypeCode"].Value.ToString();


                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                    QRPSYS.BL.SYSPGM.UserCommonCode clsUComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                    brwChannel.mfCredentials(clsUComCode);

                    DataTable dtMTMTypeCode = clsUComCode.mfReadUserCommonCode("QUA", "U0009", m_resSys.GetString("SYS_LANG"));
                    grd.mfSetGridCellValueList(this.uGridMttypeGroupList, e.Cell.Row.Index, "MTMTypeCode", "", "선택", dtMTMTypeCode);

                    e.Cell.Row.Cells["MLMTypeCode"].Value = "";

                }
                
                // 계측기중분류코드 DropDown 설정
                if (e.Cell.Column.Key == "MTLTypeCode")
                {
                    // 변수

                    String strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    String strMTLTypeCode = e.Cell.Row.Cells["MTLTypeCode"].Value.ToString();


                    // BL 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                    QRPSYS.BL.SYSPGM.UserCommonCode clsUComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                    brwChannel.mfCredentials(clsUComCode);

                    DataTable dtMTMTypeCode = clsUComCode.mfReadUserCommonCodeMMType("QUA", "U0010",strMTLTypeCode, m_resSys.GetString("SYS_LANG"));
                    grd.mfSetGridCellValueList(this.uGridMttypeGroupList, e.Cell.Row.Index, "MTMTypeCode","", "선택", dtMTMTypeCode);

                    e.Cell.Row.Cells["MTMTypeCode"].Value = "";
                    

                }
 
     
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }




        // CellChange Event
        private void uGridMttypeGroupList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀변경시 RowSelector 이미지 변경
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void frmMAS0020_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        // 공장 선택 안됐을때 상세 그리드의 드랍다운 리스트를 누르면 공장선택하라고 메세지 박스 띄우는 이벤트
        private void uGridMttypeGroupList_BeforeCellListDropDown(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");
                // 대분류 선택시 공장 선택 여부확인
                if (e.Cell.Column.Key == "MTLTypeCode")
                {
                    if (e.Cell.Row.Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000261",strLang)
                            , e.Cell.Row.Index + msg.GetMessge_Text("M000560",strLang)
                            , Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uGridMttypeGroupList.ActiveCell = e.Cell.Row.Cells["PlantCode"];
                        this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                }
                // 중분류 선택시 공장/대분류 선택 여부 확인
                else if (e.Cell.Column.Key == "MTMTypeCode")
                {
                    if (e.Cell.Row.Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000261",strLang)
                            , e.Cell.Row.Index + msg.GetMessge_Text("M000560",strLang)
                            , Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uGridMttypeGroupList.ActiveCell = e.Cell.Row.Cells["PlantCode"];
                        this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                    else if (e.Cell.Row.Cells["MTLTypeCode"].Value.ToString().Equals(string.Empty))
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000369",strLang)
                            , e.Cell.Row.Index + msg.GetMessge_Text("M000565",strLang)
                            , Infragistics.Win.HAlign.Right);

                        this.uGridMttypeGroupList.ActiveCell = e.Cell.Row.Cells["MTLTypeCode"];
                        this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                }
               
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridMttypeGroupList_BeforeCellUpdate(object sender, Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult Result = new DialogResult();
                string strLang = m_resSys.GetString("SYS_LANG");

                if (e.Cell.Column.Key == "MTTypeCode")
                {
                    
                    if (e.Cell.Row.Cells["PlantCode"].Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000261",strLang)
                            , e.Cell.Row.Index + msg.GetMessge_Text("M000560",strLang)
                            , Infragistics.Win.HAlign.Right);

                        // Focus
                        this.uGridMttypeGroupList.ActiveCell = e.Cell.Row.Cells["PlantCode"];
                        this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                    else if (e.Cell.Row.Cells["MTLTypeCode"].Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M000369",strLang)
                            , e.Cell.Row.Index + msg.GetMessge_Text("M000565",strLang)
                            , Infragistics.Win.HAlign.Right);

                        
                        this.uGridMttypeGroupList.ActiveCell = e.Cell.Row.Cells["MTLTypeCode"];
                        this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);

                    }
                    else if (e.Cell.Row.Cells["MTMTypeCode"].Value.ToString() == "")
                    {
                        Result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001123",strLang)
                            , e.Cell.Row.Index + msg.GetMessge_Text("M000571",strLang)
                            , Infragistics.Win.HAlign.Right);
                        
                        this.uGridMttypeGroupList.ActiveCell = e.Cell.Row.Cells["MTMTypeCode"];
                        this.uGridMttypeGroupList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        
    }
}
