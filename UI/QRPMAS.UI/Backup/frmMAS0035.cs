﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMAS0035.cs                                         */
/* 프로그램명   : SparePart창고정보등록                                 */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0035 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0035()
        {
            InitializeComponent();
        }
        private void frmMAS0035_Activated(object sender, EventArgs e)
        {
            //툴바활성
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //사용여부설정
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0035_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            titleArea.mfSetLabelText("SparePart창고정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤 초기화
            SetToolAuth();
            InitComboBox();
            InitGrid();
            InitLabel();
            InitButton();

            // ContentGroupBox 닫힘상태로
            this.uGroupBoxContentsArea.Expanded = false;
            this.uGroupBoxContentsArea.Visible = false;
        }



        #region 컨트롤 초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //레이블초기화
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //그리드초기화
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //SparePart창고정보
                //--기본설정
                grd.mfInitGeneralGrid(this.uGrid, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGrid, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGrid, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                grd.mfSetGridColumn(this.uGrid, 0, "SPInventoryCode", "창고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid, 0, "SPInventoryName", "창고명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid, 0, "SPInventoryNameCh", "창고명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid, 0, "SPInventoryNameEn", "창고명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGrid, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //--폰트설정
                this.uGrid.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;


                // 그리드에 DropDown 추가
                // 그리드 컬럼에 공장 콤보박스 추가


                // BL 공장호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                // Plant정보를 얻어오는 함수를 호출하여 DataTable에 저장
                DataTable dtPlantList = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(uGrid, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", ComboDefaultValue_Choice(), dtPlantList);

                // 1.브라우저 객체 생성 -> 2.채널등록 -> 3.공통코드객체 선언 및 생성 -> 4.권한설정 -> 5.데이터테이블 형태로 반환받아 옴. -> 6. 그리드 컬럼에 매칭

                // QRPBrowser brwChannel = new QRPBrowser(); // 1
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode"); // 2
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode(); // 3
                brwChannel.mfCredentials(clsComCode); // 4

                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG")); // 5

                //그리드 컬럼 전체에 적용하여 넣기(mfSetGridColumnValueList)
                grd.mfSetGridColumnValueList(this.uGrid, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag); 

                //--한줄생성
                grd.mfAddRowGrid(this.uGrid, 0);

                //SparePart창고상세정보
                //--기본설정
                grd.mfInitGeneralGrid(this.uGridSection, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGridSection, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSection, 0, "SectionCode", "Section코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSection, 0, "SectionName", "Section명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSection, 0, "SectionNameCh", "Section명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSection, 0, "SectionNameEn", "Section명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSection, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //--폰트설정
                this.uGridSection.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridSection.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //--사용여부
                ////DataTable dtUseFlag1 = new DataTable();
                ////DataRow drUse1;

                //////--컬럼생성
                ////dtUseFlag1.Columns.Add("Key");
                ////dtUseFlag1.Columns.Add("Value");

                //////--컬럼에값넣기
                ////drUse1 = dtUseFlag1.NewRow();
                ////drUse1["Key"] = "T";
                ////drUse1["Value"] = "사용함";
                ////dtUseFlag1.Rows.Add(drUse1);

                ////drUse1 = dtUseFlag1.NewRow();
                ////drUse1["Key"] = "F";
                ////drUse1["Value"] = "사용안함";
                ////dtUseFlag1.Rows.Add(drUse1);

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtUserFlag = clsCom.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                //--그리드에넣기
                grd.mfSetGridColumnValueList(this.uGridSection, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUserFlag);

                //--한줄생성
                grd.mfAddRowGrid(this.uGridSection, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //콤보박스초기화
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue_All(), "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //버튼초기화
        private void InitButton()
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 툴바기능

        public void mfSearch()
        {
             try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory rslt = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(rslt);

                String strPlantCode = this.uComboPlant.Value.ToString();

                DataTable dt = rslt.mfReadSPInventory(strPlantCode, m_resSys.GetString("SYS_LANG"));
                
                this.uGrid.DataSource = dt;
                this.uGrid.DataBind();

                

                WinGrid grd = new WinGrid();

                // 바인딩후 체크박스 상태를 모두 Uncheck로 만든다
                grd.mfSetAllUnCheckedGridColumn(this.uGrid, 0, "Check");

                // RowSelector Clear
                grd.mfClearRowSeletorGrid(this.uGrid);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                 
                DialogResult DResult = new DialogResult();
                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    // 디비로 가져온 정보의 PK 편집 불가 상태로
                    for (int i = 0; i < this.uGrid.Rows.Count; i++)
                    {
                        this.uGrid.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGrid.Rows[i].Cells["SPInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGrid.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                        this.uGrid.Rows[i].Cells["SpInventoryCode"].Appearance.BackColor = Color.Gainsboro;
                    }

                    grd.mfSetAutoResizeColWidth(this.uGrid, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfSave()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                // BL 호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);

                // 저장 함수호출 매개변수 DataTable
                DataTable dtSPInventory = clsSPInventory.mfSetDataInfo();

                DialogResult DResult = new DialogResult();

                string strLang = m_resSys.GetString("SYS_LANG");

                if(this.uGrid.Rows.Count > 0)
                    this.uGrid.ActiveCell = this.uGrid.Rows[0].Cells[0];

                for (int i = 0; i < this.uGrid.Rows.Count; i++)
                {
                    //그리드가 수정되었을때 저장
                    if (this.uGrid.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        string strRowNum = this.uGrid.Rows[i].RowSelectorNumber.ToString();

                        //필수 입력사항 확인
                        if (this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000481",strLang), Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["PlantCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["SPInventoryCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000470",strLang), Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["SPInventoryCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["SPInventoryName"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000469",strLang), Infragistics.Win.HAlign.Center);

                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["SPInventoryName"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        ////else if (this.uGrid.Rows[i].Cells["SPInventoryNameCh"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                        ////        , (i + 1) + "번째 열의 SparePart 창고명_중문을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["SPInventoryNameCh"];
                        ////    this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}
                        ////else if (this.uGrid.Rows[i].Cells["SPInventoryNameEn"].Value.ToString() == "")
                        ////{
                        ////    msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                        ////        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "확인창", "필수입력사항 확인"
                        ////        , (i + 1) + "번째의 열의 SparePart 창고명_영문을 입력해주세요", Infragistics.Win.HAlign.Center);

                        ////    this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["SPInventoryNameEn"];
                        ////    this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        ////    return;
                        ////}
                        else
                        {
                           
                            DataRow drSpInventory = dtSPInventory.NewRow();
                            drSpInventory["PlantCode"] = this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString();
                            drSpInventory["SPInventoryCode"] = this.uGrid.Rows[i].Cells["SPInventoryCode"].Value.ToString();
                            drSpInventory["SPInventoryName"] = this.uGrid.Rows[i].Cells["SPInventoryName"].Value.ToString();
                            drSpInventory["SPInventoryNameEn"] = this.uGrid.Rows[i].Cells["SPInventoryNameEn"].Value.ToString();
                            drSpInventory["SPInventoryNameCh"] = this.uGrid.Rows[i].Cells["SPInventoryNameCh"].Value.ToString();
                            drSpInventory["UseFlag"] = this.uGrid.Rows[i].Cells["UseFlag"].Value.ToString();
                            dtSPInventory.Rows.Add(drSpInventory);
                        }
                    }
                }
                if (dtSPInventory.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001264", "M001053", "M000936",
                                            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        QRPProgressBar uProgressPopup = new QRPProgressBar();
                        Thread uTh = uProgressPopup.mfStartThread();
                        uProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //저장함수 호출
                        String strSPInventory = clsSPInventory.mfSaveSPInventory(dtSPInventory, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                        //Decoding
                        TransErrRtn ErrEtn = new TransErrRtn();
                        ErrEtn = ErrEtn.mfDecodingErrMessage(strSPInventory);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        uProgressPopup.mfCloseProgressPopup(this);

                        //처리결과에 따른 메세지 박스
                        if (ErrEtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500
                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001135", "M001037", "M000930"
                            , Infragistics.Win.HAlign.Right);
                            mfSearch();

                        }
                        else
                        {
                            string strMes = "";

                            if (ErrEtn.ErrMessage.Equals(string.Empty))
                                strMes = msg.GetMessge_Text("M000953", strLang);
                            else
                                strMes = ErrEtn.ErrMessage;

                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMes
                                , Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfDelete()
        {
            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
            try
            {
                titleArea.Focus();
                //SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SPInventory), "SPInventory");
                QRPMAS.BL.MASEQU.SPInventory clsSPInventory = new QRPMAS.BL.MASEQU.SPInventory();
                brwChannel.mfCredentials(clsSPInventory);

                //함수호출 매개변수 DataTable
                DataTable dtSPInventory = clsSPInventory.mfSetDataInfo();

                #region 필수 확인/DataCreate

                if(this.uGrid.Rows.Count > 0)
                    this.uGrid.ActiveCell = this.uGrid.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");

                //chek된것 삭제
                for (int i = 0; i < uGrid.Rows.Count; i++)
                {
                    // 활성셀을 첫번째줄 첫번째 셀로 이동
                    //this.uGrid.ActiveCell = this.uGrid.Rows[0].Cells[0];

                    if (Convert.ToBoolean(this.uGrid.Rows[i].Cells["Check"].Value))
                    {
                        string strRowNum = this.uGrid.Rows[i].RowSelectorNumber.ToString();

                        //필수입력사항 확인
                        if (this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString().Equals(string.Empty))
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                , strRowNum + msg.GetMessge_Text("M000481",strLang), Infragistics.Win.HAlign.Center);

                            //Focus Cell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["PlantCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGrid.Rows[i].Cells["SPInventoryCode"].Value.ToString().Equals(string.Empty))
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , msg.GetMessge_Text("M001264", strLang), msg.GetMessge_Text("M001228", strLang), strRowNum + msg.GetMessge_Text("M000527", strLang), Infragistics.Win.HAlign.Center);

                            //FocusCell
                            this.uGrid.ActiveCell = this.uGrid.Rows[i].Cells["SPInventoryCode"];
                            this.uGrid.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else
                        {
                            DataRow drSPInventory = dtSPInventory.NewRow();
                            drSPInventory["PlantCode"] = this.uGrid.Rows[i].Cells["PlantCode"].Value.ToString();
                            drSPInventory["SPInventoryCode"] = this.uGrid.Rows[i].Cells["SPInventoryCode"].Value.ToString();
                            dtSPInventory.Rows.Add(drSPInventory);
                        }
                    }
                }
                #endregion

                #region BL
                if (dtSPInventory.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                        , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M000650", "M000675"
                        , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        //처리로직
                        //함수호출
                        string strSG = clsSPInventory.mfDeleteSPInventory(dtSPInventory);

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strSG);
                        //처리로직끝

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        //삭제성공여부
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "M001135", "M000638", "M000677",
                                 Infragistics.Win.HAlign.Right);

                            // 리스트 갱신
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001135", "M000638", "M000923",
                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
            // 펼침상태가 false 인경우
            if (this.uGroupBoxContentsArea.Expanded == false)
            {
                this.uGroupBoxContentsArea.Expanded = true;
            }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGrid.Rows.Count == 0)//&& this.uGridSection.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000811", "M000805", Infragistics.Win.HAlign.Right);

                    return;
                }

                WinGrid grd = new WinGrid();

                //if (this.uGrid.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGrid);

                //if (this.uGridSection.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                //    grd.mfDownLoadGridToExcel(this.uGridSection);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
        }
        #endregion

        #region 이벤트
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridSection, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        //접히거나펴지는 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid.Height = 740;
                    for (int i = 0; i < uGrid.Rows.Count; i++)
                    {
                        uGrid.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// 콤보 기본값(선택)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue_Choice()
        {
            try
            {
                string strRtnValue = "";
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();

                if (strLang.Equals("KOR"))
                    strRtnValue = "선택";
                else if (strLang.Equals("CHN"))
                    strRtnValue = "选择";
                else if (strLang.Equals("ENG"))
                    strRtnValue = "Choice";

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보 기본값(전체)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue_All()
        {
            try
            {
                string strRtnValue = "";
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();

                if (strLang.Equals("KOR"))
                    strRtnValue = "전체";
                else if (strLang.Equals("CHN"))
                    strRtnValue = "全部";
                else if (strLang.Equals("ENG"))
                    strRtnValue = "All";


                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }
        #endregion

    }
}
