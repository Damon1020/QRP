﻿namespace QRPMAS.UI
{
    partial class frmMASZ0005
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0005));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonFileDown = new Infragistics.Win.Misc.UltraButton();
            this.uGridEquipPMD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uGridEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGridEquipPMAdmit = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uTextRejectReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRejectReason = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTabEquipPM = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.uTextCreateDate = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextRevisionReason = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextEtc = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevisionReason = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEtc = new Infragistics.Win.Misc.UltraLabel();
            this.uTextAdmitUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAcceptUser = new Infragistics.Win.Misc.UltraLabel();
            this.uDateAdmitDate = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uLabelAcceptDate = new Infragistics.Win.Misc.UltraLabel();
            this.uTextCreateUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextCreateUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelCreateUser = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextEquipGroupCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelEquipGroupCode = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVersionNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelRevisionNo = new Infragistics.Win.Misc.UltraLabel();
            this.uTextStandardNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelStandardNo = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTabPageControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMD)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMAdmit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabEquipPM)).BeginInit();
            this.uTabEquipPM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAdmitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNo)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.uButtonFileDown);
            this.ultraTabPageControl1.Controls.Add(this.uGridEquipPMD);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1040, 502);
            // 
            // uButtonFileDown
            // 
            this.uButtonFileDown.Location = new System.Drawing.Point(12, 12);
            this.uButtonFileDown.Name = "uButtonFileDown";
            this.uButtonFileDown.Size = new System.Drawing.Size(88, 28);
            this.uButtonFileDown.TabIndex = 5;
            this.uButtonFileDown.Click += new System.EventHandler(this.uButtonFileDown_Click);
            // 
            // uGridEquipPMD
            // 
            this.uGridEquipPMD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipPMD.DisplayLayout.Appearance = appearance40;
            this.uGridEquipPMD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipPMD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance41.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance41.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance41.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.Appearance = appearance41;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance42;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance43.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance43.BackColor2 = System.Drawing.SystemColors.Control;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMD.DisplayLayout.GroupByBox.PromptAppearance = appearance43;
            this.uGridEquipPMD.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipPMD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipPMD.DisplayLayout.Override.ActiveCellAppearance = appearance44;
            appearance45.BackColor = System.Drawing.SystemColors.Highlight;
            appearance45.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipPMD.DisplayLayout.Override.ActiveRowAppearance = appearance45;
            this.uGridEquipPMD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipPMD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.Override.CardAreaAppearance = appearance46;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipPMD.DisplayLayout.Override.CellAppearance = appearance47;
            this.uGridEquipPMD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipPMD.DisplayLayout.Override.CellPadding = 0;
            appearance48.BackColor = System.Drawing.SystemColors.Control;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMD.DisplayLayout.Override.GroupByRowAppearance = appearance48;
            appearance49.TextHAlignAsString = "Left";
            this.uGridEquipPMD.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.uGridEquipPMD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipPMD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipPMD.DisplayLayout.Override.RowAppearance = appearance50;
            this.uGridEquipPMD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipPMD.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.uGridEquipPMD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipPMD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipPMD.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipPMD.Location = new System.Drawing.Point(12, 40);
            this.uGridEquipPMD.Name = "uGridEquipPMD";
            this.uGridEquipPMD.Size = new System.Drawing.Size(1020, 452);
            this.uGridEquipPMD.TabIndex = 0;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.uGridEquipList);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1040, 502);
            // 
            // uGridEquipList
            // 
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipList.DisplayLayout.Appearance = appearance55;
            this.uGridEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance52.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance52.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance52.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.GroupByBox.Appearance = appearance52;
            appearance53.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance53;
            this.uGridEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance54.BackColor2 = System.Drawing.SystemColors.Control;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance54;
            this.uGridEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance63;
            appearance58.BackColor = System.Drawing.SystemColors.Highlight;
            appearance58.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance58;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.CardAreaAppearance = appearance57;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipList.DisplayLayout.Override.CellAppearance = appearance56;
            this.uGridEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance60.BackColor = System.Drawing.SystemColors.Control;
            appearance60.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance60.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance60.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance60.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance60;
            appearance62.TextHAlignAsString = "Left";
            this.uGridEquipList.DisplayLayout.Override.HeaderAppearance = appearance62;
            this.uGridEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipList.DisplayLayout.Override.RowAppearance = appearance61;
            this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance59.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance59;
            this.uGridEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipList.Location = new System.Drawing.Point(12, 12);
            this.uGridEquipList.Name = "uGridEquipList";
            this.uGridEquipList.Size = new System.Drawing.Size(1020, 484);
            this.uGridEquipList.TabIndex = 0;
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uGridEquipPMAdmit
            // 
            this.uGridEquipPMAdmit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipPMAdmit.DisplayLayout.Appearance = appearance15;
            this.uGridEquipPMAdmit.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipPMAdmit.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMAdmit.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMAdmit.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.uGridEquipPMAdmit.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipPMAdmit.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.uGridEquipPMAdmit.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipPMAdmit.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipPMAdmit.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipPMAdmit.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridEquipPMAdmit.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipPMAdmit.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMAdmit.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipPMAdmit.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridEquipPMAdmit.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipPMAdmit.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipPMAdmit.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.uGridEquipPMAdmit.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridEquipPMAdmit.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipPMAdmit.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipPMAdmit.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridEquipPMAdmit.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipPMAdmit.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.uGridEquipPMAdmit.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipPMAdmit.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipPMAdmit.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipPMAdmit.Location = new System.Drawing.Point(0, 80);
            this.uGridEquipPMAdmit.Name = "uGridEquipPMAdmit";
            this.uGridEquipPMAdmit.Size = new System.Drawing.Size(1070, 760);
            this.uGridEquipPMAdmit.TabIndex = 3;
            this.uGridEquipPMAdmit.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridEquipPMAdmit_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 4;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRejectReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRejectReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAdmitUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabEquipPM);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextRevisionReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevisionReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEtc);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAdmitUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAcceptUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uDateAdmitDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAcceptDate);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextCreateUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelCreateUser);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroupName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroupName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextEquipGroupCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelEquipGroupCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextVersionNum);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelRevisionNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextStandardNo);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelStandardNo);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uTextRejectReason
            // 
            this.uTextRejectReason.Location = new System.Drawing.Point(148, 128);
            this.uTextRejectReason.MaxLength = 100;
            this.uTextRejectReason.Name = "uTextRejectReason";
            this.uTextRejectReason.Size = new System.Drawing.Size(784, 21);
            this.uTextRejectReason.TabIndex = 45;
            // 
            // uLabelRejectReason
            // 
            this.uLabelRejectReason.Location = new System.Drawing.Point(12, 132);
            this.uLabelRejectReason.Name = "uLabelRejectReason";
            this.uLabelRejectReason.Size = new System.Drawing.Size(130, 20);
            this.uLabelRejectReason.TabIndex = 44;
            this.uLabelRejectReason.Text = "ultraLabel1";
            // 
            // uTextAdmitUserID
            // 
            appearance10.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextAdmitUserID.Appearance = appearance10;
            this.uTextAdmitUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance11.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance11;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextAdmitUserID.ButtonsRight.Add(editorButton1);
            this.uTextAdmitUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAdmitUserID.Location = new System.Drawing.Point(148, 60);
            this.uTextAdmitUserID.MaxLength = 20;
            this.uTextAdmitUserID.Name = "uTextAdmitUserID";
            this.uTextAdmitUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitUserID.TabIndex = 43;
            this.uTextAdmitUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextAdmitUserID_KeyDown);
            this.uTextAdmitUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextAdmitUserID_EditorButtonClick);
            // 
            // uTabEquipPM
            // 
            this.uTabEquipPM.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabEquipPM.Controls.Add(this.ultraTabSharedControlsPage1);
            this.uTabEquipPM.Controls.Add(this.ultraTabPageControl1);
            this.uTabEquipPM.Controls.Add(this.ultraTabPageControl2);
            this.uTabEquipPM.Location = new System.Drawing.Point(12, 156);
            this.uTabEquipPM.Name = "uTabEquipPM";
            this.uTabEquipPM.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.uTabEquipPM.Size = new System.Drawing.Size(1044, 528);
            this.uTabEquipPM.TabIndex = 42;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "점검항목상세";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "점검그룹 설비리스트";
            this.uTabEquipPM.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1040, 502);
            // 
            // uTextCreateDate
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateDate.Appearance = appearance9;
            this.uTextCreateDate.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateDate.Location = new System.Drawing.Point(588, 36);
            this.uTextCreateDate.Name = "uTextCreateDate";
            this.uTextCreateDate.ReadOnly = true;
            this.uTextCreateDate.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateDate.TabIndex = 41;
            // 
            // uLabelCreateDate
            // 
            this.uLabelCreateDate.Location = new System.Drawing.Point(452, 36);
            this.uLabelCreateDate.Name = "uLabelCreateDate";
            this.uLabelCreateDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelCreateDate.TabIndex = 40;
            this.uLabelCreateDate.Text = "ultraLabel2";
            // 
            // uTextRevisionReason
            // 
            appearance3.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionReason.Appearance = appearance3;
            this.uTextRevisionReason.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextRevisionReason.Location = new System.Drawing.Point(148, 84);
            this.uTextRevisionReason.Name = "uTextRevisionReason";
            this.uTextRevisionReason.ReadOnly = true;
            this.uTextRevisionReason.Size = new System.Drawing.Size(784, 21);
            this.uTextRevisionReason.TabIndex = 39;
            // 
            // uTextEtc
            // 
            this.uTextEtc.Location = new System.Drawing.Point(148, 104);
            this.uTextEtc.MaxLength = 100;
            this.uTextEtc.Name = "uTextEtc";
            this.uTextEtc.Size = new System.Drawing.Size(784, 21);
            this.uTextEtc.TabIndex = 39;
            // 
            // uLabelRevisionReason
            // 
            this.uLabelRevisionReason.Location = new System.Drawing.Point(12, 84);
            this.uLabelRevisionReason.Name = "uLabelRevisionReason";
            this.uLabelRevisionReason.Size = new System.Drawing.Size(130, 20);
            this.uLabelRevisionReason.TabIndex = 38;
            this.uLabelRevisionReason.Text = "ultraLabel1";
            // 
            // uLabelEtc
            // 
            this.uLabelEtc.Location = new System.Drawing.Point(12, 108);
            this.uLabelEtc.Name = "uLabelEtc";
            this.uLabelEtc.Size = new System.Drawing.Size(130, 20);
            this.uLabelEtc.TabIndex = 38;
            this.uLabelEtc.Text = "ultraLabel1";
            // 
            // uTextAdmitUserName
            // 
            appearance2.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitUserName.Appearance = appearance2;
            this.uTextAdmitUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextAdmitUserName.Location = new System.Drawing.Point(252, 60);
            this.uTextAdmitUserName.Name = "uTextAdmitUserName";
            this.uTextAdmitUserName.ReadOnly = true;
            this.uTextAdmitUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextAdmitUserName.TabIndex = 35;
            // 
            // uLabelAcceptUser
            // 
            this.uLabelAcceptUser.Location = new System.Drawing.Point(12, 60);
            this.uLabelAcceptUser.Name = "uLabelAcceptUser";
            this.uLabelAcceptUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelAcceptUser.TabIndex = 33;
            this.uLabelAcceptUser.Text = "ultraLabel2";
            // 
            // uDateAdmitDate
            // 
            appearance26.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAdmitDate.Appearance = appearance26;
            this.uDateAdmitDate.BackColor = System.Drawing.Color.PowderBlue;
            this.uDateAdmitDate.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uDateAdmitDate.Location = new System.Drawing.Point(588, 60);
            this.uDateAdmitDate.Name = "uDateAdmitDate";
            this.uDateAdmitDate.Size = new System.Drawing.Size(100, 21);
            this.uDateAdmitDate.TabIndex = 32;
            // 
            // uLabelAcceptDate
            // 
            this.uLabelAcceptDate.Location = new System.Drawing.Point(452, 60);
            this.uLabelAcceptDate.Name = "uLabelAcceptDate";
            this.uLabelAcceptDate.Size = new System.Drawing.Size(130, 20);
            this.uLabelAcceptDate.TabIndex = 31;
            this.uLabelAcceptDate.Text = "ultraLabel2";
            // 
            // uTextCreateUserName
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Appearance = appearance27;
            this.uTextCreateUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserName.Location = new System.Drawing.Point(252, 36);
            this.uTextCreateUserName.Name = "uTextCreateUserName";
            this.uTextCreateUserName.ReadOnly = true;
            this.uTextCreateUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserName.TabIndex = 30;
            // 
            // uTextCreateUserID
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserID.Appearance = appearance5;
            this.uTextCreateUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextCreateUserID.Location = new System.Drawing.Point(148, 36);
            this.uTextCreateUserID.Name = "uTextCreateUserID";
            this.uTextCreateUserID.ReadOnly = true;
            this.uTextCreateUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextCreateUserID.TabIndex = 29;
            // 
            // uLabelCreateUser
            // 
            this.uLabelCreateUser.Location = new System.Drawing.Point(12, 36);
            this.uLabelCreateUser.Name = "uLabelCreateUser";
            this.uLabelCreateUser.Size = new System.Drawing.Size(130, 20);
            this.uLabelCreateUser.TabIndex = 28;
            this.uLabelCreateUser.Text = "ultraLabel2";
            // 
            // uTextEquipGroupName
            // 
            appearance12.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Appearance = appearance12;
            this.uTextEquipGroupName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupName.Location = new System.Drawing.Point(588, 12);
            this.uTextEquipGroupName.Name = "uTextEquipGroupName";
            this.uTextEquipGroupName.ReadOnly = true;
            this.uTextEquipGroupName.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipGroupName.TabIndex = 27;
            // 
            // uLabelEquipGroupName
            // 
            this.uLabelEquipGroupName.Location = new System.Drawing.Point(452, 12);
            this.uLabelEquipGroupName.Name = "uLabelEquipGroupName";
            this.uLabelEquipGroupName.Size = new System.Drawing.Size(130, 20);
            this.uLabelEquipGroupName.TabIndex = 26;
            this.uLabelEquipGroupName.Text = "ultraLabel2";
            // 
            // uTextEquipGroupCode
            // 
            appearance7.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupCode.Appearance = appearance7;
            this.uTextEquipGroupCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextEquipGroupCode.Location = new System.Drawing.Point(148, 12);
            this.uTextEquipGroupCode.Name = "uTextEquipGroupCode";
            this.uTextEquipGroupCode.ReadOnly = true;
            this.uTextEquipGroupCode.Size = new System.Drawing.Size(150, 21);
            this.uTextEquipGroupCode.TabIndex = 25;
            // 
            // uLabelEquipGroupCode
            // 
            this.uLabelEquipGroupCode.Location = new System.Drawing.Point(12, 12);
            this.uLabelEquipGroupCode.Name = "uLabelEquipGroupCode";
            this.uLabelEquipGroupCode.Size = new System.Drawing.Size(130, 20);
            this.uLabelEquipGroupCode.TabIndex = 24;
            this.uLabelEquipGroupCode.Text = "ultraLabel2";
            // 
            // uTextVersionNum
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Appearance = appearance8;
            this.uTextVersionNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextVersionNum.Location = new System.Drawing.Point(996, 60);
            this.uTextVersionNum.Name = "uTextVersionNum";
            this.uTextVersionNum.ReadOnly = true;
            this.uTextVersionNum.Size = new System.Drawing.Size(36, 21);
            this.uTextVersionNum.TabIndex = 23;
            this.uTextVersionNum.Visible = false;
            // 
            // uLabelRevisionNo
            // 
            this.uLabelRevisionNo.Location = new System.Drawing.Point(984, 64);
            this.uLabelRevisionNo.Name = "uLabelRevisionNo";
            this.uLabelRevisionNo.Size = new System.Drawing.Size(12, 8);
            this.uLabelRevisionNo.TabIndex = 22;
            this.uLabelRevisionNo.Text = "-";
            this.uLabelRevisionNo.Visible = false;
            // 
            // uTextStandardNo
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardNo.Appearance = appearance14;
            this.uTextStandardNo.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextStandardNo.Location = new System.Drawing.Point(880, 60);
            this.uTextStandardNo.Name = "uTextStandardNo";
            this.uTextStandardNo.ReadOnly = true;
            this.uTextStandardNo.Size = new System.Drawing.Size(104, 21);
            this.uTextStandardNo.TabIndex = 21;
            this.uTextStandardNo.Visible = false;
            // 
            // uLabelStandardNo
            // 
            this.uLabelStandardNo.Location = new System.Drawing.Point(744, 60);
            this.uLabelStandardNo.Name = "uLabelStandardNo";
            this.uLabelStandardNo.Size = new System.Drawing.Size(130, 20);
            this.uLabelStandardNo.TabIndex = 20;
            this.uLabelStandardNo.Text = "ultraLabel1";
            this.uLabelStandardNo.Visible = false;
            // 
            // frmMASZ0005
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridEquipPMAdmit);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0005";
            this.Load += new System.EventHandler(this.frmMASZ0005_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0005_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMASZ0005_FormClosing);
            this.Resize += new System.EventHandler(this.frmMASZ0005_Resize);
            this.ultraTabPageControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMD)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipPMAdmit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRejectReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabEquipPM)).EndInit();
            this.uTabEquipPM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRevisionReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEtc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAdmitUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uDateAdmitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextCreateUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextEquipGroupCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVersionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextStandardNo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipPMAdmit;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEtc;
        private Infragistics.Win.Misc.UltraLabel uLabelEtc;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptUser;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor uDateAdmitDate;
        private Infragistics.Win.Misc.UltraLabel uLabelAcceptDate;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupName;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextEquipGroupCode;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGroupCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVersionNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextStandardNo;
        private Infragistics.Win.Misc.UltraLabel uLabelStandardNo;
        private Infragistics.Win.Misc.UltraLabel uLabelCreateDate;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabEquipPM;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextCreateDate;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipPMD;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipList;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAdmitUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRevisionReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRevisionReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRevisionNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRejectReason;
        private Infragistics.Win.Misc.UltraLabel uLabelRejectReason;
        private Infragistics.Win.Misc.UltraButton uButtonFileDown;
    }
}