﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통기준정보                                          */
/* 모듈(분류)명 : 설비관리 기준정보                                     */
/* 프로그램ID   : frmMASZ0026.cs                                        */
/* 프로그램명   : 정비사별 설비할당 정보                                */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가참조
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMASZ0026 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();
        //Debug모드를 위한 변수
        private bool m_bolDebugMode = false;
        private string m_strDBConn = "";

        public frmMASZ0026()
        {
            InitializeComponent();
        }

        private void frmMASZ0026_Activated(object sender, EventArgs e)
        {
            //툴바 설정//
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0026_Resize(object sender, EventArgs e)
        {
            //try
            //{
            //    if (this.Width > 1070)
            //    {
            //        uGroupBoxUser.Height = this.Height - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
            //    }
            //    else
            //    {
            //        uGroupBoxUser.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            //    }

            //}
            //catch (System.Exception ex)
            //{
            //    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
            //    frmErr.ShowDialog();
            //}
            //finally
            //{
            //}
        }
        
        private void frmMASZ0026_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void frmMASZ0026_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("정비사별 설비할당 정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetRunMode();
            SetToolAuth();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();
            InitGroupBox();

            
            //QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            //grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화 Method

        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void SetRunMode()
        {
            try
            {
                if (this.Tag != null)
                {
                    string[] sep = { "|" };
                    string[] arrArg = this.Tag.ToString().Split(sep, StringSplitOptions.None);

                    if (arrArg.Count() > 2)
                    {
                        //MessageBox.Show(this.Tag.ToString());
                        if (arrArg[1].ToString().ToUpper() == "DEBUG")
                        {
                            m_bolDebugMode = true;
                            if (arrArg.Count() > 3)
                                m_strDBConn = arrArg[2].ToString();
                        }

                    }
                    //Tag에 외부시스템에서 넘겨준 인자가 있으므로 인자에 따라 처리로직을 넣는다.
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
            
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 라벨 초기화
                WinLabel winLabel = new WinLabel();
                winLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelSearchDept, "부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelSearchUserID, "사용자ID", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelSearchUserName, "사용자명", m_resSys.GetString("SYS_FONTNAME"), true, false);


                winLabel.mfSetLabel(this.uLabelUserID, "사용자ID", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelUserName, "사용자명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelDeptName, "부서", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelPosition, "직위", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelTelNum, "전화번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelHpNum, "핸드폰번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelSubUser, "설비정비(부)", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelYN, "퇴사여부", m_resSys.GetString("SYS_FONTNAME"), true, false);



                winLabel.mfSetLabel(this.uLabelStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelLocation, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelEquipLargeType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelEquipGorup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);

                winLabel.mfSetLabel(this.uLabelM, "설비지정여부", m_resSys.GetString("SYS_FONTNAME"), false, false);
                //winLabel.mfSetLabel(this.uLabelS, "부", m_resSys.GetString("SYS_FONTNAME"), false, false);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼 초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();
                wButton.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                
                wButton.mfSetButton(this.uButtonOk, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                wButton.mfSetButton(this.uButtonSearch, "조회", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Search);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //콤보박스 초기화
                WinComboEditor combo = new WinComboEditor();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                clsPlant.Dispose();
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue_S()
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid wGrid = new WinGrid();

                #region 사용자정보

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridUserList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridUserList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "UserID", "사용자ID", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "UserName", "사용자명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "DeptName", "부서", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "Position", "직위", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "TelNum", "전화번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "No_Extension", "내선번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "HpNum", "핸드폰번호", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "EMail", "이메일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridUserList, 0, "YN_USE", "사용여부(퇴사여부)", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridUserList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridUserList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 정비사설비리스트

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquip, 0, "Check", "행삭제", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "Chk", "정비(부)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 70, true, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 130, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipTypeName", "설비유형", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "AdminRight", "관리자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "M");

                wGrid.mfSetGridColumn(this.uGridEquip, 0, "SubUserName", "(부)관리자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 5
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                #region DropDown

                //시스템공통코드정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                //관리자콤보 정보조회 매서드 호출
                DataTable dtAdmin = clsCommonCode.mfReadCommonCode("C0057", m_resSys.GetString("SYS_LANG"));

                //관리자콤보 그리드에 삽입
                wGrid.mfSetGridColumnValueList(this.uGridEquip, 0, "AdminRight", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtAdmin);

                #endregion

                // Set FontSize
                this.uGridEquip.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquip.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 공백추가
                wGrid.mfAddRowGrid(this.uGridEquip, 0);

                this.uGridEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
                this.uGridEquip.DisplayLayout.Bands[0].Columns["Chk"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                #endregion

                #region 설비리스트

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, true, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 130, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipTypeName", "설비유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridEquipList.DisplayLayout.Override.SelectedAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;
                this.uGridEquipList.DisplayLayout.Override.ActiveAppearancesEnabled = Infragistics.Win.DefaultableBoolean.False;

                #endregion

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void InitGroupBox()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grBox = new WinGroupBox();

                grBox.mfSetGroupBox(this.uGroupBoxUser, GroupBoxType.LIST, "정비사리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grBox.mfSetGroupBox(this.uGroupBoxLineInfo, GroupBoxType.DETAIL, "정비사상세정보", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                grBox.mfSetGroupBox(this.uGroupBoxEquip, GroupBoxType.LIST, "설비리스트", m_resSys.GetString("SYS_FONTNAME"), Infragistics.Win.Misc.GroupBoxViewStyle.Default
                    , Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                //폰트설정
                uGroupBoxUser.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxUser.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //폰트설정
                uGroupBoxLineInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxLineInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                //폰트설정
                uGroupBoxEquip.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxEquip.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보 기본값(선택)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue_S()
        {
            try
            {
                string strRtnValue = "";
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();

                if (strLang.Equals("KOR"))
                    strRtnValue = "선택";
                else if (strLang.Equals("CHN"))
                    strRtnValue = "选择";
                else if (strLang.Equals("ENG"))
                    strRtnValue = "Choice";

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        /// <summary>
        /// 콤보 기본값(전체)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue_A()
        {
            try
            {
                string strRtnValue = "";
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();

                if (strLang.Equals("KOR"))
                    strRtnValue = "전체";
                else if (strLang.Equals("CHN"))
                    strRtnValue = "全部";
                else if (strLang.Equals("ENG"))
                    strRtnValue = "All";
                

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        #endregion


        #region 2.ToolBar 정보
        // 검색

        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();

                string strSearchPlantcode = this.uComboSearchPlant.Value.ToString();
                string strSearchDeptCode = this.uComboSearchDept.Value.ToString();

                if (strSearchPlantcode.Equals(string.Empty))
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M000274", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 연결
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                DataTable dtUser = new DataTable();
                //dtUser = clsUser.mfReadSYSUser(strSearchPlantcode, "", m_resSys.GetString("SYS_LANG"));
                dtUser = clsUser.mfReadSYSUser_DeptLikeAll(strSearchPlantcode, strSearchDeptCode, this.uTextSearchUserID.Text,this.uTextSearchUserName.Text ,m_resSys.GetString("SYS_LANG"));

                this.uGridUserList.DataSource = dtUser;
                this.uGridUserList.DataBind();

                

                if (dtUser.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //YN_USE(사용여부,퇴사여부) Y가아니면 해당줄의 색을 변경한다.
                    for (int i = 0; i < this.uGridUserList.Rows.Count; i++)
                    {
                        if (!this.uGridUserList.Rows[i].Cells["YN_USE"].Value.ToString().Equals("Y"))
                            this.uGridUserList.Rows[i].Appearance.BackColor = Color.Salmon;
                    }

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                #region 필수확인
                //---------------------------------------------필수 입력사항 체크 및 값 저장 -------------------------------------------------//
                if (this.uGridUserList.Rows.Count == 0 || this.uGridEquip.Rows.Count == 0)
                {

                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                             "M001264", "M001230", "M001047", Infragistics.Win.HAlign.Right);

                    return;

                }

                #endregion

                #region 컬럼설정
                //설비그룹정비사 컬럼Set
                QRPMAS.BL.MASEQU.EquipWorker clsEquipWorker;
                if (m_bolDebugMode == false)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipWorker), "EquipWorker");
                    clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker();
                    brwChannel.mfCredentials(clsEquipWorker);
                }
                else
                    clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker(m_strDBConn);

                DataTable dtEquip = clsEquipWorker.mfSetDateInfo();
                int intCnt = 0;
                int intEquip = 0;
                #endregion

                #region 정보저장

                //루프를 돌며 해당 정비사의 담당설비를 저장한다.
                if (this.uGridEquip.Rows.Count > 0)
                {
                    this.uGridEquip.ActiveCell = this.uGridEquip.Rows[0].Cells["Check"];

                    for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                    {
                        if (this.uGridEquip.Rows[i].Cells["EquipCode"].Value.ToString() != ""
                            && this.uGridEquip.Rows[i].Cells["EquipName"].Value.ToString() != ""
                            && this.uGridEquip.Rows[i].Hidden.Equals(false))
                        {
                            DataRow drSaveEquip;
                            drSaveEquip = dtEquip.NewRow();

                            drSaveEquip["UserID"] = this.uTextUserID.Text.ToUpper();
                            drSaveEquip["EquipCode"] = this.uGridEquip.Rows[i].Cells["EquipCode"].Value.ToString().ToUpper();
                            drSaveEquip["AdminRight"] = this.uGridEquip.Rows[i].Cells["AdminRight"].Value.ToString();

                            dtEquip.Rows.Add(drSaveEquip);

                            //설비정비(부)를 입력한 경우 테이블에 저장
                            if (this.uGridEquip.Rows[i].Cells["SubUserName"].Value.ToString().Equals(string.Empty))
                            {
                                intEquip++; //(부)관리자 선택가능 설비 수

                                if (Convert.ToBoolean(this.uGridEquip.Rows[i].Cells["Chk"].Value) 
                                    && !this.uTextSubUserID.Text.Equals(string.Empty)
                                && !this.uTextSubUserName.Text.Equals(string.Empty))
                                {
                                    //등록하는 설비의 (부)관리자가 설비를 이미 할당하고있는 지 조회
                                    DataTable dtUser = clsEquipWorker.mfReadEquipWorker(this.uTextPlantCode.Text, this.uTextSubUserID.Text, m_resSys.GetString("SYS_LANG"));

                                    if (dtUser.Rows.Count > 0)
                                    {
                                        for (int o = 0; o < dtUser.Rows.Count; o++)
                                        {
                                            if (dtUser.Rows[o]["EquipCode"].ToString().Equals(this.uGridEquip.Rows[i].Cells["EquipCode"].Value.ToString()))
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500,Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                    , msg.GetMessge_Text("M001264", m_resSys.GetString("SYS_LANG")), msg.GetMessge_Text("M000014",m_resSys.GetString("SYS_LANG"))
                                                                    , msg.GetMessge_Text("M000844", m_resSys.GetString("SYS_LANG")) + this.uGridEquip.Rows[i].Cells["EquipCode"].Value.ToString() + msg.GetMessge_Text("M000016",m_resSys.GetString("SYS_LANG"))
                                                                    , Infragistics.Win.HAlign.Right);

                                                this.uGridEquip.ActiveCell = this.uGridEquip.Rows[i].Cells["EquipCode"];
                                                return;
                                            }
                                        }
                                    }

                                    drSaveEquip = dtEquip.NewRow();

                                    drSaveEquip["UserID"] = this.uTextSubUserID.Text.ToUpper();
                                    drSaveEquip["EquipCode"] = this.uGridEquip.Rows[i].Cells["EquipCode"].Value.ToString().ToUpper();
                                    drSaveEquip["AdminRight"] = "S";


                                    dtEquip.Rows.Add(drSaveEquip);
                                    
                                    intCnt++; // (부) 관리자로 선택한 설비 수
                                }
                            }
                        }
                    }
                }
                #endregion

                //(부)관리자는 입력되어있는데 선택되어있는게 없다면 확인을 시켜준다.
                if (!this.uTextSubUserID.Text.Equals(string.Empty)
                    && !this.uTextSubUserName.Text.Equals(string.Empty) 
                    && !intEquip.Equals(0) 
                    && intCnt.Equals(0))
                {
                    if(msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                            "M001264", "M000882", "M000013", Infragistics.Win.HAlign.Right) == DialogResult.No)
                            return;
                }

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //진행 팝업창 열기
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", m_resSys.GetString("SYS_LANG")));
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //삭제BL처리
                    string strRtn = clsEquipWorker.mfSaveEquipWorker(uTextPlantCode.Text, uTextUserID.Text, dtEquip, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    /////////////

                    //진행 팝업창 닫기
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;

                    //삭제결과 메세지 보여주기
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);

                        #region 정비사정보 초기화
                        uTextPlantCode.Clear();
                        uTextUserID.Clear();
                        uTextUserName.Clear();
                        uTextDeptName.Clear();
                        uTextPosition.Clear();
                        uTextTelNum.Clear();
                        uTextHpNum.Clear();
                        this.uTextSubUserID.Clear();
                        this.uTextSubUserName.Clear();

                        if (this.uGridEquip.Rows.Count > 0)
                        {
                            this.uGridEquip.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquip.Rows.All);
                            this.uGridEquip.DeleteSelectedRows(false);
                        }

                        if (this.uGridEquip.DisplayLayout.Override.AllowAddNew.Equals(Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom))
                            this.uGridEquip.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                        this.uComboEquipLoc.Items.Clear();
                        this.uComboEquipLargeType.Items.Clear();
                        this.uComboStation.Items.Clear();

                        #endregion

                        mfSearch();
                    }
                    else
                    {
                        string strErrMessge = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strErrMessge = msg.GetMessge_Text("M000953",m_resSys.GetString("SYS_LANG"));
                        else
                            strErrMessge = ErrRtn.ErrMessage;

                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     msg.GetMessge_Text("M001135", m_resSys.GetString("SYS_LANG"))
                                                     , msg.GetMessge_Text("M001037", m_resSys.GetString("SYS_LANG")), strErrMessge,
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 삭제        
        public void mfDelete()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //---------------------------------------------필수 입력사항 체크 및 값 저장 -------------------------------------------------//
                if (this.uGridUserList.Rows.Count == 0 || this.uGridEquip.Rows.Count == 0)
                {

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                 "M001264", "M001230", "M000643", Infragistics.Win.HAlign.Right);

                        return;

                }
               

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    //진행 팝업창 열기
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", m_resSys.GetString("SYS_LANG")));
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //삭제BL처리
                    QRPMAS.BL.MASEQU.EquipWorker clsEquipWorker;
                    if (m_bolDebugMode == false)
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipWorker), "EquipWorker");
                        clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker();
                        brwChannel.mfCredentials(clsEquipWorker);
                    }
                    else
                        clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker(m_strDBConn);

                    string strRtn = clsEquipWorker.mfDeleteEquipWorker(uTextPlantCode.Text, uTextUserID.Text);

                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    /////////////

                    //진행 팝업창 닫기
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;

                    //삭제결과 메세지 보여주기
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);

                        ////ReBind
                        InitValue();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 신규
        public void mfCreate()
        {
            try
            {
                InitValue();

                //// 펼침상태가 false 인경우
                //if (this.uGroupBoxContentsArea.Expanded == false)
                //{
                //    this.uGroupBoxContentsArea.Expanded = true;

                //}
                //// 이미 펼쳐진 상태이면 컴포넌트 초기화
                //else
                //{
                //    while (this.uGridEquipList.Rows.Count > 0)
                //    {
                //        this.uGridEquipList.Rows[0].Delete(false);
                //    }
                //}
                while (this.uGridEquip.Rows.Count > 0)
                {
                    this.uGridEquip.Rows[0].Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitValue()
        {
            try
            {
                uTextPlantCode.Clear();
                uTextUserID.Clear();
                uTextUserName.Clear();
                uTextDeptName.Clear();
                uTextPosition.Clear();
                uTextTelNum.Clear();
                uTextHpNum.Clear();
                this.uTextSubUserID.Clear();
                this.uTextSubUserName.Clear();

                if (this.uGridEquip.Rows.Count > 0)
                {
                    this.uGridEquip.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquip.Rows.All);
                    this.uGridEquip.DeleteSelectedRows(false);
                }

                if (this.uGridEquipList.Rows.Count > 0)
                {
                    this.uGridEquipList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipList.Rows.All);
                    this.uGridEquipList.DeleteSelectedRows(false);
                }

                this.uComboEquipLoc.Items.Clear();
                this.uComboEquipLargeType.Items.Clear();
                this.uComboStation.Items.Clear();
                this.uComboProcessGroup.Items.Clear();
                this.uComboEquipGroup.Items.Clear();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 출력
        public void mfPrint()
        {
        }

        // 엑셀다운
        public void mfExcel()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridUserList.Rows.Count == 0 && this.uGridEquip.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001173", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                if(this.uGridUserList.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridUserList);
                if (this.uGridEquip.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquip);



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Event

        #region Grid

        //그리드에 신규줄이생성시 수정불가로 변경
        private void uGridEquipList_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            //if(!e.Row.Cells["AdminRight"].Value.Equals(string.Empty))
            //{
            //    e.Row.Cells["AdminRight"].Value = string.Empty;
            //}
            //e.Row.Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }

        private void uGridUserList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (e.Cell.Row.Cells["UserID"].Value.ToString() != "")
                {
                    WinMessageBox msg = new WinMessageBox();
                    
                    //if (!this.uTextUserID.Text.Equals(string.Empty))
                    //{
                    //    if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                        "정보선택확인창", "선택확인", "선택하신 사용자를 설비정비(부)로 선택하시겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    //    {
                    //        //동일 아이디를 선택한 경우 리턴
                    //        if (this.uTextUserID.Text.Equals(e.Cell.Row.Cells["UserID"].Value.ToString()))
                    //        {
                    //            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                    //                            "확인창", "ID정보확인", "선택하신 ID가 이미 선택되어 있습니다.", Infragistics.Win.HAlign.Right);
                    //            return;
                    //        }
                    //        this.uTextSubUserID.Text = e.Cell.Row.Cells["UserID"].Value.ToString();
                    //        this.uTextSubUserName.Text = e.Cell.Row.Cells["UserName"].Value.ToString();
                    //        return;
                    //    }
                    //}

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread threadPop = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                    this.MdiParent.Cursor = Cursors.WaitCursor;


                    string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                    if (this.uTextPlantCode.Text.Equals(string.Empty) || !this.uTextPlantCode.Text.Equals(strPlantCode))
                    {
                        uTextPlantCode.Text = strPlantCode;

                        WinComboEditor com = new WinComboEditor();

                        //Station정보 BL호출
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                        QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                        brwChannel.mfCredentials(clsStation);

                        //Station콤보조회 매서드 실행
                        DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                        this.uComboStation.Items.Clear();

                        //Station콤보에 삽입
                        com.mfSetComboEditor(this.uComboStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                          , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue_A(), "StationCode", "StationName", dtStation);

                    }


                    uTextUserID.Text = e.Cell.Row.Cells["UserID"].Value.ToString();
                    uTextUserName.Text = e.Cell.Row.Cells["UserName"].Value.ToString();
                    uTextDeptName.Text = e.Cell.Row.Cells["DeptName"].Value.ToString();
                    uTextPosition.Text = e.Cell.Row.Cells["Position"].Value.ToString();
                    uTextTelNum.Text = e.Cell.Row.Cells["TelNum"].Value.ToString();
                    uTextHpNum.Text = e.Cell.Row.Cells["HpNum"].Value.ToString();

                    if (this.uGridEquip.DisplayLayout.Override.AllowAddNew.Equals(Infragistics.Win.UltraWinGrid.AllowAddNew.No))
                        this.uGridEquip.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;

                    // BL 연결
                    QRPMAS.BL.MASEQU.EquipWorker clsEquipWorker;
                    if (m_bolDebugMode == false)
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipWorker), "EquipWorker");
                        clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker();
                        brwChannel.mfCredentials(clsEquipWorker);
                    }
                    else
                        clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker(m_strDBConn);

                    DataTable dtEquipWorker = new DataTable();
                    dtEquipWorker = clsEquipWorker.mfReadEquipWorker(uTextPlantCode.Text, uTextUserID.Text, m_resSys.GetString("SYS_LANG"));

                    //테이터바인드
                    uGridEquip.DataSource = dtEquipWorker;
                    uGridEquip.DataBind();
                    if (dtEquipWorker.Rows.Count > 0)
                    {
                        

                        WinGrid grd = new WinGrid();
                        grd.mfSetAutoResizeColWidth(this.uGridEquip, 0);

                        //해당정비사 설비의 (부)관리자 정보조회매서드 실행
                        DataTable dtSub = clsEquipWorker.mfReadEquipWorker_Sub(uTextPlantCode.Text, uTextUserID.Text, m_resSys.GetString("SYS_LANG"));

                        //정보가 있는 경우 루프를 돌며 해당 설비의 부관리자셀에 뿌려줌.
                        if (dtSub.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtSub.Rows.Count; i++)
                            {
                                for (int t = 0; t < this.uGridEquip.Rows.Count; t++)
                                {
                                    if (dtSub.Rows[i]["EquipCode"].ToString().Equals(this.uGridEquip.Rows[t].Cells["EquipCode"].Value.ToString()))
                                    {
                                        if (this.uGridEquip.Rows[t].Cells["SubUserName"].Value.ToString().Equals(string.Empty))
                                            this.uGridEquip.Rows[t].Cells["SubUserName"].Value = dtSub.Rows[i]["SubUserName"];
                                        else
                                            this.uGridEquip.Rows[t].Cells["SubUserName"].Value = this.uGridEquip.Rows[t].Cells["SubUserName"].Value.ToString() + "," + dtSub.Rows[i]["SubUserName"].ToString();
                                        break;
                                    }
                                }
                            }


                        }
                        //정비사가 해당설비의 부관리자거나 부관리자가 있는 경우 부관리자체크박스수정불가처리
                        for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                        {
                            this.uGridEquip.Rows[i].Cells["EquipCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            if (this.uGridEquip.Rows[i].GetCellValue("AdminRight").ToString().Equals("S")
                                || !this.uGridEquip.Rows[i].GetCellValue("SubUserName").ToString().Equals(string.Empty))
                            {
                                this.uGridEquip.Rows[i].Cells["Chk"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }
                        }

                        
                    }



                    if (!this.uTextSubUserName.Text.Equals(string.Empty))
                    {
                        this.uTextSubUserID.Clear();
                        this.uTextSubUserName.Clear();
                    }
                    

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 설비의 부담당자 설정시 공백이아니면 메세지출력한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridEquip_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {       
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //(부)관리자를 선택 시 부관리자란이 공백이아닌경우 메세지를띄어준다.
                if (e.Cell.Column.Key.Equals("Chk"))
                {
                    if (Convert.ToBoolean(e.Cell.Value)
                        && e.Cell.Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.AllowEdit))
                    {
                        if (!e.Cell.Row.Cells["SubUserName"].Value.ToString().Equals(string.Empty))
                        {
                            

                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000014", "M000012", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = false;
                            return;
                        }
                    }
                }

                // 업데이트 셀이 설비코드 컬럼이라면 입력된 설비코드를 조회하여 정보를 넣는다. 만약 정보가 없다면 메세지 박스를 띄운다.
                if (e.Cell.Column.Key.Equals("EquipCode"))
                {
                    if (!e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        //설비코드 중복여부 확인
                        for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                        {
                            if (!i.Equals(e.Cell.Row.Index)
                                && this.uGridEquip.Rows[i].Hidden.Equals(false)
                                && e.Cell.Value.ToString().Equals(this.uGridEquip.Rows[i].Cells["EquipCode"].Value.ToString()))
                            {
                                msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                       , "M001264", "M000719", "M000900", Infragistics.Win.HAlign.Right);
                                //if (!e.Cell.Row.Cells["EquipName"].Value.ToString().Equals(string.Empty))
                                //    e.Cell.Row.Cells["EquipName"].Value = string.Empty;
                                e.Cell.Row.Delete(false);

                                return;
                            }
                        }

                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                        QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                        brwChannel.mfCredentials(clsEquip);

                        DataTable dtEquipInfo = clsEquip.mfReadEquipName(this.uTextPlantCode.Text == null ? "" : this.uTextPlantCode.Text, e.Cell.Value.ToString(), m_resSys.GetString("SYS_LANG"));

                        if (dtEquipInfo.Rows.Count > 0)
                        {
                            e.Cell.Row.Cells["EquipName"].Value = dtEquipInfo.Rows[0]["EquipName"].ToString();
                            e.Cell.Row.Cells["StationName"].Value = dtEquipInfo.Rows[0]["StationName"].ToString();
                            e.Cell.Row.Cells["EquipLocName"].Value = dtEquipInfo.Rows[0]["EquipLocName"].ToString();

                            if (!this.uTextSubUserID.Text.Equals(string.Empty)
                                && !this.uTextSubUserName.Text.Equals(string.Empty))
                            {
                                SearchEquip(this.uTextPlantCode.Text == null ? "" : this.uTextPlantCode.Text, this.uTextSubUserID.Text);
                            }
                        }
                        else
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000718", "M000901", Infragistics.Win.HAlign.Right);

                                e.Cell.Row.Delete(false);
                            
                        }

                    }
                    else
                    {
                        //if (!e.Cell.Row.Cells["EquipName"].Value.ToString().Equals(string.Empty))
                        //    e.Cell.Row.Cells["EquipName"].Value = string.Empty;
                        e.Cell.Row.Delete(false);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Combo

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                //공장정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();


                if (!strPlantCode.Equals(string.Empty))
                {
                    InitValue();

                    //부서콤보 초기화
                    this.uComboSearchDept.Items.Clear();

                    //부서정보BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.Dept), "Dept");
                    QRPSYS.BL.SYSUSR.Dept clsDept = new QRPSYS.BL.SYSUSR.Dept();
                    brwChannel.mfCredentials(clsDept);

                    //부서정보 콤보 매서드 실행
                    DataTable dtDept = clsDept.mfReadSYSDeptForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    //부서콤보에 추가
                    WinComboEditor com = new WinComboEditor();
                    com.mfSetComboEditor(this.uComboSearchDept, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME"), true
                        , false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", ComboDefaultValue_A(), "DeptCode", "DeptName", dtDept);

                    if(this.uComboSearchPlant.Value.Equals(m_resSys.GetString("SYS_PLANTCODE")))
                    {
                        this.uComboSearchDept.Value = m_resSys.GetString("SYS_DEPTCODE");
                    }


                    //유저정보리스트 초기화
                    if (this.uGridUserList.Rows.Count > 0)
                    {
                        this.uGridUserList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridUserList.Rows.All);
                        this.uGridUserList.DeleteSelectedRows(false);
                    }

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Station 선택시 위치가변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboStation.Value.ToString();
                string strStation = this.uComboStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //검색조건저장
                    string strPlantCode = this.uTextPlantCode.Text;

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //설비위치정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsEquipLocation);

                    //설비위치정보콤보조회 매서드 실행
                    DataTable dtLoc = clsEquipLocation.mfReadLocation_Combo(strPlantCode, strStationCode, m_resSys.GetString("SYS_LANG"));

                    WinComboEditor wCombo = new WinComboEditor();

                    //설비위치정보콤보 이전 정보 클리어
                    this.uComboEquipLoc.Items.Clear();

                    wCombo.mfSetComboEditor(this.uComboEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue_S(), "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(strPlantCode, strStationCode, "", "", "", 3);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 위치변경시 설비대분류 중분류 그룹이변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboLocation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uTextPlantCode.Text, this.uComboStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), "", "", 3);

                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 설비대분류가 변경 시 중분류 그룹이 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uTextPlantCode.Text, this.uComboStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 중분류가 변경 시 그룹이 변경
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboEquipLargeType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboEquipLargeType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboEquipLargeType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboEquipLargeType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboEquipLargeType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboEquipLargeType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uTextPlantCode.Text, this.uComboStation.Value.ToString(), this.uComboEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboEquipLargeType.Value.ToString(), 1);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region Button

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridEquip.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridEquip.Rows[i].Hidden = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //확인버튼을 누르면 선택된 설비가 정비사할당 설비 리스트 그리드로 들어간다.
        private void uButtonOk_Click(object sender, EventArgs e)
        {
            try
            {
                //설비리스트가 없으면 리턴
                if (this.uGridEquipList.Rows.Count == 0)
                    return;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                DataTable dtSource = new DataTable();
                dtSource.Columns.Add("EquipCode", typeof(string));
                dtSource.Columns.Add("EquipName", typeof(string));
                dtSource.Columns.Add("AreaName", typeof(string));
                dtSource.Columns.Add("StationName", typeof(string));
                dtSource.Columns.Add("EquipLocName", typeof(string));
                dtSource.Columns.Add("EquipTypeName", typeof(string));
                dtSource.Columns.Add("EquipProcGubunName", typeof(string));
                dtSource.Columns.Add("AdminRight", typeof(string));
                dtSource.Columns.Add("SubUserName", typeof(string));
                dtSource.Columns.Add("Chk", typeof(string));
                //정비사정보를 담았는지 체크하는 변수
                string strSave = "";

                //For문을 돌며 체크박스에 선택되어있는 설비정보를 이동시킨다.
                //단 정비사가 이미 설비정보를 가지고 있으면 이동하지 않는다.
                for (int i = 0; i < this.uGridEquipList.Rows.Count; i++) 
                {
                    if (Convert.ToBoolean(this.uGridEquipList.Rows[i].Cells["Check"].Value).Equals(true))
                    {
                        string strChk = "";
                        if (this.uGridEquip.Rows.Count > 0)
                        {
                            
                            //정비사 설비리스트 그리드를 돌며 중복확인
                            for (int j = 0; j < this.uGridEquip.Rows.Count; j++)
                            {
                                if (this.uGridEquip.Rows[j].Hidden.Equals(false)
                                    && !this.uGridEquip.Rows[j].Cells["EquipCode"].Value.Equals(string.Empty)
                                    && !this.uGridEquip.Rows[j].Cells["EquipName"].Value.Equals(string.Empty))
                                {
                                    //설비중복확인
                                    if (this.uGridEquip.Rows[j].Cells["EquipCode"].Value.Equals(this.uGridEquipList.Rows[i].Cells["EquipCode"].Value))
                                    {
                                        if (strChk.Equals(string.Empty))
                                            strChk = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value.ToString();
                                    }
                                    if (strSave.Equals(string.Empty))
                                    {
                                        DataRow drSource = dtSource.NewRow();
                                        drSource["EquipCode"] = this.uGridEquip.Rows[j].Cells["EquipCode"].Value;
                                        drSource["EquipName"] = this.uGridEquip.Rows[j].Cells["EquipName"].Value;
                                        drSource["AreaName"] = this.uGridEquip.Rows[j].Cells["AreaName"].Value;
                                        drSource["StationName"] = this.uGridEquip.Rows[j].Cells["StationName"].Value;
                                        drSource["EquipLocName"] = this.uGridEquip.Rows[j].Cells["EquipLocName"].Value;
                                        drSource["EquipTypeName"] = this.uGridEquip.Rows[j].Cells["EquipTypeName"].Value;
                                        drSource["EquipProcGubunName"] = this.uGridEquip.Rows[j].Cells["EquipProcGubunName"].Value;
                                        
                                        if(this.uGridEquip.Rows[j].Cells["Chk"].Activation.Equals(Infragistics.Win.UltraWinGrid.Activation.NoEdit))
                                            drSource["SubUserName"] = this.uGridEquip.Rows[j].Cells["SubUserName"].Value;
                                        
                                        drSource["AdminRight"] = this.uGridEquip.Rows[j].Cells["AdminRight"].Value;
                                        drSource["Chk"] = this.uGridEquip.Rows[j].Cells["Chk"].Value;
                                        dtSource.Rows.Add(drSource);
                                    }
                                }
                            }

                            if(strSave.Equals(string.Empty))
                                strSave = "ok";
                        }

                        if (strChk.Equals(string.Empty))
                        {
                            
                            int intCnt = this.uGridEquip.Rows.Count;
                            
                            //데이터테이블에 선택한 설비정보를 추가한다.
                            DataRow drSource = dtSource.NewRow();
                            drSource["EquipCode"] = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value;
                            drSource["EquipName"] = this.uGridEquipList.Rows[i].Cells["EquipName"].Value;
                            drSource["AreaName"] = this.uGridEquipList.Rows[i].Cells["AreaName"].Value;
                            drSource["StationName"] = this.uGridEquipList.Rows[i].Cells["StationName"].Value;
                            drSource["EquipLocName"] = this.uGridEquipList.Rows[i].Cells["EquipLocName"].Value;
                            drSource["EquipTypeName"] = this.uGridEquipList.Rows[i].Cells["EquipTypeName"].Value;
                            drSource["EquipProcGubunName"] = this.uGridEquipList.Rows[i].Cells["EquipProcGubunName"].Value;
                            drSource["AdminRight"] = "M";
                            drSource["Chk"] = "false";
                            dtSource.Rows.Add(drSource);
                            
                            //else
                            //{
                            //    this.uGridEquip.DisplayLayout.Bands[0].AddNew();
                            //    this.uGridEquip.Rows[intCnt - 1].Cells["EquipCode"].Value = this.uGridEquipList.Rows[i].Cells["EquipCode"].Value;
                            //    this.uGridEquip.Rows[intCnt - 1].Cells["EquipName"].Value = this.uGridEquipList.Rows[i].Cells["EquipName"].Value;
                            //    this.uGridEquip.Rows[intCnt - 1].Cells["AreaName"].Value = this.uGridEquipList.Rows[i].Cells["AreaName"].Value;
                            //    this.uGridEquip.Rows[intCnt - 1].Cells["StationName"].Value = this.uGridEquipList.Rows[i].Cells["StationName"].Value;
                            //    this.uGridEquip.Rows[intCnt - 1].Cells["EquipLocName"].Value = this.uGridEquipList.Rows[i].Cells["EquipLocName"].Value;
                            //    this.uGridEquip.Rows[intCnt - 1].Cells["EquipTypeName"].Value = this.uGridEquipList.Rows[i].Cells["EquipTypeName"].Value;
                            //    this.uGridEquip.Rows[intCnt - 1].Cells["EquipProcGubunName"].Value = this.uGridEquipList.Rows[i].Cells["EquipProcGubunName"].Value;
                            //    this.uGridEquip.Rows[intCnt - 1].Cells["AdminRight"].Value = "M";
                            //    //this.uGridEquip.DisplayLayout.Bands[0].AddNew();
                            //    //this.uGridEquip.Rows[intCnt].Hidden = true;
                            //}
                            
                        }

                        // 이동한 설비정보를 숨긴다.
                        if (this.uGridEquipList.Rows[i].Hidden.Equals(false))
                        {
                            //this.uGridEquipList.Rows[i].Hidden = true;
                            this.uGridEquipList.Rows[i].Cells["Check"].Value = false;
                        }
                    }
                }

                // 설비 수정불가, 부관리자가 존재할경우 부관리자 체크박스 수정불가처리
                if (dtSource.Rows.Count > 0)
                {
                    this.uGridEquip.DataSource = dtSource;
                    this.uGridEquip.DataBind();

                    if (this.uGridEquip.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                        {
                            this.uGridEquip.Rows[i].Cells["EquipCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

                            if (this.uGridEquip.Rows[i].GetCellValue("AdminRight").ToString().Equals("S")
                                || !this.uGridEquip.Rows[i].GetCellValue("SubUserName").ToString().Equals(string.Empty))
                            {
                                this.uGridEquip.Rows[i].Cells["Chk"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }
                        }

                        //설비정비(부) 란에 입력이 되어있다면 부관리자에 대한 설비정보조회 매서드 실행
                        if (!this.uTextSubUserID.Text.Equals(string.Empty) && !this.uTextSubUserName.Text.Equals(string.Empty))
                            SearchEquip(this.uTextPlantCode.Text, this.uTextSubUserID.Text);
                    }
                }

                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //검색버튼누를시 정비사가 선택되어있으면 조회한다.
        private void uButtonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if(this.uTextPlantCode.Text.Equals(string.Empty))
                    return;

                //검색조건저장
                string strPlantCode = this.uTextPlantCode.Text;
                string strStation = "";
                string strLocCode = "";
                string strProcessCode = "";
                string strEquipTypeCode = "";
                string strEquipGroup = "";

                if (this.uComboStation.Items.Count > 0)
                    strStation = this.uComboStation.Value.ToString();

                if(this.uComboEquipLoc.Items.Count > 0)
                    strLocCode = this.uComboEquipLoc.Value.ToString();

                if (this.uComboProcessGroup.Items.Count > 0)
                    strProcessCode = this.uComboProcessGroup.Value.ToString();

                if (this.uComboEquipLargeType.Items.Count > 0)
                    strEquipTypeCode = this.uComboEquipLargeType.Value.ToString();

                if (this.uComboEquipGroup.Items.Count > 0)
                    strEquipGroup = this.uComboEquipGroup.Value.ToString();

                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //설비정보BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                //설비조회매서드 실행
                DataTable dtEquip = clsEquip.mfReadEquip_Info(strPlantCode,strStation,strLocCode,strProcessCode,strEquipTypeCode,strEquipGroup, m_resSys.GetString("SYS_LANG"));

                //데이터바인드
                this.uGridEquipList.DataSource = dtEquip;
                this.uGridEquipList.DataBind();


                if (dtEquip.Rows.Count == 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    
                    DialogResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipList, 0);

                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipWorker), "EquipWorker");
                    QRPMAS.BL.MASEQU.EquipWorker clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker();
                    brwChannel.mfCredentials(clsEquipWorker);

                    DataTable dtWorkerEquip = clsEquipWorker.mfReadEquipWorker_All(strPlantCode, m_resSys.GetString("SYS_LANG"));

                    if (dtWorkerEquip.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtWorkerEquip.Rows.Count; i++)
                        {
                            for (int k = 0; k < this.uGridEquipList.Rows.Count; k++)
                            {
                                if (dtWorkerEquip.Rows[i]["EquipCode"].ToString().Equals(this.uGridEquipList.Rows[k].Cells["EquipCode"].Value.ToString()))
                                {
                                    this.uGridEquipList.Rows[k].Appearance.BackColor = Color.Plum;
                                    break;
                                }
                            }
                        }
                    }
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                }



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region Text

        //입력한 값이 변경시 사용자 명이 초기화된다.
        private void uTextSubUserID_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (!this.uTextSubUserName.Text.Equals(string.Empty))
                {
                    if (this.uGridEquip.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                        {
                            if (this.uGridEquip.Rows[i].Cells["Chk"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                && !this.uGridEquip.Rows[i].Cells["SubUserName"].Value.ToString().Equals(string.Empty))
                            {
                                this.uGridEquip.Rows[i].Cells["SubUserName"].Value = string.Empty;
                            }
                        }
                    }
                    this.uTextSubUserName.Clear();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //엔터키누를 시사용자정보를 조회한다.
        private void uTextSubUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                
                if (e.KeyData == Keys.Enter)
                {
                    //정비사(부) 공장코드 저장
                    string strTechnicianID = this.uTextSubUserID.Text;
                    string strPlantCode = this.uTextPlantCode.Text;

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    if (strPlantCode.Equals(string.Empty))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000652", "M001080", Infragistics.Win.HAlign.Right);
                        this.uTextSubUserID.Clear();
                        return;
                    }

                    //공백 확인
                    if (strTechnicianID.Equals(string.Empty))
                    {

                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextSubUserID.Focus();
                        return;
                    }

                    if (strTechnicianID.Equals(this.uTextUserID.Text))
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001264", "M000056", "M000847", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextSubUserID.Clear();
                        this.uTextSubUserID.Focus();
                        return;
                    }

                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    DataTable dtUser = clsUser.mfReadSYSUser(strPlantCode, strTechnicianID, m_resSys.GetString("SYS_LANG"));

                    if (dtUser.Rows.Count > 0)
                    {
                        this.uTextSubUserName.Text = dtUser.Rows[0]["UserName"].ToString();

                        if (this.uGridEquip.Rows.Count > 0)
                            SearchEquip(strPlantCode, strTechnicianID);
                    }
                    else
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                       "M001264", "M000962", "M000885", Infragistics.Win.HAlign.Right);

                        if(!this.uTextSubUserName.Text.Equals(string.Empty))
                            this.uTextSubUserName.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //버튼 클릭시 사용자 정보 팝업창을 띄운다.
        private void uTextSubUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장정보 저장
                string strPlant = this.uTextPlantCode.Text;

                if (strPlant.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M001081", "M001080", Infragistics.Win.HAlign.Right);
                    return;
                }
                QRPMAS.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보를 보낸다.
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                if (this.uTextUserID.Text.Equals(frmUser.UserID))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                               Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                              "M001264", "M000056", "M000847", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (!frmUser.UserID.Equals(string.Empty) && !frmUser.UserName.Equals(string.Empty))
                {
                    this.uTextSubUserID.Text = frmUser.UserID;
                    this.uTextSubUserName.Text = frmUser.UserName;

                    SearchEquip(strPlant, frmUser.UserID);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #endregion

        
        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                string strDeFautlt = ComboDefaultValue_A();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDeFautlt, "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDeFautlt, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDeFautlt, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboEquipLargeType.Items.Clear();
                    this.uComboEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboEquipLargeType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDeFautlt, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDeFautlt, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDeFautlt, "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// (부)관리자의 설비를 조회하여 부관리자 설비가 존재한다면 이름을 채워준다.
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strUserID"></param>
        private void SearchEquip(string strPlantCode ,string strUserID)
        {
            try
            {

                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //정비사 BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipWorker), "EquipWorker");
                QRPMAS.BL.MASEQU.EquipWorker clsEquipWorker = new QRPMAS.BL.MASEQU.EquipWorker();
                brwChannel.mfCredentials(clsEquipWorker);

                //정비사가 할당하는 설비조회 매서드 실행
                DataTable dtEquipWorker = clsEquipWorker.mfReadEquipWorker(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                string strEquipM = "";

                //정보가 있는 경우 담당하는 설비가 부인 설비를 그리드에 찾아서 (부)관리자 란에 이름을 박는다.
                if (dtEquipWorker.Rows.Count > 0)
                {
                    for (int w = 0; w < dtEquipWorker.Rows.Count; w++)
                    {
                        for (int i = 0; i < this.uGridEquip.Rows.Count; i++)
                        {
                            if (this.uGridEquip.Rows[i].Cells["Chk"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit
                                && !this.uGridEquip.Rows[i].Cells["EquipName"].Value.ToString().Equals(string.Empty))
                            {

                                if (this.uGridEquip.Rows[i].Cells["SubUserName"].Value.ToString().Equals(string.Empty)
                                    && dtEquipWorker.Rows[w]["EquipCode"].ToString().Equals(this.uGridEquip.Rows[i].Cells["EquipCode"].Value.ToString()))
                                {
                                    if (dtEquipWorker.Rows[w]["AdminRight"].ToString().Equals("M"))
                                    {
                                        if(strEquipM.Equals(string.Empty))
                                            strEquipM = dtEquipWorker.Rows[w]["EquipCode"].ToString();
                                        else
                                            strEquipM = strEquipM +","+dtEquipWorker.Rows[w]["EquipCode"].ToString();
                                    }
                                    else
                                        this.uGridEquip.Rows[i].Cells["SubUserName"].Value = this.uTextSubUserName.Text;
                                    //else
                                    //    this.uGridEquip.Rows[i].Cells["SubUserName"].Value = string.Empty;

                                    break;
                                }

                            }
                        }
                    }
                }

                if(!strEquipM.Equals(string.Empty))
                {
                    WinMessageBox msg = new WinMessageBox();
                    DialogResult dr = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000723", strEquipM + "M000697", Infragistics.Win.HAlign.Right);
                    
                }



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        

        #region 주석처리
        //private void uGridEquipList_KeyDown(object sender, KeyEventArgs e)
        //{
        //try
        //{
        //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
        //    QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

        //    bool bolCheckCode = true;

        //    Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
        //    Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs uCell = sender as Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs;

        //    if (e.KeyCode == Keys.Enter)
        //    {
        //        if (uGrid.ActiveCell.Column.Key == "EquipCode")
        //        {
        //            string strEquipCode = uGrid.ActiveCell.Row.Cells["EquipCode"].Text;

        //            brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
        //            QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
        //            brwChannel.mfCredentials(clsEquip);

        //            DataTable dtEquip = clsEquip.mfReadEquipInfoDetail(uTextPlantCode.Text, strEquipCode, m_resSys.GetString("SYS_LANG"));
        //            if (dtEquip.Rows.Count <= 0)
        //            {
        //                uGrid.ActiveCell.Row.Cells["EquipCode"].Value = "";
        //                uGrid.ActiveCell.Row.Cells["EquipName"].Value = "";
        //                uGrid.ActiveCell.Row.Cells["AreaName"].Value = "";
        //                uGrid.ActiveCell.Row.Cells["StationName"].Value = "";
        //                uGrid.ActiveCell.Row.Cells["EquipLocName"].Value = "";
        //                uGrid.ActiveCell.Row.Cells["EquipProcGubunName"].Value = "";
        //            }
        //            else
        //            {
        //                //입력한 설비코드가 리스트에 있는지 체크한다
        //                for (int i = 0; i < uGrid.Rows.Count; i++)
        //                {
        //                    if (strEquipCode == uGrid.Rows[i].Cells["EquipCode"].Value.ToString())
        //                    {
        //                        bolCheckCode = false;
        //                        break;
        //                    }
        //                }
        //                if (bolCheckCode == true)
        //                {
        //                    uGrid.ActiveCell.Row.Cells["EquipCode"].Value = dtEquip.Rows[0]["EquipCode"].ToString();
        //                    uGrid.ActiveCell.Row.Cells["EquipName"].Value = dtEquip.Rows[0]["EquipName"].ToString();
        //                    uGrid.ActiveCell.Row.Cells["AreaName"].Value = dtEquip.Rows[0]["AreaName"].ToString();
        //                    uGrid.ActiveCell.Row.Cells["StationName"].Value = dtEquip.Rows[0]["StationName"].ToString();
        //                    uGrid.ActiveCell.Row.Cells["EquipLocName"].Value = dtEquip.Rows[0]["EquipLocName"].ToString();
        //                    uGrid.ActiveCell.Row.Cells["EquipProcGubunName"].Value = dtEquip.Rows[0]["EquipProcGubunName"].ToString();
        //                }
        //                else
        //                {
        //                    msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        //                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                                         "입력결과", "입력결과", "중복된 설비코드를 입력하였습니다. 다른 설비코드를 입력하세요.",
        //                                         Infragistics.Win.HAlign.Right);
        //                    uGrid.ActiveCell.Row.Cells["EquipCode"].Value = "";
        //                    uGrid.ActiveCell.Row.Cells["EquipName"].Value = "";
        //                    uGrid.ActiveCell.Row.Cells["AreaName"].Value = "";
        //                    uGrid.ActiveCell.Row.Cells["StationName"].Value = "";
        //                    uGrid.ActiveCell.Row.Cells["EquipLocName"].Value = "";
        //                    uGrid.ActiveCell.Row.Cells["EquipProcGubunName"].Value = "";

        //                }
        //            }

        //        }
        //    }
        //}
        //catch (System.Exception ex)
        //{
        //    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //    frmErr.ShowDialog();
        //}
        //finally
        //{
        //}
        //}

        //private void uGridEquipList_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        //{
        //try
        //{
        //    WinMessageBox msg = new WinMessageBox();

        //    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

        //    QRPMAS.UI.frmPOP0005 frmEquip = new frmPOP0005();
        //    frmEquip.PlantCode = uTextPlantCode.Text;
        //    frmEquip.ShowDialog();

        //    if (frmEquip.PlantCode != "" && uTextPlantCode.Text != frmEquip.PlantCode)
        //    {
        //        msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
        //                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
        //                                   "확인창", "입력사항확인", "공장이 맞지 않습니다.", Infragistics.Win.HAlign.Right);

        //        return;
        //    }

        //    uGridEquip.Rows[e.Cell.Row.Index].Cells["EquipCode"].Value = frmEquip.EquipCode;
        //    uGridEquip.Rows[e.Cell.Row.Index].Cells["EquipName"].Value = frmEquip.EquipName;
        //    uGridEquip.Rows[e.Cell.Row.Index].Cells["AreaName"].Value = frmEquip.AreaName;
        //    uGridEquip.Rows[e.Cell.Row.Index].Cells["StationName"].Value = frmEquip.StationName;
        //    uGridEquip.Rows[e.Cell.Row.Index].Cells["EquipLocName"].Value = frmEquip.EquipLocName;
        //    uGridEquip.Rows[e.Cell.Row.Index].Cells["EquipProcGubunName"].Value = frmEquip.EquipProcGubunName;
        //}
        //catch (System.Exception ex)
        //{
        //    QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //    frmErr.ShowDialog();
        //}
        //finally
        //{
        //}

        //}

        //private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        //{
        //    try
        //    {
        //        if (uGroupBoxContentsArea.Expanded == false)
        //        {
        //            Point point = new Point(0, 130);
        //            this.uGroupBoxContentsArea.Location = point;
        //            this.uGridUserList.Height = 45;
        //        }
        //        else
        //        {
        //            Point point = new Point(0, 825);
        //            this.uGroupBoxContentsArea.Location = point;
        //            this.uGridUserList.Height = 740;

        //            for (int i = 0; i < uGridUserList.Rows.Count; i++)
        //            {
        //                uGridUserList.Rows[i].Fixed = false;
        //            }
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
        //        frmErr.ShowDialog();
        //    }
        //    finally
        //    {
        //    }
        //}

        #endregion
    }
}
