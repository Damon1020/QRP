﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0034.cs                                         */
/* 프로그램명   : 불량유형정보등록                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-27                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가참조
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;

namespace QRPMAS.UI
{
    public partial class frmMAS0034 : Form, IToolbar
    {
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0034()
        {
            InitializeComponent();
        }

        private void frmMAS0034_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            titleArea.mfSetLabelText("불량유형정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmMAS0034_Activated(object sender, EventArgs e)
        {
            // ToolBar 활성화
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        #region 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lb = new WinLabel();

                lb.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally 
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo Resoucce
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // BL호출 //
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                WinComboEditor combo = new WinComboEditor();
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue("A", m_resSys.GetString("SYS_LANG")), "PlantCode", "PlantName"
                    , dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        public void InitGrid()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridInspectFaultType, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼추가
                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "Check", "선택", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 50, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "PlantCode", "공장", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "InspectFaultGubun", "검사불량유형", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, true, false, 1
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "InspectFaultTypeCode", "불량유형코드", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "InspectFaultTypeName", "불량유형명", true, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "InspectFaultTypeNameCh", "불량유형명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "InspectFaultTypeNameEn", "불량유형명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "ProcessGroup", "공정그룹", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "LocationOrMaterial", "LocationOrMaterial", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "QCNFlag", "QCN여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 110, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "ParentInspectFaultTypeCode", "상위불량유형코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 130, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridInspectFaultType, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                // Set FontSize
                this.uGridInspectFaultType.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridInspectFaultType.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 입력을 위한 공백줄 추가
                wGrid.mfAddRowGrid(this.uGridInspectFaultType, 0);

                // DropDown 설정
                // Plant
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridInspectFaultType, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", ComboDefaultValue("S", m_resSys.GetString("SYS_LANG")), dtPlant);

                // UseFlag
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);

                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridInspectFaultType, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                // 검사불량유형구분
                DataTable dtFaultType = clsComCode.mfReadCommonCode("C0052", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridInspectFaultType, 0, "InspectFaultGubun", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtFaultType);

                // QCNFlag
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserCommonCode), "UserCommonCode");
                QRPSYS.BL.SYSPGM.UserCommonCode clsUComCode = new QRPSYS.BL.SYSPGM.UserCommonCode();
                brwChannel.mfCredentials(clsUComCode);

                DataTable dtQCNFlag = clsUComCode.mfReadUserCommonCode("QUA", "U0001", m_resSys.GetString("SYS_LANG"));
                wGrid.mfSetGridColumnValueList(this.uGridInspectFaultType, 0, "QCNFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtQCNFlag);                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">S : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun, string strLang)
        {
            try
            {
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                strLang = strLang.ToUpper();
                if (strGubun.Equals("S"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }


        #endregion

        #region ToolBar...
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsIFType = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsIFType);

                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                // 함수호출
                DataTable dt = clsIFType.mfReadInspectFaultType(strPlantCode);
                // DataBinding
                this.uGridInspectFaultType.DataSource = dt;
                this.uGridInspectFaultType.DataBind();

                WinGrid grd = new WinGrid();
                grd.mfSetAutoResizeColWidth(this.uGridInspectFaultType, 0);


                // PK 편집 불가 상태로
                for (int i = 0; i < this.uGridInspectFaultType.Rows.Count; i++)
                {
                    this.uGridInspectFaultType.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                    this.uGridInspectFaultType.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"].Appearance.BackColor = Color.Gainsboro;
                    this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                }

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 그리드에 데이터가 있을시
                if (this.uGridInspectFaultType.Rows.Count > 0)
                {
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    DataTable dtProcessGroup = new DataTable();
                    WinGrid wGrid = new WinGrid();

                    // 공정그룹
                    dtProcessGroup = clsProcess.mfReadMASProcessGroup(this.uComboSearchPlant.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    wGrid.mfSetGridColumnValueList(this.uGridInspectFaultType, 0, "ProcessGroup", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtProcessGroup);
                }

                DialogResult DResult = new DialogResult();
                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // 변수 및 인스턴스객체 생성
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                QRPMAS.BL.MASQUA.InspectFaultType clsIFType = new QRPMAS.BL.MASQUA.InspectFaultType();
                brwChannel.mfCredentials(clsIFType);

                DataTable dtInspectFaultType = clsIFType.mfSetDataInfo();

                if(this.uGridInspectFaultType.Rows.Count > 0)
                    this.uGridInspectFaultType.ActiveCell = this.uGridInspectFaultType.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");

                for (int i = 0; i < this.uGridInspectFaultType.Rows.Count; i++)
                {
                    // 셀수정이 일어난 행만 저장
                    if (this.uGridInspectFaultType.Rows[i].RowSelectorAppearance.Image != null)
                    {
                        // 필수입력사항 확인
                        if (this.uGridInspectFaultType.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , this.uGridInspectFaultType.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000560",strLang)
                                            , Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridInspectFaultType.ActiveCell = this.uGridInspectFaultType.Rows[i].Cells["PlantCode"];
                            this.uGridInspectFaultType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridInspectFaultType.Rows[i].Cells["InspectFaultGubun"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , this.uGridInspectFaultType.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000553",strLang)
                                            , Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridInspectFaultType.ActiveCell = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultGubun"];
                            this.uGridInspectFaultType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                            return;
                        }
                        else if (this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString().Trim() == string.Empty)
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , this.uGridInspectFaultType.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000568",strLang)
                                            , Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridInspectFaultType.ActiveCell = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"];
                            this.uGridInspectFaultType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else if (this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeName"].Value.ToString() == "")
                        {
                            msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , this.uGridInspectFaultType.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000567",strLang)
                                            , Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridInspectFaultType.ActiveCell = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeName"];
                            this.uGridInspectFaultType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                        else
                        {
                            DataTable dtCheckPK = clsIFType.mfReadInspectFaultType_QCNFlag(this.uGridInspectFaultType.Rows[i].Cells["PlantCode"].Value.ToString(), this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString());

                            if (dtCheckPK.Rows.Count != 0 && this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"].Activation == Infragistics.Win.UltraWinGrid.Activation.AllowEdit)
                            {
                                msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001122",strLang)
                                            , this.uGridInspectFaultType.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000505",strLang)
                                            , Infragistics.Win.HAlign.Right);

                                this.uGridInspectFaultType.ActiveCell = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"];
                                this.uGridInspectFaultType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else
                            {
                                DataRow row = dtInspectFaultType.NewRow();
                                row["PlantCode"] = this.uGridInspectFaultType.Rows[i].Cells["PlantCode"].Value.ToString();
                                row["InspectFaultGubun"] = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultGubun"].Value.ToString();
                                row["InspectFaultTypeCode"] = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString().ToUpper();
                                row["InspectFaultTypeName"] = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeName"].Value.ToString();
                                row["InspectFaultTypeNameCh"] = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeNameCh"].Value.ToString().Trim() == string.Empty ? 
                                                                this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeName"].Value.ToString().ToUpper() : this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeNameCh"].Value.ToString().ToUpper();
                                row["InspectFaultTypeNameEn"] = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeNameEn"].Value.ToString().Trim() == string.Empty ?
                                                                this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeName"].Value.ToString().ToUpper() : this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeNameEn"].Value.ToString().ToUpper();
                                row["ProcessGroup"] = this.uGridInspectFaultType.Rows[i].Cells["ProcessGroup"].Value.ToString();
                                row["LocationOrMaterial"] = this.uGridInspectFaultType.Rows[i].Cells["LocationOrMaterial"].Value.ToString();
                                row["QCNFlag"] = this.uGridInspectFaultType.Rows[i].Cells["QCNFlag"].Value.ToString();
                                row["ParentInspectFaultTypeCode"] = this.uGridInspectFaultType.Rows[i].Cells["ParentInspectFaultTypeCode"].Value.ToString();
                                row["UseFlag"] = this.uGridInspectFaultType.Rows[i].Cells["UseFlag"].Value.ToString();
                                dtInspectFaultType.Rows.Add(row);

                                for (int k = 0; k < dtInspectFaultType.Rows.Count; k++)
                                {
                                    for (int j = 0; j < dtInspectFaultType.Rows.Count; j++)
                                    {
                                        if (k != j)
                                        {
                                            if (dtInspectFaultType.Rows[k]["InspectFaultTypeCode"].ToString().ToUpper() == dtInspectFaultType.Rows[j]["InspectFaultTypeCode"].ToString().ToUpper())
                                            {
                                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                                                    , "M001135", "M001122", "M000911", Infragistics.Win.HAlign.Right);

                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (dtInspectFaultType.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                                , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread t1 = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                        this.MdiParent.Cursor = Cursors.WaitCursor;

                        // 처리 로직 //
                        // 저장함수 호출
                        String rtMSG = clsIFType.mfSaveInspectFaultType(dtInspectFaultType, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // Decoding //
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                        // 처리로직 끝//

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 처리결과에 따른 메세지 박스
                        System.Windows.Forms.DialogResult Dresult;
                        if (ErrRtn.ErrNum == 0)
                        {
                            Dresult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                            mfSearch();
                        }
                        else
                        {
                            string strMes = "";
                            if (ErrRtn.ErrMessage.Equals(string.Empty))
                                strMes = msg.GetMessge_Text("M000953", strLang);
                            else
                                strMes = ErrRtn.ErrMessage;

                            Dresult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMes,
                                                Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
            try
            {
                //if (datagrid.IsCurrentCellInEditMode)
                //{
                //    datagrid.EndEdit(DataGridViewDataErrorContexts.Commit);
                //}

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();
                this.uGridInspectFaultType.ActiveCell = this.uGridInspectFaultType.Rows[0].Cells[0];
                int check = 0;
                for (int i = 0; i < this.uGridInspectFaultType.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridInspectFaultType.Rows[i].Cells["Check"].Value) == true)
                    {
                        check++;
                    }
                }
                if (check == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                                                        , "M001264", "M001228", "M000641", Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {

                    DResult = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Default
                                                                            , "M001264", "M000650", "M000675", Infragistics.Win.HAlign.Right);

                    if (DResult == DialogResult.Yes)
                    {
                        QRPBrowser brwChnnel = new QRPBrowser();
                        brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.InspectFaultType), "InspectFaultType");
                        QRPMAS.BL.MASQUA.InspectFaultType clsFault = new QRPMAS.BL.MASQUA.InspectFaultType();
                        brwChnnel.mfCredentials(clsFault);

                        DataTable dtDelFault = clsFault.mfSetDataInfo();
                        DataRow dr;
                        for (int i = 0; i < this.uGridInspectFaultType.Rows.Count; i++)
                        {
                            if (Convert.ToBoolean(this.uGridInspectFaultType.Rows[i].Cells["Check"].Value) == true)
                            {
                                dr = dtDelFault.NewRow();
                                dr["PlantCode"] = this.uGridInspectFaultType.Rows[i].Cells["PlantCode"].Value.ToString();
                                dr["InspectFaultTypeCode"] = this.uGridInspectFaultType.Rows[i].Cells["InspectFaultTypeCode"].Value.ToString();

                                dtDelFault.Rows.Add(dr);
                            }
                        }
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread threadPop = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", m_resSys.GetString("SYS_LANG")));
                        this.MdiParent.Cursor = Cursors.WaitCursor; 

                        string rtMSG = clsFault.mfDeleteInspectFaultType(dtDelFault);

                        //Decoding
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);

                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                          "M001135", "M000638", "M000926",
                                          Infragistics.Win.HAlign.Right);
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                          "M001135", "M000638", "M000923",
                                          Infragistics.Win.HAlign.Right);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfCreate()
        {
        }
        public void mfPrint()
        {
        }
        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                if (this.uGridInspectFaultType.Rows.Count == 0)
                    return;

                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(this.uGridInspectFaultType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region Events...
        // 셀 수정시 RowSelector 이미지 변경
        private void uGridInspectFaultType_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀 수정시 RowSelector 이미지 변화
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        // 그리드 업데이트 이벤트
        private void uGridInspectFaultType_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 자동 행삭제
                QRPCOM.QRPUI.WinGrid wGrid = new WinGrid();
                if (wGrid.mfCheckCellDataInRow(this.uGridInspectFaultType, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                QRPBrowser brwChannel = new QRPBrowser();

                // 공장코드 변경시
                if (e.Cell.Column.Key.Equals("PlantCode"))
                {
                    DataTable dtProcessGroup = new DataTable();
                    // 공장코드가 공백이 아닐시
                    if (!e.Cell.Value.ToString().Equals(string.Empty))
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                        QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                        brwChannel.mfCredentials(clsProcess);

                        // 메소드 호출
                        dtProcessGroup = clsProcess.mfReadMASProcessGroup(e.Cell.Value.ToString(), m_resSys.GetString("SYS_LANG"));
                    }

                    // 공정그룹 적용
                    wGrid.mfSetGridCellValueList(this.uGridInspectFaultType, e.Cell.Row.Index, "ProcessGroup", "", "선택", dtProcessGroup);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void frmMAS0034_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }
    }
}
