﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMASZ0010.cs                                        */
/* 프로그램명   : 설비구성품정보등록                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMASZ0010 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();
        string m_strPlantCode = "";


        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        public frmMASZ0010()
        {
            InitializeComponent();
        }
         private void frmMASZ0010_Activated(object sender, EventArgs e)
        {
            //툴바활성화
            
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        

        private void frmMASZ0010_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //타이틀설정
            titleArea.mfSetLabelText("설비구성품정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);
            //컨트롤초기화
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitButton();
            InitGroupBox();
            InitText();
            // ContentGroupBox 닫힘상태로
            this.uGroupBoxContentsArea.Expanded = false;
        }
        #region 컨트롤초기화

        private void frmMASZ0010_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                //레이블설정
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelSearchEquipType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelEquipCode, "설비코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelEquipName, "설비명", m_resSys.GetString("SYS_FONTNAME"), true, false);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                //기본정보
                //--그리드기본설정
                grd.mfInitGeneralGrid(this.uGridEquip, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //--컬럼설정
                grd.mfSetGridColumn(this.uGridEquip, 0, "PlantCode", "공장코드",false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "AreaName", "Area", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipProcGubunName", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 15, Infragistics.Win.HAlign.Left,
                   Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "SuperEquipCode", "Super설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "ModelName", "모델", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "SerialNo", "SerialNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipTypeName", "설비유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipGroupName", "설비그룹명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 20, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "VendorName", "Vendor", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "GRDate", "STS입고일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "MakeYear", "제작년도", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "EquipLevelCode", "설비등급", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquip, 0, "On-Line적용여부", "On-Line적용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 15, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--Grid초기화 후 Font크기를 아래와 같이 적용

                this.uGridEquip.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquip.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                
                //빈줄추가
                //grd.mfAddRowGrid(this.uGridEquip, 0);

                //설비정보
                //--그리드기본설정
                grd.mfInitGeneralGrid(this.uGridEquipBOM, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None, true,
                    Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button,
                    Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //--컬럼설정
                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "CommonCode", "설비구성품구분", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "DurableMatCode", "구성품코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "DurableMatName", "구성품명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, true, false, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "SerialFlag", "Lot구성여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 10, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "F");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 40, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "LotReturn", "Lot반납", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit,100, false, false, 0, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "DurableInventoryCode", "출고창고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 40, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                
                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "InputQty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "#,###", "nnnnnnnnn", "0");
                
                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "UnitCode", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 80, false, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "EA");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 170, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipBOM, 0, "DataFlag", "데이터여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 170, false, true, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //--Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridEquipBOM.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipBOM.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridEquipBOM.DisplayLayout.Bands[0].Columns["LotReturn"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                //빈줄추가
                grd.mfAddRowGrid(this.uGridEquipBOM, 0);

                //채널연결

                //설비구성품구분 콤보
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsSYS = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsSYS);

                DataTable dtCommon = clsSYS.mfReadCommonCode("C0015", m_resSys.GetString("SYS_LANG"));
                grd.mfSetGridColumnValueList(this.uGridEquipBOM, 0, "CommonCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtCommon);

                //단위콤보
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit clsUnit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(clsUnit);

                DataTable dtUnit = clsUnit.mfReadUnit("", m_resSys.GetString("SYS_LANG"));
                grd.mfSetGridColumnValueList(this.uGridEquipBOM, 0, "UnitCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtUnit);


                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void InitGroupBox()
        {
            try
            {
                WinGroupBox gb = new WinGroupBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //  GroupBox
                gb.mfSetGroupBox(this.uGroupBoxContentsArea, GroupBoxType.INFO, "상세정보", m_resSys.GetString("SYS_FONTNAME")
                    , Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default, Infragistics.Win.Misc.GroupBoxBorderStyle.Rounded
                    , Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default, Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);


                //폰트설정
                uGroupBoxContentsArea.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxContentsArea.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;



                // ExtandableGroupBox 설정
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // SearchArea Plant ComboBox

                //--- 공장 ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue_All()
                    , "PlantCode", "PlantName", dtPlant);

                //this.uComboArea.Items.Clear();
                //this.uComboArea.Text = "";

                //this.uComboStation.Items.Clear();
                //this.uComboStation.Text = "";

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void InitText()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                
                this.uTextEquipCode.ReadOnly = false;
                this.uTextEquipName.ReadOnly = false;

                this.uTextEquipCode.Text = "";
                this.uTextEquipName.Text = "";          

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System Resourceinfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonDeleteRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        #endregion

        #region 툴바
        public void mfSearch()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                // BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                //검색조건
                string strPlantCode = uComboSearchPlant.Value.ToString();
                string strStation = uComboSearchStation.Value.ToString();
                string strEquipLoc = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipLargeType = this.uComboSearchEquipType.Value.ToString();
                string strEquipGroup = this.uComboSearchEquipGroup.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;


                // Call Method
                DataTable dtEquip = clsEquip.mfReadEquip_Info(strPlantCode,strStation,strEquipLoc,strProcessGroup,strEquipLargeType,strEquipGroup, m_resSys.GetString("SYS_LANG"));

                //테이터바인드
                uGridEquip.DataSource = dtEquip;
                uGridEquip.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);


                if(dtEquip.Rows.Count == 0)
                {/* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquip, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfSave()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataTable dtEquipDurableBOM = new DataTable();
                DataTable dtEquipSPBOM = new DataTable();
                DataRow row;
                DialogResult DResult = new DialogResult();

                int intRowCheck = 0;
                int intSeq = 0;


                // 필수입력사항 확인
                if (this.uTextEquipCode.Text == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M001020", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                string strLang = m_resSys.GetString("SYS_LANG");

                if(this.uGridEquipBOM.Rows.Count > 0)
                    this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[0].Cells[0];

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                {
                    if (this.uGridEquipBOM.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;
                        string strRowNum = this.uGridEquipBOM.Rows[i].RowSelectorNumber.ToString();

                        #region 필수입력사항

                        if (this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , strRowNum + msg.GetMessge_Text("M000509",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[i].Cells["CommonCode"];
                            this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , strRowNum + msg.GetMessge_Text("M000496",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"];
                            this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        //if (this.uGridEquipBOM.Rows[i].Cells["DurableMatName"].Value.ToString() == "")
                        //{
                        //    DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        //                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                        //                    , strRowNum + msg.GetMessge_Text("M000493",strLang), Infragistics.Win.HAlign.Center);

                        //    // Focus Cell
                        //    this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[i].Cells["DurableMatName"];
                        //    this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                        //    return;
                        //}

                        if (this.uGridEquipBOM.Rows[i].Cells["InputQty"].Value.ToString() == "0")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , strRowNum + msg.GetMessge_Text("M000510",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[i].Cells["InputQty"];
                            this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridEquipBOM.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , strRowNum + msg.GetMessge_Text("M000499",strLang), Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[i].Cells["UnitCode"];
                            this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGridEquipBOM.Rows[i].Cells["SerialFlag"].Value.ToString() == "T")
                        {
                            if (this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , strRowNum + msg.GetMessge_Text("M000466",strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[i].Cells["LotNo"];
                                this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                        }

                        if (Convert.ToBoolean(uGridEquipBOM.Rows[i].Cells["LotReturn"].Value) == true) 
                        {
                            if (this.uGridEquipBOM.Rows[i].Cells["DurableInventoryCode"].Value.ToString() == "")
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang)
                                            , strRowNum + msg.GetMessge_Text("M000528",strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[i].Cells["DurableInventoryCode"];
                                this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                        }
                        //}
                        #endregion

                    }
                }

                if (intRowCheck == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000398", Infragistics.Win.HAlign.Center);

                    // Focus
                    return;
                }

                if(this.uGridEquipBOM.Rows.Count > 0)
                    this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[0].Cells[0];

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                {
                    if (this.uGridEquipBOM.Rows[i].Hidden == false)
                    {
                        if (this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString() == "SP")
                        {
                            for (int k = i + 1; k < this.uGridEquipBOM.Rows.Count; k++)
                            {
                                if (this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString() == this.uGridEquipBOM.Rows[k].Cells["DurableMatCode"].Value.ToString())
                                {
                                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M001228", "M001121", Infragistics.Win.HAlign.Center);

                                    // Focus
                                    this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[k].Cells["DurableMatCode"];
                                    this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                    return;
                                }
                            }
                        }
                    }
                }

                if(this.uGridEquipBOM.Rows.Count > 0)
                    this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[0].Cells[0];

                for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++) 
                {
                    
                    if (this.uGridEquipBOM.Rows[i].Hidden == false)
                    {
                        if (this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString() == "TL")
                        {
                            if (this.uGridEquipBOM.Rows[i].Cells["SerialFlag"].Value.ToString() == "T")
                            {
                                for (int k = i + 1; k < this.uGridEquipBOM.Rows.Count; k++)
                                {
                                    if (this.uGridEquipBOM.Rows[k].Hidden == false)
                                    {
                                        if (this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString() == this.uGridEquipBOM.Rows[k].Cells["LotNo"].Value.ToString())
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , "M001264", "M001228", "M001119", Infragistics.Win.HAlign.Center);

                                            // Focus
                                            this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[k].Cells["LotNo"];
                                            this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int k = i + 1; k < this.uGridEquipBOM.Rows.Count; k++)
                                {
                                    if (this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString() == this.uGridEquipBOM.Rows[k].Cells["DurableMatCode"].Value.ToString())
                                    {
                                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001228", "M001121", Infragistics.Win.HAlign.Center);

                                        // Focus
                                        this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[k].Cells["DurableMatCode"];
                                        this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }

                

                DialogResult dir = new DialogResult();


                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000876"
                                            , Infragistics.Win.HAlign.Right);



                if (dir != DialogResult.No)
                {
                    


                    //BL 호출 : 금형/치공구 저장을 위한 데이터테이블 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                    QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                    brwChannel.mfCredentials(clsEquipDurableBOM);

                    dtEquipDurableBOM = clsEquipDurableBOM.mfSetDatainfo();

                    for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                    {
                        if (this.uGridEquipBOM.Rows[i].Hidden == false)
                        {
                            if (this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString() == "TL")
                            {
                                if (Convert.ToBoolean(uGridEquipBOM.Rows[i].Cells["LotReturn"].Value) == false)     //Lot반납에 체크가 되어있으면 행삭제로 간주함
                                {
                                    intSeq = intSeq + 1;

                                    row = dtEquipDurableBOM.NewRow();
                                    row["PlantCode"] = m_strPlantCode;
                                    row["EquipCode"] = this.uTextEquipCode.Text;
                                    row["Seq"] = intSeq;
                                    row["DurableMatCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString();
                                    row["LotNo"] = this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString();
                                    row["InputQty"] = this.uGridEquipBOM.Rows[i].Cells["InputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGridEquipBOM.Rows[i].Cells["UnitCode"].Value.ToString();
                                    row["EtcDesc"] = this.uGridEquipBOM.Rows[i].Cells["EtcDesc"].Value.ToString();
                                    dtEquipDurableBOM.Rows.Add(row);
                                }
                            }
                        }
                    }

                    intSeq = 0;

                    //BL 호출 : SparePart 저장을 위한 데이터테이블 호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipSPBOM), "EquipSPBOM");
                    QRPMAS.BL.MASEQU.EquipSPBOM clsEquipSPBOM = new QRPMAS.BL.MASEQU.EquipSPBOM();
                    brwChannel.mfCredentials(clsEquipSPBOM);

                    dtEquipSPBOM = clsEquipSPBOM.mfSetDatainfo();

                    for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                    {
                        if (this.uGridEquipBOM.Rows[i].Hidden == false)
                        {
                            if (this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString() == "SP")
                            {
                                intSeq = intSeq + 1;

                                row = dtEquipSPBOM.NewRow();
                                row["PlantCode"] = m_strPlantCode;
                                row["EquipCode"] = this.uTextEquipCode.Text;
                                row["Seq"] = intSeq;
                                row["SparePartCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString();
                                row["InputQty"] = this.uGridEquipBOM.Rows[i].Cells["InputQty"].Value.ToString();
                                row["UnitCode"] = this.uGridEquipBOM.Rows[i].Cells["UnitCode"].Value.ToString();
                                row["EtcDesc"] = this.uGridEquipBOM.Rows[i].Cells["EtcDesc"].Value.ToString();
                                dtEquipSPBOM.Rows.Add(row);
                            }
                        }
                    }

                    //BL 호출 : 치공구 Lot이 있는 경우 재고에서 (-) 처리 하기 위한 데이터 테이블 호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    //BL 호출 : 치공구 Lot이 있는 경우 재고에서 (-) 이력을 남기기 위한 데이터 테이블 호출
                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStockMoveHist), "DurableStockMoveHist");
                    QRPDMM.BL.DMMICP.DurableStockMoveHist clsDurableStockMoveHist = new QRPDMM.BL.DMMICP.DurableStockMoveHist();
                    brwChannel.mfCredentials(clsDurableStockMoveHist);


                    DataTable dtDurableStock = clsDurableStock.mfSetDatainfo();

                    DataTable dtDurableStockMoveHist = clsDurableStockMoveHist.mfSetDatainfo();

                    string strNow = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    string strUserID = m_resSys.GetString("SYS_USERID");
                    string strUserIP = m_resSys.GetString("SYS_USERIP");
                    for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                    {
                        if (this.uGridEquipBOM.Rows[i].Hidden == false &&
                            this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString() == "TL")
                        {
                            // 새로 입력한 데이터
                            if (this.uGridEquipBOM.Rows[i].Cells["DataFlag"].Value.ToString() != "T")   
                            {
                                if (this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString() != "" &&
                                    this.uGridEquipBOM.Rows[i].Cells["DurableInventoryCode"].Value.ToString() != "")
                                {
                                    row = dtDurableStock.NewRow();

                                    row["PlantCode"] = m_strPlantCode;
                                    row["DurableInventoryCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                                    row["DurableMatCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString();
                                    row["Qty"] = "-" + this.uGridEquipBOM.Rows[i].Cells["InputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGridEquipBOM.Rows[i].Cells["UnitCode"].Value.ToString();
                                    row["LotNo"] = this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString();

                                    dtDurableStock.Rows.Add(row);


                                    //이력정보
                                    row = dtDurableStockMoveHist.NewRow();
                                    row["MoveGubunCode"] = "M03";
                                    row["DocCode"] = "";
                                    row["MoveDate"] = strNow;
                                    row["MoveChargeID"] = strUserID;
                                    row["PlantCode"] = m_strPlantCode;
                                    row["ProductCode"] = "";
                                    row["DurableInventoryCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                                    row["LotNo"] = this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString();
                                    row["EquipCode"] = this.uTextEquipCode.Text;
                                    row["DurableMatCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString();
                                    row["MoveQty"] = "-" + this.uGridEquipBOM.Rows[i].Cells["InputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGridEquipBOM.Rows[i].Cells["UnitCode"].Value.ToString();
                                    row["UserIP"] = strUserIP;
                                    row["UserID"] = strUserID;

                                    dtDurableStockMoveHist.Rows.Add(row);

                                }
                            }

                            //기존의 데이터에서 LotReturn 에 체크한 경우
                            else
                            {
                                if (this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString() != "" &&
                                    this.uGridEquipBOM.Rows[i].Cells["DurableInventoryCode"].Value.ToString() != "" &&
                                    Convert.ToBoolean(uGridEquipBOM.Rows[i].Cells["LotReturn"].Value) == true)
                                {
                                    row = dtDurableStock.NewRow();

                                    row["PlantCode"] = m_strPlantCode;
                                    row["DurableInventoryCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                                    row["DurableMatCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString();
                                    row["Qty"] = this.uGridEquipBOM.Rows[i].Cells["InputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGridEquipBOM.Rows[i].Cells["UnitCode"].Value.ToString();
                                    row["LotNo"] = this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString();

                                    dtDurableStock.Rows.Add(row);

                                    //이력정보
                                    row = dtDurableStockMoveHist.NewRow();

                                    row["MoveGubunCode"] = "M04";
                                    row["DocCode"] = "";
                                    row["MoveDate"] = strNow;
                                    row["MoveChargeID"] = strUserID;
                                    row["PlantCode"] = m_strPlantCode;
                                    row["ProductCode"] = "";
                                    row["DurableInventoryCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                                    row["LotNo"] = this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString();
                                    row["EquipCode"] = this.uTextEquipCode.Text;
                                    row["DurableMatCode"] = this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString();
                                    row["MoveQty"] = this.uGridEquipBOM.Rows[i].Cells["InputQty"].Value.ToString();
                                    row["UnitCode"] = this.uGridEquipBOM.Rows[i].Cells["UnitCode"].Value.ToString();
                                    row["UserIP"] = strUserIP;
                                    row["UserID"] = strUserID;

                                    dtDurableStockMoveHist.Rows.Add(row);
                                }
                            }
                        }                        
                    }



                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //BL 호출 : 금형치공구, SparePart 저장을 위한 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                    QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                    brwChannel.mfCredentials(clsEquip);

                    // 처리 로직 //
                    // 저장함수 호출
                    string rtMSG = clsEquip.mfSaveEquipBOM(dtEquipDurableBOM, 
                                                            dtEquipSPBOM, 
                                                            dtDurableStock,
                                                            m_strPlantCode, 
                                                            this.uTextEquipCode.Text, 
                                                            dtDurableStockMoveHist,
                                                            strUserIP, 
                                                            strUserID);

                    // Decoding //
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                    // 처리로직 끝//

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 처리결과에 따른 메세지 박스
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000930",
                                            Infragistics.Win.HAlign.Right);
                        mfCreate();
                        mfSearch();

                        ////SaveInspectType();
                    }
                    else
                    {
                        string strMsg = "";

                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMsg = msg.GetMessge_Text("M000953", strLang);
                        else
                            strMsg = ErrRtn.ErrMessage;
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500
                                                      , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                      , msg.GetMessge_Text("M001135", strLang)
                                                      , msg.GetMessge_Text("M001037", strLang), strMsg
                                                      , Infragistics.Win.HAlign.Right);
                    }

                    
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
        }
        public void mfCreate()
        {
            try
            {
                //InitGrid();
                if (this.uGridEquipBOM.Rows.Count > 0)
                {
                    this.uGridEquipBOM.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipBOM.Rows.All);
                    this.uGridEquipBOM.DeleteSelectedRows(false);
                }
                InitText();
                //InitGroupBox();
                m_strPlantCode = "";
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquip.Rows.Count == 0 && (this.uGridEquipBOM.Rows.Count == 0 || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000811", "M000805", Infragistics.Win.HAlign.Right);
                    return;
                }

                //처리 로직//
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                if(this.uGridEquip.Rows.Count > 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquip);

                if (this.uGridEquipBOM.Rows.Count > 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridEquipBOM);
                /////////////
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }
        #endregion

        #region 이벤트
        public void mfPrint()
        {
        }
        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridEquip, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 170);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquip.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquip.Height = 740;
                    for (int i = 0; i < uGridEquip.Rows.Count; i++)
                    {
                        uGridEquip.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
        #endregion

        private void uButtonDeleteRow_Click(object sender, EventArgs e)
        {
            // 행삭제 이벤트
            try
            {
                for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridEquipBOM.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGridEquipBOM.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }      

        

        #region 그리드 이벤트

        private void uGridEquip_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                InitText();
                m_strPlantCode = "";

                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                // ExtandableGroupBox 설정
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    
                }

                e.Cell.Row.Fixed = true;

                this.uTextEquipCode.Text = e.Cell.Row.Cells["EquipCode"].Value.ToString();
                this.uTextEquipName.Text = e.Cell.Row.Cells["EquipName"].Value.ToString();
                m_strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInventory = clsDurableInventory.mfReadDurableInventoryCombo(m_strPlantCode, m_resSys.GetString("SYS_LANG"));
                grd.mfSetGridColumnValueList(this.uGridEquipBOM, 0, "DurableInventoryCode", Infragistics.Win.ValueListDisplayStyle.DisplayText
                    , "", "", dtDurableInventory);


                //----------- 상세 그리드 바인딩--------------//
                //BL호출                
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipDurableBOM), "EquipDurableBOM");
                QRPMAS.BL.MASEQU.EquipDurableBOM clsEquipDurableBOM = new QRPMAS.BL.MASEQU.EquipDurableBOM();
                brwChannel.mfCredentials(clsEquipDurableBOM);

                //매서드호출
                DataTable dtEquipBOM = clsEquipDurableBOM.mfReadEquipDurableBOM_EquipSTBOM(m_strPlantCode, this.uTextEquipCode.Text, m_resSys.GetString("SYS_LANG"));


                //테이터바인드
                uGridEquipBOM.DataSource = dtEquipBOM;
                uGridEquipBOM.DataBind();


                uGridEquipBOM_AfterDataBinding();
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEquipBOM_AfterDataBinding()
        {
            try
            {
                for (int i = 0; i < uGridEquipBOM.Rows.Count; i++)
                {
                    uGridEquipBOM.DisplayLayout.Rows[i].Appearance.BackColor = Color.Yellow;

                    if (this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString() != "")
                    {
                        for (int j = 0; j < uGridEquipBOM.Rows[i].Cells.Count; j++)
                        {
                            if (uGridEquipBOM.Rows[i].Cells[j].Column.Key.ToString() == "LotReturn" ||
                                uGridEquipBOM.Rows[i].Cells[j].Column.Key.ToString() == "DurableInventoryCode"||
                                uGridEquipBOM.Rows[i].Cells[j].Column.Key.ToString() == "UnitCode")
                            {
                                uGridEquipBOM.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            }
                            else
                            {
                                uGridEquipBOM.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0; j < uGridEquipBOM.Rows[i].Cells.Count; j++)
                        {
                            if (uGridEquipBOM.Rows[i].Cells[j].Column.Key.ToString() == "Check"||
                                uGridEquipBOM.Rows[i].Cells[j].Column.Key.ToString() == "UnitCode")
                            {
                                uGridEquipBOM.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                            }
                            else
                            {
                                uGridEquipBOM.Rows[i].Cells[j].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }

                        }
                    }
                }

                if (uGridEquipBOM.Rows.Count > 0)
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipBOM, 0);
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEquipBOM_KeyDown(object sender, KeyEventArgs e)
        {
            ////// 행삭제 이벤트
            ////try
            ////{
            ////    // SystemInfo ResourceSet
            ////    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            ////    WinMessageBox msg = new WinMessageBox();

            ////    Infragistics.Win.UltraWinGrid.UltraGrid uGrid = sender as Infragistics.Win.UltraWinGrid.UltraGrid;
            ////    Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs uCell = sender as Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs;

            ////    if (e.KeyCode == Keys.Enter)
            ////    {
            ////        if (uGrid.ActiveCell.Column.Key == "DurableMatCode")
            ////        {
            ////            string strCommonCode = uGrid.ActiveCell.Row.Cells["CommonCode"].Value.ToString();
            ////            string strDurableMatCode = uGrid.ActiveCell.Row.Cells["DurableMatCode"].Text;

            ////            //--------------------------- 금형/치공구 일 경우는 MASDurableMat 에서 코드값 확인------------------------///
            ////            if (strCommonCode == "TL")
            ////            {                            
                            
            ////                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
            ////                QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
            ////                brwChannel.mfCredentials(clsDurableMat);

            ////                DataTable dtDurableMat = clsDurableMat.mfReadDurableMatDetail(m_strPlantCode, strDurableMatCode,  m_resSys.GetString("SYS_LANG"));

            ////                if (dtDurableMat.Rows.Count > 0)
            ////                {
            ////                    uGrid.ActiveCell.Row.Cells["DurableMatName"].Value = dtDurableMat.Rows[0]["DurableMatName"].ToString();
            ////                }
            ////                else
            ////                {
            ////                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, "굴림", 500, 500,
            ////                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////                                "확인창", "입력확인", "올바른 코드를 입력하세요",
            ////                                Infragistics.Win.HAlign.Right);
            ////                    uGrid.ActiveCell.Row.Cells["DurableMatName"].Value = "";
            ////                    uGrid.ActiveCell.Row.Cells["DurableMatCode"].Value = "";
            ////                }
            ////            }


            ////            //--------------------------- SparePart 일 경우는 MASSparePart 에서 코드값 확인------------------------///
            ////            else if (strCommonCode == "SP")
            ////            {
            ////                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.SparePart), "SparePart");
            ////                QRPMAS.BL.MASEQU.SparePart clsSparePart = new QRPMAS.BL.MASEQU.SparePart();
            ////                brwChannel.mfCredentials(clsSparePart);

            ////                DataTable dtSparePart = clsSparePart.mfReadSparePartDetail(m_strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

            ////                if (dtSparePart.Rows.Count > 0)
            ////                {
            ////                    uGrid.ActiveCell.Row.Cells["DurableMatName"].Value = dtSparePart.Rows[0]["SparePartName"].ToString();
            ////                }
            ////                else
            ////                {
            ////                    DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, "굴림", 500, 500,
            ////                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////                                "확인창", "입력확인", "올바른 코드를 입력하세요",
            ////                                Infragistics.Win.HAlign.Right);
            ////                    uGrid.ActiveCell.Row.Cells["DurableMatName"].Value = "";
            ////                    uGrid.ActiveCell.Row.Cells["DurableMatCode"].Value = "";
            ////                }
            ////            }

            ////            else 
            ////            {
                           
            ////                DialogResult Result = msg.mfSetMessageBox(MessageBoxType.Warning, "굴림", 500, 500,
            ////                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
            ////                            "확인창", "입력확인", "설비구성품구분을 먼저 선택하세요",
            ////                            Infragistics.Win.HAlign.Right);
            ////                uGrid.ActiveCell.Row.Cells["DurableMatName"].Value = "";
            ////                uGrid.ActiveCell.Row.Cells["DurableMatCode"].Value = "";
                            
            ////            }
            ////        }
                    
            ////    }
            ////}
            ////catch (Exception ex)
            ////{
            ////}
            ////finally
            ////{
            ////}
        }

        private void uGridEquipBOM_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridEquipBOM, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);

                WinMessageBox msg = new WinMessageBox();
                string strColumn = e.Cell.Column.Key;
                string strCommonCode = e.Cell.Row.Cells["CommonCode"].Value.ToString();
                string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();

               
                #region 치공구 중복 및 Lot여부
                if (strColumn == "DurableMatCode" && !e.Cell.Value.ToString().Equals(string.Empty))
                {
                    e.Cell.Row.Cells["InputQty"].Value = "1";
                    e.Cell.Row.Cells["LotNo"].Value = "";
                    if (strCommonCode == "TL")
                    {
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                        QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                        brwChannel.mfCredentials(clsDurableMat);
                        DataTable dtDurableMat = clsDurableMat.mfReadDurableMatDetail(m_strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                        if (dtDurableMat.Rows.Count > 0)
                        {
                            e.Cell.Row.Cells["SerialFlag"].Value = dtDurableMat.Rows[0]["SerialFlag"].ToString();

                            if (dtDurableMat.Rows[0]["SerialFlag"].ToString() == "T")
                            {
                                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotReturn"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["DurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                                //QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                                //brwChannel.mfCredentials(clsDurableLot);
                                //DataTable dtDurableLot = clsDurableLot.mfReadMASDurableLot_Combo(m_strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                                brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                                QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                                brwChannel.mfCredentials(clsDurableStock);

                                DataTable dtStock = new DataTable();

                                dtStock = clsDurableStock.mfReadDMMDurableStock_ComboLot(m_strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                                grd.mfSetGridCellValueList(this.uGridEquipBOM, e.Cell.Row.Index, "LotNo", "", "", dtStock);

                                e.Cell.Row.Cells["InputQty"].Value = "1";

                                
                            }
                            else
                            {
                                //Lot이 없는 치공구의 중복검사
                                for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                                {
                                    if (!i.Equals(e.Cell.Row.Index)
                                        && this.uGridEquipBOM.Rows[i].Hidden.Equals(false)
                                        && this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString().Equals("TL")
                                        && this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.ToString().Equals(string.Empty)
                                        && this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.Equals(e.Cell.Row.Cells["DurableMatCode"].Value))
                                    {

                                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001264", "M000320", "M000843" + " " + this.uGridEquipBOM.Rows[i].RowSelectorNumber + "M000576", Infragistics.Win.HAlign.Right);
                                        e.Cell.Value = string.Empty;
                                        e.Cell.Row.Cells["DurableMatName"].Value = string.Empty;
                                        return;
                                    }
                                }

                                e.Cell.Row.Cells["InputQty"].Value = "0";
                                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotReturn"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["DurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }
                        }
                    }
                    else
                    {
                        e.Cell.Row.Cells["InputQty"].Value = "0";
                        this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                        this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotReturn"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["DurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEquipBOM_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();

                string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();
                string strCommonCode = e.Cell.Row.Cells["CommonCode"].Value.ToString();
                string strDataFlag = e.Cell.Row.Cells["DataFlag"].Value.ToString();
                string strLotNo = e.Cell.Row.Cells["LotNo"].Value.ToString();

                if (m_strPlantCode == "")
                    return;

                if (strDataFlag == "T" )
                    return;

                if (strCommonCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000694", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uGridEquipBOM.ActiveCell = this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["CommonCode"];
                    this.uGridEquipBOM.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                    
                    return;
                }

                //설비구성품구분이 SparePart인경우
                if (e.Cell.Row.Cells["CommonCode"].Value.ToString() == "SP")
                {
                    QRPCOM.UI.frmCOM0008 frmSP = new QRPCOM.UI.frmCOM0008();
                    frmSP.PlantCode = m_strPlantCode;
                    
                    frmSP.ShowDialog();


                    for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                    {
                        if (!i.Equals(e.Cell.Row.Index)
                            && this.uGridEquipBOM.Rows[i].Hidden.Equals(false)
                            && this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString().Equals("SP")
                            && this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.Equals(frmSP.SparePartCode))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000318", "M000843" + " " + this.uGridEquipBOM.Rows[i].RowSelectorNumber + "M000575", Infragistics.Win.HAlign.Right);

                            return;
                        }
                    }



                    e.Cell.Row.Cells["DurableMatCode"].Value = frmSP.SparePartCode;
                    e.Cell.Row.Cells["DurableMatName"].Value = frmSP.SparePartName;
                }

                //설비구성품구분이 금형치공구인경우
                else if (e.Cell.Row.Cells["CommonCode"].Value.ToString() == "TL")
                {
                    QRPCOM.UI.frmCOM0007 frmTL = new QRPCOM.UI.frmCOM0007();
                    frmTL.PlantCode = m_strPlantCode;

                    frmTL.ShowDialog();
                    e.Cell.Row.Cells["DurableMatName"].Value = frmTL.DurableMatName;
                    e.Cell.Row.Cells["DurableMatCode"].Value = frmTL.DurableMatCode;
                    
                } 
                 

                
                

                
                //frm.ShowDialog();

                //e.Cell.Row.Cells["ChgSparePartCode"].Value = frm.SparePartCode;

                //if (e.Cell.Row.Cells["ChgSparePartCode"].Value != "")
                //{
                //    e.Cell.Row.Cells["ChgSparePartName"].Value = frm.SparePartName;
                //}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEquipBOM_BeforeCellActivate(object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e)
        {
            try
            {
                //// SystemInfo ResourceSet
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //QRPGlobal grdImg = new QRPGlobal();
                //e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                //QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                //if (grd.mfCheckCellDataInRow(this.uGridEquipBOM, 0, e.Cell.Row.Index))
                //    e.Cell.Row.Delete(false);

                //string strColumn = e.Cell.Column.Key;
                //string strCommonCode = e.Cell.Row.Cells["CommonCode"].Value.ToString();
                //string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();

                ////if (strColumn == "LotNo")
                ////{
                ////    if (strCommonCode == "TL")
                ////    {
                ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableMat), "DurableMat");
                ////        QRPMAS.BL.MASDMM.DurableMat clsDurableMat = new QRPMAS.BL.MASDMM.DurableMat();
                ////        brwChannel.mfCredentials(clsDurableMat);
                ////        DataTable dtDurableMat = clsDurableMat.mfReadDurableMatDetail(m_strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                ////        if (dtDurableMat.Rows.Count > 0)
                ////        {
                ////            e.Cell.Row.Cells["SerialFlag"].Value = dtDurableMat.Rows[0]["SerialFlag"].ToString();

                ////            if (dtDurableMat.Rows[0]["SerialFlag"].ToString() == "T")
                ////            {
                ////                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableLot), "DurableLot");
                ////                QRPMAS.BL.MASDMM.DurableLot clsDurableLot = new QRPMAS.BL.MASDMM.DurableLot();
                ////                brwChannel.mfCredentials(clsDurableLot);
                ////                DataTable dtDurableLot = clsDurableLot.mfReadMASDurableLot_Combo(m_strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                ////                grd.mfSetGridColumnValueList(this.uGridEquipBOM, 0, "LotNo", Infragistics.Win.ValueListDisplayStyle.DisplayText
                ////                , "", "", dtDurableLot);

                ////                e.Cell.Row.Cells["InputQty"].Value = "1";

                ////                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                ////            }
                ////            else
                ////            {
                ////                e.Cell.Row.Cells["InputQty"].Value = "0";
                ////                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                ////                this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                ////            }
                ////        }
                ////    }
                ////    else
                ////    {
                ////        e.Cell.Row.Cells["InputQty"].Value = "0";
                ////        this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotNo"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                ////        this.uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["InputQty"].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;

                ////    }
                ////}

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEquipBOM_CellListSelect(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {

                int intSelectItem = Convert.ToInt32(e.Cell.ValueListResolved.SelectedItemIndex.ToString());

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                WinMessageBox msg = new WinMessageBox();
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                titleArea.Focus();
                WinGrid wGrid = new WinGrid();


                if (m_strPlantCode == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , "M001264", "M001228", "M000266", Infragistics.Win.HAlign.Center);

                    // Focus
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                #region LotNo
                if (e.Cell.Column.Key.ToString().Equals("LotNo"))
                {

                    e.Cell.Row.Cells["DurableInventoryCode"].Value = "";

                    //string strLotNo = e.Cell.ValueList.GetValue(e.Cell.ValueList.SelectedItemIndex).ToString();
                    //string strLotNo = e.Cell.Column.ValueList.GetValue(e.Cell.Column.ValueList.SelectedItemIndex).ToString();
                    string strDurableMatCode = e.Cell.Row.Cells["DurableMatCode"].Value.ToString();

                    for (int i = 0; i < this.uGridEquipBOM.Rows.Count; i++)
                    {
                        if(!i.Equals(e.Cell.Row.Index)
                           && this.uGridEquipBOM.Rows[i].Hidden.Equals(false)
                           && this.uGridEquipBOM.Rows[i].Cells["CommonCode"].Value.ToString().Equals("TL")
                           && this.uGridEquipBOM.Rows[i].Cells["DurableMatCode"].Value.ToString().Equals(strDurableMatCode)
                           && this.uGridEquipBOM.Rows[i].Cells["LotNo"].Value.Equals(e.Cell.Value))
                        {
                            msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M000318", "M000843" + " " + this.uGridEquipBOM.Rows[i].RowSelectorNumber + "M000576", Infragistics.Win.HAlign.Right);

                            e.Cell.Value = string.Empty;
                            return;
                        }
                    }


                    brwChannel.mfRegisterChannel(typeof(QRPDMM.BL.DMMICP.DurableStock), "DurableStock");
                    QRPDMM.BL.DMMICP.DurableStock clsDurableStock = new QRPDMM.BL.DMMICP.DurableStock();
                    brwChannel.mfCredentials(clsDurableStock);

                    DataTable dtStock = clsDurableStock.mfReadDMMDurableStock_ComboLot(m_strPlantCode, strDurableMatCode, m_resSys.GetString("SYS_LANG"));

                    e.Cell.Row.Cells["DurableInventoryCode"].Value = dtStock.Rows[intSelectItem]["DurableInventoryCode"].ToString();

                    uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["DurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    uGridEquipBOM.Rows[e.Cell.Row.Index].Cells["LotReturn"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
              
                }
                #endregion


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            {
            }
        }

        private void uGridEquipBOM_AfterRowInsert(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            try
            {
                
                this.uGridEquipBOM.Rows[e.Row.Index].Cells["LotReturn"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }

            finally
            {
            }
        }

        private void uGridEquipBOM_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            e.Cell.Row.RowSelectorAppearance.Image = SysRes.ModifyCellImage;
        }

        #endregion

        #region Combo

        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                this.uComboArea.Items.Clear();
                this.uComboStation.Items.Clear();

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                //--- Station ---//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                brwChannel.mfCredentials(clsStation);

                DataTable dtStation = clsStation.mfReadStation(strPlantCode, m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue_All()
                    , "StationCode", "StationName", dtStation);
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }

        //설비위치 변경시 자동조회
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //설비중분류 변경시 자동조회
        private void uComboSearchEquipType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboSearchEquipType.Value.ToString(), 1);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //Station콤보 선택시 자동조회
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //검색조건저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //설비위치정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsEquipLocation);

                    //설비위치정보콤보조회 매서드 실행
                    DataTable dtLoc = clsEquipLocation.mfReadLocation_Combo(strPlantCode, strStationCode, m_resSys.GetString("SYS_LANG"));

                    WinComboEditor wCombo = new WinComboEditor();

                    //설비위치정보콤보 이전 정보 클리어
                    this.uComboSearchEquipLoc.Items.Clear();

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue_All(), "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(strPlantCode, strStationCode, "", "", "", 3);

                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비대분류콤보 선택시 자동조회
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion


        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();
                string strValue = ComboDefaultValue_All();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strValue, "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strValue, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strValue, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strValue, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strValue, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strValue, "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보 기본값(전체)
        /// </summary>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue_All()
        {
            try
            {
                string strRtnValue = "";
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();

                if (strLang.Equals("KOR"))
                    strRtnValue = "전체";
                else if (strLang.Equals("CHN"))
                    strRtnValue = "全部";
                else if (strLang.Equals("ENG"))
                    strRtnValue = "All";


                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }
    }
}

