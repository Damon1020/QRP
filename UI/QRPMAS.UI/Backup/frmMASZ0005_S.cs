﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMASZ0005_S.cs                                      */
/* 프로그램명   : 설비점검정보승인                                      */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : 2012-07-04 : ~~~~~ 추가 (권종구)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMASZ0005_S : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();

        public frmMASZ0005_S()
        {
            InitializeComponent();
        }

        private void frmMASZ0005_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        /// <summary>
        /// 폼을닫거나 닫을때마다 발생되는이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMASZ0005_FormClosing(object sender, FormClosingEventArgs e)
        {
            //그리드설정
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }
        private void frmMASZ0005_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBoxContentsArea.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBoxContentsArea.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        private void frmMASZ0005_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("설비점검정보승인", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitGrid();
            InitComboBox();
            InitButton();

             //ContentGroupBox 닫힘상태로
            this.uGroupBoxContentsArea.Expanded = false;

            //그리드 설정 
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);

        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelSearchEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelSearchEquipType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, false);

                wLabel.mfSetLabel(this.uLabelStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "생성자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateDate, "생성일", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAcceptUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAdmitSecondUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAcceptDate, "승인일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevisionReason, "개정사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRejectReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);


                //////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPCOM.QRPGLO.QRPBrowser();
                //////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                //////QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                //////brwChannel.mfCredentials(clsUser);

                //////DataTable dtUser = clsUser.mfReadSYSUser(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_LANG"));

                this.uTextCreateUserID.Text = m_resSys.GetString("SYS_USERID");
                this.uTextCreateUserName.Text = m_resSys.GetString("SYS_USERNAME");

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonFileDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 설비점검정보 승인리스트
                //--------------------------------------------- 설비점검정보 승인 리스트
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipPMAdmit, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    ,false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "PlantCode", "공장코", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                   , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "StdNumber", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "StdNumVersion", "표준번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "VersionNum", "개정번호", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "StationName", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "EquipLocName", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "ProcessgroupCode", "설비대분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "EquipLargeTypeCode", "설비중분류", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "EquipGroupCode", "설비점검그룹코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "EquipGroupName", "설비점검그룹명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "WriteName", "생성자", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "WriteDate", "생성일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "AdmitID", "승인자1", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "AdmitName", "승인자1", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "AdmitSecondID", "승인자2", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 20
                   , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                   , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "AdmitSecondName", "승인자2", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMAdmit, 0, "RevisionReason", "개정사유", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");


                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGridEquipPMAdmit, 0);

                // 폰트 설정
                this.uGridEquipPMAdmit.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipPMAdmit.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 점검항목상세
                // ------------------------------점검항목상세 Grid------------------------------------------------------------------
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipPMD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMPeriodCode", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, false, 3
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");


                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, true, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "ImageFile", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "MeasureValueFlag", "수치입력여부", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "UnitDesc", "단위", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "LevelCode", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                this.uGridEquipPMD.DisplayLayout.Bands[0].Columns["MeasureValueFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                

                #region DropDown
                string strLang = m_resSys.GetString("SYS_LANG");
                string strDefault = ComboDefaultValue("S", strLang);
                //--점검주기 콤보
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtPeriod = clsCommonCode.mfReadCommonCode("C0007", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridEquipPMD, 0, "PMPeriodCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPeriod);

                //--난이도 콤보

                DataTable dtLevel = clsCommonCode.mfReadCommonCode("C0008", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridEquipPMD, 0, "LevelCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtLevel);

                #endregion


               
                // 폰트 설정
                this.uGridEquipPMD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipPMD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 점검그룹설비리스트
                //점검그룹설비리스트---------------------------------------------------------------------------------------------------
                //--기본설정
                wGrid.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    ,false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));


                string strName = "";

                if (strLang.Equals("KOR"))
                    strName = "주간점검,월간점검,분기점검,반기점검,년간점검";
                else if (strLang.Equals("CHN"))
                    strName = "每周点检,每月点检,季度点检,半年点检,年间点检";
                else if (strLang.Equals("ENG"))
                    strName = "주간점검,월간점검,분기점검,반기점검,년간점검";


                string[] strGroups = strName.Split(',');


                // Set GridGroup
                this.uGridEquipList.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupWeek", strGroups[0], 7, 0, 1, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupMonth", strGroups[1], 8, 0, 2, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQUA = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupQUA", strGroups[2], 10, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHAF = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupHAF", strGroups[3], 13, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupYear = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupYEA", strGroups[4], 16, 0, 3, 2, false);

                // 컬럼설정

                //-->설비에 대한 설비그룹은 MDM에서 정하기 때문에 설비편집 불가 처리
                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "AreaCode", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "StationCode", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocCode", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunCode", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMWeekDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, group1);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 0, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 0, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 1, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 0, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 1, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 0, 0, 1, 1, uGroupYear);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 1, 0, 1, 1, uGroupYear);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupYear);


                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupWeek"].CellAppearance.ForeColor = Color.Black;
                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupMonth"].CellAppearance.ForeColor = Color.Black;
                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupQUA"].CellAppearance.ForeColor = Color.Black;
                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupHAF"].CellAppearance.ForeColor = Color.Black;
                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupYEA"].CellAppearance.ForeColor = Color.Black;

                
                //--Grid초기화 후 Font크기를 아래와 같이 적용
                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                //---------------------------------------------------------------------------------------------------------------------------------------------------
                #endregion

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        
        #endregion

        #region 툴바
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항 확인

                //입력사항 확인
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001228", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                #endregion

                //공장코드 와 설비그룹코드 ,Station ,위치,유형 저장
                string strPlantCode = uComboSearchPlant.Value == null ? "" : uComboSearchPlant.Value.ToString();
                string strEuqipGroupCode = uComboSearchEquipGroup.Value == null ? "" : uComboSearchEquipGroup.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value == null ? "" : this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value == null ? "" : this.uComboSearchEquipLoc.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value == null ? "" : this.uComboSearchEquipType.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value == null ? "" : this.uComboProcessGroup.Value.ToString();
                
                //ExpandGroupBox True면 숨김
                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                    //Value초기화
                    ValueClear();
                }
                
                //Popup창 실행
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;
                //처리 로직//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);

                DataTable dtAdmit = clsEquipPMH.mfReadEquipPMAdmit_S(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipTypeCode, strEuqipGroupCode, m_resSys.GetString("SYS_LANG"));
                
                this.uGridEquipPMAdmit.DataSource = dtAdmit;
                this.uGridEquipPMAdmit.DataBind();
                //데이터 바인드

                this.MdiParent.Cursor = Cursors.Default;

                m_ProgressPopup.mfCloseProgressPopup(this);

                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtAdmit.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipPMAdmit, 0);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                System.Windows.Forms.DialogResult result;
                WinMessageBox msg = new WinMessageBox();

                #region 필수 입력사항
                if (this.uGroupBoxContentsArea.Expanded == false || this.uGridEquipPMD.Rows.Count == 0)
                {
                    //저장할 정보가 없습니다.
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000882", "M001047", Infragistics.Win.HAlign.Right);
                    
                    return;
                }
                
                string strUserID = m_resSys.GetString("SYS_USERID").ToUpper();

                //승인자가 승인처리 한 정보인 경우 메세지 처리
                if (this.uTextAdmitUserID.Text.ToUpper().Equals(strUserID) && this.uCheckAdmit.Checked)
                {
                    //이미 승인하신 정보는 다시 승인 할 수 없습니다.
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001469", "M001471"
                                , Infragistics.Win.HAlign.Right);

                    return;
                }

                if (this.uTextSecondID.Text.ToUpper().Equals(strUserID) && this.uCheckSecondAdmit.Checked)
                {
                    //이미 승인하신 정보는 다시 승인 할 수 없습니다.
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001469", "M001471"
                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);

                DataTable dtAdmit = clsEquipPMH.mfSetEquipPMHData();

                //---------------필수입력사항확인------------------//
                if (this.uTextAdmitUserID.Text.Equals(string.Empty) || this.uTextAdmitUserName.Text.Equals(string.Empty))
                {
                    //승인자
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000770", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uTextAdmitUserID.Focus();
                    return;
                }
                else if (this.uDateAdmitDate.Value == null || this.uDateAdmitDate.Value.ToString().Equals(string.Empty))
                {
                    //승인일을 선택해주세요
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M001299", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uDateAdmitDate.DropDown();
                    return;
                }
                else
                {
                    //----값 저장----//
                    DataRow drAdmit;
                    drAdmit = dtAdmit.NewRow();
                    drAdmit["PlantCode"] = this.uTextPlantCode.Text;
                    drAdmit["StdNumber"] = this.uTextStandardNo.Text;
                    drAdmit["VersionNum"] = this.uTextVersionNum.Text;
                    drAdmit["AdmitID"] = this.uTextAdmitUserID.Text; // 첫번째 승인자
                    drAdmit["AdmitSecondID"] = this.uTextSecondID.Text; //두번째 승인자 
                    drAdmit["AdmitDate"] = this.uDateAdmitDate.DateTime.Date.ToString("yyyy-MM-dd");
                    drAdmit["RejectReason"] = this.uTextRejectReason.Text;
                    drAdmit["EtcDesc"] = this.uTextEtc.Text;


                    dtAdmit.Rows.Add(drAdmit);

                }

                #endregion

                #region 승인여부 메세지 박스
                //--------------------승인 여부 메시지 박스 ------------------------------//
                DialogResult diResult = msg.mfSetMessageBox(MessageBoxType.AgreeReject, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001053", "M000936",
                                    Infragistics.Win.HAlign.Right);
                
                if (diResult == DialogResult.Yes) // 저장 시 승인 클릭
                {
                    if (this.uTextAdmitUserID.Text.ToUpper().Equals(strUserID)) // 접속자 ID와 승인자 ID 와 동일 할경우 
                    {
                        dtAdmit.Rows[0]["AdmitState"] = "T"; // 승인상태
                        this.uCheckAdmit.Checked = true; // 첫번째 승인 처리
                    }
                    if (this.uTextSecondID.Text.ToUpper().Equals(strUserID))
                    {
                        dtAdmit.Rows[0]["AdmitSecondState"] = "T"; //승인상태
                        this.uCheckSecondAdmit.Checked = true; //두번째 승인처리
                    }
                    if (this.uTextSecondID.Text.Trim().Equals(string.Empty)) //두번째 승인자가 없을 경우 체크박스 승인처리
                    {
                        dtAdmit.Rows[0]["AdmitSecondState"] = "T"; //승인상태
                        this.uCheckSecondAdmit.Checked = true;
                    }
                    if(this.uCheckAdmit.Checked && this.uCheckSecondAdmit.Checked) //승인이 둘다 체크 되었을 경우 최종승인처리
                        dtAdmit.Rows[0]["AdmitStatusCode"] = "FN";
                }
                else if (diResult == DialogResult.No) // 저장시 반려
                {
                    dtAdmit.Rows[0]["AdmitStatusCode"] = "RE";
                }
                else
                {
                    return;
                }
                #endregion

                #region BL
                //-------------------------저장 팝업 창 ---------------------------//
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //--------------처리로직-----------------//
                string strRtn = clsEquipPMH.mfSaveEquipPMAdmit_S(dtAdmit, strUserID, m_resSys.GetString("SYS_USERIP"));

                //Decoding//
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                /////////////
                //---------------------------------------//
                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                #endregion

                #region 결과처리
                //처리결과값에 대한 메세지박스//
                
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);
                    mfSearch();
                }
                else
                {
                    string strLang = m_resSys.GetString("SYS_LANG");
                    string strMessage = "";
                    if (!ErrRtn.ErrMessage.Equals(string.Empty))
                        strMessage = ErrRtn.ErrMessage;
                    else
                        strMessage = msg.GetMessge_Text("M000953",strLang);


                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 msg.GetMessge_Text("M001135", strLang), msg.GetMessge_Text("M001037", strLang), strMessage,
                                                 Infragistics.Win.HAlign.Right);
                }
                #endregion


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfDelete()
        {
           
        }

        public void mfCreate()
        {
            
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipPMAdmit.Rows.Count == 0 && 
                    ((this.uGridEquipList.Rows.Count == 0 && this.uGridEquipPMD.Rows.Count == 0) || this.uGroupBoxContentsArea.Expanded.Equals(false)))
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "M000811", "M000812", Infragistics.Win.HAlign.Right);
                    return;
                }

                ////처리 로직//
                WinGrid grd = new WinGrid();

                if (this.uGridEquipPMAdmit.Rows.Count != 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquipPMAdmit);

                if (this.uGridEquipPMD.Rows.Count != 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridEquipPMD);

                if (this.uGridEquipList.Rows.Count != 0 && this.uGroupBoxContentsArea.Expanded.Equals(true))
                    grd.mfDownLoadGridToExcel(this.uGridEquipList);

                ///////////////


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion 

        #region 이벤트

        /// <summary>
        /// ExpandGroupBox펼침과 닫힘에 대한 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipPMAdmit.Height = 60;
                    
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridEquipPMAdmit.Height = 760;
                    for (int i = 0; i < this.uGridEquipPMAdmit.Rows.Count; i++)
                    {
                        this.uGridEquipPMAdmit.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        /// <summary>
        /// 승인리스트 더블 클릭 시 발생하는 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridEquipPMAdmit_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                bool bolChk = false;

                string strUserID = m_resSys.GetString("SYS_USERID").ToUpper();

                if (e.Cell.Row.Cells["AdmitID"].Value.ToString().ToUpper().Equals(strUserID))
                    bolChk = true;
                if (e.Cell.Row.Cells["AdmitSecondID"].Value.ToString().ToUpper().Equals(strUserID))
                    bolChk = true;

                if(!bolChk)
                {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M000882", "M001468", Infragistics.Win.HAlign.Right);
                        return;
                }

                //공장코드 설비그룹코드 표준번호 개정번호 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                this.uTextPlantCode.Text = strPlantCode;
                string strStdNumber = e.Cell.Row.Cells["StdNumber"].Value.ToString();
                string strVersion = e.Cell.Row.Cells["VersionNum"].Value.ToString();


                if (SearchHeader(strPlantCode,strStdNumber,strVersion,m_resSys.GetString("SYS_LANG")))
                {
                    if (this.uGroupBoxContentsArea.Expanded == false)
                        this.uGroupBoxContentsArea.Expanded = true;


                    e.Cell.Row.Fixed = true;

                    //탭컨트롤 인덱스 기본값으로
                    if (this.uTabEquipPM.SelectedTab.Index != 0)
                    {
                        this.uTabEquipPM.Tabs[0].Selected = true;
                    }

                    //그리드에 있는 정보 텍스트에 넣기

                    this.uTextVersionNum.Text = strVersion;
                    this.uTextStandardNo.Text = strStdNumber;

                    SearchDetail(strStdNumber, strVersion);

                    

                    WinGrid grd = new WinGrid();

                    if (this.uGridEquipPMD.Rows.Count > 0)
                        grd.mfSetAutoResizeColWidth(this.uGridEquipPMD, 0);

                    if (this.uGridEquipList.Rows.Count > 0)
                        grd.mfSetAutoResizeColWidth(this.uGridEquipList, 0);

                    //------------------------------//
                }
                else
                {
                    //승인되었거나 반려되어 조회할 수 없습니다.
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001469", "M001470", Infragistics.Win.HAlign.Right);
                    if (this.uGroupBoxContentsArea.Expanded == true)
                        this.uGroupBoxContentsArea.Expanded = false;

                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 엔터누를시 발생하는 이벤트 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextAdmitUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (this.uTextAdmitUserID.ReadOnly)
                    return;

                if (e.KeyData == Keys.Enter)
                {
                    //승인자 공장코드 저장
                    string strAdmitID = this.uTextAdmitUserID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strAdmitID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextCreateUserID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000882", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    //BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                    QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                    brwChannel.mfCredentials(clsUser);

                    //매서드호출
                    DataTable dtCreat = clsUser.mfReadSYSUser(strPlantCode, strAdmitID, m_resSys.GetString("SYS_LANG"));

                    //정보가 없을 시
                    if (dtCreat.Rows.Count == 0)
                    {
                        /* 검색결과 Record수 = 0이면 메시지 띄움 */
                        System.Windows.Forms.DialogResult result;
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);

                        this.uTextAdmitUserID.Text = "";
                        this.uTextAdmitUserName.Text = "";


                        return;
                    }

                    //정보가 확인 시 이름 추가
                    string strUserName = dtCreat.Rows[0]["UserName"].ToString();


                    //정보가 확인 시 승인자 이름 추가
                    this.uTextAdmitUserName.Text = strUserName;


                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 승인자 검색버튼 클릭시 생김
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextAdmitUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                if (this.uTextAdmitUserID.ReadOnly)
                    return;

                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                QRPMAS.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장보냄
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                if (frmUser.PlantCode !="" && strPlant != frmUser.PlantCode)
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000962", "M000268", Infragistics.Win.HAlign.Right);

                    return;
                }

                this.uTextAdmitUserID.Text = frmUser.UserID;
                this.uTextAdmitUserName.Text = frmUser.UserName;
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }



        #region 콤보이벤트
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uGroupBoxContentsArea.Expanded == true)
                {
                    this.uGroupBoxContentsArea.Expanded = false;
                }
                ValueClear();

                //콤보박스 클리어
                this.uComboSearchStation.Items.Clear();

                //공장코드 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                /////////////////////////////////////////////////////////////

                //Station정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                brwChannel.mfCredentials(clsStation);
                
                //Station콤보조회 BL호출
                DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                wCombo.mfSetComboEditor(this.uComboSearchStation, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue("A", strLang), "StationCode", "StationName", dtStation);

                ////////////////////////////////////////////////////////////

                mfSearchCombo(this.uComboSearchPlant.Value.ToString(), "", "", "", "", 3);
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        

        //설비위치 변경시 자동조회
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //설비중분류 변경시 자동조회
        private void uComboSearchEquipType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipType.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipType.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipType.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboSearchEquipType.Value.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //Station콤보 선택시 자동조회
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //검색조건저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //설비위치정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsEquipLocation);

                    //설비위치정보콤보조회 매서드 실행
                    DataTable dtLoc = clsEquipLocation.mfReadLocation_Combo(strPlantCode, strStationCode, m_resSys.GetString("SYS_LANG"));

                    WinComboEditor wCombo = new WinComboEditor();

                    //설비위치정보콤보 이전 정보 클리어
                    this.uComboSearchEquipLoc.Items.Clear();

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLoc, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue("A", m_resSys.GetString("SYS_LANG")), "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(strPlantCode, strStationCode, "", "", "", 3);

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비대분류콤보 선택시 자동조회
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion


        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                int intFileNum = 0;
                for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                {
                    //경로가 없는 행이 있는지 체크
                    if (!this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString().Equals(string.Empty)
                        && this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString().Contains(":\\") == false 
                        && Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["Check"].Value) == true)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비점검정보 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0023");


                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                    {
                        if (this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["Check"].Value) == true)
                            arrFile.Add(this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        #endregion

        #region Method
        
        /// <summary>
        /// 헤더정보조회
        /// </summary>
        private bool SearchHeader(string strPlantCode, string strStdNumber,string strVersion,string strLang)
        {
            try
            {
                //--설비점검정보헤더 조회 --//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);

                DataTable dtEquipPMH = clsEquipPMH.mfReadEquipPMAdmitH_S(strPlantCode, strStdNumber, strVersion, strLang);
                
                if (dtEquipPMH.Rows.Count == 0 || !dtEquipPMH.Rows[0]["AdmitStatusCode"].ToString().Equals("AR"))
                    return false;

                //데이터바인드
                this.uTextAdmitUserID.Text = dtEquipPMH.Rows[0]["AdmitID"].ToString();
                this.uTextAdmitUserName.Text = dtEquipPMH.Rows[0]["AdmitName"].ToString();
                this.uTextSecondID.Text = dtEquipPMH.Rows[0]["AdmitSecondID"].ToString();
                this.uTextSecondName.Text = dtEquipPMH.Rows[0]["AdmitSecondName"].ToString();
                this.uTextCreateDate.Text = dtEquipPMH.Rows[0]["WriteDate"].ToString();
                this.uTextCreateUserID.Text = dtEquipPMH.Rows[0]["WriteID"].ToString();
                this.uTextCreateUserName.Text = dtEquipPMH.Rows[0]["WriteName"].ToString();
                this.uTextEtc.Text = dtEquipPMH.Rows[0]["EtcDesc"].ToString();
                this.uTextRejectReason.Text = dtEquipPMH.Rows[0]["RejectReason"].ToString();
                this.uTextRevisionReason.Text = dtEquipPMH.Rows[0]["RevisionReason"].ToString();

                this.uTextEquipGroupCode.Text = dtEquipPMH.Rows[0]["EquipGroupCode"].ToString();
                this.uCheckAdmit.Checked = dtEquipPMH.Rows[0]["AdmitState"].ToString() == "T" ? true : false;
                this.uCheckSecondAdmit.Checked = dtEquipPMH.Rows[0]["AdmitSecondState"].ToString() == "T" ? true : false;


                SearchEquipList(strPlantCode, dtEquipPMH.Rows[0]["StationCode"].ToString(), dtEquipPMH.Rows[0]["EquipLocCode"].ToString(),
                                dtEquipPMH.Rows[0]["ProcessGroupCode"].ToString(), dtEquipPMH.Rows[0]["EquipLargeTypeCode"].ToString(),
                                dtEquipPMH.Rows[0]["EquipGroupCode"].ToString(), strLang);

                return true;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return false;
            }
            finally
            { }
        }

        /// <summary>
        /// 상세정보조회
        /// </summary>
        /// <param name="strStdNumber"></param>
        /// <param name="strVersion"></param>
        private void SearchDetail(string strStdNumber, string strVersion)
        {
            try
            {
                //---설비점검정보상세 조회--//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMD), "EquipPMD");
                QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                brwChannel.mfCredentials(clsEquipPMD);

                DataTable dtPMD = clsEquipPMD.mfReadEquipPMD(strStdNumber, strVersion);

                //데이터바인드
                this.uGridEquipPMD.DataSource = dtPMD;
                this.uGridEquipPMD.DataBind();

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
                
        }

        /// <summary>
        /// 설비그룹리스트 조회
        /// </summary>
        /// <param name="strPlantCode"></param>
        /// <param name="strLang"></param>
        private void SearchEquipList(string strPlantCode, string strStation,string strEquipLocCode, string strProcessGroupCode,string strLargeTypeCode,string strEquipGroupCode,string strLang)
        {
            try
            {  //--------설비리스트조회---------//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChannel.mfCredentials(clsEquipGroup);

                DataTable dtEquip = clsEquipGroup.mfReadEquipGroupList_PM(strPlantCode, strStation, strEquipLocCode, strProcessGroupCode, strLargeTypeCode, strEquipGroupCode, strLang);

                //데이터바인드
                this.uGridEquipList.DataSource = dtEquip;
                this.uGridEquipList.DataBind();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value 와 그리드 지우기
        /// </summary>
        private void ValueClear()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                this.uTextPlantCode.Clear();
                this.uTextAdmitUserID.Text = "";
                
                this.uTextAdmitUserName.Text = "";
                this.uTextSecondID.Clear();
                this.uTextSecondName.Clear();
                this.uTextCreateDate.Text = "";
                this.uTextCreateUserID.Text = "";
                this.uTextCreateUserName.Text = "";
                this.uTextEquipGroupCode.Text = "";
                this.uTextEtc.Text = "";
                this.uTextVersionNum.Text = "";
                this.uTextStandardNo.Text = "";
                this.uTextRevisionReason.Text = "";
                this.uTextRejectReason.Text = "";
                
                this.uDateAdmitDate.DateTime = DateTime.Today;

                if (this.uGridEquipList.Rows.Count > 0)
                {
                    this.uGridEquipList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipList.Rows.All);
                    this.uGridEquipList.DeleteSelectedRows(false);
                }

                if (this.uGridEquipPMD.Rows.Count > 0)
                {
                    this.uGridEquipPMD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipPMD.Rows.All);
                    this.uGridEquipPMD.DeleteSelectedRows(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();

                string strDefault = ComboDefaultValue("A", strLang);

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }
                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">S : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun, string strLang)
        {
            try
            {
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                strLang = strLang.ToUpper();
                if (strGubun.Equals("S"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }
        #endregion

    }
}
