﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 설비관리기준정보                                      */
/* 프로그램ID   : frmMASZ0013.cs                                        */
/* 프로그램명   : 설비변경점유형정보                                    */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                2011-08-05 : ~~~~~ 추가 (권종구)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//참조추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources; 

namespace QRPMAS.UI
{
    public partial class frmMASZ0013 : Form,IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0013()
        {
            InitializeComponent();
        }
        private void frmMASZ0013_Activated(object sender, EventArgs e)
        {
            //툴바활성
            QRPBrowser ToolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //사용여부설정
            ToolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0013_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

            //타이틀설정
            titleArea.mfSetLabelText("설비변경점유형정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            //컨트롤 초기화
            SetToolAuth();
            InitGrid();
            InitLabel();
            InitComboBox();
        }
       
        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();

                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //기본설정
                grd.mfInitGeneralGrid(this.uGridEquipCCSType, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons,
                    Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridEquipCCSType, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridEquipCCSType, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 10, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                grd.mfSetGridColumn(this.uGridEquipCCSType, 0, "EquipCCSTypeCode", "설비변경점코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 5, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSType, 0, "EquipCCSTypeName", "설비변경점명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSType, 0, "EquipCCSTypeNameCh", "설비변경점명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSType, 0, "EquipCCSTypeNameEn", "설비변경점명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridEquipCCSType, 0, "CCSFlag", "CCS의뢰대상여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 0, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridEquipCCSType, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, true, false, 1, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //폰트설정
                this.uGridEquipCCSType.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipCCSType.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                QRPBrowser brwChannel = new QRPBrowser();
                

                //사용여부
                //bl호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);
                //매서드호출
                DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                
                //공장
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //그리드에넣기
                grd.mfSetGridColumnValueList(this.uGridEquipCCSType, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUseFlag);
                grd.mfSetGridColumnValueList(this.uGridEquipCCSType, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtPlant);

                //한줄생성
                grd.mfAddRowGrid(this.uGridEquipCCSType, 0);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        //콤보박스초기화
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바기능

        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                //팝업창실행
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //-------------------------------------------처리 로직----------------------------------------------------------------//
                string strPlantCode = uComboPlant.Value.ToString();
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCCSType), "EquipCCSType");
                QRPMAS.BL.MASEQU.EquipCCSType clsEquipCCSType = new QRPMAS.BL.MASEQU.EquipCCSType();
                brwChannel.mfCredentials(clsEquipCCSType);
                //매서드호출
                DataTable dtEquipCCSType = clsEquipCCSType.mfReadEquipCCSType(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //데이터바인드
                this.uGridEquipCCSType.DataSource = dtEquipCCSType;
                this.uGridEquipCCSType.DataBind();

                

                //--------------------------------------------------------------------------------------------------------------------//
                //커서변경
                this.MdiParent.Cursor = Cursors.Default;
                //팝업창종료
                m_ProgressPopup.mfCloseProgressPopup(this);
                /* 검색결과 Record수 = 0이면 메시지 띄움 */
                if (dtEquipCCSType.Rows.Count == 0)
                {
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "처리결과", "조회처리결과", "조회결과가 없습니다.",
                                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                    //PK 처리
                    for (int i = 0; i < dtEquipCCSType.Rows.Count; i++)
                    {
                        this.uGridEquipCCSType.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridEquipCCSType.Rows[i].Cells["PlantCode"].Appearance.BackColor = Color.Gainsboro;
                        this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeCode"].Appearance.BackColor = Color.Gainsboro;
                    }

                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGridEquipCCSType, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 정보저장
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCCSType), "EquipCCSType");
                QRPMAS.BL.MASEQU.EquipCCSType clsEquipCCSType = new QRPMAS.BL.MASEQU.EquipCCSType();
                brwChannel.mfCredentials(clsEquipCCSType);

                DataTable dtEquipCCSType = clsEquipCCSType.mfDataSet();

                //-----------------------------필수 입력사항 체크 ------------------------------------------//
                if (uGridEquipCCSType.Rows.Count != 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridEquipCCSType.ActiveCell = this.uGridEquipCCSType.Rows[0].Cells[0];

                    for (int i = 0; i < uGridEquipCCSType.Rows.Count; i++)
                    {
                        if (this.uGridEquipCCSType.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            #region 필수입력사항
                            if (this.uGridEquipCCSType.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 열의 공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                                // Focus Cell
                                this.uGridEquipCCSType.ActiveCell = this.uGridEquipCCSType.Rows[i].Cells["PlantCode"];
                                this.uGridEquipCCSType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;

                            }
                            if (this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 열의 설비변경점코드를 입력해주세요.", Infragistics.Win.HAlign.Right);

                                // Focus Cell
                                this.uGridEquipCCSType.ActiveCell = this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeCode"];
                                this.uGridEquipCCSType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            if (this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeName"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 열의 설비변경점명을 입력해주세요.", Infragistics.Win.HAlign.Right);

                                // Focus Cell
                                this.uGridEquipCCSType.ActiveCell = this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeName"];
                                this.uGridEquipCCSType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            if (this.uGridEquipCCSType.Rows[i].Cells["CCSFlag"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 열의 CCS의뢰대상여부를 선택해주세요.", Infragistics.Win.HAlign.Right);

                                return;
                            }
                            if (this.uGridEquipCCSType.Rows[i].Cells["UseFlag"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 열의 사용여부를 선택해주세요.", Infragistics.Win.HAlign.Right);

                                // Focus Cell
                                this.uGridEquipCCSType.ActiveCell = this.uGridEquipCCSType.Rows[i].Cells["UseFlag"];
                                this.uGridEquipCCSType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;
                            }
                            #endregion
                            
                            DataRow drEquipCCSType;
                            drEquipCCSType = dtEquipCCSType.NewRow();
                            drEquipCCSType["PlantCode"] = this.uGridEquipCCSType.Rows[i].Cells["PlantCode"].Value.ToString();
                            drEquipCCSType["EquipCCSTypeCode"] = this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeCode"].Value.ToString();
                            drEquipCCSType["EquipCCSTypeName"] = this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeName"].Value.ToString();
                            drEquipCCSType["EquipCCSTypeNameCh"] = this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeNameCh"].Value.ToString();
                            drEquipCCSType["EquipCCSTypeNameEn"] = this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeNameEn"].Value.ToString();
                            drEquipCCSType["CCSFlag"] = this.uGridEquipCCSType.Rows[i].Cells["CCSFlag"].Value.ToString();
                            drEquipCCSType["UseFlag"] = this.uGridEquipCCSType.Rows[i].Cells["UseFlag"].Value.ToString();
                            dtEquipCCSType.Rows.Add(drEquipCCSType);

                            
                        }
                    }
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                         "확인창", "입력확인", "저장정보가 없습니다", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (dtEquipCCSType.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                         "확인창", "입력확인", "저장정보가 없습니다", Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "저장확인", "입력한 정보를 저장하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {



                    //팝업창실행
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                    //커서변경
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    string strRtn = clsEquipCCSType.mfSaveEquipCCStype(dtEquipCCSType, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                    //DeCoding//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    /////////////

                    //커서변경
                    this.MdiParent.Cursor = Cursors.Default;

                    //팝업창종료
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }

                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                //SystemResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();

                #region 삭제정보저장
                //BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipCCSType), "EquipCCSType");
                QRPMAS.BL.MASEQU.EquipCCSType clsEquipCCSType = new QRPMAS.BL.MASEQU.EquipCCSType();
                brwChannel.mfCredentials(clsEquipCCSType);
                //매서드호출
                DataTable dtEquipCCSType = clsEquipCCSType.mfDataSet();
                if (this.uGridEquipCCSType.Rows.Count != 0)
                {
                    //Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridEquipCCSType.ActiveCell = this.uGridEquipCCSType.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridEquipCCSType.Rows.Count; i++)
                    {
                        if (this.uGridEquipCCSType.Rows[i].RowSelectorAppearance.Image != null)
                        {
                            #region 필수입력사항
                            if (this.uGridEquipCCSType.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 열의 공장을 선택해주세요.", Infragistics.Win.HAlign.Right);

                                // Focus Cell
                                this.uGridEquipCCSType.ActiveCell = this.uGridEquipCCSType.Rows[i].Cells["PlantCode"];
                                this.uGridEquipCCSType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                                return;

                            }
                            if (this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                   Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                  "확인창", "필수입력사항확인", (i + 1) + "번째 열의 설비변경점코드를 입력해주세요.", Infragistics.Win.HAlign.Right);

                                // Focus Cell
                                this.uGridEquipCCSType.ActiveCell = this.uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeCode"];
                                this.uGridEquipCCSType.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            #endregion
                            
                            DataRow drEquipCCSType;
                            drEquipCCSType = dtEquipCCSType.NewRow();
                            drEquipCCSType["PlantCode"] = uGridEquipCCSType.Rows[i].Cells["PlantCode"].Value.ToString();
                            drEquipCCSType["EquipCCSTypeCode"] = uGridEquipCCSType.Rows[i].Cells["EquipCCSTypeCode"].Value.ToString();
                            dtEquipCCSType.Rows.Add(drEquipCCSType);

                            
                        }
                    }
                }
                else
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                         "확인창", "입력확인", "삭제정보가 없습니다", Infragistics.Win.HAlign.Right);
                    return;
                }

                if (dtEquipCCSType.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                          Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                         "확인창", "입력확인", "삭제정보가 없습니다", Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {



                    //팝업창 실행
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    //커서변경
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//

                    string strRtn = clsEquipCCSType.mfDeleteEquipCCSType(dtEquipCCSType);

                    //DeCoding//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);

                    /////////////

                    //커서변경
                    this.MdiParent.Cursor = Cursors.Default;
                    //팝업창종료
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 성공적으로 삭제했습니다.",
                                                    Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "처리결과", "삭제처리결과", "선택한 정보를 삭제하지 못했습니다.",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        
        
        public void mfCreate()
        {
            //try
            //{
            //    while (this.uGridEquipCCSType.Rows.Count > 0)
            //    {
            //        this.uGridEquipCCSType.Rows[0].Delete(false);
            //    }
            //}
            //catch (Exception ex)
            //{ }
            //finally
            //{ }
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {

            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipCCSType.Rows.Count == 0)
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "확인창", "출력정보 확인", "엑셀출력 정보가 없습니다.", Infragistics.Win.HAlign.Right);
                    return;
                }
                    
                WinGrid grd = new WinGrid();
                grd.mfDownLoadGridToExcel(uGridEquipCCSType);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfPrint()
        {
           
        }

        #endregion

        // 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        private void uGridEquipCCSType_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridEquipCCSType, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uGridEquipCCSType_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            //RowSelector란에 편집이미지를 나타나게함
            QRPGlobal grdImg = new QRPGlobal();
            e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

        }
    }
}
