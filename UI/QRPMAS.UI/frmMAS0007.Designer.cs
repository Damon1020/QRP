﻿namespace QRPMAS.UI
{
    partial class frmMAS0007
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0007));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            this.uGridMateriallist = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uComboUseFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboInspectTypeFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelInstpectTypeFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uComboMaterialGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMaterialGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uComboConsumableType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboMaterialType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelMaterialType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelConsumableType = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSpec = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSpec = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUseflag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uTextMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor6 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraComboEditor7 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor8 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor9 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraComboEditor10 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraComboEditor11 = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.ultraLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel11 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel12 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel13 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraLabel14 = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchMaterialName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboInspectFlag = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchConsumableType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchInspect = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSearchMaterialCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchMaterialCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchConsumableType = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchMaterialType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchMaterialType = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGridMateriallist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectTypeFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterialGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboConsumableType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterialType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialType)).BeginInit();
            this.SuspendLayout();
            // 
            // uGridMateriallist
            // 
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridMateriallist.DisplayLayout.Appearance = appearance2;
            this.uGridMateriallist.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridMateriallist.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance3.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance3.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMateriallist.DisplayLayout.GroupByBox.Appearance = appearance3;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMateriallist.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridMateriallist.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridMateriallist.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridMateriallist.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridMateriallist.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridMateriallist.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridMateriallist.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridMateriallist.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridMateriallist.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridMateriallist.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridMateriallist.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridMateriallist.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridMateriallist.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridMateriallist.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridMateriallist.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridMateriallist.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridMateriallist.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridMateriallist.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridMateriallist.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridMateriallist.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridMateriallist.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridMateriallist.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridMateriallist.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridMateriallist.Location = new System.Drawing.Point(0, 100);
            this.uGridMateriallist.Name = "uGridMateriallist";
            this.uGridMateriallist.Size = new System.Drawing.Size(1070, 720);
            this.uGridMateriallist.TabIndex = 2;
            this.uGridMateriallist.Text = "ultraGrid1";
            this.uGridMateriallist.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridMateriallist_DoubleClickRow);
            this.uGridMateriallist.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridMateriallist_CellChange);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 170);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 675);
            this.uGroupBoxContentsArea.TabIndex = 3;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboUseFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboInspectTypeFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelInstpectTypeFlag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboMaterialGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialGroup);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboConsumableType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboMaterialType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uComboPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelConsumableType);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextSpec);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelSpec);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelPlant);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelUseflag);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialNameEn);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialName);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialNameCh);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextMaterialCode);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelMaterialCode);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 655);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uComboUseFlag
            // 
            this.uComboUseFlag.Location = new System.Drawing.Point(520, 136);
            this.uComboUseFlag.Name = "uComboUseFlag";
            this.uComboUseFlag.Size = new System.Drawing.Size(130, 21);
            this.uComboUseFlag.TabIndex = 33;
            this.uComboUseFlag.Text = "ultraComboEditor3";
            // 
            // uComboInspectTypeFlag
            // 
            this.uComboInspectTypeFlag.Location = new System.Drawing.Point(176, 136);
            this.uComboInspectTypeFlag.MaxLength = 50;
            this.uComboInspectTypeFlag.Name = "uComboInspectTypeFlag";
            this.uComboInspectTypeFlag.Size = new System.Drawing.Size(176, 21);
            this.uComboInspectTypeFlag.TabIndex = 32;
            // 
            // uLabelInstpectTypeFlag
            // 
            this.uLabelInstpectTypeFlag.Location = new System.Drawing.Point(20, 136);
            this.uLabelInstpectTypeFlag.Name = "uLabelInstpectTypeFlag";
            this.uLabelInstpectTypeFlag.Size = new System.Drawing.Size(150, 20);
            this.uLabelInstpectTypeFlag.TabIndex = 31;
            this.uLabelInstpectTypeFlag.Text = "검사대상여부";
            // 
            // uComboMaterialGroup
            // 
            this.uComboMaterialGroup.Location = new System.Drawing.Point(176, 64);
            this.uComboMaterialGroup.MaxLength = 50;
            this.uComboMaterialGroup.Name = "uComboMaterialGroup";
            this.uComboMaterialGroup.Size = new System.Drawing.Size(176, 21);
            this.uComboMaterialGroup.TabIndex = 30;
            // 
            // uLabelMaterialGroup
            // 
            this.uLabelMaterialGroup.Location = new System.Drawing.Point(20, 64);
            this.uLabelMaterialGroup.Name = "uLabelMaterialGroup";
            this.uLabelMaterialGroup.Size = new System.Drawing.Size(150, 20);
            this.uLabelMaterialGroup.TabIndex = 29;
            this.uLabelMaterialGroup.Text = "자재그룹";
            // 
            // uComboConsumableType
            // 
            this.uComboConsumableType.Location = new System.Drawing.Point(176, 112);
            this.uComboConsumableType.MaxLength = 50;
            this.uComboConsumableType.Name = "uComboConsumableType";
            this.uComboConsumableType.Size = new System.Drawing.Size(176, 21);
            this.uComboConsumableType.TabIndex = 28;
            // 
            // uComboMaterialType
            // 
            this.uComboMaterialType.Location = new System.Drawing.Point(176, 88);
            this.uComboMaterialType.MaxLength = 50;
            this.uComboMaterialType.Name = "uComboMaterialType";
            this.uComboMaterialType.Size = new System.Drawing.Size(176, 21);
            this.uComboMaterialType.TabIndex = 26;
            // 
            // uLabelMaterialType
            // 
            this.uLabelMaterialType.Location = new System.Drawing.Point(20, 88);
            this.uLabelMaterialType.Name = "uLabelMaterialType";
            this.uLabelMaterialType.Size = new System.Drawing.Size(150, 20);
            this.uLabelMaterialType.TabIndex = 27;
            this.uLabelMaterialType.Text = "자재유형";
            // 
            // uComboPlant
            // 
            appearance28.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Appearance = appearance28;
            this.uComboPlant.BackColor = System.Drawing.Color.PowderBlue;
            this.uComboPlant.Location = new System.Drawing.Point(176, 16);
            this.uComboPlant.MaxLength = 50;
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(176, 21);
            this.uComboPlant.TabIndex = 25;
            // 
            // uLabelConsumableType
            // 
            this.uLabelConsumableType.Location = new System.Drawing.Point(20, 112);
            this.uLabelConsumableType.Name = "uLabelConsumableType";
            this.uLabelConsumableType.Size = new System.Drawing.Size(150, 20);
            this.uLabelConsumableType.TabIndex = 23;
            this.uLabelConsumableType.Text = "자재종류";
            // 
            // uTextSpec
            // 
            appearance29.BackColor = System.Drawing.Color.White;
            this.uTextSpec.Appearance = appearance29;
            this.uTextSpec.BackColor = System.Drawing.Color.White;
            this.uTextSpec.Location = new System.Drawing.Point(520, 112);
            this.uTextSpec.Name = "uTextSpec";
            this.uTextSpec.Size = new System.Drawing.Size(372, 21);
            this.uTextSpec.TabIndex = 22;
            // 
            // uLabelSpec
            // 
            this.uLabelSpec.Location = new System.Drawing.Point(364, 112);
            this.uLabelSpec.Name = "uLabelSpec";
            this.uLabelSpec.Size = new System.Drawing.Size(150, 20);
            this.uLabelSpec.TabIndex = 21;
            this.uLabelSpec.Text = "SPEC";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(20, 16);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(150, 20);
            this.uLabelPlant.TabIndex = 16;
            this.uLabelPlant.Text = "공장";
            // 
            // uLabelUseflag
            // 
            this.uLabelUseflag.Location = new System.Drawing.Point(364, 136);
            this.uLabelUseflag.Name = "uLabelUseflag";
            this.uLabelUseflag.Size = new System.Drawing.Size(150, 20);
            this.uLabelUseflag.TabIndex = 10;
            this.uLabelUseflag.Text = "사용여부";
            // 
            // uTextMaterialNameEn
            // 
            appearance30.BackColor = System.Drawing.Color.White;
            this.uTextMaterialNameEn.Appearance = appearance30;
            this.uTextMaterialNameEn.BackColor = System.Drawing.Color.White;
            this.uTextMaterialNameEn.Location = new System.Drawing.Point(520, 88);
            this.uTextMaterialNameEn.Name = "uTextMaterialNameEn";
            this.uTextMaterialNameEn.Size = new System.Drawing.Size(372, 21);
            this.uTextMaterialNameEn.TabIndex = 7;
            // 
            // uLabelMaterialNameEn
            // 
            this.uLabelMaterialNameEn.Location = new System.Drawing.Point(364, 88);
            this.uLabelMaterialNameEn.Name = "uLabelMaterialNameEn";
            this.uLabelMaterialNameEn.Size = new System.Drawing.Size(150, 20);
            this.uLabelMaterialNameEn.TabIndex = 6;
            this.uLabelMaterialNameEn.Text = "자재설명(영문)";
            // 
            // uTextMaterialName
            // 
            appearance31.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMaterialName.Appearance = appearance31;
            this.uTextMaterialName.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMaterialName.Location = new System.Drawing.Point(520, 40);
            this.uTextMaterialName.Name = "uTextMaterialName";
            this.uTextMaterialName.Size = new System.Drawing.Size(372, 21);
            this.uTextMaterialName.TabIndex = 5;
            // 
            // uLabelMaterialName
            // 
            this.uLabelMaterialName.Location = new System.Drawing.Point(364, 40);
            this.uLabelMaterialName.Name = "uLabelMaterialName";
            this.uLabelMaterialName.Size = new System.Drawing.Size(150, 20);
            this.uLabelMaterialName.TabIndex = 4;
            this.uLabelMaterialName.Text = "자재설명";
            // 
            // uTextMaterialNameCh
            // 
            appearance32.BackColor = System.Drawing.Color.White;
            this.uTextMaterialNameCh.Appearance = appearance32;
            this.uTextMaterialNameCh.BackColor = System.Drawing.Color.White;
            this.uTextMaterialNameCh.Location = new System.Drawing.Point(520, 64);
            this.uTextMaterialNameCh.Name = "uTextMaterialNameCh";
            this.uTextMaterialNameCh.Size = new System.Drawing.Size(372, 21);
            this.uTextMaterialNameCh.TabIndex = 3;
            // 
            // uLabelMaterialNameCh
            // 
            this.uLabelMaterialNameCh.Location = new System.Drawing.Point(364, 64);
            this.uLabelMaterialNameCh.Name = "uLabelMaterialNameCh";
            this.uLabelMaterialNameCh.Size = new System.Drawing.Size(150, 20);
            this.uLabelMaterialNameCh.TabIndex = 2;
            this.uLabelMaterialNameCh.Text = "자재설명(중문)";
            // 
            // uTextMaterialCode
            // 
            appearance33.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMaterialCode.Appearance = appearance33;
            this.uTextMaterialCode.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextMaterialCode.Location = new System.Drawing.Point(176, 40);
            this.uTextMaterialCode.Name = "uTextMaterialCode";
            this.uTextMaterialCode.Size = new System.Drawing.Size(176, 21);
            this.uTextMaterialCode.TabIndex = 1;
            // 
            // uLabelMaterialCode
            // 
            this.uLabelMaterialCode.Location = new System.Drawing.Point(20, 40);
            this.uLabelMaterialCode.Name = "uLabelMaterialCode";
            this.uLabelMaterialCode.Size = new System.Drawing.Size(150, 20);
            this.uLabelMaterialCode.TabIndex = 0;
            this.uLabelMaterialCode.Text = "자재코드";
            // 
            // ultraLabel4
            // 
            this.ultraLabel4.Location = new System.Drawing.Point(344, 112);
            this.ultraLabel4.Name = "ultraLabel4";
            this.ultraLabel4.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel4.TabIndex = 21;
            this.ultraLabel4.Text = "SPEC";
            // 
            // ultraComboEditor6
            // 
            this.ultraComboEditor6.Location = new System.Drawing.Point(124, 160);
            this.ultraComboEditor6.Name = "ultraComboEditor6";
            this.ultraComboEditor6.Size = new System.Drawing.Size(130, 21);
            this.ultraComboEditor6.TabIndex = 33;
            this.ultraComboEditor6.Text = "ultraComboEditor3";
            // 
            // ultraComboEditor7
            // 
            this.ultraComboEditor7.Location = new System.Drawing.Point(124, 136);
            this.ultraComboEditor7.MaxLength = 50;
            this.ultraComboEditor7.Name = "ultraComboEditor7";
            this.ultraComboEditor7.Size = new System.Drawing.Size(176, 21);
            this.ultraComboEditor7.TabIndex = 32;
            // 
            // ultraLabel5
            // 
            this.ultraLabel5.Location = new System.Drawing.Point(20, 136);
            this.ultraLabel5.Name = "ultraLabel5";
            this.ultraLabel5.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel5.TabIndex = 31;
            this.ultraLabel5.Text = "검사대상여부";
            // 
            // ultraComboEditor8
            // 
            this.ultraComboEditor8.Location = new System.Drawing.Point(124, 64);
            this.ultraComboEditor8.MaxLength = 50;
            this.ultraComboEditor8.Name = "ultraComboEditor8";
            this.ultraComboEditor8.Size = new System.Drawing.Size(176, 21);
            this.ultraComboEditor8.TabIndex = 30;
            // 
            // ultraLabel6
            // 
            this.ultraLabel6.Location = new System.Drawing.Point(20, 64);
            this.ultraLabel6.Name = "ultraLabel6";
            this.ultraLabel6.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel6.TabIndex = 29;
            this.ultraLabel6.Text = "자재그룹";
            // 
            // ultraComboEditor9
            // 
            this.ultraComboEditor9.Location = new System.Drawing.Point(124, 112);
            this.ultraComboEditor9.MaxLength = 50;
            this.ultraComboEditor9.Name = "ultraComboEditor9";
            this.ultraComboEditor9.Size = new System.Drawing.Size(176, 21);
            this.ultraComboEditor9.TabIndex = 28;
            // 
            // ultraComboEditor10
            // 
            this.ultraComboEditor10.Location = new System.Drawing.Point(124, 88);
            this.ultraComboEditor10.MaxLength = 50;
            this.ultraComboEditor10.Name = "ultraComboEditor10";
            this.ultraComboEditor10.Size = new System.Drawing.Size(176, 21);
            this.ultraComboEditor10.TabIndex = 26;
            // 
            // ultraLabel7
            // 
            this.ultraLabel7.Location = new System.Drawing.Point(20, 88);
            this.ultraLabel7.Name = "ultraLabel7";
            this.ultraLabel7.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel7.TabIndex = 27;
            this.ultraLabel7.Text = "자재유형";
            // 
            // ultraComboEditor11
            // 
            appearance26.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor11.Appearance = appearance26;
            this.ultraComboEditor11.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraComboEditor11.Location = new System.Drawing.Point(124, 16);
            this.ultraComboEditor11.MaxLength = 50;
            this.ultraComboEditor11.Name = "ultraComboEditor11";
            this.ultraComboEditor11.Size = new System.Drawing.Size(176, 21);
            this.ultraComboEditor11.TabIndex = 25;
            // 
            // ultraLabel8
            // 
            this.ultraLabel8.Location = new System.Drawing.Point(20, 112);
            this.ultraLabel8.Name = "ultraLabel8";
            this.ultraLabel8.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel8.TabIndex = 23;
            this.ultraLabel8.Text = "자재종류";
            // 
            // ultraTextEditor1
            // 
            appearance16.BackColor = System.Drawing.Color.White;
            this.ultraTextEditor1.Appearance = appearance16;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.White;
            this.ultraTextEditor1.Location = new System.Drawing.Point(448, 112);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.ReadOnly = true;
            this.ultraTextEditor1.Size = new System.Drawing.Size(372, 21);
            this.ultraTextEditor1.TabIndex = 22;
            // 
            // ultraLabel9
            // 
            this.ultraLabel9.Location = new System.Drawing.Point(20, 16);
            this.ultraLabel9.Name = "ultraLabel9";
            this.ultraLabel9.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel9.TabIndex = 16;
            this.ultraLabel9.Text = "공장";
            // 
            // ultraLabel10
            // 
            this.ultraLabel10.Location = new System.Drawing.Point(20, 160);
            this.ultraLabel10.Name = "ultraLabel10";
            this.ultraLabel10.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel10.TabIndex = 10;
            this.ultraLabel10.Text = "사용여부";
            // 
            // ultraTextEditor2
            // 
            appearance17.BackColor = System.Drawing.Color.White;
            this.ultraTextEditor2.Appearance = appearance17;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.White;
            this.ultraTextEditor2.Location = new System.Drawing.Point(448, 88);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(372, 21);
            this.ultraTextEditor2.TabIndex = 7;
            // 
            // ultraLabel11
            // 
            this.ultraLabel11.Location = new System.Drawing.Point(344, 88);
            this.ultraLabel11.Name = "ultraLabel11";
            this.ultraLabel11.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel11.TabIndex = 6;
            this.ultraLabel11.Text = "자재설명(영문)";
            // 
            // ultraTextEditor3
            // 
            appearance15.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor3.Appearance = appearance15;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor3.Location = new System.Drawing.Point(448, 40);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.ReadOnly = true;
            this.ultraTextEditor3.Size = new System.Drawing.Size(372, 21);
            this.ultraTextEditor3.TabIndex = 5;
            // 
            // ultraLabel12
            // 
            this.ultraLabel12.Location = new System.Drawing.Point(344, 40);
            this.ultraLabel12.Name = "ultraLabel12";
            this.ultraLabel12.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel12.TabIndex = 4;
            this.ultraLabel12.Text = "자재설명";
            // 
            // ultraTextEditor4
            // 
            appearance18.BackColor = System.Drawing.Color.White;
            this.ultraTextEditor4.Appearance = appearance18;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.White;
            this.ultraTextEditor4.Location = new System.Drawing.Point(448, 64);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.ReadOnly = true;
            this.ultraTextEditor4.Size = new System.Drawing.Size(372, 21);
            this.ultraTextEditor4.TabIndex = 3;
            // 
            // ultraLabel13
            // 
            this.ultraLabel13.Location = new System.Drawing.Point(344, 64);
            this.ultraLabel13.Name = "ultraLabel13";
            this.ultraLabel13.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel13.TabIndex = 2;
            this.ultraLabel13.Text = "자재설명(중문)";
            // 
            // ultraTextEditor5
            // 
            appearance25.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor5.Appearance = appearance25;
            this.ultraTextEditor5.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor5.Location = new System.Drawing.Point(124, 40);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.ReadOnly = true;
            this.ultraTextEditor5.Size = new System.Drawing.Size(176, 21);
            this.ultraTextEditor5.TabIndex = 1;
            // 
            // ultraLabel14
            // 
            this.ultraLabel14.Location = new System.Drawing.Point(20, 40);
            this.ultraLabel14.Name = "ultraLabel14";
            this.ultraLabel14.Size = new System.Drawing.Size(100, 20);
            this.ultraLabel14.TabIndex = 0;
            this.ultraLabel14.Text = "자재코드";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 4;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboInspectFlag);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchInspect);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialCode);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchConsumableType);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchMaterialType);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchMaterialType);
            this.uGroupBoxSearchArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 60);
            this.uGroupBoxSearchArea.TabIndex = 6;
            // 
            // uTextSearchMaterialName
            // 
            appearance21.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Appearance = appearance21;
            this.uTextSearchMaterialName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSearchMaterialName.Location = new System.Drawing.Point(544, 12);
            this.uTextSearchMaterialName.Name = "uTextSearchMaterialName";
            this.uTextSearchMaterialName.ReadOnly = true;
            this.uTextSearchMaterialName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchMaterialName.TabIndex = 7;
            // 
            // uComboInspectFlag
            // 
            this.uComboInspectFlag.Location = new System.Drawing.Point(420, 36);
            this.uComboInspectFlag.MaxLength = 5;
            this.uComboInspectFlag.Name = "uComboInspectFlag";
            this.uComboInspectFlag.Size = new System.Drawing.Size(120, 21);
            this.uComboInspectFlag.TabIndex = 4;
            // 
            // uComboSearchConsumableType
            // 
            this.uComboSearchConsumableType.Location = new System.Drawing.Point(115, 35);
            this.uComboSearchConsumableType.MaxLength = 50;
            this.uComboSearchConsumableType.Name = "uComboSearchConsumableType";
            this.uComboSearchConsumableType.Size = new System.Drawing.Size(141, 21);
            this.uComboSearchConsumableType.TabIndex = 3;
            // 
            // uLabelSearchInspect
            // 
            this.uLabelSearchInspect.Location = new System.Drawing.Point(316, 36);
            this.uLabelSearchInspect.Name = "uLabelSearchInspect";
            this.uLabelSearchInspect.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchInspect.TabIndex = 6;
            this.uLabelSearchInspect.Text = "ultraLabel1";
            // 
            // uTextSearchMaterialCode
            // 
            appearance20.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance20.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance20;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSearchMaterialCode.ButtonsRight.Add(editorButton1);
            this.uTextSearchMaterialCode.Location = new System.Drawing.Point(420, 12);
            this.uTextSearchMaterialCode.MaxLength = 20;
            this.uTextSearchMaterialCode.Name = "uTextSearchMaterialCode";
            this.uTextSearchMaterialCode.Size = new System.Drawing.Size(120, 21);
            this.uTextSearchMaterialCode.TabIndex = 2;
            this.uTextSearchMaterialCode.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSearchMaterialCode_EditorButtonClick);
            // 
            // uLabelSearchMaterialCode
            // 
            this.uLabelSearchMaterialCode.Location = new System.Drawing.Point(316, 12);
            this.uLabelSearchMaterialCode.Name = "uLabelSearchMaterialCode";
            this.uLabelSearchMaterialCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterialCode.TabIndex = 6;
            this.uLabelSearchMaterialCode.Text = "ultraLabel1";
            // 
            // uLabelSearchConsumableType
            // 
            this.uLabelSearchConsumableType.Location = new System.Drawing.Point(11, 35);
            this.uLabelSearchConsumableType.Name = "uLabelSearchConsumableType";
            this.uLabelSearchConsumableType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchConsumableType.TabIndex = 6;
            this.uLabelSearchConsumableType.Text = "ultraLabel1";
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboSearchPlant.TabIndex = 1;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 4;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // uComboSearchMaterialType
            // 
            this.uComboSearchMaterialType.Location = new System.Drawing.Point(688, 36);
            this.uComboSearchMaterialType.MaxLength = 50;
            this.uComboSearchMaterialType.Name = "uComboSearchMaterialType";
            this.uComboSearchMaterialType.Size = new System.Drawing.Size(136, 21);
            this.uComboSearchMaterialType.TabIndex = 5;
            // 
            // uLabelSearchMaterialType
            // 
            this.uLabelSearchMaterialType.Location = new System.Drawing.Point(584, 36);
            this.uLabelSearchMaterialType.Name = "uLabelSearchMaterialType";
            this.uLabelSearchMaterialType.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchMaterialType.TabIndex = 2;
            this.uLabelSearchMaterialType.Text = "ultraLabel2";
            // 
            // frmMAS0007
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridMateriallist);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0007";
            this.Load += new System.EventHandler(this.frmMAS0007_Load);
            this.Activated += new System.EventHandler(this.frmMAS0007_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0007_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGridMateriallist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectTypeFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterialGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboConsumableType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboMaterialType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraComboEditor11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboInspectFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchConsumableType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchMaterialCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchMaterialType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGridMateriallist;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialName;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialNameCh;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextMaterialNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelUseflag;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelSpec;
        private Infragistics.Win.Misc.UltraLabel uLabelConsumableType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMaterialType;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboConsumableType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectTypeFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelInstpectTypeFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboMaterialGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelMaterialGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboUseFlag;
        private Infragistics.Win.Misc.UltraLabel ultraLabel4;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor6;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor7;
        private Infragistics.Win.Misc.UltraLabel ultraLabel5;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor8;
        private Infragistics.Win.Misc.UltraLabel ultraLabel6;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor9;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor10;
        private Infragistics.Win.Misc.UltraLabel ultraLabel7;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor ultraComboEditor11;
        private Infragistics.Win.Misc.UltraLabel ultraLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.Misc.UltraLabel ultraLabel9;
        private Infragistics.Win.Misc.UltraLabel ultraLabel10;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.Misc.UltraLabel ultraLabel11;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.Misc.UltraLabel ultraLabel12;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.Misc.UltraLabel ultraLabel13;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.Misc.UltraLabel ultraLabel14;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboInspectFlag;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchConsumableType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchInspect;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialCode;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchConsumableType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchMaterialType;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchMaterialType;
    }
}