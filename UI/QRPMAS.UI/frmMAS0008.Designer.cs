﻿namespace QRPMAS.UI
{
    partial class frmMAS0008
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0008));
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            this.uGridInven = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.uGroupBoxPanel = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGroupBoxSection = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridSectionInfo = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxInvenInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUseFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInventoryNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInventoryNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInventoryName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextInventoryCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlant = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelInvenNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInvenNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInvenName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelInvenCode = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            ((System.ComponentModel.ISupportInitialize)(this.uGridInven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.uGroupBoxPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSection)).BeginInit();
            this.uGroupBoxSection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridSectionInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInvenInfo)).BeginInit();
            this.uGroupBoxInvenInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInventoryNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInventoryNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInventoryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInventoryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // uGridInven
            // 
            this.uGridInven.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridInven.DisplayLayout.Appearance = appearance4;
            this.uGridInven.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridInven.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance1.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInven.DisplayLayout.GroupByBox.Appearance = appearance1;
            appearance2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInven.DisplayLayout.GroupByBox.BandLabelAppearance = appearance2;
            this.uGridInven.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance3.BackColor2 = System.Drawing.SystemColors.Control;
            appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridInven.DisplayLayout.GroupByBox.PromptAppearance = appearance3;
            this.uGridInven.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridInven.DisplayLayout.MaxRowScrollRegions = 1;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridInven.DisplayLayout.Override.ActiveCellAppearance = appearance12;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridInven.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridInven.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridInven.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            this.uGridInven.DisplayLayout.Override.CardAreaAppearance = appearance6;
            appearance5.BorderColor = System.Drawing.Color.Silver;
            appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridInven.DisplayLayout.Override.CellAppearance = appearance5;
            this.uGridInven.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridInven.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridInven.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance11.TextHAlignAsString = "Left";
            this.uGridInven.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridInven.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridInven.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            appearance10.BorderColor = System.Drawing.Color.Silver;
            this.uGridInven.DisplayLayout.Override.RowAppearance = appearance10;
            this.uGridInven.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance8.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridInven.DisplayLayout.Override.TemplateAddRowAppearance = appearance8;
            this.uGridInven.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridInven.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridInven.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridInven.Location = new System.Drawing.Point(0, 80);
            this.uGridInven.Name = "uGridInven";
            this.uGridInven.Size = new System.Drawing.Size(1070, 760);
            this.uGridInven.TabIndex = 1;
            this.uGridInven.Text = "ultraGrid1";
            this.uGridInven.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridInven_DoubleClickCell);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxContentsArea.Controls.Add(this.uGroupBoxPanel);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 2;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // uGroupBoxPanel
            // 
            this.uGroupBoxPanel.Controls.Add(this.uGroupBoxSection);
            this.uGroupBoxPanel.Controls.Add(this.uGroupBoxInvenInfo);
            this.uGroupBoxPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBoxPanel.Location = new System.Drawing.Point(3, 17);
            this.uGroupBoxPanel.Name = "uGroupBoxPanel";
            this.uGroupBoxPanel.Size = new System.Drawing.Size(1064, 695);
            this.uGroupBoxPanel.TabIndex = 0;
            // 
            // uGroupBoxSection
            // 
            this.uGroupBoxSection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxSection.Controls.Add(this.uGridSectionInfo);
            this.uGroupBoxSection.Location = new System.Drawing.Point(12, 100);
            this.uGroupBoxSection.Name = "uGroupBoxSection";
            this.uGroupBoxSection.Size = new System.Drawing.Size(1040, 588);
            this.uGroupBoxSection.TabIndex = 1;
            // 
            // uGridSectionInfo
            // 
            this.uGridSectionInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridSectionInfo.DisplayLayout.Appearance = appearance49;
            this.uGridSectionInfo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridSectionInfo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSectionInfo.DisplayLayout.GroupByBox.Appearance = appearance50;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSectionInfo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance51;
            this.uGridSectionInfo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridSectionInfo.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.uGridSectionInfo.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridSectionInfo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridSectionInfo.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridSectionInfo.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.uGridSectionInfo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridSectionInfo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.uGridSectionInfo.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridSectionInfo.DisplayLayout.Override.CellAppearance = appearance56;
            this.uGridSectionInfo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridSectionInfo.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridSectionInfo.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.uGridSectionInfo.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.uGridSectionInfo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridSectionInfo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.uGridSectionInfo.DisplayLayout.Override.RowAppearance = appearance59;
            this.uGridSectionInfo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridSectionInfo.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.uGridSectionInfo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridSectionInfo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridSectionInfo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridSectionInfo.Location = new System.Drawing.Point(12, 28);
            this.uGridSectionInfo.Name = "uGridSectionInfo";
            this.uGridSectionInfo.Size = new System.Drawing.Size(1016, 552);
            this.uGridSectionInfo.TabIndex = 0;
            // 
            // uGroupBoxInvenInfo
            // 
            this.uGroupBoxInvenInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uGroupBoxInvenInfo.Controls.Add(this.uTextUseFlag);
            this.uGroupBoxInvenInfo.Controls.Add(this.uTextInventoryNameEn);
            this.uGroupBoxInvenInfo.Controls.Add(this.uTextInventoryNameCh);
            this.uGroupBoxInvenInfo.Controls.Add(this.uTextInventoryName);
            this.uGroupBoxInvenInfo.Controls.Add(this.uTextInventoryCode);
            this.uGroupBoxInvenInfo.Controls.Add(this.uTextPlant);
            this.uGroupBoxInvenInfo.Controls.Add(this.uLabelInvenNameEn);
            this.uGroupBoxInvenInfo.Controls.Add(this.uLabelInvenNameCh);
            this.uGroupBoxInvenInfo.Controls.Add(this.uLabelPlant);
            this.uGroupBoxInvenInfo.Controls.Add(this.uLabelFlag);
            this.uGroupBoxInvenInfo.Controls.Add(this.uLabelInvenName);
            this.uGroupBoxInvenInfo.Controls.Add(this.uLabelInvenCode);
            this.uGroupBoxInvenInfo.Location = new System.Drawing.Point(12, 14);
            this.uGroupBoxInvenInfo.Name = "uGroupBoxInvenInfo";
            this.uGroupBoxInvenInfo.Size = new System.Drawing.Size(1040, 82);
            this.uGroupBoxInvenInfo.TabIndex = 0;
            // 
            // uTextUseFlag
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseFlag.Appearance = appearance14;
            this.uTextUseFlag.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUseFlag.Location = new System.Drawing.Point(644, 28);
            this.uTextUseFlag.Name = "uTextUseFlag";
            this.uTextUseFlag.ReadOnly = true;
            this.uTextUseFlag.Size = new System.Drawing.Size(150, 21);
            this.uTextUseFlag.TabIndex = 19;
            // 
            // uTextInventoryNameEn
            // 
            appearance15.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInventoryNameEn.Appearance = appearance15;
            this.uTextInventoryNameEn.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInventoryNameEn.Location = new System.Drawing.Point(644, 52);
            this.uTextInventoryNameEn.Name = "uTextInventoryNameEn";
            this.uTextInventoryNameEn.ReadOnly = true;
            this.uTextInventoryNameEn.Size = new System.Drawing.Size(150, 21);
            this.uTextInventoryNameEn.TabIndex = 18;
            // 
            // uTextInventoryNameCh
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInventoryNameCh.Appearance = appearance16;
            this.uTextInventoryNameCh.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInventoryNameCh.Location = new System.Drawing.Point(380, 52);
            this.uTextInventoryNameCh.Name = "uTextInventoryNameCh";
            this.uTextInventoryNameCh.ReadOnly = true;
            this.uTextInventoryNameCh.Size = new System.Drawing.Size(150, 21);
            this.uTextInventoryNameCh.TabIndex = 17;
            // 
            // uTextInventoryName
            // 
            appearance17.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInventoryName.Appearance = appearance17;
            this.uTextInventoryName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInventoryName.Location = new System.Drawing.Point(116, 52);
            this.uTextInventoryName.Name = "uTextInventoryName";
            this.uTextInventoryName.ReadOnly = true;
            this.uTextInventoryName.Size = new System.Drawing.Size(150, 21);
            this.uTextInventoryName.TabIndex = 16;
            // 
            // uTextInventoryCode
            // 
            appearance18.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInventoryCode.Appearance = appearance18;
            this.uTextInventoryCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextInventoryCode.Location = new System.Drawing.Point(380, 28);
            this.uTextInventoryCode.Name = "uTextInventoryCode";
            this.uTextInventoryCode.ReadOnly = true;
            this.uTextInventoryCode.Size = new System.Drawing.Size(150, 21);
            this.uTextInventoryCode.TabIndex = 15;
            // 
            // uTextPlant
            // 
            appearance19.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Appearance = appearance19;
            this.uTextPlant.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlant.Location = new System.Drawing.Point(116, 28);
            this.uTextPlant.Name = "uTextPlant";
            this.uTextPlant.ReadOnly = true;
            this.uTextPlant.Size = new System.Drawing.Size(150, 21);
            this.uTextPlant.TabIndex = 14;
            // 
            // uLabelInvenNameEn
            // 
            this.uLabelInvenNameEn.Location = new System.Drawing.Point(540, 52);
            this.uLabelInvenNameEn.Name = "uLabelInvenNameEn";
            this.uLabelInvenNameEn.Size = new System.Drawing.Size(100, 20);
            this.uLabelInvenNameEn.TabIndex = 13;
            this.uLabelInvenNameEn.Text = "창고명_영문";
            // 
            // uLabelInvenNameCh
            // 
            this.uLabelInvenNameCh.Location = new System.Drawing.Point(276, 52);
            this.uLabelInvenNameCh.Name = "uLabelInvenNameCh";
            this.uLabelInvenNameCh.Size = new System.Drawing.Size(100, 20);
            this.uLabelInvenNameCh.TabIndex = 11;
            this.uLabelInvenNameCh.Text = "창고명_중문";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 28);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 0;
            this.uLabelPlant.Text = "공장";
            // 
            // uLabelFlag
            // 
            this.uLabelFlag.Location = new System.Drawing.Point(540, 28);
            this.uLabelFlag.Name = "uLabelFlag";
            this.uLabelFlag.Size = new System.Drawing.Size(100, 20);
            this.uLabelFlag.TabIndex = 7;
            this.uLabelFlag.Text = "사용여부";
            // 
            // uLabelInvenName
            // 
            this.uLabelInvenName.Location = new System.Drawing.Point(12, 52);
            this.uLabelInvenName.Name = "uLabelInvenName";
            this.uLabelInvenName.Size = new System.Drawing.Size(100, 20);
            this.uLabelInvenName.TabIndex = 2;
            this.uLabelInvenName.Text = "창고명";
            // 
            // uLabelInvenCode
            // 
            this.uLabelInvenCode.Location = new System.Drawing.Point(276, 28);
            this.uLabelInvenCode.Name = "uLabelInvenCode";
            this.uLabelInvenCode.Size = new System.Drawing.Size(100, 20);
            this.uLabelInvenCode.TabIndex = 0;
            this.uLabelInvenCode.Text = "창고코드";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 3;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance13;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 5;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(150, 21);
            this.uComboSearchPlant.TabIndex = 1;
            this.uComboSearchPlant.Text = "ultraComboEditor1";
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            this.uLabelSearchPlant.Text = "ultraLabel1";
            // 
            // frmMAS0008
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridInven);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0008";
            this.Load += new System.EventHandler(this.frmMAS0008_Load);
            this.Activated += new System.EventHandler(this.frmMAS0008_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0008_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGridInven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.uGroupBoxPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSection)).EndInit();
            this.uGroupBoxSection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridSectionInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxInvenInfo)).EndInit();
            this.uGroupBoxInvenInfo.ResumeLayout(false);
            this.uGroupBoxInvenInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInventoryNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInventoryNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInventoryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextInventoryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid uGridInven;
        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel uGroupBoxPanel;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxInvenInfo;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSection;
        private Infragistics.Win.Misc.UltraLabel uLabelInvenCode;
        private Infragistics.Win.Misc.UltraLabel uLabelInvenName;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelInvenNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelInvenNameCh;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridSectionInfo;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInventoryNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInventoryNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInventoryName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextInventoryCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
    }
}