﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통기준정보                                          */
/* 모듈(분류)명 : 자재관리 기준정보                                     */
/* 프로그램ID   : frmMAS0008.cs                                         */
/* 프로그램명   : 창고정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMAS0008 : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0008()
        {
            InitializeComponent();
        }

        private void frmMAS0008_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //검색,저장,삭제,새로만들기,프린트,엑셀 사용여부
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }
        private void frmMAS0008_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            //tetle text설정
            titleArea.mfSetLabelText("창고정보", m_resSys.GetString("SYS_FONTNAME"), 12);
            
            // 초기화 Method 호출
            SetToolAuth();
            initGroupBox();
            initGrid();
            initLabel();
            initComBox();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void initGrid()
        {
            // SystemInfo Resource
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                WinGrid grd = new WinGrid();
                //창고//
                //그리드초기화
                grd.mfInitGeneralGrid(this.uGridInven, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼추가
                grd.mfSetGridColumn(this.uGridInven, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInven, 0, "PlantName", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInven, 0, "InventoryCode", "창고코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInven, 0, "InventoryName", "창고명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInven, 0, "InventoryNameCh", "창고명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInven, 0, "InventoryNameEn", "창고명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                ////grd.mfSetGridColumn(this.uGridInven, 0, "InventoryType", "창고유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10
                ////, Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridInven, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                //폰트설정
                uGridInven.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridInven.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                ////////// Set DropDown
                ////////// Plant
                ////////QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                ////////brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                ////////QRPMAS.BL.MASPRC.Plant plant = new QRPMAS.BL.MASPRC.Plant();
                ////////brwChannel.mfCredentials(plant);

                ////////DataTable dtPlant = plant.mfReadPlantForCombo(m_resSys.GetString("SYS_FONTNAME"));
                
                ////////grd.mfSetGridColumnValueList(this.uGridInven, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtPlant);

                ////////// UseFlag
                //////////사용여부
                ////////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                ////////QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                ////////brwChannel.mfCredentials(clsCommonCode);

                ////////DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                ////////grd.mfSetGridColumnValueList(this.uGridInven, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);


                // Section //
                //그리드초기화
                grd.mfInitGeneralGrid(this.uGridSectionInfo, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                ////grd.mfSetGridColumn(this.uGridSectionInfo, 0, "Check", "", true,Infragistics.Win.UltraWinGrid.Activation.AllowEdit,30, false, false, 0, Infragistics.Win.HAlign.Center,
                ////    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridSectionInfo, 0, "SectionNum", "SectionNum", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSectionInfo, 0, "SectionName", "Section 명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSectionInfo, 0, "SectionNameCh", "Section명_중문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSectionInfo, 0, "SectionNameEn", "Section명_영문", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 150, false, false, 50, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit,"", "", "");

                grd.mfSetGridColumn(this.uGridSectionInfo, 0, "ColumnNum", "열", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSectionInfo, 0, "RowNum", "단", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 10, Infragistics.Win.HAlign.Right,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSectionInfo, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 80, false, false, 100, Infragistics.Win.HAlign.Left,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridSectionInfo, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 100, Infragistics.Win.HAlign.Center,
                    Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                
                //폰트설정
                uGridSectionInfo.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridSectionInfo.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                

                grd.mfAddRowGrid(this.uGridSectionInfo, 0);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void initLabel()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);

                lbl.mfSetLabel(this.uLabelInvenCode, "창고코드", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInvenName, "창고명", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInvenNameCh, "창고명_중문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelInvenNameEn, "창고명_영문", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabelFlag, "사용여부", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그룹박스초기화
        /// </summary>
        private void initGroupBox()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGroupBox grpbox = new WinGroupBox();

                grpbox.mfSetGroupBox(this.uGroupBoxInvenInfo, GroupBoxType.INFO, "창고정보", m_resSys.GetString("SYS_FONTNAME"),
                    Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default,
                    Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                    Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);
                
                grpbox.mfSetGroupBox(this.uGroupBoxSection, GroupBoxType.DETAIL, "Section정보", m_resSys.GetString("SYS_FONTNAME"),
                    Infragistics.Win.Misc.GroupBoxViewStyle.Default, Infragistics.Win.Misc.GroupBoxHeaderPosition.Default,
                    Infragistics.Win.Misc.GroupBoxBorderStyle.RectangularSolid, Infragistics.Win.Misc.GroupBoxCaptionAlignment.Default,
                    Infragistics.Win.Misc.GroupBoxVerticalTextOrientation.Default);

                //폰트설정
                uGroupBoxInvenInfo.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxInvenInfo.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxSection.HeaderAppearance.FontData.SizeInPoints = 9;
                uGroupBoxSection.HeaderAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;

                uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 콤보박스초기화 및 Value 저장
        /// </summary>
        private void initComBox()
        {
            try
            {
                // SystemInfo Resource
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinComboEditor wCombo = new WinComboEditor();
                // BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = new DataTable();
                 
                // 함수호출
                dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                // 공장조회
                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONNAME"),
                   true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                   , "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion
       
        #region 이벤트관련
        //////// 셀 수정이 일어나면 RowSelector Image 설정하는 구문
        //////private void uGridSectionInfo_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        //////{
        //////    try
        //////    {
        //////        QRPGlobal grdImg = new QRPGlobal();
        //////        e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
        //////        QRPCOM.QRPUI.WinGrid grd = new WinGrid();
        //////        if (grd.mfCheckCellDataInRow(this.uGridSectionInfo, 0, e.Cell.Row.Index))
        //////            e.Cell.Row.Delete(false);
        //////    }
        //////    catch
        //////    {
        //////    }
        //////    finally
        //////    {
        //////    }
        //////}

        //그룹박스 펼침상태
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInven.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridInven.Height = 760;
                    for (int i = 0; i < uGridInven.Rows.Count; i++)
                    {
                        uGridInven.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Inventory 그리드 더블 클릭시...
        private void uGridInven_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }
                if (e.Cell.Value.ToString() != "")
                {
                    // SystemInfo Resource
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinGrid grd = new WinGrid();
                    
                    // 라인정보 그룹박스에 그리드의 정보를 나타내기 위한 구문
                    this.uTextPlant.Text = e.Cell.Row.Cells["PlantName"].Value.ToString();
                    this.uTextInventoryCode.Text = e.Cell.Row.Cells["InventoryCode"].Value.ToString();
                    this.uTextInventoryName.Text = e.Cell.Row.Cells["InventoryName"].Value.ToString();
                    this.uTextInventoryNameCh.Text = e.Cell.Row.Cells["InventoryNameCh"].Value.ToString();
                    this.uTextInventoryNameEn.Text = e.Cell.Row.Cells["InventoryNameEn"].Value.ToString();
                    this.uTextUseFlag.Text = e.Cell.Row.Cells["UseFlag"].Value.ToString();

                    //BL호출
                    QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                    brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Section), "Section");
                    QRPMAS.BL.MASMAT.Section clsSection = new QRPMAS.BL.MASMAT.Section();
                    brwChnnel.mfCredentials(clsSection);

                    String strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                    String strInventoryCode = e.Cell.Row.Cells["InventoryCode"].Value.ToString();

                    //함수호출
                    DataTable dtSection = clsSection.mfReadSection(strPlantCode, strInventoryCode, m_resSys.GetString("SYS_LANG"));

                    this.uGridSectionInfo.DataSource = dtSection;
                    this.uGridSectionInfo.DataBind();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바
        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {
                WinGrid grd = new WinGrid();
                //systemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();
                
                //검색 팝업창
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread thredPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASMAT.Inventory), "Inventory");
                QRPMAS.BL.MASMAT.Inventory inventory = new QRPMAS.BL.MASMAT.Inventory();
                brwChnnel.mfCredentials(inventory);

                String strPlant = this.uComboSearchPlant.Value.ToString();

                //함수호출
                DataTable dtInven = inventory.mfReadInventory(strPlant, m_resSys.GetString("SYS_LANG"));

                this.uGridInven.DataSource = dtInven;
                this.uGridInven.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                if (dtInven.Rows.Count == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information,
                        500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                }
                
                // ContentGroupBox 접은상태로
                this.uGroupBoxContentsArea.Expanded = false;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }

        }
        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
        }
        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
        }
        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
        }

        /// <summary>
        /// 출력
        /// </summary>
        public void mfPrint()
        {

        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            grd.mfDownLoadGridToExcel(this.uGridSectionInfo);
        }
        #endregion

        private void frmMAS0008_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
    }
}