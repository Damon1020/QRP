﻿namespace QRPMAS.UI
{
    partial class frmMASZ0026
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0026));
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uLabelYN = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboSearchDept = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchUserName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchDept = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextHpNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelHpNum = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTelNum = new Infragistics.Win.Misc.UltraLabel();
            this.uTextTelNum = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uGridUserList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.uGridEquip = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxLineInfo = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUserName = new Infragistics.Win.Misc.UltraLabel();
            this.uTextSubUserName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSubUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPosition = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSubUser = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelPosition = new Infragistics.Win.Misc.UltraLabel();
            this.uTextDeptName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelDeptName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelUserID = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBoxEquip = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextS = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextM = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uComboProcessGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboEquipGroup = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboEquipLoc = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboEquipLargeType = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboStation = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelProcessGroup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelS = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelM = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipGorup = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelLocation = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelEquipLargeType = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelStation = new Infragistics.Win.Misc.UltraLabel();
            this.uButtonOk = new Infragistics.Win.Misc.UltraButton();
            this.uButtonSearch = new Infragistics.Win.Misc.UltraButton();
            this.uGridEquipList = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxUser = new Infragistics.Win.Misc.UltraGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHpNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTelNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUserList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLineInfo)).BeginInit();
            this.uGroupBoxLineInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSubUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSubUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeptName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEquip)).BeginInit();
            this.uGroupBoxEquip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxUser)).BeginInit();
            this.uGroupBoxUser.SuspendLayout();
            this.SuspendLayout();
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelYN);
            this.uGroupBoxSearchArea.Controls.Add(this.ultraTextEditor1);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uTextSearchUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDept);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchUserName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchUserID);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDept);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uLabelYN
            // 
            this.uLabelYN.Location = new System.Drawing.Point(948, 12);
            this.uLabelYN.Name = "uLabelYN";
            this.uLabelYN.Size = new System.Drawing.Size(72, 20);
            this.uLabelYN.TabIndex = 9;
            // 
            // ultraTextEditor1
            // 
            appearance52.BackColor = System.Drawing.Color.Salmon;
            this.ultraTextEditor1.Appearance = appearance52;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.Salmon;
            this.ultraTextEditor1.Location = new System.Drawing.Point(1024, 12);
            this.ultraTextEditor1.Multiline = true;
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.ReadOnly = true;
            this.ultraTextEditor1.Size = new System.Drawing.Size(24, 16);
            this.ultraTextEditor1.TabIndex = 7;
            // 
            // uTextSearchUserName
            // 
            this.uTextSearchUserName.Location = new System.Drawing.Point(840, 12);
            this.uTextSearchUserName.Name = "uTextSearchUserName";
            this.uTextSearchUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchUserName.TabIndex = 12;
            // 
            // uTextSearchUserID
            // 
            this.uTextSearchUserID.Location = new System.Drawing.Point(628, 12);
            this.uTextSearchUserID.Name = "uTextSearchUserID";
            this.uTextSearchUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextSearchUserID.TabIndex = 12;
            // 
            // uComboSearchDept
            // 
            this.uComboSearchDept.Location = new System.Drawing.Point(372, 12);
            this.uComboSearchDept.MaxLength = 50;
            this.uComboSearchDept.Name = "uComboSearchDept";
            this.uComboSearchDept.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchDept.TabIndex = 4;
            // 
            // uLabelSearchUserName
            // 
            this.uLabelSearchUserName.Location = new System.Drawing.Point(736, 12);
            this.uLabelSearchUserName.Name = "uLabelSearchUserName";
            this.uLabelSearchUserName.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchUserName.TabIndex = 3;
            // 
            // uLabelSearchUserID
            // 
            this.uLabelSearchUserID.Location = new System.Drawing.Point(524, 12);
            this.uLabelSearchUserID.Name = "uLabelSearchUserID";
            this.uLabelSearchUserID.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchUserID.TabIndex = 3;
            // 
            // uLabelSearchDept
            // 
            this.uLabelSearchDept.Location = new System.Drawing.Point(268, 12);
            this.uLabelSearchDept.Name = "uLabelSearchDept";
            this.uLabelSearchDept.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchDept.TabIndex = 3;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPlant.TabIndex = 2;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchPlant
            // 
            appearance17.TextHAlignAsString = "Left";
            appearance17.TextVAlignAsString = "Middle";
            this.uLabelSearchPlant.Appearance = appearance17;
            this.uLabelSearchPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearchPlant.TabIndex = 0;
            // 
            // uTextPlantCode
            // 
            appearance24.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Appearance = appearance24;
            this.uTextPlantCode.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPlantCode.Location = new System.Drawing.Point(696, 16);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.ReadOnly = true;
            this.uTextPlantCode.Size = new System.Drawing.Size(8, 21);
            this.uTextPlantCode.TabIndex = 11;
            this.uTextPlantCode.Visible = false;
            // 
            // uTextHpNum
            // 
            appearance27.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextHpNum.Appearance = appearance27;
            this.uTextHpNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextHpNum.Location = new System.Drawing.Point(684, 16);
            this.uTextHpNum.Name = "uTextHpNum";
            this.uTextHpNum.ReadOnly = true;
            this.uTextHpNum.Size = new System.Drawing.Size(12, 21);
            this.uTextHpNum.TabIndex = 10;
            this.uTextHpNum.Visible = false;
            // 
            // uLabelHpNum
            // 
            appearance22.TextHAlignAsString = "Left";
            appearance22.TextVAlignAsString = "Middle";
            this.uLabelHpNum.Appearance = appearance22;
            this.uLabelHpNum.Location = new System.Drawing.Point(668, 16);
            this.uLabelHpNum.Name = "uLabelHpNum";
            this.uLabelHpNum.Size = new System.Drawing.Size(8, 20);
            this.uLabelHpNum.TabIndex = 9;
            this.uLabelHpNum.Visible = false;
            // 
            // uLabelTelNum
            // 
            appearance15.TextHAlignAsString = "Left";
            appearance15.TextVAlignAsString = "Middle";
            this.uLabelTelNum.Appearance = appearance15;
            this.uLabelTelNum.Location = new System.Drawing.Point(656, 16);
            this.uLabelTelNum.Name = "uLabelTelNum";
            this.uLabelTelNum.Size = new System.Drawing.Size(8, 20);
            this.uLabelTelNum.TabIndex = 0;
            this.uLabelTelNum.Visible = false;
            // 
            // uTextTelNum
            // 
            appearance16.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTelNum.Appearance = appearance16;
            this.uTextTelNum.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextTelNum.Location = new System.Drawing.Point(676, 16);
            this.uTextTelNum.Name = "uTextTelNum";
            this.uTextTelNum.ReadOnly = true;
            this.uTextTelNum.Size = new System.Drawing.Size(8, 21);
            this.uTextTelNum.TabIndex = 6;
            this.uTextTelNum.Visible = false;
            // 
            // uGridUserList
            // 
            this.uGridUserList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridUserList.DisplayLayout.Appearance = appearance5;
            this.uGridUserList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridUserList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUserList.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUserList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridUserList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridUserList.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridUserList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridUserList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridUserList.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridUserList.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridUserList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridUserList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridUserList.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridUserList.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridUserList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridUserList.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridUserList.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridUserList.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridUserList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridUserList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridUserList.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridUserList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridUserList.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridUserList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridUserList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridUserList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridUserList.Location = new System.Drawing.Point(12, 8);
            this.uGridUserList.Name = "uGridUserList";
            this.uGridUserList.Size = new System.Drawing.Size(304, 732);
            this.uGridUserList.TabIndex = 3;
            this.uGridUserList.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.uGridUserList_DoubleClickCell);
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 76);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 2;
            this.uButtonDeleteRow.Click += new System.EventHandler(this.uButtonDeleteRow_Click);
            // 
            // uGridEquip
            // 
            this.uGridEquip.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquip.DisplayLayout.Appearance = appearance29;
            this.uGridEquip.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquip.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.GroupByBox.Appearance = appearance30;
            appearance31.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.BandLabelAppearance = appearance31;
            this.uGridEquip.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance32.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance32.BackColor2 = System.Drawing.SystemColors.Control;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquip.DisplayLayout.GroupByBox.PromptAppearance = appearance32;
            this.uGridEquip.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquip.DisplayLayout.MaxRowScrollRegions = 1;
            appearance33.BackColor = System.Drawing.SystemColors.Window;
            appearance33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquip.DisplayLayout.Override.ActiveCellAppearance = appearance33;
            appearance34.BackColor = System.Drawing.SystemColors.Highlight;
            appearance34.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquip.DisplayLayout.Override.ActiveRowAppearance = appearance34;
            this.uGridEquip.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquip.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.CardAreaAppearance = appearance35;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            appearance36.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquip.DisplayLayout.Override.CellAppearance = appearance36;
            this.uGridEquip.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquip.DisplayLayout.Override.CellPadding = 0;
            appearance37.BackColor = System.Drawing.SystemColors.Control;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquip.DisplayLayout.Override.GroupByRowAppearance = appearance37;
            appearance38.TextHAlignAsString = "Left";
            this.uGridEquip.DisplayLayout.Override.HeaderAppearance = appearance38;
            this.uGridEquip.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquip.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquip.DisplayLayout.Override.RowAppearance = appearance39;
            this.uGridEquip.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquip.DisplayLayout.Override.TemplateAddRowAppearance = appearance40;
            this.uGridEquip.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquip.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquip.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquip.Location = new System.Drawing.Point(12, 84);
            this.uGridEquip.Name = "uGridEquip";
            this.uGridEquip.Size = new System.Drawing.Size(708, 136);
            this.uGridEquip.TabIndex = 1;
            this.uGridEquip.UpdateMode = Infragistics.Win.UltraWinGrid.UpdateMode.OnCellChange;
            this.uGridEquip.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridEquip_AfterCellUpdate);
            // 
            // uGroupBoxLineInfo
            // 
            this.uGroupBoxLineInfo.Controls.Add(this.uGridEquip);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextUserID);
            this.uGroupBoxLineInfo.Controls.Add(this.uButtonDeleteRow);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextUserName);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelUserName);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextPlantCode);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextSubUserName);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextHpNum);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextSubUserID);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextPosition);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelSubUser);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelHpNum);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelPosition);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelTelNum);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextTelNum);
            this.uGroupBoxLineInfo.Controls.Add(this.uTextDeptName);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelDeptName);
            this.uGroupBoxLineInfo.Controls.Add(this.uLabelUserID);
            this.uGroupBoxLineInfo.Location = new System.Drawing.Point(332, 80);
            this.uGroupBoxLineInfo.Name = "uGroupBoxLineInfo";
            this.uGroupBoxLineInfo.Size = new System.Drawing.Size(736, 236);
            this.uGroupBoxLineInfo.TabIndex = 0;
            // 
            // uTextUserID
            // 
            appearance26.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserID.Appearance = appearance26;
            this.uTextUserID.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserID.Location = new System.Drawing.Point(116, 28);
            this.uTextUserID.Name = "uTextUserID";
            this.uTextUserID.ReadOnly = true;
            this.uTextUserID.Size = new System.Drawing.Size(135, 21);
            this.uTextUserID.TabIndex = 8;
            // 
            // uTextUserName
            // 
            appearance28.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Appearance = appearance28;
            this.uTextUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextUserName.Location = new System.Drawing.Point(364, 28);
            this.uTextUserName.Name = "uTextUserName";
            this.uTextUserName.ReadOnly = true;
            this.uTextUserName.Size = new System.Drawing.Size(135, 21);
            this.uTextUserName.TabIndex = 3;
            // 
            // uLabelUserName
            // 
            appearance46.TextHAlignAsString = "Left";
            appearance46.TextVAlignAsString = "Middle";
            this.uLabelUserName.Appearance = appearance46;
            this.uLabelUserName.Location = new System.Drawing.Point(260, 28);
            this.uLabelUserName.Name = "uLabelUserName";
            this.uLabelUserName.Size = new System.Drawing.Size(100, 20);
            this.uLabelUserName.TabIndex = 0;
            // 
            // uTextSubUserName
            // 
            appearance14.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSubUserName.Appearance = appearance14;
            this.uTextSubUserName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextSubUserName.Location = new System.Drawing.Point(468, 52);
            this.uTextSubUserName.Name = "uTextSubUserName";
            this.uTextSubUserName.ReadOnly = true;
            this.uTextSubUserName.Size = new System.Drawing.Size(100, 21);
            this.uTextSubUserName.TabIndex = 5;
            // 
            // uTextSubUserID
            // 
            appearance55.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance55.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance55;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextSubUserID.ButtonsRight.Add(editorButton1);
            this.uTextSubUserID.Location = new System.Drawing.Point(364, 52);
            this.uTextSubUserID.MaxLength = 20;
            this.uTextSubUserID.Name = "uTextSubUserID";
            this.uTextSubUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextSubUserID.TabIndex = 5;
            this.uTextSubUserID.ValueChanged += new System.EventHandler(this.uTextSubUserID_ValueChanged);
            this.uTextSubUserID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uTextSubUserID_KeyDown);
            this.uTextSubUserID.EditorButtonClick += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.uTextSubUserID_EditorButtonClick);
            // 
            // uTextPosition
            // 
            appearance53.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPosition.Appearance = appearance53;
            this.uTextPosition.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextPosition.Location = new System.Drawing.Point(684, 60);
            this.uTextPosition.Name = "uTextPosition";
            this.uTextPosition.ReadOnly = true;
            this.uTextPosition.Size = new System.Drawing.Size(8, 21);
            this.uTextPosition.TabIndex = 5;
            this.uTextPosition.Visible = false;
            // 
            // uLabelSubUser
            // 
            appearance19.TextHAlignAsString = "Left";
            appearance19.TextVAlignAsString = "Middle";
            this.uLabelSubUser.Appearance = appearance19;
            this.uLabelSubUser.Location = new System.Drawing.Point(260, 52);
            this.uLabelSubUser.Name = "uLabelSubUser";
            this.uLabelSubUser.Size = new System.Drawing.Size(100, 20);
            this.uLabelSubUser.TabIndex = 0;
            // 
            // uLabelPosition
            // 
            appearance54.TextHAlignAsString = "Left";
            appearance54.TextVAlignAsString = "Middle";
            this.uLabelPosition.Appearance = appearance54;
            this.uLabelPosition.Location = new System.Drawing.Point(668, 56);
            this.uLabelPosition.Name = "uLabelPosition";
            this.uLabelPosition.Size = new System.Drawing.Size(8, 20);
            this.uLabelPosition.TabIndex = 0;
            this.uLabelPosition.Visible = false;
            // 
            // uTextDeptName
            // 
            appearance23.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeptName.Appearance = appearance23;
            this.uTextDeptName.BackColor = System.Drawing.Color.Gainsboro;
            this.uTextDeptName.Location = new System.Drawing.Point(116, 52);
            this.uTextDeptName.Name = "uTextDeptName";
            this.uTextDeptName.ReadOnly = true;
            this.uTextDeptName.Size = new System.Drawing.Size(135, 21);
            this.uTextDeptName.TabIndex = 4;
            // 
            // uLabelDeptName
            // 
            appearance20.TextHAlignAsString = "Left";
            appearance20.TextVAlignAsString = "Middle";
            this.uLabelDeptName.Appearance = appearance20;
            this.uLabelDeptName.Location = new System.Drawing.Point(12, 52);
            this.uLabelDeptName.Name = "uLabelDeptName";
            this.uLabelDeptName.Size = new System.Drawing.Size(100, 20);
            this.uLabelDeptName.TabIndex = 0;
            // 
            // uLabelUserID
            // 
            appearance21.TextHAlignAsString = "Left";
            appearance21.TextVAlignAsString = "Middle";
            this.uLabelUserID.Appearance = appearance21;
            this.uLabelUserID.Location = new System.Drawing.Point(12, 28);
            this.uLabelUserID.Name = "uLabelUserID";
            this.uLabelUserID.Size = new System.Drawing.Size(100, 20);
            this.uLabelUserID.TabIndex = 0;
            // 
            // uGroupBoxEquip
            // 
            this.uGroupBoxEquip.Controls.Add(this.uTextS);
            this.uGroupBoxEquip.Controls.Add(this.uTextM);
            this.uGroupBoxEquip.Controls.Add(this.uComboProcessGroup);
            this.uGroupBoxEquip.Controls.Add(this.uComboEquipGroup);
            this.uGroupBoxEquip.Controls.Add(this.uComboEquipLoc);
            this.uGroupBoxEquip.Controls.Add(this.uComboEquipLargeType);
            this.uGroupBoxEquip.Controls.Add(this.uComboStation);
            this.uGroupBoxEquip.Controls.Add(this.uLabelProcessGroup);
            this.uGroupBoxEquip.Controls.Add(this.uLabelS);
            this.uGroupBoxEquip.Controls.Add(this.uLabelM);
            this.uGroupBoxEquip.Controls.Add(this.uLabelEquipGorup);
            this.uGroupBoxEquip.Controls.Add(this.uLabelLocation);
            this.uGroupBoxEquip.Controls.Add(this.uLabelEquipLargeType);
            this.uGroupBoxEquip.Controls.Add(this.uLabelStation);
            this.uGroupBoxEquip.Controls.Add(this.uButtonOk);
            this.uGroupBoxEquip.Controls.Add(this.uButtonSearch);
            this.uGroupBoxEquip.Controls.Add(this.uGridEquipList);
            this.uGroupBoxEquip.Location = new System.Drawing.Point(332, 316);
            this.uGroupBoxEquip.Name = "uGroupBoxEquip";
            this.uGroupBoxEquip.Size = new System.Drawing.Size(736, 520);
            this.uGroupBoxEquip.TabIndex = 4;
            // 
            // uTextS
            // 
            appearance56.BackColor = System.Drawing.Color.Plum;
            this.uTextS.Appearance = appearance56;
            this.uTextS.BackColor = System.Drawing.Color.Plum;
            this.uTextS.Location = new System.Drawing.Point(116, 104);
            this.uTextS.Multiline = true;
            this.uTextS.Name = "uTextS";
            this.uTextS.ReadOnly = true;
            this.uTextS.Size = new System.Drawing.Size(24, 16);
            this.uTextS.TabIndex = 7;
            // 
            // uTextM
            // 
            appearance58.BackColor = System.Drawing.Color.Salmon;
            this.uTextM.Appearance = appearance58;
            this.uTextM.BackColor = System.Drawing.Color.Salmon;
            this.uTextM.Location = new System.Drawing.Point(540, 32);
            this.uTextM.Multiline = true;
            this.uTextM.Name = "uTextM";
            this.uTextM.ReadOnly = true;
            this.uTextM.Size = new System.Drawing.Size(24, 16);
            this.uTextM.TabIndex = 7;
            this.uTextM.Visible = false;
            // 
            // uComboProcessGroup
            // 
            this.uComboProcessGroup.Location = new System.Drawing.Point(116, 56);
            this.uComboProcessGroup.MaxLength = 50;
            this.uComboProcessGroup.Name = "uComboProcessGroup";
            this.uComboProcessGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboProcessGroup.TabIndex = 6;
            this.uComboProcessGroup.ValueChanged += new System.EventHandler(this.uComboProcessGroup_ValueChanged);
            // 
            // uComboEquipGroup
            // 
            this.uComboEquipGroup.Location = new System.Drawing.Point(116, 80);
            this.uComboEquipGroup.MaxLength = 50;
            this.uComboEquipGroup.Name = "uComboEquipGroup";
            this.uComboEquipGroup.Size = new System.Drawing.Size(120, 21);
            this.uComboEquipGroup.TabIndex = 6;
            // 
            // uComboEquipLoc
            // 
            this.uComboEquipLoc.Location = new System.Drawing.Point(348, 32);
            this.uComboEquipLoc.MaxLength = 50;
            this.uComboEquipLoc.Name = "uComboEquipLoc";
            this.uComboEquipLoc.Size = new System.Drawing.Size(120, 21);
            this.uComboEquipLoc.TabIndex = 5;
            this.uComboEquipLoc.ValueChanged += new System.EventHandler(this.uComboLocation_ValueChanged);
            // 
            // uComboEquipLargeType
            // 
            this.uComboEquipLargeType.Location = new System.Drawing.Point(348, 56);
            this.uComboEquipLargeType.MaxLength = 50;
            this.uComboEquipLargeType.Name = "uComboEquipLargeType";
            this.uComboEquipLargeType.Size = new System.Drawing.Size(120, 21);
            this.uComboEquipLargeType.TabIndex = 5;
            this.uComboEquipLargeType.ValueChanged += new System.EventHandler(this.uComboEquipLargeType_ValueChanged);
            // 
            // uComboStation
            // 
            this.uComboStation.Location = new System.Drawing.Point(116, 32);
            this.uComboStation.MaxLength = 50;
            this.uComboStation.Name = "uComboStation";
            this.uComboStation.Size = new System.Drawing.Size(120, 21);
            this.uComboStation.TabIndex = 5;
            this.uComboStation.ValueChanged += new System.EventHandler(this.uComboStation_ValueChanged);
            // 
            // uLabelProcessGroup
            // 
            this.uLabelProcessGroup.Location = new System.Drawing.Point(12, 56);
            this.uLabelProcessGroup.Name = "uLabelProcessGroup";
            this.uLabelProcessGroup.Size = new System.Drawing.Size(100, 20);
            this.uLabelProcessGroup.TabIndex = 4;
            // 
            // uLabelS
            // 
            this.uLabelS.Location = new System.Drawing.Point(508, 28);
            this.uLabelS.Name = "uLabelS";
            this.uLabelS.Size = new System.Drawing.Size(24, 19);
            this.uLabelS.TabIndex = 4;
            // 
            // uLabelM
            // 
            this.uLabelM.Location = new System.Drawing.Point(12, 104);
            this.uLabelM.Name = "uLabelM";
            this.uLabelM.Size = new System.Drawing.Size(100, 20);
            this.uLabelM.TabIndex = 4;
            // 
            // uLabelEquipGorup
            // 
            this.uLabelEquipGorup.Location = new System.Drawing.Point(12, 80);
            this.uLabelEquipGorup.Name = "uLabelEquipGorup";
            this.uLabelEquipGorup.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipGorup.TabIndex = 4;
            // 
            // uLabelLocation
            // 
            this.uLabelLocation.Location = new System.Drawing.Point(244, 32);
            this.uLabelLocation.Name = "uLabelLocation";
            this.uLabelLocation.Size = new System.Drawing.Size(100, 20);
            this.uLabelLocation.TabIndex = 4;
            // 
            // uLabelEquipLargeType
            // 
            this.uLabelEquipLargeType.Location = new System.Drawing.Point(244, 56);
            this.uLabelEquipLargeType.Name = "uLabelEquipLargeType";
            this.uLabelEquipLargeType.Size = new System.Drawing.Size(100, 20);
            this.uLabelEquipLargeType.TabIndex = 4;
            // 
            // uLabelStation
            // 
            this.uLabelStation.Location = new System.Drawing.Point(12, 32);
            this.uLabelStation.Name = "uLabelStation";
            this.uLabelStation.Size = new System.Drawing.Size(100, 20);
            this.uLabelStation.TabIndex = 4;
            // 
            // uButtonOk
            // 
            this.uButtonOk.Location = new System.Drawing.Point(332, 96);
            this.uButtonOk.Name = "uButtonOk";
            this.uButtonOk.Size = new System.Drawing.Size(88, 28);
            this.uButtonOk.TabIndex = 3;
            this.uButtonOk.Click += new System.EventHandler(this.uButtonOk_Click);
            // 
            // uButtonSearch
            // 
            this.uButtonSearch.Location = new System.Drawing.Point(240, 96);
            this.uButtonSearch.Name = "uButtonSearch";
            this.uButtonSearch.Size = new System.Drawing.Size(88, 28);
            this.uButtonSearch.TabIndex = 3;
            this.uButtonSearch.Click += new System.EventHandler(this.uButtonSearch_Click);
            // 
            // uGridEquipList
            // 
            this.uGridEquipList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridEquipList.DisplayLayout.Appearance = appearance18;
            this.uGridEquipList.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridEquipList.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.BandLabelAppearance = appearance41;
            this.uGridEquipList.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance42.BackColor2 = System.Drawing.SystemColors.Control;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridEquipList.DisplayLayout.GroupByBox.PromptAppearance = appearance42;
            this.uGridEquipList.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridEquipList.DisplayLayout.MaxRowScrollRegions = 1;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridEquipList.DisplayLayout.Override.ActiveCellAppearance = appearance43;
            appearance44.BackColor = System.Drawing.SystemColors.Highlight;
            appearance44.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridEquipList.DisplayLayout.Override.ActiveRowAppearance = appearance44;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridEquipList.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.CardAreaAppearance = appearance45;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            appearance47.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridEquipList.DisplayLayout.Override.CellAppearance = appearance47;
            this.uGridEquipList.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridEquipList.DisplayLayout.Override.CellPadding = 0;
            appearance48.BackColor = System.Drawing.SystemColors.Control;
            appearance48.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance48.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance48.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance48.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridEquipList.DisplayLayout.Override.GroupByRowAppearance = appearance48;
            appearance49.TextHAlignAsString = "Left";
            this.uGridEquipList.DisplayLayout.Override.HeaderAppearance = appearance49;
            this.uGridEquipList.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridEquipList.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.BorderColor = System.Drawing.Color.Silver;
            this.uGridEquipList.DisplayLayout.Override.RowAppearance = appearance50;
            this.uGridEquipList.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance51.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridEquipList.DisplayLayout.Override.TemplateAddRowAppearance = appearance51;
            this.uGridEquipList.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridEquipList.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridEquipList.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridEquipList.Location = new System.Drawing.Point(12, 104);
            this.uGridEquipList.Name = "uGridEquipList";
            this.uGridEquipList.Size = new System.Drawing.Size(708, 396);
            this.uGridEquipList.TabIndex = 0;
            this.uGridEquipList.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.uGridEquipList_AfterRowInsert);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 1;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxUser
            // 
            this.uGroupBoxUser.Controls.Add(this.uGridUserList);
            this.uGroupBoxUser.Location = new System.Drawing.Point(0, 80);
            this.uGroupBoxUser.Name = "uGroupBoxUser";
            this.uGroupBoxUser.Size = new System.Drawing.Size(328, 752);
            this.uGroupBoxUser.TabIndex = 5;
            // 
            // frmMASZ0026
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxUser);
            this.Controls.Add(this.uGroupBoxEquip);
            this.Controls.Add(this.uGroupBoxLineInfo);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0026";
            this.Load += new System.EventHandler(this.frmMASZ0026_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0026_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMASZ0026_FormClosing);
            this.Resize += new System.EventHandler(this.frmMASZ0026_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextHpNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTelNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridUserList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxLineInfo)).EndInit();
            this.uGroupBoxLineInfo.ResumeLayout(false);
            this.uGroupBoxLineInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSubUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSubUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDeptName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxEquip)).EndInit();
            this.uGroupBoxEquip.ResumeLayout(false);
            this.uGroupBoxEquip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboProcessGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboEquipLargeType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboStation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridEquipList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxUser)).EndInit();
            this.uGroupBoxUser.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridUserList;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquip;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxLineInfo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTelNum;
        private Infragistics.Win.Misc.UltraLabel uLabelTelNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPosition;
        private Infragistics.Win.Misc.UltraLabel uLabelPosition;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDeptName;
        private Infragistics.Win.Misc.UltraLabel uLabelDeptName;
        private Infragistics.Win.Misc.UltraLabel uLabelUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextHpNum;
        private Infragistics.Win.Misc.UltraLabel uLabelHpNum;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDept;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDept;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxEquip;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLargeType;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboStation;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipLargeType;
        private Infragistics.Win.Misc.UltraLabel uLabelStation;
        private Infragistics.Win.Misc.UltraButton uButtonSearch;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridEquipList;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipLoc;
        private Infragistics.Win.Misc.UltraLabel uLabelLocation;
        private Infragistics.Win.Misc.UltraButton uButtonOk;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSubUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSubUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelSubUser;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchUserName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchUserID;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchUserName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchUserID;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboProcessGroup;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboEquipGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelProcessGroup;
        private Infragistics.Win.Misc.UltraLabel uLabelEquipGorup;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextS;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextM;
        private Infragistics.Win.Misc.UltraLabel uLabelS;
        private Infragistics.Win.Misc.UltraLabel uLabelM;
        private Infragistics.Win.Misc.UltraLabel uLabelYN;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
    }
}