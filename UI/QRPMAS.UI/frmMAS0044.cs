﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0044.cs                                         */
/* 프로그램명   : 치공구창고정보                                        */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-07                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0044 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        

        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0044()
        {
            InitializeComponent();
        }

        private void frmMAS0044_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0044_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("치공구창고정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitLabel();
            InitButton();
            InitComboBox();
            InitGrid();

            // Contents GroupBox 접힌상태로

            this.uGroupBoxContentsArea.Expanded = false;
            this.uTextDurableInventoryCode.Visible = false;
            this.uTextPlantCode.Visible = false;
        }

        #region 초기화 함수
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화

        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDelete, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center
                    , m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue("A"), "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                // Inventory Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGrid1, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid1, 0, "Check","선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", m_resSys.GetString("SYS_PLANTCODE"));

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryCode", "창고코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryName", "창고명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryNameCh", "창고명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "DurableInventoryNameEn", "창고명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid1, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                // Section Grid
                // 일반설정 
                wGrid.mfInitGeneralGrid(this.uGrid2, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGrid2, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");


                wGrid.mfSetGridColumn(this.uGrid2, 0, "SectionCode", "Section코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "SectionName", "Section명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "SectionNameCh", "Section명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "SectionNameEn", "Section명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGrid2, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                // 공장컬럼 DropDonw 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "PlantCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", ComboDefaultValue("C"), dtPlant);


                // 사용여부 DropDown 설정
                ////DataTable dtUseFlag = new DataTable();

                ////dtUseFlag.Columns.Add("Key", typeof(String));
                ////dtUseFlag.Columns.Add("Value", typeof(String));

                ////DataRow row1 = dtUseFlag.NewRow();
                ////row1["Key"] = "T";
                ////row1["Value"] = "사용함";
                ////dtUseFlag.Rows.Add(row1);

                ////DataRow row2 = dtUseFlag.NewRow();
                ////row2["Key"] = "F";
                ////row2["Value"] = "사용안함";
                ////dtUseFlag.Rows.Add(row2);

                // 사용여부 콤보박스 설정
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtUseFlag = clsCom.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                // Inventory Grid
                wGrid.mfSetGridColumnValueList(this.uGrid1, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);
                // Section Grid
                wGrid.mfSetGridColumnValueList(this.uGrid2, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGrid1, 0);
                wGrid.mfAddRowGrid(this.uGrid2, 0);

                // FontSize
                this.uGrid1.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid1.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGrid2.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGrid2.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">C : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                string strLang = m_resSys.GetString("SYS_LANG").ToUpper();
                if (strGubun.Equals("C"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }
        #endregion

        #region ToolBar Method
        public void mfSearch()
        {
            try
            {
                if (this.uGroupBoxContentsArea.Expanded)
                    this.uGroupBoxContentsArea.Expanded = false;

                string strPlantCode = uComboSearchPlant.Value.ToString();
                         //string strDurableMatTypeCode = uComboMold.Value.ToString();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //--------------------------------------------------------------------처리로직----------------------------------------------------------//
                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChnnel.mfCredentials(clsDurableInventory);


                DataTable dtDurableInventory = new DataTable();
                //조회함수호출
                dtDurableInventory = clsDurableInventory.mfReadDurableInventory(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인딩
                this.uGrid1.DataSource = dtDurableInventory;
                this.uGrid1.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                //--------------------------------------------------------------------------------------------------------------------------------------//
                if (dtDurableInventory.Rows.Count == 0)
                {/* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                                        Infragistics.Win.HAlign.Right);
                }
                else
                {
                    for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                    {
                        this.uGrid1.Rows[i].Cells["PlantCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }
                    WinGrid grd = new WinGrid();
                    grd.mfSetAutoResizeColWidth(this.uGrid1, 0);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfSave()
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                    Save_MASDurableInventory();
                else
                    Save_MASDurableSection();


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void Save_MASDurableInventory()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataRow row;
                DialogResult DResult = new DialogResult();
                int intRowCheck = 0;

                if (this.uGrid1.Rows.Count > 0)
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                string strLang = m_resSys.GetString("SYS_LANG");
                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {

                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;
                        if (this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001228",strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000560",strLang)
                                            , Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["PlantCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }

                        if (this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Value.ToString() == "")
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , msg.GetMessge_Text("M001264",strLang)
                                            , msg.GetMessge_Text("M001228",strLang)
                                            , this.uGrid1.Rows[i].RowSelectorNumber + msg.GetMessge_Text("M000529",strLang)
                                            , Infragistics.Win.HAlign.Center);

                            // Focus Cell
                            this.uGrid1.ActiveCell = this.uGrid1.Rows[i].Cells["DurableInventoryCode"];
                            this.uGrid1.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                            return;
                        }
                    }
                }

                if (intRowCheck == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001228", "M001052", Infragistics.Win.HAlign.Center);
                    return;
                }
                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                           , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000876"
                                           , Infragistics.Win.HAlign.Right);


                if (dir == DialogResult.No)
                    return;


                // 채널 연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 호출 : 금형/치공구 저장을 위한 데이터테이블 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInventory = clsDurableInventory.mfSetDatainfo();

                if(this.uGrid1.Rows.Count > 0)
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (this.uGrid1.Rows[i].Hidden == false)
                    {

                            row = dtDurableInventory.NewRow();
                            row["PlantCode"] = this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DurableInventoryCode"] = this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                            row["DurableInventoryName"] = this.uGrid1.Rows[i].Cells["DurableInventoryName"].Value.ToString();
                            row["DurableInventoryNameCh"] = this.uGrid1.Rows[i].Cells["DurableInventoryNameCh"].Value.ToString();
                            row["DurableInventoryNameEn"] = this.uGrid1.Rows[i].Cells["DurableInventoryNameEn"].Value.ToString();
                            row["UseFlag"] = this.uGrid1.Rows[i].Cells["UseFlag"].Value.ToString();
                            dtDurableInventory.Rows.Add(row);
                        
                    }
                }

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 처리 로직 //
                // 저장함수 호출
                string rtMSG = clsDurableInventory.mfSaveMASDurableInventory(dtDurableInventory, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);


                    mfCreate();
                    mfSearch();
                }
                else
                {
                    string strMes = "";
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMes = msg.GetMessge_Text("M000953", strLang);
                    else
                        strMes = ErrRtn.ErrMessage;

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        msg.GetMessge_Text("M001135",strLang)
                                        , msg.GetMessge_Text("M001037",strLang)
                                        , strMes,
                                        Infragistics.Win.HAlign.Right);
                }

                


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        private void Save_MASDurableSection()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataRow row;
                DialogResult DResult = new DialogResult();

                int intRowCheck = 0;

                if(this.uGrid2.Rows.Count > 0)
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (this.uGrid2.Rows[i].Hidden == false)
                    {
                        intRowCheck = intRowCheck + 1;
                    }
                }

                if (intRowCheck == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M000953", "M001018", Infragistics.Win.HAlign.Center);
                    return;
                }

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                           , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M001053", "M000876"
                                           , Infragistics.Win.HAlign.Right);


                if (dir == DialogResult.No)
                    return;


                // 채널 연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 호출 : 
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableSection), "DurableSection");
                QRPMAS.BL.MASDMM.DurableSection clsDurableSection = new QRPMAS.BL.MASDMM.DurableSection();
                brwChannel.mfCredentials(clsDurableSection);

                DataTable dtDurableSection = clsDurableSection.mfSetDatainfo();

                if(this.uGrid2.Rows.Count > 0)
                    this.uGrid2.ActiveCell = this.uGrid2.Rows[0].Cells[0];

                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (this.uGrid2.Rows[i].Hidden == false)
                    {

                        row = dtDurableSection.NewRow();
                        row["PlantCode"] = this.uTextPlantCode.Text;
                        row["DurableInventoryCode"] = this.uTextDurableInventoryCode.Text;
                        row["SectionNum"] = i + 1;
                        row["SectionName"] = this.uGrid2.Rows[i].Cells["SectionName"].Value.ToString();
                        row["SectionNameCh"] = this.uGrid2.Rows[i].Cells["SectionNameCh"].Value.ToString();
                        row["SectionNameEn"] = this.uGrid2.Rows[i].Cells["SectionNameEn"].Value.ToString();
                        row["ColumnNum"] = "";
                        row["RowNum"] = "";
                        row["UseFlag"] = this.uGrid2.Rows[i].Cells["UseFlag"].Value.ToString();
                        dtDurableSection.Rows.Add(row);

                    }
                }
                string strLang = m_resSys.GetString("SYS_LANG");
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                // 처리 로직 //
                // 저장함수 호출
                string rtMSG = clsDurableSection.mfDelete_Save_MASDurableSection(dtDurableSection, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M001037", "M000930",
                                        Infragistics.Win.HAlign.Right);


                    mfCreate();
                    mfSearch();
                }
                else
                {
                    string strMes = "";
                    
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMes = msg.GetMessge_Text("M000953", strLang);
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMes,
                                        Infragistics.Win.HAlign.Right);
                }

                


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfDelete()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                DataRow row;
                DialogResult DResult = new DialogResult();

                int intRowCheck = 0;

                if(this.uGrid1.Rows.Count > 0)
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                // 필수 입력사항 확인
                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    
                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            intRowCheck = intRowCheck + 1;
                        }
                    }
                }
                if (intRowCheck == 0)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "M001264", "M001228", "M000649", Infragistics.Win.HAlign.Center);
                    return;
                }
            
               

                DialogResult dir = new DialogResult();
                dir = msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                           , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", " M000650", "M000922"
                                           , Infragistics.Win.HAlign.Right);


                if (dir == DialogResult.No)
                    return;


                // 채널 연결
                QRPBrowser brwChannel = new QRPBrowser();

                //BL 호출 : 금형/치공구 저장을 위한 데이터테이블 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                brwChannel.mfCredentials(clsDurableInventory);

                DataTable dtDurableInventory = clsDurableInventory.mfSetDatainfo();

                if(this.uGrid1.Rows.Count > 0)
                    this.uGrid1.ActiveCell = this.uGrid1.Rows[0].Cells[0];

                for (int i = 0; i < this.uGrid1.Rows.Count; i++)
                {
                    if (this.uGrid1.Rows[i].Hidden == false)
                    {
                        if (Convert.ToBoolean(this.uGrid1.Rows[i].Cells["Check"].Value) == true)
                        {
                            row = dtDurableInventory.NewRow();
                            row["PlantCode"] = this.uGrid1.Rows[i].Cells["PlantCode"].Value.ToString();
                            row["DurableInventoryCode"] = this.uGrid1.Rows[i].Cells["DurableInventoryCode"].Value.ToString();
                            row["DurableInventoryName"] = this.uGrid1.Rows[i].Cells["DurableInventoryName"].Value.ToString();
                            row["DurableInventoryNameCh"] = this.uGrid1.Rows[i].Cells["DurableInventoryNameCh"].Value.ToString();
                            row["DurableInventoryNameEn"] = this.uGrid1.Rows[i].Cells["DurableInventoryNameEn"].Value.ToString();
                            row["UseFlag"] = this.uGrid1.Rows[i].Cells["UseFlag"].Value.ToString();
                            dtDurableInventory.Rows.Add(row);
                        }
                    }
                }
                string strLang = m_resSys.GetString("SYS_LANG");
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", strLang));
                this.MdiParent.Cursor = Cursors.WaitCursor;



                // 처리 로직 //
                // 삭제함수 호출
                string rtMSG = clsDurableInventory.mfDeleteMASDurableInventory(dtDurableInventory, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                // Decoding //
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                // 처리로직 끝//

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // 처리결과에 따른 메세지 박스
                System.Windows.Forms.DialogResult result;
                if (ErrRtn.ErrNum == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001135", "M000638", "M000960",
                                        Infragistics.Win.HAlign.Right);


                    mfCreate();
                    mfSearch();
                }
                else
                {
                    string strMes = "";
                    
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMes = msg.GetMessge_Text("M000689",strLang);
                    else
                        strMes = ErrRtn.ErrMessage;

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        msg.GetMessge_Text("M001135",strLang),
                                        msg.GetMessge_Text("M000638",strLang), strMes,
                                        Infragistics.Win.HAlign.Right);
                }

                


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        public void mfCreate()
        {
            try
            {
                this.uTextPlantCode.Text = "";
                this.uTextDurableInventoryCode.Text = "";
                while (this.uGrid2.Rows.Count > 0)
                {
                    this.uGrid2.Rows[0].Delete(false);
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfPrint()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }

        public void mfExcel()
        {
            try
            {

            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion
       
        #region Events....
        // Inventory Grid Cell Update
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 수정시 이미지 변경

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 자동행삭제

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid1, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Section Grid Cell Update
        private void uGrid2_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 수정시 이미지 변경

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                // 자동행삭제

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGrid2, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Contents GroupBox Expande State Changing
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 145);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 60;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGrid1.Height = 720;

                    for (int i = 0; i < uGrid1.Rows.Count; i++)
                    {
                        uGrid1.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // Delete Button Click Event
        private void uButtonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGrid2.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGrid2.Rows[i].Cells["Check"].Value) == true)
                    {
                        this.uGrid2.Rows[i].Hidden = true;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                ////string strPlantCode = uComboSearchPlant.Value.ToString();
                //////System ResourceInfo
                ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                ////WinMessageBox msg = new WinMessageBox();
                //////ProgressBar보이기
                ////QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                ////Thread threadPop = m_ProgressPopup.mfStartThread();
                //////m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //////커서Change
                ////this.MdiParent.Cursor = Cursors.WaitCursor;

                //////--------------------------------------------------------------------처리로직----------------------------------------------------------//
                //////BL호출
                ////QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                ////brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableInventory), "DurableInventory");
                ////QRPMAS.BL.MASDMM.DurableInventory clsDurableInventory = new QRPMAS.BL.MASDMM.DurableInventory();
                ////brwChnnel.mfCredentials(clsDurableInventory);


                ////DataTable dtDurableMat = new DataTable();
                //////조회함수호출
                ////dtDurableMat = clsDurableInventory.mfReadDurableInventory(strPlantCode, m_resSys.GetString("SYS_LANG"));

                //////그리드에 바인딩
                ////this.uGrid1.DataSource = dtDurableMat;
                ////this.uGrid1.DataBind();
                //////--------------------------------------------------------------------------------------------------------------------------------------//
                //mfSearch();

            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        private void uGrid1_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                mfCreate();

                string strPlantCode = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                string strDurableInventoryCode = e.Cell.Row.Cells["DurableInventoryCode"].Value.ToString();

                this.uTextPlantCode.Text = e.Cell.Row.Cells["PlantCode"].Value.ToString();
                this.uTextDurableInventoryCode.Text = e.Cell.Row.Cells["DurableInventoryCode"].Value.ToString();

                if (strPlantCode == "")                
                    return;
                

                if (strDurableInventoryCode =="")                
                    return;

                e.Cell.Row.Fixed = true;

                if(this.uGroupBoxContentsArea.Expanded.Equals(false))
                    uGroupBoxContentsArea.Expanded = true;

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();


                //--------------------------------------------------------------------처리로직----------------------------------------------------------//
                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChnnel = new QRPBrowser();
                brwChnnel.mfRegisterChannel(typeof(QRPMAS.BL.MASDMM.DurableSection), "DurableSection");
                QRPMAS.BL.MASDMM.DurableSection clsDurableSection = new QRPMAS.BL.MASDMM.DurableSection();
                brwChnnel.mfCredentials(clsDurableSection);


                DataTable dtDurableSection = new DataTable();
                //조회함수호출
                dtDurableSection = clsDurableSection.mfReadDurableSection(strPlantCode, strDurableInventoryCode, m_resSys.GetString("SYS_LANG"));

                //그리드에 바인딩
                this.uGrid2.DataSource = dtDurableSection;
                this.uGrid2.DataBind();

                //--------------------------------------------------------------------------------------------------------------------------------------//



            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

    }
}
