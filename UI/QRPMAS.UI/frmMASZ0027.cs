﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가참조
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMASZ0027 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0027()
        {
            InitializeComponent();
        }

        private void frmMASZ0027_Activated(object sender, EventArgs e)
        {
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0027_Load(object sender, EventArgs e)
        {
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("HoldCode 정보", m_resSys.GetString("SYS_FONTNAME"), 12);

            SetToolAuth();
            InitLabel();
            InitComboBox();
            InitGrid();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 초기화 Method
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화

        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 라벨 초기화

                WinLabel winLabel = new WinLabel();
                winLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                winLabel.mfSetLabel(this.uLabelHoldType, "사유코드유형", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //콤보박스 초기화

                WinComboEditor combo = new WinComboEditor();

                // BL 호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);

                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 50, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", ComboDefaultValue("S", m_resSys.GetString("SYS_LANG"))
                    , "PlantCode", "PlantName", dtPlant);

                //HoldTypeCode BL 호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                QRPMAS.BL.MASPRC.ReasonCode clsReason = new QRPMAS.BL.MASPRC.ReasonCode();
                brwChannel.mfCredentials(clsReason);

                String strPlantCode = m_resSys.GetString("SYS_PLANTCODE");

                DataTable dtHoldCode = clsReason.mfReadMASReasonTypeCode_Combo(strPlantCode);

                combo.mfSetComboEditor(this.uComboHoldType, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Left, "", "", ComboDefaultValue("A", m_resSys.GetString("SYS_LANG")), "REASONCODETYPE", "REASONCODENAME"
                    , dtHoldCode);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();
                // 그리드 일반 설정
                grd.mfInitGeneralGrid(uGridList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                grd.mfSetGridColumn(uGridList, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 50, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridList, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridList, 0, "ReasonCodeType", "사유코드유형", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridList, 0, "ReasonCode", "사유코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridList, 0, "Description", "사유코드명", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridList, 0, "MaterialAbnormalProcFlag", "원자재이상발생적용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(uGridList, 0, "MaterialSpecialFlag", "원자재특채적용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(uGridList, 0, "INSProcFlag", "공정검사적용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(uGridList, 0, "QCNFlag", "QCN적용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(uGridList, 0, "CCSFlag", "CCS적용", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(uGridList, 0, "Use_System", "사용시스템", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                // Set FontSize
                this.uGridList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 헤더 체크박스 없애기

                this.uGridList.DisplayLayout.Bands[0].Columns["MaterialAbnormalProcFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGridList.DisplayLayout.Bands[0].Columns["MaterialSpecialFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGridList.DisplayLayout.Bands[0].Columns["INSProcFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGridList.DisplayLayout.Bands[0].Columns["QCNFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;
                this.uGridList.DisplayLayout.Bands[0].Columns["CCSFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                // 사용여부 콤보박스 설정
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCom = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCom);

                DataTable dtUseFlag = clsCom.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 콤보 기본값
        /// </summary>
        /// <param name="strGubun">S : 선택, A : 전체</param>
        /// <param name="strLang">사용언어</param>
        /// <returns></returns>
        private string ComboDefaultValue(string strGubun, string strLang)
        {
            try
            {
                string strRtnValue = "";
                strGubun = strGubun.ToUpper();
                strLang = strLang.ToUpper();
                if (strGubun.Equals("S"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "선택";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "选择";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "Choice";
                }
                if (strGubun.Equals("A"))
                {
                    if (strLang.Equals("KOR"))
                        strRtnValue = "전체";
                    else if (strLang.Equals("CHN"))
                        strRtnValue = "全部";
                    else if (strLang.Equals("ENG"))
                        strRtnValue = "All";
                }

                return strRtnValue;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
                return string.Empty;
            }
            finally
            { }
        }

        #endregion

        #region ToolBar
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // 조회를 위한 Plant/Routing 변수

                string strSearchPlantCode = this.uComboSearchPlant.Value.ToString();
                string strHoldCode = this.uComboHoldType.Value.ToString();

                // SystemInfo 리소스

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                QRPMAS.BL.MASPRC.ReasonCode clsReasonCode = new QRPMAS.BL.MASPRC.ReasonCode();
                brwChannel.mfCredentials(clsReasonCode);
                DataTable dt = new DataTable();

                dt = clsReasonCode.mfReadMASReasonCode(strSearchPlantCode, strHoldCode, m_resSys.GetString("SYS_LANG"));
                this.uGridList.DataSource = dt;
                this.uGridList.DataBind();

                WinGrid grd = new WinGrid();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    grd.mfSetAutoResizeColWidth(this.uGridList, 0);
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장

        /// </summary>
        public void mfSave()
        {
            try
            {
                if (this.uGridList.Rows.Count > 0)
                {
                    // SystemInfo 리소스

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    WinMessageBox msg = new WinMessageBox();

                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500
                                            , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista, "M001264", "M001053", "M000936"
                                            , Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // Active Cell 이동
                        this.uGridList.ActiveCell = this.uGridList.Rows[0].Cells[0];

                        QRPBrowser brwChannel = new QRPBrowser();
                        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.ReasonCode), "ReasonCode");
                        QRPMAS.BL.MASPRC.ReasonCode clsReasonCode = new QRPMAS.BL.MASPRC.ReasonCode();
                        brwChannel.mfCredentials(clsReasonCode);

                        DataTable dtReasonCode = clsReasonCode.mfSetDataInfo();
                        DataRow drRow;

                        for (int i = 0; i < this.uGridList.Rows.Count; i++)
                        {
                            if (this.uGridList.Rows[i].RowSelectorAppearance.Image != null)
                            {
                                drRow = dtReasonCode.NewRow();
                                drRow["PlantCode"] = this.uGridList.Rows[i].Cells["PlantCode"].Value.ToString();
                                drRow["ReasonCodeType"] = this.uGridList.Rows[i].Cells["ReasonCodeType"].Value.ToString();
                                drRow["ReasonCode"] = this.uGridList.Rows[i].Cells["ReasonCode"].Value.ToString();
                                if (this.uGridList.Rows[i].Cells["MaterialAbnormalProcFlag"].Value.Equals("True"))
                                    drRow["MaterialAbnormaProclFlag"] = "T";
                                else
                                    drRow["MaterialAbnormaProclFlag"] = "F";
                                if (this.uGridList.Rows[i].Cells["MaterialSpecialFlag"].Value.Equals("True"))
                                    drRow["MaterialSpecialFlag"] = "T";
                                else
                                    drRow["MaterialSpecialFlag"] = "F";
                                if (this.uGridList.Rows[i].Cells["INSProcFlag"].Value.Equals("True"))
                                    drRow["INSProcFlag"] = "T";
                                else
                                    drRow["INSProcFlag"] = "F";
                                if (this.uGridList.Rows[i].Cells["QCNFlag"].Value.Equals("True"))
                                    drRow["QCNFlag"] = "T";
                                else
                                    drRow["QCNFlag"] = "F";
                                if (this.uGridList.Rows[i].Cells["CCSFlag"].Value.Equals("True"))
                                    drRow["CCSFlag"] = "T";
                                else
                                    drRow["CCSFlag"] = "F";
                                dtReasonCode.Rows.Add(drRow);
                            }
                        }

                        if (dtReasonCode.Rows.Count > 0)
                        {
                            QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                            Thread threadPop = m_ProgressPopup.mfStartThread();
                            m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", m_resSys.GetString("SYS_LANG")));
                            this.MdiParent.Cursor = Cursors.WaitCursor;

                            string strErrRtn = clsReasonCode.mfSaveMASReasonCode(dtReasonCode, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                            this.MdiParent.Cursor = Cursors.Default;
                            m_ProgressPopup.mfCloseProgressPopup(this);

                            // 결과검사

                            TransErrRtn ErrRtn = new TransErrRtn();
                            ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                            DialogResult DResult = new DialogResult();

                            if (ErrRtn.ErrNum == 0)
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000930",
                                                    Infragistics.Win.HAlign.Right);
                                mfSearch();
                            }
                            else
                            {
                                DResult = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001037", "M000953",
                                                    Infragistics.Win.HAlign.Right);
                            }
                        }
                        else
                        {
                            DialogResult DResult = new DialogResult();
                            DResult = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001032", "M001047", Infragistics.Win.HAlign.Right);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        // 신규
        public void mfCreate()
        {
        }

        // 출력
        public void mfPrint()
        {
        }

        // 엑셀다운
        public void mfExcel()
        {
            //처리 로직//
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            grd.mfDownLoadGridToExcel(this.uGridList);
        }
        #endregion

        private void frmMASZ0027_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void uGridList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀 수정시 RowSelector 이미지 변화
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                if (e.Cell.Column.Key.Equals("MaterialAbnormalProcFlag"))
                {
                    for (int i = 0; i < this.uGridList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridList.Rows[i].Cells["MaterialAbnormalProcFlag"].Value).Equals(true))
                        {
                            if (!i.Equals(e.Cell.Row.Index))
                            {
                                this.uGridList.Rows[i].Cells["MaterialAbnormalProcFlag"].Value = false;
                            }
                        }
                    }
                }
                else if (e.Cell.Column.Key.Equals("MaterialSpecialFlag"))
                {
                    for (int i = 0; i < this.uGridList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridList.Rows[i].Cells["MaterialSpecialFlag"].Value).Equals(true))
                        {
                            if (!i.Equals(e.Cell.Row.Index))
                            {
                                this.uGridList.Rows[i].Cells["MaterialSpecialFlag"].Value = false;
                            }
                        }
                    }
                }
                else if (e.Cell.Column.Key.Equals("INSProcFlag"))
                {
                    for (int i = 0; i < this.uGridList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridList.Rows[i].Cells["INSProcFlag"].Value).Equals(true))
                        {
                            if (!i.Equals(e.Cell.Row.Index))
                            {
                                this.uGridList.Rows[i].Cells["INSProcFlag"].Value = false;
                            }
                        }
                    }
                }
                else if (e.Cell.Column.Key.Equals("QCNFlag"))
                {
                    for (int i = 0; i < this.uGridList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridList.Rows[i].Cells["QCNFlag"].Value).Equals(true))
                        {
                            if (!i.Equals(e.Cell.Row.Index))
                            {
                                this.uGridList.Rows[i].Cells["QCNFlag"].Value = false;
                            }
                        }
                    }
                }
                else if (e.Cell.Column.Key.Equals("CCSFlag"))
                {
                    for (int i = 0; i < this.uGridList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridList.Rows[i].Cells["CCSFlag"].Value).Equals(true))
                        {
                            if (!i.Equals(e.Cell.Row.Index))
                            {
                                this.uGridList.Rows[i].Cells["CCSFlag"].Value = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


    }
}
