﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 품질관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0033.cs                                         */
/* 프로그램명   : 단위정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-26                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using QRPMAS.BL.MASGEN;
using System.Threading;
using System.Resources;


namespace QRPMAS.UI
{
    public partial class frmMAS0033 : Form, IToolbar
    {
        // 리소스 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0033()
        {
            InitializeComponent();
        }

        private void frmMAS0033_Load(object sender, EventArgs e)
        {
            //System ResourceInfo
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            titleArea.mfSetLabelText("단위정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 그리드 초기화 함수호출
            SetToolAuth();
            InitGrid();
            InitLabel();
           // InitComboBox();
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        private void frmMAS0033_Activated(object sender, EventArgs e)
        {
            // ToolBar 활성화
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, true, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinLabel lbl = new WinLabel();

                //lbl.mfSetLabel(this.uLabelPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, false);
                lbl.mfSetLabel(this.uLabel1, "단위명", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        /// <summary>
        /// 그리드 초기화 함수
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResoucreSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //그리드 일반설정
                grd.mfInitGeneralGrid(this.uGridUnitList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드 컬럼추가
                ////grd.mfSetGridColumn(this.uGridUnitList, 0, "Plant", "공장", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, true, 10
                ////    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridUnitList, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridUnitList, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridUnitList, 0, "UnitName", "단위명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 15
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridUnitList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //grd.mfSetGridColumn(this.uGridUnitList, 0, "UnitCode", "단위코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 350, true, false, 10
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGridUnitList, 0, "UnitName", "단위명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 350, true, false, 50
                //    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                //grd.mfSetGridColumn(this.uGridUnitList, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 300, false, false, 1
                //    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                //    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                // 그리드에 사용여부 DropBox 추가
                //DataTable dtUseFlag = new DataTable();

                //dtUseFlag.Columns.Add("Key", typeof(String));
                //dtUseFlag.Columns.Add("Value", typeof(String));

                //DataRow row;

                //row = dtUseFlag.NewRow();
                //row["Key"] = "T";
                //row["Value"] = "사용함";
                //dtUseFlag.Rows.Add(row);

                //row = null;
                //row = dtUseFlag.NewRow();
                //row["Key"] = "F";
                //row["Value"] = "사용안함";
                //dtUseFlag.Rows.Add(row);

                // 그리드 컬럼에 콤보박스 추가
                //grd.mfSetGridColumnValueList(this.uGridUnitList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsComCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsComCode);
                DataTable dtUseFlag = clsComCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));

                grd.mfSetGridColumnValueList(this.uGridUnitList, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);

                //폰트설정
                uGridUnitList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridUnitList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                // 공백줄 추가
                grd.mfAddRowGrid(this.uGridUnitList, 0);

                // Check열 DataType 설정
                //this.uGridUnitList.DisplayLayout.Bands[0].Columns["Check"].DataType = typeof(Boolean);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                //// SystemInfo ResourceSet
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                //WinComboEditor wCombo = new WinComboEditor();

                //// SearchArea Plant ComboBox
                //// BL호출
                //QRPBrowser brwChannel = new QRPBrowser();
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                //QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                //brwChannel.mfCredentials(clsPlant);

                //// Call Method
                //DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                //wCombo.mfSetComboEditor(this.uComboPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                //    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                //    , "PlantCode", "PlantName", dtPlant);

            }
            catch
            {
            }
            finally
            {
            }
        }
#endregion

        #region 툴바
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                //QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                //Thread threadPop = m_ProgressPopup.mfStartThread();
                //m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //this.MdiParent.Cursor = Cursors.WaitCursor;

                //WinGrid grd = new WinGrid();
                //// 처리로직 //
                //// BL 호출
                //// QRPBrowser Interface 생성
                //QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                //// 함수호출을 통해 App.Server와 연결
                //brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                //// BL객체 Interface 생성
                //Unit unit = new Unit();
                //// 생성한 객체에 권한 설정
                //brwChannel.mfCredentials(unit);

                //// SystemInfo Resource 변수
                //ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //// 조회 함수호출
                //DataTable dt = unit.mfReadUnit("");
                //this.uGridUnitList.DataSource = dt;
                //this.uGridUnitList.DataBind();

                //// 바인딩후 체크박스 상태를 모두 Uncheck로 만든다
                //grd.mfSetAllUnCheckedGridColumn(this.uGridUnitList, 0, "Check");

                //// RowSelector Clear
                //grd.mfClearRowSeletorGrid(this.uGridUnitList);

                //// 처리로직 - 끝 //

                //this.MdiParent.Cursor = Cursors.Default;
                //m_ProgressPopup.mfCloseProgressPopup(this);

                //// 조회결과가 없을경우 메세지 출력
                //WinMessageBox msg = new WinMessageBox();
                //if (dt.Rows.Count == 0)
                //    msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                //        , "처리결과", "조회처리결과", "조회결과가 없습니다", Infragistics.Win.HAlign.Right);
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                this.MdiParent.Cursor = Cursors.WaitCursor;

                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                QRPMAS.BL.MASGEN.Unit unit = new QRPMAS.BL.MASGEN.Unit();
                brwChannel.mfCredentials(unit);


                String strUnitName = this.ultraTextEditor1.Text;


                DataTable dt = unit.mfReadUnit(strUnitName, m_resSys.GetString("SYS_LANG"));
                this.uGridUnitList.DataSource = dt;
                this.uGridUnitList.DataBind();

                WinGrid grd = new WinGrid();

                //// 바인딩후 체크박스 상태를 모두 Uncheck로 만든다
                //grd.mfSetAllUnCheckedGridColumn(this.uGridUnitList, 0, "Check");

                // RowSelector Clear
                //grd.mfClearRowSeletorGrid(this.uGridUnitList);

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                
                if (dt.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    for (int i = 0; i < this.uGridUnitList.Rows.Count; i++)
                    {
                        this.uGridUnitList.Rows[i].Cells["UnitCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        this.uGridUnitList.Rows[i].Cells["UnitCode"].Appearance.BackColor = Color.Gainsboro;
                    }
                    grd.mfSetAutoResizeColWidth(this.uGridUnitList, 0);
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                // SystemInfo Resource 변수
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // Message Resource
                //ResourceSet m_resMessage = new ResourceSet(SysRes.MessageLangRes);

                // 저장여부 확인 메세지창
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000936",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                    QRPMAS.BL.MASGEN.Unit save_unit = new QRPMAS.BL.MASGEN.Unit();
                    brwChannel.mfCredentials(save_unit);

                    // 저장함수 호출시 매개변수로 쓰일 DataTable
                    DataTable dtSave = save_unit.mfSetDataInfo();
                    if(this.uGridUnitList.Rows.Count > 0)
                        this.uGridUnitList.ActiveCell = this.uGridUnitList.Rows[0].Cells[0];

                    string strLang = m_resSys.GetString("SYS_LANG");

                    // for문 돌면서 체크된 데이터를 저장
                    for (int i = 0; i < this.uGridUnitList.Rows.Count; i++)
                    {
                        // RowSelector 에 이미지가 있는것만 저장한다.
                        if (this.uGridUnitList.Rows[i].RowSelectorAppearance.Image != null)
                        {  

                            // 필수 입력사항 확인
                            if (this.uGridUnitList.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang), (i + 1) + msg.GetMessge_Text("M000563",strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridUnitList.ActiveCell = this.uGridUnitList.Rows[i].Cells["UnitCode"];
                                this.uGridUnitList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else if (this.uGridUnitList.Rows[i].Cells["UnitName"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang), (i + 1) + msg.GetMessge_Text("M000562",strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridUnitList.ActiveCell = this.uGridUnitList.Rows[i].Cells["UnitName"];
                                this.uGridUnitList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else
                            {
                                for (int j = 0; j < this.uGridUnitList.Rows.Count; j++)
                                {
                                    if (i != j)
                                    {
                                        if (this.uGridUnitList.Rows[i].Cells["UnitCode"].Value.ToString() == this.uGridUnitList.Rows[j].Cells["UnitCode"].Value.ToString())
                                        {
                                            msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang), (i + 1) + msg.GetMessge_Text("M000570",strLang), Infragistics.Win.HAlign.Center);

                                            // Focus Cell
                                            this.uGridUnitList.ActiveCell = this.uGridUnitList.Rows[i].Cells["UnitCode"];
                                            this.uGridUnitList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                            return;
                                        }
                                    }
                                }

                                // 행추가
                                DataRow row = dtSave.NewRow();
                                row["UnitCode"] = this.uGridUnitList.Rows[i].Cells["UnitCode"].Value.ToString();
                                row["UnitName"] = this.uGridUnitList.Rows[i].Cells["UnitName"].Value.ToString();
                                row["UseFlag"] = this.uGridUnitList.Rows[i].Cells["UseFlag"].Value.ToString();
                                dtSave.Rows.Add(row);
                            }
                        }
                    }

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M001036", strLang));
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//

                    // BL 저장함수 호출
                    string rtMSG = save_unit.mfSaveMASUnit(dtSave, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                    // 함수호출 성공여부를 확인하기 위한 Class 변수
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                    //처리로직-끝//

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (ErrRtn.ErrNum == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M001037", "M000930",
                                            Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {

                        string strMes = "";
                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = msg.GetMessge_Text("M000953", strLang);
                        else
                            strMes = ErrRtn.ErrMessage;

                        msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_FONTNAME"),500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M001037",strLang), strMes,
                                            Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                // SystemInfo Resource 변수
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 삭제여부 확인 메시지창
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    // BL호출
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASGEN.Unit), "Unit");
                    QRPMAS.BL.MASGEN.Unit del_Unit = new QRPMAS.BL.MASGEN.Unit();
                    brwChannel.mfCredentials(del_Unit);

                    // 저장함수 호출시 매개변수로 쓰일 DataTable
                    DataTable dtUnit = del_Unit.mfSetDataInfo();

                    if(this.uGridUnitList.Rows.Count > 0)
                        this.uGridUnitList.ActiveCell = this.uGridUnitList.Rows[0].Cells[0];

                    string strLang = m_resSys.GetString("SYS_LANG");
                    // for문 돌면서 체크된것 삭제
                    for (int i = 0; i < this.uGridUnitList.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridUnitList.Rows[i].Cells["Check"].Value) == true)
                        {
                            // 필수입력사항 체크
                            // 필수 입력사항 확인
                            if (this.uGridUnitList.Rows[i].Cells["UnitCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"),500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                    , msg.GetMessge_Text("M001264",strLang), msg.GetMessge_Text("M001228",strLang), (i + 1) + msg.GetMessge_Text("M000563",strLang), Infragistics.Win.HAlign.Center);

                                // Focus Cell
                                this.uGridUnitList.ActiveCell = this.uGridUnitList.Rows[i].Cells["UnitCode"];
                                this.uGridUnitList.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;
                            }
                            else
                            {
                                DataRow row = dtUnit.NewRow();
                                row["UnitCode"] = this.uGridUnitList.Rows[i].Cells["UnitCode"].Value.ToString();
                                dtUnit.Rows.Add(row);
                            }
                        }
                    }

                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000637", strLang));
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    // 처리 로직 //
                    // 삭제함수 호출
                    String rtMSG = del_Unit.mfDeleteMASUnit(dtUnit);

                    // 결과처리를 위한 구문
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(rtMSG);
                    // 처리로직 끝 //

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    // 삭제성공여부
                    if (ErrRtn.ErrNum == 0)
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            "M001135", "M000638", "M000926",
                                            Infragistics.Win.HAlign.Right);
                        mfSearch();
                    }
                    else
                    {
                        string strMes = "";

                        if (ErrRtn.ErrMessage.Equals(string.Empty))
                            strMes = msg.GetMessge_Text("M000923", strLang);
                        else
                            strMes = ErrRtn.ErrMessage;

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                            msg.GetMessge_Text("M001135",strLang), msg.GetMessge_Text("M000638",strLang), strMes,
                                            Infragistics.Win.HAlign.Right);
                    }
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        public void mfCreate()
        {
        }

        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀
        /// </summary>
        public void mfExcel()
        {
            WinGrid grd = new WinGrid();
            
            //엑셀저장함수 호출
            if(this.uGridUnitList.Rows.Count > 0)
                grd.mfDownLoadGridToExcel(this.uGridUnitList);

        }
        #endregion

        private void frmMAS0033_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        private void uGridUnitList_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 셀변경시 RowSelector 이미지 변경
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // RowSelector Image 설정 및 행 자동삭제 이벤트
        private void uGridUnitList_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 행 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridUnitList, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (System.Exception ex)
            {
            	
            }
            finally
            {
            }
        }
    }
}