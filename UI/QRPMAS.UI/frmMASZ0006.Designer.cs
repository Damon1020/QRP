﻿namespace QRPMAS.UI
{
    partial class frmMASZ0006
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton1 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.EditorButton editorButton2 = new Infragistics.Win.UltraWinEditors.EditorButton();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0006));
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            this.ultraGrid4 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGrid5 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.ultraTextEditor10 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelAmendReason = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor9 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor6 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAcceptUserID = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor7 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel9 = new Infragistics.Win.Misc.UltraLabel();
            this.uText3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uText1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.uTabControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage3 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTextEditor8 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel10 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel8 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor5 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel7 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel6 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor4 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.ultraTextEditor3 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel5 = new Infragistics.Win.Misc.UltraLabel();
            this.ultraTextEditor2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel4 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel3 = new Infragistics.Win.Misc.UltraLabel();
            this.uText2 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabel2 = new Infragistics.Win.Misc.UltraLabel();
            this.uLabel1 = new Infragistics.Win.Misc.UltraLabel();
            this.uGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearch = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearch = new Infragistics.Win.Misc.UltraLabel();
            this.uComboPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelPlant = new Infragistics.Win.Misc.UltraLabel();
            this.titleArea = new QRPUserControl.TitleArea();
            this.ultraTabPageControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid4)).BeginInit();
            this.ultraTabPageControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl1)).BeginInit();
            this.uTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.uButtonDeleteRow);
            this.ultraTabPageControl5.Controls.Add(this.ultraGrid4);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(1040, 482);
            // 
            // uButtonDeleteRow
            // 
            appearance72.FontData.BoldAsString = "False";
            appearance72.Image = global::QRPMAS.UI.Properties.Resources.btn_delTable;
            this.uButtonDeleteRow.Appearance = appearance72;
            this.uButtonDeleteRow.Location = new System.Drawing.Point(12, 12);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 58;
            this.uButtonDeleteRow.Text = "행삭제";
            // 
            // ultraGrid4
            // 
            this.ultraGrid4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid4.DisplayLayout.Appearance = appearance24;
            this.ultraGrid4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance25.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance25.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid4.DisplayLayout.GroupByBox.Appearance = appearance25;
            appearance26.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance26;
            this.ultraGrid4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance27.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance27.BackColor2 = System.Drawing.SystemColors.Control;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid4.DisplayLayout.GroupByBox.PromptAppearance = appearance27;
            this.ultraGrid4.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            appearance28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid4.DisplayLayout.Override.ActiveCellAppearance = appearance28;
            appearance29.BackColor = System.Drawing.SystemColors.Highlight;
            appearance29.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid4.DisplayLayout.Override.ActiveRowAppearance = appearance29;
            this.ultraGrid4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid4.DisplayLayout.Override.CardAreaAppearance = appearance30;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid4.DisplayLayout.Override.CellAppearance = appearance31;
            this.ultraGrid4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid4.DisplayLayout.Override.CellPadding = 0;
            appearance32.BackColor = System.Drawing.SystemColors.Control;
            appearance32.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance32.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance32.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance32.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid4.DisplayLayout.Override.GroupByRowAppearance = appearance32;
            appearance33.TextHAlignAsString = "Left";
            this.ultraGrid4.DisplayLayout.Override.HeaderAppearance = appearance33;
            this.ultraGrid4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid4.DisplayLayout.Override.RowAppearance = appearance34;
            this.ultraGrid4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid4.DisplayLayout.Override.TemplateAddRowAppearance = appearance35;
            this.ultraGrid4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid4.Location = new System.Drawing.Point(12, 40);
            this.ultraGrid4.Name = "ultraGrid4";
            this.ultraGrid4.Size = new System.Drawing.Size(1024, 444);
            this.ultraGrid4.TabIndex = 1;
            this.ultraGrid4.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ultraGrid4_AfterCellUpdate);
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.ultraGrid5);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(1040, 482);
            // 
            // ultraGrid5
            // 
            this.ultraGrid5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraGrid5.DisplayLayout.Appearance = appearance12;
            this.ultraGrid5.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraGrid5.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance13.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance13.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance13.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.GroupByBox.Appearance = appearance13;
            appearance14.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid5.DisplayLayout.GroupByBox.BandLabelAppearance = appearance14;
            this.ultraGrid5.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance15.BackColor2 = System.Drawing.SystemColors.Control;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraGrid5.DisplayLayout.GroupByBox.PromptAppearance = appearance15;
            this.ultraGrid5.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraGrid5.DisplayLayout.MaxRowScrollRegions = 1;
            appearance16.BackColor = System.Drawing.SystemColors.Window;
            appearance16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraGrid5.DisplayLayout.Override.ActiveCellAppearance = appearance16;
            appearance17.BackColor = System.Drawing.SystemColors.Highlight;
            appearance17.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraGrid5.DisplayLayout.Override.ActiveRowAppearance = appearance17;
            this.ultraGrid5.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraGrid5.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.Override.CardAreaAppearance = appearance18;
            appearance19.BorderColor = System.Drawing.Color.Silver;
            appearance19.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraGrid5.DisplayLayout.Override.CellAppearance = appearance19;
            this.ultraGrid5.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraGrid5.DisplayLayout.Override.CellPadding = 0;
            appearance20.BackColor = System.Drawing.SystemColors.Control;
            appearance20.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance20.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance20.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance20.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraGrid5.DisplayLayout.Override.GroupByRowAppearance = appearance20;
            appearance21.TextHAlignAsString = "Left";
            this.ultraGrid5.DisplayLayout.Override.HeaderAppearance = appearance21;
            this.ultraGrid5.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraGrid5.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            this.ultraGrid5.DisplayLayout.Override.RowAppearance = appearance22;
            this.ultraGrid5.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraGrid5.DisplayLayout.Override.TemplateAddRowAppearance = appearance23;
            this.ultraGrid5.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraGrid5.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraGrid5.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraGrid5.Location = new System.Drawing.Point(12, 12);
            this.ultraGrid5.Name = "ultraGrid5";
            this.ultraGrid5.Size = new System.Drawing.Size(1024, 476);
            this.ultraGrid5.TabIndex = 3;
            this.ultraGrid5.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ultraGrid5_AfterCellUpdate);
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 130);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 17;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor10);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabelAmendReason);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor9);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor6);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTextAcceptUserID);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor7);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel9);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uText3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uText1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraDateTimeEditor1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uTabControl1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor8);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel10);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel8);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor5);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel7);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel6);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel5);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraTextEditor2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel4);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel3);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uText2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel2);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uLabel1);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // ultraTextEditor10
            // 
            this.ultraTextEditor10.Location = new System.Drawing.Point(144, 132);
            this.ultraTextEditor10.Name = "ultraTextEditor10";
            this.ultraTextEditor10.Size = new System.Drawing.Size(784, 21);
            this.ultraTextEditor10.TabIndex = 60;
            // 
            // uLabelAmendReason
            // 
            this.uLabelAmendReason.Location = new System.Drawing.Point(12, 132);
            this.uLabelAmendReason.Name = "uLabelAmendReason";
            this.uLabelAmendReason.Size = new System.Drawing.Size(125, 20);
            this.uLabelAmendReason.TabIndex = 59;
            this.uLabelAmendReason.Text = "ultraLabel1";
            // 
            // ultraTextEditor9
            // 
            appearance5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Appearance = appearance5;
            this.ultraTextEditor9.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor9.Location = new System.Drawing.Point(248, 108);
            this.ultraTextEditor9.Name = "ultraTextEditor9";
            this.ultraTextEditor9.ReadOnly = true;
            this.ultraTextEditor9.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor9.TabIndex = 58;
            // 
            // ultraTextEditor1
            // 
            appearance4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor1.Appearance = appearance4;
            this.ultraTextEditor1.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor1.Location = new System.Drawing.Point(144, 60);
            this.ultraTextEditor1.Name = "ultraTextEditor1";
            this.ultraTextEditor1.ReadOnly = true;
            this.ultraTextEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor1.TabIndex = 57;
            // 
            // ultraTextEditor6
            // 
            appearance48.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraTextEditor6.Appearance = appearance48;
            this.ultraTextEditor6.BackColor = System.Drawing.Color.PowderBlue;
            appearance2.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance2.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton1.Appearance = appearance2;
            editorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.ultraTextEditor6.ButtonsRight.Add(editorButton1);
            this.ultraTextEditor6.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraTextEditor6.Location = new System.Drawing.Point(528, 84);
            this.ultraTextEditor6.Name = "ultraTextEditor6";
            this.ultraTextEditor6.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor6.TabIndex = 56;
            // 
            // uTextAcceptUserID
            // 
            appearance10.BackColor = System.Drawing.Color.PowderBlue;
            this.uTextAcceptUserID.Appearance = appearance10;
            this.uTextAcceptUserID.BackColor = System.Drawing.Color.PowderBlue;
            appearance11.Image = global::QRPMAS.UI.Properties.Resources.btn_Zoom;
            appearance11.ImageHAlign = Infragistics.Win.HAlign.Center;
            editorButton2.Appearance = appearance11;
            editorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.WindowsVistaButton;
            this.uTextAcceptUserID.ButtonsRight.Add(editorButton2);
            this.uTextAcceptUserID.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.uTextAcceptUserID.Location = new System.Drawing.Point(144, 108);
            this.uTextAcceptUserID.Name = "uTextAcceptUserID";
            this.uTextAcceptUserID.Size = new System.Drawing.Size(100, 21);
            this.uTextAcceptUserID.TabIndex = 55;
            // 
            // ultraTextEditor7
            // 
            appearance9.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Appearance = appearance9;
            this.ultraTextEditor7.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor7.Location = new System.Drawing.Point(632, 84);
            this.ultraTextEditor7.Name = "ultraTextEditor7";
            this.ultraTextEditor7.ReadOnly = true;
            this.ultraTextEditor7.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor7.TabIndex = 54;
            // 
            // uLabel9
            // 
            this.uLabel9.Location = new System.Drawing.Point(396, 84);
            this.uLabel9.Name = "uLabel9";
            this.uLabel9.Size = new System.Drawing.Size(125, 20);
            this.uLabel9.TabIndex = 52;
            this.uLabel9.Text = "ultraLabel2";
            // 
            // uText3
            // 
            appearance8.BackColor = System.Drawing.Color.Gainsboro;
            this.uText3.Appearance = appearance8;
            this.uText3.BackColor = System.Drawing.Color.Gainsboro;
            this.uText3.Location = new System.Drawing.Point(144, 36);
            this.uText3.Name = "uText3";
            this.uText3.ReadOnly = true;
            this.uText3.Size = new System.Drawing.Size(100, 21);
            this.uText3.TabIndex = 51;
            // 
            // uText1
            // 
            appearance98.BackColor = System.Drawing.Color.Gainsboro;
            this.uText1.Appearance = appearance98;
            this.uText1.BackColor = System.Drawing.Color.Gainsboro;
            this.uText1.Location = new System.Drawing.Point(144, 12);
            this.uText1.Name = "uText1";
            this.uText1.ReadOnly = true;
            this.uText1.Size = new System.Drawing.Size(100, 21);
            this.uText1.TabIndex = 50;
            // 
            // ultraDateTimeEditor1
            // 
            appearance7.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor1.Appearance = appearance7;
            this.ultraDateTimeEditor1.BackColor = System.Drawing.Color.PowderBlue;
            this.ultraDateTimeEditor1.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista;
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(144, 84);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(100, 21);
            this.ultraDateTimeEditor1.TabIndex = 49;
            // 
            // uTabControl1
            // 
            this.uTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uTabControl1.Controls.Add(this.ultraTabSharedControlsPage3);
            this.uTabControl1.Controls.Add(this.ultraTabPageControl5);
            this.uTabControl1.Controls.Add(this.ultraTabPageControl6);
            this.uTabControl1.Location = new System.Drawing.Point(12, 180);
            this.uTabControl1.Name = "uTabControl1";
            this.uTabControl1.SharedControlsPage = this.ultraTabSharedControlsPage3;
            this.uTabControl1.Size = new System.Drawing.Size(1044, 508);
            this.uTabControl1.TabIndex = 48;
            ultraTab1.TabPage = this.ultraTabPageControl5;
            ultraTab1.Text = "정검항목상세";
            ultraTab2.TabPage = this.ultraTabPageControl6;
            ultraTab2.Text = "정검그룹설비리스트";
            this.uTabControl1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            // 
            // ultraTabSharedControlsPage3
            // 
            this.ultraTabSharedControlsPage3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage3.Name = "ultraTabSharedControlsPage3";
            this.ultraTabSharedControlsPage3.Size = new System.Drawing.Size(1040, 482);
            // 
            // ultraTextEditor8
            // 
            this.ultraTextEditor8.Location = new System.Drawing.Point(144, 156);
            this.ultraTextEditor8.Name = "ultraTextEditor8";
            this.ultraTextEditor8.Size = new System.Drawing.Size(784, 21);
            this.ultraTextEditor8.TabIndex = 47;
            // 
            // uLabel10
            // 
            this.uLabel10.Location = new System.Drawing.Point(12, 156);
            this.uLabel10.Name = "uLabel10";
            this.uLabel10.Size = new System.Drawing.Size(125, 20);
            this.uLabel10.TabIndex = 46;
            this.uLabel10.Text = "ultraLabel7";
            // 
            // uLabel8
            // 
            this.uLabel8.Location = new System.Drawing.Point(12, 83);
            this.uLabel8.Name = "uLabel8";
            this.uLabel8.Size = new System.Drawing.Size(125, 20);
            this.uLabel8.TabIndex = 44;
            this.uLabel8.Text = "ultraLabel6";
            // 
            // ultraTextEditor5
            // 
            appearance50.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Appearance = appearance50;
            this.ultraTextEditor5.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor5.Location = new System.Drawing.Point(144, 109);
            this.ultraTextEditor5.Name = "ultraTextEditor5";
            this.ultraTextEditor5.ReadOnly = true;
            this.ultraTextEditor5.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor5.TabIndex = 43;
            // 
            // uLabel7
            // 
            this.uLabel7.Location = new System.Drawing.Point(12, 108);
            this.uLabel7.Name = "uLabel7";
            this.uLabel7.Size = new System.Drawing.Size(125, 20);
            this.uLabel7.TabIndex = 41;
            this.uLabel7.Text = "ultraLabel2";
            // 
            // uLabel6
            // 
            this.uLabel6.Location = new System.Drawing.Point(12, 58);
            this.uLabel6.Name = "uLabel6";
            this.uLabel6.Size = new System.Drawing.Size(125, 20);
            this.uLabel6.TabIndex = 40;
            this.uLabel6.Text = "ultraLabel2";
            // 
            // ultraTextEditor4
            // 
            appearance6.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Appearance = appearance6;
            this.ultraTextEditor4.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor4.Location = new System.Drawing.Point(632, 59);
            this.ultraTextEditor4.Name = "ultraTextEditor4";
            this.ultraTextEditor4.ReadOnly = true;
            this.ultraTextEditor4.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor4.TabIndex = 38;
            // 
            // ultraTextEditor3
            // 
            appearance49.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Appearance = appearance49;
            this.ultraTextEditor3.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor3.Location = new System.Drawing.Point(528, 59);
            this.ultraTextEditor3.Name = "ultraTextEditor3";
            this.ultraTextEditor3.ReadOnly = true;
            this.ultraTextEditor3.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor3.TabIndex = 37;
            // 
            // uLabel5
            // 
            this.uLabel5.Location = new System.Drawing.Point(396, 59);
            this.uLabel5.Name = "uLabel5";
            this.uLabel5.Size = new System.Drawing.Size(125, 20);
            this.uLabel5.TabIndex = 36;
            this.uLabel5.Text = "ultraLabel2";
            // 
            // ultraTextEditor2
            // 
            appearance51.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Appearance = appearance51;
            this.ultraTextEditor2.BackColor = System.Drawing.Color.Gainsboro;
            this.ultraTextEditor2.Location = new System.Drawing.Point(528, 36);
            this.ultraTextEditor2.Name = "ultraTextEditor2";
            this.ultraTextEditor2.ReadOnly = true;
            this.ultraTextEditor2.Size = new System.Drawing.Size(100, 21);
            this.ultraTextEditor2.TabIndex = 35;
            // 
            // uLabel4
            // 
            this.uLabel4.Location = new System.Drawing.Point(396, 36);
            this.uLabel4.Name = "uLabel4";
            this.uLabel4.Size = new System.Drawing.Size(125, 20);
            this.uLabel4.TabIndex = 34;
            this.uLabel4.Text = "ultraLabel2";
            // 
            // uLabel3
            // 
            this.uLabel3.Location = new System.Drawing.Point(12, 36);
            this.uLabel3.Name = "uLabel3";
            this.uLabel3.Size = new System.Drawing.Size(125, 20);
            this.uLabel3.TabIndex = 32;
            this.uLabel3.Text = "ultraLabel2";
            // 
            // uText2
            // 
            appearance97.BackColor = System.Drawing.Color.Gainsboro;
            this.uText2.Appearance = appearance97;
            this.uText2.BackColor = System.Drawing.Color.Gainsboro;
            this.uText2.Location = new System.Drawing.Point(528, 12);
            this.uText2.Name = "uText2";
            this.uText2.ReadOnly = true;
            this.uText2.Size = new System.Drawing.Size(100, 21);
            this.uText2.TabIndex = 31;
            // 
            // uLabel2
            // 
            this.uLabel2.Location = new System.Drawing.Point(396, 12);
            this.uLabel2.Name = "uLabel2";
            this.uLabel2.Size = new System.Drawing.Size(125, 20);
            this.uLabel2.TabIndex = 30;
            this.uLabel2.Text = "ultraLabel2";
            // 
            // uLabel1
            // 
            this.uLabel1.Location = new System.Drawing.Point(12, 12);
            this.uLabel1.Name = "uLabel1";
            this.uLabel1.Size = new System.Drawing.Size(125, 20);
            this.uLabel1.TabIndex = 28;
            this.uLabel1.Text = "ultraLabel2";
            // 
            // uGrid1
            // 
            this.uGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGrid1.DisplayLayout.Appearance = appearance36;
            this.uGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance37.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance37.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.GroupByBox.Appearance = appearance37;
            appearance38.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance38;
            this.uGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance39.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance39.BackColor2 = System.Drawing.SystemColors.Control;
            appearance39.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance39;
            this.uGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.uGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance40;
            appearance41.BackColor = System.Drawing.SystemColors.Highlight;
            appearance41.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance41;
            this.uGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance42.BackColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.CardAreaAppearance = appearance42;
            appearance43.BorderColor = System.Drawing.Color.Silver;
            appearance43.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGrid1.DisplayLayout.Override.CellAppearance = appearance43;
            this.uGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance44.BackColor = System.Drawing.SystemColors.Control;
            appearance44.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance44.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance44.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance44.BorderColor = System.Drawing.SystemColors.Window;
            this.uGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance44;
            appearance45.TextHAlignAsString = "Left";
            this.uGrid1.DisplayLayout.Override.HeaderAppearance = appearance45;
            this.uGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            this.uGrid1.DisplayLayout.Override.RowAppearance = appearance46;
            this.uGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance47.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance47;
            this.uGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGrid1.Location = new System.Drawing.Point(0, 80);
            this.uGrid1.Name = "uGrid1";
            this.uGrid1.Size = new System.Drawing.Size(1070, 760);
            this.uGrid1.TabIndex = 16;
            this.uGrid1.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGrid1_AfterCellUpdate);
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearch);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearch);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 15;
            // 
            // uComboSearch
            // 
            this.uComboSearch.Location = new System.Drawing.Point(388, 12);
            this.uComboSearch.Name = "uComboSearch";
            this.uComboSearch.Size = new System.Drawing.Size(140, 21);
            this.uComboSearch.TabIndex = 6;
            this.uComboSearch.Text = "uCombo";
            // 
            // uLabelSearch
            // 
            this.uLabelSearch.Location = new System.Drawing.Point(284, 12);
            this.uLabelSearch.Name = "uLabelSearch";
            this.uLabelSearch.Size = new System.Drawing.Size(100, 20);
            this.uLabelSearch.TabIndex = 5;
            this.uLabelSearch.Text = "ultraLabel1";
            // 
            // uComboPlant
            // 
            this.uComboPlant.Location = new System.Drawing.Point(116, 12);
            this.uComboPlant.Name = "uComboPlant";
            this.uComboPlant.Size = new System.Drawing.Size(140, 21);
            this.uComboPlant.TabIndex = 4;
            this.uComboPlant.Text = "uCombo";
            // 
            // uLabelPlant
            // 
            this.uLabelPlant.Location = new System.Drawing.Point(12, 12);
            this.uLabelPlant.Name = "uLabelPlant";
            this.uLabelPlant.Size = new System.Drawing.Size(100, 20);
            this.uLabelPlant.TabIndex = 2;
            this.uLabelPlant.Text = "ultraLabel1";
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 14;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // frmMASZ0006
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGrid1);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0006";
            this.Load += new System.EventHandler(this.frmMASZ0006_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0006_Activated);
            this.Resize += new System.EventHandler(this.frmMASZ0006_Resize);
            this.ultraTabPageControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid4)).EndInit();
            this.ultraTabPageControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGrid5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAcceptUserID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTabControl1)).EndInit();
            this.uTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTextEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboPlant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor7;
        private Infragistics.Win.Misc.UltraLabel uLabel9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl uTabControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        private Infragistics.Win.UltraWinGrid.UltraGrid ultraGrid5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor8;
        private Infragistics.Win.Misc.UltraLabel uLabel10;
        private Infragistics.Win.Misc.UltraLabel uLabel8;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor5;
        private Infragistics.Win.Misc.UltraLabel uLabel7;
        private Infragistics.Win.Misc.UltraLabel uLabel6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor4;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor3;
        private Infragistics.Win.Misc.UltraLabel uLabel5;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor2;
        private Infragistics.Win.Misc.UltraLabel uLabel4;
        private Infragistics.Win.Misc.UltraLabel uLabel3;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uText2;
        private Infragistics.Win.Misc.UltraLabel uLabel2;
        private Infragistics.Win.Misc.UltraLabel uLabel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGrid1;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearch;
        private Infragistics.Win.Misc.UltraLabel uLabelSearch;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelPlant;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor6;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAcceptUserID;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor9;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor1;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor ultraTextEditor10;
        private Infragistics.Win.Misc.UltraLabel uLabelAmendReason;
    }
}