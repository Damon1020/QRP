﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 설비관리                                              */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0029_S.cs                                       */
/* 프로그램명   : 설비점검정보등록                                      */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-04                                            */
/* 수정이력     : 2011-08-12 : ~~~~~ 추가 (권종구)                      */
/*                xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;
using System.Collections;
using System.IO;

namespace QRPMAS.UI
{
    public partial class frmMAS0029_S : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수

        QRPGlobal SysRes = new QRPGlobal();

        //BL호출을 위한 전역변수
        QRPBrowser brwChannel = new QRPBrowser();

        //수정 유무 확인 전역변수
        string strEdit = "F";

        public frmMAS0029_S()
        {
            InitializeComponent();
        }

        private void frmMAS0029_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            brwChannel.mfActiveToolBar(this.ParentForm, true, true, true, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0029_Load(object sender, EventArgs e)
        {
            // SystemInfo Resource 변수 선언
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            // 타이틀 Text 설정함수 호출
            this.titleArea.mfSetLabelText("설비점검정보등록", m_resSys.GetString("SYS_FONTNAME"), 12);

            // 초기화 Method
            SetToolAuth();
            InitButton();
            InitComboBox();
            InitGrid();
            InitLabel();
            InitValue();

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Label 초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel wLabel = new WinLabel();

                wLabel.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipGroup, "설비그룹", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelProcessGroup, "설비대분류", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchStation, "Station", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipLoc, "위치", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelSearchEquipType, "설비중분류", m_resSys.GetString("SYS_FONTNAME"), true, true);

                wLabel.mfSetLabel(this.uLabelStandardNo, "표준번호", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelCreateUser, "생성자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelCreateDate, "생성일", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAcceptUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, true);
                wLabel.mfSetLabel(this.uLabelAdmitSecondUser, "승인자", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelAcceptState, "승인상태", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelEtc, "비고", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRejectReason, "반려사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
                wLabel.mfSetLabel(this.uLabelRevisionReason, "개정사유", m_resSys.GetString("SYS_FONTNAME"), true, false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Button 초기화

        /// </summary>
        private void InitButton()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton wButton = new WinButton();

                wButton.mfSetButton(this.uButtonDeleteRow1, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
                wButton.mfSetButton(this.uButtonCopy, "복사", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Copy);
                wButton.mfSetButton(this.uButtonFileDown, "다운로드", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Filedownload);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// ComboBox 초기화

        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                // Search Plant ComboBox
                // Call BL
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);
                
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));

                wCombo.mfSetComboEditor(this.uComboSearchPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "선택", "PlantCode", "PlantName", dtPlant);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Grid 초기화

        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid wGrid = new WinGrid();

                #region 점검항목상세 Grid
                // 점검항목상세 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipPMD, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True
                    , Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 컬럼설정
                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "Seq", "순번", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 80, false, true, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "", "0");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectRegion", "부위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, true, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMPeriodCode", "점검주기", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectCriteria", "기준", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMInspectName", "점검항목", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "PMMethod", "점검방법", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "ImageFile", "첨부파일", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 1000
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "FaultFixMethod", "이상조치방법", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "MeasureValueFlag", "수치입력여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 0
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "StandardManCount", "표준공수(人)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "StandardTime", "표준공수(分)", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, true, 10
                    , Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Integer, "", "nnnnnnn", "0");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "UnitDesc", "단위", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 100
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                wGrid.mfSetGridColumn(this.uGridEquipPMD, 0, "LevelCode", "난이도", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 120, false, false, 1
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "");

                this.uGridEquipPMD.DisplayLayout.Bands[0].Columns["MeasureValueFlag"].Header.CheckBoxVisibility = Infragistics.Win.UltraWinGrid.HeaderCheckBoxVisibility.Never;

                #region DropDown

                string strLang = m_resSys.GetString("SYS_LANG");
                string strDefault = "선택";
                //--점검주기 콤보
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);

                DataTable dtPeriod = clsCommonCode.mfReadCommonCode("C0007", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridEquipPMD, 0, "PMPeriodCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefault, dtPeriod);
                //--난이도 콤보

                DataTable dtLevel = clsCommonCode.mfReadCommonCode("C0008", m_resSys.GetString("SYS_LANG"));

                wGrid.mfSetGridColumnValueList(this.uGridEquipPMD, 0, "LevelCode", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", strDefault, dtLevel);

                #endregion

                // 빈줄추가
                wGrid.mfAddRowGrid(this.uGridEquipPMD, 0);
                
                // Set Font
                this.uGridEquipPMD.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipPMD.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                #endregion

                #region 점검그룹 설비리스트 Grid
                // 일반설정
                wGrid.mfInitGeneralGrid(this.uGridEquipList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button
                    , Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons
                    , Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                
                string strName = "";

                if (strLang.Equals("KOR"))
                    strName = "주간점검,월간점검,분기점검,반기점검,년간점검";
                else if (strLang.Equals("CHN"))
                    strName = "每周点检,每月点检,季度点检,半年点检,年间点检";
                else if (strLang.Equals("ENG"))
                    strName = "주간점검,월간점검,분기점검,반기점검,년간점검";


                string[] strGroups = strName.Split(',');
                

                // Set GridGroup
                this.uGridEquipList.DisplayLayout.Bands[0].RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.GroupLayout;
                Infragistics.Win.UltraWinGrid.UltraGridGroup group1 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupWeek", strGroups[0], 7, 0, 1, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup group2 = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupMonth", strGroups[1], 8, 0, 2, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupQUA = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupQUA", strGroups[2], 10, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupHAF = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupHAF", strGroups[3], 13, 0, 3, 2, false);
                Infragistics.Win.UltraWinGrid.UltraGridGroup uGroupYear = wGrid.mfSetGridGroup(this.uGridEquipList, 0, "GroupYEA", strGroups[4], 16, 0, 3, 2, false);

                // 컬럼설정

                //-->설비에 대한 설비그룹은 MDM에서 정하기 때문에 설비편집 불가 처리
                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "AreaCode", "Area", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "StationCode", "Station", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipLocCode", "위치", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 3, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipProcGubunCode", "설비공정구분", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 50
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 4, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipCode", "설비코드", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 5, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "EquipName", "설비명", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 150, false, false, 50
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 6, 0, 1, 2, null);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMWeekDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 0, 0, 1, 1, group1);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 0, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMMonthDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 1, 0, 1, 1, group2);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 0, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 1, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMQuarterDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupQUA);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 0, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 1, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMHalfDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupHAF);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearMonth", "점검월", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 0, 0, 1, 1, uGroupYear);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearWeek", "점검주간", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "0", 1, 0, 1, 1, uGroupYear);

                wGrid.mfSetGridColumn(this.uGridEquipList, 0, "PMYearDayName", "점검요일", false, Infragistics.Win.UltraWinGrid.Activation.NoEdit, 100, false, false, 3
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "", 2, 0, 1, 1, uGroupYear);

                // Set Font
                this.uGridEquipList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridEquipList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupWeek"].CellAppearance.ForeColor = Color.Black;
                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupMonth"].CellAppearance.ForeColor = Color.Black;
                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupQUA"].CellAppearance.ForeColor = Color.Black;
                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupHAF"].CellAppearance.ForeColor = Color.Black;
                this.uGridEquipList.DisplayLayout.Bands[0].Groups["GroupYEA"].CellAppearance.ForeColor = Color.Black;


                #endregion
                

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Value초기화
        /// </summary>
        private void InitValue()
        {
            try
            {
                // SystemInfo Resource 변수 선언
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strUser = m_resSys.GetString("SYS_USERID");
                this.uTextStandardNo.Text = "";
                this.uTextVersionNo.Text = "";
                
                this.uTextCreateUserName.Text = "";
                this.uDateCreateDate.Value = DateTime.Now;
                
                this.uTextAcceptUserName.Text = "";
                this.uTextAcceptState.Text = "";
                this.uTextAcceptState.Tag = null;
                this.uTextEtc.Text = "";
                this.uTextRejectReason.Text = "";
                this.uTextRevisionReason.Text = "";
                
                this.uTextCreateUserName.Text = "";

                this.uTextAcceptUserID.Text = strUser;
                this.uTextAcceptUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextCreateUserID.Text = strUser;
                this.uTextCreateUserName.Text = m_resSys.GetString("SYS_USERNAME");
                this.uTextSecondID.Clear();
                this.uTextSecondName.Clear();
                
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 툴바

        /// <summary>
        /// 검색
        /// </summary>
        public void mfSearch()
        {
            try
            {

                //System ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                #region 필수입력사항 확인

                //입력사항 확인
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001228", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                

                if (this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001228", "M000128", Infragistics.Win.HAlign.Right);
                    this.uComboSearchStation.DropDown();
                    return;
                }

                if (this.uComboSearchEquipLoc.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001228", "M000831", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipLoc.DropDown();
                    return;
                }

                if (this.uComboProcessGroup.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001228", "M000698", Infragistics.Win.HAlign.Right);

                    this.uComboProcessGroup.DropDown();
                    return;
                }

                if (this.uComboSearchEquipType.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001228", "M000720", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipType.DropDown();
                    return;
                }
                if (this.uComboSearchEquipGroup.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001235", "M001228", "M000695", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipGroup.DropDown();
                    return;
                }
                
                #endregion

                //공장코드 와 설비그룹코드 ,Station ,위치,유형 저장
                string strPlantCode = uComboSearchPlant.Value.ToString();
                string strEuqipGroupCode = uComboSearchEquipGroup.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();

                #region BL
                //Popup창 실행
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);

                DataTable dtEquipPMH = clsEquipPMH.mfReadEquipPMH_H(strPlantCode, strEuqipGroupCode, strStationCode, strEquipLocCode, strEquipTypeCode, strProcessGroup, m_resSys.GetString("SYS_LANG"));
                #endregion

                #region 데이터 바인드 및 승인상태확인

                if (dtEquipPMH.Rows.Count != 0)
                {
                    //데이터바인드
                    this.uTextStandardNo.Text = dtEquipPMH.Rows[0]["StdNumber"].ToString();
                    this.uTextVersionNo.Text = dtEquipPMH.Rows[0]["VersionNum"].ToString();

                    this.uTextAcceptState.Tag = dtEquipPMH.Rows[0]["AdmitStatusCode"].ToString();
                    this.uTextAcceptState.Text = dtEquipPMH.Rows[0]["AdmitStatusName"].ToString();

                    this.uTextAcceptUserID.Text = dtEquipPMH.Rows[0]["AdmitID"].ToString();
                    this.uTextCreateUserID.Text = dtEquipPMH.Rows[0]["WriteID"].ToString();
                    this.uTextEtc.Text = dtEquipPMH.Rows[0]["EtcDesc"].ToString();
                    this.uTextRejectReason.Text = dtEquipPMH.Rows[0]["RejectReason"].ToString();
                    this.uTextRevisionReason.Text = dtEquipPMH.Rows[0]["RevisionReason"].ToString();
                    this.uDateCreateDate.Value = dtEquipPMH.Rows[0]["WriteDate"].ToString();
                    this.uTextCreateUserName.Text = dtEquipPMH.Rows[0]["WriteName"].ToString();
                    this.uTextAcceptUserName.Text = dtEquipPMH.Rows[0]["AdmitName"].ToString();

                    this.uTextSecondID.Text = dtEquipPMH.Rows[0]["AdmitSecondID"].ToString();
                    this.uTextSecondName.Text = dtEquipPMH.Rows[0]["AdmitSecondName"].ToString();
                    ///////////
                    string strStdNumber = dtEquipPMH.Rows[0]["StdNumber"].ToString();
                    string strVersionNum = dtEquipPMH.Rows[0]["VersionNum"].ToString();

                    EquipSearch(strStdNumber, strVersionNum);

                    //-------------------------------//
                    this.MdiParent.Cursor = Cursors.Default;

                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (dtEquipPMH.Rows[0]["AdmitStatusCode"].ToString() == "RE")
                    {
                        ControlReadOnly("BB");

                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001264", "M001075", "M000413",
                                             Infragistics.Win.HAlign.Right);

                    }
                    else if (dtEquipPMH.Rows[0]["AdmitStatusCode"].ToString() == "AR")
                    {
                        ControlReadOnly("AR");

                        //msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        //                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //                     "확인창", "정보확인", "승인 요청 중 이므로 수정이 불가합니다.",
                        //                     Infragistics.Win.HAlign.Right);
                        
                    }
                    else if (dtEquipPMH.Rows[0]["AdmitStatusCode"].ToString() == "FN")
                    {
                        ControlReadOnly("FN");
                        this.uTextVersionNo.Text = (Convert.ToInt32(this.uTextVersionNo.Text) + 1).ToString();
                        //if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        //             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //             "확인창", "정보확인", "승인 완료된 정보입니다. 개정 하시겠습니까?",
                        //             Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        //{


                        //    ControlReadOnly("FN");
                        //    this.uTextVersionNo.Text = (Convert.ToInt32(this.uTextVersionNo.Text) + 1).ToString();
                        //}
                    }
                    else
                    {
                        ControlReadOnly("BB");
                    }
                }
                #endregion

                else
                {
                    this.MdiParent.Cursor = Cursors.Default;

                    m_ProgressPopup.mfCloseProgressPopup(this);

                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M001135", "M001115", "M001102",
                                              Infragistics.Win.HAlign.Right);

                    InitSearch();

                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
                //System ResouectInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                System.Windows.Forms.DialogResult result;

                //검색조건의 정보화 표준번호저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipGroupCode = this.uComboSearchEquipGroup.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strAdmitStatus = "";

                //그룹웨어 전송에 필요한 Parameter들
                string strPlantName = this.uComboSearchPlant.Text;
                string strStationName = this.uComboSearchStation.Text;
                string strEquipGroupName = this.uComboSearchEquipGroup.Text; 
                //개정번호 최대값
                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);
                DataTable dtVersionNum = clsEquipPMH.dtReadEquipPMHMaxVer(this.uTextStandardNo.Text);
                string strVersionNo = Convert.ToString(Convert.ToInt32(dtVersionNum.Rows[0]["MaxVersion"].ToString()) + 1);
                
                
                //데이터SET
                DataTable dtEquipPMH = clsEquipPMH.mfSetEquipPMHData();

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMD), "EquipPMD");
                QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                brwChannel.mfCredentials(clsEquipPMD);

                DataTable dtEquipPMD = clsEquipPMD.mfSetEquipPMDData();

                #region 데이터 유무확인
                //---------------------------------------데이터 여부확인 -----------------------------------------------------//
                DataTable dtPMH = clsEquipPMH.mfReadEquipPMH_H(strPlantCode, strEquipGroupCode, strStationCode, strEquipLocCode, strEquipTypeCode, strProcessGroup, m_resSys.GetString("SYS_LANG"));

                //정보가 있을시
                if (dtPMH.Rows.Count != 0)
                {
                    strAdmitStatus = dtPMH.Rows[0]["AdmitStatusCode"].ToString();

                    if (this.uTextRevisionReason.ReadOnly.Equals(true) && strAdmitStatus == "FN")
                    {
                        if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001075", "M000759",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        {
                            this.uTextVersionNo.Text = (Convert.ToInt32(strVersionNo) + 1).ToString();
                            ControlReadOnly("FN");
                            return;

                        }
                        else
                        {
                            return;
                        }
                    }
                    if (strAdmitStatus == "AR")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                    Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                    "M001264", "M001075", "M000761",
                                    Infragistics.Win.HAlign.Right);
                        return;
                    }
                    if (strAdmitStatus == "WR" || strAdmitStatus == "RE")
                    {
                        strVersionNo = Convert.ToString(Convert.ToInt32(strVersionNo) - 1);
                    }
                }


                if (this.uTextVersionNo.Text == "")
                {
                    strVersionNo = "0";
                }

                #endregion

                #region 필수 입력사항체크

                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000278", Infragistics.Win.HAlign.Right);
                    //Foucs 
                    this.uComboSearchPlant.DropDown();
                    return;
                }
               

                if (strStationCode.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000128", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uComboSearchStation.DropDown();
                    return;
                }

                if (this.uComboSearchEquipLoc.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001230", "M000831", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipLoc.DropDown();
                    return;
                }

                if (this.uComboProcessGroup.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001230", "M000698", Infragistics.Win.HAlign.Right);

                    this.uComboProcessGroup.DropDown();
                    return;
                }

                if (this.uComboSearchEquipType.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001230", "M000720", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipType.DropDown();
                    return;
                }
                if (this.uComboSearchEquipGroup.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000696", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uComboSearchEquipGroup.DropDown();
                    return;
                }
                if (this.uTextCreateUserID.Text.Equals(string.Empty) 
                    || this.uTextCreateUserName.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000655", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uTextCreateUserID.Focus();
                    return;
                }
                if (this.uDateCreateDate.Value == null 
                    || this.uDateCreateDate.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000654", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uDateCreateDate.DropDown();
                    return;
                }
                if (this.uTextAcceptUserID.Text.Equals(string.Empty) 
                    || this.uTextAcceptUserName.Text.Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000770", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uTextAcceptUserID.Focus();
                    return;
                }

                if (this.uTextCreateUserID.Text.Equals(this.uTextAcceptUserID.Text)
                    || this.uTextCreateUserID.Text.Equals(this.uTextSecondID.Text))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M001487", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uTextAcceptUserID.Focus();
                    return;
                }
                if (this.uTextAcceptUserID.Text.Equals(this.uTextSecondID.Text))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M001488", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uTextSecondID.Focus();
                    return;
                }
                ////콤보박스 선택값 Validation Check//////////
                QRPCOM.QRPUI.CommonControl check = new QRPCOM.QRPUI.CommonControl();
                if (!check.mfCheckValidValueBeforSave(this)) return;
                /////////////////////////////////////////////

                #endregion

                #region 헤더저장
                if (uTextStandardNo.Text == "")
                {
                    DataRow drPMH;
                    drPMH = dtEquipPMH.NewRow();
                    drPMH["StdNumber"] = "";
                    drPMH["VersionNum"] = strVersionNo;
                    drPMH["PlantCode"] = strPlantCode;
                    drPMH["EquipGroupCode"] = strEquipGroupCode;
                    drPMH["EquipLocCode"] = strEquipLocCode;
                    drPMH["EquipLargeTypeCode"] = strEquipTypeCode;
                    drPMH["StationCode"] = strStationCode;
                    drPMH["ProcessGroup"] = strProcessGroup;
                    drPMH["WriteID"] = uTextCreateUserID.Text;
                    drPMH["WriteDate"] = uDateCreateDate.DateTime.Date.ToString("yyyy-MM-dd");
                    drPMH["AdmitID"] = uTextAcceptUserID.Text;
                    drPMH["AdmitSecondID"] = uTextAcceptUserID.Text;
                    drPMH["EtcDesc"] = uTextEtc.Text;
                    drPMH["RejectReason"] = uTextRejectReason.Text;
                    drPMH["RevisionReason"] = uTextRevisionReason.Text;
                    //GW 변수들
                    drPMH["PlantName"] = this.uComboSearchPlant.Text;
                    drPMH["StationName"] = this.uComboSearchStation.Text;
                    drPMH["EquipLocName"] = this.uComboSearchEquipLoc.Text;
                    drPMH["EquipLargeTypeName"] = this.uComboProcessGroup.Text;
                    drPMH["EquipMiddleTypeName"] = this.uComboSearchEquipType.Text;
                    drPMH["EquipGroupName"] = this.uComboSearchEquipGroup.Text;
                    dtEquipPMH.Rows.Add(drPMH);
                }
                else
                {
                    DataRow drPMH;
                    drPMH = dtEquipPMH.NewRow();
                    drPMH["StdNumber"] = this.uTextStandardNo.Text;
                    drPMH["VersionNum"] = strVersionNo;
                    drPMH["PlantCode"] = strPlantCode;
                    drPMH["EquipGroupCode"] = strEquipGroupCode;
                    drPMH["EquipLocCode"] = strEquipLocCode;
                    drPMH["EquipLargeTypeCode"] = strEquipTypeCode;
                    drPMH["StationCode"] = strStationCode;
                    drPMH["ProcessGroup"] = strProcessGroup;
                    drPMH["WriteID"] = this.uTextCreateUserID.Text;
                    drPMH["WriteDate"] = this.uDateCreateDate.DateTime.Date.ToString("yyyy-MM-dd");
                    drPMH["AdmitID"] = this.uTextAcceptUserID.Text;
                    drPMH["AdmitSecondID"] =this.uTextSecondID.Text;
                    drPMH["EtcDesc"] = this.uTextEtc.Text;
                    drPMH["RejectReason"] = this.uTextRejectReason.Text;
                    drPMH["RevisionReason"] = this.uTextRevisionReason.Text;
                    //GW 변수들
                    drPMH["PlantName"] = this.uComboSearchPlant.Text;
                    drPMH["StationName"] = this.uComboSearchStation.Text;
                    drPMH["EquipLocName"] = this.uComboSearchEquipLoc.Text;
                    drPMH["EquipLargeTypeName"] = this.uComboProcessGroup.Text;
                    drPMH["EquipMiddleTypeName"] = this.uComboSearchEquipType.Text;
                    drPMH["EquipGroupName"] = this.uComboSearchEquipGroup.Text;
                    dtEquipPMH.Rows.Add(drPMH);
                }

                if (dtEquipPMH.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M001047"
                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion

                #region 상세저장

                QRPCOM.UI.frmCOM0012 frm = new QRPCOM.UI.frmCOM0012();
                DialogResult _dr = new DialogResult();

                //신규나 수정된 파일 첨부유무판단
                string strFileChk = "";

                //파일 경로 저장할 배열변수
                ArrayList arrFil = new ArrayList();
                ArrayList arrFileName = new ArrayList();
                ArrayList arrFuulName = new ArrayList();

                if (this.uGridEquipPMD.Rows.Count != 0)
                {
                    //--Grid 내용을 저장할 경우 활성화 Cell을 해당 Grid의 맨 앞 Cell로 이동시킨다.
                    this.uGridEquipPMD.ActiveCell = this.uGridEquipPMD.Rows[0].Cells[0];

                    for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                    {
                        //이미지 가 있을시
                        //if (this.uGridEquipPMD.Rows[i].RowSelectorAppearance.Image != null)
                        //{
                        //행이 숨김 상태가 아니면
                        if (this.uGridEquipPMD.Rows[i].Hidden == false)
                        {
                            if (this.uGridEquipPMD.Rows[i].Cells["PMInspectName"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001230", "M001068", Infragistics.Win.HAlign.Right);

                                //Foucs Cell
                                this.uGridEquipPMD.ActiveCell = this.uGridEquipPMD.Rows[i].Cells["PMInspectName"];
                                this.uGridEquipPMD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;

                            }
                            if (this.uGridEquipPMD.Rows[i].Cells["PMPeriodCode"].Value.ToString() == "")
                            {
                                msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                     Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001264", "M001230", "M001067", Infragistics.Win.HAlign.Right);

                                //Foucs Cell
                                this.uGridEquipPMD.ActiveCell = this.uGridEquipPMD.Rows[i].Cells["PMPeriodCode"];
                                this.uGridEquipPMD.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);
                                return;

                            }

                            //첨부파일에 신규나 수정이 된 즉 파일 경로가 있으면
                            if (this.uGridEquipPMD.Rows[i].GetCellValue("ImageFile").ToString().Contains(":\\"))
                            {
                                strFileChk = "OK";
                                FileInfo fil = new FileInfo(this.uGridEquipPMD.Rows[i].GetCellValue("ImageFile").ToString());

                                //파일이름 설정
                                string strFil = fil.DirectoryName + "\\";
                                string strFileName = "_" + this.uGridEquipPMD.Rows[i].RowSelectorNumber + "_" + fil.Name;

                                //파일 정보 배열 변수에 지정
                                arrFil.Add(strFil);
                                arrFileName.Add(strFileName);
                                arrFuulName.Add(this.uGridEquipPMD.Rows[i].GetCellValue("ImageFile"));

                                DataRow drPMD;
                                drPMD = dtEquipPMD.NewRow();
                                drPMD["StdNumber"] = this.uTextStandardNo.Text;
                                drPMD["VersionNum"] = strVersionNo;
                                drPMD["Seq"] = this.uGridEquipPMD.Rows[i].RowSelectorNumber.ToString();
                                drPMD["PMInspectName"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectName"].Value;
                                drPMD["PMInspectRegion"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectRegion"].Value;
                                drPMD["PMInspectCriteria"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectCriteria"].Value;
                                drPMD["PMPeriodCode"] = this.uGridEquipPMD.Rows[i].Cells["PMPeriodCode"].Value;
                                drPMD["PMMethod"] = this.uGridEquipPMD.Rows[i].Cells["PMMethod"].Value.ToString();
                                drPMD["FaultFixMethod"] = this.uGridEquipPMD.Rows[i].Cells["FaultFixMethod"].Value;

                                drPMD["ImageFile"] = strFileName;

                                drPMD["UnitDesc"] = this.uGridEquipPMD.Rows[i].GetCellValue("UnitDesc");
                                drPMD["StandardManCount"] = this.uGridEquipPMD.Rows[i].Cells["StandardManCount"].Value.ToString() == "" ? 0 : this.uGridEquipPMD.Rows[i].Cells["StandardManCount"].Value;
                                drPMD["StandardTime"] = this.uGridEquipPMD.Rows[i].Cells["StandardTime"].Value.ToString() == "" ? 0 : this.uGridEquipPMD.Rows[i].Cells["StandardTime"].Value;
                                drPMD["LevelCode"] = this.uGridEquipPMD.Rows[i].Cells["LevelCode"].Value;
                                drPMD["MeasureValueFlag"] = Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["MeasureValueFlag"].Value) == false ? "F" : "T";
                                dtEquipPMD.Rows.Add(drPMD);

                            }
                            //파일 경로가 없는 경우
                            else
                            {
                                DataRow drPMD;
                                drPMD = dtEquipPMD.NewRow();
                                drPMD["StdNumber"] = this.uTextStandardNo.Text;
                                drPMD["VersionNum"] = strVersionNo;
                                drPMD["Seq"] = this.uGridEquipPMD.Rows[i].RowSelectorNumber.ToString();
                                drPMD["PMInspectName"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectName"].Value.ToString();
                                drPMD["PMInspectRegion"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectRegion"].Value.ToString();
                                drPMD["PMInspectCriteria"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectCriteria"].Value.ToString();
                                drPMD["PMPeriodCode"] = this.uGridEquipPMD.Rows[i].Cells["PMPeriodCode"].Value.ToString();
                                drPMD["PMMethod"] = this.uGridEquipPMD.Rows[i].Cells["PMMethod"].Value.ToString();
                                drPMD["FaultFixMethod"] = this.uGridEquipPMD.Rows[i].Cells["FaultFixMethod"].Value.ToString();
                                drPMD["ImageFile"] = this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString();
                                drPMD["UnitDesc"] = this.uGridEquipPMD.Rows[i].GetCellValue("UnitDesc");
                                if (this.uGridEquipPMD.Rows[i].Cells["StandardManCount"].Value.ToString() == "")
                                {
                                    drPMD["StandardManCount"] = "0";
                                }
                                else
                                {
                                    drPMD["StandardManCount"] = this.uGridEquipPMD.Rows[i].Cells["StandardManCount"].Value;
                                }
                                if (this.uGridEquipPMD.Rows[i].Cells["StandardTime"].Value.ToString() == "")
                                {
                                    drPMD["StandardTime"] = "0";
                                }
                                else
                                {
                                    drPMD["StandardTime"] = this.uGridEquipPMD.Rows[i].Cells["StandardTime"].Value.ToString();
                                }
                                drPMD["LevelCode"] = this.uGridEquipPMD.Rows[i].Cells["LevelCode"].Value.ToString();
                                drPMD["MeasureValueFlag"] = Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["MeasureValueFlag"].Value) == false ? "F" : "T";
                                dtEquipPMD.Rows.Add(drPMD);
                            }

                        }

                        //}
                    }
                }


                if (dtEquipPMD.Rows.Count == 0)
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Error, 500, 500
                                , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                , "M001264", "M001230", "M001054"
                                , Infragistics.Win.HAlign.Right);
                    return;
                }

                #endregion




                DialogResult diResult = msg.mfSetMessageBox(MessageBoxType.ReqAgreeSave, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001053", "M000876",
                                        Infragistics.Win.HAlign.Right);


                
                if (diResult == DialogResult.Yes)
                {
                    //승인상태 승인요청
                    dtEquipPMH.Rows[0]["AdmitStatusCode"] = "AR";

                    //////////결재선 팝업창 호출        -- 2012-07-16 QRP내에서 승인처리
                    //frm.ApprovalUserID = uTextAcceptUserID.Text;
                    //frm.ApprovalUserName = uTextAcceptUserName.Text;
                    //frm.WriteID = uTextCreateUserID.Text;
                    //frm.WriteName = uTextCreateUserName.Text;

                    //_dr = frm.ShowDialog();
                    //if (_dr == DialogResult.No || _dr == DialogResult.Cancel)
                    //{
                    //    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                    , "M001264", "M001053", "M001030", Infragistics.Win.HAlign.Center);

                    //    return;
                    //}
                }
                else if (diResult == DialogResult.No)
                {
                    //승인상태  작성중
                    dtEquipPMH.Rows[0]["AdmitStatusCode"] = "WR";
                }
                else if (diResult == DialogResult.Cancel)
                {
                    return;
                }


                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread t1 = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "저장중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //처리 로직//

                string strRtn = clsEquipPMH.mfSaveEquipPMH_H_S(dtEquipPMH, dtEquipPMD, m_resSys.GetString("SYS_USERID"), m_resSys.GetString("SYS_USERIP"));

                //Decoding//
                TransErrRtn ErrRtn = new TransErrRtn();
                ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                /////////////

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);
                

                string strGetStd = ErrRtn.mfGetReturnValue(0);
                string strGetVerNum = ErrRtn.mfGetReturnValue(1);

                if (ErrRtn.ErrNum == 0)
                {
                    #region 파일 업로드

                    //파일 경로가 있는 경우
                    if (!strFileChk.Equals(string.Empty))
                    {
                        //리턴받아온 표준번호와 개정번호 저장
                        
                        #region 파일 경로 첨부파일 경로 BL
                        //파일 경로 가져오기 
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                        QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAcce = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                        brwChannel.mfCredentials(clsSysAcce);

                        DataTable dtSysAcce = clsSysAcce.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                        //첨부파일 경로 가져오기
                        brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                        QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                        brwChannel.mfCredentials(clsSysFilePath);

                        DataTable dtSysFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0023");

                        #endregion

                        for (int i = 0; i < arrFil.Count; i++)
                        {
                            /*arrFil = 파일 디렉토리 
                             *              + "\\"                                  arrFileName = _순번_파일명*/
                            arrFil[i] = arrFil[i] + strGetStd + "_" + strGetVerNum + arrFileName[i];

                            //변경한 화일이 있으면 삭제하기
                            if (File.Exists(arrFil[i].ToString()))
                                File.Delete(arrFil[i].ToString());

                            //변경된파일이름으로 복사하기
                            File.Copy(arrFuulName[i].ToString(), arrFil[i].ToString());
                        }

                        //Upload정보 설정
                        frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                        fileAtt.mfInitSetSystemFileInfo(arrFil, "", dtSysAcce.Rows[0]["SystemAddressPath"].ToString(),
                                                                   dtSysFilePath.Rows[0]["ServerPath"].ToString(),
                                                                   dtSysFilePath.Rows[0]["FolderName"].ToString(),
                                                                   dtSysAcce.Rows[0]["AccessID"].ToString(),
                                                                   dtSysAcce.Rows[0]["AccessPassword"].ToString());
                        fileAtt.ShowDialog();
                    }

                    #endregion

                    #region 그룹웨어 전송 -- 2012-07-16 그룹웨어 처리 안함 주석
                    //if (diResult == DialogResult.Yes)
                    //{
                    //    // //From DataTable 찾기
                    //    int intVersion = Convert.ToInt32(strGetVerNum) - 1;
                    //    dtEquipPMH.Rows[0]["StdNumber"] = strGetStd;
                    //    dtEquipPMH.Rows[0]["VersionNum"] = strGetVerNum;
                    //    DataTable dtFrom = clsEquipPMD.mfReadEquipPMD(strGetStd, intVersion.ToString());
                    //    DataTable dtTo = clsEquipPMD.mfReadEquipPMD(strGetStd, strGetVerNum.ToString());

                    //    DataTable dtRtn = clsEquipPMH.mfSaveMASEquipPMGRWApproval(dtEquipPMH, dtFrom, dtTo, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                    //    //그룹웨어 테스트용 메소드
                    //    //DataTable dtRtn = mfSaveMASEquipPMGRWApproval(dtEquipPMH, dtFrom, dtTo, frm.dtSendLine, frm.dtCcLine, frm.dtFormInfo, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));
                    
                    //    if (dtRtn == null)//실패
                    //    {
                    //        this.MdiParent.Cursor = Cursors.Default;
                    //        m_ProgressPopup.mfCloseProgressPopup(this);
                    //        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                        , "M001135", "M001037", "M000328", Infragistics.Win.HAlign.Center);

                    //        return;
                    //    }
                    //    else if (dtRtn.Rows.Count.Equals(0))//실패
                    //    {
                    //        this.MdiParent.Cursor = Cursors.Default;
                    //        m_ProgressPopup.mfCloseProgressPopup(this);
                    //        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                        , "M001135", "M001037", "M000328", Infragistics.Win.HAlign.Center);

                    //        return;
                    //    }
                    //    else if (!dtRtn.Rows[0]["CD_CODE"].Equals("00") || !Convert.ToBoolean(dtRtn.Rows[0]["CD_STATUS"].ToString())) //실패
                    //    {
                    //        this.MdiParent.Cursor = Cursors.Default;
                    //        m_ProgressPopup.mfCloseProgressPopup(this);
                    //        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                    //                        , "M001135", "M001037", dtRtn.Rows[0]["MSG"].ToString(), Infragistics.Win.HAlign.Center);

                    //        return;
                    //    }
                    //}
                    #endregion

                    result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                  Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                 "M001135", "M001037", "M000930",
                                                Infragistics.Win.HAlign.Right);

                    mfSearch();

                    if (strEdit == "T")
                        strEdit = "F";
                }
                else
                {
                    string strMessage = "";
                    if (ErrRtn.ErrMessage.Equals(string.Empty))
                        strMessage = ErrRtn.ErrMessage;
                    else
                        strMessage = "M000953";

                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500
                                                 , Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                 , msg.GetMessge_Text("M001135",m_resSys.GetString("SYS_LANG"))
                                                 , msg.GetMessge_Text("M001037",m_resSys.GetString("SYS_LANG")), strMessage
                                                 , Infragistics.Win.HAlign.Right);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 그룹웨어 테스트용 사용금지
        //////public DataTable mfSaveMASEquipPMGRWApproval(DataTable dt, DataTable dtFromItem, DataTable dtToItem, DataTable dtSendLine, DataTable dtCcLine, DataTable dtFormInfo, string strUserIP, string strUserID)
        //////{
        //////    //QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
        //////    DataTable dtRtn = new DataTable();

        //////    try
        //////    {
               
        //////        QRPBrowser brwChannel = new QRPBrowser();
        //////        //HeaderBL
        //////        brwChannel.mfRegisterChannel(typeof(QRPGRW.IF.QRPGRW), "QRPGRW");
        //////        QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
        //////        brwChannel.mfCredentials(grw);

        //////        string strPlantCode = dt.Rows[0]["PlantCode"].ToString();
        //////        string strStdNumber = dt.Rows[0]["StdNumber"].ToString();
        //////        string strVersionNum = dt.Rows[0]["VersionNum"].ToString();
        //////        string strLegacyKey = strPlantCode + "||" + strStdNumber + "||" + strVersionNum;

        //////        DataTable dtfile = new DataTable();
        //////        dtfile.Columns.Add("ImageFile", typeof(string));

        //////        DataTable dtFileInfo = grw.mfSetFileInfoDataTable();
        //////        string strFilePach = "MASEquipPMD\\";
        //////        foreach (DataRow dr in dtfile.Rows)
        //////        {
        //////            DataRow _dr = dtFileInfo.NewRow();
        //////            _dr["PlantCode"] = strPlantCode;
        //////            _dr["NM_FILE"] = dr["ImageFile"].ToString();
        //////            _dr["NM_LOCATION"] = strFilePach;

        //////            dtFileInfo.Rows.Add(_dr);
        //////        }

        //////        string strPlantName = dt.Rows[0]["PlantName"].ToString();
        //////        string strStationName = dt.Rows[0]["StationName"].ToString();
        //////        string strEquipGroupName = dt.Rows[0]["EquipGroupName"].ToString();
        //////        string strRevisionReason = dt.Rows[0]["RevisionReason"].ToString();
        //////        string strEquipLocName = dt.Rows[0]["EquipLocName"].ToString();
        //////        string strEquipLTypeName = dt.Rows[0]["EquipLargeTypeName"].ToString();
        //////        string strEquipMTypeName = dt.Rows[0]["EquipMiddleTypeName"].ToString();

        //////        //dtRtn = grw.EquipPM(strLegacyKey, dtSendLine, dtCcLine, strPlantCode, strStationName, strEquipLocName, strEquipLTypeName, strEquipMTypeName, strEquipGroupName
        //////        //                , strRevisionReason, dtFromItem, dtToItem, dtFormInfo, dtFileInfo, strUserIP, strUserID);
        //////        dtRtn = EquipPM(strLegacyKey, dtSendLine, dtCcLine, strPlantName, strStationName, strEquipLocName, strEquipLTypeName, strEquipMTypeName, strEquipGroupName
        //////                        , strRevisionReason, dtFromItem, dtToItem, dtFormInfo, dtFileInfo, strUserIP, strUserID);

        //////    }
        //////    catch (Exception ex)
        //////    {
        //////        return dtRtn;
        //////        throw (ex);
        //////    }
        //////    return dtRtn;
        //////}
        //////public DataTable EquipPM(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine,
        //////    string strPlantName, string strStationName, string strEquipLocName, string strEquipLTypeName, string strEquipMTypeName,
        //////    string strEquipGroupName, string strRevisionReason, DataTable dtFromItem, DataTable dtToItem, DataTable dtFormInfo, DataTable dtFileInfo, string strUserIP, string strUserID)
        //////{
        //////    QRPBrowser brwChannel = new QRPBrowser();
        //////    brwChannel.mfRegisterChannel(typeof(QRPGRW.IF.QRPGRW), "QRPGRW");
        //////    QRPGRW.IF.QRPGRW grw = new QRPGRW.IF.QRPGRW();
        //////    brwChannel.mfCredentials(grw);

        //////    string strContents = ChangContentString_EquipPM(strPlantName, strStationName, strEquipLocName, strEquipLTypeName, strEquipMTypeName
        //////        , strEquipGroupName, strRevisionReason, dtFromItem, dtToItem);
        //////    string m_strFmpf_EquipPM = "";
        //////    //첨부파일 데이터테이블 
        //////    DataTable dtRtn = SetDraftForm(strLegacyKey, dtSendLine, dtCcLine, "WF_STS_EQPCHECK_INFO", strContents, string.Empty, dtFormInfo, dtFileInfo, m_strFmpf_EquipPM, strUserIP, strUserID);
        //////    return dtFileInfo;
        //////}
        //////private string ChangContentString_EquipPM(string strPlantName, string strStationName, string strEquipLocName, string strEquipLTypeName, string strEquipMTypeName, string strEquipGroupName, string strRevisionReason, DataTable dtFromItem, DataTable dtToItem)
        //////{
        //////    string strReturnString = string.Empty;

        //////    string strHeader = string.Empty;
        //////    string strReason = string.Empty;
        //////    string strFrom = string.Empty;
        //////    string strFromItem = string.Empty;
        //////    string strTo = string.Empty;
        //////    string strToItem = string.Empty;

        //////    //전체 문서 리턴
        //////    strReturnString = strHeader + strRevisionReason + strFrom + strFromItem + "<p>" + strTo + strToItem;
        //////    return strReturnString;
        //////}
        //////public DataTable SetDraftForm(string strLegacyKey, DataTable dtSendLine, DataTable dtCcLine, string strfmpfKey, string strContens, string strempty, DataTable dtForminfo, DataTable dtFileinfo, string strfmpf, string strUserIP, string strUserID)
        //////{
        //////    return dtSendLine;
        //////}
        #endregion

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);
                //데이터SET
                DataTable dtEquipPMH = clsEquipPMH.mfSetEquipPMHData();

                //표준번호,개정번호 저장
                string strStdNumber = uTextStandardNo.Text; 
                string strVersionNo = uTextVersionNo.Text;


                if (this.uTextAcceptState.Tag == null || this.uTextAcceptState.Tag.ToString() == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M000882", "M000643", Infragistics.Win.HAlign.Right);
                    return;
                }
                
                //승인상태
                string strState = this.uTextAcceptState.Tag.ToString();

                if (strState == "FN")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001075", "M000767", Infragistics.Win.HAlign.Right);
                    return;
                }
                if (strState == "AR")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001075", "M000768", Infragistics.Win.HAlign.Right);
                    return;
                }

                #region 필수입력사항
                //--------------------------------------필수입력사항체크-------------------------------------------//
                if (this.uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000278", Infragistics.Win.HAlign.Right);
                    //Foucs 
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                if (this.uComboSearchEquipGroup.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000696", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uComboSearchEquipGroup.DropDown();
                    return;
                }

                if (this.uComboSearchStation.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                "M001264", "M001230", "M000128", Infragistics.Win.HAlign.Right);
                    //Foucs
                    this.uComboSearchStation.DropDown();
                    return;
                }

                if (this.uComboSearchEquipLoc.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001230", "M000831", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipLoc.DropDown();
                    return;
                }
                if (this.uComboSearchEquipType.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001230", "M000706", Infragistics.Win.HAlign.Right);
                    this.uComboSearchEquipType.DropDown();
                    return;
                }
                if (this.uComboProcessGroup.Value.ToString().Equals(string.Empty))
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                 Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                "M001264", "M001230", "M000291", Infragistics.Win.HAlign.Right);
                    this.uComboProcessGroup.DropDown();
                    return;
                }
                #endregion
                
                DataRow drPMH;
                drPMH = dtEquipPMH.NewRow();
                drPMH["StdNumber"] = strStdNumber;
                drPMH["VersionNum"] = strVersionNo;
                drPMH["PlantCode"] = uComboSearchPlant.Value.ToString();
                drPMH["EquipGroupCode"] = uComboSearchEquipGroup.Value.ToString();
                drPMH["EquipLocCode"] = this.uComboSearchEquipLoc.Value.ToString();
                drPMH["EquipLargeTypeCode"] = this.uComboSearchEquipType.Value.ToString();
                drPMH["StationCode"] = this.uComboSearchStation.Value.ToString();
                drPMH["ProcessGroup"] = this.uComboProcessGroup.Value.ToString();
                dtEquipPMH.Rows.Add(drPMH);
                


                //-------------------------------------------------------------------------------------------------//
                
         
                if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M000650", "M000675",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                {
                    QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                    Thread t1 = m_ProgressPopup.mfStartThread();
                    m_ProgressPopup.mfOpenProgressPopup(this, "삭제중...");
                    this.MdiParent.Cursor = Cursors.WaitCursor;

                    //처리 로직//
                    string strRtn = clsEquipPMH.mfDeleteEquipPMH_H(dtEquipPMH);

                    //Decoding//
                    TransErrRtn ErrRtn = new TransErrRtn();
                    ErrRtn = ErrRtn.mfDecodingErrMessage(strRtn);
                    ///////////

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    System.Windows.Forms.DialogResult result;
                    if (ErrRtn.ErrNum == 0)
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000677",
                                                    Infragistics.Win.HAlign.Right);
                        
                        //그리드의 전체행을 선택하여 삭제한다.
                        if (this.uGridEquipPMD.Rows.Count > 0)
                        {
                            this.uGridEquipPMD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipPMD.Rows.All);
                            this.uGridEquipPMD.DeleteSelectedRows(false);
                        }
                        if (this.uGridEquipList.Rows.Count > 0)
                        {
                            this.uGridEquipList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipPMD.Rows.All);
                            this.uGridEquipList.DeleteSelectedRows(false);
                        }

                        InitValue();
                        //탭컨트롤 인덱스 기본값으로
                        if (this.uTab.SelectedTab.Index != 0)
                        {
                            this.uTab.Tabs[0].Selected = true;
                        }

                        this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                    }
                    else
                    {
                        result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500,
                                                      Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                     "M001135", "M000638", "M000676",
                                                     Infragistics.Win.HAlign.Right);
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 신규
        /// </summary>
        public void mfCreate()
        {
            try
            {
                //System ResourceInfor
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (this.uGridEquipList.Rows.Count > 0)
                {
                    this.uGridEquipList.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipList.Rows.All);
                    this.uGridEquipList.DeleteSelectedRows(false);
                }

                if (this.uGridEquipPMD.Rows.Count > 0)
                {
                    this.uGridEquipPMD.Selected.Rows.AddRange((Infragistics.Win.UltraWinGrid.UltraGridRow[])this.uGridEquipPMD.Rows.All);
                    this.uGridEquipPMD.DeleteSelectedRows(false);
                }

                
                InitValue();
                //탭컨트롤 인덱스 기본값으로
                if (this.uTab.SelectedTab.Index != 0)
                {
                    this.uTab.Tabs[0].Selected = true;
                }
                this.uComboSearchPlant.Value = m_resSys.GetString("SYS_PLANTCODE");
                
                ControlReadOnly("a");

                //수정여부
                if (strEdit == "T")
                { strEdit = "F"; }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        public void mfPrint()
        {
        }

        /// <summary>
        /// 엑셀출력
        /// </summary>
        public void mfExcel()
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();

                if (this.uGridEquipList.Rows.Count == 0 && this.uGridEquipPMD.Rows.Count == 0 )
                {
                    msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                        , "M001264", "M000811", "M000710", Infragistics.Win.HAlign.Right);
                    return;
                }

                ////처리 로직//
                WinGrid grd = new WinGrid();
                if(this.uGridEquipPMD.Rows.Count != 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquipPMD);

                if (this.uGridEquipList.Rows.Count != 0)
                    grd.mfDownLoadGridToExcel(this.uGridEquipList);
                ///////////////


            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 이벤트

        private void frmMAS0029_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.Width > 1070)
                {
                    uGroupBox.Width = this.Width - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth;
                }
                else
                {
                    uGroupBox.Anchor = AnchorStyles.Top | AnchorStyles.Left;
                }

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 날짜 입력시 값변환
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uDateCreateDate_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //수정여부
                if (strEdit != "T")
                { strEdit = "T"; }
            }
            catch (Exception ex) { }
            finally { }
        }
        /// <summary>
        /// 셀업데이트 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGrid1_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // Cell 수정시 RowSelector 이미지 변경

                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                
                
                // 빈줄이면 자동삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridEquipPMD, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// RowSelector란에 편집이미지 를 나타나게 한다
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridEquipPMD_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                QRPGlobal grdImg = new QRPGlobal();
                e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;

                if (Convert.ToInt32(e.Cell.Row.Cells["Seq"].Value) != e.Cell.Row.RowSelectorNumber)
                {
                    e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
                }
                //수정여부
                if (strEdit != "T")
                { strEdit = "T"; }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 그리드안에 있는 첨부파일 버튼을 클릭 할경우 파일 경로가나옴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uGridEquipPMD_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog openFile = new OpenFileDialog();
                openFile.Filter = "Image files (*.bmp, *.jpg, *.gif)|*.bmp;*.jpg;*gif"; //"Image files(*.jpg,*.jpeg,*jpe,*.jfif,*.bmp,*.dib,*.gif,*.tif,*.tiff,*.png)|*.jpg,*.jpeg,*jpe,*.jfif,*.bmp,*.dib,*.gif,*.tif,*.tiff,*.png";
                openFile.FilterIndex = 1;
                openFile.RestoreDirectory = true;

                if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string strImageFile = openFile.FileName;
                    e.Cell.Value = strImageFile;

                    QRPGlobal grdImg = new QRPGlobal();
                    e.Cell.Row.RowSelectorAppearance.Image = grdImg.ModifyCellImage;
                    e.Cell.Row.Cells["Seq"].Value = e.Cell.Row.RowSelectorNumber;
                }
            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 텍스트이벤트

        /// <summary>
        /// v생성자 에디트버튼 클릭 시 자동 입력
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextCreateUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //수정여부
                if (strEdit != "T")
                { strEdit = "T"; }

                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }
                QRPMAS.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장보냄
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                this.uTextCreateUserID.Text = frmUser.UserID;
                this.uTextCreateUserName.Text = frmUser.UserName;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 승인자 에디트버튼 클릭 시 자동 입력
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextAcceptUserID_EditorButtonClick(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            try
            {
                //수정여부
                if (strEdit != "T")
                { strEdit = "T"; }

                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strPlant = this.uComboSearchPlant.Value.ToString();
                if (strPlant == "")
                {
                    msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                               "M001264", "M000882", "M000266", Infragistics.Win.HAlign.Right);
                    this.uComboSearchPlant.DropDown();
                    return;
                }

                QRPMAS.UI.frmPOP0011 frmUser = new frmPOP0011();
                //공장정보보냄
                frmUser.PlantCode = strPlant;
                frmUser.ShowDialog();

                Infragistics.Win.UltraWinEditors.UltraTextEditor utext = (Infragistics.Win.UltraWinEditors.UltraTextEditor)sender;

                if (utext.Name.Equals("uTextAcceptUserID"))
                {
                    this.uTextAcceptUserID.Text = frmUser.UserID;
                    this.uTextAcceptUserName.Text = frmUser.UserName;
                }
                else
                {
                    this.uTextSecondID.Text = frmUser.UserID;
                    this.uTextSecondName.Text = frmUser.UserName;
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 생성자 텍스트박스 입력후 엔터 누를 시 자동 이름입력
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextCreateUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //수정여부
                if (strEdit != "T")
                { strEdit = "T"; }

                if (e.KeyData == Keys.Enter)
                {
                    //생성자 공장코드 저장
                    string strCreateID = this.uTextCreateUserID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                    //공백 확인
                    if (strCreateID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        this.uTextCreateUserID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000280", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    UserSearch(strCreateID, strPlantCode, "Ct");
                    
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        private void uTextCreateUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextCreateUserName.Text.Equals(string.Empty))
                this.uTextCreateUserName.Clear();
        }

        /// <summary>
        /// 승인자자 텍스트박스 입력후 엔터 누를 시 자동 이름입력
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextAcceptUserID_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //수정여부
                if (strEdit != "T")
                { strEdit = "T"; }

                if (e.KeyData == Keys.Enter)
                {
                    Infragistics.Win.UltraWinEditors.UltraTextEditor utext = (Infragistics.Win.UltraWinEditors.UltraTextEditor)sender;

                    //승인자,공장코드 저장
                    string strAcceptID = utext.Name == "uTextAcceptUserID" ? this.uTextAcceptUserID.Text : this.uTextSecondID.Text;
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    WinMessageBox msg = new WinMessageBox();

                    //System ResourceInfo
                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                    

                    //공백 확인
                    if (strAcceptID == "")
                    {

                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000056", "M000054", Infragistics.Win.HAlign.Right);

                        //Focus
                        if (utext.Name.Equals("uTextAcceptUserID"))
                            this.uTextAcceptUserID.Focus();
                        else
                            this.uTextSecondID.Focus();
                        return;
                    }
                    else if (strPlantCode == "")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                        "M000240", "M000280", "M000266", Infragistics.Win.HAlign.Right);

                        //DropDown
                        this.uComboSearchPlant.DropDown();
                        return;
                    }

                    if (utext.Name.Equals("uTextAcceptUserID"))
                        UserSearch(strAcceptID, strPlantCode, "At");
                    else
                        UserSearch(strAcceptID, strPlantCode, "Ats");
                    
                    
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 승인자 ID 변경시 명 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextAcceptUserID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextAcceptUserName.Text.Equals(string.Empty))
                this.uTextAcceptUserName.Clear();
        }
        /// <summary>
        /// 승인자 ID 변경시 명 초기화
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextSecondID_ValueChanged(object sender, EventArgs e)
        {
            if (!this.uTextSecondName.Text.Equals(string.Empty))
                this.uTextSecondName.Clear();
        }

        /// <summary>
        /// 비고 수정시 값 변환
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextEtc_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //수정여부
                if (strEdit != "T")
                { strEdit = "T"; }
            }
            catch (Exception ex) { }
            finally { }
        }

        /// <summary>
        /// 개정사유 수정시 값 변환
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uTextRevisionReason_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //수정여부
                if (strEdit != "T")
                { strEdit = "T"; }
            }
            catch (Exception ex) { }
            finally { }
        }



        #endregion

        #region 콤보이벤트

        //공장선택에따라 콤보바뀜
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //콤보박스 클리어
                this.uComboSearchStation.Items.Clear();

                //공장코드 저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();
               
                /////////////////////////////////////////////////////////////

                //Station정보 BL호출
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Station), "Station");
                QRPMAS.BL.MASEQU.Station clsStation = new QRPMAS.BL.MASEQU.Station();
                brwChannel.mfCredentials(clsStation);

                //Station콤보조회 BL호출
                DataTable dtStation = clsStation.mfReadStationCombo(strPlantCode, strLang);

                wCombo.mfSetComboEditor(this.uComboSearchStation, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                    , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                    , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "StationCode", "StationName", dtStation);

                ////////////////////////////////////////////////////////////

                mfSearchCombo(this.uComboSearchPlant.Value.ToString(),"","","","",3);
                InitSearch();
                
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //공장콤보클릭 전 저장여부 확인
        private void uComboSearchPlant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (strEdit == "T")
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001075", "M000757",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        mfSave();
                        strEdit = "F";

                    }
                    else
                    {
                        strEdit = "F";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //설비그룹콤보클릭 전 저장여부 확인
        private void uComboSearchEquipGroup_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                if (strEdit == "T")
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, 500, 500,
                                        Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                        "M001264", "M001075", "M000757",
                                        Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {

                        mfSave();
                        strEdit = "F";

                    }
                    else
                    {
                        strEdit = "F";
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        private void frmMAS0029_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }

        //설비그룹선택 시 자동 조회
        private void uComboSearchEquipGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                string strEuqipGroupCode = this.uComboSearchEquipGroup.Value.ToString();
                if (strEuqipGroupCode == "")
                    return;

                string strEquipGroup = this.uComboSearchEquipGroup.Text;
                string strOk = "";
                for (int i = 0; i < this.uComboSearchEquipGroup.Items.Count; i++)
                {
                    if (strEuqipGroupCode.Equals(this.uComboSearchEquipGroup.Items[i].DataValue.ToString()) && strEquipGroup.Equals(this.uComboSearchEquipGroup.Items[i].DisplayText))
                    {
                        strOk = "OK";
                        break;
                    }
                }

                if (!strOk.Equals(string.Empty))
                {

                    ReadEquipPMH();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //설비위치 변경시 자동조회
        private void uComboSearchEquipLoc_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboSearchEquipLoc.Value.ToString();
                //위치정보가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboSearchEquipLoc.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboSearchEquipLoc.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboSearchEquipLoc.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipLoc.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), "", "", 3);

                        ReadEquipPMH();
                    }
                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //설비중분류 변경시 자동조회
        private void uComboSearchEquipType_ValueChanged(object sender, EventArgs e)
        {
            try
            {
               string strCode = this.uComboSearchEquipType.Value.ToString();
                //코드가 공백이아닐 경우 검색함
               if (!strCode.Equals(string.Empty))
               {
                   string strText = this.uComboSearchEquipType.Text.ToString();
                   string strChk = "";
                   for (int i = 0; i < this.uComboSearchEquipType.Items.Count; i++)
                   {
                       if (strCode.Equals(this.uComboSearchEquipType.Items[i].DataValue.ToString()) && strText.Equals(this.uComboSearchEquipType.Items[i].DisplayText))
                       {
                           strChk = "OK";
                           break;
                       }
                   }

                   if (!strChk.Equals(string.Empty))
                   {
                       mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), this.uComboSearchEquipType.Value.ToString(), 1);
                       ReadEquipPMH();
                   }
               }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// Station콤보 선택시 자동조회
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uComboSearchStation_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strStation = this.uComboSearchStation.Text.ToString();
                string strChk = "";
                for (int i = 0; i < this.uComboSearchStation.Items.Count; i++)
                {
                    if (strStationCode.Equals(this.uComboSearchStation.Items[i].DataValue.ToString()) && strStation.Equals(this.uComboSearchStation.Items[i].DisplayText))
                    {
                        strChk = "OK";
                        break;
                    }
                }

                if (!strChk.Equals(string.Empty))
                {
                    //검색조건저장
                    string strPlantCode = this.uComboSearchPlant.Value.ToString();

                    ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);


                    //설비위치정보 BL호출
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipLocation), "EquipLocation");
                    QRPMAS.BL.MASEQU.EquipLocation clsEquipLocation = new QRPMAS.BL.MASEQU.EquipLocation();
                    brwChannel.mfCredentials(clsEquipLocation);

                    //설비위치정보콤보조회 매서드 실행
                    DataTable dtLoc = clsEquipLocation.mfReadLocation_Combo(strPlantCode, strStationCode, m_resSys.GetString("SYS_LANG"));

                    WinComboEditor wCombo = new WinComboEditor();

                    //설비위치정보콤보 이전 정보 클리어
                    this.uComboSearchEquipLoc.Items.Clear();

                    wCombo.mfSetComboEditor(this.uComboSearchEquipLoc, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", "선택", "EquipLocCode", "EquipLocName", dtLoc);

                    mfSearchCombo(strPlantCode, strStationCode, "", "", "", 3);

                    //Station이 공백이 아닐 시 자동조회
                    if (this.uComboProcessGroup.Items.Count > 0 && !this.uComboSearchStation.Value.Equals(string.Empty))
                        ReadEquipPMH();
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        //설비대분류콤보 선택시 자동조회
        private void uComboProcessGroup_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string strCode = this.uComboProcessGroup.Value.ToString();
                //코드가 공백이아닌 경우 검색
                if (!strCode.Equals(string.Empty))
                {
                    string strText = this.uComboProcessGroup.Text.ToString();
                    string strChk = "";
                    for (int i = 0; i < this.uComboProcessGroup.Items.Count; i++)
                    {
                        if (strCode.Equals(this.uComboProcessGroup.Items[i].DataValue.ToString()) && strText.Equals(this.uComboProcessGroup.Items[i].DisplayText))
                        {
                            strChk = "OK";
                            break;
                        }
                    }

                    if (!strChk.Equals(string.Empty))
                    {
                        mfSearchCombo(this.uComboSearchPlant.Value.ToString(), this.uComboSearchStation.Value.ToString(), this.uComboSearchEquipLoc.Value.ToString(), this.uComboProcessGroup.Value.ToString(), "", 2);
                        ReadEquipPMH();
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        #region 버튼이벤트

        // 행삭제 버튼 이벤트
        private void uButtonDeleteRow1_Click(object sender, EventArgs e)
        {
            try
            {
                if (strEdit != "T")
                { strEdit = "T"; }

                
                for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["Check"].Value) == true)
                    {
                        //행 삭제
                        this.uGridEquipPMD.Rows[i].Hidden = true;
                    }
                    
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 복사 버튼 클릭시, 팝업 호출
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonCopy_Click(object sender, EventArgs e)
        {
            DialogResult result = new DialogResult();
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                if (uComboSearchPlant.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001115", "M000266", Infragistics.Win.HAlign.Right);

                    this.uComboSearchPlant.DropDown();
                    return;
                }
                else if (uComboSearchStation.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001115", "M000128", Infragistics.Win.HAlign.Right);

                    this.uComboSearchStation.DropDown();
                    return;
                }
                else if (uComboSearchEquipLoc.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001115", "M000831", Infragistics.Win.HAlign.Right);

                    this.uComboSearchEquipLoc.DropDown();
                    return;
                }
                else if (uComboProcessGroup.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001115", "M000698", Infragistics.Win.HAlign.Right);

                    this.uComboProcessGroup.DropDown();
                    return;
                }
                else if (uComboSearchEquipType.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                   "M001135", "M001115", "M000720", Infragistics.Win.HAlign.Right);

                    this.uComboSearchEquipType.DropDown();
                    return;
                }
                else if (uComboSearchEquipGroup.Value.ToString().Equals(string.Empty))
                {
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                       "M001135", "M001115", "M000695", Infragistics.Win.HAlign.Right);

                    this.uComboSearchEquipGroup.DropDown();
                    return;
                }
                else
                {
                    frmPOP0021 frmCopy = new frmPOP0021();
                    frmCopy.ShowDialog();

                    //DataTable이 비어있으면 바인딩하지 않는다.
                    if (frmCopy.dtRtnEquip.Rows.Count > 0)
                    {
                        DataTable dtSource = frmCopy.dtRtnEquip;

                        ////기존정보가 있는 경우 데이터 테이블에 추가시킨다.
                        //if (this.uGridEquipPMD.Rows.Count > 0)
                        //{
                        //    for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                        //    {
                        //        if (this.uGridEquipPMD.Rows[i].Hidden.Equals(false))
                        //        {
                        //            DataRow drSource = dtSource.NewRow();

                        //            drSource["Seq"] = this.uGridEquipPMD.Rows[i].Cells["Seq"].Value;
                        //            drSource["PMInspectName"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectName"].Value;
                        //            drSource["PMInspectRegion"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectRegion"].Value;
                        //            drSource["PMInspectCriteria"] = this.uGridEquipPMD.Rows[i].Cells["PMInspectCriteria"].Value;
                        //            drSource["PMPeriodCode"] = this.uGridEquipPMD.Rows[i].Cells["PMPeriodCode"].Value;
                        //            drSource["PMMethod"] = this.uGridEquipPMD.Rows[i].Cells["PMMethod"].Value;
                        //            drSource["FaultFixMethod"] = this.uGridEquipPMD.Rows[i].Cells["FaultFixMethod"].Value;
                        //            drSource["ImageFile"] = this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value;
                        //            drSource["UnitDesc"] = this.uGridEquipPMD.Rows[i].Cells["UnitDesc"].Value;
                        //            drSource["StandardManCount"] = this.uGridEquipPMD.Rows[i].Cells["StandardManCount"].Value;
                        //            drSource["StandardTime"] = this.uGridEquipPMD.Rows[i].Cells["StandardTime"].Value;
                        //            drSource["LevelCode"] = this.uGridEquipPMD.Rows[i].Cells["LevelCode"].Value;
                        //            drSource["MeasureValueFlag"] = this.uGridEquipPMD.Rows[i].Cells["MeasureValueFlag"].Value;

                        //            dtSource.Rows.Add(drSource);
                        //        }
                        //    }
                        //}

                        WinGrid grd = new WinGrid();
                        this.uGridEquipPMD.DataSource = dtSource;
                        this.uGridEquipPMD.DataBind();
                        grd.mfSetAutoResizeColWidth(this.uGridEquipPMD, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 첨부파일을 다운로드한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonFileDown_Click(object sender, EventArgs e)
        {
            WinMessageBox msg = new WinMessageBox();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            try
            {
                int intFileNum = 0;
                for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                {
                    //삭제가 안된 행에서 경로가 없는 행이 있는지 체크
                    if (this.uGridEquipPMD.Rows[i].Hidden == false && this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["Check"].Value) == true)
                        intFileNum++;
                }
                if (intFileNum == 0)
                {
                    DialogResult result = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                              Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001135", "M001135", "M000359",
                                             Infragistics.Win.HAlign.Right);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog saveFolder = new FolderBrowserDialog();
                saveFolder.RootFolder = Environment.SpecialFolder.Desktop;  //검색을 시작할 루트폴더 지정
                saveFolder.SelectedPath = Environment.CurrentDirectory;     //
                saveFolder.ShowNewFolderButton = true;                      //새폴더생성 버튼 보여주게 처리
                saveFolder.Description = "Download Folder";

                string strPlantCode = this.uComboSearchPlant.Value.ToString();

                if (saveFolder.ShowDialog() == DialogResult.OK)
                {
                    string strSaveFolder = saveFolder.SelectedPath + "\\";

                    //화일서버 연결정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemAccessInfo), "SystemAccessInfo");
                    QRPSYS.BL.SYSPGM.SystemAccessInfo clsSysAccess = new QRPSYS.BL.SYSPGM.SystemAccessInfo();
                    brwChannel.mfCredentials(clsSysAccess);
                    DataTable dtSysAccess = clsSysAccess.mfReadSystemAccessInfoDetail(strPlantCode, "S02");

                    //설비점검정보 첨부파일 저장경로정보 가져오기
                    brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.SystemFilePath), "SystemFilePath");
                    QRPSYS.BL.SYSPGM.SystemFilePath clsSysFilePath = new QRPSYS.BL.SYSPGM.SystemFilePath();
                    brwChannel.mfCredentials(clsSysFilePath);
                    DataTable dtFilePath = clsSysFilePath.mfReadSystemFilePathDetail(strPlantCode, "D0023");

                    
                    frmCOMFileAttach fileAtt = new frmCOMFileAttach();
                    ArrayList arrFile = new ArrayList();

                    for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                    {
                        if (this.uGridEquipPMD.Rows[i].Hidden == false && this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString().Contains(":\\") == false && Convert.ToBoolean(this.uGridEquipPMD.Rows[i].Cells["Check"].Value) == true)
                            arrFile.Add(this.uGridEquipPMD.Rows[i].Cells["ImageFile"].Value.ToString());
                    }

                    //Upload정보 설정
                    fileAtt.mfInitSetSystemFileInfo(arrFile, strSaveFolder, dtSysAccess.Rows[0]["SystemAddressPath"].ToString(),
                                                                           dtFilePath.Rows[0]["ServerPath"].ToString(),
                                                                           dtFilePath.Rows[0]["FolderName"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessID"].ToString(),
                                                                           dtSysAccess.Rows[0]["AccessPassword"].ToString());
                    fileAtt.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #endregion

        #region 조회용매서드

        /// <summary>
        /// 콤보용 조회매서드
        /// </summary>
        private void ReadEquipPMH()
        {
            try
            {
               
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinMessageBox msg = new WinMessageBox();

                InitSearch();

                //검색조건 정보저장
                string strPlantCode = this.uComboSearchPlant.Value.ToString();
                string strEquipGroupCode = this.uComboSearchEquipGroup.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                string strEquipStationCode = this.uComboSearchStation.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Items.Count == 0 ? "" : this.uComboProcessGroup.Value.ToString();
                string strAdmitStatus = "";

                //위의 정보중 하나라도 공백이있는 경우 검색 안함
                if (strPlantCode.Equals(string.Empty) 
                    || strEquipGroupCode.Equals(string.Empty) 
                    || strEquipStationCode.Equals(string.Empty)
                    || strEquipLocCode.Equals(string.Empty)
                    || strEquipTypeCode.Equals(string.Empty)
                    || strProcessGroup.Equals(string.Empty))
                {
                    return;
                }

                //Popup창 실행
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                //커서변경
                this.MdiParent.Cursor = Cursors.WaitCursor;


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMH), "EquipPMH");
                QRPMAS.BL.MASEQU.EquipPMH clsEquipPMH = new QRPMAS.BL.MASEQU.EquipPMH();
                brwChannel.mfCredentials(clsEquipPMH);

                DataTable dtEquipPMH = clsEquipPMH.mfReadEquipPMH_H(strPlantCode, strEquipGroupCode,strEquipStationCode,strEquipLocCode,strEquipTypeCode, strProcessGroup,m_resSys.GetString("SYS_LANG"));

                if (dtEquipPMH.Rows.Count <= 0)
                {
                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);
                    return;
                }

                strAdmitStatus = dtEquipPMH.Rows[0]["AdmitStatusCode"].ToString();

                if (dtEquipPMH.Rows.Count != 0)
                {
                    //-----데이터바인드-----//
                    this.uTextStandardNo.Text = dtEquipPMH.Rows[0]["StdNumber"].ToString();
                    this.uTextVersionNo.Text = dtEquipPMH.Rows[0]["VersionNum"].ToString();
                    this.uTextAcceptState.Text = dtEquipPMH.Rows[0]["AdmitStatusName"].ToString();
                    this.uTextAcceptState.Tag = dtEquipPMH.Rows[0]["AdmitStatusCode"].ToString();
                    this.uTextAcceptUserID.Text = dtEquipPMH.Rows[0]["AdmitID"].ToString();
                    this.uTextCreateUserID.Text = dtEquipPMH.Rows[0]["WriteID"].ToString();
                    this.uTextEtc.Text = dtEquipPMH.Rows[0]["EtcDesc"].ToString();
                    this.uTextRejectReason.Text = dtEquipPMH.Rows[0]["RejectReason"].ToString();
                    this.uTextRevisionReason.Text = dtEquipPMH.Rows[0]["RevisionReason"].ToString();
                    this.uDateCreateDate.Value = dtEquipPMH.Rows[0]["WriteDate"].ToString();
                    this.uTextCreateUserName.Text = dtEquipPMH.Rows[0]["WriteName"].ToString();
                    this.uTextAcceptUserName.Text = dtEquipPMH.Rows[0]["AdmitName"].ToString();

                    this.uTextSecondID.Text = dtEquipPMH.Rows[0]["AdmitSecondID"].ToString();
                    this.uTextSecondName.Text = dtEquipPMH.Rows[0]["AdmitSecondName"].ToString();

                    /////--------------//////
                    string strStdNumber = dtEquipPMH.Rows[0]["StdNumber"].ToString();
                    string strVersionNum = dtEquipPMH.Rows[0]["VersionNum"].ToString();

                    EquipSearch(strStdNumber, strVersionNum);

                    this.MdiParent.Cursor = Cursors.Default;
                    m_ProgressPopup.mfCloseProgressPopup(this);

                    if (strAdmitStatus == "RE")
                    {
                        msg.mfSetMessageBox(MessageBoxType.Information, 500, 500,
                                             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001264", "M001075", "M000413",
                                             Infragistics.Win.HAlign.Right);
                        ControlReadOnly("BB");

                    }
                    else if (strAdmitStatus == "AR")
                    {
                        ControlReadOnly("AR");

                        msg.mfSetMessageBox(MessageBoxType.Error, 500, 500,
                                             Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                             "M001264", "M001075", "M000760",
                                             Infragistics.Win.HAlign.Right);

                    }
                    else if (strAdmitStatus == "FN")
                    {
                        ControlReadOnly("FN");
                        this.uTextVersionNo.Text = (Convert.ToInt32(this.uTextVersionNo.Text) + 1).ToString();
                        //if (msg.mfSetMessageBox(MessageBoxType.YesNo, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                        //            Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                        //            "확인창", "정보확인", "승인 완료된 정보입니다. 개정 하시겠습니까?",
                        //            Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                        //{


                        //    ControlReadOnly("FN");
                        //    this.uTextVersionNo.Text = (Convert.ToInt32(this.uTextVersionNo.Text) + 1).ToString();
                        //}

                    }
                    else
                    {
                        ControlReadOnly("BB");
                    }
                }

                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비점검정보상세,설비리스트조회
        /// </summary>
        /// <param name="strStdNumber">표준번호</param>
        /// <param name="intVersionNum">개정번호</param>
        private void EquipSearch(string strStdNumber, string strVersionNum)
        {

            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //공장코드 와 설비그룹코드 저장
                string strPlantCode = uComboSearchPlant.Value.ToString();
                string strEuqipGroupCode = uComboSearchEquipGroup.Value.ToString();
                string strStationCode = this.uComboSearchStation.Value.ToString();
                string strEquipLocCode = this.uComboSearchEquipLoc.Value.ToString();
                string strProcessGroup = this.uComboProcessGroup.Value.ToString();
                string strEquipTypeCode = this.uComboSearchEquipType.Value.ToString();
                //------설비점검정보상세 조회--------//


                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipPMD), "EquipPMD");
                QRPMAS.BL.MASEQU.EquipPMD clsEquipPMD = new QRPMAS.BL.MASEQU.EquipPMD();
                brwChannel.mfCredentials(clsEquipPMD);

                DataTable dtEquipPMD = clsEquipPMD.mfReadEquipPMD(strStdNumber, strVersionNum);

                //데이터바인드
                this.uGridEquipPMD.DataSource = dtEquipPMD;
                this.uGridEquipPMD.DataBind();

                WinGrid grd = new WinGrid();
                grd.mfSetAutoResizeColWidth(this.uGridEquipPMD, 0);

                //-----------------------------------//
                //--------설비리스트조회---------//
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.EquipGroup), "EquipGroup");
                QRPMAS.BL.MASEQU.EquipGroup clsEquipGroup = new QRPMAS.BL.MASEQU.EquipGroup();
                brwChannel.mfCredentials(clsEquipGroup);

                DataTable dtEquip = clsEquipGroup.mfReadEquipGroupList_PM(strPlantCode,strStationCode,strEquipLocCode,strProcessGroup,strEquipTypeCode, strEuqipGroupCode,m_resSys.GetString("SYS_LANG"));

                //데이터바인드
                this.uGridEquipList.DataSource = dtEquip;
                this.uGridEquipList.DataBind();

                if(dtEquip.Rows.Count > 0)
                    grd.mfSetAutoResizeColWidth(this.uGridEquipList, 0);
                //------------------------------//

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 생성자,승인자 Enter 검색
        /// </summary>
        /// <param name="strUserID">입력된아이디</param>
        /// <param name="strPlantCode">공장코드</param>
        /// <param name="strGubun">구분자(At=승인자, Ats = 두번때 승인자 , 그밖은 생성자)</param>
        private void UserSearch(string strUserID, string strPlantCode,string strGubun)
        {
            try
            {
                WinMessageBox msg = new WinMessageBox();

                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //BL호출
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSUSR.User), "User");
                QRPSYS.BL.SYSUSR.User clsUser = new QRPSYS.BL.SYSUSR.User();
                brwChannel.mfCredentials(clsUser);

                //매서드호출
                DataTable dtCreat = clsUser.mfReadSYSUser(strPlantCode, strUserID, m_resSys.GetString("SYS_LANG"));

                //정보가 없을 시
                if (dtCreat.Rows.Count == 0)
                {
                    /* 검색결과 Record수 = 0이면 메시지 띄움 */
                    System.Windows.Forms.DialogResult result;
                    result = msg.mfSetMessageBox(MessageBoxType.Warning, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                    "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                    if (strGubun == "At")
                    {
                        if (!this.uTextAcceptUserName.Text.Equals(string.Empty))
                            this.uTextAcceptUserName.Clear();
                    }
                    else if(strGubun == "Ats")
                    {
                        if (!this.uTextSecondName.Text.Equals(string.Empty))
                            this.uTextSecondName.Clear();
                    }
                    else
                    {
                        if (!this.uTextCreateUserName.Text.Equals(string.Empty))
                            this.uTextCreateUserName.Clear();
                    }
                    return;
                }

                //정보가 확인 시 이름 추가
                string strUserName = dtCreat.Rows[0]["UserName"].ToString();

                if (strGubun == "At")
                    //정보가 확인 시 승인자 이름 추가
                    this.uTextAcceptUserName.Text = dtCreat.Rows[0]["UserName"].ToString();
                else if (strGubun == "Ats") //정보가 확인 시 승인자 이름 추가
                    this.uTextSecondName.Text = dtCreat.Rows[0]["UserName"].ToString();
                else    //정보가 확인 시 생성자 이름 추가
                    this.uTextCreateUserName.Text = dtCreat.Rows[0]["UserName"].ToString();

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 설비대분류,설비중분류, 설비소분류 콤보조회
        /// </summary>
        /// <param name="strPlantCode">공장</param>
        /// <param name="strStationCode">Station</param>
        /// <param name="strEquipLocCode">위치</paramm>
        /// <param name="strProcessGroup">설비대분류</param>
        /// <param name="strEquipLargeTypeCode">설비중분류</param>
        /// <param name="intCnt">조회단계</param>
        private void mfSearchCombo(string strPlantCode, string strStationCode, string strEquipLocCode, string strProcessGroup, string strEquipLargeTypeCode, int intCnt)
        {
            try
            {
                //System ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                string strLang = m_resSys.GetString("SYS_LANG");
                WinComboEditor wCombo = new WinComboEditor();
                string strDefault = "선택";

                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASEQU.Equip), "Equip");
                QRPMAS.BL.MASEQU.Equip clsEquip = new QRPMAS.BL.MASEQU.Equip();
                brwChannel.mfCredentials(clsEquip);

                if (intCnt.Equals(3))
                {
                    this.uComboProcessGroup.Items.Clear();
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();


                    //ProcessGroup(설비대분류)
                    DataTable dtProcGroup = clsEquip.mfReadEquip_ProcessCombo(strPlantCode, strStationCode, strEquipLocCode);


                    wCombo.mfSetComboEditor(this.uComboProcessGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "ProcessGroupCode", "ProcessGroupName", dtProcGroup);

                    ////////////////////////////////////////////////////////////

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(2))
                {
                    this.uComboSearchEquipType.Items.Clear();
                    this.uComboSearchEquipGroup.Items.Clear();

                    //설비중분류조회 매서드 실행
                    DataTable dtType = clsEquip.mfReadEquip_EquipLargeType(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipType, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                       , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                       , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipLargeTypeCode", "EquipLargeTypeName", dtType);

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);
                }
                if (intCnt.Equals(1))
                {
                    this.uComboSearchEquipGroup.Items.Clear();

                    /////////////----- 설비소분류 콤보박스 ----/////////////////

                    DataTable dtEquipGroup = clsEquip.mfReadEquip_EquipGroupCombo(strPlantCode, strStationCode, strEquipLocCode, strProcessGroup, strEquipLargeTypeCode, strLang);

                    wCombo.mfSetComboEditor(this.uComboSearchEquipGroup, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista
                        , m_resSys.GetString("SYS_FONTNAME"), true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default
                        , true, 100, Infragistics.Win.HAlign.Left, "", "", strDefault, "EquipGroupCode", "EquipGroupName", dtEquipGroup);

                }

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #endregion

        #region 컨트롤제어

        /// <summary>
        /// 컨트롤 수정불가 가능처리
        /// </summary>
        /// <param name="strAdmitStatus">승인상태</param>
        private void ControlReadOnly(string strAdmitStatus)
        {
            try
            {
                if (strAdmitStatus == "AR")
                {

                    this.uTextAcceptUserID.ReadOnly = true;
                    this.uTextCreateUserID.ReadOnly = true;
                    this.uTextSecondID.ReadOnly = true;
                    this.uTextEtc.ReadOnly = true;
                    this.uDateCreateDate.ReadOnly = true;
                    this.uTextRevisionReason.ReadOnly = true;
                    if (this.uGridEquipPMD.Rows.Count > 0)
                    {
                        for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                        {
                            this.uGridEquipPMD.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                        }
                        
                    }
                }
                else if (strAdmitStatus == "FN")
                {
                    this.uTextAcceptUserID.ReadOnly = false;
                    this.uTextCreateUserID.ReadOnly = false;
                    this.uTextSecondID.ReadOnly = false;
                    this.uTextEtc.ReadOnly = false;
                    this.uDateCreateDate.ReadOnly = false;

                    this.uTextRevisionReason.ReadOnly = false;
                    for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                    {
                        this.uGridEquipPMD.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    }
                }
                else
                {
                    this.uTextAcceptUserID.ReadOnly = false;
                    this.uTextCreateUserID.ReadOnly = false;
                    this.uTextSecondID.ReadOnly = false;
                    this.uTextEtc.ReadOnly = false;
                    this.uDateCreateDate.ReadOnly = false;
                    
                    this.uTextRevisionReason.ReadOnly = true;
                    for (int i = 0; i < this.uGridEquipPMD.Rows.Count; i++)
                    {
                        this.uGridEquipPMD.Rows[i].Activation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    }

                }

            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 조회나 검색완료후 콤보를제외한컨트롤 초기화
        /// </summary>
        private void InitSearch()
        {
            try
            {
                while (this.uGridEquipPMD.Rows.Count > 0)
                {
                    this.uGridEquipPMD.Rows[0].Delete(false);
                }
                while (this.uGridEquipList.Rows.Count > 0)
                {
                    this.uGridEquipList.Rows[0].Delete(false);
                }
                InitValue();
                //탭컨트롤 인덱스 기본값으로
                if (this.uTab.SelectedTab.Index != 0)
                {
                    this.uTab.Tabs[0].Selected = true;
                }
                ControlReadOnly("a");
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        #endregion

        



        



    }
}
