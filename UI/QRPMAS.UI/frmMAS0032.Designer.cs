﻿namespace QRPMAS.UI
{
    partial class frmMAS0032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMAS0032));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            this.uGroupBoxContentsArea = new Infragistics.Win.Misc.UltraExpandableGroupBox();
            this.ultraExpandableGroupBoxPanel1 = new Infragistics.Win.Misc.UltraExpandableGroupBoxPanel();
            this.uGridVendor = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupSearchARrea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextSearchVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextSearchVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelSearchVendorName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchVendorCode = new Infragistics.Win.Misc.UltraLabel();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uTextUseFlag = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelUseFlag = new Infragistics.Win.Misc.UltraLabel();
            this.uTextVendorCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextFax = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextBossName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorNameEn = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextTel = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorNameCh = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextAddress = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextRegNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextVendorName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uLabelFax = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelTel = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelAddress = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelBossName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelRegNo = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendorCode = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendorName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendorNameEn = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelVendorNameCh = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridVendorP = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDeleteRow = new Infragistics.Win.Misc.UltraButton();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).BeginInit();
            this.uGroupBoxContentsArea.SuspendLayout();
            this.ultraExpandableGroupBoxPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchARrea)).BeginInit();
            this.uGroupSearchARrea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBossName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorNameEn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorNameCh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).BeginInit();
            this.uGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridVendorP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uGroupBoxContentsArea
            // 
            this.uGroupBoxContentsArea.Controls.Add(this.ultraExpandableGroupBoxPanel1);
            this.uGroupBoxContentsArea.ExpandedSize = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.Location = new System.Drawing.Point(0, 135);
            this.uGroupBoxContentsArea.Name = "uGroupBoxContentsArea";
            this.uGroupBoxContentsArea.Size = new System.Drawing.Size(1070, 715);
            this.uGroupBoxContentsArea.TabIndex = 7;
            this.uGroupBoxContentsArea.ExpandedStateChanging += new System.ComponentModel.CancelEventHandler(this.uGroupBoxContentsArea_ExpandedStateChanging);
            // 
            // ultraExpandableGroupBoxPanel1
            // 
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.uGroupBox1);
            this.ultraExpandableGroupBoxPanel1.Controls.Add(this.ultraGroupBox2);
            this.ultraExpandableGroupBoxPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraExpandableGroupBoxPanel1.Location = new System.Drawing.Point(3, 17);
            this.ultraExpandableGroupBoxPanel1.Name = "ultraExpandableGroupBoxPanel1";
            this.ultraExpandableGroupBoxPanel1.Size = new System.Drawing.Size(1064, 695);
            this.ultraExpandableGroupBoxPanel1.TabIndex = 0;
            // 
            // uGridVendor
            // 
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridVendor.DisplayLayout.Appearance = appearance5;
            this.uGridVendor.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridVendor.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridVendor.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridVendor.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.uGridVendor.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridVendor.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.uGridVendor.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridVendor.DisplayLayout.MaxRowScrollRegions = 1;
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridVendor.DisplayLayout.Override.ActiveCellAppearance = appearance13;
            appearance8.BackColor = System.Drawing.SystemColors.Highlight;
            appearance8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridVendor.DisplayLayout.Override.ActiveRowAppearance = appearance8;
            this.uGridVendor.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridVendor.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.uGridVendor.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance6.BorderColor = System.Drawing.Color.Silver;
            appearance6.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridVendor.DisplayLayout.Override.CellAppearance = appearance6;
            this.uGridVendor.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridVendor.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridVendor.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance12.TextHAlignAsString = "Left";
            this.uGridVendor.DisplayLayout.Override.HeaderAppearance = appearance12;
            this.uGridVendor.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridVendor.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.uGridVendor.DisplayLayout.Override.RowAppearance = appearance11;
            this.uGridVendor.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance9.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridVendor.DisplayLayout.Override.TemplateAddRowAppearance = appearance9;
            this.uGridVendor.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridVendor.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridVendor.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridVendor.Location = new System.Drawing.Point(0, 80);
            this.uGridVendor.Name = "uGridVendor";
            this.uGridVendor.Size = new System.Drawing.Size(1070, 770);
            this.uGridVendor.TabIndex = 6;
            this.uGridVendor.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridVendor_DoubleClickRow);
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 4;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupSearchARrea
            // 
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupSearchARrea.Appearance = appearance1;
            this.uGroupSearchARrea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupSearchARrea.Controls.Add(this.uTextSearchVendorCode);
            this.uGroupSearchARrea.Controls.Add(this.uTextSearchVendorName);
            this.uGroupSearchARrea.Controls.Add(this.uLabelSearchVendorName);
            this.uGroupSearchARrea.Controls.Add(this.uLabelSearchVendorCode);
            this.uGroupSearchARrea.Dock = System.Windows.Forms.DockStyle.Top;
            this.uGroupSearchARrea.Location = new System.Drawing.Point(0, 40);
            this.uGroupSearchARrea.Name = "uGroupSearchARrea";
            this.uGroupSearchARrea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupSearchARrea.TabIndex = 8;
            // 
            // uTextSearchVendorCode
            // 
            this.uTextSearchVendorCode.Location = new System.Drawing.Point(128, 12);
            this.uTextSearchVendorCode.MaxLength = 20;
            this.uTextSearchVendorCode.Name = "uTextSearchVendorCode";
            this.uTextSearchVendorCode.Size = new System.Drawing.Size(120, 21);
            this.uTextSearchVendorCode.TabIndex = 97;
            // 
            // uTextSearchVendorName
            // 
            this.uTextSearchVendorName.Location = new System.Drawing.Point(420, 12);
            this.uTextSearchVendorName.MaxLength = 50;
            this.uTextSearchVendorName.Name = "uTextSearchVendorName";
            this.uTextSearchVendorName.Size = new System.Drawing.Size(150, 21);
            this.uTextSearchVendorName.TabIndex = 97;
            // 
            // uLabelSearchVendorName
            // 
            this.uLabelSearchVendorName.Location = new System.Drawing.Point(304, 12);
            this.uLabelSearchVendorName.Name = "uLabelSearchVendorName";
            this.uLabelSearchVendorName.Size = new System.Drawing.Size(112, 20);
            this.uLabelSearchVendorName.TabIndex = 96;
            this.uLabelSearchVendorName.Text = "c";
            // 
            // uLabelSearchVendorCode
            // 
            this.uLabelSearchVendorCode.Location = new System.Drawing.Point(12, 12);
            this.uLabelSearchVendorCode.Name = "uLabelSearchVendorCode";
            this.uLabelSearchVendorCode.Size = new System.Drawing.Size(112, 20);
            this.uLabelSearchVendorCode.TabIndex = 96;
            this.uLabelSearchVendorCode.Text = "4";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.uTextUseFlag);
            this.ultraGroupBox2.Controls.Add(this.uLabelUseFlag);
            this.ultraGroupBox2.Controls.Add(this.uTextVendorCode);
            this.ultraGroupBox2.Controls.Add(this.uTextFax);
            this.ultraGroupBox2.Controls.Add(this.uTextBossName);
            this.ultraGroupBox2.Controls.Add(this.uTextVendorNameEn);
            this.ultraGroupBox2.Controls.Add(this.uTextTel);
            this.ultraGroupBox2.Controls.Add(this.uTextVendorNameCh);
            this.ultraGroupBox2.Controls.Add(this.uTextAddress);
            this.ultraGroupBox2.Controls.Add(this.uTextRegNo);
            this.ultraGroupBox2.Controls.Add(this.uTextVendorName);
            this.ultraGroupBox2.Controls.Add(this.uLabelFax);
            this.ultraGroupBox2.Controls.Add(this.uLabelTel);
            this.ultraGroupBox2.Controls.Add(this.uLabelAddress);
            this.ultraGroupBox2.Controls.Add(this.uLabelBossName);
            this.ultraGroupBox2.Controls.Add(this.uLabelRegNo);
            this.ultraGroupBox2.Controls.Add(this.uLabelVendorCode);
            this.ultraGroupBox2.Controls.Add(this.uLabelVendorName);
            this.ultraGroupBox2.Controls.Add(this.uLabelVendorNameEn);
            this.ultraGroupBox2.Controls.Add(this.uLabelVendorNameCh);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1064, 132);
            this.ultraGroupBox2.TabIndex = 92;
            // 
            // uTextUseFlag
            // 
            this.uTextUseFlag.Location = new System.Drawing.Point(672, 104);
            this.uTextUseFlag.Name = "uTextUseFlag";
            this.uTextUseFlag.ReadOnly = true;
            this.uTextUseFlag.Size = new System.Drawing.Size(100, 21);
            this.uTextUseFlag.TabIndex = 134;
            // 
            // uLabelUseFlag
            // 
            this.uLabelUseFlag.Location = new System.Drawing.Point(548, 104);
            this.uLabelUseFlag.Name = "uLabelUseFlag";
            this.uLabelUseFlag.Size = new System.Drawing.Size(120, 20);
            this.uLabelUseFlag.TabIndex = 133;
            // 
            // uTextVendorCode
            // 
            appearance25.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextVendorCode.Appearance = appearance25;
            this.uTextVendorCode.Location = new System.Drawing.Point(136, 8);
            this.uTextVendorCode.Name = "uTextVendorCode";
            this.uTextVendorCode.ReadOnly = true;
            this.uTextVendorCode.Size = new System.Drawing.Size(150, 21);
            this.uTextVendorCode.TabIndex = 132;
            // 
            // uTextFax
            // 
            appearance17.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextFax.Appearance = appearance17;
            this.uTextFax.Location = new System.Drawing.Point(672, 80);
            this.uTextFax.Name = "uTextFax";
            this.uTextFax.ReadOnly = true;
            this.uTextFax.Size = new System.Drawing.Size(200, 21);
            this.uTextFax.TabIndex = 127;
            // 
            // uTextBossName
            // 
            appearance18.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextBossName.Appearance = appearance18;
            this.uTextBossName.Location = new System.Drawing.Point(672, 56);
            this.uTextBossName.Name = "uTextBossName";
            this.uTextBossName.ReadOnly = true;
            this.uTextBossName.Size = new System.Drawing.Size(200, 21);
            this.uTextBossName.TabIndex = 126;
            // 
            // uTextVendorNameEn
            // 
            appearance19.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextVendorNameEn.Appearance = appearance19;
            this.uTextVendorNameEn.Location = new System.Drawing.Point(672, 32);
            this.uTextVendorNameEn.Name = "uTextVendorNameEn";
            this.uTextVendorNameEn.ReadOnly = true;
            this.uTextVendorNameEn.Size = new System.Drawing.Size(300, 21);
            this.uTextVendorNameEn.TabIndex = 125;
            // 
            // uTextTel
            // 
            appearance26.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextTel.Appearance = appearance26;
            this.uTextTel.Location = new System.Drawing.Point(136, 80);
            this.uTextTel.Name = "uTextTel";
            this.uTextTel.ReadOnly = true;
            this.uTextTel.Size = new System.Drawing.Size(200, 21);
            this.uTextTel.TabIndex = 128;
            // 
            // uTextVendorNameCh
            // 
            appearance22.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextVendorNameCh.Appearance = appearance22;
            this.uTextVendorNameCh.Location = new System.Drawing.Point(136, 32);
            this.uTextVendorNameCh.Name = "uTextVendorNameCh";
            this.uTextVendorNameCh.ReadOnly = true;
            this.uTextVendorNameCh.Size = new System.Drawing.Size(300, 21);
            this.uTextVendorNameCh.TabIndex = 131;
            // 
            // uTextAddress
            // 
            appearance23.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextAddress.Appearance = appearance23;
            this.uTextAddress.Location = new System.Drawing.Point(136, 104);
            this.uTextAddress.Name = "uTextAddress";
            this.uTextAddress.ReadOnly = true;
            this.uTextAddress.Size = new System.Drawing.Size(400, 21);
            this.uTextAddress.TabIndex = 130;
            // 
            // uTextRegNo
            // 
            appearance24.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextRegNo.Appearance = appearance24;
            this.uTextRegNo.Location = new System.Drawing.Point(136, 56);
            this.uTextRegNo.Name = "uTextRegNo";
            this.uTextRegNo.ReadOnly = true;
            this.uTextRegNo.Size = new System.Drawing.Size(200, 21);
            this.uTextRegNo.TabIndex = 129;
            // 
            // uTextVendorName
            // 
            appearance14.BackColor2 = System.Drawing.Color.Gainsboro;
            this.uTextVendorName.Appearance = appearance14;
            this.uTextVendorName.Location = new System.Drawing.Point(672, 8);
            this.uTextVendorName.Name = "uTextVendorName";
            this.uTextVendorName.ReadOnly = true;
            this.uTextVendorName.Size = new System.Drawing.Size(300, 21);
            this.uTextVendorName.TabIndex = 124;
            // 
            // uLabelFax
            // 
            this.uLabelFax.Location = new System.Drawing.Point(548, 80);
            this.uLabelFax.Name = "uLabelFax";
            this.uLabelFax.Size = new System.Drawing.Size(120, 20);
            this.uLabelFax.TabIndex = 120;
            // 
            // uLabelTel
            // 
            this.uLabelTel.Location = new System.Drawing.Point(12, 80);
            this.uLabelTel.Name = "uLabelTel";
            this.uLabelTel.Size = new System.Drawing.Size(120, 20);
            this.uLabelTel.TabIndex = 119;
            // 
            // uLabelAddress
            // 
            this.uLabelAddress.Location = new System.Drawing.Point(12, 104);
            this.uLabelAddress.Name = "uLabelAddress";
            this.uLabelAddress.Size = new System.Drawing.Size(120, 20);
            this.uLabelAddress.TabIndex = 121;
            // 
            // uLabelBossName
            // 
            this.uLabelBossName.Location = new System.Drawing.Point(548, 56);
            this.uLabelBossName.Name = "uLabelBossName";
            this.uLabelBossName.Size = new System.Drawing.Size(120, 20);
            this.uLabelBossName.TabIndex = 123;
            // 
            // uLabelRegNo
            // 
            this.uLabelRegNo.Location = new System.Drawing.Point(12, 56);
            this.uLabelRegNo.Name = "uLabelRegNo";
            this.uLabelRegNo.Size = new System.Drawing.Size(120, 20);
            this.uLabelRegNo.TabIndex = 122;
            // 
            // uLabelVendorCode
            // 
            this.uLabelVendorCode.Location = new System.Drawing.Point(12, 8);
            this.uLabelVendorCode.Name = "uLabelVendorCode";
            this.uLabelVendorCode.Size = new System.Drawing.Size(120, 20);
            this.uLabelVendorCode.TabIndex = 115;
            this.uLabelVendorCode.Text = "3";
            // 
            // uLabelVendorName
            // 
            this.uLabelVendorName.Location = new System.Drawing.Point(548, 8);
            this.uLabelVendorName.Name = "uLabelVendorName";
            this.uLabelVendorName.Size = new System.Drawing.Size(120, 20);
            this.uLabelVendorName.TabIndex = 116;
            this.uLabelVendorName.Text = "4";
            // 
            // uLabelVendorNameEn
            // 
            this.uLabelVendorNameEn.Location = new System.Drawing.Point(548, 32);
            this.uLabelVendorNameEn.Name = "uLabelVendorNameEn";
            this.uLabelVendorNameEn.Size = new System.Drawing.Size(120, 20);
            this.uLabelVendorNameEn.TabIndex = 118;
            this.uLabelVendorNameEn.Text = "6";
            // 
            // uLabelVendorNameCh
            // 
            this.uLabelVendorNameCh.Location = new System.Drawing.Point(12, 32);
            this.uLabelVendorNameCh.Name = "uLabelVendorNameCh";
            this.uLabelVendorNameCh.Size = new System.Drawing.Size(120, 20);
            this.uLabelVendorNameCh.TabIndex = 117;
            this.uLabelVendorNameCh.Text = "5";
            // 
            // uGroupBox1
            // 
            this.uGroupBox1.Controls.Add(this.uGridVendorP);
            this.uGroupBox1.Controls.Add(this.ultraGroupBox1);
            this.uGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGroupBox1.Location = new System.Drawing.Point(0, 132);
            this.uGroupBox1.Name = "uGroupBox1";
            this.uGroupBox1.Size = new System.Drawing.Size(1064, 563);
            this.uGroupBox1.TabIndex = 93;
            // 
            // uGridVendorP
            // 
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridVendorP.DisplayLayout.Appearance = appearance30;
            this.uGridVendorP.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridVendorP.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridVendorP.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridVendorP.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.uGridVendorP.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridVendorP.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.uGridVendorP.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridVendorP.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridVendorP.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance33.BackColor = System.Drawing.SystemColors.Highlight;
            appearance33.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridVendorP.DisplayLayout.Override.ActiveRowAppearance = appearance33;
            this.uGridVendorP.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridVendorP.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.uGridVendorP.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance31.BorderColor = System.Drawing.Color.Silver;
            appearance31.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridVendorP.DisplayLayout.Override.CellAppearance = appearance31;
            this.uGridVendorP.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridVendorP.DisplayLayout.Override.CellPadding = 0;
            appearance35.BackColor = System.Drawing.SystemColors.Control;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridVendorP.DisplayLayout.Override.GroupByRowAppearance = appearance35;
            appearance37.TextHAlignAsString = "Left";
            this.uGridVendorP.DisplayLayout.Override.HeaderAppearance = appearance37;
            this.uGridVendorP.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridVendorP.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.uGridVendorP.DisplayLayout.Override.RowAppearance = appearance36;
            this.uGridVendorP.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance34.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridVendorP.DisplayLayout.Override.TemplateAddRowAppearance = appearance34;
            this.uGridVendorP.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridVendorP.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridVendorP.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridVendorP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uGridVendorP.Location = new System.Drawing.Point(3, 48);
            this.uGridVendorP.Name = "uGridVendorP";
            this.uGridVendorP.Size = new System.Drawing.Size(1058, 512);
            this.uGridVendorP.TabIndex = 3;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.uButtonDeleteRow);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1058, 48);
            this.ultraGroupBox1.TabIndex = 2;
            // 
            // uButtonDeleteRow
            // 
            this.uButtonDeleteRow.Location = new System.Drawing.Point(11, 11);
            this.uButtonDeleteRow.Name = "uButtonDeleteRow";
            this.uButtonDeleteRow.Size = new System.Drawing.Size(88, 28);
            this.uButtonDeleteRow.TabIndex = 1;
            this.uButtonDeleteRow.Text = "ultraButton1";
            // 
            // frmMAS0032
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupSearchARrea);
            this.Controls.Add(this.uGroupBoxContentsArea);
            this.Controls.Add(this.uGridVendor);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMAS0032";
            this.Load += new System.EventHandler(this.frmMAS0032_Load);
            this.Activated += new System.EventHandler(this.frmMAS0032_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMAS0032_FormClosing);
            this.Resize += new System.EventHandler(this.frmMAS0032_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxContentsArea)).EndInit();
            this.uGroupBoxContentsArea.ResumeLayout(false);
            this.ultraExpandableGroupBoxPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupSearchARrea)).EndInit();
            this.uGroupSearchARrea.ResumeLayout(false);
            this.uGroupSearchARrea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextSearchVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextUseFlag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextBossName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorNameEn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorNameCh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextRegNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextVendorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBox1)).EndInit();
            this.uGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridVendorP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraExpandableGroupBox uGroupBoxContentsArea;
        private Infragistics.Win.Misc.UltraExpandableGroupBoxPanel ultraExpandableGroupBoxPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridVendor;
        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupSearchARrea;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendorName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextSearchVendorName;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextUseFlag;
        private Infragistics.Win.Misc.UltraLabel uLabelUseFlag;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextFax;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextBossName;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorNameEn;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextTel;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorNameCh;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextAddress;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextRegNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextVendorName;
        private Infragistics.Win.Misc.UltraLabel uLabelFax;
        private Infragistics.Win.Misc.UltraLabel uLabelTel;
        private Infragistics.Win.Misc.UltraLabel uLabelAddress;
        private Infragistics.Win.Misc.UltraLabel uLabelBossName;
        private Infragistics.Win.Misc.UltraLabel uLabelRegNo;
        private Infragistics.Win.Misc.UltraLabel uLabelVendorCode;
        private Infragistics.Win.Misc.UltraLabel uLabelVendorName;
        private Infragistics.Win.Misc.UltraLabel uLabelVendorNameEn;
        private Infragistics.Win.Misc.UltraLabel uLabelVendorNameCh;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBox1;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridVendorP;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraButton uButtonDeleteRow;
    }
}