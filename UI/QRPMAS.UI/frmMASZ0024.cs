﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 품질관리기본정보                                      */
/* 프로그램ID   : frmMASZ0024.cs                                        */
/* 프로그램명   : CCS의뢰 기본항목정보                                  */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-07-11                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using 추가
using QRPCOM.QRPUI;
using QRPCOM.QRPGLO;
using System.Resources;
using System.EnterpriseServices;
using System.Threading;
using System.Collections;

namespace QRPMAS.UI
{
    public partial class frmMASZ0024 : Form, IToolbar
    {
        //다국어지원
        QRPGlobal SysRes = new QRPGlobal();

        public frmMASZ0024()
        {
            InitializeComponent();
        }

        private void frmMASZ0024_Activated(object sender, EventArgs e)
        {
            //해당 화면에 대한 툴바버튼 활성여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, true, false, true, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMASZ0024_Load(object sender, EventArgs e)
        {                        
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            this.titleArea.mfSetLabelText("CCS의뢰 기본항목정보", m_resSys.GetString("SYS-FONTNAME"), 12);

            //컨트롤초기화  
            SetToolAuth();          
            InitLabel();
            InitGrid();
            InitComboBox();
            InitButton();

            uGroupBoxContentsArea.Expanded = false;             

            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfLoadGridColumnProperty(this);           
        }

        #region 컨트롤초기화
        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 버튼 초기화                
        /// </summary>
        private void InitButton()
        {
            try
            {
                //System ResourceInfo                
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinButton btn = new WinButton();                
                btn.mfSetButton(this.uButtonDelRow, "행삭제", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_delTable);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }
        /// <summary>
        /// 레이블초기화
        /// </summary>
        private void InitLabel()
        {
            try
            {
                //System ResourceInfo 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinLabel lbl = new WinLabel();
                //검색Label
                lbl.mfSetLabel(this.uLabelSearchPlant, "공장", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                lbl.mfSetLabel(this.uLabelSearchProcess, "공정", m_resSys.GetString("SYS_FONFNEMAE"), true, false);
                //상세Label                
                lbl.mfSetLabel(this.uLabelPlant,"공장", m_resSys.GetString("SYS_FONFNEMAE"), true, true);
                lbl.mfSetLabel(this.uLabelProcess, "공정", m_resSys.GetString("SYS_FONFNEMAE"), true, true);                
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}

        }
        /// <summary>
        /// 콤보박스초기화
        /// </summary>
        private void InitComboBox()
        {
            try
            {
                // SystemInfo ResourceSet
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor Combo = new WinComboEditor();

                // SearchArea Plant ComboBox
                // BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                brwChannel.mfCredentials(clsPlant);                
                
                // 함수호출
                DataTable dtPlant = clsPlant.mfReadPlantForCombo(m_resSys.GetString("SYS_LANG"));
                // 검색:공장콤보 
                Combo.mfSetComboEditor(this.uComboSearchPlant, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
                // 상세:공장콤보
                Combo.mfSetComboEditor(this.uComboPlant, true, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, m_resSys.GetString("SYS_PLANTCODE"), "", "전체"
                    , "PlantCode", "PlantName", dtPlant);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //System ResourceInfo 
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinGrid grd = new WinGrid();

                //리스트 그리드 설정
                grd.mfInitGeneralGrid(this.uGridList, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , false, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));
                //컬럼설정
                grd.mfSetGridColumn(this.uGridList, 0, "PlantCode", "공장코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGridList, 0, "PlantName", "공장명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGridList, 0, "ProcessCode", "공정코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, true, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGridList, 0, "ProcessName", "공정명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGridList, 0, "CCSReqItemCode", "기본항목코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGridList, 0, "CCSReqItemName", "기본항목명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGridList, 0, "CCSReqItemNameCh", "기본항목명 중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                grd.mfSetGridColumn(this.uGridList, 0, "CCSReqItemNameEn", "기본항목명 영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, false, false, 10
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");
                //폰트설정
                uGridList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                //빈줄추가
                grd.mfAddRowGrid(this.uGridList, 0);
                    
                
                //상세 그리드 설정                                
                grd.mfInitGeneralGrid(this.uGridDetail, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None
                    , true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, true
                    , Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Button, Infragistics.Win.UltraWinGrid.SelectType.Single
                    , Infragistics.Win.DefaultableBoolean.True, Infragistics.Win.UltraWinGrid.FilterUIType.HeaderIcons, Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom
                    , 0, false, m_resSys.GetString("SYS_FONTNAME"));                
                //컬럼설정
                grd.mfSetGridColumn(this.uGridDetail, 0, "Check", "선택", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridDetail, 0, "CCSReqItemCode", "기본항목코드", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 150, true, false, 10
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "CCSReqItemName", "기본항목명", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, true, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "CCSReqItemNameCh", "기본항목명_중문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "CCSReqItemNameEn", "기본항목명_영문", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 200, false, false, 20
                    , Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridDetail, 0, "UseFlag", "사용여부", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 100, false, false, 20
                    , Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown, "", "", "T");

                //사용여부 DropDwon --> BL호출                
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                brwChannel.mfCredentials(clsCommonCode);
                //매소드 호출
                DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", m_resSys.GetString("SYS_LANG"));
                //그리드에 Data 넣기
                grd.mfSetGridColumnValueList(this.uGridDetail, 0, "UseFlag", Infragistics.Win.ValueListDisplayStyle.DisplayText, "", "선택", dtUseFlag);                

                //폰트설정
                uGridDetail.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                uGridDetail.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;
                //빈줄추가
                grd.mfAddRowGrid(this.uGridDetail, 0);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {}
        }

        // 화면 초기화
        private void InitClear()
        {
            try
            {
                //항목초기화
                this.uComboSearchPlant.Value = "";

                this.uComboPlant.ReadOnly = false;
                this.uComboPlant.Value = "";
                this.uComboPlant.Appearance.BackColor = Color.PowderBlue;

                this.uComboProcess.ReadOnly = false;
                this.uComboProcess.Value = "";
                this.uComboProcess.Appearance.BackColor = Color.PowderBlue;

                this.uGridDetail.DisplayLayout.Bands[0].Columns["CCSReqItemCode"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;

                //그리드 내용 초기화            
                while (this.uGridDetail.Rows.Count > 0)
                {
                    this.uGridDetail.Rows[0].Delete(false);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region 툴바관련
        public void mfSearch()
        {
            try
            {
                // SystemsInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //조회조건 변수 설정
                string strSearchPlantCode = this.uComboSearchPlant.Value.ToString();
                string strSearchProcessCode = this.uComboSearchProcess.Value.ToString();

                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, "검색중...");
                this.MdiParent.Cursor = Cursors.WaitCursor;

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqItem), "CCSReqItem");
                QRPMAS.BL.MASQUA.CCSReqItem clsCCSReqItem = new QRPMAS.BL.MASQUA.CCSReqItem();
                brwChannel.mfCredentials(clsCCSReqItem);
                DataTable dtCCSReqItem = new DataTable();

                //Method 호출
                dtCCSReqItem = clsCCSReqItem.mfReadCCSReqItem(strSearchPlantCode, strSearchProcessCode, m_resSys.GetString("SYS_LANG"));
                this.uGridList.DataSource = dtCCSReqItem;
                this.uGridList.DataBind();

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                // ContentGroupBox 접은상태로
                this.uGroupBoxContentsArea.Expanded = false;

                DialogResult DResult = new DialogResult();
                WinMessageBox msg = new WinMessageBox();
                if (dtCCSReqItem.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "처리결과", "조회처리결과", "조회결과가 없습니다.", Infragistics.Win.HAlign.Right);    
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }
        public void mfSave()
        {
            try
            {
                //SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                QRPCOM.QRPUI.WinMessageBox msg = new WinMessageBox();
                DialogResult DResult = new DialogResult();

                // 헤더 필수 입력정보 확인
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                    , "확인창", "필수입력사항확인", "저장할 행을 더블클릭 또는 신규버튼을 클릭하세요.", Infragistics.Win.HAlign.Right);
                    return;
                }
                else
                {
                    if (this.uComboPlant.Value.ToString() == "")
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "확인창", "필수입력사항확인", "공장을 선택해 주세요", Infragistics.Win.HAlign.Right);

                        // Set Focus
                        this.uComboPlant.DropDown();
                        return;
                    }
                    else if (this.uComboProcess.Value.ToString() == "")
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "확인창", "필수입력사항확인", "공정을 선택해 주세요", Infragistics.Win.HAlign.Right);

                        // Set Focus
                        this.uComboProcess.DropDown();
                        return;
                    }
                    else if (this.uGridDetail.Rows.Count <= 0)
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                                   , "확인창", "필수입력사항확인", "기본항목코드 및 기본항목명을 확인하세요.", Infragistics.Win.HAlign.Right);                                     
                        return;

                    }
                }               

                
                // BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqItem), "CCSReqItem");
                QRPMAS.BL.MASQUA.CCSReqItem clsSave = new QRPMAS.BL.MASQUA.CCSReqItem();
                brwChannel.mfCredentials(clsSave);
                //저장 함수호출 매개변수 DataTable
                DataTable dtSave = clsSave.mfSetDataInfo();                

                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    this.uGridDetail.ActiveCell = this.uGridDetail.Rows[0].Cells[0];                    
                
                    if (this.uGridDetail.Rows[i].Cells["CCSReqItemCode"].Value.ToString() == "")
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항확인", (i + 1).ToString() + "번째줄의 CCS의뢰 기본항목코드 입력해 주세요", Infragistics.Win.HAlign.Right);

                        // Set Focus
                        this.uGridDetail.ActiveCell = this.uGridDetail.Rows[i].Cells["CCSReqItemCode"];
                        this.uGridDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                    else if (this.uGridDetail.Rows[i].Cells["CCSReqItemName"].Value.ToString() == "")
                    {
                        DResult = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                                            , "확인창", "필수입력사항확인", (i + 1).ToString() + "번째줄의 CCS의뢰 기본항목명을 입력해 주세요", Infragistics.Win.HAlign.Right);

                        // Set Focus
                        this.uGridDetail.ActiveCell = this.uGridDetail.Rows[i].Cells["CCSReqItemName"];
                        this.uGridDetail.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditModeAndDropdown);
                        return;
                    }
                    else
                    {
                        // 저장 Start
                        DataRow drRow = dtSave.NewRow();

                        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                        drRow["ProcessCode"] = this.uComboProcess.Value.ToString();
                        drRow["CCSReqItemCode"] = this.uGridDetail.Rows[i].Cells["CCSReqItemCode"].Value.ToString();
                        drRow["CCSReqItemName"] = this.uGridDetail.Rows[i].Cells["CCSReqItemName"].Value.ToString();
                        drRow["CCSReqItemNameCh"] = this.uGridDetail.Rows[i].Cells["CCSReqItemNameCh"].Value.ToString();
                        drRow["CCSReqItemNameEn"] = this.uGridDetail.Rows[i].Cells["CCSReqItemNameEn"].Value.ToString();
                        drRow["UseFlag"] = this.uGridDetail.Rows[i].Cells["UseFlag"].Value.ToString();                        
                        drRow["Check"] = this.uGridDetail.Rows[i].Hidden.ToString();

                        dtSave.Rows.Add(drRow);
                    }                    
                }
                if(dtSave.Rows.Count > 0)
                {
                    if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                           "확인창", "저장확인", "입력한 정보를 저장하시겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                    {
                        // 프로그래스 팝업창 Open
                        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                        Thread thread = m_ProgressPopup.mfStartThread();
                        m_ProgressPopup.mfOpenProgressPopup(this, "저장중 입니다.");
                        this.MdiParent.Cursor = Cursors.WaitCursor;
                        
                        // 입력 정보 BL로 이동
                        string strErrRtn = clsSave.mfSaveCCSReqItem(dtSave, m_resSys.GetString("SYS_USERIP"), m_resSys.GetString("SYS_USERID"));

                        // 프로그래스 팝업창 Close
                        this.MdiParent.Cursor = Cursors.Default;
                        m_ProgressPopup.mfCloseProgressPopup(this);

                        // 저장결과
                        TransErrRtn ErrRtn = new TransErrRtn();
                        ErrRtn = ErrRtn.mfDecodingErrMessage(strErrRtn);
                        if (ErrRtn.ErrNum == 0)
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                            "처리결과", "저장처리결과", "입력한 정보를 성공적으로 저장했습니다.", Infragistics.Win.HAlign.Right);
                            //List갱신
                            mfSearch();
                        }
                        else
                        {
                            DResult = msg.mfSetMessageBox(MessageBoxType.Warning, m_resSys.GetString("SYS_LANG"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                                                                 "처리결과", "저장처리결과", "입력한 정보를 저장하지 못했습니다.", Infragistics.Win.HAlign.Right);
                            return;
                        }                        
                    }
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }
        public void mfDelete()
        {
            try
            {
                //** 상세 그리드 행삭제 버튼으로 대체한다. **//

                ////// SystemsInfo ResourceSet
                ////ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                ////WinMessageBox msg = new WinMessageBox();
                ////DialogResult Result = new DialogResult();

                ////if (this.uGroupBoxContentsArea.Expanded == false )
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////        , "확인창", "삭제시 입력사항 확인", "삭제할 행을 더블클릭 하세요", Infragistics.Win.HAlign.Right);
                ////}
                ////else if (this.uComboPlant.Value.ToString() == "" || this.uComboProcess.Value.ToString() == "")
                ////{
                ////    Result = msg.mfSetMessageBox(MessageBoxType.Error, m_resSys.GetString("SYS_FONTNAME"), 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                ////        , "확인창", "필수입력사항 확인", "그리드에서 삭제할 행을 선택해 주세요.", Infragistics.Win.HAlign.Center);
                ////    this.uComboPlant.DropDown();
                ////    return;
                ////}
                ////else
                ////{
                ////    if (msg.mfSetMessageBox(MessageBoxType.YesNo, "굴림", 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                        "확인창", "삭제확인", "선택한 정보를 삭제하겠습니까?", Infragistics.Win.HAlign.Right) == DialogResult.Yes)
                ////    {
                ////        QRPBrowser brwChannel = new QRPBrowser();
                ////        brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqItem), "CCSReqItem");
                ////        QRPMAS.BL.MASQUA.CCSReqItem clsDelete = new QRPMAS.BL.MASQUA.CCSReqItem();
                ////        brwChannel.mfCredentials(clsDelete);

                ////        // DataTable 컬럼 설정
                ////        DataTable dtDelete = clsDelete.mfSetDataInfo();

                ////        DataRow drRow = dtDelete.NewRow();

                ////        drRow["PlantCode"] = this.uComboPlant.Value.ToString();
                ////        drRow["ProcessCode"] = this.uComboProcess.Value.ToString();

                ////        dtDelete.Rows.Add(drRow);

                ////        QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                ////        Thread thread = m_ProgressPopup.mfStartThread();
                ////        m_ProgressPopup.mfOpenProgressPopup(this, "삭제중");
                ////        this.MdiParent.Cursor = Cursors.WaitCursor;

                ////        string rtnMsg = clsDelete.mfDeleteCCSReqItem(dtDelete);

                ////        // Decoding //
                ////        TransErrRtn ErrRtn = new TransErrRtn();
                ////        ErrRtn = ErrRtn.mfDecodingErrMessage(rtnMsg);
                ////        // 처리로직 끝 //

                ////        this.MdiParent.Cursor = Cursors.Default;
                ////        m_ProgressPopup.mfCloseProgressPopup(this);

                ////        // 삭제성공여부
                ////        if (ErrRtn.ErrNum == 0)
                ////        {
                ////            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                ////                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                                "처리결과", "삭제처리결과", "입력한 정보를 성공적으로 삭제했습니다.",
                ////                                Infragistics.Win.HAlign.Right);

                ////            // 리스트 갱신
                ////            mfSearch();
                ////        }
                ////        else
                ////        {
                ////            Result = msg.mfSetMessageBox(MessageBoxType.Information, m_resSys.GetString("SYS_FONTNAME"), 500, 500,
                ////                                Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista,
                ////                                "처리결과", "삭제처리결과", "입력한 정보를 성공적으로 삭제하지못했습니다.",
                ////                                Infragistics.Win.HAlign.Right);
                ////        }
                ////    }
                ////}
            }
            catch
            { }
            finally
            { }
        }
        public void mfCreate()
        {
            try
            {
                // 펼침상태가 false 인경우
                if (this.uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    InitClear();
                }
                // 이미 펼쳐진 상태이면 컴포넌트 초기화
                else
                {
                    InitClear();
                }
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }        

        public void mfExcel()
        {
            try
            {
                WinGrid grd = new WinGrid();

                //엑셀저장함수 호출
                grd.mfDownLoadGridToExcel(this.uGridList);
            }
            catch(Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }
        public void mfPrint()
        {           
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }
        #endregion


        #region 공통이벤트
        // 행삭제버튼 기능
        private void uButtonDelRow_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < this.uGridDetail.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(this.uGridDetail.Rows[i].Cells["Check"].Value) == true)
                        this.uGridDetail.Rows[i].Hidden = true;
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 셀 업데이트 이벤트
        private void uGridDetail_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                // 자동 행삭제
                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                if (grd.mfCheckCellDataInRow(this.uGridDetail, 0, e.Cell.Row.Index))
                    e.Cell.Row.Delete(false);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        // 그룹박스 펼침상태 이벤트
        private void uGroupBoxContentsArea_ExpandedStateChanging(object sender, CancelEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    Point point = new Point(0, 130);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridList.Height = 45;
                }
                else
                {
                    Point point = new Point(0, 825);
                    this.uGroupBoxContentsArea.Location = point;
                    this.uGridList.Height = 740;

                    for (int i = 0; i < uGridList.Rows.Count; i++)
                    {
                        uGridList.Rows[i].Fixed = false;
                    }
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        
        // 폼을 닫기 전에 현재 폼의 Grid에 대한 열폭 및 열순서를 저장                
        private void frmMASZ0024_FormClosing(object sender, FormClosingEventArgs e)
        {
            QRPCOM.QRPUI.WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }        
        #endregion

        #region Action 이벤트

        // 그리드 더블클릭시 상세정보를보여준다.
        private void uGridList_DoubleClickCell(object sender, Infragistics.Win.UltraWinGrid.DoubleClickCellEventArgs e)
        {
            try
            {
                if (uGroupBoxContentsArea.Expanded == false)
                {
                    this.uGroupBoxContentsArea.Expanded = true;
                    e.Cell.Row.Fixed = true;
                }

                // SystemsInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //그리드에 있는 값을 받아서 상세정보 항목에 보여줌.
                this.uComboPlant.Value = e.Cell.Row.Cells["PlantCode"].Value;
                this.uComboProcess.Value = e.Cell.Row.Cells["ProcessCode"].Value;

                //PK항목 입력불가 처리
                this.uComboPlant.ReadOnly = true;
                this.uComboPlant.Appearance.BackColor = Color.Gainsboro;
                this.uComboProcess.ReadOnly = true;
                this.uComboProcess.Appearance.BackColor = Color.Gainsboro;

                String strPlantCode = this.uComboPlant.Value.ToString();
                String strProcessCode = this.uComboProcess.Value.ToString();

                //BL호출
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASQUA.CCSReqItem), "CCSReqItem");
                QRPMAS.BL.MASQUA.CCSReqItem clsCCSReqItem = new QRPMAS.BL.MASQUA.CCSReqItem();
                brwChannel.mfCredentials(clsCCSReqItem);
                DataTable dtCCSReqItem = new DataTable();
                //Mtehod 호출
                dtCCSReqItem = clsCCSReqItem.mfReadCCSReqItem(strPlantCode, strProcessCode, m_resSys.GetString("SYS_LANG"));
                this.uGridDetail.DataSource = dtCCSReqItem;
                this.uGridDetail.DataBind();

                // 바인딩된 그리드 PK 값 편집 불가 상태로
                for (int i = 0; i < uGridDetail.Rows.Count; i++)
                {
                    this.uGridDetail.Rows[i].Cells["CCSReqItemCode"].Activation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    this.uGridDetail.Rows[i].Cells["CCSReqItemCode"].Appearance.BackColor = Color.Gainsboro;
                }

                this.MdiParent.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }                

        // 검색 : 공장콤보 선택시 해당 공정정보 콤보 설정.
        private void uComboSearchPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo 리소스
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinComboEditor wCombo = new WinComboEditor();

                DataTable dtProcess = new DataTable();
                String strPlantCode = this.uComboSearchPlant.Value.ToString();

                this.uComboSearchProcess.Items.Clear();

                if (strPlantCode != "")
                {
                    // Bl 연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);

                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));
                }

                wCombo.mfSetComboEditor(this.uComboSearchProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                    , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "전체"
                    , "ProcessCode", "ProcessName", dtProcess);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { 
            }
        }

        // 상세정보 : 공장콤보 선택시 해당 공정정보 콤보 설정.
        private void uComboPlant_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //SystemInfo 리소트
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);                
                WinComboEditor wCombo = new WinComboEditor();
                DataTable dtProcess = new DataTable();

                string strPlantCode = this.uComboPlant.Value.ToString();

                this.uComboProcess.Items.Clear();                

                if (strPlantCode != "")
                {
                    //BL연결
                    QRPBrowser brwChannel = new QRPBrowser();
                    brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Process), "Process");
                    QRPMAS.BL.MASPRC.Process clsProcess = new QRPMAS.BL.MASPRC.Process();
                    brwChannel.mfCredentials(clsProcess);
                    
                    dtProcess = clsProcess.mfReadProcessForCombo(strPlantCode, m_resSys.GetString("SYS_LANG"));                    
                }
                wCombo.mfSetComboEditor(this.uComboProcess, false, false, Infragistics.Win.EmbeddableElementDisplayStyle.WindowsVista, m_resSys.GetString("SYS_FONTNAME")
                        , true, false, "", true, Infragistics.Win.DropDownResizeHandleStyle.Default, true, 100, Infragistics.Win.HAlign.Center, "", "", "선택"
                        , "ProcessCode", "ProcessName", dtProcess);
                this.uComboProcess.Appearance.BackColor = Color.PowderBlue;
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
       
        #endregion                
    }
}

