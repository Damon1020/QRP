﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 마스터관리                                            */
/* 모듈(분류)명 : 기준정보                                              */
/* 프로그램ID   : frmMAS0042_P1.cs                                      */
/* 프로그램명   : 금형치공구Lot추가리스트팝업창                         */
/* 작성자       : 권종구                                                */
/* 작성일자     : 2011-01-18                                            */
/* 수정이력     : 2011-09-26 :                                          */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// 추가
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;
using System.Threading;
using System.Resources;

namespace QRPMAS.UI
{
    public partial class frmMAS0042_P1 : Form
    {
        QRPGlobal SysRes = new QRPGlobal();

        private DataTable dtLotInfo;

        public DataTable LotInfo
        {
            get { return dtLotInfo; }
            set { dtLotInfo = value; }
        }


        public frmMAS0042_P1()
        {
            InitializeComponent();
        }

        public frmMAS0042_P1(string strPlantCode)
        {
            InitializeComponent();
            InitButton();
            InitGrid();
            InitData(strPlantCode);

            //선택한정보를 넘겨주기위한 컬럼설정
            dtLotInfo = new DataTable();
            dtLotInfo.Columns.Add("PlantCode", typeof(string));
            dtLotInfo.Columns.Add("Package", typeof(string));
            dtLotInfo.Columns.Add("ModelName", typeof(string));
            dtLotInfo.Columns.Add("EquipCode", typeof(string));
            dtLotInfo.Columns.Add("LotNo", typeof(string));
            dtLotInfo.Columns.Add("RepairReqCode", typeof(string));
            dtLotInfo.Columns.Add("DurableMatCode", typeof(string));
            dtLotInfo.Columns.Add("GRDate", typeof(string));
            dtLotInfo.Columns.Add("CurUsage", typeof(string));
            dtLotInfo.Columns.Add("CumUsage", typeof(string));
            dtLotInfo.Columns.Add("LastInspectDate", typeof(string));
            dtLotInfo.Columns.Add("DiscardName", typeof(string));
            dtLotInfo.Columns.Add("DiscardDate", typeof(string));
            dtLotInfo.Columns.Add("DiscardChargeID", typeof(string));
            dtLotInfo.Columns.Add("DiscardChargeName", typeof(string));
            dtLotInfo.Columns.Add("DiscardReason", typeof(string));
            dtLotInfo.Columns.Add("WriteID", typeof(string));

            QRPBrowser brw = new QRPBrowser();
            brw.mfSetFormLanguage(this);
        }

        /// <summary>
        /// 버튼초기화
        /// </summary>
        private void InitButton()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinButton btn = new WinButton();

                btn.mfSetButton(this.uButtonOK, "확인", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_OK);
                btn.mfSetButton(this.uButtonCancel, "취소", m_resSys.GetString("SYS_FONTNAME"), Properties.Resources.btn_Cancel);

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }


        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                //ResourceInfo
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                WinGrid grd = new WinGrid();

                // 그리드 기본설정
                grd.mfInitGeneralGrid(this.uGridLot, true, Infragistics.Win.UltraWinGrid.NewColumnLoadStyle.Hide, Infragistics.Win.UltraWinGrid.AutoFitStyle.None,
                    true, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never, false, Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.None,
                     Infragistics.Win.UltraWinGrid.SelectType.Single, Infragistics.Win.DefaultableBoolean.False, Infragistics.Win.UltraWinGrid.FilterUIType.Default,
                      Infragistics.Win.UltraWinGrid.AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                //컬럼설정
                grd.mfSetGridColumn(this.uGridLot, 0, "Check", "", false, Infragistics.Win.UltraWinGrid.Activation.AllowEdit, 30, false, false, 0, 
                    Infragistics.Win.HAlign.Center,Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(this.uGridLot, 0, "Package", "Package", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40,
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "ModelName", "ModelName", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40,
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "EquipCode", "설비", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 20,
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "LotNo", "LotNo", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40,
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "WriteName", "수리출고요청자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, false, 40,
                    Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "EtcDesc", "비고", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 200, false, false, 100,
                    Infragistics.Win.HAlign.Left, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "RepairReqCode", "수리요청코드", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 20,
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "PlantCode", "공장", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 40,
                    Infragistics.Win.HAlign.Center, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "Qty", "수량", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 40,
                    Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(this.uGridLot, 0, "WriteID", "수리출고요청자", false, Infragistics.Win.UltraWinGrid.Activation.ActivateOnly, 100, false, true, 40,
                    Infragistics.Win.HAlign.Right, Infragistics.Win.VAlign.Middle, Infragistics.Win.UltraWinGrid.MergedCellStyle.Never,
                    Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");




                this.uGridLot.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;

            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 데이터 바인드
        /// </summary>
        /// <param name="strPlantCode">공장코드</param>
        private void InitData(string strPlantCode)
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                //신규 Lot정보BL호출
                QRPBrowser brwChannel = new QRPBrowser();
                brwChannel.mfRegisterChannel(typeof(QRPEQU.BL.EQUREP.RepairLotHistory), "RepairLotHistory");
                QRPEQU.BL.EQUREP.RepairLotHistory clsRepairLotHistory = new QRPEQU.BL.EQUREP.RepairLotHistory();
                brwChannel.mfCredentials(clsRepairLotHistory);

                //신규 Lot정보 조회 매서드실행
                DataTable dtLotInfo = clsRepairLotHistory.mfReadRepairLotHistory(strPlantCode, m_resSys.GetString("SYS_LANG"));

                this.uGridLot.DataSource = dtLotInfo;
                this.uGridLot.DataBind();


            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        /// <summary>
        /// 확인버튼을 누를 경우 체크된 Lot정보가 저장된다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uButtonOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.uGridLot.Rows.Count > 0)
                {
                    DataRow drLot;
                    for (int i = 0; i < this.uGridLot.Rows.Count; i++)
                    {
                        if (Convert.ToBoolean(this.uGridLot.Rows[i].Cells["Check"].Value))
                        {
                            drLot = dtLotInfo.NewRow();
                            drLot["Package"] = this.uGridLot.Rows[i].GetCellValue("Package");
                            drLot["ModelName"] = this.uGridLot.Rows[i].GetCellValue("ModelName");
                            drLot["EquipCode"] = this.uGridLot.Rows[i].GetCellValue("EquipCode");
                            drLot["LotNo"] = this.uGridLot.Rows[i].GetCellValue("LotNo");
                            drLot["RepairReqCode"] = this.uGridLot.Rows[i].GetCellValue("RepairReqCode");
                            drLot["PlantCode"] = this.uGridLot.Rows[i].GetCellValue("PlantCode");
                            drLot["WriteID"] = this.uGridLot.Rows[i].GetCellValue("WriteID"); 
                            drLot["GRDate"] = DateTime.Now.Date.ToString("yyyy-MM-dd");
                            drLot["CurUsage"] = 0;
                            drLot["CumUsage"] = 0;
                            dtLotInfo.Rows.Add(drLot);
                        }
                    }
                }

                this.Close();
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            { }
        }

        //취소버튼이 눌려지면 팝업창을 닫는다.
        private void uButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    
    }


   
}
