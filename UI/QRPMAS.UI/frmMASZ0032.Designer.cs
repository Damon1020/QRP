﻿namespace QRPMAS.UI
{
    partial class frmMASZ0032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMASZ0032));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            this.titleArea = new QRPUserControl.TitleArea();
            this.uGroupBoxSearchArea = new Infragistics.Win.Misc.UltraGroupBox();
            this.uComboSearchPackage = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchPackage = new Infragistics.Win.Misc.UltraLabel();
            this.uComboSearchDurableMatName = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uComboSearchPlant = new Infragistics.Win.UltraWinEditors.UltraComboEditor();
            this.uLabelSearchDurableMatName = new Infragistics.Win.Misc.UltraLabel();
            this.uLabelSearchPlant = new Infragistics.Win.Misc.UltraLabel();
            this.uGroupDurable = new Infragistics.Win.Misc.UltraGroupBox();
            this.uGridDurableMat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGroupBoxDurableMatLot = new Infragistics.Win.Misc.UltraGroupBox();
            this.uButtonDelete = new Infragistics.Win.Misc.UltraButton();
            this.uTextLotNo = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextDurableMatCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uTextPlantCode = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            this.uButtonMove = new Infragistics.Win.Misc.UltraButton();
            this.uGridLotNoSpecInfo = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uGridDurableSpec = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.uTextDurableMatName = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).BeginInit();
            this.uGroupBoxSearchArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableMatName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDurable)).BeginInit();
            this.uGroupDurable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDurableMatLot)).BeginInit();
            this.uGroupBoxDurableMatLot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLotNoSpecInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableSpec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).BeginInit();
            this.SuspendLayout();
            // 
            // titleArea
            // 
            this.titleArea.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("titleArea.BackgroundImage")));
            this.titleArea.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleArea.FontName = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.titleArea.Location = new System.Drawing.Point(0, 0);
            this.titleArea.Name = "titleArea";
            this.titleArea.Size = new System.Drawing.Size(1070, 40);
            this.titleArea.TabIndex = 0;
            this.titleArea.TextColor = System.Drawing.Color.Empty;
            this.titleArea.TextName = "";
            // 
            // uGroupBoxSearchArea
            // 
            this.uGroupBoxSearchArea.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.uGroupBoxSearchArea.Appearance = appearance1;
            this.uGroupBoxSearchArea.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPackage);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uComboSearchPlant);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchDurableMatName);
            this.uGroupBoxSearchArea.Controls.Add(this.uLabelSearchPlant);
            this.uGroupBoxSearchArea.Location = new System.Drawing.Point(0, 40);
            this.uGroupBoxSearchArea.Name = "uGroupBoxSearchArea";
            this.uGroupBoxSearchArea.Size = new System.Drawing.Size(1070, 40);
            this.uGroupBoxSearchArea.TabIndex = 2;
            // 
            // uComboSearchPackage
            // 
            this.uComboSearchPackage.Location = new System.Drawing.Point(424, 10);
            this.uComboSearchPackage.Name = "uComboSearchPackage";
            this.uComboSearchPackage.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchPackage.TabIndex = 9;
            // 
            // uLabelSearchPackage
            // 
            this.uLabelSearchPackage.Location = new System.Drawing.Point(296, 10);
            this.uLabelSearchPackage.Name = "uLabelSearchPackage";
            this.uLabelSearchPackage.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchPackage.TabIndex = 8;
            // 
            // uComboSearchDurableMatName
            // 
            this.uComboSearchDurableMatName.Location = new System.Drawing.Point(140, 10);
            this.uComboSearchDurableMatName.MaxLength = 50;
            this.uComboSearchDurableMatName.Name = "uComboSearchDurableMatName";
            this.uComboSearchDurableMatName.Size = new System.Drawing.Size(144, 21);
            this.uComboSearchDurableMatName.TabIndex = 7;
            // 
            // uComboSearchPlant
            // 
            this.uComboSearchPlant.Location = new System.Drawing.Point(920, 10);
            this.uComboSearchPlant.MaxLength = 50;
            this.uComboSearchPlant.Name = "uComboSearchPlant";
            this.uComboSearchPlant.Size = new System.Drawing.Size(36, 21);
            this.uComboSearchPlant.TabIndex = 6;
            this.uComboSearchPlant.Visible = false;
            this.uComboSearchPlant.ValueChanged += new System.EventHandler(this.uComboSearchPlant_ValueChanged);
            // 
            // uLabelSearchDurableMatName
            // 
            this.uLabelSearchDurableMatName.Location = new System.Drawing.Point(12, 10);
            this.uLabelSearchDurableMatName.Name = "uLabelSearchDurableMatName";
            this.uLabelSearchDurableMatName.Size = new System.Drawing.Size(124, 20);
            this.uLabelSearchDurableMatName.TabIndex = 4;
            // 
            // uLabelSearchPlant
            // 
            this.uLabelSearchPlant.Location = new System.Drawing.Point(900, 10);
            this.uLabelSearchPlant.Name = "uLabelSearchPlant";
            this.uLabelSearchPlant.Size = new System.Drawing.Size(16, 20);
            this.uLabelSearchPlant.TabIndex = 5;
            this.uLabelSearchPlant.Visible = false;
            // 
            // uGroupDurable
            // 
            this.uGroupDurable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.uGroupDurable.Controls.Add(this.uGridDurableMat);
            this.uGroupDurable.Location = new System.Drawing.Point(0, 80);
            this.uGroupDurable.Name = "uGroupDurable";
            this.uGroupDurable.Size = new System.Drawing.Size(460, 760);
            this.uGroupDurable.TabIndex = 7;
            // 
            // uGridDurableMat
            // 
            this.uGridDurableMat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            appearance2.BackColor = System.Drawing.SystemColors.Window;
            appearance2.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableMat.DisplayLayout.Appearance = appearance2;
            this.uGridDurableMat.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableMat.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMat.DisplayLayout.GroupByBox.BandLabelAppearance = appearance4;
            this.uGridDurableMat.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance5.BackColor2 = System.Drawing.SystemColors.Control;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance5.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableMat.DisplayLayout.GroupByBox.PromptAppearance = appearance5;
            this.uGridDurableMat.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableMat.DisplayLayout.MaxRowScrollRegions = 1;
            appearance6.BackColor = System.Drawing.SystemColors.Window;
            appearance6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableMat.DisplayLayout.Override.ActiveCellAppearance = appearance6;
            appearance7.BackColor = System.Drawing.SystemColors.Highlight;
            appearance7.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableMat.DisplayLayout.Override.ActiveRowAppearance = appearance7;
            this.uGridDurableMat.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableMat.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.Override.CardAreaAppearance = appearance8;
            appearance9.BorderColor = System.Drawing.Color.Silver;
            appearance9.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableMat.DisplayLayout.Override.CellAppearance = appearance9;
            this.uGridDurableMat.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableMat.DisplayLayout.Override.CellPadding = 0;
            appearance10.BackColor = System.Drawing.SystemColors.Control;
            appearance10.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance10.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance10.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance10.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableMat.DisplayLayout.Override.GroupByRowAppearance = appearance10;
            appearance11.TextHAlignAsString = "Left";
            this.uGridDurableMat.DisplayLayout.Override.HeaderAppearance = appearance11;
            this.uGridDurableMat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableMat.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance12.BackColor = System.Drawing.SystemColors.Window;
            appearance12.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableMat.DisplayLayout.Override.RowAppearance = appearance12;
            this.uGridDurableMat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance13.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableMat.DisplayLayout.Override.TemplateAddRowAppearance = appearance13;
            this.uGridDurableMat.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableMat.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableMat.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableMat.Location = new System.Drawing.Point(12, 28);
            this.uGridDurableMat.Name = "uGridDurableMat";
            this.uGridDurableMat.Size = new System.Drawing.Size(440, 720);
            this.uGridDurableMat.TabIndex = 3;
            this.uGridDurableMat.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.uGridDurableMat_DoubleClickRow);
            // 
            // uGroupBoxDurableMatLot
            // 
            this.uGroupBoxDurableMatLot.Controls.Add(this.uTextDurableMatName);
            this.uGroupBoxDurableMatLot.Controls.Add(this.uButtonDelete);
            this.uGroupBoxDurableMatLot.Controls.Add(this.uTextLotNo);
            this.uGroupBoxDurableMatLot.Controls.Add(this.uTextDurableMatCode);
            this.uGroupBoxDurableMatLot.Controls.Add(this.uTextPlantCode);
            this.uGroupBoxDurableMatLot.Controls.Add(this.uButtonMove);
            this.uGroupBoxDurableMatLot.Controls.Add(this.uGridLotNoSpecInfo);
            this.uGroupBoxDurableMatLot.Controls.Add(this.uGridDurableSpec);
            this.uGroupBoxDurableMatLot.Location = new System.Drawing.Point(464, 80);
            this.uGroupBoxDurableMatLot.Name = "uGroupBoxDurableMatLot";
            this.uGroupBoxDurableMatLot.Size = new System.Drawing.Size(600, 760);
            this.uGroupBoxDurableMatLot.TabIndex = 8;
            // 
            // uButtonDelete
            // 
            this.uButtonDelete.Location = new System.Drawing.Point(12, 416);
            this.uButtonDelete.Name = "uButtonDelete";
            this.uButtonDelete.Size = new System.Drawing.Size(88, 28);
            this.uButtonDelete.TabIndex = 5;
            this.uButtonDelete.Click += new System.EventHandler(this.uButtonDelete_Click);
            // 
            // uTextLotNo
            // 
            this.uTextLotNo.Location = new System.Drawing.Point(564, 420);
            this.uTextLotNo.Name = "uTextLotNo";
            this.uTextLotNo.Size = new System.Drawing.Size(21, 21);
            this.uTextLotNo.TabIndex = 4;
            // 
            // uTextDurableMatCode
            // 
            this.uTextDurableMatCode.Location = new System.Drawing.Point(536, 420);
            this.uTextDurableMatCode.Name = "uTextDurableMatCode";
            this.uTextDurableMatCode.Size = new System.Drawing.Size(21, 21);
            this.uTextDurableMatCode.TabIndex = 3;
            // 
            // uTextPlantCode
            // 
            this.uTextPlantCode.Location = new System.Drawing.Point(508, 420);
            this.uTextPlantCode.Name = "uTextPlantCode";
            this.uTextPlantCode.Size = new System.Drawing.Size(21, 21);
            this.uTextPlantCode.TabIndex = 2;
            // 
            // uButtonMove
            // 
            this.uButtonMove.Location = new System.Drawing.Point(252, 416);
            this.uButtonMove.Name = "uButtonMove";
            this.uButtonMove.Size = new System.Drawing.Size(88, 28);
            this.uButtonMove.TabIndex = 1;
            this.uButtonMove.Click += new System.EventHandler(this.uButtonMove_Click);
            // 
            // uGridLotNoSpecInfo
            // 
            appearance3.BackColor = System.Drawing.SystemColors.Window;
            appearance3.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridLotNoSpecInfo.DisplayLayout.Appearance = appearance3;
            this.uGridLotNoSpecInfo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridLotNoSpecInfo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLotNoSpecInfo.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLotNoSpecInfo.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.uGridLotNoSpecInfo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridLotNoSpecInfo.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.uGridLotNoSpecInfo.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridLotNoSpecInfo.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.CellAppearance = appearance21;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.uGridLotNoSpecInfo.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.RowAppearance = appearance24;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridLotNoSpecInfo.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.uGridLotNoSpecInfo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridLotNoSpecInfo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridLotNoSpecInfo.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridLotNoSpecInfo.Location = new System.Drawing.Point(12, 444);
            this.uGridLotNoSpecInfo.Name = "uGridLotNoSpecInfo";
            this.uGridLotNoSpecInfo.Size = new System.Drawing.Size(576, 304);
            this.uGridLotNoSpecInfo.TabIndex = 0;
            this.uGridLotNoSpecInfo.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.uGridLotNoSpecInfo_AfterCellUpdate);
            // 
            // uGridDurableSpec
            // 
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.uGridDurableSpec.DisplayLayout.Appearance = appearance26;
            this.uGridDurableSpec.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.uGridDurableSpec.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableSpec.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableSpec.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.uGridDurableSpec.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.uGridDurableSpec.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.uGridDurableSpec.DisplayLayout.MaxColScrollRegions = 1;
            this.uGridDurableSpec.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uGridDurableSpec.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.uGridDurableSpec.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.uGridDurableSpec.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.uGridDurableSpec.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.uGridDurableSpec.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance37.BorderColor = System.Drawing.Color.Silver;
            appearance37.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.uGridDurableSpec.DisplayLayout.Override.CellAppearance = appearance37;
            this.uGridDurableSpec.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.uGridDurableSpec.DisplayLayout.Override.CellPadding = 0;
            appearance38.BackColor = System.Drawing.SystemColors.Control;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.uGridDurableSpec.DisplayLayout.Override.GroupByRowAppearance = appearance38;
            appearance39.TextHAlignAsString = "Left";
            this.uGridDurableSpec.DisplayLayout.Override.HeaderAppearance = appearance39;
            this.uGridDurableSpec.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.uGridDurableSpec.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            appearance40.BorderColor = System.Drawing.Color.Silver;
            this.uGridDurableSpec.DisplayLayout.Override.RowAppearance = appearance40;
            this.uGridDurableSpec.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance41.BackColor = System.Drawing.SystemColors.ControlLight;
            this.uGridDurableSpec.DisplayLayout.Override.TemplateAddRowAppearance = appearance41;
            this.uGridDurableSpec.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.uGridDurableSpec.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.uGridDurableSpec.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.uGridDurableSpec.Location = new System.Drawing.Point(12, 28);
            this.uGridDurableSpec.Name = "uGridDurableSpec";
            this.uGridDurableSpec.Size = new System.Drawing.Size(576, 388);
            this.uGridDurableSpec.TabIndex = 0;
            // 
            // uTextDurableMatName
            // 
            this.uTextDurableMatName.Location = new System.Drawing.Point(480, 420);
            this.uTextDurableMatName.Name = "uTextDurableMatName";
            this.uTextDurableMatName.Size = new System.Drawing.Size(20, 21);
            this.uTextDurableMatName.TabIndex = 6;
            this.uTextDurableMatName.Visible = false;
            // 
            // frmMASZ0032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1070, 850);
            this.ControlBox = false;
            this.Controls.Add(this.uGroupBoxDurableMatLot);
            this.Controls.Add(this.uGroupDurable);
            this.Controls.Add(this.uGroupBoxSearchArea);
            this.Controls.Add(this.titleArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMASZ0032";
            this.Load += new System.EventHandler(this.frmMASZ0032_Load);
            this.Activated += new System.EventHandler(this.frmMASZ0032_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMASZ0032_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxSearchArea)).EndInit();
            this.uGroupBoxSearchArea.ResumeLayout(false);
            this.uGroupBoxSearchArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPackage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchDurableMatName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uComboSearchPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupDurable)).EndInit();
            this.uGroupDurable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGroupBoxDurableMatLot)).EndInit();
            this.uGroupBoxDurableMatLot.ResumeLayout(false);
            this.uGroupBoxDurableMatLot.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uTextLotNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextPlantCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridLotNoSpecInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uGridDurableSpec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uTextDurableMatName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private QRPUserControl.TitleArea titleArea;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxSearchArea;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPackage;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPackage;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchDurableMatName;
        private Infragistics.Win.UltraWinEditors.UltraComboEditor uComboSearchPlant;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchDurableMatName;
        private Infragistics.Win.Misc.UltraLabel uLabelSearchPlant;
        private Infragistics.Win.Misc.UltraGroupBox uGroupDurable;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableMat;
        private Infragistics.Win.Misc.UltraGroupBox uGroupBoxDurableMatLot;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridLotNoSpecInfo;
        private Infragistics.Win.UltraWinGrid.UltraGrid uGridDurableSpec;
        private Infragistics.Win.Misc.UltraButton uButtonMove;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextLotNo;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatCode;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextPlantCode;
        private Infragistics.Win.Misc.UltraButton uButtonDelete;
        private Infragistics.Win.UltraWinEditors.UltraTextEditor uTextDurableMatName;
    }
}