﻿/*----------------------------------------------------------------------*/
/* 시스템명     : 공통기준정보                                          */
/* 모듈(분류)명 : 공정관리 기준정보                                     */
/* 프로그램ID   : frmMAS0001.cs                                         */
/* 프로그램명   : 공장정보                                              */
/* 작성자       : 이종호                                                */
/* 작성일자     : 2011-07-20                                            */
/* 수정이력     : xxxx-xx-xx : ~~~~~ 수정 (홍길동)                      */
/*                xxxx-xx-xx : ~~~~~ 추가 (홍길동)                      */
/*----------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//추가참조
using QRPCOM.QRPGLO;
using QRPCOM.QRPUI;
using System.EnterpriseServices;    //COM+ 서비스 사용하기위함
using System.Threading;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Resources;

using Microsoft.Win32;

// Form Class 앞부분에 해당묘듈의 namespace 추가
namespace QRPMAS.UI
{
    // IToolbar Interface 상속 추가
    public partial class frmMAS0001 : Form, IToolbar
    {
        // 리소스 호출을 위한 전역변수
        QRPGlobal SysRes = new QRPGlobal();

        public frmMAS0001()
        {
            InitializeComponent();
        }

        // 툴바버튼 활성여부 처리 이벤트
        private void frmMAS0001_Activated(object sender, EventArgs e)
        {
            // 해당화면에 대한 툴바버튼 활성화 여부 처리
            QRPBrowser toolButton = new QRPBrowser();
            ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
            toolButton.mfActiveToolBar(this.ParentForm, true, false, false, false, false, true, m_resSys.GetString("SYS_USERID"), this.Name);
        }

        private void frmMAS0001_Load(object sender, EventArgs e)
        {
            try
            {
                // SystemInfo Resource 변수 선언 => 언어, 폰트, 사용자IP, 사용자ID, 공장코드, 부서코드
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                // 타이틀 Text 설정함수 호출
                this.titleArea.mfSetLabelText("공장정보", m_resSys.GetString("SYS_FONTNAME"), 12);

                // 그리드 초기화 함수 호출
                SetToolAuth();
                InitGrid();

                QRPCOM.QRPUI.WinGrid grd = new WinGrid();
                grd.mfLoadGridColumnProperty(this);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        #region 초기화 Method

        /// <summary>
        /// 사용자-화면툴바 권한 설정
        /// </summary>
        private void SetToolAuth()
        {
            try
            {
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 사용자에 대한 프로그램 권한정보 읽기//
                QRPCOM.QRPGLO.QRPBrowser brwChannel = new QRPBrowser();

                brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.UserAuth), "UserAuth");
                QRPSYS.BL.SYSPGM.UserAuth UAuth = new QRPSYS.BL.SYSPGM.UserAuth();
                brwChannel.mfCredentials(UAuth);
                DataTable dtAuth = UAuth.mfReadUserAuth_Program(m_resSys.GetString("SYS_PLANTCODE"), m_resSys.GetString("SYS_USERID"), this.Name, m_resSys.GetString("SYS_LANG"));

                m_resSys.Close();
                QRPCOM.QRPGLO.QRPGlobal Global = new QRPCOM.QRPGLO.QRPGlobal();
                Global.mfMakeToolInfoResource(dtAuth);

            }
            catch (System.Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }


        /// <summary>
        /// 그리드초기화
        /// </summary>
        private void InitGrid()
        {
            try
            {
                // SystemInfo Resource 변수
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);

                // 그리드 일반속성 초기화
                WinGrid grd = new WinGrid();
                grd.mfInitGeneralGrid(uGridPlantList, true, NewColumnLoadStyle.Hide, AutoFitStyle.ResizeAllColumns
                    , false, DefaultableBoolean.False, MergedCellStyle.Never
                    , true, FixedHeaderIndicator.Button, SelectType.Single
                    , DefaultableBoolean.True, FilterUIType.HeaderIcons
                    , AllowAddNew.No, 0, false, m_resSys.GetString("SYS_FONTNAME"));

                // 그리드에 열추가
                ////grd.mfSetGridColumn(uGridPlantList, 0, "Check", "선택", false, Activation.AllowEdit, 30, false, false, 0
                ////    , HAlign.Center, VAlign.Middle, MergedCellStyle.Never
                ////    , Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox, "", "", "false");

                grd.mfSetGridColumn(uGridPlantList, 0, "PlantCode", "공장코드", false, Activation.AllowEdit, 100, false, false, 10
                    , HAlign.Center, VAlign.Middle, MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridPlantList, 0, "PlantName", "공장명", false, Activation.AllowEdit, 150, false, false, 50
                    , HAlign.Left, VAlign.Middle, MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridPlantList, 0, "PlantNameCh", "공장명_중문", false, Activation.AllowEdit, 150, false, false, 50
                    , HAlign.Left, VAlign.Middle, MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridPlantList, 0, "PlantNameEn", "공장명_영문", false, Activation.AllowEdit, 150, false, true, 50
                    , HAlign.Left, VAlign.Middle, MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridPlantList, 0, "RegionName", "지역명", false, Activation.AllowEdit, 130, false, true, 20
                    , HAlign.Left, VAlign.Middle, MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridPlantList, 0, "CountryName", "국가명", false, Activation.AllowEdit, 130, false, true, 20
                    , HAlign.Left, VAlign.Middle, MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                grd.mfSetGridColumn(uGridPlantList, 0, "UseFlag", "사용여부", false, Activation.ActivateOnly, 100, false, false, 1
                    , HAlign.Center, VAlign.Middle, MergedCellStyle.Never
                    , Infragistics.Win.UltraWinGrid.ColumnStyle.Edit, "", "", "");

                // Set FontSize
                this.uGridPlantList.DisplayLayout.Override.HeaderAppearance.FontData.SizeInPoints = 9;
                this.uGridPlantList.DisplayLayout.Override.CellAppearance.FontData.SizeInPoints = 9;

                ////////// 그리드 컬럼에 사용여부 콤보박스 추가
                ////// Call BL
                ////QRPBrowser brwChannel = new QRPBrowser();
                ////brwChannel.mfRegisterChannel(typeof(QRPSYS.BL.SYSPGM.CommonCode), "CommonCode");
                ////QRPSYS.BL.SYSPGM.CommonCode clsCommonCode = new QRPSYS.BL.SYSPGM.CommonCode();
                ////brwChannel.mfCredentials(clsCommonCode);

                ////DataTable dtUseFlag = clsCommonCode.mfReadCommonCode("C0001", "KOR");

                ////// 그리드 컬럼에 콤보박스 추가
                ////grd.mfSetGridColumnValueList(uGridPlantList, 0, "UseFlag", ValueListDisplayStyle.DisplayText, "", "", dtUseFlag);
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }
        #endregion

        #region ToolBar...
        /// <summary>
        /// 조회
        /// </summary>
        public void mfSearch()
        {
            try
            {
                // QRPBrowser Interface 생성
                QRPBrowser brwChannel = new QRPBrowser();

                // SystemInfo Resource 변수
                ResourceSet m_resSys = new ResourceSet(SysRes.SystemInfoRes);
                WinMessageBox msg = new WinMessageBox();
                QRPProgressBar m_ProgressPopup = new QRPProgressBar();
                Thread threadPop = m_ProgressPopup.mfStartThread();
                m_ProgressPopup.mfOpenProgressPopup(this, msg.GetMessge_Text("M000220", m_resSys.GetString("SYS_LANG")));
                //m_ProgressPopup.mfStartThread(this, "검색중...");
                //this.MdiParent.Cursor = Cursors.WaitCursor;

                WinGrid grd = new WinGrid();
                // 처리로직 //
                // BL 호출

                // 함수호출을 통해 App.Server와 연결
                brwChannel.mfRegisterChannel(typeof(QRPMAS.BL.MASPRC.Plant), "Plant");
                // BL객체 Interface 생성
                QRPMAS.BL.MASPRC.Plant clsPlant = new QRPMAS.BL.MASPRC.Plant();
                // 생성한 객체에 권한 설정
                brwChannel.mfCredentials(clsPlant);

                // 조회 함수호출
                DataTable dtPlant = clsPlant.mfReadMASPlant(m_resSys.GetString("SYS_LANG"));
                this.uGridPlantList.DataSource = dtPlant;
                this.uGridPlantList.DataBind();

                ////// 바인딩후 체크박스 상태를 모두 Uncheck로 만든다
                ////grd.mfSetAllUnCheckedGridColumn(this.uGridPlantList, 0, "Check");

                // 처리로직 - 끝 //

                this.MdiParent.Cursor = Cursors.Default;
                m_ProgressPopup.mfCloseProgressPopup(this);

                DialogResult DResult = new DialogResult();
                // 조회 결과가 없을시 메세지창 띄움

                if (dtPlant.Rows.Count == 0)
                    DResult = msg.mfSetMessageBox(MessageBoxType.Information, 500, 500, Infragistics.Win.UltraMessageBox.MessageBoxStyle.Vista
                        , "M001135", "M001115", "M001102", Infragistics.Win.HAlign.Right);
                else
                {
                    grd.mfSetAutoResizeColWidth(this.uGridPlantList, 0);
                }
            }
            catch (Exception ex)
            {
                QRPCOM.frmErrorInfo frmErr = new QRPCOM.frmErrorInfo(ex);
                frmErr.ShowDialog();
            }
            finally
            {
            }
        }

        /// <summary>
        /// 저장
        /// </summary>
        public void mfSave()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        /// <summary>
        /// 삭제
        /// </summary>
        public void mfDelete()
        {
            try
            {
            }
            catch
            {
            }
            finally
            {
            }
        }

        // 신규
        public void mfCreate()
        {
        }

        // 출력
        public void mfPrint()
        {
        }

        // 엑셀다운
        public void mfExcel()
        {
            WinGrid grd = new WinGrid();

            //엑셀저장함수 호출
            grd.mfDownLoadGridToExcel(this.uGridPlantList);
        }
        #endregion

        private void frmMAS0001_FormClosing(object sender, FormClosingEventArgs e)
        {
            WinGrid grd = new WinGrid();
            grd.mfSaveGridColumnProperty(this);
        }
    }
}